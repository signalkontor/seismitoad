using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;

namespace AdvantageModel.Models
{
    public partial class doa_kontaktdaten
    {
        public const string Contact = "Kontaktdaten";

        [ExcludeFromExport]
        [Display(Name = "ID",Description = Contact)]
        public int id { get; set; }

        [ExcludeFromExport]
        public string ds_status { get; set; }
        
        [ExcludeFromExport]
        [Display(Name = "Änderungsdatum", Description = Contact)]
        public System.DateTime changed_time { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Nachname", Description = Contact)]
        public string nachname { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Vorname", Description = Contact)]
        public string vorname { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Geburtsdatum", Description = Contact)]
        public System.DateTime geburtsdatum { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Geschlecht", Description = Contact)]
        public string geschlecht { get; set; } /* m=männlich, w=weiblich */

        /* 
         * 1 Aktiv
         * 2 Neu
         * 3 Inaktiv
         * 4 Fremdpersonal
         * 5 Gesperrt
         */

        [ExcludeFromExport]
        [Display(Name = "Benutzerstatus", Description = Contact)]
        public string userstatus { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Bemerkung", Description = Contact)]
        public string userstatus_bemerkung { get; set; }

        [Display(Name = "Erstkontakt durch", Description = Contact)]
        public string erstkontakt { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Straße", Description = Contact)]
        public string strasse { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Postleitzahl", Description = Contact)]
        public string plz { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Ort", Description = Contact)]
        public string ort { get; set; }
        
        [ExcludeFromExport]
        [Display(Name = "Land", Description = Contact)]
        public string land { get; set; }

        [Display(Name = "Straße (Anschrift 2)", Description = Contact)]
        public string strasse2 { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Postleitzahl (Anschrift 2)", Description = Contact)]
        public string plz2 { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Ort (Anschrift 2)", Description = Contact)]
        public string ort2 { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Land (Anschrift 2)", Description = Contact)]
        public string land2 { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Vorwahl 1", Description = Contact)]
        public string tel1_vorwahl { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Rufnummer 1", Description = Contact)]
        public string tel1_nummer { get; set; }

        [Display(Name = "Vorwahl 2", Description = Contact)]
        public string tel2_vorwahl { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Rufnummer 2", Description = Contact)]
        public string tel2_nummer { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Fax-Vorwahl", Description = Contact)]
        public string fax_vorwahl { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Fax-Rufnummer", Description = Contact)]
        public string fax_nummer { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Mobil-Vorwahl", Description = Contact)]
        public string mobil_vorwahl { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Mobil-Rufnummer", Description = Contact)]
        public string mobil_nummer { get; set; }

        [ExcludeFromExport]
        [Display(Name = "E-Mail 1", Description = Contact)]
        public string email1 { get; set; }

        [ExcludeFromExport]
        [Display(Name = "E-Mail 2", Description = Contact)]
        public string email2 { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Webseite", Description = Contact)]
        public string homepage { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Gesundheitszeugnis", Description = Contact)]
        [UIHint("Bool")]
        public string gesundheitszeugnis { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Kopie vorhanden", Description = Contact)]
        [UIHint("Bool")]
        public string gesundheitszeugnis_kopie { get; set; }

        [ExcludeFromExport]
        public string gesundheitszeugnis_dateiname { get; set; }

        [Display(Name = "Gewerbeschein", Description = Contact)]
        [ExcludeFromExport]
        [UIHint("Bool")]
        public string gewerbeschein { get; set; }

        [Display(Name = "Kopie vorhanden", Description = Contact)]
        [UIHint("Bool")]
        [ExcludeFromExport]
        public string gewerbeschein_kopie { get; set; }

        [ExcludeFromExport]
        public string gewerbeschein_dateiname { get; set; }

        [ExcludeFromExport]
        public string kopie_vorhanden { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        public string fs_dateiname { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Führerscheinklasse", Description = Contact)]
        [ExcludeFromExport]
        public string fs_klasse { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Führerschein seit", Description = Contact)]
        [UIHint("Date")]
        public System.DateTime fs_seit_date { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Bemerkungen", Description = Contact)]
        public string fs_bemerkung { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Eigener PKW", Description = Contact)]
        [UIHint("Bool")]
        public string eigener_pkw { get; set; }

        [Display(Name = "Rahmenvertrag (alt)", Description = Contact)]
        [UIHint("Bool")]
        public string rahmenvertrag { get; set; }

        [Display(Name = "Rahmenvertrag (neu)", Description = Contact)]
        [UIHint("Bool")]
        public string rahmenvertrag_neu { get; set; }

        [Display(Name = "Selbstauskunft", Description = Contact)]
        [UIHint("Bool")]
        public Nullable<short> selbstauskunft { get; set; }

        [ExcludeFromExport]
        public string selbstauskunft_dateiname { get; set; } // MIGRATION Keine Übernahme
        public virtual doa_login doa_login { get; set; }
    }
}
