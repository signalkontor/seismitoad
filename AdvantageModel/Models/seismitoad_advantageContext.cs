using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AdvantageModel.Models.Mapping;

namespace AdvantageModel.Models
{
    public partial class seismitoad_advantageContext : DbContext
    {
        static seismitoad_advantageContext()
        {
            Database.SetInitializer<seismitoad_advantageContext>(null);
        }

        public seismitoad_advantageContext()
            : base("Name=seismitoad_advantageContext")
        {
        }

        public DbSet<doa_bewertung> doa_bewertung { get; set; }
        public DbSet<doa_formular1> doa_formular1 { get; set; }
        public DbSet<doa_formular3> doa_formular3 { get; set; }
        public DbSet<doa_kontaktdaten> doa_kontaktdaten { get; set; }
        public DbSet<doa_kunde> doa_kunde { get; set; }
        public DbSet<doa_login> doa_login { get; set; }
        public DbSet<doa_profil> doa_profil { get; set; }
        public DbSet<doa_projekt> doa_projekt { get; set; }
        public DbSet<doa_qualifikation> doa_qualifikation { get; set; }
        public DbSet<sr_kunde2projekt> sr_kunde2projekt { get; set; }
        public DbSet<sr_projekte2promoter> sr_projekte2promoter { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new doa_bewertungMap());
            modelBuilder.Configurations.Add(new doa_formular1Map());
            modelBuilder.Configurations.Add(new doa_formular3Map());
            modelBuilder.Configurations.Add(new doa_kontaktdatenMap());
            modelBuilder.Configurations.Add(new doa_kundeMap());
            modelBuilder.Configurations.Add(new doa_loginMap());
            modelBuilder.Configurations.Add(new doa_profilMap());
            modelBuilder.Configurations.Add(new doa_projektMap());
            modelBuilder.Configurations.Add(new doa_qualifikationMap());
            modelBuilder.Configurations.Add(new sr_kunde2projektMap());
            modelBuilder.Configurations.Add(new sr_projekte2promoterMap());
        }
    }
}
