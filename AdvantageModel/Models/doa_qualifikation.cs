using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;

namespace AdvantageModel.Models
{
    public partial class doa_qualifikation
    {
        public const string Qualification = "Qualifikation";

        [ExcludeFromExport]
        public int id { get; set; }

        [ExcludeFromExport]
        public string ds_status { get; set; } /* 1=normal?, 2=in bearbeitung? 3=gel�scht? */

        [ExcludeFromExport]
        public System.DateTime changed_time { get; set; } // MIGRATION Keine �bernahme

        [ExcludeFromExport]
        [Display(Name = "Englisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_en { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [ExcludeFromExport]
        [Display(Name = "Franz�sisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_fr { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [ExcludeFromExport]
        [Display(Name = "Spanisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_sp { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [ExcludeFromExport]
        [Display(Name = "Italienisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_it { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [ExcludeFromExport]
        [Display(Name = "T�rkisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_tr { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Polnisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_pl { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [ExcludeFromExport]
        [Display(Name = "Sonstige Sprachen", Description = Qualification)]
        public string sprache_sonstige { get; set; } /* Freitext */

        [Display(Name = "Sonstige Bemerkungen", Description = Qualification)]
        public string sprache_bemerkung { get; set; } /* Wird anscheinend nicht angezeigt */ // MIGRATION Keine �bernahme

        [Display(Name = "interne Schulungen", Description = Qualification)]
        public string schulung_intern { get; set; } /* Ungenutzt? In der DB immer leer */// MIGRATION Keine �bernahme

        [Display(Name = "externe Schulungen", Description = Qualification)]
        public string schulung_extern { get; set; } /* Freitext */

        [Display(Name = "Projekte", Description = Qualification)]
        public string projekte { get; set; } /* Ungenutzt? In der DB immer leer */

        [Display(Name = "besondere F�higkeiten", Description = Qualification)]
        public string besondere_faehigkeiten { get; set; } /* Freitext */

        [ExcludeFromExport]
        [Display(Name = "Ausbildung/Beruf", Description = Qualification)]
        public string ausbildung { get; set; } /* Freitext */

        [ExcludeFromExport]
        [Display(Name = "Bevorzugte Arbeitszeiten", Description = Qualification)]
        public string bevorzugte_arbeitszeiten { get; set; } /* XXXXX X=0|1=Ja|Nein Reihenfolge: Werktags, Abends, Am Wochenende, Immer, Ferien-/Semesterferien */

        [Display(Name = "Sonstige Bemerkungen", Description = Qualification)]
        public string sonstige_bemerkungen { get; set; }

        [Display(Name = "Gastronomie (Promotionerfahrung)", Description = Qualification)]
        public string gastro { get; set; } /* Freitext */ // MIGRATION Keine �bernahme

        [Display(Name = "Sampling (Promotionerfahrung)", Description = Qualification)]
        public string sampling { get; set; } /* Freitext */ // MIGRATION Keine �bernahme

        [Display(Name = "Fachberatung (Promotionerfahrung)", Description = Qualification)]
        public string fachberatung { get; set; } /* Freitext */ // MIGRATION Keine �bernahme

        [Display(Name = "Messe/Event (Promotionerfahrung)", Description = Qualification)]
        public string messe_event { get; set; } /* Freitext */ // MIGRATION Keine �bernahme

        [Display(Name = "Verkostung (Promotionerfahrung)", Description = Qualification)]
        public string verkostung { get; set; } /* Freitext */ // MIGRATION Keine �bernahme

        [ExcludeFromExport]
        [Display(Name = "Sonstige (Promotionerfahrung)", Description = Qualification)]
        public string sonstige { get; set; } /* Freitext */

        [Display(Name = "Moderation", Description = Qualification)]
        [UIHint("Bool")]
        public string moderation { get; set; } /* 1= ja, 0 = nein */ // MIGRATION Keine �bernahme

        [Display(Name = "Aufbauhelfer", Description = Qualification)]
        [UIHint("Bool")]
        public string aufbauhelfer { get; set; } /* 1= ja, 0 = nein */ // MIGRATION Keine �bernahme

        [Display(Name = "VIP-Einsatz", Description = Qualification)]
        [UIHint("Bool")]
        public string vip_einsatz { get; set; } /* 1= ja, 0 = nein */ // MIGRATION Keine �bernahme

        [Display(Name = "Model", Description = Qualification)]
        [UIHint("Bool")]
        public string model { get; set; } /* 1= ja, 0 = nein */ // MIGRATION Keine �bernahme

        [Display(Name = "Mysteryshopper", Description = Qualification)]
        [UIHint("Bool")]
        public string mysteryshopper { get; set; } /* 1= ja, 0 = nein */ // MIGRATION Keine �bernahme

        [ExcludeFromExport]
        [Display(Name = "Computerkenntnisse", Description = Qualification)]
        public string pc_kenntnisse { get; set; } /* Freitext */

        [ExcludeFromExport]
        [Display(Name = "�bernachtung m�glich in", Description = Qualification)]
        public string uebernachtung { get; set; } /* Freitext */

        [Display(Name = "Eingabedatum", Description = Qualification)]
        public System.DateTime eingabe_datum { get; set; } // MIGRATION Keine �bernahme

        [Display(Name = "�nderungsdatum", Description = Qualification)]
        public System.DateTime update_datum { get; set; } // MIGRATION Keine �bernahme

        [Display(Name = "�nderung durch", Description = Qualification)]
        public string update_durch { get; set; } // MIGRATION Keine �bernahme

        [Display(Name = "Bemerkungen", Description = Qualification)]
        [ExcludeFromExport]
        public string bemerkung { get; set; } /* Freitext */
        public virtual doa_login doa_login { get; set; }
    }
}
