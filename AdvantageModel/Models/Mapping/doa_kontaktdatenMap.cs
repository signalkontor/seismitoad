using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_kontaktdatenMap : EntityTypeConfiguration<doa_kontaktdaten>
    {
        public doa_kontaktdatenMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.nachname)
                .HasMaxLength(255);

            this.Property(t => t.vorname)
                .HasMaxLength(255);

            this.Property(t => t.geschlecht)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.userstatus)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.userstatus_bemerkung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.erstkontakt)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.strasse)
                .HasMaxLength(255);

            this.Property(t => t.plz)
                .HasMaxLength(7);

            this.Property(t => t.ort)
                .HasMaxLength(255);

            this.Property(t => t.land)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.strasse2)
                .HasMaxLength(255);

            this.Property(t => t.plz2)
                .HasMaxLength(7);

            this.Property(t => t.ort2)
                .HasMaxLength(255);

            this.Property(t => t.land2)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.tel1_vorwahl)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.tel1_nummer)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.tel2_vorwahl)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.tel2_nummer)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.fax_vorwahl)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.fax_nummer)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.mobil_vorwahl)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.mobil_nummer)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.email1)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.email2)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.homepage)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.gesundheitszeugnis)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.gesundheitszeugnis_kopie)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.gesundheitszeugnis_dateiname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.gewerbeschein)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.gewerbeschein_kopie)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.gewerbeschein_dateiname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.kopie_vorhanden)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.fs_dateiname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.fs_klasse)
                .IsRequired()
                .HasMaxLength(60);

            this.Property(t => t.fs_bemerkung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.eigener_pkw)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.rahmenvertrag)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.rahmenvertrag_neu)
                .HasMaxLength(1);

            this.Property(t => t.selbstauskunft_dateiname)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("doa_kontaktdaten");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.nachname).HasColumnName("nachname");
            this.Property(t => t.vorname).HasColumnName("vorname");
            this.Property(t => t.geburtsdatum).HasColumnName("geburtsdatum");
            this.Property(t => t.geschlecht).HasColumnName("geschlecht");
            this.Property(t => t.userstatus).HasColumnName("userstatus");
            this.Property(t => t.userstatus_bemerkung).HasColumnName("userstatus_bemerkung");
            this.Property(t => t.erstkontakt).HasColumnName("erstkontakt");
            this.Property(t => t.strasse).HasColumnName("strasse");
            this.Property(t => t.plz).HasColumnName("plz");
            this.Property(t => t.ort).HasColumnName("ort");
            this.Property(t => t.land).HasColumnName("land");
            this.Property(t => t.strasse2).HasColumnName("strasse2");
            this.Property(t => t.plz2).HasColumnName("plz2");
            this.Property(t => t.ort2).HasColumnName("ort2");
            this.Property(t => t.land2).HasColumnName("land2");
            this.Property(t => t.tel1_vorwahl).HasColumnName("tel1_vorwahl");
            this.Property(t => t.tel1_nummer).HasColumnName("tel1_nummer");
            this.Property(t => t.tel2_vorwahl).HasColumnName("tel2_vorwahl");
            this.Property(t => t.tel2_nummer).HasColumnName("tel2_nummer");
            this.Property(t => t.fax_vorwahl).HasColumnName("fax_vorwahl");
            this.Property(t => t.fax_nummer).HasColumnName("fax_nummer");
            this.Property(t => t.mobil_vorwahl).HasColumnName("mobil_vorwahl");
            this.Property(t => t.mobil_nummer).HasColumnName("mobil_nummer");
            this.Property(t => t.email1).HasColumnName("email1");
            this.Property(t => t.email2).HasColumnName("email2");
            this.Property(t => t.homepage).HasColumnName("homepage");
            this.Property(t => t.gesundheitszeugnis).HasColumnName("gesundheitszeugnis");
            this.Property(t => t.gesundheitszeugnis_kopie).HasColumnName("gesundheitszeugnis_kopie");
            this.Property(t => t.gesundheitszeugnis_dateiname).HasColumnName("gesundheitszeugnis_dateiname");
            this.Property(t => t.gewerbeschein).HasColumnName("gewerbeschein");
            this.Property(t => t.gewerbeschein_kopie).HasColumnName("gewerbeschein_kopie");
            this.Property(t => t.gewerbeschein_dateiname).HasColumnName("gewerbeschein_dateiname");
            this.Property(t => t.kopie_vorhanden).HasColumnName("kopie_vorhanden");
            this.Property(t => t.fs_dateiname).HasColumnName("fs_dateiname");
            this.Property(t => t.fs_klasse).HasColumnName("fs_klasse");
            this.Property(t => t.fs_seit_date).HasColumnName("fs_seit_date");
            this.Property(t => t.fs_bemerkung).HasColumnName("fs_bemerkung");
            this.Property(t => t.eigener_pkw).HasColumnName("eigener_pkw");
            this.Property(t => t.rahmenvertrag).HasColumnName("rahmenvertrag");
            this.Property(t => t.rahmenvertrag_neu).HasColumnName("rahmenvertrag_neu");
            this.Property(t => t.selbstauskunft).HasColumnName("selbstauskunft");
            this.Property(t => t.selbstauskunft_dateiname).HasColumnName("selbstauskunft_dateiname");

            // Relationships
            this.HasRequired(t => t.doa_login)
                .WithOptional(t => t.doa_kontaktdaten);

        }
    }
}
