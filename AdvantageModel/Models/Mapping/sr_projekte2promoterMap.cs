using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class sr_projekte2promoterMap : EntityTypeConfiguration<sr_projekte2promoter>
    {
        public sr_projekte2promoterMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status, t.changed_time, t.id_added_doa, t.gebucht });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.id_added_doa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.gebucht)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("sr_projekte2promoter");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.id_added_doa).HasColumnName("id_added_doa");
            this.Property(t => t.gebucht).HasColumnName("gebucht");
        }
    }
}
