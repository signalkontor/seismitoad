using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_loginMap : EntityTypeConfiguration<doa_login>
    {
        public doa_loginMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.login_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.password)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.loginfreigabe)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.rolle)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.user_typ)
                .IsRequired()
                .HasMaxLength(64);

            // Table & Column Mappings
            this.ToTable("doa_login");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.login_name).HasColumnName("login_name");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.loginfreigabe).HasColumnName("loginfreigabe");
            this.Property(t => t.rolle).HasColumnName("rolle");
            this.Property(t => t.user_typ).HasColumnName("user_typ");
            this.Property(t => t.first_login).HasColumnName("first_login");
            this.Property(t => t.last_login).HasColumnName("last_login");
            this.Property(t => t.skip_fragebogen).HasColumnName("skip_fragebogen");
        }
    }
}
