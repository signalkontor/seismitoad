using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_formular3Map : EntityTypeConfiguration<doa_formular3>
    {
        public doa_formular3Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.frage1)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage2)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage3)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage4)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage5)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage6)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage7)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage8)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage9)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage10)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage11)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage12)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage13)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage14)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage15)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage16)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage17)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage18)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.frage19)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.dataId)
                .IsRequired()
                .HasMaxLength(11);

            // Table & Column Mappings
            this.ToTable("doa_formular3");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.eintragsdatum).HasColumnName("eintragsdatum");
            this.Property(t => t.start_time).HasColumnName("start_time");
            this.Property(t => t.end_time).HasColumnName("end_time");
            this.Property(t => t.used_time).HasColumnName("used_time");
            this.Property(t => t.bewerter_id).HasColumnName("bewerter_id");
            this.Property(t => t.frage1).HasColumnName("frage1");
            this.Property(t => t.frage2).HasColumnName("frage2");
            this.Property(t => t.frage3).HasColumnName("frage3");
            this.Property(t => t.frage4).HasColumnName("frage4");
            this.Property(t => t.frage5).HasColumnName("frage5");
            this.Property(t => t.frage6).HasColumnName("frage6");
            this.Property(t => t.frage7).HasColumnName("frage7");
            this.Property(t => t.frage8).HasColumnName("frage8");
            this.Property(t => t.frage9).HasColumnName("frage9");
            this.Property(t => t.frage10).HasColumnName("frage10");
            this.Property(t => t.frage11).HasColumnName("frage11");
            this.Property(t => t.frage12).HasColumnName("frage12");
            this.Property(t => t.frage13).HasColumnName("frage13");
            this.Property(t => t.frage14).HasColumnName("frage14");
            this.Property(t => t.frage15).HasColumnName("frage15");
            this.Property(t => t.frage16).HasColumnName("frage16");
            this.Property(t => t.frage17).HasColumnName("frage17");
            this.Property(t => t.frage18).HasColumnName("frage18");
            this.Property(t => t.frage19).HasColumnName("frage19");
            this.Property(t => t.dataId).HasColumnName("dataId");
        }
    }
}
