using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_profilMap : EntityTypeConfiguration<doa_profil>
    {
        public doa_profilMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.groesse)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.konf_groesse)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.gewicht)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.jeansgroesse_w)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.jeansgroesse_l)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.schuhgroesse)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.haarfarbe)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.tshirt_groesse)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.tattoo_piercing)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.bemerkung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.name_bild1)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.name_bild2)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("doa_profil");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.groesse).HasColumnName("groesse");
            this.Property(t => t.konf_groesse).HasColumnName("konf_groesse");
            this.Property(t => t.gewicht).HasColumnName("gewicht");
            this.Property(t => t.jeansgroesse_w).HasColumnName("jeansgroesse_w");
            this.Property(t => t.jeansgroesse_l).HasColumnName("jeansgroesse_l");
            this.Property(t => t.schuhgroesse).HasColumnName("schuhgroesse");
            this.Property(t => t.haarfarbe).HasColumnName("haarfarbe");
            this.Property(t => t.tshirt_groesse).HasColumnName("tshirt_groesse");
            this.Property(t => t.tattoo_piercing).HasColumnName("tattoo_piercing");
            this.Property(t => t.engagement_durchschnitt).HasColumnName("engagement_durchschnitt");
            this.Property(t => t.teamfaehigkeit_durchschnitt).HasColumnName("teamfaehigkeit_durchschnitt");
            this.Property(t => t.zuverlaessigkeit_durchschnitt).HasColumnName("zuverlaessigkeit_durchschnitt");
            this.Property(t => t.kundenumgang_durchschnitt).HasColumnName("kundenumgang_durchschnitt");
            this.Property(t => t.belastbarkeit_durchschnitt).HasColumnName("belastbarkeit_durchschnitt");
            this.Property(t => t.erscheinung_durchschnitt).HasColumnName("erscheinung_durchschnitt");
            this.Property(t => t.bemerkung).HasColumnName("bemerkung");
            this.Property(t => t.name_bild1).HasColumnName("name_bild1");
            this.Property(t => t.name_bild2).HasColumnName("name_bild2");

            // Relationships
            this.HasRequired(t => t.doa_login)
                .WithOptional(t => t.doa_profil);

        }
    }
}
