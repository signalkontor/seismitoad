using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_qualifikationMap : EntityTypeConfiguration<doa_qualifikation>
    {
        public doa_qualifikationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.sprache_en)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_fr)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_sp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_it)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_tr)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_pl)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sprache_sonstige)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.sprache_bemerkung)
                .IsRequired();

            this.Property(t => t.schulung_intern)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.schulung_extern)
                .IsRequired();

            this.Property(t => t.projekte)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.besondere_faehigkeiten)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.ausbildung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.bevorzugte_arbeitszeiten)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.sonstige_bemerkungen)
                .IsRequired();

            this.Property(t => t.gastro)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.sampling)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.fachberatung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.messe_event)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.verkostung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.sonstige)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.moderation)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.aufbauhelfer)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.vip_einsatz)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.model)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.mysteryshopper)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.pc_kenntnisse)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.uebernachtung)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.update_durch)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.bemerkung)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("doa_qualifikation");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.sprache_en).HasColumnName("sprache_en");
            this.Property(t => t.sprache_fr).HasColumnName("sprache_fr");
            this.Property(t => t.sprache_sp).HasColumnName("sprache_sp");
            this.Property(t => t.sprache_it).HasColumnName("sprache_it");
            this.Property(t => t.sprache_tr).HasColumnName("sprache_tr");
            this.Property(t => t.sprache_pl).HasColumnName("sprache_pl");
            this.Property(t => t.sprache_sonstige).HasColumnName("sprache_sonstige");
            this.Property(t => t.sprache_bemerkung).HasColumnName("sprache_bemerkung");
            this.Property(t => t.schulung_intern).HasColumnName("schulung_intern");
            this.Property(t => t.schulung_extern).HasColumnName("schulung_extern");
            this.Property(t => t.projekte).HasColumnName("projekte");
            this.Property(t => t.besondere_faehigkeiten).HasColumnName("besondere_faehigkeiten");
            this.Property(t => t.ausbildung).HasColumnName("ausbildung");
            this.Property(t => t.bevorzugte_arbeitszeiten).HasColumnName("bevorzugte_arbeitszeiten");
            this.Property(t => t.sonstige_bemerkungen).HasColumnName("sonstige_bemerkungen");
            this.Property(t => t.gastro).HasColumnName("gastro");
            this.Property(t => t.sampling).HasColumnName("sampling");
            this.Property(t => t.fachberatung).HasColumnName("fachberatung");
            this.Property(t => t.messe_event).HasColumnName("messe_event");
            this.Property(t => t.verkostung).HasColumnName("verkostung");
            this.Property(t => t.sonstige).HasColumnName("sonstige");
            this.Property(t => t.moderation).HasColumnName("moderation");
            this.Property(t => t.aufbauhelfer).HasColumnName("aufbauhelfer");
            this.Property(t => t.vip_einsatz).HasColumnName("vip_einsatz");
            this.Property(t => t.model).HasColumnName("model");
            this.Property(t => t.mysteryshopper).HasColumnName("mysteryshopper");
            this.Property(t => t.pc_kenntnisse).HasColumnName("pc_kenntnisse");
            this.Property(t => t.uebernachtung).HasColumnName("uebernachtung");
            this.Property(t => t.eingabe_datum).HasColumnName("eingabe_datum");
            this.Property(t => t.update_datum).HasColumnName("update_datum");
            this.Property(t => t.update_durch).HasColumnName("update_durch");
            this.Property(t => t.bemerkung).HasColumnName("bemerkung");

            // Relationships
            this.HasRequired(t => t.doa_login)
                .WithOptional(t => t.doa_qualifikation);

        }
    }
}
