using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_formular1Map : EntityTypeConfiguration<doa_formular1>
    {
        public doa_formular1Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.frage1)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage2)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage3)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage4)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage5)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage6)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage7)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage8)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage9)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage10)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage11)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage12)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage13)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage14)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage15)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage16)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage17)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage18)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage19)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage20)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage21)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage22)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage23)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage24)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage25)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage26)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage27)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage28)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage29)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage30)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage31)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage32)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage33)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage34)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage35)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage36)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage37)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage38)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage39)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage40)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage41)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage42)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage43)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage44)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage45)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage46)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage47)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage48)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage49)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage50)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage51)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.frage52)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("doa_formular1");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dataId).HasColumnName("dataId");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.eintragsdatum).HasColumnName("eintragsdatum");
            this.Property(t => t.start_time).HasColumnName("start_time");
            this.Property(t => t.end_time).HasColumnName("end_time");
            this.Property(t => t.used_time).HasColumnName("used_time");
            this.Property(t => t.frage1).HasColumnName("frage1");
            this.Property(t => t.frage2).HasColumnName("frage2");
            this.Property(t => t.frage3).HasColumnName("frage3");
            this.Property(t => t.frage4).HasColumnName("frage4");
            this.Property(t => t.frage5).HasColumnName("frage5");
            this.Property(t => t.frage6).HasColumnName("frage6");
            this.Property(t => t.frage7).HasColumnName("frage7");
            this.Property(t => t.frage8).HasColumnName("frage8");
            this.Property(t => t.frage9).HasColumnName("frage9");
            this.Property(t => t.frage10).HasColumnName("frage10");
            this.Property(t => t.frage11).HasColumnName("frage11");
            this.Property(t => t.frage12).HasColumnName("frage12");
            this.Property(t => t.frage13).HasColumnName("frage13");
            this.Property(t => t.frage14).HasColumnName("frage14");
            this.Property(t => t.frage15).HasColumnName("frage15");
            this.Property(t => t.frage16).HasColumnName("frage16");
            this.Property(t => t.frage17).HasColumnName("frage17");
            this.Property(t => t.frage18).HasColumnName("frage18");
            this.Property(t => t.frage19).HasColumnName("frage19");
            this.Property(t => t.frage20).HasColumnName("frage20");
            this.Property(t => t.frage21).HasColumnName("frage21");
            this.Property(t => t.frage22).HasColumnName("frage22");
            this.Property(t => t.frage23).HasColumnName("frage23");
            this.Property(t => t.frage24).HasColumnName("frage24");
            this.Property(t => t.frage25).HasColumnName("frage25");
            this.Property(t => t.frage26).HasColumnName("frage26");
            this.Property(t => t.frage27).HasColumnName("frage27");
            this.Property(t => t.frage28).HasColumnName("frage28");
            this.Property(t => t.frage29).HasColumnName("frage29");
            this.Property(t => t.frage30).HasColumnName("frage30");
            this.Property(t => t.frage31).HasColumnName("frage31");
            this.Property(t => t.frage32).HasColumnName("frage32");
            this.Property(t => t.frage33).HasColumnName("frage33");
            this.Property(t => t.frage34).HasColumnName("frage34");
            this.Property(t => t.frage35).HasColumnName("frage35");
            this.Property(t => t.frage36).HasColumnName("frage36");
            this.Property(t => t.frage37).HasColumnName("frage37");
            this.Property(t => t.frage38).HasColumnName("frage38");
            this.Property(t => t.frage39).HasColumnName("frage39");
            this.Property(t => t.frage40).HasColumnName("frage40");
            this.Property(t => t.frage41).HasColumnName("frage41");
            this.Property(t => t.frage42).HasColumnName("frage42");
            this.Property(t => t.frage43).HasColumnName("frage43");
            this.Property(t => t.frage44).HasColumnName("frage44");
            this.Property(t => t.frage45).HasColumnName("frage45");
            this.Property(t => t.frage46).HasColumnName("frage46");
            this.Property(t => t.frage47).HasColumnName("frage47");
            this.Property(t => t.frage48).HasColumnName("frage48");
            this.Property(t => t.frage49).HasColumnName("frage49");
            this.Property(t => t.frage50).HasColumnName("frage50");
            this.Property(t => t.frage51).HasColumnName("frage51");
            this.Property(t => t.frage52).HasColumnName("frage52");
            this.Property(t => t.bewerter_id).HasColumnName("bewerter_id");
        }
    }
}
