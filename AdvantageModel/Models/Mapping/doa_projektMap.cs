using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_projektMap : EntityTypeConfiguration<doa_projekt>
    {
        public doa_projektMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.beschreibung)
                .IsRequired();

            this.Property(t => t.kostenstelle)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("doa_projekt");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.beschreibung).HasColumnName("beschreibung");
            this.Property(t => t.datum_von).HasColumnName("datum_von");
            this.Property(t => t.datum_bis).HasColumnName("datum_bis");
            this.Property(t => t.kostenstelle).HasColumnName("kostenstelle");
            this.Property(t => t.id_kunde).HasColumnName("id_kunde");
        }
    }
}
