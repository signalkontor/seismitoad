using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_bewertungMap : EntityTypeConfiguration<doa_bewertung>
    {
        public doa_bewertungMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.bewerter_vorname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.bewerter_nachname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.bewerter_rolle)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.formular_name)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("doa_bewertung");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_promoter).HasColumnName("id_promoter");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.id_bewerter).HasColumnName("id_bewerter");
            this.Property(t => t.bewerter_vorname).HasColumnName("bewerter_vorname");
            this.Property(t => t.bewerter_nachname).HasColumnName("bewerter_nachname");
            this.Property(t => t.bewerter_rolle).HasColumnName("bewerter_rolle");
            this.Property(t => t.bewerter_datum).HasColumnName("bewerter_datum");
            this.Property(t => t.engagement).HasColumnName("engagement");
            this.Property(t => t.teamfaehigkeit).HasColumnName("teamfaehigkeit");
            this.Property(t => t.zuverlaessigkeit).HasColumnName("zuverlaessigkeit");
            this.Property(t => t.kundenumgang).HasColumnName("kundenumgang");
            this.Property(t => t.belastbarkeit).HasColumnName("belastbarkeit");
            this.Property(t => t.erscheinung).HasColumnName("erscheinung");
            this.Property(t => t.id_projekt).HasColumnName("id_projekt");
            this.Property(t => t.formular_name).HasColumnName("formular_name");
            this.Property(t => t.formular_id).HasColumnName("formular_id");
            this.Property(t => t.old_data).HasColumnName("old_data");
        }
    }
}
