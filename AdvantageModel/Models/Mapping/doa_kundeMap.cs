using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AdvantageModel.Models.Mapping
{
    public class doa_kundeMap : EntityTypeConfiguration<doa_kunde>
    {
        public doa_kundeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.ds_status });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ds_status)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.plz)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ort)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.ap_vorname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.ap_nachname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.bemerkung)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("doa_kunde");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ds_status).HasColumnName("ds_status");
            this.Property(t => t.changed_time).HasColumnName("changed_time");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.plz).HasColumnName("plz");
            this.Property(t => t.ort).HasColumnName("ort");
            this.Property(t => t.ap_vorname).HasColumnName("ap_vorname");
            this.Property(t => t.ap_nachname).HasColumnName("ap_nachname");
            this.Property(t => t.bemerkung).HasColumnName("bemerkung");
        }
    }
}
