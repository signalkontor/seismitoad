using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;

namespace AdvantageModel.Models
{
    public partial class doa_login
    {
        public const string Login = "Login";

        [ExcludeFromExport]
        [Display(Name = "ID", Description = Login)]
        public int id { get; set; }

        [Display(Name = "Status", Description = Login)]
        [ExcludeFromExport]
        public string ds_status { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Änderungsdatum", Description = Login)]
        public System.DateTime changed_time { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Benutzername", Description = Login)]
        public string login_name { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Passwort", Description = Login)]
        public string password { get; set; }

        [Display(Name = "Kontofreigabe", Description = Login)]
        [UIHint("Bool")]
        public string loginfreigabe { get; set; }

        [Display(Name = "Rolle", Description = Login)]
        public string rolle { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Benutzertyp", Description = Login)]
        public string user_typ { get; set; }

        [Display(Name = "Erstes Einlogdatum", Description = Login)]
        [UIHint("Date")]
        public System.DateTime first_login { get; set; }

        [Display(Name = "Letztes Einlogdatum", Description = Login)]
        [UIHint("Date")]
        public System.DateTime last_login { get; set; }

        [Display(Name = "Fragebogen überspringen", Description = Login)]
        [UIHint("Bool")]
        public int skip_fragebogen { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Promoter ID", Description = Login)]
        public int? employeeId { get; set; }

        public virtual doa_kontaktdaten doa_kontaktdaten { get; set; }
        public virtual doa_profil doa_profil { get; set; }
        public virtual doa_qualifikation doa_qualifikation { get; set; }
    }
}
