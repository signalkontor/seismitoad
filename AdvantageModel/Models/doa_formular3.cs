using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class doa_formular3
    {
        public int id { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public System.DateTime eintragsdatum { get; set; }
        public System.DateTime start_time { get; set; }
        public System.DateTime end_time { get; set; }
        public System.DateTime used_time { get; set; }
        public int bewerter_id { get; set; }
        public string frage1 { get; set; }
        public string frage2 { get; set; }
        public string frage3 { get; set; }
        public string frage4 { get; set; }
        public string frage5 { get; set; }
        public string frage6 { get; set; }
        public string frage7 { get; set; }
        public string frage8 { get; set; }
        public string frage9 { get; set; }
        public string frage10 { get; set; }
        public string frage11 { get; set; }
        public string frage12 { get; set; }
        public string frage13 { get; set; }
        public string frage14 { get; set; }
        public string frage15 { get; set; }
        public string frage16 { get; set; }
        public string frage17 { get; set; }
        public string frage18 { get; set; }
        public string frage19 { get; set; }
        public string dataId { get; set; }
    }
}
