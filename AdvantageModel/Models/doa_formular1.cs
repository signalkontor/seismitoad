using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class doa_formular1
    {
        public int id { get; set; }
        public int dataId { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public System.DateTime eintragsdatum { get; set; }
        public System.TimeSpan start_time { get; set; }
        public System.TimeSpan end_time { get; set; }
        public System.TimeSpan used_time { get; set; }
        public string frage1 { get; set; }
        public string frage2 { get; set; }
        public string frage3 { get; set; }
        public string frage4 { get; set; }
        public string frage5 { get; set; }
        public string frage6 { get; set; }
        public string frage7 { get; set; }
        public string frage8 { get; set; }
        public string frage9 { get; set; }
        public string frage10 { get; set; }
        public string frage11 { get; set; }
        public string frage12 { get; set; }
        public string frage13 { get; set; }
        public string frage14 { get; set; }
        public string frage15 { get; set; }
        public string frage16 { get; set; }
        public string frage17 { get; set; }
        public string frage18 { get; set; }
        public string frage19 { get; set; }
        public string frage20 { get; set; }
        public string frage21 { get; set; }
        public string frage22 { get; set; }
        public string frage23 { get; set; }
        public string frage24 { get; set; }
        public string frage25 { get; set; }
        public string frage26 { get; set; }
        public string frage27 { get; set; }
        public string frage28 { get; set; }
        public string frage29 { get; set; }
        public string frage30 { get; set; }
        public string frage31 { get; set; }
        public string frage32 { get; set; }
        public string frage33 { get; set; }
        public string frage34 { get; set; }
        public string frage35 { get; set; }
        public string frage36 { get; set; }
        public string frage37 { get; set; }
        public string frage38 { get; set; }
        public string frage39 { get; set; }
        public string frage40 { get; set; }
        public string frage41 { get; set; }
        public string frage42 { get; set; }
        public string frage43 { get; set; }
        public string frage44 { get; set; }
        public string frage45 { get; set; }
        public string frage46 { get; set; }
        public string frage47 { get; set; }
        public string frage48 { get; set; }
        public string frage49 { get; set; }
        public string frage50 { get; set; }
        public string frage51 { get; set; }
        public string frage52 { get; set; }
        public int bewerter_id { get; set; }
    }
}
