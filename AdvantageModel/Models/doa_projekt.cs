using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class doa_projekt
    {
        public int id { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public string name { get; set; }
        public string beschreibung { get; set; }
        public System.DateTime datum_von { get; set; }
        public System.DateTime datum_bis { get; set; }
        public string kostenstelle { get; set; }
        public int id_kunde { get; set; }
    }
}
