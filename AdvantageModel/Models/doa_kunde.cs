using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class doa_kunde
    {
        public int id { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public string name { get; set; }
        public string plz { get; set; }
        public string ort { get; set; }
        public string ap_vorname { get; set; }
        public string ap_nachname { get; set; }
        public string bemerkung { get; set; }
    }
}
