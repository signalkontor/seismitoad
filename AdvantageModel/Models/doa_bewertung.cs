using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class doa_bewertung
    {
        public int id { get; set; }
        public int id_promoter { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public int id_bewerter { get; set; }
        public string bewerter_vorname { get; set; }
        public string bewerter_nachname { get; set; }
        public string bewerter_rolle { get; set; }
        public System.DateTime bewerter_datum { get; set; }
        public decimal engagement { get; set; }
        public decimal teamfaehigkeit { get; set; }
        public decimal zuverlaessigkeit { get; set; }
        public decimal kundenumgang { get; set; }
        public decimal belastbarkeit { get; set; }
        public decimal erscheinung { get; set; }
        public int id_projekt { get; set; }
        public string formular_name { get; set; }
        public int formular_id { get; set; }
        public int old_data { get; set; }
    }
}
