using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;

namespace AdvantageModel.Models
{
    public partial class doa_profil
    {
        public const string Profile = "Profil";

        [ExcludeFromExport]
        [Display(Name = "ID", Description = Profile)]
        public int id { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Status", Description = Profile)]
        public string ds_status { get; set; }

        [Display(Name = "Änderungsdatum", Description = Profile)]
        [ExcludeFromExport]
        public System.DateTime changed_time { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Körpergröße(cm)", Description = Profile)]
        public string groesse { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Konfektionsgröße", Description = Profile)]
        public string konf_groesse { get; set; }

        [Display(Name = "Gewicht", Description = Profile)]
        public string gewicht { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Bundweite", Description = Profile)]
        public string jeansgroesse_w { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Beinlänge", Description = Profile)]
        public string jeansgroesse_l { get; set; }

        [Display(Name = "Schuhgröße", Description = Profile)]
        public string schuhgroesse { get; set; } // MIGRATION Keine Übernahme
        /*
         1 Blond
         2 Mittelblond
         3 Dunkelblond
         4 Braun
         5 Dunkelbraun
         6 Rot
         7 Schwarz
         8 Andere
        */

        [ExcludeFromExport]
        [Display(Name = "Haarfarbe", Description = Profile)]
        [UIHint("HairColor")]
        public string haarfarbe { get; set; }

        [ExcludeFromExport]
        [Display(Name = "T-Shirt-Größe", Description = Profile)]
        public string tshirt_groesse { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Tattoos oder Piercings?", Description = Profile)]
        [UIHint("Bool")]
        public string tattoo_piercing { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Engagement", Description = Profile)]
        public decimal engagement_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Teamfähigkeit", Description = Profile)]
        public decimal teamfaehigkeit_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Zuverlässigkeit", Description = Profile)]
        public decimal zuverlaessigkeit_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Kundendialog", Description = Profile)]
        public decimal kundenumgang_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Belastbarkeit", Description = Profile)]
        public decimal belastbarkeit_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [Display(Name = "Bewertung(Durchschnitt) Erscheinung", Description = Profile)]
        public decimal erscheinung_durchschnitt { get; set; } // MIGRATION Keine Übernahme

        [ExcludeFromExport]
        [Display(Name = "Sonstige Bewertung", Description = Profile)]
        public string bemerkung { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Bild 1", Description = Profile)]
        public string name_bild1 { get; set; }

        [ExcludeFromExport]
        [Display(Name = "Bild 2", Description = Profile)]
        public string name_bild2 { get; set; }

        public virtual doa_login doa_login { get; set; }
    }
}
