using System;
using System.Collections.Generic;

namespace AdvantageModel.Models
{
    public partial class sr_projekte2promoter
    {
        public int id { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }
        public int id_added_doa { get; set; }
        public int gebucht { get; set; }
    }
}
