﻿/*
 * Dieses Skript stellt folgende Funktionen bereit.
 * Vorrausetzungen: jQuery, jQueryUI, Eric Hynds Multiselect + Multiselect-Filter
 *
 * Disable-Ajax-Caching:
 *   Wird dieses Skript in eine Seite eingebunden wird das Caching von Ajax-Requests
 *   deaktiviert. Dies möchte man in der Regel, da der Internet Explorer zu aggresiv
 *   cacht.
 * 
 * Submitting-Checkboxes:
 *   Checkboxen mit der Klasse "submitting-checkbox" schicken automatisch 
 *   das Formular zu dem sie gehören ab. Dies erfolgt zur Zeit immer per POST.
 *
 * Multiselect:
 *   Macht aus allen <select multiple="multiple" /> Elementen schöne Multiselects.
 *   Hat das Element die CSS-Klasse "filter" ist das Multiselect filterbar.
 *   Ist keine der Optionen ausgewählt zeigt das Multiselect standardmäßig
 *   "Bitte wählen..." an. Dieser Text kann geändert werden indem man dem Element
 *   ein "label"-Attribute gibt und darüber den Text setzt.
 *   Beispiel: <select multiple="multiple" label="Noch kein Wert gewählt" />
 *
 * Confirm:
 *   Elemente die ein "confirm" Attribut haben fragen bei Kicks nach einer Bestätigung.
 *   Als Meldung wird dabei der Wert des "confirm" Attributes genommen. Gibt der User
 *   die negative Antwort wird "false" zurückgegeben. So kann man z.B. bei einem Submit-
 *   Button das senden des Formulars verhindern.
 *
 * Toggle:
 *   Elemente die ein "toggle" Attribut haben blenden nach dem Anklicken das - über den
 *   Selector im "toggle" Attribut spezifitierte - Element ein bzw. aus.
 *
 *  Close-Window-On-Escape:
 *    Telerik-Windows werden bei drücken von Escape geschlossen. Diese Funktion funktioniert
 *    nur, wenn das jQuery Hotkeys Plugin eingebunden wurde.
 *
 * String.startsWith:
 *   Erweitert den String-Datentyp um die Methode startsWith().
 *
 * String.contains:
 *   Erweitert den String-Datentyp um die Methode contains().
 */


/*
 * Wir packen einige Sachen in diese Funktion, damit diese Funktion auch in Partials
 * aufgerufen werden kann, wenn z.B. ein Form Submit ein Partial zurückliefert in dem wiederum 
 * Elemente mit jQuery behandelt werden müssen. Eine Form #myForm würde also loadLive('#myForm')
 * im Result eines Ajax Requests aufrufen.
 */
// TODO diese Methode in eine Klasse packen
function loadLive(context) {
    $("input[type=checkbox].submitting-checkbox", context).click(function () {
        var form = $(this).closest("form");
        // TODO Anhand "method" der Form get oder post verwenden
        $.post(form.attr("action"), form.serialize());
    });

    $('select[multiple=multiple]', context).each(function () {
        var elem = $(this);
        var label = elem.attr('label');
        var filter = elem.hasClass("filter");
        var multiselect = elem.multiselect({
            multiple: true,
            header: true,
            noneSelectedText: label ? label : "Bitte wählen...",
            selectedText: "# von # ausgewählt",
            selectedList: 1,
            checkAllText: "Alle",
            uncheckAllText: "Keins",
            minWidth: "225",
            classes: elem.attr('class')
        });
        if (filter) {
            multiselect.multiselectfilter();
        }
    });

    $("[confirm]").click(function (event) {
        if (!confirm($(this).attr("confirm"))) {
            event.preventDefault();
            return false;
        }
        return true;
    });

    $("[toggle]", context).click(function (e) {
        e.preventDefault();
        $($(this).attr("toggle")).toggle('slow');
    });

    $(document).bind("keydown", "esc", function () {
        try {
            $(".t-window").each(function() {
                $(this).data("tWindow").close();
            });
        } catch (e) {
            // ignore
        }
    });
}

$(document).ready(function () {
    $.ajaxSetup({ cache: false });
    loadLive();
});

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.contains != 'function') {
    String.prototype.contains = function (str) {
        return this.indexOf(str) >= 0;
    };
}