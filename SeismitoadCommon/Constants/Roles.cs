﻿namespace SeismitoadCommon.Constants
{
    public static class Roles
    {
        /// <summary>
        /// Marker für Accounts von PER MEDIA Mitarbeitern.
        /// </summary>
        public const string Permedia = "permedia";

        /// <summary>
        /// Hat Zugriff auf den Admin-Bereich (Per Media, signalkontor)
        /// </summary>
        public const string Admin = "administrator";

        /// <summary>
        /// Kunden-Login
        /// </summary>
        public const string Customer = "customer";

        /// <summary>
        /// Darf Daten seiner Sales-Region sehen, wobei die Sales-Region seinem Benutzernamen entspricht.
        /// </summary>
        public const string RegionalManager = "regional-manager";

        /// <summary>
        /// Darf den Download-Bereich sehen
        /// </summary>
        public const string DownloadsViewer = "downloads-viewer";

        /// <summary>
        /// Darf den Download-Bereich bearbeiten
        /// </summary>
        public const string DownloadsEditor = "downloads-editor";
        
        /// <summary>
        /// Darf Provisionstage buchen
        /// </summary>
        public const string SelfbookingPromoter = "self-booking-promoter";

        /// <summary>
        /// Darf Reports eingeben (Promotoren)
        /// </summary>
        public const string Reporter = "reporter";

        /// <summary>
        /// Darf Reports (nachträglich) bearbeiten (Per Media)
        /// </summary>
        public const string ReportEditor = "report-editor";

        /// <summary>
        /// Darf die News lesen (Promotoren, Per Media)
        /// </summary>
        public const string NewsReader = "news-reader";

        /// <summary>
        /// Darf die News und Downloads bearbeiten (Per Media)
        /// </summary>
        public const string NewsEditor = "news-editor";

        /// <summary>
        /// Darf in MasterInstance die Locations bearbeiten
        /// </summary>
        public const string LocationEditor = "location-editor";

        /// <summary>
        /// Darf den Analyse-Bereich sehen (Per Media, Kunde)
        /// </summary>
        public const string Analyst = "analyst";

        /// <summary>
        /// Promoter die zusätzlich diese Rolle haben sehen neue Features schon eher.
        /// Wurde eingeführt um die PromoterInstance mit einigen Friendly-Usern Testen zu
        /// können, bevor alle 10.000 User auf die neue Anwendung geschickt werden.
        /// </summary>
        public const string BetaTester = "beta-tester";

        /// <summary>
        /// Zugriff auf ELMAH und sonstige Bereiche die nur für uns bestimmt sind.
        /// </summary>
        public const string Signalkontor = "signalkontor";
    }
}
