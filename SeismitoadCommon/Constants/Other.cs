﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace SeismitoadCommon.Constants
{
    public static class Other
    {
        public static readonly CultureInfo GermanCultureInfo = CultureInfo.GetCultureInfo("de-DE");
        public const string DayOfWeekFormat = "{0:ddd}";
        public const string TimeFormat = "{0:HH:mm}";
        public const string DateFormat = "{0:dd.MM.yyyy}";
        public const string DateTimeFormat = "{0:dd.MM.yyyy HH:mm}";
        public const string FullDateTimeFormat = "{0:ddd dd.MM.yyyy HH:mm}";
        public static readonly Regex LargestCampaignAreaRegex = new Regex("(Location\\.LargestCampaignArea|LargestCampaignArea)~(eq|ge|gt|le|lt|ne)~([-+]?\\b\\d+\\b)");
        
        /// Regulärerausdruck für Telefonnummern. Geprüft wird folgendes:
        /// 1. Telefonnummer beginnt mit einem Pluszeichen.
        /// 2. Es folgt eine 1 bis 3 stellige Ländervorwahl.
        /// 3. Nach der Ländervorwahl folgt ein Leerzeichen.
        /// 4. Danach folgt eine 2 bis 5 stellige (Orts/Mobil)-Vorwahl.
        /// 5. Es folgt ein Minuszeichen.
        /// 6. Es folgt die eigentliche Rufnummer (mindestens 3 stellig)
        /// 7. Optional folgt ein Minuszeichen mit einer 1-4 stelligen Durchwahlnummer
        /// 
        /// Bis alle Daten korrekt gepflegt sind, ist auch die angabe n.a. erlaubt.
        /// 
        /// Beispiele:
        /// +49 179-1234567
        /// +49 40-3197478-0
        /// +1 650-253-0001
        public const string PhoneNumberRegex = @"^([+][0-9]{1,3}) ([0-9]{2,5})-([0-9]{3,})(-[0-9]{1,4})?";
        public const string PhoneNumberExample = "Die Nummer muss das Format +49 321-12345-001 haben.";
    }
}
