﻿namespace SeismitoadCommon.Constants
{
    public static class Charts
    {
        public const string IntegerFormat = "{0:#,0}";
        public const string PiecesFormat = "{0:#,0} Stück";
        public const string PiecesFormatWithTwoDecimals = "{0:#,0.00} Stück";
        public const string PiecesFractionFormat = "{0:#,0.00} Stück";
        public const string EuroFormat = "{0:#,0.00} €";
        public const string EuroFormatNoDecimals = "{0:#,0} €";
        public const string PercentageFormat = "{0:0.00}%";
        public const string PercentageFormat2 = "{0:0.00%;; }";
        public const string PercentageFormat3 = "{0:0.00%}";
    }
}
