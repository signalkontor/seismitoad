﻿using System;
using System.Collections.Generic;

namespace SeismitoadCommon.Extensions
{
    public static class MiscExtensions
    {
        public static int RoundOffToNext(this double i, int to)
        {
            return ((int)Math.Round(i / to)) * to;
        }

        public static int RoundOffToNext(this int i, int to)
        {
            return RoundOffToNext((double)i, to);
        }

        public static int RoundOffToNext(this decimal i, int to)
        {
            return RoundOffToNext((double)i, to);
        }

        public static string SafeJoin(this IEnumerable<object> values, string seperator)
        {
            return values == null ? null : ";" + String.Join(seperator, values) + ";";
        }

        public static string[] SafeSplit(this string values, char seperator)
        {
            return values == null ? null : values.Split(new[] { seperator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static TResult ValueOrDefault<T, TResult>(this T arg, Func<T, TResult> expression, TResult defaultValue)
        {
            try
            {
                return expression(arg);
            }
            catch
            {
                return defaultValue;
            }            
        }

        public static DateTime FromUnixTimeStampToDateTime(this long unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0)
                .AddSeconds(unixTimeStamp)
                .ToLocalTime();
        }

        public static bool? AsNullableBool(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return null;
            return "1" == value;
        }

        public static int? AsNullableInt(this string value)
        {
            int result;
            return Int32.TryParse(value, out result) ? result : (int?) null;
        }
    }
}
