﻿using System;

namespace SeismitoadCommon
{
    public static class LocalDateTime
    {
        private static readonly TimeZoneInfo TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
        public static DateTime Now
        {
            get { return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo); }
        }
    }
}
