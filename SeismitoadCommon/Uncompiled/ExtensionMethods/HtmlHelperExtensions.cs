﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SeismitoadCommon.ExtensionMethods
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString YesNoImage(this HtmlHelper helper, bool value, IDictionary<string, object> htmlAttributes = null)
        {
            var tagBuilder = new TagBuilder("img");
            if (htmlAttributes != null)
            {
                tagBuilder.MergeAttributes(htmlAttributes, true);
            }
            tagBuilder.MergeAttribute("alt", value ? "Ja" : "Nein");
            tagBuilder.MergeAttribute("src", value ? "/Content/images/yes.png" : "/Content/images/no.png");
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString DeleteLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, object routeValues, string confirmMessage)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.Action(actionName, controllerName, routeValues);

            var aTag = new TagBuilder("a");
            aTag.MergeAttribute("href", url);
            aTag.MergeAttribute("rel", confirmMessage);
            aTag.MergeAttribute("class", "ajax-delete");
            aTag.SetInnerText(linkText);
            return MvcHtmlString.Create(aTag.ToString());
        }

        public static RouteValueDictionary ToRouteValueDictionary(this NameValueCollection collection)
        {
            var rvd = new RouteValueDictionary();
            foreach (var key in collection.AllKeys)
            {
                var values = collection.GetValues(key);
                if (values == null) continue;
                for (var i = 0; i < values.Length; i++)
                {
                    var value = values[i];
                    if(string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(value)) continue;
                    rvd.Add(values.Length > 1 ? key + "[" + i + "]" : key, value);
                }
            }
            return rvd;
            //return new RouteValueDictionary(collection.AllKeys.ToDictionary(e => e, v => (object)collection[v]));
        }

        public static string AddQueryString(this string url, NameValueCollection queryString)
        {
            if (!url.Contains("?"))
            {
                if(queryString.Count > 0)
                    url += "?";
            }
            else if(!url.EndsWith("&"))
            {
                url += "&";
            }

            foreach (var key in queryString.AllKeys)
            {
                var strings = queryString.GetValues(key);
                if (strings == null) continue;
                if (strings.Length > 1)
                {
                    // Wenn für einen Parameter mehrere Werte übergeben werden (z.B. name=1&name=2)
                    // dann generiert Telerik-MVC daraus falsche URLs (name=1,2). Da der ModelBinder
                    // aber auch die Syntax name[0]=1&name[1]=2 versteht verwenden wir einfach diese.
                    for (var index = 0; index < strings.Length; index++)
                    {
                        url += string.Format("{0}[{1}]={2}&", key, index, strings[index]);
                    }
                }
                else
                {
                    url += string.Format("{0}={1}&", key, strings[0]);
                }
            }

            if (queryString.Count > 0)
                url = url.Substring(0, url.Length - 1);

            return url;
        }

        public static string ToQueryString(this NameValueCollection nameValueCollection)
        {
            var parts = new Collection<string>();
            foreach (string name in nameValueCollection)
            {
                var values = nameValueCollection.GetValues(name);
                if (values == null) continue;
                foreach(var value in values)
                {
                    parts.Add(string.Format("{0}={1}", name, HttpUtility.UrlEncode(value)));
                }
            }
            return String.Join("&", parts);
        }

        public static MvcHtmlString KendoTemplateFor<T, TResult>(this HtmlHelper<T> html, Expression<Func<T, TResult>> expression)
        {
            expression.Compile();
            var prefix = html.ViewData["KendoExpressionPrefix"] as string ?? "";
            var body = expression.Body.ToString();
            body = body.Substring(body.IndexOf('.'));

            return new MvcHtmlString(string.Format("#: {0}{1} #", prefix, body));
        }
    }
}
