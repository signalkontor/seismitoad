﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SeismitoadCommon.ActionResults
{
    public class PdfResult : FilePathResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PdfResult"/> class.
        /// </summary>
        /// <param name="filedownloadname">The file name the user
        /// will download.</param>
        /// <param name="url">The url to render.</param>
        /// <param name="wkhtmlexepath">The path to the wkhtmltopdf.exe used to create the PDF</param>
        /// <param name="filepath"></param>
        public PdfResult(string filedownloadname, string url, string wkhtmlexepath, string filepath = null)
            : base(filepath ?? Path.GetTempFileName(), "application/pdf")
        {
            FileDownloadName = filedownloadname;
            Url = url;
            WkHtmlExePath = wkhtmlexepath;
        }

        /// <summary>
        /// Enables processing of the result of an action method by a 
        /// custom type that inherits from the 
        /// <see cref="T:System.Web.Mvc.ActionResult"/> class.
        /// Creates a PDF file and passes it out of the response object.
        /// </summary>
        /// <param name="context">The context in which the result is executed. 
        /// The context information includes the controller,
        /// HTTP content, request context, and route data.</param>
        public override void ExecuteResult(ControllerContext context)
        {
            var arguments = string.Format("--print-media-type \"{0}\" \"{1}\"", Url, FileName);

            var pi = new ProcessStartInfo(WkHtmlExePath, arguments);
            var p = Process.Start(pi);
            p.WaitForExit(5 * 60 * 1000);

            base.ExecuteResult(context);
        }

        /// <summary>
        /// Gets or sets the url used as content for the PDF.
        /// </summary>
        /// <value>The Url.</value>
        public string Url { get; set; }

        public string WkHtmlExePath { get; set; }
    }
}
