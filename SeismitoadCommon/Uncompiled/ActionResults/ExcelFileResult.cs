using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;

namespace SeismitoadCommon.ActionResults
{
    public class Excel2007FileResult : FileResult
    {
        private readonly string _filename;
        private readonly ExcelPackage _package;

        public Excel2007FileResult(string filename, ExcelPackage package)
            : base("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        {
            _filename = filename;
            _package = package;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", _filename));
            _package.SaveAs(response.OutputStream);
            response.OutputStream.Flush();
            response.OutputStream.Close();
            response.OutputStream.Dispose();
            _package.Dispose();
            //response.BinaryWrite(_package.GetAsByteArray());
        }
    }
}