﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadCommon.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SkRequiredIfAttribute : ValidationAttribute, IMetadataAware
    {
        /// <summary>
        /// Die Eigenschaft ist nur dann erforderlich, wenn der Wert eines anderen Properties einem gebeben Wert entspricht.
        /// Optional kann die Abhängigkeit von dem anderen Wert auch nur für bestimmte Rollen gelten. Dazu kann diesem Attribute
        /// wie dem <see cref="SkRequiredAttribute"/> eine Liste von Rollen übergeben werden.
        /// </summary>
        /// <param name="otherPropertyName">Name der anderen Eigenschaft.</param>
        /// <param name="otherPropertyValue">Wert den die andere Eigenschaft haben muss damit diese Eigenschaft ein Pflichtfeld ist.</param>
        public SkRequiredIfAttribute(string otherPropertyName, object otherPropertyValue)
        {
            _otherPropertyName = otherPropertyName;
            _otherPropertyValue = otherPropertyValue;
        }

        public string Roles { get; set; }

        private readonly string _otherPropertyName;
        private readonly object _otherPropertyValue;

        public override bool RequiresValidationContext { get { return true; } }

        public bool IsRequired
        {
            get
            {
                // Wenn keine Rollen angebeben wurden ist es ein Pflichtfeld
                if (string.IsNullOrWhiteSpace(Roles))
                    return true;

                // Aufruf findet ohne HttpContext statt, z.B. aus einer Konsolenanwendung oder
                // einem neu erzeugten Thread. In diesem Fall macht es keinen Sinn anhand der 
                // Benutzerrollen zu prüfen ob ein Wert erforderlich ist.
                if (HttpContext.Current == null)
                    return false;

                // Wenn der angemeldete Benutzer eine der angebeben Rollen hat, ist es ein Pflichtfeld
                return Roles.Split(',').Select(s => s.Trim()).Any(HttpContext.Current.User.IsInRole);
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // Zuerst prüfen wir, ob dieses Feld grundsätzlich ein Pflichtfeld für den angemeldeten
            // Benutzer ist.
            if (!IsRequired)
                return ValidationResult.Success;

            // Das Feld ist für diesen User ein Pflichtfeld. Allerdings nur dann, wenn der Wert
            // des Properties _otherPropertyName den Wert _otherPropertyValue hat. Dies prüfen wir
            // in den folgenden Zeilen.

            var otherProperty = validationContext.ObjectType.GetProperty(_otherPropertyName);
            if(otherProperty == null) 
                throw new InvalidOperationException(string.Format("Type {0} has no property named {1}", validationContext.ObjectType.Name, _otherPropertyName));

            if (!Equals(otherProperty.GetValue(validationContext.ObjectInstance), _otherPropertyValue))
            {
                // Der Wert der anderen Eigenschaft entspricht nicht dem Wert der diese Eigenschaft zum Pflichtfeld macht.
                // Daher brauchen wir den Wert auch nicht weiter prüfen.
                return ValidationResult.Success;
            }

            // Aufgrund des Werts der anderen Eigenschaft ist diese Eigenschaft erforderlich. Also prüfen wir
            // nun, ob ein Wert angegeben wurde.
            return value == null
                       ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName))
                       : ValidationResult.Success;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (metadata == null)
                throw new ArgumentNullException("metadata");

            // Wir können hier (leider) nicht metadata.IsRequired = true setzen, weil sonst ASP.NET MVC
            // automatisch das normale RequiredAttribute hinzufügt. Das wollen wir nicht, da dieses
            // Attribut erst nach unserem ausgeführt wird und damit das Ergbnis unserer IsValid()-Methode
            // überschrieben wird. Stattdessen setzen wir in AdditionalValues ein Flag. Anhand dieses
            // Flags entscheidet das ObjectTemplate, ob hinter dem Label das "Pflichtfeld-Sternchen"
            // angezeigt wird.
            if(IsRequired)
                metadata.AdditionalValues.Add("SkIsRequired", true);
        }
    }
}
