﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadCommon.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SkRequiredAttribute : Attribute, IMetadataAware
    {
        public string Roles { get; set; }
        public string Users { get; set; }
        public bool Invert { get; set; }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (metadata == null)
                throw new ArgumentNullException("metadata");

            // Es reicht IsRequired zu setzen, um eine Required-Validierung zu forcieren.
            // Der von zu ASP.NET MVC gehörende DataAnnotationsModelValidatorProvider fügt
            // nämlich automatisch ein RequiredAttribute hinzu, wenn IsRequired gesetzt ist.

            if (!string.IsNullOrWhiteSpace(Roles))
                metadata.IsRequired = Roles.Split(',').Select(s => s.Trim()).Any(HttpContext.Current.User.IsInRole);
            else if(!string.IsNullOrWhiteSpace(Users))
                metadata.IsRequired = Users.Split(',').Select(s => s.Trim()).Any(e => e == HttpContext.Current.User.Identity.Name);
            else
                metadata.IsRequired = true;

            if (Invert)
                metadata.IsRequired = !metadata.IsRequired;
        }
    }
}
