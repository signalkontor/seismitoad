﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadCommon.ModelBinders
{
    public class DefaultModelBinderWithHtmlValidation : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                return base.BindModel(controllerContext, bindingContext);
            }
            catch (HttpRequestValidationException)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("Das Feld '{0}' enthält ungültige Zeichen.", bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelName));
                bindingContext.ModelState.AddModelError("InvalidCharactersInInput", "Die Zeichen < und > sind als Eingabe nicht erlaubt.");
            }

            //Cast the value provider to an IUnvalidatedValueProvider, which allows to skip validation
            IUnvalidatedValueProvider provider = bindingContext.ValueProvider as IUnvalidatedValueProvider;
            if (provider == null) return null;

            //Get the attempted value, skiping the validation
            var result = provider.GetValue(bindingContext.ModelName, skipValidation: true);

            return result?.AttemptedValue;
        }
    }
}
