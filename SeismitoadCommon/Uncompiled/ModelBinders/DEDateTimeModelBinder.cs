﻿using System;
using System.Web.Mvc;

namespace SeismitoadCommon.ModelBinders
{
    public class DEDateTimeModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult res = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            var date = res != null ? res.AttemptedValue : null;

            if (String.IsNullOrEmpty(date))
                return null;

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, bindingContext.ValueProvider.GetValue(bindingContext.ModelName));

            try
            {
                //return res.ConvertTo(typeof(DateTime));
                return DateTime.Parse(date, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
            }
            catch (Exception)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, String.Format("\"{0}\" is invalid.", bindingContext.ModelName));
                return null;
            }
        }
    }
}
