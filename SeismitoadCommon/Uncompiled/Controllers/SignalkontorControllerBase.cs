using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;

namespace SeismitoadCommon.Controllers
{
    public abstract class SignalkontorControllerBase : Controller
    {
        public static readonly string ClrVersion;
        public static readonly string DotNetVersion;
        public static readonly string EfVersion;
        public static readonly string WebAppVersion;

        static SignalkontorControllerBase()
        {
            ClrVersion = Environment.Version.ToString();
            DotNetVersion = Assembly.GetAssembly(typeof(Object)).GetName().Version.ToString();
            EfVersion = Assembly.GetAssembly(typeof(DbContext)).GetName().Version.ToString();
            WebAppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}
