﻿using System.Collections.Specialized;
using System.Web.Mvc;
using System.Security.Principal;
using SeismitoadCommon.ExtensionMethods;
using SeismitoadCommon.Filters;

namespace SeismitoadCommon.Controllers
{
    public abstract partial class SignOnController : Controller
    {
        //
        // GET: /SignOn/To/{some url}
        public virtual ActionResult To(string url)
        {
            return To(User, url);
        }

        [NonAction]
        public static ActionResult To(IPrincipal user, string url)
        {
            var additionalParams = new NameValueCollection
                {
                    {"SSO", SingleSignOnFilter.GetSSOParameter(user.Identity.Name)}
                };
            return new RedirectResult(url.AddQueryString(additionalParams));
        }
    }
}
