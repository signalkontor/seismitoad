﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace SeismitoadCommon.Filters
{
    public class SingleSignOnFilter : IAuthorizationFilter
    {
        private const string Secret = "sDghd#%1sjnLKlka..67g";

        public static string GetSSOParameter(string username)
        {
            var nonce = LocalDateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture);
            var token = SecurityToken.SecurityToken.Generate(Secret, username, nonce);

            var stringBuilder = new StringBuilder();
            stringBuilder.Append(username);
            stringBuilder.Append(";");
            stringBuilder.Append(nonce);
            stringBuilder.Append(";");
            stringBuilder.Append(token);
            return stringBuilder.ToString();
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;

            // Der zugriff auf httpContext.Request.Form[] oder httpContext.Request.Params[] kann eine
            // Exception werfen, wenn verbotene Zeichen im Request gefunden wurden (z.B. HTML-Tags oder
            // JavaScript-Code). Da es uns an dieser Stelle nur um das Thema SSO geht können wir
            // uns mit System.Web.Helpers.Validation.Unvalidated() eine Kopie des Requests holen welche
            // keine Exception in solchen Fällen wirft.

            var ssoParameter = httpContext.Request.Unvalidated.QueryString.Get("SSO");
            // No SSO parameter
            if (string.IsNullOrWhiteSpace(ssoParameter))
                return;

            var ssoParts = ssoParameter.Split(';');
            // SSO Header has invalid format
            if (ssoParts.Length != 3)
                return;

            var ticks = Convert.ToInt64(ssoParts[1]);
            var dateTime = new DateTime(ticks);
            var token = SecurityToken.SecurityToken.Generate(Secret, ssoParts[0], ssoParts[1]);
            if (token == ssoParts[2] && Math.Abs(dateTime.Subtract(LocalDateTime.Now).TotalMinutes) < 5)
            {
                // Authenticate user
                var cookie = FormsAuthentication.GetAuthCookie(ssoParts[0], false);
                httpContext.Response.Cookies.Add(cookie);
            }

            // Redirect User to remove SSO parameter from URL
            var index = httpContext.Request.Url.AbsoluteUri.IndexOf("?SSO=");
            // Das hier funktioniert nur, wenn der SSO Parameter immer der letzte Parameter ist!
            var urlWithoutSSOParameter = httpContext.Request.Url.AbsoluteUri.Substring(0, index);
            filterContext.Result = new RedirectResult(urlWithoutSSOParameter);
        }
    }
}
