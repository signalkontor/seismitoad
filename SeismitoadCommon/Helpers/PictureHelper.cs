﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace SeismitoadCommon.Helpers
{
    public static class PictureHelper
    {
        public static string ConvertExePath { get; set; }
        public static string RotatePicture(string pathToPicture, int degrees = 90)
        {
            var commandLine = string.Format("\"{0}\" -rotate {1} \"{2}\"", pathToPicture, degrees, pathToPicture);
            var processInfo = new ProcessStartInfo
                {
                    FileName = ConvertExePath,                  
                    Arguments = commandLine,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    UseShellExecute = false
                };

            var process = new Process {StartInfo = processInfo};
            process.Start();

            process.WaitForExit(1000);

            return process.ExitCode == 0 ? process.StandardOutput.ReadToEnd() : process.StandardError.ReadToEnd();

        }
    }
}
