﻿using System;

namespace SeismitoadCommon.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ExcludeFromExportAttribute : Attribute
    {
        public string AllowedRoles { get; set; }

        public string[] AllowedRolesArray { get { return !string.IsNullOrWhiteSpace(AllowedRoles) ? AllowedRoles.Split(',') : new string[0]; } }

        public ExcludeFromExportAttribute(params string[] allowedRoles)
        {
            if (allowedRoles != null)
            {
                AllowedRoles = string.Join(",", allowedRoles);
            }
        }
    }

}