﻿using System.ComponentModel.DataAnnotations;

namespace SeismitoadCommon.Models
{
    public enum Attendance
    {
        [Display(Name = "Unbekannt")]
        Unknown = 0,
        [Display(Name = "Pünktlich")]
        InTime = 1,
        [Display(Name = "Verspätet")]
        Late = 2,
        [Display(Name = "Nicht erschienen")]
        DidNotAttend = 3
    }
}
