﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadCommon.Models
{
    public class MissingReportModel
    {
        public string Url { get; set; }
        [Display(Name = "Aktion")]
        public string Campaign { get; set; }
        [Display(Name = "Datum")]
        public DateTime Date { get; set; }
        [Display(Name = "Einsatzort")]
        public string LocationName { get; set; }
        [Display(Name = "Straße")]
        public string Street { get; set; }
        [Display(Name = "PLZ")]
        public string PostalCode { get; set; }
        [Display(Name = "Ort")]
        public string City { get; set; }
    }
}