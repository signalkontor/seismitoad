using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class Contact : IHasId
    {
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Display(Name = "Anrede", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public string Title { get; set; }

        [Display(Name = "Vorname", Description = "Basisdaten")]
        public string Firstname { get; set; }
        
        [Display(Name = "Name", Description = "Basisdaten")]
        public string Lastname { get; set; }
        
        [Display(Name = "Telefon", Description = "Basisdaten")]
        [DataType(DataType.PhoneNumber)]
        [UIHint("PhoneNumber")]
        public string Phone { get; set; }

        [Display(Name = "Fax", Description = "Basisdaten")]
        [DataType(DataType.PhoneNumber)]
        [UIHint("PhoneNumber")]
        public string Fax { get; set; }

        [EmailAddress]
        [Display(Name = "E-Mail", Description = "Basisdaten")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Funktion", Description = "Basisdaten")]
        public string Type { get; set; }

        [Display(Name = "Bemerkung", Description = "Basisdaten")]
        public string Comment { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        [Column("Location_Id")]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ICollection<CampaignLocation> CampaignLocations { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} ({2})", Firstname, Lastname, Type);
        }
    }
}