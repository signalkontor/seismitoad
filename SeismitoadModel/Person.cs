﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectTemplate.Attributes;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public abstract class Person
    {
        protected const string Basisdaten = "Basisdaten";

        [Required]
        [Display(Name = "Anrede", Description = Basisdaten)]
        [UIHint("SingleSelect")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Vorname", Description = Basisdaten)]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Name", Description = Basisdaten)]
        public string Lastname { get; set; }

        [Required]
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public Guid ProviderUserKey { get; set; }
    }
}
