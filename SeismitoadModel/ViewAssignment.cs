﻿using System;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class ViewAssignment
    {
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int EmployeeCount { get; set; }
        public int AssignedEmployeeCount { get; set; }
        public AssignmentState State { get; set; }
        public Attendance? Attendance { get; set; }
        public int LocationId { get; set; }
        public int CampaignId { get; set; }
        public DateTime? PredecessorDateStart { get; set; }
        public DateTime? SuccessorDateStart { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeFirstname { get; set; }
        public string EmployeeLastname { get; set; }
        public string RoleShortName{ get; set; }
        public string Color { get; set; }
        public string Notes { get; set; }
        public int Marker { get; set; }
    }
}
