﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace SeismitoadModel
{
    public abstract class Alert : ITrackChanges
    {
        public int Id { get; set; }
        public AlertPriority Priority { get; set; }
        public string Text { get; set; }
        public virtual User UserInCharge { get; set; }
        public virtual ICollection<AlertComment> Comments { get; set; }
    }

    public class MissingReportAlert : Alert
    {
        public MissingReportAlert()
        {
            Priority = AlertPriority.Medium;
        }
        public virtual AssignedEmployee AssignedEmployee { get; set; }
    }

    // Neue Personalanforderungen werden angezeigt und als solche gekennzeichnet
    public class StaffRequirementsChangedAlert : Alert
    {
        public StaffRequirementsChangedAlert()
        {
            Priority = AlertPriority.Medium;
        }
        public virtual Campaign Campaign { get; set; }
    }

    // Ab einer gewissen Projektphase (zu definieren) werden alle Änderungen
    // an den Projektdaten den Beteiligten als Warnungen angezeigt.
    public class ProjectChangedAlert : Alert
    {
        public ProjectChangedAlert()
        {
            Priority = AlertPriority.Low;
        }
        public virtual Campaign Campaign { get; set; }
    }

    // Aktionstage müssen X Tage im vorraus (voll-)besetzt sein.
    public class AssignmentIncompleteAlert : Alert
    {
        public AssignmentIncompleteAlert()
        {
            Priority = AlertPriority.Critical;
        }
        public virtual Assignment Assignment { get; set; }
    }

    // Personalplanung abzuschließen bis: 3 Wochen vor erstem Locationdatum/erster Einsatz
    public class StaffSearchUnfinishedAlert : Alert
    {
        public StaffSearchUnfinishedAlert()
        {
            Priority = AlertPriority.High;
        }
        public Campaign Campaign { get; set; }
    }

    public class AlertComment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual User User { get; set; }
    }

    public enum AlertPriority
    {
        VeryLow = 1,
        Low = 3,
        Medium = 5,
        High = 7,
        Critical = 10
    }
}
