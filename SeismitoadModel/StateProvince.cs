using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadModel
{
    public class StateProvince
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}