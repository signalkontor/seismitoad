using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class ChangeSet : IHasId
    {
        #region Constructor
        public ChangeSet()
        {
            Date = LocalDateTime.Now;
            Changes = new Collection<Change>();
        }
        #endregion

        #region Simple properties
        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public string TableName { get; set; }
        [Required]
        public string EntityId { get; set; }
        #endregion

        #region Collection properties
        public virtual ICollection<Change> Changes { get; set; }
        #endregion
    }

}