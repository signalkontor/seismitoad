﻿using System.ComponentModel.DataAnnotations;

namespace SeismitoadModel
{
    public class TrainingParticipant
    {
        public int Id { get; set; }
        public int TrainingDayId { get; set; }

        [Required]
        public string State { get; set; }
        public string Travel { get; set; }
        public decimal? TravelCost { get; set; }
        public string Hotel { get; set; }
        public string Remark { get; set; }

        public virtual TrainingDay TrainingDay { get; set; }
        public virtual CampaignEmployee CampaignEmployee { get; set; }
    }
}