namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignEmployeeAdditionalProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignEmployees", "IsApplication", c => c.Boolean(nullable: false));
            AddColumn("dbo.CampaignEmployees", "Remark", c => c.String());
            AddColumn("dbo.CampaignEmployees", "ContactBy", c => c.String());
            AddColumn("dbo.CampaignEmployees", "LocationsText", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampaignEmployees", "LocationsText");
            DropColumn("dbo.CampaignEmployees", "ContactBy");
            DropColumn("dbo.CampaignEmployees", "Remark");
            DropColumn("dbo.CampaignEmployees", "IsApplication");
        }
    }
}
