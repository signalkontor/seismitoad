namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewCampaignEmployeeState3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignEmployees", "InformationRejected", c => c.DateTime());
            Sql(@"GO
ALTER TRIGGER [dbo].[UpdateCampaignEmployeeState]
	ON [dbo].[CampaignEmployees]
	FOR INSERT, UPDATE 
	AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @CampaignId int;
	DECLARE @EmployeeId int;
	DECLARE @Invited datetime;
	DECLARE @NotInterested datetime;
	DECLARE @InformationRequested datetime;
	DECLARE @InformationSent datetime;
	DECLARE @InformationRejected datetime;
	DECLARE @Applied datetime;
	DECLARE @Rejected datetime;
	DECLARE @Accepted datetime;
	DECLARE @Deferred datetime;
	DECLARE @ContractSent datetime;
	DECLARE @ContractCancelled datetime;
	DECLARE @ContractRejected datetime;
	DECLARE @ContractAccepted datetime;

	DECLARE @State nvarchar(MAX);
	DECLARE @Date datetime;

	SELECT @CampaignId = i.CampaignId from inserted i;
	SELECT @EmployeeId = i.EmployeeId from inserted i;
	SELECT @Invited = i.Invited from inserted i;	
    SELECT @NotInterested = i.NotInterested from inserted i;	
	SELECT @InformationRequested = i.InformationRequested from inserted i;
	SELECT @InformationSent = i.InformationSent from inserted i;
	SELECT @InformationRejected = i.InformationRejected from inserted i;
	SELECT @Applied = i.Applied from inserted i;
	SELECT @Rejected = i.Rejected from inserted i;
	SELECT @Accepted = i.Accepted from inserted i;
	SELECT @Deferred = i.Deferred from inserted i;
	SELECT @ContractSent = i.ContractSent from inserted i;
	SELECT @ContractCancelled = i.ContractCancelled from inserted i;
	SELECT @ContractRejected = i.ContractRejected from inserted i;
	SELECT @ContractAccepted = i.ContractAccepted from inserted i;
	
	SET @State = null;
	IF @State IS NULL AND @ContractCancelled IS NOT NULL
	BEGIN
		SET @State = 'ContractCancelled';
		SET @Date = @ContractCancelled;
	END
	IF @State IS NULL AND @ContractAccepted IS NOT NULL
	BEGIN
		SET @State = 'ContractAccepted';
		SET @Date = @ContractAccepted;
	END
	IF @State IS NULL AND @ContractRejected IS NOT NULL
	BEGIN
		SET @State = 'ContractRejected';
		SET @Date = @ContractRejected;
	END
	IF @State IS NULL AND @ContractSent IS NOT NULL
	BEGIN
		SET @State = 'ContractSent';
		SET @Date = @ContractSent;
	END
	IF @State IS NULL AND @Accepted IS NOT NULL
	BEGIN
		SET @State = 'Accepted';
		SET @Date = @Accepted;
	END
	IF @State IS NULL AND @Rejected IS NOT NULL
	BEGIN
		SET @State = 'Rejected';
		SET @Date = @Rejected;
	END
	IF @State IS NULL AND @Deferred IS NOT NULL
	BEGIN
		SET @State = 'Deferred';
		SET @Date = @Deferred;
	END
	IF @State IS NULL AND @Applied IS NOT NULL
	BEGIN
		SET @State = 'Applied';
		SET @Date = @Applied;
	END
	IF @State IS NULL AND @InformationSent IS NOT NULL
	BEGIN
		SET @State = 'InformationSent';
		SET @Date = @InformationSent;
	END
	IF @State IS NULL AND @InformationRejected IS NOT NULL
	BEGIN
		SET @State = 'InformationRejected';
		SET @Date = @InformationRejected;
	END
	IF @State IS NULL AND @InformationRequested IS NOT NULL
	BEGIN
		SET @State = 'InformationRequested';
		SET @Date = @InformationRequested;
	END
	IF @State IS NULL AND @NotInterested IS NOT NULL
	BEGIN
		SET @State = 'NotInterested';
		SET @Date = @NotInterested;
	END
	IF @State IS NULL AND @Invited IS NOT NULL
	BEGIN
		SET @State = 'Invited';
		SET @Date = @Invited;
	END

	UPDATE dbo.CampaignEmployees SET State = @State, StateSetDate = @Date
	WHERE CampaignId = @CampaignId AND EmployeeId = @EmployeeId
END
GO");
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampaignEmployees", "InformationRejected");
        }
    }
}
