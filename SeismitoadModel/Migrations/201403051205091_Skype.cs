namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Skype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "Skype", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "Skype");
        }
    }
}
