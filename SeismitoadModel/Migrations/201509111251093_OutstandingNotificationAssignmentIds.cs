namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OutstandingNotificationAssignmentIds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OutstandingNotifications", "AssignmentIds", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OutstandingNotifications", "AssignmentIds");
        }
    }
}
