namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ViewAssignments : DbMigration
    {
        public override void Up()
        {
            Sql(@"
/****** Object:  View [dbo].[vw_Assignments]    Script Date: 15.12.2014 17:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Assignments]
AS
SELECT     dbo.Assignments.Id, dbo.Assignments.DateStart, dbo.Assignments.DateEnd, dbo.Assignments.EmployeeCount,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.AssignedEmployees AS ia
                            WHERE      (Assignment_Id = dbo.Assignments.Id)) AS AssignedEmployeeCount, dbo.Assignments.State, dbo.AssignedEmployees.Attendance, 
                      dbo.Assignments.State AS Expr1, dbo.AssignedEmployees.Attendance AS Expr2, dbo.AssignedEmployees.ContactEmployee, dbo.Locations.Name AS LocationName, 
                      dbo.Locations.Street AS LocationStreet, dbo.Locations.PostalCode AS LocationPostalCode, dbo.Locations.City AS LocationCity, dbo.Locations.Id AS LocationId, 
                      dbo.CampaignLocations.Campaign_Id AS CampaignId, PredecessorAssignments.DateStart AS PredecessorDateStart, 
                      SuccessorAssignments.DateStart AS SuccessorDateStart, dbo.Employees.Id AS EmployeeId, dbo.Employees.Firstname AS EmployeeFirstname, 
                      dbo.Employees.Lastname AS EmployeeLastname,
                          (SELECT     STUFF
                                                       ((SELECT     ', ' + dbo.AssignmentRoles.ShortName
                                                           FROM         dbo.AssignedEmployeeAssignmentRoles ir INNER JOIN
                                                                                 dbo.AssignmentRoles ON dbo.AssignmentRoles.Id = ir.AssignmentRole_Id
                                                           WHERE     ir.AssignedEmployee_AssignmentId = dbo.Assignments.Id AND 
                                                                                 ir.AssignedEmployee_EmployeeId = dbo.AssignedEmployees.Employee_Id FOR XML PATH('')), 1, 2, '')) AS RoleShortName
FROM         dbo.Assignments LEFT OUTER JOIN
                      dbo.AssignedEmployees ON dbo.Assignments.Id = dbo.AssignedEmployees.Assignment_Id INNER JOIN
                      dbo.CampaignLocations ON dbo.Assignments.CampaignLocation_CampaignId = dbo.CampaignLocations.Campaign_Id AND 
                      dbo.Assignments.CampaignLocation_LocationId = dbo.CampaignLocations.Location_Id INNER JOIN
                      dbo.Locations ON dbo.CampaignLocations.Location_Id = dbo.Locations.Id LEFT OUTER JOIN
                      dbo.Assignments AS PredecessorAssignments ON dbo.Assignments.Predecessor_Id = PredecessorAssignments.Id LEFT OUTER JOIN
                      dbo.Assignments AS SuccessorAssignments ON SuccessorAssignments.Predecessor_Id = dbo.Assignments.Id LEFT OUTER JOIN
                      dbo.Employees ON dbo.AssignedEmployees.Employee_Id = dbo.Employees.Id

GO
");
        }
        
        public override void Down()
        {
            Sql("DROP VIEW [dbo].[vw_Assignments]");
        }
    }
}
