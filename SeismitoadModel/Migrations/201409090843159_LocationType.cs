namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            Sql(@"GO
SET IDENTITY_INSERT [LocationTypes] ON
INSERT INTO [LocationTypes] ([Id],[Name]) VALUES (1, 'Retailer');
INSERT INTO [LocationTypes] ([Id],[Name]) VALUES (2, 'Shopping-Center');
INSERT INTO [LocationTypes] ([Id],[Name]) VALUES (3, 'Universitšt');
INSERT INTO [LocationTypes] ([Id],[Name]) VALUES (4, 'Event Location');
SET IDENTITY_INSERT [LocationTypes] OFF
GO
");         
            AlterColumn("dbo.Locations", "Type", c => c.Int());
            RenameColumn("dbo.Locations", "Type", "LocationTypeId");
            CreateIndex("dbo.Locations", "LocationTypeId");
            Sql(@"
UPDATE [Locations] SET LocationTypeId = 4 WHERE LocationTypeId = 6;
UPDATE [Locations] SET LocationTypeId = 3 WHERE LocationTypeId = 2;
UPDATE [Locations] SET LocationTypeId = 2 WHERE LocationTypeId = 1;
UPDATE [Locations] SET LocationTypeId = 1 WHERE LocationTypeId = 0;
UPDATE [Locations] SET LocationTypeId = NULL WHERE LocationTypeId = -1;
GO
");
            AddForeignKey("dbo.Locations", "LocationTypeId", "dbo.LocationTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Locations", "LocationTypeId", "dbo.LocationTypes");
            DropIndex("dbo.Locations", new[] { "LocationTypeId" });
            RenameColumn("dbo.Locations", "LocationTypeId", "Type");
            DropTable("dbo.LocationTypes");
            AlterColumn("dbo.Locations", "Type", c => c.Int(nullable: false));
        }
    }
}
