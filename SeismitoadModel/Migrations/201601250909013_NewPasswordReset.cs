namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewPasswordReset : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "PasswordResetCode", c => c.String(nullable: true, maxLength: 40, fixedLength: true));
            Sql("UPDATE dbo.Employees SET PasswordResetCode = PasswordRetrievalKey WHERE PasswordRetrievalKey IS NOT NULL");
            DropColumn("dbo.Accounts", "PasswordRetrievalKey");
            DropColumn("dbo.Employees", "PasswordRetrievalKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "PasswordRetrievalKey", c => c.Guid());
            AddColumn("dbo.Accounts", "PasswordRetrievalKey", c => c.Guid());
        }
    }
}
