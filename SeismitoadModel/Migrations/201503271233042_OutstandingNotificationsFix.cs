namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OutstandingNotificationsFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OutstandingNotifications", "AssignmentId", "dbo.Assignments");
            DropIndex("dbo.OutstandingNotifications", new[] { "AssignmentId" });
            AddColumn("dbo.OutstandingNotifications", "CampaignId", c => c.Int(nullable: false));
            CreateIndex("dbo.OutstandingNotifications", "CampaignId");
            AddForeignKey("dbo.OutstandingNotifications", "CampaignId", "dbo.Campaigns", "Id", cascadeDelete: true);
            DropColumn("dbo.OutstandingNotifications", "AssignmentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OutstandingNotifications", "AssignmentId", c => c.Int(nullable: false));
            DropForeignKey("dbo.OutstandingNotifications", "CampaignId", "dbo.Campaigns");
            DropIndex("dbo.OutstandingNotifications", new[] { "CampaignId" });
            DropColumn("dbo.OutstandingNotifications", "CampaignId");
            CreateIndex("dbo.OutstandingNotifications", "AssignmentId");
            AddForeignKey("dbo.OutstandingNotifications", "AssignmentId", "dbo.Assignments", "Id", cascadeDelete: true);
        }
    }
}
