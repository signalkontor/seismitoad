namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignLocationRelations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CampaignEmployees", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" }, "dbo.CampaignEmployees");
            DropForeignKey("dbo.ApplicantLocations", "Location_Id", "dbo.Locations");
            DropIndex("dbo.CampaignEmployees", new[] { "EmployeeId" });
            DropIndex("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" });
            DropIndex("dbo.ApplicantLocations", new[] { "Location_Id" });
            CreateTable(
                "dbo.CampaignEmployees",
                c => new
                    {
                        CampaignId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        IsApplication = c.Boolean(nullable: false),
                        Remark = c.String(),
                        ContactBy = c.String(),
                        State = c.Int(nullable: false),
                        LocationsText = c.String(),
                    })
                .PrimaryKey(t => new { t.CampaignId, t.EmployeeId })
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.CampaignEmployeeLocations",
                c => new
                    {
                        CampaignEmployee_CampaignId = c.Int(nullable: false),
                        CampaignEmployee_EmployeeId = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CampaignEmployee_CampaignId, t.CampaignEmployee_EmployeeId, t.Location_Id })
                .ForeignKey("dbo.CampaignEmployees", t => new { t.CampaignEmployee_CampaignId, t.CampaignEmployee_EmployeeId }, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => new { t.CampaignEmployee_CampaignId, t.CampaignEmployee_EmployeeId })
                .Index(t => t.Location_Id);
            
            DropTable("dbo.CampaignEmployees");
            DropTable("dbo.ApplicantLocations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ApplicantLocations",
                c => new
                    {
                        Applicant_CampaignId = c.Int(nullable: false),
                        Applicant_EmployeeId = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Applicant_CampaignId, t.Applicant_EmployeeId, t.Location_Id });
            
            CreateTable(
                "dbo.CampaignEmployees",
                c => new
                    {
                        CampaignId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        IsApplication = c.Boolean(nullable: false),
                        Remark = c.String(),
                        ContactBy = c.String(),
                        State = c.Int(nullable: false),
                        LocationsText = c.String(),
                    })
                .PrimaryKey(t => new { t.CampaignId, t.EmployeeId });
            
            DropForeignKey("dbo.CampaignEmployeeLocations", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.CampaignEmployeeLocations", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" }, "dbo.CampaignEmployees");
            DropForeignKey("dbo.CampaignEmployees", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.CampaignEmployeeLocations", new[] { "Location_Id" });
            DropIndex("dbo.CampaignEmployeeLocations", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" });
            DropIndex("dbo.CampaignEmployees", new[] { "EmployeeId" });
            DropTable("dbo.CampaignEmployeeLocations");
            DropTable("dbo.CampaignEmployees");
            CreateIndex("dbo.ApplicantLocations", "Location_Id");
            CreateIndex("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" });
            CreateIndex("dbo.CampaignEmployees", "EmployeeId");
            AddForeignKey("dbo.ApplicantLocations", "Location_Id", "dbo.Locations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" }, "dbo.CampaignEmployees", new[] { "CampaignId", "EmployeeId" }, cascadeDelete: true);
            AddForeignKey("dbo.CampaignEmployees", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
