namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrainingDayLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrainingDays", "Location", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TrainingDays", "Location");
        }
    }
}
