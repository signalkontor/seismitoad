namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Trainings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trainings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployeeTrainings",
                c => new
                    {
                        Employee_Id = c.Int(nullable: false),
                        Training_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Employee_Id, t.Training_Id })
                .ForeignKey("dbo.Employees", t => t.Employee_Id, cascadeDelete: true)
                .ForeignKey("dbo.Trainings", t => t.Training_Id, cascadeDelete: true)
                .Index(t => t.Employee_Id)
                .Index(t => t.Training_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeTrainings", "Training_Id", "dbo.Trainings");
            DropForeignKey("dbo.EmployeeTrainings", "Employee_Id", "dbo.Employees");
            DropIndex("dbo.EmployeeTrainings", new[] { "Training_Id" });
            DropIndex("dbo.EmployeeTrainings", new[] { "Employee_Id" });
            DropTable("dbo.EmployeeTrainings");
            DropTable("dbo.Trainings");
        }
    }
}
