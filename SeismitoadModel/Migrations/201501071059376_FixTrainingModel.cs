namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixTrainingModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeTrainings", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.EmployeeTrainings", "Training_Id", "dbo.Trainings");
            DropForeignKey("dbo.TrainingDays", "Training_Id", "dbo.Trainings");
            DropIndex("dbo.TrainingDays", new[] { "Training_Id" });
            DropIndex("dbo.EmployeeTrainings", new[] { "Employee_Id" });
            DropIndex("dbo.EmployeeTrainings", new[] { "Training_Id" });
            CreateTable(
                "dbo.EmployeeTrainingDays",
                c => new
                    {
                        Employee_Id = c.Int(nullable: false),
                        TrainingDay_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Employee_Id, t.TrainingDay_Id })
                .ForeignKey("dbo.Employees", t => t.Employee_Id, cascadeDelete: true)
                .ForeignKey("dbo.TrainingDays", t => t.TrainingDay_Id, cascadeDelete: true)
                .Index(t => t.Employee_Id)
                .Index(t => t.TrainingDay_Id);
            
            AlterColumn("dbo.TrainingDays", "Training_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.TrainingDays", "Training_Id");
            AddForeignKey("dbo.TrainingDays", "Training_Id", "dbo.Trainings", "Id", cascadeDelete: true);
            DropTable("dbo.EmployeeTrainings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeTrainings",
                c => new
                    {
                        Employee_Id = c.Int(nullable: false),
                        Training_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Employee_Id, t.Training_Id });
            
            DropForeignKey("dbo.TrainingDays", "Training_Id", "dbo.Trainings");
            DropForeignKey("dbo.EmployeeTrainingDays", "TrainingDay_Id", "dbo.TrainingDays");
            DropForeignKey("dbo.EmployeeTrainingDays", "Employee_Id", "dbo.Employees");
            DropIndex("dbo.EmployeeTrainingDays", new[] { "TrainingDay_Id" });
            DropIndex("dbo.EmployeeTrainingDays", new[] { "Employee_Id" });
            DropIndex("dbo.TrainingDays", new[] { "Training_Id" });
            AlterColumn("dbo.TrainingDays", "Training_Id", c => c.Int());
            DropTable("dbo.EmployeeTrainingDays");
            CreateIndex("dbo.EmployeeTrainings", "Training_Id");
            CreateIndex("dbo.EmployeeTrainings", "Employee_Id");
            CreateIndex("dbo.TrainingDays", "Training_Id");
            AddForeignKey("dbo.TrainingDays", "Training_Id", "dbo.Trainings", "Id");
            AddForeignKey("dbo.EmployeeTrainings", "Training_Id", "dbo.Trainings", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeTrainings", "Employee_Id", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
