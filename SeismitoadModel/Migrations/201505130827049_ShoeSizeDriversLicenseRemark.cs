namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShoeSizeDriversLicenseRemark : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "ShoeSize", c => c.Int());
            AddColumn("dbo.EmployeeProfiles", "DriversLicenseRemark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "DriversLicenseRemark");
            DropColumn("dbo.EmployeeProfiles", "ShoeSize");
        }
    }
}
