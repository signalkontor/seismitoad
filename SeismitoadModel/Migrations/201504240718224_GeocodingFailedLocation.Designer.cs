// <auto-generated />
namespace SeismitoadModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class GeocodingFailedLocation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(GeocodingFailedLocation));
        
        string IMigrationMetadata.Id
        {
            get { return "201504240718224_GeocodingFailedLocation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
