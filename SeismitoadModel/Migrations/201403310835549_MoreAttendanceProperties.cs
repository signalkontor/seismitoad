namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Spatial;
    
    public partial class MoreAttendanceProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AssignedEmployees", "AttendanceMobileCheckInDate", c => c.DateTime());
            AddColumn("dbo.AssignedEmployees", "AttendanceMobileCheckOutDate", c => c.DateTime());
            AddColumn("dbo.AssignedEmployees", "AttendanceMobileCheckInCoordinates", c => c.Geography());
            AddColumn("dbo.AssignedEmployees", "AttendanceMobileCheckOutCoordinates", c => c.Geography());
            AddColumn("dbo.AssignedEmployees", "AttendanceSetByUser", c => c.Guid());
            AddColumn("dbo.AssignedEmployees", "AttendanceType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AssignedEmployees", "AttendanceType");
            DropColumn("dbo.AssignedEmployees", "AttendanceSetByUser");
            DropColumn("dbo.AssignedEmployees", "AttendanceMobileCheckOutCoordinates");
            DropColumn("dbo.AssignedEmployees", "AttendanceMobileCheckInCoordinates");
            DropColumn("dbo.AssignedEmployees", "AttendanceMobileCheckOutDate");
            DropColumn("dbo.AssignedEmployees", "AttendanceMobileCheckInDate");
        }
    }
}
