namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssignedEmployeeExtraData : DbMigration
    {
        public override void Up()
        {
            Sql(@"
    CREATE TABLE [dbo].[AssignedEmployeeExtraData](
	[AssignmentId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Notified] [datetime] NULL,
	[Accepted] [datetime] NULL,
	[Rejected] [datetime] NULL,
	[Color]  AS (case when [Notified] IS NULL then 'blue' when [Accepted] IS NULL AND [Rejected] IS NULL then 'pink' when [Accepted] IS NOT NULL then 'green' when [Rejected] IS NOT NULL then 'red' else '' end) PERSISTED NOT NULL
) ON [PRIMARY]
");
            
            Sql(@"
INSERT INTO [dbo].[AssignedEmployeeExtraData] ([AssignmentId],[EmployeeId],[Notified],[Accepted])
(SELECT Assignment_Id, Employee_Id, '1900-01-01', '1900-01-01' FROM [dbo].[AssignedEmployees])");
            
            Sql(@"
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[UpdateAssignedEmployeeExtraData]
   ON  [dbo].[AssignedEmployees]
   AFTER INSERT, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE ExtraData FROM [dbo].[AssignedEmployeeExtraData] ExtraData INNER JOIN
	deleted ON deleted.Assignment_Id = ExtraData.[AssignmentId] AND deleted.Employee_Id = ExtraData.[EmployeeId]

	INSERT INTO [dbo].[AssignedEmployeeExtraData]
           ([AssignmentId]
           ,[EmployeeId]
           ,[Notified]
           ,[Accepted])
	(SELECT Assignment_Id, Employee_Id, NULL, NULL FROM inserted)

END
GO
");

            Sql(@"
ALTER VIEW [dbo].[vw_Assignments]
AS
SELECT     dbo.Assignments.Id, dbo.Assignments.DateStart, dbo.Assignments.DateEnd, dbo.Assignments.EmployeeCount,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.AssignedEmployees AS ia
                            WHERE      (Assignment_Id = dbo.Assignments.Id)) AS AssignedEmployeeCount, dbo.Assignments.State, dbo.AssignedEmployees.Attendance, 
                      dbo.Assignments.State AS Expr1, dbo.AssignedEmployees.Attendance AS Expr2, dbo.AssignedEmployees.ContactEmployee, dbo.Locations.Name AS LocationName, 
                      dbo.Locations.Street AS LocationStreet, dbo.Locations.PostalCode AS LocationPostalCode, dbo.Locations.City AS LocationCity, dbo.Locations.Id AS LocationId, 
                      dbo.CampaignLocations.Campaign_Id AS CampaignId, PredecessorAssignments.DateStart AS PredecessorDateStart, 
                      SuccessorAssignments.DateStart AS SuccessorDateStart, ISNULL(dbo.Employees.Id, -1) AS EmployeeId, dbo.Employees.Firstname AS EmployeeFirstname, 
                      dbo.Employees.Lastname AS EmployeeLastname, dbo.AssignedEmployeeExtraData.Color as Color,
                          (SELECT     STUFF
                                                       ((SELECT     ', ' + dbo.AssignmentRoles.ShortName
                                                           FROM         dbo.AssignedEmployeeAssignmentRoles ir INNER JOIN
                                                                                 dbo.AssignmentRoles ON dbo.AssignmentRoles.Id = ir.AssignmentRole_Id
                                                           WHERE     ir.AssignedEmployee_AssignmentId = dbo.Assignments.Id AND 
                                                                                 ir.AssignedEmployee_EmployeeId = dbo.AssignedEmployees.Employee_Id FOR XML PATH('')), 1, 2, '')) AS RoleShortName
FROM         dbo.Assignments LEFT OUTER JOIN
                      dbo.AssignedEmployees ON dbo.Assignments.Id = dbo.AssignedEmployees.Assignment_Id LEFT OUTER JOIN
					  dbo.AssignedEmployeeExtraData ON dbo.AssignedEmployees.Assignment_Id = dbo.AssignedEmployeeExtraData.AssignmentId AND dbo.AssignedEmployees.Employee_Id = dbo.AssignedEmployeeExtraData.EmployeeId INNER JOIN
                      dbo.CampaignLocations ON dbo.Assignments.CampaignLocation_CampaignId = dbo.CampaignLocations.Campaign_Id AND 
                      dbo.Assignments.CampaignLocation_LocationId = dbo.CampaignLocations.Location_Id INNER JOIN
                      dbo.Locations ON dbo.CampaignLocations.Location_Id = dbo.Locations.Id LEFT OUTER JOIN
                      dbo.Assignments AS PredecessorAssignments ON dbo.Assignments.Predecessor_Id = PredecessorAssignments.Id LEFT OUTER JOIN
                      dbo.Assignments AS SuccessorAssignments ON SuccessorAssignments.Predecessor_Id = dbo.Assignments.Id LEFT OUTER JOIN
                      dbo.Employees ON dbo.AssignedEmployees.Employee_Id = dbo.Employees.Id
GO
");
        }
        
        public override void Down()
        {
            
        }
    }
}
