namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GeocodingFailedLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "GeocodingFailed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "GeocodingFailed");
        }
    }
}
