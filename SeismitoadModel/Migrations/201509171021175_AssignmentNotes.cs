namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssignmentNotes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "Notes", c => c.String());
            AddColumn("dbo.Assignments", "Marker", c => c.Int(nullable: false));
            Sql(@"
ALTER VIEW [dbo].[vw_Assignments]
AS
SELECT  
    dbo.Assignments.Id,
    dbo.Assignments.DateStart,
    dbo.Assignments.DateEnd,
    dbo.Assignments.EmployeeCount,
    (SELECT COUNT(*) FROM dbo.AssignedEmployees WHERE (Assignment_Id = dbo.Assignments.Id)) AS AssignedEmployeeCount,
    dbo.Assignments.State,
    dbo.AssignedEmployees.Attendance,
    dbo.AssignedEmployees.ContactEmployee,
    dbo.Locations.Name AS LocationName,
    dbo.Locations.Street AS LocationStreet,
    dbo.Locations.PostalCode AS LocationPostalCode,
    dbo.Locations.City AS LocationCity,
    dbo.Locations.Id AS LocationId, 
    dbo.CampaignLocations.Campaign_Id AS CampaignId,
    PredecessorAssignments.DateStart AS PredecessorDateStart, 
    SuccessorAssignments.DateStart AS SuccessorDateStart,
    ISNULL(dbo.Employees.Id, -1) AS EmployeeId,
    dbo.Employees.Firstname AS EmployeeFirstname, 
    dbo.Employees.Lastname AS EmployeeLastname,
    CASE WHEN dbo.Assignments.State = 0 THEN dbo.AssignedEmployeeExtraData.Color ELSE NULL END as Color,
    (SELECT STUFF ((SELECT ', ' + dbo.AssignmentRoles.ShortName
        FROM dbo.AssignedEmployeeAssignmentRoles ir INNER JOIN
             dbo.AssignmentRoles ON dbo.AssignmentRoles.Id = ir.AssignmentRole_Id
        WHERE ir.AssignedEmployee_AssignmentId = dbo.Assignments.Id AND ir.AssignedEmployee_EmployeeId = dbo.AssignedEmployees.Employee_Id FOR XML PATH('')), 1, 2, '')) AS RoleShortName,
    dbo.Assignments.Notes,
    dbo.Assignments.Marker
FROM
    dbo.Assignments LEFT OUTER JOIN
        dbo.AssignedEmployees ON dbo.Assignments.Id = dbo.AssignedEmployees.Assignment_Id LEFT OUTER JOIN
        dbo.AssignedEmployeeExtraData ON dbo.AssignedEmployees.Assignment_Id = dbo.AssignedEmployeeExtraData.AssignmentId AND dbo.AssignedEmployees.Employee_Id = dbo.AssignedEmployeeExtraData.EmployeeId INNER JOIN
        dbo.CampaignLocations ON dbo.Assignments.CampaignLocation_CampaignId = dbo.CampaignLocations.Campaign_Id AND 
        dbo.Assignments.CampaignLocation_LocationId = dbo.CampaignLocations.Location_Id INNER JOIN
        dbo.Locations ON dbo.CampaignLocations.Location_Id = dbo.Locations.Id LEFT OUTER JOIN
        dbo.Assignments AS PredecessorAssignments ON dbo.Assignments.Predecessor_Id = PredecessorAssignments.Id LEFT OUTER JOIN
        dbo.Assignments AS SuccessorAssignments ON SuccessorAssignments.Predecessor_Id = dbo.Assignments.Id LEFT OUTER JOIN
        dbo.Employees ON dbo.AssignedEmployees.Employee_Id = dbo.Employees.Id

GO
");
        }
        
        public override void Down()
        {
            Sql(@"ALTER VIEW [dbo].[vw_Assignments]
AS
SELECT  
    dbo.Assignments.Id,
    dbo.Assignments.DateStart,
    dbo.Assignments.DateEnd,
    dbo.Assignments.EmployeeCount,
    (SELECT COUNT(*) FROM dbo.AssignedEmployees WHERE (Assignment_Id = dbo.Assignments.Id)) AS AssignedEmployeeCount,
    dbo.Assignments.State,
    dbo.AssignedEmployees.Attendance,
    dbo.AssignedEmployees.ContactEmployee,
    dbo.Locations.Name AS LocationName,
    dbo.Locations.Street AS LocationStreet,
    dbo.Locations.PostalCode AS LocationPostalCode,
    dbo.Locations.City AS LocationCity,
    dbo.Locations.Id AS LocationId, 
    dbo.CampaignLocations.Campaign_Id AS CampaignId,
    PredecessorAssignments.DateStart AS PredecessorDateStart, 
    SuccessorAssignments.DateStart AS SuccessorDateStart,
    ISNULL(dbo.Employees.Id, -1) AS EmployeeId,
    dbo.Employees.Firstname AS EmployeeFirstname, 
    dbo.Employees.Lastname AS EmployeeLastname,
    dbo.AssignedEmployeeExtraData.Color as Color,
    (SELECT STUFF ((SELECT ', ' + dbo.AssignmentRoles.ShortName
        FROM dbo.AssignedEmployeeAssignmentRoles ir INNER JOIN
             dbo.AssignmentRoles ON dbo.AssignmentRoles.Id = ir.AssignmentRole_Id
        WHERE ir.AssignedEmployee_AssignmentId = dbo.Assignments.Id AND ir.AssignedEmployee_EmployeeId = dbo.AssignedEmployees.Employee_Id FOR XML PATH('')), 1, 2, '')) AS RoleShortName
FROM
    dbo.Assignments LEFT OUTER JOIN
        dbo.AssignedEmployees ON dbo.Assignments.Id = dbo.AssignedEmployees.Assignment_Id LEFT OUTER JOIN
        dbo.AssignedEmployeeExtraData ON dbo.AssignedEmployees.Assignment_Id = dbo.AssignedEmployeeExtraData.AssignmentId AND dbo.AssignedEmployees.Employee_Id = dbo.AssignedEmployeeExtraData.EmployeeId INNER JOIN
        dbo.CampaignLocations ON dbo.Assignments.CampaignLocation_CampaignId = dbo.CampaignLocations.Campaign_Id AND 
        dbo.Assignments.CampaignLocation_LocationId = dbo.CampaignLocations.Location_Id INNER JOIN
        dbo.Locations ON dbo.CampaignLocations.Location_Id = dbo.Locations.Id LEFT OUTER JOIN
        dbo.Assignments AS PredecessorAssignments ON dbo.Assignments.Predecessor_Id = PredecessorAssignments.Id LEFT OUTER JOIN
        dbo.Assignments AS SuccessorAssignments ON SuccessorAssignments.Predecessor_Id = dbo.Assignments.Id LEFT OUTER JOIN
        dbo.Employees ON dbo.AssignedEmployees.Employee_Id = dbo.Employees.Id

GO
");
            DropColumn("dbo.Assignments", "Marker");
            DropColumn("dbo.Assignments", "Notes");
        }
    }
}
