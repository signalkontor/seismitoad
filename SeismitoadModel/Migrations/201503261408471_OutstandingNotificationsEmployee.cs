namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OutstandingNotificationsEmployee : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.OutstandingNotifications", "EmployeeId");
            AddForeignKey("dbo.OutstandingNotifications", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OutstandingNotifications", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.OutstandingNotifications", new[] { "EmployeeId" });
        }
    }
}
