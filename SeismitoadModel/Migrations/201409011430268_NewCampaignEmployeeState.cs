namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewCampaignEmployeeState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignEmployees", "Invited", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "InformationRequested", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "InformationSent", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "Applied", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "Rejected", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "Accepted", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "Deferred", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "ContractSent", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "ContractCancelled", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "ContractRejected", c => c.DateTime());
            AddColumn("dbo.CampaignEmployees", "ContractAccepted", c => c.DateTime());
            DropColumn("dbo.CampaignEmployees", "IsApplication");
            DropColumn("dbo.CampaignEmployees", "State");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CampaignEmployees", "State", c => c.Int(nullable: false));
            AddColumn("dbo.CampaignEmployees", "IsApplication", c => c.Boolean(nullable: false));
            DropColumn("dbo.CampaignEmployees", "ContractAccepted");
            DropColumn("dbo.CampaignEmployees", "ContractRejected");
            DropColumn("dbo.CampaignEmployees", "ContractCancelled");
            DropColumn("dbo.CampaignEmployees", "ContractSent");
            DropColumn("dbo.CampaignEmployees", "Deferred");
            DropColumn("dbo.CampaignEmployees", "Accepted");
            DropColumn("dbo.CampaignEmployees", "Rejected");
            DropColumn("dbo.CampaignEmployees", "Applied");
            DropColumn("dbo.CampaignEmployees", "InformationSent");
            DropColumn("dbo.CampaignEmployees", "InformationRequested");
            DropColumn("dbo.CampaignEmployees", "Invited");
        }
    }
}
