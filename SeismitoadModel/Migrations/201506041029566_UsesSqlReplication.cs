namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsesSqlReplication : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "UsesSqlReplication", c => c.Boolean(nullable: false, defaultValue: true));
            Sql("UPDATE dbo.Campaigns SET UsesSqlReplication = 0 WHERE Id NOT IN (12, 23)");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campaigns", "UsesSqlReplication");
        }
    }
}
