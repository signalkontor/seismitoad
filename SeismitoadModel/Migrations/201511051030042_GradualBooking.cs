namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GradualBooking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "GradualBooking", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campaigns", "GradualBooking");
        }
    }
}
