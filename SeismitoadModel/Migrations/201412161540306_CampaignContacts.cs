namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignContacts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "ContactPMName1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMPhone1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMEmail1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMRemark1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMName2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMPhone2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMEmail2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMRemark2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMName3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMPhone3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMEmail3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMRemark3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMName4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMPhone4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMEmail4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactPMRemark4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADName1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADPhone1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADEmail1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADRemark1", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADName2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADPhone2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADEmail2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADRemark2", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADName3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADPhone3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADEmail3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADRemark3", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADName4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADPhone4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADEmail4", c => c.String());
            AddColumn("dbo.Campaigns", "ContactADRemark4", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campaigns", "ContactADRemark4");
            DropColumn("dbo.Campaigns", "ContactADEmail4");
            DropColumn("dbo.Campaigns", "ContactADPhone4");
            DropColumn("dbo.Campaigns", "ContactADName4");
            DropColumn("dbo.Campaigns", "ContactADRemark3");
            DropColumn("dbo.Campaigns", "ContactADEmail3");
            DropColumn("dbo.Campaigns", "ContactADPhone3");
            DropColumn("dbo.Campaigns", "ContactADName3");
            DropColumn("dbo.Campaigns", "ContactADRemark2");
            DropColumn("dbo.Campaigns", "ContactADEmail2");
            DropColumn("dbo.Campaigns", "ContactADPhone2");
            DropColumn("dbo.Campaigns", "ContactADName2");
            DropColumn("dbo.Campaigns", "ContactADRemark1");
            DropColumn("dbo.Campaigns", "ContactADEmail1");
            DropColumn("dbo.Campaigns", "ContactADPhone1");
            DropColumn("dbo.Campaigns", "ContactADName1");
            DropColumn("dbo.Campaigns", "ContactPMRemark4");
            DropColumn("dbo.Campaigns", "ContactPMEmail4");
            DropColumn("dbo.Campaigns", "ContactPMPhone4");
            DropColumn("dbo.Campaigns", "ContactPMName4");
            DropColumn("dbo.Campaigns", "ContactPMRemark3");
            DropColumn("dbo.Campaigns", "ContactPMEmail3");
            DropColumn("dbo.Campaigns", "ContactPMPhone3");
            DropColumn("dbo.Campaigns", "ContactPMName3");
            DropColumn("dbo.Campaigns", "ContactPMRemark2");
            DropColumn("dbo.Campaigns", "ContactPMEmail2");
            DropColumn("dbo.Campaigns", "ContactPMPhone2");
            DropColumn("dbo.Campaigns", "ContactPMName2");
            DropColumn("dbo.Campaigns", "ContactPMRemark1");
            DropColumn("dbo.Campaigns", "ContactPMEmail1");
            DropColumn("dbo.Campaigns", "ContactPMPhone1");
            DropColumn("dbo.Campaigns", "ContactPMName1");
        }
    }
}
