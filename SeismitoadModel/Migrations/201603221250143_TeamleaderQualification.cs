namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TeamleaderQualification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "TeamleaderQualification", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "TeamleaderQualification");
        }
    }
}
