namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TeamComputedColumns : DbMigration
    {
        public override void Up()
        {
            Sql(@"
-- =============================================
-- Author:		Mikko Jania
-- Create date: 30.04.2014
-- Description: Baut einen String der die
--              Teammitglieder anzeigt.               
-- =============================================
CREATE FUNCTION [dbo].[Team] 
(
	-- Add the parameters for the function here
	@AssignmentId int
)
RETURNS nvarchar(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(MAX)

	SELECT @Result = STUFF(
	(
		SELECT ', ' + [dbo].[Employees].[Firstname] + ' ' + [dbo].[Employees].[Lastname]
		FROM [dbo].[Employees]
		INNER JOIN [dbo].[AssignedEmployees]
			ON [dbo].[AssignedEmployees].[Employee_Id] = [dbo].[Employees].[Id]
		WHERE [dbo].[AssignedEmployees].[Assignment_Id] = @AssignmentId
		FOR XML PATH('')
	),
	1,
	2,
	'')
	
	-- Return the result of the function
	RETURN @Result

END
GO
");

            Sql(@"
-- =============================================
-- Author:		Mikko Jania
-- Create date: 30.04.2014
-- Description: Baut einen String der die
--              Teammitglieder anzeigt.               
-- =============================================
CREATE FUNCTION [dbo].[TeamShort] 
(
	-- Add the parameters for the function here
	@AssignmentId int
)
RETURNS nvarchar(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(MAX)

	SELECT @Result = STUFF(
	(
		SELECT ', ' + [dbo].[Employees].[Lastname]
		FROM [dbo].[Employees]
		INNER JOIN [dbo].[AssignedEmployees]
			ON [dbo].[AssignedEmployees].[Employee_Id] = [dbo].[Employees].[Id]
		WHERE [dbo].[AssignedEmployees].[Assignment_Id] = @AssignmentId
		FOR XML PATH('')
	),
	1,
	2,
	'')
	
	-- Return the result of the function
	RETURN @Result

END
GO
");
            Sql("ALTER TABLE dbo.Assignments ADD Team AS ([dbo].[Team]([Id]))");
            Sql("ALTER TABLE dbo.Assignments ADD TeamShort AS ([dbo].[TeamShort]([Id]))");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Assignments", "TeamShort");
            DropColumn("dbo.Assignments", "Team");
            Sql("DROP FUNCTION [dbo].[TeamShort]");
            Sql("DROP FUNCTION [dbo].[Team]");
        }
    }
}
