namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrainingDay : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TrainingDays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateFrom = c.DateTime(nullable: false),
                        DateTil = c.DateTime(nullable: false),
                        Remarks = c.String(),
                        Training_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trainings", t => t.Training_Id)
                .Index(t => t.Training_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TrainingDays", "Training_Id", "dbo.Trainings");
            DropIndex("dbo.TrainingDays", new[] { "Training_Id" });
            DropTable("dbo.TrainingDays");
        }
    }
}
