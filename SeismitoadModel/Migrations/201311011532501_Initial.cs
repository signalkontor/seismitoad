namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applicants",
                c => new
                    {
                        CampaignId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CampaignId, t.EmployeeId })
                .ForeignKey("dbo.Campaigns", t => t.CampaignId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CampaignId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Campaigns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Customer_Id = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        Manager = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        InstanceUrl = c.String(),
                        EmployeesPerAssignment = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        IsApplyable = c.Boolean(nullable: false),
                        Duration = c.String(),
                        Contact = c.String(),
                        Days = c.String(),
                        Locations = c.String(),
                        Requirements = c.String(),
                        Times = c.String(),
                        Budget = c.String(),
                        Classification = c.String(),
                        BidSum = c.String(),
                        PaymentTargets = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_Id, cascadeDelete: true)
                .Index(t => t.Customer_Id);
            
            CreateTable(
                "dbo.CampaignLocations",
                c => new
                    {
                        Campaign_Id = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                        NetworkConnectionAvailable = c.Boolean(),
                        State = c.Int(nullable: false),
                        Classification = c.String(),
                        SalesRegion = c.String(),
                        Notes = c.String(),
                        Contact_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.Campaign_Id, t.Location_Id })
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .ForeignKey("dbo.Campaigns", t => t.Campaign_Id, cascadeDelete: true)
                .Index(t => t.Contact_Id)
                .Index(t => t.Location_Id)
                .Index(t => t.Campaign_Id);
            
            CreateTable(
                "dbo.Assignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        EmployeeCount = c.Int(nullable: false),
                        Synchronized = c.Boolean(),
                        State = c.Int(nullable: false),
                        CampaignLocation_CampaignId = c.Int(nullable: false),
                        CampaignLocation_LocationId = c.Int(nullable: false),
                        Predecessor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CampaignLocations", t => new { t.CampaignLocation_CampaignId, t.CampaignLocation_LocationId })
                .ForeignKey("dbo.Assignments", t => t.Predecessor_Id)
                .Index(t => new { t.CampaignLocation_CampaignId, t.CampaignLocation_LocationId })
                .Index(t => t.Predecessor_Id);
            
            CreateTable(
                "dbo.AssignedEmployees",
                c => new
                    {
                        Assignment_Id = c.Int(nullable: false),
                        Employee_Id = c.Int(nullable: false),
                        Attendance = c.Int(nullable: false),
                        DateAttendanceSet = c.DateTime(),
                        AttendanceComment = c.String(),
                        ContactEmployee = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Assignment_Id, t.Employee_Id })
                .ForeignKey("dbo.Employees", t => t.Employee_Id, cascadeDelete: true)
                .ForeignKey("dbo.Assignments", t => t.Assignment_Id, cascadeDelete: true)
                .Index(t => t.Employee_Id)
                .Index(t => t.Assignment_Id);
            
            CreateTable(
                "dbo.AssignmentRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ShortName = c.String(nullable: false),
                        Description = c.String(),
                        DoesReport = c.Boolean(nullable: false),
                        OnlySalesReport = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        StateComment = c.String(),
                        ImportMatchType = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Firstname = c.String(nullable: false),
                        Lastname = c.String(nullable: false),
                        ProviderUserKey = c.Guid(nullable: false),
                        PasswordRetrievalKey = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Experiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Sector = c.Int(nullable: false),
                        SectorDetails = c.Int(nullable: false),
                        Task = c.Int(nullable: false),
                        OtherTask = c.String(),
                        Timeframe = c.String(nullable: false),
                        Title = c.String(),
                        Product = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Agency = c.String(nullable: false),
                        Employee_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Employee_Id, cascadeDelete: true)
                .Index(t => t.Employee_Id);
            
            CreateTable(
                "dbo.EmployeeProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                        Email2 = c.String(),
                        Website = c.String(),
                        ExternalUntil = c.DateTime(),
                        IsExternal = c.Boolean(nullable: false),
                        PhoneMobileNo = c.String(),
                        PhoneNo = c.String(),
                        FaxNo = c.String(),
                        Street = c.String(),
                        PostalCode = c.String(),
                        City = c.String(),
                        Country = c.Int(),
                        PersonalDataComment = c.String(),
                        Height = c.Int(),
                        Size = c.Int(),
                        ShirtSize = c.Int(),
                        JeansWidth = c.Int(),
                        JeansLength = c.Int(),
                        HairColor = c.Int(),
                        VisibleTattoos = c.Boolean(),
                        FacialPiercing = c.Boolean(),
                        OtherAbilitiesText = c.String(),
                        SkeletonContract = c.Boolean(),
                        CopyTradeLicense = c.Boolean(),
                        ValidTradeLicense = c.Boolean(),
                        TaxOfficeLocation = c.String(),
                        TaxNumber = c.String(),
                        TurnoverTaxDeductible = c.Boolean(),
                        FreelancerQuestionaire = c.Boolean(),
                        HealthCertificate = c.Boolean(),
                        CopyHealthCertificate = c.Boolean(),
                        CopyIdentityCard = c.Boolean(),
                        Comment = c.String(),
                        DriversLicense = c.Boolean(),
                        DriversLicenseSince = c.DateTime(),
                        HasDigitalDriversCard = c.Boolean(),
                        IsCarAvailable = c.Boolean(),
                        IsBahncardAvailable = c.Boolean(),
                        Education = c.Int(),
                        OtherCompletedApprenticeships = c.String(),
                        Studies = c.Boolean(),
                        StudiesDetails = c.String(),
                        WorkExperience = c.String(),
                        LanguagesGerman = c.Int(),
                        LanguagesEnglish = c.Int(),
                        LanguagesSpanish = c.Int(),
                        LanguagesFrench = c.Int(),
                        LanguagesItalian = c.Int(),
                        LanguagesTurkish = c.Int(),
                        LanguagesOther = c.String(),
                        SoftwareKnownledge = c.Int(),
                        SoftwareKnownledgeDetails = c.String(),
                        HardwareKnownledge = c.Int(),
                        HardwareKnownledgeDetails = c.String(),
                        InternalTraining = c.String(),
                        ExternalTraining = c.String(),
                        TravelWillingness = c.Boolean(),
                        AccommodationPossible2 = c.String(),
                        PersonalityTestAnswers = c.String(),
                        ArtisticAbilities = c.String(),
                        GastronomicAbilities = c.String(),
                        Sports = c.String(),
                        OtherAbilities = c.String(),
                        DriversLicenseClass1 = c.String(),
                        DriversLicenseClass2 = c.String(),
                        Apprenticeships = c.String(),
                        PreferredTasks = c.String(),
                        PreferredTypes = c.String(),
                        PreferredWorkSchedule = c.String(),
                        AccommodationPossible1 = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Engagement = c.Decimal(precision: 18, scale: 2),
                        Reliability = c.Decimal(precision: 18, scale: 2),
                        Toughness = c.Decimal(precision: 18, scale: 2),
                        Teamwork = c.Decimal(precision: 18, scale: 2),
                        Customerdialog = c.Decimal(precision: 18, scale: 2),
                        Appearance = c.Decimal(precision: 18, scale: 2),
                        Comment = c.String(),
                        RatingType = c.Int(nullable: false),
                        Campaign_Id = c.Int(),
                        Employee_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Employee_Id, cascadeDelete: true)
                .ForeignKey("dbo.Campaigns", t => t.Campaign_Id)
                .Index(t => t.Employee_Id)
                .Index(t => t.Campaign_Id);
            
            CreateTable(
                "dbo.FaxMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sender = c.String(),
                        Subject = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        AttachementFile = c.String(),
                        Received = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        Assignment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assignments", t => t.Assignment_Id)
                .Index(t => t.Assignment_Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        Phone = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Type = c.String(),
                        Comment = c.String(),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalId = c.Int(),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Name2 = c.String(),
                        Street = c.String(nullable: false),
                        Street2 = c.String(),
                        PostalCode = c.String(nullable: false),
                        City = c.String(nullable: false),
                        StateProvince_Id = c.Int(nullable: false),
                        GeographicCoordinates = c.Geography(),
                        LocationGroup_Id = c.Int(),
                        Notes = c.String(),
                        CenterManagementStreet = c.String(),
                        CenterManagementPostalCode = c.String(),
                        CenterManagementCity = c.String(),
                        Phone = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VisitorFrequency = c.Int(),
                        Area = c.Int(),
                        SellingArea = c.Int(),
                        CampaignAreaPrices = c.String(),
                        Audience = c.Int(),
                        Opening = c.Int(),
                        CatchmentArea = c.Int(),
                        Parking = c.Int(),
                        OutdoorArea = c.Boolean(),
                        Retailers = c.String(),
                        WiFiAvailable = c.Boolean(),
                        ConferenceRoomSize = c.String(),
                        CostTransferPossible = c.Boolean(),
                        RoomRates = c.String(),
                        ConferenceEquipmentCost = c.String(),
                        ConferencePackageCost = c.String(),
                        State = c.Int(nullable: false),
                        OpeningHours_MondayStart = c.Int(nullable: false),
                        OpeningHours_MondayEnd = c.Int(nullable: false),
                        OpeningHours_TuesdayStart = c.Int(nullable: false),
                        OpeningHours_TuesdayEnd = c.Int(nullable: false),
                        OpeningHours_WednesdayStart = c.Int(nullable: false),
                        OpeningHours_WednesdayEnd = c.Int(nullable: false),
                        OpeningHours_ThursdayStart = c.Int(nullable: false),
                        OpeningHours_ThursdayEnd = c.Int(nullable: false),
                        OpeningHours_FridayStart = c.Int(nullable: false),
                        OpeningHours_FridayEnd = c.Int(nullable: false),
                        OpeningHours_SaturdayStart = c.Int(nullable: false),
                        OpeningHours_SaturdayEnd = c.Int(nullable: false),
                        OpeningHours_SundayStart = c.Int(nullable: false),
                        OpeningHours_SundayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_MondayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_MondayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_TuesdayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_TuesdayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_WednesdayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_WednesdayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_ThursdayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_ThursdayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_FridayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_FridayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_SaturdayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_SaturdayEnd = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_SundayStart = c.Int(nullable: false),
                        OpeningHoursIncomingGoodsDepartment_SundayEnd = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationGroups", t => t.LocationGroup_Id)
                .ForeignKey("dbo.StateProvinces", t => t.StateProvince_Id, cascadeDelete: true)
                .Index(t => t.LocationGroup_Id)
                .Index(t => t.StateProvince_Id);
            
            CreateTable(
                "dbo.CampaignAreas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Location_Id = c.Int(nullable: false),
                        Size = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.LocationGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateProvinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Street = c.String(nullable: false),
                        ZipCode = c.String(nullable: false),
                        City = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserKey = c.Guid(nullable: false),
                        CampaignRoles = c.String(),
                        Campaign_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaigns", t => t.Campaign_Id, cascadeDelete: true)
                .Index(t => t.Campaign_Id);
            
            CreateTable(
                "dbo.Changes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PropName = c.String(nullable: false),
                        OldValueRaw = c.String(),
                        NewValueRaw = c.String(),
                        OldValueDisplay = c.String(),
                        NewValueDisplay = c.String(),
                        ChangeSet_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChangeSets", t => t.ChangeSet_Id, cascadeDelete: true)
                .Index(t => t.ChangeSet_Id);
            
            CreateTable(
                "dbo.ChangeSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                        TableName = c.String(nullable: false),
                        EntityId = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssignedEmployeeAssignmentRoles",
                c => new
                    {
                        AssignedEmployee_AssignmentId = c.Int(nullable: false),
                        AssignedEmployee_EmployeeId = c.Int(nullable: false),
                        AssignmentRole_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AssignedEmployee_AssignmentId, t.AssignedEmployee_EmployeeId, t.AssignmentRole_Id })
                .ForeignKey("dbo.AssignedEmployees", t => new { t.AssignedEmployee_AssignmentId, t.AssignedEmployee_EmployeeId }, cascadeDelete: true)
                .ForeignKey("dbo.AssignmentRoles", t => t.AssignmentRole_Id, cascadeDelete: true)
                .Index(t => new { t.AssignedEmployee_AssignmentId, t.AssignedEmployee_EmployeeId })
                .Index(t => t.AssignmentRole_Id);
            
            CreateTable(
                "dbo.ApplicantLocations",
                c => new
                    {
                        Applicant_CampaignId = c.Int(nullable: false),
                        Applicant_EmployeeId = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Applicant_CampaignId, t.Applicant_EmployeeId, t.Location_Id })
                .ForeignKey("dbo.Applicants", t => new { t.Applicant_CampaignId, t.Applicant_EmployeeId }, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => new { t.Applicant_CampaignId, t.Applicant_EmployeeId })
                .Index(t => t.Location_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Changes", "ChangeSet_Id", "dbo.ChangeSets");
            DropForeignKey("dbo.ApplicantLocations", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" }, "dbo.Applicants");
            DropForeignKey("dbo.Applicants", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Applicants", "CampaignId", "dbo.Campaigns");
            DropForeignKey("dbo.Ratings", "Campaign_Id", "dbo.Campaigns");
            DropForeignKey("dbo.Logins", "Campaign_Id", "dbo.Campaigns");
            DropForeignKey("dbo.Campaigns", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.CampaignLocations", "Campaign_Id", "dbo.Campaigns");
            DropForeignKey("dbo.CampaignLocations", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.CampaignLocations", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Locations", "StateProvince_Id", "dbo.StateProvinces");
            DropForeignKey("dbo.Locations", "LocationGroup_Id", "dbo.LocationGroups");
            DropForeignKey("dbo.Contacts", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.CampaignAreas", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.Assignments", "Predecessor_Id", "dbo.Assignments");
            DropForeignKey("dbo.FaxMessages", "Assignment_Id", "dbo.Assignments");
            DropForeignKey("dbo.Assignments", new[] { "CampaignLocation_CampaignId", "CampaignLocation_LocationId" }, "dbo.CampaignLocations");
            DropForeignKey("dbo.AssignedEmployees", "Assignment_Id", "dbo.Assignments");
            DropForeignKey("dbo.AssignedEmployees", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.Ratings", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.EmployeeProfiles", "Id", "dbo.Employees");
            DropForeignKey("dbo.Experiences", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.AssignedEmployeeAssignmentRoles", "AssignmentRole_Id", "dbo.AssignmentRoles");
            DropForeignKey("dbo.AssignedEmployeeAssignmentRoles", new[] { "AssignedEmployee_AssignmentId", "AssignedEmployee_EmployeeId" }, "dbo.AssignedEmployees");
            DropIndex("dbo.Changes", new[] { "ChangeSet_Id" });
            DropIndex("dbo.ApplicantLocations", new[] { "Location_Id" });
            DropIndex("dbo.ApplicantLocations", new[] { "Applicant_CampaignId", "Applicant_EmployeeId" });
            DropIndex("dbo.Applicants", new[] { "EmployeeId" });
            DropIndex("dbo.Applicants", new[] { "CampaignId" });
            DropIndex("dbo.Ratings", new[] { "Campaign_Id" });
            DropIndex("dbo.Logins", new[] { "Campaign_Id" });
            DropIndex("dbo.Campaigns", new[] { "Customer_Id" });
            DropIndex("dbo.CampaignLocations", new[] { "Campaign_Id" });
            DropIndex("dbo.CampaignLocations", new[] { "Location_Id" });
            DropIndex("dbo.CampaignLocations", new[] { "Contact_Id" });
            DropIndex("dbo.Locations", new[] { "StateProvince_Id" });
            DropIndex("dbo.Locations", new[] { "LocationGroup_Id" });
            DropIndex("dbo.Contacts", new[] { "Location_Id" });
            DropIndex("dbo.CampaignAreas", new[] { "Location_Id" });
            DropIndex("dbo.Assignments", new[] { "Predecessor_Id" });
            DropIndex("dbo.FaxMessages", new[] { "Assignment_Id" });
            DropIndex("dbo.Assignments", new[] { "CampaignLocation_CampaignId", "CampaignLocation_LocationId" });
            DropIndex("dbo.AssignedEmployees", new[] { "Assignment_Id" });
            DropIndex("dbo.AssignedEmployees", new[] { "Employee_Id" });
            DropIndex("dbo.Ratings", new[] { "Employee_Id" });
            DropIndex("dbo.EmployeeProfiles", new[] { "Id" });
            DropIndex("dbo.Experiences", new[] { "Employee_Id" });
            DropIndex("dbo.AssignedEmployeeAssignmentRoles", new[] { "AssignmentRole_Id" });
            DropIndex("dbo.AssignedEmployeeAssignmentRoles", new[] { "AssignedEmployee_AssignmentId", "AssignedEmployee_EmployeeId" });
            DropTable("dbo.ApplicantLocations");
            DropTable("dbo.AssignedEmployeeAssignmentRoles");
            DropTable("dbo.ChangeSets");
            DropTable("dbo.Changes");
            DropTable("dbo.Logins");
            DropTable("dbo.Customers");
            DropTable("dbo.StateProvinces");
            DropTable("dbo.LocationGroups");
            DropTable("dbo.CampaignAreas");
            DropTable("dbo.Locations");
            DropTable("dbo.Contacts");
            DropTable("dbo.FaxMessages");
            DropTable("dbo.Ratings");
            DropTable("dbo.EmployeeProfiles");
            DropTable("dbo.Experiences");
            DropTable("dbo.Employees");
            DropTable("dbo.AssignmentRoles");
            DropTable("dbo.AssignedEmployees");
            DropTable("dbo.Assignments");
            DropTable("dbo.CampaignLocations");
            DropTable("dbo.Campaigns");
            DropTable("dbo.Applicants");
        }
    }
}
