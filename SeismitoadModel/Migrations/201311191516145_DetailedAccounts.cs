namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DetailedAccounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        ProviderUserKey = c.Guid(nullable: false),
                        Title = c.String(nullable: false),
                        Firstname = c.String(nullable: false),
                        Lastname = c.String(nullable: false),
                        PasswordRetrievalKey = c.Guid(),
                        CustomerId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ProviderUserKey)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);

            RenameColumn("dbo.Campaigns", "Manager", "ManagerId");
            CreateIndex("dbo.Campaigns", "ManagerId");
            Sql("INSERT INTO dbo.Accounts (ProviderUserKey, Title, Firstname, Lastname, Discriminator) SELECT DISTINCT ManagerId, 'Unbekannt', 'N.N.', 'N.N.', 'AdminAccount' FROM dbo.Campaigns");
            AddForeignKey("dbo.Campaigns", "ManagerId", "dbo.Accounts", "ProviderUserKey");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Campaigns", "ManagerId", "dbo.Accounts");
            DropIndex("dbo.Accounts", new[] { "CustomerId" });
            DropIndex("dbo.Campaigns", new[] { "ManagerId" });
            RenameColumn("dbo.Campaigns", "ManagerId", "Manager"); 
            DropTable("dbo.Accounts");
        }
    }
}
