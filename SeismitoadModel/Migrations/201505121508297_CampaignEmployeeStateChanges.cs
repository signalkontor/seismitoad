namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignEmployeeStateChanges : DbMigration
    {
        public override void Up()
        {
            Sql(@"
/****** Object:  UserDefinedFunction [dbo].[CampaignEmployeeStateAndDate]    Script Date: 12.05.2015 17:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[CampaignEmployeeStateAndDate]
(
	@CampaignId int, 
	@EmployeeId int
)
RETURNS 
@CampaignEmployeeStateAndDateResults TABLE 
(
	[State] nvarchar(20),
	[Date] datetime2
)
AS
BEGIN
	DECLARE @State NVARCHAR(20);
	DECLARE @Date datetime2;

    DECLARE @StateSetDate datetime;
	DECLARE @Invited datetime;
	DECLARE @NotInterested datetime;
	DECLARE @InformationRequested datetime;
	DECLARE @InformationSent datetime;
	DECLARE @InformationRejected datetime;
	DECLARE @Applied datetime;
	DECLARE @Rejected datetime;
	DECLARE @Accepted datetime;
	DECLARE @Deferred datetime;
	DECLARE @ContractSent datetime;
	DECLARE @ContractCancelled datetime;
	DECLARE @ContractRejected datetime;
	DECLARE @ContractAccepted datetime;

	SELECT
        @StateSetDate = StateSetDate,
		@Invited = Invited,	
		@NotInterested = NotInterested,
		@InformationRequested = InformationRequested,
		@InformationSent = InformationSent,
		@InformationRejected = InformationRejected,
		@Applied = Applied,
		@Rejected = Rejected,
		@Accepted = Accepted,
		@Deferred = Deferred,
		@ContractSent = ContractSent,
		@ContractCancelled = ContractCancelled,
		@ContractRejected = ContractRejected,
		@ContractAccepted = ContractAccepted
	FROM CampaignEmployees i
	WHERE CampaignId = @CampaignId AND EmployeeId = @EmployeeId;

	SET @State = null;
	IF @State IS NULL AND @NotInterested IS NOT NULL
	BEGIN
		SET @State = 'NotInterested';
		SET @Date = @NotInterested;
	END
	IF @State IS NULL AND @ContractCancelled IS NOT NULL
	BEGIN
		SET @State = 'ContractCancelled';
		SET @Date = @ContractCancelled;
	END
	IF @State IS NULL AND @ContractAccepted IS NOT NULL
	BEGIN
		SET @State = 'ContractAccepted';
		SET @Date = @ContractAccepted;
	END
	IF @State IS NULL AND @ContractRejected IS NOT NULL
	BEGIN
		SET @State = 'ContractRejected';
		SET @Date = @ContractRejected;
	END
	IF @State IS NULL AND @ContractSent IS NOT NULL
	BEGIN
		SET @State = 'ContractSent';
		SET @Date = @ContractSent;
	END
	IF @State IS NULL AND @Accepted IS NOT NULL
	BEGIN
		SET @State = 'Accepted';
		SET @Date = @Accepted;
	END
	IF @State IS NULL AND @Rejected IS NOT NULL
	BEGIN
		SET @State = 'Rejected';
		SET @Date = @Rejected;
	END
	IF @State IS NULL AND @Deferred IS NOT NULL
	BEGIN
		SET @State = 'Deferred';
		SET @Date = @Deferred;
	END
	IF @State IS NULL AND @Applied IS NOT NULL
	BEGIN
		SET @State = 'Applied';
		SET @Date = @Applied;
	END
	IF @State IS NULL AND @InformationSent IS NOT NULL
	BEGIN
		SET @State = 'InformationSent';
		SET @Date = @InformationSent;
	END
	IF @State IS NULL AND @InformationRejected IS NOT NULL
	BEGIN
		SET @State = 'InformationRejected';
		SET @Date = @InformationRejected;
	END
	IF @State IS NULL AND @InformationRequested IS NOT NULL
	BEGIN
		SET @State = 'InformationRequested';
		SET @Date = @InformationRequested;
	END
	IF @State IS NULL AND @Invited IS NOT NULL
	BEGIN
		SET @State = 'Invited';
		SET @Date = @Invited;
	END
    IF @State IS NULL
    BEGIN
	    SET @State = 'New';
        SET @Date = @StateSetDate;
    END

	INSERT @CampaignEmployeeStateAndDateResults
	SELECT @State, @Date
		
	RETURN
END

GO
");
        }
        
        public override void Down()
        {
        }
    }
}
