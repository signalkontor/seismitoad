namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OutstandingNotifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OutstandingNotifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DataId = c.Guid(nullable: false),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        AssignmentId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        Type = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assignments", t => t.AssignmentId, cascadeDelete: true)
                .Index(t => t.AssignmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OutstandingNotifications", "AssignmentId", "dbo.Assignments");
            DropIndex("dbo.OutstandingNotifications", new[] { "AssignmentId" });
            DropTable("dbo.OutstandingNotifications");
        }
    }
}
