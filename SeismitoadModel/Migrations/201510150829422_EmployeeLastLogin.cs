namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeLastLogin : DbMigration
    {
        public override void Up()
        {
            Sql(@"-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMembershipLastLogin]
(
	-- Add the parameters for the function here
	@UserId uniqueidentifier
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result datetime

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = (SELECT TOP 1 LastLoginDate FROM seismitoad_aspnet.dbo.aspnet_Membership WHERE UserId = @UserId)

	-- Return the result of the function
	RETURN @Result

END
GO");
            Sql("ALTER TABLE dbo.Employees ADD LastLogin AS dbo.GetMembershipLastLogin(ProviderUserKey)");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "LastLogin");
        }
    }
}
