// <auto-generated />
namespace SeismitoadModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ViewAssignmentRemoveContactEmployee : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ViewAssignmentRemoveContactEmployee));
        
        string IMigrationMetadata.Id
        {
            get { return "201509151125131_ViewAssignmentRemoveContactEmployee"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
