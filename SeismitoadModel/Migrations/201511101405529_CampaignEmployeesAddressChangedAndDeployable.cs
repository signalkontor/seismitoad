namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignEmployeesAddressChangedAndDeployable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignEmployees", "AddressChanged", c => c.Boolean(nullable: false));
            AddColumn("dbo.CampaignEmployees", "HasAssignments", c => c.Boolean(nullable: false));
            AddColumn("dbo.CampaignEmployees", "Deployable", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampaignEmployees", "Deployable");
            DropColumn("dbo.CampaignEmployees", "HasAssignments");
            DropColumn("dbo.CampaignEmployees", "AddressChanged");
        }
    }
}
