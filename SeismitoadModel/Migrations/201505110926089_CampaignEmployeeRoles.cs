namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignEmployeeRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignEmployees", "Roles", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampaignEmployees", "Roles");
        }
    }
}
