namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrainingParticipant : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeTrainingDays", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.EmployeeTrainingDays", "TrainingDay_Id", "dbo.TrainingDays");
            DropIndex("dbo.EmployeeTrainingDays", new[] { "Employee_Id" });
            DropIndex("dbo.EmployeeTrainingDays", new[] { "TrainingDay_Id" });
            RenameColumn(table: "dbo.TrainingDays", name: "Training_Id", newName: "TrainingId");
            RenameIndex(table: "dbo.TrainingDays", name: "IX_Training_Id", newName: "IX_TrainingId");
            CreateTable(
                "dbo.TrainingParticipants",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false),
                        TrainingDayId = c.Int(nullable: false),
                        State = c.String(nullable: false),
                        Travel = c.String(),
                        TravelCost = c.Decimal(precision: 18, scale: 2),
                        Hotel = c.String(),
                        Remark = c.String(),
                    })
                .PrimaryKey(t => new { t.EmployeeId, t.TrainingDayId })
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.TrainingDays", t => t.TrainingDayId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.TrainingDayId);
            
            AddColumn("dbo.TrainingDays", "MaxParticipants", c => c.Int(nullable: false));
            AddColumn("dbo.Trainings", "CampaignId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trainings", "CampaignId");
            AddForeignKey("dbo.Trainings", "CampaignId", "dbo.Campaigns", "Id", cascadeDelete: true);
            DropTable("dbo.EmployeeTrainingDays");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeTrainingDays",
                c => new
                    {
                        Employee_Id = c.Int(nullable: false),
                        TrainingDay_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Employee_Id, t.TrainingDay_Id });
            
            DropForeignKey("dbo.Trainings", "CampaignId", "dbo.Campaigns");
            DropForeignKey("dbo.TrainingParticipants", "TrainingDayId", "dbo.TrainingDays");
            DropForeignKey("dbo.TrainingParticipants", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Trainings", new[] { "CampaignId" });
            DropIndex("dbo.TrainingParticipants", new[] { "TrainingDayId" });
            DropIndex("dbo.TrainingParticipants", new[] { "EmployeeId" });
            DropColumn("dbo.Trainings", "CampaignId");
            DropColumn("dbo.TrainingDays", "MaxParticipants");
            DropTable("dbo.TrainingParticipants");
            RenameIndex(table: "dbo.TrainingDays", name: "IX_TrainingId", newName: "IX_Training_Id");
            RenameColumn(table: "dbo.TrainingDays", name: "TrainingId", newName: "Training_Id");
            CreateIndex("dbo.EmployeeTrainingDays", "TrainingDay_Id");
            CreateIndex("dbo.EmployeeTrainingDays", "Employee_Id");
            AddForeignKey("dbo.EmployeeTrainingDays", "TrainingDay_Id", "dbo.TrainingDays", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeTrainingDays", "Employee_Id", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
