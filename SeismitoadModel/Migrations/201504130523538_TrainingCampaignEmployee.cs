namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrainingCampaignEmployee : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TrainingParticipants", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.TrainingParticipants", new[] { "EmployeeId" });
            AddColumn("dbo.TrainingParticipants", "CampaignId", c => c.Int(nullable: false));
            CreateIndex("dbo.TrainingParticipants", new[] { "CampaignId", "EmployeeId" });
            AddForeignKey("dbo.TrainingParticipants", new[] { "CampaignId", "EmployeeId" }, "dbo.CampaignEmployees", new[] { "CampaignId", "EmployeeId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TrainingParticipants", new[] { "CampaignId", "EmployeeId" }, "dbo.CampaignEmployees");
            DropIndex("dbo.TrainingParticipants", new[] { "CampaignId", "EmployeeId" });
            DropColumn("dbo.TrainingParticipants", "CampaignId");
            CreateIndex("dbo.TrainingParticipants", "EmployeeId");
            AddForeignKey("dbo.TrainingParticipants", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
