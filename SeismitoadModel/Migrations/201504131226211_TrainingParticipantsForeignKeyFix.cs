namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrainingParticipantsForeignKeyFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TrainingParticipants", new[] { "EmployeeId", "TrainingDayId" }, "dbo.CampaignEmployees");
            DropIndex("dbo.TrainingParticipants", new[] { "EmployeeId", "TrainingDayId" });
            RenameColumn(table: "dbo.TrainingParticipants", name: "CampaignId", newName: "CampaignEmployee_CampaignId");
            DropPrimaryKey("dbo.TrainingParticipants");
            AddColumn("dbo.TrainingParticipants", "Id", c => c.Int(nullable: false, identity: true));
            RenameColumn(table: "dbo.TrainingParticipants", name: "EmployeeId", newName: "CampaignEmployee_EmployeeId");
            AddPrimaryKey("dbo.TrainingParticipants", "Id");
            //CreateIndex("dbo.TrainingParticipants", "TrainingDayId"); // gibts schon
            CreateIndex("dbo.TrainingParticipants", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" });
            AddForeignKey("dbo.TrainingParticipants", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" }, "dbo.CampaignEmployees", new[] { "CampaignId", "EmployeeId" });
        }
        
        public override void Down()
        {
            AddColumn("dbo.TrainingParticipants", "CampaignId", c => c.Int(nullable: false));
            DropForeignKey("dbo.TrainingParticipants", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" }, "dbo.CampaignEmployees");
            DropIndex("dbo.TrainingParticipants", new[] { "CampaignEmployee_CampaignId", "CampaignEmployee_EmployeeId" });
            DropIndex("dbo.TrainingParticipants", new[] { "TrainingDayId" });
            DropPrimaryKey("dbo.TrainingParticipants");
            DropColumn("dbo.TrainingParticipants", "CampaignEmployee_EmployeeId");
            DropColumn("dbo.TrainingParticipants", "Id");
            AddPrimaryKey("dbo.TrainingParticipants", new[] { "EmployeeId", "TrainingDayId" });
            RenameColumn(table: "dbo.TrainingParticipants", name: "CampaignEmployee_CampaignId", newName: "EmployeeId");
            CreateIndex("dbo.TrainingParticipants", new[] { "EmployeeId", "TrainingDayId" });
            AddForeignKey("dbo.TrainingParticipants", new[] { "EmployeeId", "TrainingDayId" }, "dbo.CampaignEmployees", new[] { "CampaignId", "EmployeeId" });
        }
    }
}
