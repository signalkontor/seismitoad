namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampaignEmployeeVisible : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE dbo.CampaignEmployees ADD VisibleForEmployee AS CAST(CASE WHEN [InformationRejected] IS NULL AND [Rejected] IS NULL AND [NotInterested] IS NULL AND [ContractCancelled] IS NULL AND [ContractRejected] IS NULL AND [ContractAccepted] IS NULL THEN 1 ELSE 0 END AS BIT) PERSISTED");
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampaignEmployees", "VisibleForEmployee");
        }
    }
}
