namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeProfileCoordsAndOldDbProjects : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "GeocodingFailed", c => c.Boolean(nullable: false));
            AddColumn("dbo.EmployeeProfiles", "ProjectsOldDbSearch", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "ProjectsOldDbSearch");
            DropColumn("dbo.EmployeeProfiles", "GeocodingFailed");
        }
    }
}
