namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameApplicants : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Applicants", newName: "CampaignEmployees");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.CampaignEmployees", newName: "Applicants");
        }
    }
}
