namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectNumber : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Campaigns", "Number", c => c.String(nullable: false, maxLength: 2, fixedLength: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Campaigns", "Number", c => c.Int(nullable: false));
        }
    }
}
