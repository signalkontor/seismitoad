namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Spatial;
    
    public partial class EmployeeProfileGeographicCoordinates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "GeographicCoordinates", c => c.Geography());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "GeographicCoordinates");
        }
    }
}
