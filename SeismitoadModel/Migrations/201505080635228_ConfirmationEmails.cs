namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConfirmationEmails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConfirmationEmails",
                c => new
                    {
                        AssignmentId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        LocationName = c.String(),
                        Street = c.String(),
                        PostcalCode = c.String(),
                        City = c.String(),
                        Filename = c.String(),
                    })
                .PrimaryKey(t => new { t.AssignmentId, t.EmployeeId });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ConfirmationEmails");
        }
    }
}
