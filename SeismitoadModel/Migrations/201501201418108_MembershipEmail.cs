namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MembershipEmail : DbMigration
    {
        public override void Up()
        {
            Sql(@"
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION GetMembershipEmail
(
	-- Add the parameters for the function here
	@UserId uniqueidentifier
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result NVARCHAR(MAX)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = (SELECT TOP 1 Email FROM seismitoad_aspnet.dbo.aspnet_Membership WHERE UserId = @UserId)

	-- Return the result of the function
	RETURN @Result

END
GO
ALTER TABLE dbo.Employees ADD
	Email  AS dbo.GetMembershipEmail([ProviderUserKey])
GO
");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "Email");
        }
    }
}
