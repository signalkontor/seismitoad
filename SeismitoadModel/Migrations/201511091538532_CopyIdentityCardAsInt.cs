namespace SeismitoadModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CopyIdentityCardAsInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EmployeeProfiles", "CopyIdentityCard", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EmployeeProfiles", "CopyIdentityCard", c => c.Boolean());
        }
    }
}
