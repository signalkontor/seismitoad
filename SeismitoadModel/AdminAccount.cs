﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public class AdminAccount : Account
    {
        public virtual ICollection<Campaign> Campaigns { get; set; }
    }
}
