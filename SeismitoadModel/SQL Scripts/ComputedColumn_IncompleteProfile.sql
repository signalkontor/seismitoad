ALTER TABLE dbo.EmployeeProfiles ADD
	Incomplete  AS (case when [Birthday] IS NULL OR nullif([PhoneMobileNo],'') IS NULL OR nullif([Street],'') IS NULL OR nullif([PostalCode],'') IS NULL OR nullif([City],'') IS NULL OR [Country] IS NULL OR [Height] IS NULL OR [ShirtSize] IS NULL OR [SoftwareKnownledge] IS NULL OR [HardwareKnownledge] IS NULL OR [ValidTradeLicense] IS NULL OR [DriversLicense] IS NULL OR [LanguagesGerman] IS NULL then (1) else (0) end) PERSISTED 
GO
