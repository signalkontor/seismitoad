/****** Object:  Table [dbo].[DeferredMails]    Script Date: 05.02.2016 11:25:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeferredMails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[InProgress] [bit] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Type] [nchar](7) NOT NULL,
	[AssignmentIds] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_DeferredMails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]