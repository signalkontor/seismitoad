USE [seismitoad_master]
GO

ALTER TABLE [dbo].[EmployeeProfiles]
ADD [IsExternal]
AS (isnull(CONVERT([bit],case when [ExternalUntil]>getdate() then (1) else (0) end,(0)),(0)))