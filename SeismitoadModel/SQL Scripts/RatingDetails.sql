/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.RatingDetails
	(
	Id int NOT NULL,
	Appearance01 decimal(18, 2) NULL,
	Appearance02 decimal(18, 2) NULL,
	Appearance03 decimal(18, 2) NULL,
	Appearance04 decimal(18, 2) NULL,
	Appearance05 decimal(18, 2) NULL,
	Appearance06 decimal(18, 2) NULL,
	Appearance07 decimal(18, 2) NULL,
	Appearance08 decimal(18, 2) NULL,
	Appearance09 decimal(18, 2) NULL,
	CustomerDialog01 decimal(18, 2) NULL,
	CustomerDialog02 decimal(18, 2) NULL,
	CustomerDialog03 decimal(18, 2) NULL,
	CustomerDialog04 decimal(18, 2) NULL,
	CustomerDialog05 decimal(18, 2) NULL,
	CustomerDialog06 decimal(18, 2) NULL,
	CustomerDialog07 decimal(18, 2) NULL,
	CustomerDialog08 decimal(18, 2) NULL,
	CustomerDialog09 decimal(18, 2) NULL,
	CustomerDialog10 decimal(18, 2) NULL,
	KnowledgeAndSales01 decimal(18, 2) NULL,
	KnowledgeAndSales02 decimal(18, 2) NULL,
	KnowledgeAndSales03 decimal(18, 2) NULL,
	KnowledgeAndSales04 decimal(18, 2) NULL,
	KnowledgeAndSales05 decimal(18, 2) NULL,
	KnowledgeAndSales06 decimal(18, 2) NULL,
	KnowledgeAndSales07 decimal(18, 2) NULL,
	KnowledgeAndSales08 decimal(18, 2) NULL,
	KnowledgeAndSales09 decimal(18, 2) NULL,
	KnowledgeAndSales10 decimal(18, 2) NULL,
	Engagement01 decimal(18, 2) NULL,
	Engagement02 decimal(18, 2) NULL,
	Engagement03 decimal(18, 2) NULL,
	Engagement04 decimal(18, 2) NULL,
	Engagement05 decimal(18, 2) NULL,
	Engagement06 decimal(18, 2) NULL,
	Engagement07 decimal(18, 2) NULL,
	Reliability01 decimal(18, 2) NULL,
	Reliability02 decimal(18, 2) NULL,
	Reliability03 decimal(18, 2) NULL,
	Reliability04 decimal(18, 2) NULL,
	Reliability05 decimal(18, 2) NULL,
	Reliability06 decimal(18, 2) NULL,
	Reliability07 decimal(18, 2) NULL,
	Reliability08 decimal(18, 2) NULL,
	Reliability09 decimal(18, 2) NULL,
	Reliability10 decimal(18, 2) NULL,
	Reliability11 decimal(18, 2) NULL,
	Reliability12 decimal(18, 2) NULL,
	Teamwork01 decimal(18, 2) NULL,
	Teamwork02 decimal(18, 2) NULL,
	Teamwork03 decimal(18, 2) NULL,
	Teamwork04 decimal(18, 2) NULL,
	Teamwork05 decimal(18, 2) NULL,
	Teamwork06 decimal(18, 2) NULL,
	Teamwork07 decimal(18, 2) NULL,
	Teamwork08 decimal(18, 2) NULL,
	Teamwork09 decimal(18, 2) NULL,
	Teamleader01 bit NULL,
	Teamleader02 bit NULL,
	Teamleader03 bit NULL,
	Teamleader04 bit NULL,
	Toughness01 decimal(18, 2) NULL,
	Toughness02 decimal(18, 2) NULL,
	Toughness03 decimal(18, 2) NULL,
	AppearenceRemark01 nvarchar(MAX) NULL,
	AppearenceRemark02 nvarchar(MAX) NULL,
	CustomerDialogRemark01 nvarchar(MAX) NULL,
	CustomerDialogRemark02 nvarchar(MAX) NULL,
	KnowledgeAndSalesRemark01 nvarchar(MAX) NULL,
	KnowledgeAndSalesRemark02 nvarchar(MAX) NULL,
	EngagementRemark01 nvarchar(MAX) NULL,
	EngagementRemark02 nvarchar(MAX) NULL,
	ReliabilityRemark01 nvarchar(MAX) NULL,
	ReliabilityRemark02 nvarchar(MAX) NULL,
	ReliabilityRemark03 nvarchar(MAX) NULL,
	TeamworkRemark01 nvarchar(MAX) NULL,
	TeamworkRemark02 nvarchar(MAX) NULL,
	TeamleaderRemark01 nvarchar(MAX) NULL,
	ToughnessRemark01 nvarchar(MAX) NULL,
	PositiveAttributes nvarchar(MAX) NULL,
	PossibleImprovements nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.RatingDetails ADD CONSTRAINT
	PK_RatingDetails PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.RatingDetails SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
