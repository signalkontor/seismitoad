﻿/* Vorbereitungen: Mit diesen Statements werden die Öffnungszeiten nullable gemacht, dadurch kann man sie bei der Replikation ausklammern */

ALTER TABLE Locations ALTER COLUMN OpeningHours_MondayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_MondayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_TuesdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_TuesdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_WednesdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_WednesdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_ThursdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_ThursdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_FridayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_FridayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_SaturdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_SaturdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_SundayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHours_SundayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_MondayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_MondayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_TuesdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_TuesdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_WednesdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_WednesdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_ThursdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_ThursdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_FridayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_FridayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_SaturdayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_SaturdayEnd int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_SundayStart int NULL
ALTER TABLE Locations ALTER COLUMN OpeningHoursIncomingGoodsDepartment_SundayEnd int NULL


/* Der folgende Code ist eher als Referenz gedacht. Er sind quasi die SQL Befehle die das selbe machen,
   wie der Designer in dem man die Replikation zusammenklickt. Es sollte aber besser trotzdem der Designer
   verwendet werden, hier kann man sich aber ein paar Werte abschauen die man dort braucht. Dieser Code
   ist aber nicht aktuell... */

-- Replikationsdatenbank wird aktiviert
use master
exec sp_replicationdboption @dbname = N'seismitoad_master', @optname = N'merge publish', @value = N'true'
GO

-- Mergeveröffentlichung wird hinzugefügt
use [seismitoad_master]
exec sp_addmergepublication @publication = N'seismitoad', @description = N'Merge publication of database ''seismitoad_master'' from Publisher ''SQL2008R2EXPRES\SQLSERVER2012''.', @sync_mode = N'native', @retention = 14, @allow_push = N'true', @allow_pull = N'true', @allow_anonymous = N'true', @enabled_for_internet = N'false', @snapshot_in_defaultfolder = N'true', @compress_snapshot = N'false', @ftp_port = 21, @ftp_subdirectory = N'ftp', @ftp_login = N'anonymous', @allow_subscription_copy = N'false', @add_to_active_directory = N'false', @dynamic_filters = N'true', @conflict_retention = 14, @keep_partition_changes = N'false', @allow_synctoalternate = N'false', @validate_subscriber_info = N'HOST_NAME()', @max_concurrent_merge = 0, @max_concurrent_dynamic_snapshots = 0, @use_partition_groups = N'true', @publication_compatibility_level = N'100RTM', @replicate_ddl = 1, @allow_subscriber_initiated_snapshot = N'false', @allow_web_synchronization = N'true', @web_synchronization_url = N'https://office.signalkontor.com/SQLReplication/replisapi.dll', @allow_partition_realignment = N'true', @retention_period_unit = N'days', @conflict_logging = N'both', @automatic_reinitialization_policy = 0
GO


exec sp_addpublication_snapshot @publication = N'seismitoad', @frequency_type = 4, @frequency_interval = 14, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 1, @frequency_subday_interval = 5, @active_start_time_of_day = 500, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0, @job_login = N'SQL2008R2EXPRES\Administrator', @job_password = null, @publisher_security_mode = 1
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'sa'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'SQL2008R2EXPRES\Administrator'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'NT SERVICE\SQLAgent$SQLSERVER2012'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'NT SERVICE\Winmgmt'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'NT Service\MSSQL$SQLSERVER2012'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'NT SERVICE\SQLWriter'
GO
exec sp_grant_publication_access @publication = N'seismitoad', @login = N'distributor_admin'
GO

-- Mergeartikel werden hinzugefügt
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'AssignmentRoles', @source_owner = N'dbo', @source_object = N'AssignmentRoles', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'Employees', @source_owner = N'dbo', @source_object = N'Employees', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'true', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'State', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'StateComment', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'ImportMatchType', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'Title', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'Firstname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'Lastname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'ProviderUserKey', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Employees', @column = N'rowguid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'LocationGroups', @source_owner = N'dbo', @source_object = N'LocationGroups', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'StateProvinces', @source_owner = N'dbo', @source_object = N'StateProvinces', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'Accounts', @source_owner = N'dbo', @source_object = N'Accounts', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'none', @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'true', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'ProviderUserKey', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'Title', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'Firstname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'Lastname', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'CustomerId', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'Discriminator', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Accounts', @column = N'rowguid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'Locations', @source_owner = N'dbo', @source_object = N'Locations', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'true', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 0
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Type', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Name', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Name2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Street', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'Street2', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'PostalCode', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'City', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'StateProvince_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'LocationGroup_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'State', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_MondayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_MondayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_TuesdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_TuesdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_WednesdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_WednesdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_ThursdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_ThursdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_FridayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_FridayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_SaturdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_SaturdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_SundayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHours_SundayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_MondayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_MondayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_TuesdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_TuesdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_WednesdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_WednesdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_ThursdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_ThursdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_FridayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_FridayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_SaturdayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_SaturdayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_SundayStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'OpeningHoursIncomingGoodsDepartment_SundayEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Locations', @column = N'rowguid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'Logins', @source_owner = N'dbo', @source_object = N'Logins', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'[Campaign_Id]  = CONVERT(int, CASE WHEN ISNUMERIC(HOST_NAME()) = 1 THEN HOST_NAME() ELSE Campaign_Id END)', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 3
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'CampaignLocations', @source_owner = N'dbo', @source_object = N'CampaignLocations', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'none', @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'[Campaign_Id] = CONVERT(int, CASE WHEN ISNUMERIC(HOST_NAME()) = 1 THEN HOST_NAME() ELSE Campaign_Id END)', @vertical_partition = N'true', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 3
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'Campaign_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'Location_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'State', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'Classification', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'SalesRegion', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'Notes', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'Contact_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'CampaignLocations', @column = N'rowguid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'Assignments', @source_owner = N'dbo', @source_object = N'Assignments', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'auto', @pub_identity_range = 10000, @identity_range = 1000, @threshold = 80, @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'[CampaignLocation_CampaignId] = CONVERT(int, CASE WHEN ISNUMERIC(HOST_NAME()) = 1 THEN HOST_NAME() ELSE CampaignLocation_CampaignId END)', @vertical_partition = N'true', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 3
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'DateStart', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'DateEnd', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'EmployeeCount', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'State', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'CampaignLocation_CampaignId', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'CampaignLocation_LocationId', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'Predecessor_Id', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
exec sp_mergearticlecolumn @publication = N'seismitoad', @article = N'Assignments', @column = N'rowguid', @operation = N'add', @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'AssignedEmployees', @source_owner = N'dbo', @source_object = N'AssignedEmployees', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'none', @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 3
GO
use [seismitoad_master]
exec sp_addmergearticle @publication = N'seismitoad', @article = N'AssignedEmployeeAssignmentRoles', @source_owner = N'dbo', @source_object = N'AssignedEmployeeAssignmentRoles', @type = N'table', @description = N'', @creation_script = N'', @pre_creation_cmd = N'drop', @schema_option = 0x000000010C034FD1, @identityrangemanagementoption = N'none', @destination_owner = N'dbo', @force_reinit_subscription = 1, @column_tracking = N'false', @subset_filterclause = N'', @vertical_partition = N'false', @verify_resolver_signature = 1, @allow_interactive_resolver = N'false', @fast_multicol_updateproc = N'true', @check_permissions = 0, @subscriber_upload_options = 2, @delete_tracking = N'true', @compensate_for_errors = N'false', @stream_blob_columns = N'true', @partition_options = 3
GO

-- Mergeartikel-Joinfilter werden hinzugefügt
use [seismitoad_master]
exec sp_addmergefilter @publication = N'seismitoad', @article = N'AssignedEmployeeAssignmentRoles', @filtername = N'AssignedEmployeeAssignmentRoles_AssignedEmployees', @join_articlename = N'AssignedEmployees', @join_filterclause = N'[AssignedEmployees].[Assignment_Id] = [AssignedEmployeeAssignmentRoles].[AssignedEmployee_AssignmentId] AND [AssignedEmployees].[Employee_Id] = [AssignedEmployeeAssignmentRoles].[AssignedEmployee_EmployeeId]', @join_unique_key = 1, @filter_type = 1, @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergefilter @publication = N'seismitoad', @article = N'Employees', @filtername = N'Employees_AssignedEmployees', @join_articlename = N'AssignedEmployees', @join_filterclause = N'[AssignedEmployees].[Employee_Id] = [Employees].[Id]', @join_unique_key = 0, @filter_type = 1, @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergefilter @publication = N'seismitoad', @article = N'AssignedEmployees', @filtername = N'AssignedEmployees_Assignments', @join_articlename = N'Assignments', @join_filterclause = N'[Assignments].[Id] = [AssignedEmployees].[Assignment_Id]', @join_unique_key = 1, @filter_type = 1, @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergefilter @publication = N'seismitoad', @article = N'Locations', @filtername = N'Locations_CampaignLocations', @join_articlename = N'CampaignLocations', @join_filterclause = N'[CampaignLocations].[Location_Id] = [Locations].[Id]', @join_unique_key = 0, @filter_type = 1, @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO
use [seismitoad_master]
exec sp_addmergefilter @publication = N'seismitoad', @article = N'LocationGroups', @filtername = N'LocationGroups_Locations', @join_articlename = N'Locations', @join_filterclause = N'[Locations].[LocationGroup_Id] = [LocationGroups].[Id]', @join_unique_key = 0, @filter_type = 1, @force_invalidate_snapshot = 1, @force_reinit_subscription = 1
GO

-- Mergeabonnements werden hinzugefügt
use [seismitoad_master]
exec sp_addmergesubscription @publication = N'seismitoad', @subscriber = N'SKWS1', @subscriber_db = N'test_replication', @subscription_type = N'Pull', @sync_type = N'Automatic', @subscriber_type = N'Local', @subscription_priority = 0, @description = N'', @use_interactive_resolver = N'False'
GO
use [seismitoad_master]
exec sp_addmergesubscription @publication = N'seismitoad', @subscriber = N'SQL2008R2EXPRES\SQLSERVER2012', @subscriber_db = N'demo_replication', @subscription_type = N'Pull', @sync_type = N'Automatic', @subscriber_type = N'Local', @subscription_priority = 0, @description = N'', @use_interactive_resolver = N'False'
GO

-- Mergepartitionen für die Veröffentlichung werden hinzugefügt
use [seismitoad_master]
exec sp_addmergepartition @publication = N'seismitoad', @suser_sname = N'', @host_name = N'1'
GO
use [seismitoad_master]
exec sp_addmergepartition @publication = N'seismitoad', @suser_sname = N'', @host_name = N'12'
GO

