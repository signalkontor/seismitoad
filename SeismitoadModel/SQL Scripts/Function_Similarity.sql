USE [seismitoad_master]
GO

/****** Object:  UserDefinedFunction [dbo].[Similarity]    Script Date: 11/05/2013 09:11:47 ******/
CREATE FUNCTION [dbo].[Similarity](@input1 [nvarchar](4000), @input2 [nvarchar](4000), @method [tinyint], @containmentBias [float], @minScoreHint [float])
RETURNS [float] WITH EXECUTE AS CALLER, RETURNS NULL ON NULL INPUT
AS 
EXTERNAL NAME [Microsoft.MasterDataServices.DataQuality].[Microsoft.MasterDataServices.DataQuality.SqlClr].[Similarity]
GO


