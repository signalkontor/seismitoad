using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ObjectTemplate.Attributes;

namespace SeismitoadModel
{
    public class Experience
    {
        public int Id { get; set; }
        public ExperienceType Type { get; set; }
        public int Sector { get; set; }
        public int SectorDetails { get; set; }
        public int Task { get; set; }
        public string OtherTask { get; set; }
        [Required]
        public string Timeframe { get; set; }
        public string Title { get; set; }
        [Required]
        public string Product { get; set; }
        [Required]
        public string Brand { get; set; }
        [Required]
        public string Agency { get; set; }
        public virtual Employee Employee { get; set; }
    }

    public enum ExperienceType
    {
        [Display(Name = "Merchandising")]
        Merchandising = 100,//4
        [Display(Name = "Messe")]
        TradeShow = 120,//2
        [Display(Name = "Moderation")]
        Presentation = 150,//6
        [Display(Name = "Mystery-Shopper")]
        MysteryShopper = 175,//3
        [Display(Name = "Promotion")]
        Promotion = 300,//0
        [Display(Name = "Roadshow")]
        Roadshow = 500,//1
        [Display(Name = "Trainer")]
        Trainer = 700,//5
        [Display(Name = "Sonstiges")]
        Sonstiges = 800
    }
}