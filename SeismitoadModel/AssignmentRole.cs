﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace SeismitoadModel
{
    public class AssignmentRole
    {
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public string ShortName { get; set; }

        public string Description { get; set; }
        public bool DoesReport { get; set; }
        public bool OnlySalesReport { get; set; }

        public virtual ICollection<AssignedEmployee> AssignedEmployees { get; set; }
    }
}
