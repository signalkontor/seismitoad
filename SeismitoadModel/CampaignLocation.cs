using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ObjectTemplate.Attributes;

namespace SeismitoadModel
{
    public class CampaignLocation : ITrackChanges
    {
        #region Simple properties
        [Display(Name = "W-LAN / Internet vorhanden", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        [EnumerationName("NullYesNo")]
        public bool? NetworkConnectionAvailable { get; set; }

        //[NotMapped, Display(Name = "W-LAN / Internet vorhanden"), Visibility(ShowForDisplay = false, ShowForEdit = false)]
        // Darf komischerweise kein Property sein, da sonst ToGridModel() von Telerik Probleme macht.
        public string GetNetworkConnectionBestValue()
        {
            if (NetworkConnectionAvailable.HasValue)
                return NetworkConnectionAvailable.Value ? "Ja (Aktion)" : "Nein (Aktion)";
            if (Location.WiFiAvailable.HasValue)
                return Location.WiFiAvailable.Value ? "Ja" : "Nein";
            return "Keine Angabe";
        }

        [Display(Name = "Status", Description = "Basisdaten")]
        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [UIHint("SingleSelect")]
        public CampaignLocationState State { get; set; }

        [Display(Name = "Klassifizierung", Description = "Basisdaten")]
        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [UIHint("SingleSelect")]
        public string Classification { get; set; }

        [Display(Name = "Vertriebsregion", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public string SalesRegion { get; set; }

        [Display(Name = "Bemerkung", Description = "Basisdaten")]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [Column("Campaign_Id")]
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int CampaignId { get; set; }

        [Column("Location_Id")]
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int LocationId { get; set; }

        [Column("Contact_Id")]
        [UIHint("SingleSelect")]
        [Display(Name = "Ansprechpartner", Description = "Basisdaten")]
        public int? ContactId { get; set; }
        #endregion

        #region Navigation properties 
        [Display(Name = "Ort")]
        public virtual Location Location { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }
        #endregion
    }

    public enum CampaignLocationState
    {
        [Display(Name = "Angefragt")]
        Informed = 0,
        [Display(Name = "Best�tigt")]
        Accepted = 1,
        [Display(Name = "Abgesagt")]
        Canceled = 2,
        [Display(Name = "Offen")]
        NotSet = 100
    }
}