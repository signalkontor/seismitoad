﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public class FaxMessage
    {
        public int Id { get; set; }
        
        [Display(Name = "Absender")]
        public string Sender { get; set; }
        
        [Required, Display(Name = "Betreff")]
        public string Subject { get; set; }
        
        [Required, Display(Name = "Nachricht")]
        public string Body { get; set; }
        public string AttachementFile { get; set; }

        [Required, Display(Name = "Empfangen am")]
        public DateTime Received { get; set; }

        public virtual Assignment Assignment {get; set; }
        
        [Display(Name = "Status")]
        public FaxMessageState State { get; set; }
    }

    public enum FaxMessageState
    {
        [Display(Name = "Bearbeitet")]
        Processed = 1,
        [Display(Name = "Konnte nicht zugeordnet werden")]
        ErrorAssignmentNotFound = 2,
        [Display(Name = "Inkompatibles E-Mail-Format")]
        ErrorMessageFormat = 3
    }
}
