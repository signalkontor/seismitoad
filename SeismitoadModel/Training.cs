﻿using System.Collections.Generic;

namespace SeismitoadModel
{
    public class Training
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CampaignId { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual ICollection<TrainingDay> TrainingDays { get; set; }
    }
}
