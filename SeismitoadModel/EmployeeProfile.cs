using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace SeismitoadModel
{
    public class EmployeeProfile : ITrackChanges
    {
        public int Id { get; set; }

        #region A. Persönliche Daten
        public DateTime Birthday { get; set; }
        public string Email2 { get; set; }
        public string Skype { get; set; }
        public string Website { get; set; }
        public DateTime? ExternalUntil { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool IsExternal
        {
            get { return ExternalUntil > DateTime.Now; }
            private set { /* Needed to trick EF */ }
        }

        public string PhoneMobileNo { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public int? Country { get; set; }
        public DbGeography GeographicCoordinates { get; set; }
        public bool GeocodingFailed { get; set; }
        public string PersonalDataComment { get; set; }
        #endregion

        #region B. Body Profil und Talente
        public int? Height { get; set; }
        public int? Size { get; set; }
        public int? ShirtSize { get; set; }
        public int? JeansWidth { get; set; }
        public int? JeansLength { get; set; }
        public int? ShoeSize { get; set; }
        public int? HairColor { get; set; }
        public bool? VisibleTattoos { get; set; }
        public bool? FacialPiercing { get; set; }
        public string ArtisticAbilities { get; set; }
        public string GastronomicAbilities { get; set; }
        public string Sports { get; set; }
        public string OtherAbilities { get; set; }
        public string OtherAbilitiesText { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        public bool? SkeletonContract { get; set; }
        public bool? CopyTradeLicense { get; set; }
        public bool? ValidTradeLicense { get; set; }
        public string TaxOfficeLocation { get; set; }
        public string TaxNumber { get; set; }
        public bool? TurnoverTaxDeductible { get; set; }
        public bool? FreelancerQuestionaire { get; set; }
        public bool? HealthCertificate { get; set; }
        public bool? CopyHealthCertificate { get; set; }
        public int? CopyIdentityCard { get; set; }
        public string Comment { get; set; }
        #endregion

        #region D. Mobilität
        public bool? DriversLicense { get; set; }
        public DateTime? DriversLicenseSince { get; set; }
        public string DriversLicenseClass1 { get; set; }
        public string DriversLicenseClass2 { get; set; }
        public string DriversLicenseRemark { get; set; }
        public bool? HasDigitalDriversCard { get; set; }
        public bool? IsCarAvailable { get; set; }
        public bool? IsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        public int? Education { get; set; }
        public string Apprenticeships { get; set; }
        public string OtherCompletedApprenticeships { get; set; }
        public bool? Studies { get; set; }
        public string StudiesDetails { get; set; }
        public string WorkExperience { get; set; }
        public int? LanguagesGerman { get; set; }
        public int? LanguagesEnglish { get; set; }
        public int? LanguagesSpanish { get; set; }
        public int? LanguagesFrench { get; set; }
        public int? LanguagesItalian { get; set; }
        public int? LanguagesTurkish { get; set; }
        public string LanguagesOther { get; set; }
        public int? SoftwareKnownledge { get; set; }
        public string SoftwareKnownledgeDetails { get; set; }
        public int? HardwareKnownledge { get; set; }
        public string HardwareKnownledgeDetails { get; set; }
        public string InternalTraining { get; set; }
        public string ExternalTraining { get; set; }
        public bool? TeamleaderQualification { get; set; }
        #endregion

        #region G. Auftragswunsch
        public string PreferredTasks { get; set; }
        public string PreferredTypes { get; set; }
        public string PreferredWorkSchedule { get; set; }
        public bool? TravelWillingness { get; set; }
        public string AccommodationPossible1 { get; set; }
        public string AccommodationPossible2 { get; set; }
        #endregion

        #region Persönlichkeitstest
        public string PersonalityTestAnswers { get; set; }
        #endregion

        public string ProjectsOldDbSearch { get; set; }

        public void UpdateCoordinates()
        {
            GeoServices.UpdateCoordinates(this);
        }

        public virtual Employee Employee { get; set; }
    }
}