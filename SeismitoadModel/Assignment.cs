using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ObjectTemplate.Attributes;

namespace SeismitoadModel
{
    public class Assignment
    {
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int Id { get; set; }

        [Display(Name = "Beginn", Description = "Basisdaten")]
        public DateTime DateStart { get; set; }

        [Display(Name = "Ende", Description = "Basisdaten")]
        public DateTime DateEnd { get; set; }

        [Display(Name = "Anzahl Promoter", Description = "Basisdaten")]
        public int EmployeeCount { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Team { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string TeamShort { get; set; }

        public string Notes { get; set; }

        public int Marker { get; set; }

        [Visibility(ShowForDisplay = false)]
        public virtual Assignment Successor { get; set; }

        [Visibility(ShowForDisplay = false)]
        public virtual Assignment Predecessor { get; set; }

        [Visibility(ShowForDisplay = false)]
        public virtual FaxMessage FaxMessage { get; set; }

        /// <summary>
        /// If true the Assignment was synchronized with the CustomerInstance.
        /// Never set this property directly. Use MarkForSync() instead, which will
        /// only set Synchronized to false if it is not null. This must be done
        /// because null means the Assignment was never synchronized.
        /// </summary>
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public bool? Synchronized { get; set; }

        [Display(Name = "Status", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public AssignmentState State { get; set; }

        public bool WasPostponed { get {return State == AssignmentState.PostponedLocation || State == AssignmentState.PostponedPromoter; } }

        public void MarkForSync()
        {
            // Bei SQL Replication muss nicht synchronisiert werden.
            if (CampaignLocation.Campaign.UsesSqlReplication)
            {
                Synchronized = true;
                return;
            }

            if (Synchronized.HasValue)
                Synchronized = false;
        }

        #region Navigation properties
        public virtual CampaignLocation CampaignLocation { get; set; }
        public virtual ICollection<AssignedEmployee> AssignedEmployees { get; set; }
        #endregion

        #region Not mapped properties
        [NotMapped]
        [Visibility(ShowForDisplay = false)]
        public string TrafficLight {
            get
            {
                var trafficLight = TrafficLightEnum;
                return trafficLight.HasValue ? trafficLight.Value.ToString() : String.Empty;
            }
        }

        [NotMapped]
        [Visibility(ShowForDisplay = false)]
        private TrafficLight? TrafficLightEnum
        {
            get
            {
                if (AssignedEmployees == null || CampaignLocation == null)
                    return null;
                if (State == AssignmentState.PostponedLocation || State == AssignmentState.PostponedPromoter)
                    return SeismitoadModel.TrafficLight.purple;
                if (State == AssignmentState.Deleted || CampaignLocation.State == CampaignLocationState.Canceled)
                    return SeismitoadModel.TrafficLight.red;
                if (AssignedEmployees.Count < EmployeeCount)
                    return SeismitoadModel.TrafficLight.yellow;
                return SeismitoadModel.TrafficLight.green;
            }
        }

        public bool Editable
        {
            get
            {
                switch(State)
                {
                    case AssignmentState.Deleted:
                    case AssignmentState.PostponedLocation:
                    case AssignmentState.PostponedPromoter:
                        return false;
                    default:
                        return true;
                }
            }
        }
        #endregion
    }

    public enum TrafficLight
    {
// ReSharper disable InconsistentNaming
        purple,
        red,
        //orange,
        yellow,
        green
// ReSharper restore InconsistentNaming
    }

    public enum AssignmentState
    {
        [Display(Name = "Geplant")]
        Planned = 0,
        [Display(Name = "Verschoben (Marktwunsch)")]
        PostponedLocation = 3, // Verschoben (Marktwunsch)
        [Display(Name = "Verschoben (Promoterausfall)")]
        PostponedPromoter = 4, // 
        [Display(Name = "Ausgefallen")]
        Cancelled = 5, // Komplett ausgefallen
        [Display(Name = "Gel�scht")]
        Deleted = 6,
        [Display(Name="Position besetzt")]
        PositionActive = 7
    }
}