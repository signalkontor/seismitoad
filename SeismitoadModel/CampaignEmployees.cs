﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using ObjectTemplate.Attributes;

namespace SeismitoadModel
{
    public class CampaignEmployee : ITrackChanges
    {
        public int CampaignId { get; set; }
        public virtual Campaign Campaign { get; set; }
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Zeitpunkt des Versands des Job-Hinweises an den Promoter
        /// </summary>
        public DateTime? Invited { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Promoter bekundet hat, dass er kein Interesse hat
        /// </summary>
        public DateTime? NotInterested { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Promoter die Job-Info angefordert hat
        /// </summary>
        public DateTime? InformationRequested { get; set; }
        /// <summary>
        /// Zeitpunkt an dem die Job-Info an den Promoter gesendet wurde
        /// </summary>
        public DateTime? InformationSent { get; set; }
        /// <summary>
        /// Zeitpunkt an dem die Job-Info dem Promoter verwehrt wurde ("Diesmal kein Einsatz möglich")
        /// </summary>
        public DateTime? InformationRejected { get; set; }
        /// <summary>
        /// Zeitpunkt an dem sich der Promoter beworben hat
        /// </summary>
        public DateTime? Applied { get; set; }
        /// <summary>
        /// Zeitpunkt an dem die Bewerbung abgelehnt wurde
        /// </summary>
        public DateTime? Rejected { get; set; }
        /// <summary>
        /// Zeitpunkt an dem die Bewerbung akzeptiert wurde
        /// </summary>
        public DateTime? Accepted { get; set; }
        /// <summary>
        /// Zeitpunkt an dem die Bewerbung zurückgestellt wurde
        /// </summary>
        public DateTime? Deferred { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Projektauftrag gesendet wurde
        /// </summary>
        public DateTime? ContractSent { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Projektauftrag storniert wurde
        /// </summary>
        public DateTime? ContractCancelled { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Projektauftrag vom Promoter abgelehnt wurde
        /// </summary>
        public DateTime? ContractRejected { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der Projektauftrag vom Promoter akzeptiert wurde
        /// </summary>
        public DateTime? ContractAccepted { get; set; }
        /// <summary>
        /// Der aktuelle Status
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Zeitpunkt an dem der aktuelle Status gesetzt wurde
        /// </summary>
        public DateTime? StateSetDate { get; set; }

        public string Roles { get; set; }

        public string LocationsText { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
        public string Remark { get; set; }
        public string ContactBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool VisibleForEmployee { get; set; }
        public bool AddressChanged { get; set; }
        public bool HasAssignments { get; set; }
        public string Deployable { get; set; }

        public virtual ICollection<TrainingParticipant> TrainingParticipants { get; set; }
    }
}
