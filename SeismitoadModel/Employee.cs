using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ObjectTemplate.Attributes;
using SeismitoadShared.Models;
using System;

namespace SeismitoadModel
{
    public class Employee : Person, IHasId, ITrackChanges
    {
        public Employee()
        {
            _isNew = false;
        }

        public Employee(bool isNew)
        {
            _isNew = isNew;
        }

        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Display(Name = "Status", Description = Basisdaten)]
        [UIHint("SingleSelect")]
        [EnumerationType(typeof(EmployeeState))]
        public EmployeeState State { get; set; }

        [Display(Name = "Status Bemerkung")]
        public string StateComment { get; set; }

        [Display(Name = "Import <div class='sk-info-icon' toggle='#wnd'></div>")]
        public ImportMatchType ImportMatchType { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Email { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? LastLogin { get; set; }

        public virtual ICollection<AssignedEmployee> AssignedEmployees { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<CampaignEmployee> CampaignEmployees { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual EmployeeProfile Profile { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public bool IsNew { get { return _isNew; } }
        private readonly bool _isNew;
    }

    public enum EmployeeState
    {
        [Display(Name = "Aktiv")]
        Active = 0,
        [Display(Name = "Inaktiv")]
        Inactive = 100,
        [Display(Name = "Gesperrt")]
        Blocked = 200,
        [Display(Name = "Neu")]
        New = 300,
        [Display(Name = "Fremdpersonal")]
        External = 400,
        [Display(Name = "Gel�scht")]
        Deleted = 500
    }

    public enum ImportMatchType
    {
        [Display(Name = "Nicht importiert")]
        NoImport,

        [Display(Name = "Exakt")]
        Matching,         // Gleicher Username, gleiche E-Mail Adresse (sehr gut)
        // Der User ist uns schon bekannt und hat bei uns auch den selben Usernamen

        [Display(Name = "Benutzername (neu)")]
        MatchingUsername, // Gleicher Username, aber andere E-Mail Adresse (schlecht)
        // Wir kennen zwar einen User mit diesem Namen, aber wir k�nnen nicht
        // mit sicherheit sagen, dass es der selbe ist. Daher m�ssen wir einen
        // neuen Account anlegen.

        [Display(Name = "E-Mail-Adresse")]
        MatchingEmail,    // Gleiche E-Mail Adresse, anderer Username (gut)
        // Wir kennen den User schon, allerdings hat er bei uns einen
        // anderen Usernamen bekommen, damit muss er nun leben

        [Display(Name = "Konflikt")]
        Conflict,         // Die Suche anhand des Usernames, sowie der E-Mail Adresse hatte unterschiedliche Ergebnisse (eher schlecht)
        // Entweder der User hat bereits einen doppelten Account bei uns oder aber in unserer Datenbank
        // hat bereits jemand anderes seinen Usernamen geschnappt.
        // Wir sollten davon ausgehen, dass der Account mit der selben E-Mail Adresse der korrekte ist
        // und das der mit dem selben Usernamen einem anderem User geh�rt.

        [Display(Name = "Neu")]
        NoMatch,          // Weder unter dem Usernamen, noch der E-Mail Adresse konnte ein User gefunden werden (sehr gut)
        // Dies ist mit hoher wahrscheinlichkeit ein neuer User, es k�nnte aber auch sein, dass er bei
        // uns schon mit einem anderen Namen und anderer E-Mail Adresse hinterlegt ist. In diesem Fall
        // wird er nach dem Import doppelt vorhanden sein.
    }
}