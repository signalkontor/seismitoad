using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;
using SeismitoadShared.Constants;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class Location : IHasId, ITrackChanges, IValidatableObject
    {
        private const string Basisdaten = "Basisdaten";
        private const string Kommunikation = "Kommunikation";
        private const string Details = "Details";

        #region Constructor
        public Location()
        {
            OpeningHours = new Hours();
            OpeningHoursIncomingGoodsDepartment = new Hours();
        }
        #endregion

        #region Simple properties
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int? ExternalId { get; set; }

        [Display(Name = "Status", Description = Basisdaten)]
        [UIHint("SingleSelect")]
        [Visibility(ShowForEdit = true)]
        public LocationState State { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [UIHint("SingleSelect")]
        [Display(Name = "Typ", Description = Basisdaten)]
        [FilterType(FilterType.Equals)]
        [ToggleVisibility(new [] { "LocationGroupId" }, new [] { "1" })]
        public int? LocationTypeId { get; set; }

        [Required]
        [FilterType(FilterType.Contains)]
        [Display(Name = "Name", Description = Basisdaten)]
        public string Name { get; set; }

        [Display(Name = "Zusatz", Description = Basisdaten)]
        [FilterType(FilterType.Contains)]
        public string Name2 { get; set; }

        [Required]
        [Display(Name = "Stra�e", Description = Basisdaten)]
        [FilterType(FilterType.Contains)]
        public string Street { get; set; }

        [Display(Name = "Stra�e Zusatz", Description = Basisdaten)]
        [FilterType(FilterType.Contains)]
        public string Street2 { get; set; }

        [Required]
        [Display(Name = "PLZ", Description = Basisdaten)]
        [FilterType(FilterType.Contains)]
        [RegularExpression("D-[0-9]{5}|A-[0-9]{4}", ErrorMessage = "Ung�ltige PLZ. Bitte im Format D-XXXXX bzw. A-XXXX eingeben")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Ort", Description = Basisdaten)]
        [FilterType(FilterType.Contains)]
        public string City { get; set; }

        [UIHint("SingleSelect")]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Column("StateProvince_Id")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Bundesland", Description = Basisdaten)]
        [FilterType(FilterType.Equals)]
        public int StateProvinceId { get; set; }

        [ScriptIgnore]
        [Display(Name = "Koordinaten", Description = Basisdaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("DbGeography")]
        [FilterType(FilterType.Contains)]
        public DbGeography GeographicCoordinates { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public bool GeocodingFailed { get; set; }

        public void UpdateCoordinates()
        {
            GeoServices.UpdateCoordinates(this);
        }

        [UIHint("SingleSelect")]
        [Column("LocationGroup_Id")]
        [Display(Name = "Channel", Description = Basisdaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [FilterType(FilterType.Equals)]
        public int? LocationGroupId { get; set; }

        [Display(Name = "Bemerkung", Description = Basisdaten)]
        [DataType(DataType.MultilineText)]
        [FilterType(FilterType.Contains)]
        public string Notes { get; set; }

        [Display(Name = "Centerverwaltung - Stra�e", Description = Basisdaten)]
        public string CenterManagementStreet { get; set; }

        [Display(Name = "Centerverwaltung - PLZ", Description = Basisdaten)]
        public string CenterManagementPostalCode { get; set; }

        [Display(Name = "Centerverwaltung - Ort", Description = Basisdaten)]
        public string CenterManagementCity { get; set; }

        [Display(Name = "Telefon", Description = Kommunikation)]
        [DataType(DataType.PhoneNumber)]
        [UIHint("PhoneNumber")]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string Phone { get; set; }

        [Display(Name = "Fax", Description = Kommunikation)]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [UIHint("PhoneNumber")]
        public string Fax { get; set; }

        [EmailAddress]
        [Display(Name = "E-Mail", Description = Kommunikation)]
        [Format(StartNewColumn = true)]
        [DataType(DataType.EmailAddress)]
        [FilterType(FilterType.Contains)]
        public string Email { get; set; }

        [DataType(DataType.Url)]
        [Display(Name = "Website", Description = Kommunikation)]
        [FilterType(FilterType.Contains)]
        public string Website { get; set; }

        [Display(Name = "Besucher / Tag", Description = Details)]
        [FilterType(FilterType.Equals)]
        public int? VisitorFrequency { get; set; }

        [Display(Name = "Gesamtfl�che (in qm)", Description = Details)]
        [FilterType(FilterType.Equals)]
        public int? Area { get; set; }

        [Display(Name = "Verkaufsfl�che (in qm)", Description = Details)]
        [FilterType(FilterType.Equals)]
        public int? SellingArea { get; set; }

        [Display(Name = "Preise Aktionsfl�che", Description = Details)]
        [DataType(DataType.MultilineText)]
        [FilterType(FilterType.Contains)]
        public string CampaignAreaPrices { get; set; }

        [Display(Name = "Publikumsniveau", Description = Details)]
        [UIHint("SingleSelect")]
        [FilterType(FilterType.Equals)]
        public int? Audience { get; set; }

        [Display(Name = "Er�ffnungsjahr", Description = Details)]
        public int? Opening { get; set; }

        [Display(Name = "Einzugsgebiet (mindestens...)", Description = Details)]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? CatchmentArea { get; set; }

        [Display(Name = "Parkpl�tze (mindestens...)", Description = Details)]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? Parking { get; set; }

        [Display(Name = "Standfl�che Au�enbereich", Description = Details)]
        public bool? OutdoorArea { get; set; }

        [Display(Name = "Vorhandene Retailer", Description = Details)]
        [Format(StartNewColumn = true)]
        [DataType(DataType.MultilineText)]
        public string Retailers { get; set; }

        [Display(Name = "WLAN vorhanden", Description = Details)]
        [FilterType(FilterType.Equals)]
        public bool? WiFiAvailable { get; set; }

        [Display(Name = "Gr��e Konferenzr�ume", Description = Details)]
        [DataType(DataType.MultilineText)]
        public string ConferenceRoomSize { get; set; }

        [Display(Name = "Kosten�bernahme m�glich", Description = Details)]
        public bool? CostTransferPossible { get; set; }

        [Display(Name = "Zimmerpreise", Description = Details)]
        [DataType(DataType.MultilineText)]
        public string RoomRates { get; set; }

        [Display(Name = "Kosten Konferenztechnik", Description = Details)]
        [DataType(DataType.MultilineText)]
        public string ConferenceEquipmentCost { get; set; }

        [Display(Name = "Kosten Tagungspaket", Description = Details)]
        [DataType(DataType.MultilineText)]
        public string ConferencePackageCost { get; set; }
        #endregion

        #region Collection properties
        [Display(Name = "Aktive Projekte", Description = "Aktive Projekte", Order = 20000)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [HideSurroundingHtml]
        [UIHint("CampaignLocations")]
        public virtual ICollection<CampaignLocation> CampaignLocations { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        #endregion

        #region Navigation properties
        [Display(Name = "Bundesland")]
        public virtual StateProvince StateProvince { get; set; }
        [Display(Name = "Channel")]
        public virtual LocationGroup LocationGroup { get; set; }
        [Display(Name = "Typ")]
        public virtual LocationType LocationType { get; set; }
        [Display(Name = "Fl�chen")]
        public virtual ICollection<CampaignArea> CampaignAreas { get; set; }

        [UIHint("Hours")]
        [HideSurroundingHtml]
        [Display(Name ="�ffnungszeiten", Description = "�ffnungszeiten")]
        public Hours OpeningHours { get; set; }

        [UIHint("Hours")]
        [HideSurroundingHtml]
        [Display(Name = "�ffnungszeiten Warenannahme", Description = "�ffnungszeiten WA")]
        public Hours OpeningHoursIncomingGoodsDepartment { get; set; }
        #endregion

        #region NotMapped properties
        [NotMapped]
        [Display(Name = "Name")]
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public string FriendlyName { get { return string.Format("{0} {1} {2}", Name, Street, City); } }

        [NotMapped]
        [Display(Name = "Gr��te Fl�che")]
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int LargestCampaignArea { get { return CampaignAreas != null && CampaignAreas.Any() ? CampaignAreas.Max(e => e.Size) : 0; } }

        [NotMapped]
        public CampaignArea LargestCampaignAreaHelper { get { return CampaignAreas != null && CampaignAreas.Any() ? CampaignAreas.OrderByDescending(e => e.Size).First() : new CampaignArea { Name = "K.A.", Size = 0 }; } }

        [NotMapped]
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public string ContactsText
        {
            get
            {
                return Contacts != null
                           ? string.Join("; ", Contacts.Select(e => string.Format("{0}, {1}", e.Lastname, e.Firstname)))
                           : "";
            }
        }
        #endregion

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(LocationTypeId == 0 && LocationGroupId == null)
            {
                yield return new ValidationResult(
                        "Bitte einen Channel ausw�hlen.",
                        new[] { "LocationGroupId" });
            }
        }

        public override string ToString()
        {
            return FriendlyName;
        }
    }

    #region LocationState
    public enum LocationState
    {
        [Display(Name = "Aktiv")]
        Active = 0,
        [Display(Name = "Geschlossen")]
        Inactive = 100,
        [Display(Name = "Gesperrt")]
        Blocked = 200
    }
    #endregion
}