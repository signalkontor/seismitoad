﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class AssignedEmployee
    {
        [Column("Assignment_Id")]
        public int AssignmentId { get; set; }

        [Column("Employee_Id")]
        [UIHint("SingleSelect")]
        public int EmployeeId { get; set; }

        [Display(Name = "Anwesenheit")]
        public Attendance Attendance { get; set; }

        [Display(Name = "Anwesenheit gesetzt")]
        [Column("DateAttendanceSet")]
        public DateTime? AttendanceSetDate { get; set; }

        public DateTime? AttendanceMobileCheckInDate { get; set; }
        public DateTime? AttendanceMobileCheckOutDate { get; set; }
        public DbGeography AttendanceMobileCheckInCoordinates { get; set; }
        public DbGeography AttendanceMobileCheckOutCoordinates { get; set; }

        public Guid? AttendanceSetByUser { get; set; }

        public string AttendanceType { get; set; }

        public void SetAttendanceSetBy(Guid userId)
        {
            AttendanceType = Attendance == Attendance.Unknown ? null : "People";
            AttendanceSetByUser = userId;
        }

        public void SetAttendanceSetBy(string type)
        {
            AttendanceType = type;
            AttendanceSetByUser = null;
        }

        [Display(Name = "Bemerkung")]
        public string AttendanceComment { get; set; }

        // Dieses Property sollte eigentlich entfernt werden, da es mittlerweile überflüssig ist. Allerdings ist die Spalte Teil der Replikation und
        // da Änderungen an replizierten Spalten meistens sehr sehr viele Probleme machen lassen wir das lieber.
        public bool ContactEmployee { get; set; }

        public virtual Assignment Assignment { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ICollection<AssignmentRole> AssignmentRoles { get; set; }
    }
}
