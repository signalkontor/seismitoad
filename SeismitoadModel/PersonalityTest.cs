﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public static class PersonalityTest
    {
        private static int AsInteger(char value)
        {
            return value - '0';
        }

        public static Rating CalculatePersonalityTestRating(string personalityTestAnswers)
        {
            if (string.IsNullOrWhiteSpace(personalityTestAnswers))
                return null;

            var answers = personalityTestAnswers
                .Select(AsInteger)
                .ToArray();

            var rating = new Rating
            {
                RatingType = 1,
                Engagement = answers.Where((a, index) => EngagementIndices.Contains(index)).Sum() / 12m * 1.14m,
                Reliability = answers.Where((a, index) => ReliabilityIndices.Contains(index)).Sum() / 15m * 0.955m,
                Toughness = answers.Where((a, index) => ToughnessIndices.Contains(index)).Sum() / 12m,
                Teamwork = answers.Where((a, index) => TeamworkIndices.Contains(index)).Sum() / 13m * 1.0309m,
            };

            return rating;
        }

        // ReSharper disable UnusedMember.Global
        public static readonly int[] InvertedQuestionsIndices = { 5, 6, 9, 13, 14, 16, 22, 25, 26, 28, 29, 38, 39, 42, 45, 46, 47, 49, 50, 51 };
        // ReSharper restore UnusedMember.Global

        private static readonly int[] EngagementIndices = { 0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44 };
        private static readonly int[] ReliabilityIndices = { 3, 7, 11, 15, 19, 23, 27, 31, 35, 39, 43, 47, 49, 50, 51 };
        private static readonly int[] ToughnessIndices = { 1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45 };
        private static readonly int[] TeamworkIndices = { 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 48 };

        public static readonly string[] Questions =  {
                                    "1.	Ich bin mit meiner Leistung erst dann zufrieden, wenn ich meine Erwartungen übertreffe.",
                                    "2.	Es macht mir wenig aus zu arbeiten, während andere ihrer Freizeitaktivität nachgehen.",
                                    "3.	Wenn ich etwas plane, überlege ich zunächst, wer noch bei dem Projekt mitarbeiten könnte.",
                                    "4.	Ich bin unbekümmert und gleichgültig.",
                                    "5.	Ich stelle mich gern schwierigen Situationen, um festzustellen, wie gut ich bin.",
                                    "6.	Ich fühle mich oft hilflos und wünsche mir eine Person, die meine Probleme löst.",
                                    "7.	Bei nahezu allen Aufgaben nimmt die Bearbeitung in Gruppen mehr Zeit als nötig in Anspruch.",
                                    "8.	Unabhängig davon, wie gut ich bei der Arbeit strukturiert bin, zu Hause bin ich eher unordentlich.",
                                    "9.	Ich bin mit mir erst dann zufrieden, wenn ich außergewöhnliche Leistungen vollbringe.",
                                    "10. In arbeitsintensiven Phasen führt die Beanspruchung bei mir zu körperlichen Beschwerden.",
                                    "11. Ich versuche zu jedem, dem ich begegne, freundlich zu sein.",
                                    "12. Ich versuche Aufgaben sehr sorgfältig auszuführen, so dass sie nicht noch einmal erledigt werden müssen.",
                                    "13. Mich reizen besonders Probleme, die sehr schwierig zu lösen sind.",
                                    "14. Ich bin ein ernster Mensch und glaube, dass ich die meisten Dinge schwerer als meine Kollegen nehme.",
                                    "15. Meine Arbeit stellt mich vor allem dann zufrieden, wenn ich nicht auf die Unterstützung anderer angewiesen bin.",
                                    "16. Es ärgert mich sehr, wenn andere Leute unpünktlich sind.",
                                    "17. Wenn sich ein Vorhaben als schwierig erweist, neige ich dazu, etwas Neues anzufangen.",
                                    "18. Ich fühle mich fähig, die meisten meiner Probleme zu bewältigen.",
                                    "19. Ich kann meine Fähigkeiten vor allem in der Zusammenarbeit mit anderen voll entfalten.",
                                    "20. Lieber gelte ich als unorganisiert als dass man mir sagt, ich sei nicht kreativ.",
                                    "21. Ich setze mir bevorzugt Ziele, die ich mit Sicherheit erreichen kann.",
                                    "22. In Notsituationen bewahre ich einen kühlen Kopf.",
                                    "23. Ich bekomme häufiger Streit mit meiner Familie und meinen Kollegen.",
                                    "24. „Geschludert“ wird bei mir nicht.",
                                    "25. Ich arbeite hart um meine Ziele zu erreichen.",
                                    "26. Ich bin häufig beunruhigt über Dinge die schief gehen könnten.",
                                    "27. Mir ist es wichtig, dass ich mich bei meiner Tätigkeit nicht ständig mit anderen abstimmen muss.",
                                    "28. Wenn ich eine Verpflichtung eingehe, so kann man sich auf mich bestimmt verlassen.",
                                    "29. Ich fühle mich nicht dazu berufen in meinem Leben etwas Besonderes zu 	erreichen.",
                                    "30. Wenn ich sehr viel zu tun habe, wirke ich auf andere gereizt.",
                                    "31. Zuviel Rücksicht auf Einzelne verzögert die Ziele in Projekten und Arbeitsgruppen.",
                                    "32. Ich versuche, alle mir übertragenen Aufgaben sehr gewissenhaft zu erledigen.",
                                    "33. Gelegentlich vernachlässige ich durch das viele Arbeiten mein Privatleben.",
                                    "34. Auch wenn ich sehr hart arbeiten muss, bleibe ich gelassen.",
                                    "35. Es macht mir nichts aus, Dinge solange zu erklären, bis mein Gegenüber es wirklich verstanden hat.",
                                    "36. Ich nehme die Dinge ganz genau.",
                                    "37. Man könnte mich in gewisser Weise arbeitssüchtig nennen.",
                                    "38. Ich kann problemlos viele Stunden ohne Pause arbeiten.",
                                    "39. Ich arbeite lieber eigenständig etwas aus, als es in langen Besprechungen 	und Arbeitsgruppen zu erarbeiten.",
                                    "40. Es gibt so viele kleine Aufgaben zu erledigen, dass ich sie manchmal einfach alle liegen lasse.",
                                    "41. Auch nach sehr guten Leistungen bemühe ich mich, noch besser zu werden.",
                                    "42. Bei wichtigen Ereignissen (z. B. wichtigen Gesprächen oder Präsentationen) habe ich mich gut im Griff.",
                                    "43. Ich bin häufig mit Besprechungsergebnissen unzufrieden, weil um des Konsens willen nicht die besten Entscheidungen getroffen werden.",
                                    "44. Ich befolge strikt meine ethischen Prinzipien.",
                                    "45. Bei allem was ich tue, strebe ich nach Perfektion.",
                                    "46. Wenn ich unter starkem Stress stehe, fühle ich mich manchmal als ob ich 	zusammenbreche.",
                                    "47. Andere können sich häufig nicht gut in mich hineinversetzen, um meine Ideen zu verstehen.",
                                    "48. Ich folge lieber spontanen Einfällen, als systematisch zu planen.",
                                    "49. Ich erziele die besten Arbeitsergebnisse wenn ich alleine arbeite.",
                                    "50. Der Aufwand, den man benötigt um ein hundertprozentiges Ergebnis zu erreichen, lohnt sich meistens nicht.",
                                    "51. Ich kann mich oft schwer entschließen.",
                                    "52. Manchmal bin ich nicht so zuverlässig wie ich sein sollte."
                                };
    }
}
