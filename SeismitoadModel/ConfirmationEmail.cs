﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public class ConfirmationEmail
    {
        [Column(Order = 1), Key]
        public int AssignmentId { get; set; }
        [Column(Order = 2), Key]
        public int EmployeeId { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string LocationName { get; set; }
        public string Street { get; set; }
        public string PostcalCode { get; set; }
        public string City { get; set; }
        public string Filename { get; set; }
    }
}
