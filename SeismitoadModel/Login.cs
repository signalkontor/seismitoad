using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class Login
    {
        public int Id { get; set; }
        public Guid UserKey { get; set; }
        [Display(Name="Projektrollen")]
        public string CampaignRoles { get; set; }

        [Column("Campaign_Id")]
        public int CampaignId { get; set; }
        public virtual Campaign Campaign { get; set; }
    }
}