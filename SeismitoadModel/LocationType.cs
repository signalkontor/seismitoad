using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;

namespace SeismitoadModel
{
    public class LocationType
    {
        public int Id { get; set; }
        
        [Required] 
        public string Name { get; set; }

        public virtual ICollection<Location> Locations { get; set; }
    }
}