using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SeismitoadModel
{
    [ComplexType]
    public class Hours
    {
        public int MondayStart { get; set; }
        public int MondayEnd { get; set; }
        public int TuesdayStart { get; set; }
        public int TuesdayEnd { get; set; }
        public int WednesdayStart { get; set; }
        public int WednesdayEnd { get; set; }
        public int ThursdayStart { get; set; }
        public int ThursdayEnd { get; set; }
        public int FridayStart { get; set; }
        public int FridayEnd { get; set; }
        public int SaturdayStart { get; set; }
        public int SaturdayEnd { get; set; }
        public int SundayStart { get; set; }
        public int SundayEnd { get; set; }

        #region Helper-properties and -methods
        [NotMapped]
        [Display(Name = "Mo. Beginn")]
        public string MondayStartTime
        {
            get { return IntToTimeStr(MondayStart); }
            set { MondayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Mo. Ende")]
        public string MondayEndTime
        {
            get { return IntToTimeStr(MondayEnd); }
            set { MondayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Di. Beginn")]
        public string TuesdayStartTime
        {
            get { return IntToTimeStr(TuesdayStart); }
            set { TuesdayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Di. Ende")]
        public string TuesdayEndTime
        {
            get { return IntToTimeStr(TuesdayEnd); }
            set { TuesdayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Mi. Beginn")]
        public string WednesdayStartTime
        {
            get { return IntToTimeStr(WednesdayStart); }
            set { WednesdayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Mi. Ende")]
        public string WednesdayEndTime
        {
            get { return IntToTimeStr(WednesdayEnd); }
            set { WednesdayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Do. Beginn")]
        public string ThursdayStartTime
        {
            get { return IntToTimeStr(ThursdayStart); }
            set { ThursdayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Do. Ende")]
        public string ThursdayEndTime
        {
            get { return IntToTimeStr(ThursdayEnd); }
            set { ThursdayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Fr. Beginn")]
        public string FridayStartTime
        {
            get { return IntToTimeStr(FridayStart); }
            set { FridayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Fr. Ende")]
        public string FridayEndTime
        {
            get { return IntToTimeStr(FridayEnd); }
            set { FridayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Sa. Beginn")]
        public string SaturdayStartTime
        {
            get { return IntToTimeStr(SaturdayStart); }
            set { SaturdayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "Sa. Ende")]
        public string SaturdayEndTime
        {
            get { return IntToTimeStr(SaturdayEnd); }
            set { SaturdayEnd = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "So. Beginn")]
        public string SundayStartTime
        {
            get { return IntToTimeStr(SundayStart); }
            set { SundayStart = TimeStrToInt(value); }
        }

        [NotMapped]
        [Display(Name = "So. Ende")]
        public string SundayEndTime
        {
            get { return IntToTimeStr(SundayEnd); }
            set { SundayEnd = TimeStrToInt(value); }
        }

        private static int TimeStrToInt(string time)
        {
            if (string.IsNullOrWhiteSpace(time))
                return 0;
            var colonPos = time.IndexOf(':');
            if (colonPos == -1)
                return 0;

            var h = time.Substring(0, colonPos);
            var m = time.Substring(colonPos + 1);
            return int.Parse(h) * 60 + int.Parse(m);
        }

        private static string IntToTimeStr(int time)
        {
            return string.Format("{0:t}", new DateTime().AddMinutes(time));
        }

        private void Extract(DayOfWeek dayOfWeek, out int start, out int end)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = MondayStart;
                    end = MondayEnd;
                    break;
                case DayOfWeek.Tuesday:
                    start = TuesdayStart;
                    end = TuesdayEnd;
                    break;
                case DayOfWeek.Wednesday:
                    start = WednesdayStart;
                    end = WednesdayEnd;
                    break;
                case DayOfWeek.Thursday:
                    start = ThursdayStart;
                    end = ThursdayEnd;
                    break;
                case DayOfWeek.Friday:
                    start = FridayStart;
                    end = FridayEnd;
                    break;
                case DayOfWeek.Saturday:
                    start = SaturdayStart;
                    end = SaturdayEnd;
                    break;
                case DayOfWeek.Sunday:
                    start = SundayStart;
                    end = SundayEnd;
                    break;
                default:
                    start = 0;
                    end = 0;
                    break;
            }
        }

        public string ToString(DayOfWeek dayOfWeek)
        {
            int start, end;
            Extract(dayOfWeek, out start, out end);
            return string.Format("{0:t} - {1:t}", new DateTime().AddMinutes(start), new DateTime().AddMinutes(end));
        }

        public void BestOpeningHours(ref DateTime startDate, ref DateTime endDate)
        {
            int tmpStart, tmpEnd;
            Extract(startDate.DayOfWeek, out tmpStart, out tmpEnd);
            var start = startDate.Date.AddMinutes(tmpStart);
            if(start > startDate)
            {
                startDate = start;
            }
            var end = endDate.Date.AddMinutes(tmpEnd);
            if(end > start && end < endDate)
            {
                endDate = end;
            }
        }
        #endregion

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .Append("Mo: ")
                .Append(MondayStartTime)
                .Append("-")
                .Append(MondayEndTime)
                .Append(";")

                .Append("Di: ")
                .Append(TuesdayStartTime)
                .Append("-")
                .Append(TuesdayEndTime)
                .Append(";")

                .Append("Mi: ")
                .Append(WednesdayStartTime)
                .Append("-")
                .Append(WednesdayEndTime)
                .Append(";")

                .Append("Do: ")
                .Append(ThursdayStartTime)
                .Append("-")
                .Append(ThursdayEndTime)
                .Append(";")


                .Append("Fr: ")
                .Append(FridayStartTime)
                .Append("-")
                .Append(FridayEndTime)
                .Append(";")

                .Append("Sa: ")
                .Append(SaturdayStartTime)
                .Append("-")
                .Append(SaturdayEndTime)
                .Append(";")

                .Append("So: ")
                .Append(SundayStartTime)
                .Append("-")
                .Append(SundayEndTime)
                .Append(";");

            return stringBuilder.ToString();
        }
    }
}