using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class Customer : IHasId
    {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required]
        public string City { get; set; }

        public virtual ICollection<Campaign> Campaigns { get; set; }
        public virtual ICollection<CustomerAccount> CustomerAccounts { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class CustomerAccount : Account
    {
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}