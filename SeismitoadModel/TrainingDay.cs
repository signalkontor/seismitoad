﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadModel
{
    public class TrainingDay
    {
        public int Id { get; set; }
        public int TrainingId { get; set; }

        [UIHint("KendoDateTime")]
        public DateTime DateFrom { get; set; }

        [UIHint("KendoDateTime")]
        public DateTime DateTil { get; set; }

        public string Location { get; set; }
        public string Remarks { get; set; }

        public int MaxParticipants { get; set; }

        public virtual Training Training { get; set; }
        public virtual ICollection<TrainingParticipant> Participants { get; set; }
    }
}