using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;

namespace SeismitoadModel.DatabaseContext
{
    public static class ChangeLogger
    {
        public static Func<DbContext> DbContext { private get; set; }
        public static Func<Guid?> UserKey { private get; set; }

        public static void Log(SeismitoadDbContext dbContext)
        {
            var userKey = UserKey();
            if (!userKey.HasValue)
                return;

            dbContext.ChangeTracker.DetectChanges();
            var objectContext = ((IObjectContextAdapter) dbContext).ObjectContext;

            foreach (var objectStateEntry in objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified))
            {
                if (objectStateEntry.Entity == null || !(objectStateEntry.Entity is ITrackChanges) || objectStateEntry.IsRelationship)
                    continue;

                var isCompositeKey = objectStateEntry.EntityKey.EntityKeyValues.Length > 1;
                var keyString = isCompositeKey
                        ? string.Join(";", objectStateEntry.EntityKey.EntityKeyValues.Select(e => string.Format("{0}={1}", e.Key, e.Value)))
                        : objectStateEntry.EntityKey.EntityKeyValues[0].Value.ToString();

                var changeSet = new ChangeSet
                                    {
                                        UserId = userKey.Value,
                                        TableName = objectStateEntry.EntitySet.Name,
                                        EntityId = keyString
                                    };

                var modifiedProps = objectStateEntry.GetModifiedProperties();
                var entry1 = objectStateEntry;
                var reallyModifiedProps = from p in modifiedProps
                                          where string.Compare("" + entry1.CurrentValues[p], "" + entry1.OriginalValues[p], true) != 0
                                          select p;

                foreach (var propName in reallyModifiedProps)
                {
                    var currentValue = objectStateEntry.CurrentValues[propName];
                    var originalValue = objectStateEntry.OriginalValues[propName];
                    string oldValueDisplay = null;
                    string newValueDisplay = null;

                    var navPropName = propName.EndsWith("Id") ? propName.Substring(0, propName.Length - 2) : propName;
                    var type = objectStateEntry.Entity.GetType();
                    var dbSetType = type.Namespace == "System.Data.Entity.DynamicProxies"
                                        ? type.BaseType
                                        : type;
                    var propertyInfo = type.GetProperty(navPropName);
                    if (propertyInfo.GetCustomAttributes(typeof (DatabaseGeneratedAttribute)).Any())
                    {
                        continue;
                    }
                    if ((!string.IsNullOrEmpty(navPropName) || navPropName != propName) && objectStateEntry.Entity != null && propertyInfo != null)
                    {
                        var propertyValue = propertyInfo.GetValue(objectStateEntry.Entity, null);
                        newValueDisplay = propertyValue?.ToString() ?? "[Kein Wert]";

                        using (var unchangedDbContext = DbContext())
                        {
                            // Hier steht extra kein try/catch, damit man mitbekommt, das irgendwas hier falsch ist ;-)
                            var originalEntity =
                                unchangedDbContext.Set(dbSetType)
                                    .Find(objectStateEntry.EntityKey.EntityKeyValues.Select(e => e.Value).ToArray());
                            propertyValue = propertyInfo.GetValue(originalEntity, null);
                            oldValueDisplay = propertyValue?.ToString() ?? "[Kein Wert]";
                        }
                    }
                    var change = new Change
                                        {
                                            PropName = propName,
                                            OldValueRaw = originalValue.ToString(),
                                            NewValueRaw = currentValue.ToString(),
                                            OldValueDisplay = oldValueDisplay,
                                            NewValueDisplay = newValueDisplay,
                                            ChangeSet = changeSet
                                        };
                            
                    changeSet.Changes.Add(change);
                }
                if (changeSet.Changes.Any())
                    dbContext.ChangeSets.Add(changeSet);
            }
        }
    }
}