﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using SeismitoadShared.Constants;

namespace SeismitoadModel.DatabaseContext
{
    public class SeismitoadDbContext : DbContext
    {
        public SeismitoadDbContext() : base() { }
        public SeismitoadDbContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        // ReSharper disable UnusedAutoPropertyAccessor.Global
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AdminAccount> AdminAccounts { get; set; }
        public virtual DbSet<CampaignEmployee> CampaignEmployees { get; set; }
        public virtual DbSet<Assignment> Assignments { get; set; }
        public virtual DbSet<AssignedEmployee> AssignedEmployees { get; set; }
        public virtual DbSet<AssignmentRole> AssignmentRoles { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<CampaignArea> CampaignAreas { get; set; }
        public virtual DbSet<CampaignLocation> CampaignsLocations { get; set; }
        public virtual DbSet<Change> Changes { get; set; }
        public virtual DbSet<ChangeSet> ChangeSets { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerAccount> CustomerAccounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeProfile> EmployeeProfiles { get; set; }
        public virtual DbSet<Experience> Experiences { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationGroup> LocationGroups { get; set; }
        public virtual DbSet<LocationType> LocationTypes { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<OutstandingNotification> OutstandingNotifications { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }
        public virtual DbSet<StateProvince> StateProvinces { get; set; }
        public virtual DbSet<Training> Trainings { get; set; }
        public virtual DbSet<TrainingDay> TrainingDays { get; set; }
        public virtual DbSet<TrainingParticipant> TrainingParticipants { get; set; }
        public virtual DbSet<FaxMessage> FaxMessages { get; set; }
        public virtual DbSet<ConfirmationEmail> ConfirmationEmails { get; set; }
        public virtual DbSet<ViewAssignment> ViewAssignments { get; set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Global

        /// <summary>
        /// THIS IS NOT THREAD SAFE.
        /// </summary>
        public bool RecompileAllQueries { get; set; }

        public override int SaveChanges()
        {
            ChangeLogger.Log(this);
            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Alert>().HasRequired(e => e.UserInCharge).WithMany(e => e.Alerts).WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>().HasKey(e => e.ProviderUserKey);

            modelBuilder.Entity<Assignment>().HasRequired(e => e.CampaignLocation).WithMany(e => e.Assignments).WillCascadeOnDelete(false);
            modelBuilder.Entity<Assignment>().HasMany(e => e.AssignedEmployees).WithRequired(e => e.Assignment).HasForeignKey(e => e.AssignmentId);
            modelBuilder.Entity<Assignment>().HasOptional(e => e.Successor).WithOptionalPrincipal(e => e.Predecessor);

            modelBuilder.Entity<AssignedEmployee>().HasKey(e => new { e.AssignmentId, e.EmployeeId });
            modelBuilder.Entity<AssignedEmployee>().HasRequired(e => e.Employee).WithMany(e => e.AssignedEmployees).HasForeignKey(e => e.EmployeeId);
            modelBuilder.Entity<AssignedEmployee>().HasMany(e => e.AssignmentRoles).WithMany(e => e.AssignedEmployees);

            modelBuilder.Entity<Campaign>().HasMany(e => e.Logins).WithRequired(e => e.Campaign).HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<Campaign>().HasRequired(e => e.Customer).WithMany(e => e.Campaigns).HasForeignKey(e => e.CustomerId);
            modelBuilder.Entity<Campaign>().HasMany(e => e.CampaignLocations).WithRequired(e => e.Campaign).HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<Campaign>().HasMany(e => e.Ratings).WithOptional(e => e.Campaign).HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<Campaign>().HasRequired(e => e.Manager).WithMany(e => e.Campaigns).HasForeignKey(e => e.ManagerId).WillCascadeOnDelete(false);

            modelBuilder.Entity<CampaignEmployee>().HasMany(e => e.Locations).WithMany();
            modelBuilder.Entity<CampaignEmployee>().HasRequired(e => e.Campaign).WithMany(e => e.Applicants).HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<CampaignEmployee>().HasRequired(e => e.Employee).WithMany(e => e.CampaignEmployees).HasForeignKey(e => e.EmployeeId);
            modelBuilder.Entity<CampaignEmployee>().HasKey(e => new { e.CampaignId, e.EmployeeId });

            modelBuilder.Entity<CampaignLocation>().HasKey(e => new {e.CampaignId, e.LocationId});
            modelBuilder.Entity<CampaignLocation>().HasRequired(e => e.Location).WithMany(e => e.CampaignLocations).HasForeignKey(e => e.LocationId);
            modelBuilder.Entity<CampaignLocation>().HasOptional(e => e.Contact).WithMany(e => e.CampaignLocations).HasForeignKey(e => e.ContactId);

            modelBuilder.Entity<ChangeSet>().HasMany(e => e.Changes).WithRequired(e => e.ChangeSet);

            modelBuilder.Entity<CustomerAccount>().HasRequired(e => e.Customer).WithMany(e => e.CustomerAccounts).HasForeignKey(e => e.CustomerId).WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>().HasOptional(e => e.Profile).WithRequired(e => e.Employee);
            modelBuilder.Entity<Employee>().HasMany(e => e.Ratings).WithRequired(e => e.Employee).HasForeignKey(e => e.EmployeeId);
            modelBuilder.Entity<Employee>().HasMany(e => e.Experiences).WithRequired(e => e.Employee);

            modelBuilder.Entity<Location>().HasMany(e => e.CampaignAreas).WithRequired(e => e.Location).HasForeignKey(e => e.LocationId);
            modelBuilder.Entity<Location>().HasMany(e => e.Contacts).WithRequired(e => e.Location).HasForeignKey(e => e.LocationId);
            modelBuilder.Entity<Location>().HasRequired(e => e.StateProvince).WithMany(e => e.Locations).HasForeignKey(e => e.StateProvinceId);
            modelBuilder.Entity<Location>().HasOptional(e => e.LocationGroup).WithMany(e => e.Locations).HasForeignKey(e => e.LocationGroupId);
            modelBuilder.Entity<Location>().HasOptional(e => e.LocationType).WithMany(e => e.Locations).HasForeignKey(e => e.LocationTypeId);

            modelBuilder.Entity<OutstandingNotification>().HasRequired(e => e.Campaign).WithMany().HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<OutstandingNotification>().HasRequired(e => e.Employee).WithMany().HasForeignKey(e => e.EmployeeId);

            modelBuilder.Entity<FaxMessage>().HasOptional(s => s.Assignment).WithOptionalDependent(o => o.FaxMessage);

            modelBuilder.Entity<Training>().HasRequired(e => e.Campaign).WithMany(e => e.Trainings).HasForeignKey(e => e.CampaignId);
            modelBuilder.Entity<Training>().HasMany(e => e.TrainingDays).WithRequired(e => e.Training).HasForeignKey(e => e.TrainingId);
            modelBuilder.Entity<TrainingDay>().HasMany(e => e.Participants).WithRequired(e => e.TrainingDay).HasForeignKey(e => e.TrainingDayId);
            modelBuilder.Entity<TrainingParticipant>()
                .HasRequired(e => e.CampaignEmployee)
                .WithMany(e => e.TrainingParticipants)
                .WillCascadeOnDelete(false);

            // Auch wenn die View keinen Primärschlüssel hat, braucht das EF einen. Da per Konvention "Id" der Primärschlüssel ist, dies
            // aber kein eindeutiger Schlüssel ist, führt das zu Verfälschungen der Daten. Das liegt daran, dass EF Zeilen mit selben Schlüssel
            // für gleich hält und daher auf das selbe Objekt mappt, was leider völlig falsch an dieser Stelle wäre.
            modelBuilder.Entity<ViewAssignment>().ToTable("vw_Assignments").HasKey(e => new { e.Id, e.EmployeeId });
        }

        public IQueryable<Assignment> ActiveAssignments
        {
            get { return Assignments.Where(e => e.State == AssignmentState.Planned); }
        }

        public IQueryable<Location> ActiveLocations
        {
            get { return Locations.Where(e => e.State == LocationState.Active); }
        }

        public IQueryable<Employee> ActiveEmployees { get { return Employees.Where(e => e.State == EmployeeState.Active || e.State == EmployeeState.New); } }

        public IQueryable<Employee> AvailableEmployees { get { return Employees.Where(e => e.State == EmployeeState.Active || e.State == EmployeeState.New || e.State == EmployeeState.External); } }


        /// <summary>
        /// Gets assignmnets which start/end intersects the given dateStart and dateEnd
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        private IQueryable<Assignment> GetAssignments(DateTime dateStart, DateTime dateEnd)
        {
            return Assignments.Where(e => dateStart < e.DateEnd && e.DateStart < dateEnd && e.State == AssignmentState.Planned);
        }

        public IEnumerable<Assignment> GetConflictingAssignments(DateTime dateStart, DateTime dateEnd, int employeeId, int assignmentId = -1)
        {
            return GetAssignments(dateStart, dateEnd)
                .Where(e => e.Id != assignmentId && e.AssignedEmployees.Any(i => i.EmployeeId == employeeId));
        }

        public IEnumerable<Assignment> GetConflictingAssignmentsForCampaignLocation(DateTime dateStart, DateTime dateEnd, int campaignId, int locationId)
        {
            return GetAssignments(dateStart, dateEnd)
                .Where(e => e.CampaignLocation.CampaignId == campaignId && e.CampaignLocation.LocationId == locationId);
        }

        public string GetTableName<T>()
        {
            var type = typeof (T);
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            var entityContainer = objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName,
                                                                       DataSpace.CSpace);
            return entityContainer.BaseEntitySets.First(e => e.ElementType.Name == type.Name).Name;
        }

        public Type GetEntityType(string tableName)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            var entityContainer = objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName,
                                                                       DataSpace.CSpace);

            var typeName = entityContainer.BaseEntitySets.First(e => e.Name == tableName).ElementType.Name;
            return Type.GetType("SeismitoadModel." + typeName);
        }
    }
}