﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public static class UpdateCollectionExtension
    {
        public static void ReplaceWith<T>(this ICollection<T> toReplace, IEnumerable<T> with)
        {
            var toRemove = toReplace.Except(with);
            var toAdd = with.Except(toReplace);
            foreach (var item in toRemove.ToList())
            {
                toReplace.Remove(item);
            }
            foreach (var item in toAdd.ToList())
            {
                toReplace.Add(item);
            }
        }
    }
}
