﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadModel
{
    public class OutstandingNotification
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public Guid DataId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int EmployeeId { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
        public string AssignmentIds { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
