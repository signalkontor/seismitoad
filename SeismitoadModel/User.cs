﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeismitoadModel
{
    public class User : ITrackChanges
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public virtual ICollection<Alert> Alerts { get; set; }
    }
}
