﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using ObjectTemplate.Attributes;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class CampaignArea : IHasId
    {
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int Id { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        [Column("Location_Id")]
        public int LocationId { get; set; }

        [Display(Name = "Fläche (in qm)", Description = "Aktionsfläche")]
        public int Size { get; set; }

        [Display(Name = "Bezeichnung", Description = "Aktionsfläche")]
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("{0} qm ({1})", Size, Name);
        }

        public virtual Location Location { get; set; }
    }
}
