﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace SeismitoadModel
{
    public static class GeoServices
    {
        public static void UpdateCoordinates(EmployeeProfile employee)
        {
            var coords = GetCoordinates(employee.Street, employee.City, employee.PostalCode);

            if (coords != null && coords.Length == 1)
            {
                // Das heißt wir haben das Google Maps Query Limit für erreicht.
                return;
            }
            var coordinates = ToWellKnownString(coords);
            if(coordinates != null)
            {
                employee.GeographicCoordinates = DbGeography.FromText(coordinates, 4326);
                employee.GeocodingFailed = false;
            }
            else
            {
                employee.GeocodingFailed = true;
            }
        }

        public static void UpdateCoordinates(Location location)
        {
            var coords = GetCoordinates(location.Street, location.City, location.PostalCode);

            if (coords != null && coords.Length == 1)
            {
                // Das heißt wir haben das Google Maps Query Limit für erreicht.
                return;
            }
            var coordinates = ToWellKnownString(coords);
            if (coordinates != null)
            {
                location.GeographicCoordinates = DbGeography.FromText(coordinates, 4326);
                location.GeocodingFailed = false;
            }
            else
            {
                location.GeocodingFailed = true;
            }
        }

        private static string ToWellKnownString(string[] geocoderResult)
        {
            if (geocoderResult == null || geocoderResult.Length != 2)
                return null;

            return string.Format("POINT ({0} {1})", geocoderResult[0], geocoderResult[1]);
        }

        private static string[] GetCoordinates(string street, string city, string postalCode)
        {
            var address = $"{postalCode} {city} {street}";
            var query = $"https://maps.googleapis.com/maps/api/geocode/xml?address={Uri.EscapeDataString(address)}&sensor=false&key=AIzaSyAsIFWtNinSNwN1aECKedS_yjqtojTjCqw";
            var document = XDocument.Load(query);

            var response = document.Element("GeocodeResponse");
            if (response == null)                
                return null;

            var status = response.Element("status");
            if (status != null && status.Value == "OVER_QUERY_LIMIT")
                return new[] { "OVER_QUERY_LIMIT" };

            if (status == null || status.Value != "OK")
                return null;

            var result = response.Element("result");
            if (result == null)
                return null;

            var geometry = result.Element("geometry");
            if (geometry == null)
                return null;
            
            var location = geometry.Element("location");
            if (location == null)
                return null;

            var lng = location.Element("lng");
            var lat = location.Element("lat");

            if (lng != null && lat != null)
            {
                return new[] {lng.Value, lat.Value};
            }
            return null;
        }
    }
}
