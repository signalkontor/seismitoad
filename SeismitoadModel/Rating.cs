﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ObjectTemplate.Attributes;

namespace SeismitoadModel
{
    /// <summary>
    /// Je 5 Unterkategorien zu den Kategorien Engagement, Zuverlässigkeit, Belastbarkeit, Teamfähigkeit, Kundendialog, Erscheinug definieren
    /// Es kann Projekte geben, bei denen nicht alle Kriterien ausgefüllt werden müssen.
    /// Je Kategorie erhält der Promotor dann die Durchschnittsnote all seiner Bewertungen.
    /// Bewertungen werden über den Selftest, Misterie-Shopper, PER Media durchgeführt
    ///
    /// Alle Kennzahlen werden in einer gesonderten Tabelle geschrieben
    /// Es wird eine Verlinkung zum Projekt gespeichert
    /// Es kann noch ein Kommentar hinzugefügt werden
    /// </summary>
    public class Rating
    {
        public int Id { get; set; }
        public decimal? Engagement { get; set; }
        public decimal? Reliability { get; set; }
        public decimal? Toughness { get; set; }
        public decimal? Teamwork { get; set; }
        public decimal? Customerdialog { get; set; }
        public decimal? Appearance { get; set; }
        public decimal? KnowledgeAndSales { get; set; }
        public string Comment { get; set; }
        public int RatingType { get; set; }
        [Column("Campaign_Id")]
        public int? CampaignId { get; set; }
        [Column("Employee_Id")]
        public int EmployeeId { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
