using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadModel
{
    public class LocationGroup
    {
        [Display(Name = "Channel")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Channel")]
        public string Name { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}