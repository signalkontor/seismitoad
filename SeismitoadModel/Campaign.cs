using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using ObjectTemplate.Attributes;
using SeismitoadShared.Constants;
using SeismitoadShared.Models;


namespace SeismitoadModel
{
    public class Campaign : IHasId, ITrackChanges
    {
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Column("Customer_Id")]
        [Display(Name = "Kunde", Description = "Basisdaten")]
        public int? CustomerId { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2)]
        [Display(Name = "Projektnummer", Description = "Basisdaten")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Projektleiter", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public Guid ManagerId { get; set; }

        [Required]
        [Display(Name = "Projekttitel", Description = "Basisdaten")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Inhalt", Description = "Basisdaten")]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true)]
        public string Description { get; set; }

        [Display(Name = "Adresse des Report-Tools", Description = "Basisdaten")]
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        [DataType(DataType.Url)]
        public string InstanceUrl { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Display(Name = "Teamgr��e pro Aktionsort", Description = "Basisdaten")]
        public int EmployeesPerAssignment { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [Display(Name = "Status", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public CampaignState State { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [Display(Name = "Ausschreibung aktiv?", Description = "Basisdaten")]
        [UIHint("Bool")]
        public bool IsApplyable { get; set; }

        public bool GradualBooking { get; set; }

        [Display(Name = "Aktionszeitraum", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Duration { get; set; }

        [Display(Name = "AP Kunde", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Contact { get; set; }

        [Display(Name = "Aktionstage", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Days { get; set; }

        [Display(Name = "Aktionsorte", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Locations { get; set; }

        [Display(Name = "Anforderungsprofil", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true)]
        public string Requirements { get; set; }

        [Display(Name = "Aktionszeiten", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Times { get; set; }

        [Display(Name = "Budget", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Budget { get; set; }

        [Display(Name = "Klassifizierung der Aktionsorte", Description = "Zusatzinformationen")]
        [Format(HintText = "z.B. A: Aktionstage Do, Fr, Sa, Mo; B: Aktionstage Fr, Sa, Mo")]
        [DataType(DataType.MultilineText)]
        public string Classification { get; set; }

        [Display(Name = "Angebotssumme", Description = "Bezahlung")]
        [DataType(DataType.MultilineText)]
        public string BidSum { get; set; }

        [Display(Name = "Zahlungsziele", Description = "Bezahlung")]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true)]
        public string PaymentTargets { get; set; }

        [NotMapped]
        [Display(Name = "Bilder", Description = "Dateien")]
        [UIHint("CampaignPictures")]
        //[HideSurroundingHtml]
        public string DummyForPictures { get; set; }

        [NotMapped]
        [Display(Name = "Information", Description = "Dateien")]
        [UIHint("CampaignDetailedInfo")]
        [Format(StartNewColumn = true)]
        //[HideSurroundingHtml]
        public string DetailedJobInfo { get; set; }

        #region Ansprechpartner

        [NotMapped]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Per Media Ansprechpartner", Description = "ASP")]
        public string LabelPM { get; set; }

        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName1 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone1 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail1 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark1 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName2 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone2 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail2 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark2 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName3 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone3 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail3 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark3 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName4 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone4 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail4 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark4 { get; set; }


        [NotMapped]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Advantage Ansprechpartner", Description = "ASP")]
        public string LabelAD { get; set; }

        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName1 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone1 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail1 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark1 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName2 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone2 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail2 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark2 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName3 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone3 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail3 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark3 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName4 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone4 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail4 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark4 { get; set; }

        #endregion

        public virtual AdminAccount Manager { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<Login> Logins { get; set; }
        public virtual ICollection<CampaignLocation> CampaignLocations { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<CampaignEmployee> Applicants { get; set; }
        [NotMapped] public virtual ICollection<Training> Trainings { get; set; }

        public override string ToString()
        {
            return Name;
        }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public bool UsesSqlReplication { get; set; }
    }

    public enum CampaignState
    {
        [Display(Name = "Erstellt")]
        Created = 0,
        [Display(Name = "Personal Anforderung")]
        StaffRequirments = 100,
        [Display(Name = "Personal Suche")]
        StaffSearch = 200,
        [Display(Name = "Einsatzplanung")]
        AssignmentPlanning = 300,
        [Display(Name = "Laufend")]
        Running = 500,
        [Display(Name = "Archiviert")]
        Archived = 1000,
        [Display(Name = "Gel�scht")]
        Deleted = 1100
    }
}