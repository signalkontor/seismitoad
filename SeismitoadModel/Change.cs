using System.ComponentModel.DataAnnotations;
using ObjectTemplate.Attributes;
using SeismitoadShared.Models;

namespace SeismitoadModel
{
    public class Change : IHasId
    {
        #region Simple properties
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Required]
        public string PropName { get; set; }
        public string OldValueRaw { get; set; }
        public string NewValueRaw { get; set; }
        public string OldValueDisplay { get; set; }
        public string NewValueDisplay { get; set; }
        #endregion

        #region Navigation properties
        public virtual ChangeSet ChangeSet { get; set; }
        #endregion
    }

}