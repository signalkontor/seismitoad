using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;
using SeismitoadModel.DatabaseContext;
using AssignmentRequestInstance.Models;

namespace AssignmentRequestInstance
{
    public class SelectListRepository : ObjectTemplate.SelectListRepository
    {
        protected override IEnumerable<SelectListItem> GetSpecial(string propertyName, Type type = null)
        {
            switch(propertyName)
            {
                case "AssignmentRoleIds":
                    using (var DB = new SeismitoadDbContext())
                    {
                        return DB.AssignmentRoles.OrderBy(e => e.Id).ToList().Select(e => new SelectListItem
                        {
                            Text = string.Format("{0} ({1})", e.Name, e.ShortName),
                            Value = e.Id.ToString(CultureInfo.InvariantCulture),
                            Selected = e.Id == 1
                        });
                    }
                case "UserCreated":
                    using (var DB = new SeismitoadDbContext())
                    {
                        var campaignId = Convert.ToInt16(ConfigurationManager.AppSettings["CampaignId"]);
                        var salesRegions = DB.CampaignsLocations.Where(cl => cl.CampaignId == campaignId).GroupBy(c => c.SalesRegion).ToList();
                        return salesRegions.Select(e => new SelectListItem
                        {
                            Text = e.Key ?? "-", Value = e.Key ?? "-"
                        }).OrderBy(o => o.Text);
                    }
            }
            return null;
        }
    }
}