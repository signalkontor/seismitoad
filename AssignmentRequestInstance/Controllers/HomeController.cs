﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssignmentRequestInstance.Models;
using System.Web.Security;
using MailQueue;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Extensions;
using Telerik.Web.Mvc.Extensions;

namespace AssignmentRequestInstance.Controllers
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public Guid ProviderUserKey { get; set; }
        public string FriendlyName { get; set; }
    }

    [Authorize(Roles = SeismitoadShared.Constants.Roles.Analyst + ", " + SeismitoadShared.Constants.Roles.SelfbookingPromoter)]
    public partial class HomeController : Controller
    {
        private readonly int _campaignId = Convert.ToInt16(ConfigurationManager.AppSettings["CampaignId"]);
        private static readonly Queue MailQueue = new Queue(ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"], ConfigurationManager.AppSettings["QueueName"]);

        public virtual ActionResult Index()
        {
            var user = User.Identity.Name.ToLower();
            var isAdmin = User.IsInRole(SeismitoadShared.Constants.Roles.Admin);
            var isPM = User.IsInRole(SeismitoadShared.Constants.Roles.Permedia);
            var isADM = User.IsInRole(SeismitoadShared.Constants.Roles.Analyst) && !isAdmin && !isPM;
            using (var context = new SeismitoadDbContext())
            {
                var locations = context.CampaignsLocations
                    .Where(e => e.CampaignId == _campaignId && !isADM || isADM && e.SalesRegion.ToLower() == user)
                    .Select(e => new {e.Location.Id, e.Location.Name, e.Location.City, e.Location.Street, LocationGroup = e.Location.LocationGroup.Name})
                    .ToList();

                ViewBag.Locations = locations
                    .Select(e => new SelectListItem
                    {
                        Value = e.Id.ToString(),
                        Text = (string.IsNullOrWhiteSpace(e.LocationGroup)
                            ? e.Name
                            : e.LocationGroup) +
                               ", " + e.City + ", " + e.Street
                    }).ToList();

                ViewBag.Employees = context.Employees
                    .Where(e => e.AssignedEmployees.Any(i => i.Assignment.CampaignLocation.CampaignId == _campaignId))
                    .Select(e => new EmployeeViewModel
                    {
                        Id = e.Id,
                        ProviderUserKey =  e.ProviderUserKey,
                        FriendlyName = e.Firstname + " " + e.Lastname
                    })
                    .OrderBy(e => e.FriendlyName)
                    .ToList();
            }
            IEnumerable<PromotionDayRequest> data;

            if (isAdmin || isPM || user == "toshiba" || user == "areich")
            {
                data =
                    DB.Context.PromotionDayRequests.Where(
                        r => r.State == RequestState.Requested || r.State == RequestState.Confirmed);
            }
            else
            {
                data =
                    DB.Context.PromotionDayRequests.Where(
                        r => (r.State == RequestState.Requested || r.State == RequestState.Confirmed) && r.UserCreated == HttpContext.User.Identity.Name);
            }

            return View(data);
        }

        [HttpPost]
        public virtual ActionResult Create(PromotionDayRequest model)
        {
            var user = Membership.GetUser(User.Identity.Name);
            if (user != null)
            {
                var req = new PromotionDayRequest
                    {
                        RequestDay = LocalDateTime.Now,
                        AssignmentDuration = (int) (model.AssignmentEndDate - model.AssignmentStartDate).TotalMinutes
                    };
                if (!ControllerContext.HttpContext.User.IsInRole(SeismitoadShared.Constants.Roles.Admin))
                {
                    req.UserCreated = User.Identity.Name;
                }
                if (TryUpdateModel(req))
                {
                    DB.Context.PromotionDayRequests.Add(req);
                    DB.Context.SaveChanges();
                    if (!User.IsInRole(SeismitoadShared.Constants.Roles.Reporter))
                    {
                        var mail = new MailController().ReservationNotification(req, user.Email).Mail;
                        MailQueue.Send(mail);
                    }
                    if (req.IsProvision)
                    {
                        var key = (Guid)user.ProviderUserKey;
                        using (var miDB = new SeismitoadDbContext())
                        {
                            var assignment = new Assignment();
                            assignment.CampaignLocation = miDB.CampaignsLocations.Single(cl => cl.LocationId == req.LocationId && cl.CampaignId == _campaignId);
                            assignment.EmployeeCount = 1;
                            assignment.State = AssignmentState.Planned;
                            assignment.DateStart = req.AssignmentStartDate;
                            assignment.DateEnd = req.AssignmentEndDate;
                            miDB.Assignments.Add(assignment);
                            var assignedEmployee = new AssignedEmployee { Attendance = 0, ContactEmployee = false };
                            assignedEmployee.Employee = miDB.Employees.Single(e => e.Id == req.RequestedPromoterId);
                            assignedEmployee.AssignmentRoles = new Collection<AssignmentRole>();
                            assignedEmployee.AssignmentRoles.Add(miDB.AssignmentRoles.Single(ar => ar.Id == 8));
                            assignment.AssignedEmployees = new Collection<AssignedEmployee>();
                            assignment.AssignedEmployees.Add(assignedEmployee);
                            ChangeLogger.UserKey = () => key;
                            assignment.MarkForSync();
                            miDB.Assignments.Add(assignment);
                            miDB.SaveChanges();
                        }
                        DB.Context.PromotionDayRequests.Remove(req);
                        DB.Context.SaveChanges();
                    }
                    return RedirectToAction(MVC.Home.Index());
                }
            }

            return RedirectToAction(MVC.Home.Index());
        }

        public virtual ActionResult Accept(int id)
        {
            var promotionDay = DB.Context.PromotionDayRequests.SingleOrDefault(p => p.Id == id);
            if (promotionDay != null)
            {
                if (!String.IsNullOrWhiteSpace(promotionDay.UserCreated))
                {
                    var user = Membership.GetUser(promotionDay.UserCreated);
                    if (user != null)
                    {
                        using (var context = new SeismitoadDbContext())
                        {
                            var location = context.Locations.SingleOrDefault(l => l.Id == promotionDay.LocationId);
                            ViewBag.FriendlyNameLocation = location != null
                                                               ? location.FriendlyName
                                                               : "Markt nicht mehr verfügbar";
                            var freeEmployees = context.ActiveEmployees
                                                       .Where(
                                                           e =>
                                                           e.AssignedEmployees.All(ae =>
                                                                                   ae.Assignment.DateEnd <
                                                                                   promotionDay.AssignmentStartDate ||
                                                                                   ae.Assignment.DateStart >
                                                                                   promotionDay.AssignmentEndDate))
                                                       .ToList();
                            ViewBag.EmployeeList = freeEmployees
                                .Select(
                                    e =>
                                    new SelectListItem
                                        {
                                            Selected = e.Id == promotionDay.RequestedPromoterId,
                                            Text = e.Firstname + " " + e.Lastname,
                                            Value = e.Id.ToString()
                                        })
                                .ToList();
                            ViewBag.IsPromoterFree = freeEmployees.Any(fe => fe.Id == promotionDay.RequestedPromoterId);
                        }
                        return View(promotionDay);
                    }
                }
                DB.Context.PromotionDayRequests.Remove(promotionDay);
                DB.Context.SaveChanges();
            }
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpPost]
        public virtual ActionResult Accept(int id,int promoter, int[] AssignmentRoleIds, string freeText)
        {
            var promotionDay = DB.Context.PromotionDayRequests.SingleOrDefault(p => p.Id == id);
            if (promotionDay != null)
            {
                if (!String.IsNullOrWhiteSpace(promotionDay.UserCreated))
                {
                    var user = Membership.GetUser(promotionDay.UserCreated);
                    if (user != null)
                    {
                        var key = (Guid) user.ProviderUserKey;
                        using (var miDB = new SeismitoadDbContext())
                        {
                            var assignment = new Assignment();
                            assignment.CampaignLocation =
                                miDB.CampaignsLocations.Single(
                                    cl => cl.LocationId == promotionDay.LocationId && cl.CampaignId == _campaignId);
                            assignment.EmployeeCount = 1;
                            assignment.State = AssignmentState.Planned;
                            assignment.DateStart = promotionDay.AssignmentStartDate;
                            assignment.DateEnd = promotionDay.AssignmentEndDate;
                            miDB.Assignments.Add(assignment);
                            var assignedEmployee = new AssignedEmployee {Attendance = 0, ContactEmployee = false};
                            assignedEmployee.Employee = miDB.Employees.Single(e => e.Id == promoter);
                            assignedEmployee.AssignmentRoles = new Collection<AssignmentRole>();
                            foreach (var assignmentRoleId in AssignmentRoleIds.Where(ar => ar != 0))
                            {
                                assignedEmployee.AssignmentRoles.Add(
                                    miDB.AssignmentRoles.Single(ar => ar.Id == assignmentRoleId));
                            }
                            assignment.AssignedEmployees = new Collection<AssignedEmployee>();
                            assignment.AssignedEmployees.Add(assignedEmployee);
                            ChangeLogger.UserKey = () => key;
                            assignment.MarkForSync();
                            miDB.Assignments.Add(assignment);
                            miDB.SaveChanges();
                        }
                        var mail = new MailController().AcceptionNotification(promotionDay, user.Email, freeText).Mail;
                        MailQueue.Send(mail);
                        DB.Context.PromotionDayRequests.Remove(promotionDay);
                        DB.Context.SaveChanges();
                    }
                }
            }
            return RedirectToAction(MVC.Home.Index());
        }

        public virtual ActionResult Reject(int id)
        {
            var promotionDay = DB.Context.PromotionDayRequests.SingleOrDefault(p => p.Id == id);
            if (promotionDay != null)
            {
                if (!String.IsNullOrWhiteSpace(promotionDay.UserCreated))
                {
                    var user = Membership.GetUser(promotionDay.UserCreated);
                    if (user != null)
                    {
                        return View(promotionDay);
                    }
                }
                DB.Context.PromotionDayRequests.Remove(promotionDay);
                DB.Context.SaveChanges();
            }
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpPost]
        public virtual ActionResult Reject(int id, bool noPromoterAvailable, bool showAlternativeDate, string freeText)
        {
            var promotionDay = DB.Context.PromotionDayRequests.SingleOrDefault(p => p.Id == id);
            if (promotionDay != null)
            {
                if (!String.IsNullOrWhiteSpace(promotionDay.UserCreated))
                {
                    var user = Membership.GetUser(promotionDay.UserCreated);
                    if (user != null)
                    {
                        var mail = new MailController().RejectionNotification(promotionDay, user.Email, noPromoterAvailable,
                                                                   showAlternativeDate, freeText).Mail;
                        MailQueue.Send(mail);
                    }
                }
                DB.Context.PromotionDayRequests.Remove(promotionDay);
                DB.Context.SaveChanges();
            }
            return RedirectToAction(MVC.Home.Index());
        }
    }
}
