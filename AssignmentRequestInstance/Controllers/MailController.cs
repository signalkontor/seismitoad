﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ActionMailer.Net.Mvc;
using AssignmentRequestInstance.Models;

namespace AssignmentRequestInstance.Controllers
{
    public partial class MailController : MailerBase
    {

        public virtual EmailResult ReservationNotification(PromotionDayRequest model, string email)
        {
            To.Add(email);
            Subject = "Toshiba TV Promotion – Ihre Buchungsanfrage ist bei uns eingegangen!";
            return Email("ReservationNotification", model);
        }

        public virtual EmailResult AcceptionNotification(PromotionDayRequest model, string email, string freeText = null)
        {
            To.Add(email);
            Subject = "Toshiba TV Promotion – Bestätigung Ihrer angefragten Promotionstermine";
            ViewBag.FreeText = freeText;
            return Email("AcceptionNotification", model);
        }

        public virtual EmailResult RejectionNotification(PromotionDayRequest model, string email, bool noPromoterAvailable, bool showAlternativeDate, string freeText = null)
        {
            To.Add(email);
            Subject = "Toshiba TV Promotion – Absage Ihrer angefragten Promotionstermine";
            ViewBag.DeclineReason = noPromoterAvailable ? "zu diesem Termin kein Promoter verfügbar ist" : "kein Budget mehr verfügbar ist";
            ViewBag.ShowAlternativeDate = showAlternativeDate;
            ViewBag.FreeText = freeText;
            return Email("RejectionNotification", model);
        }

    }
}
