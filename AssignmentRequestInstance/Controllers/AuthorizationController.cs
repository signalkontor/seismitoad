﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AssignmentRequestInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace AssignmentRequestInstance.Controllers
{
    [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin)]
    public partial class AuthorizationController : Controller
    {
        //
        // GET: /Authorization/

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult Insert(Login model)
        {
            using (var context = new SeismitoadDbContext())
            {
                model.CampaignId = Convert.ToInt32(ConfigurationManager.AppSettings["CampaignId"]);
                model.CampaignRoles = SeismitoadShared.Constants.Roles.SelfbookingPromoter;
                if (context.Logins.Any(e => e.CampaignId == model.CampaignId && e.UserKey == model.UserKey && e.CampaignRoles.Contains(model.CampaignRoles)))
                    return RedirectToAction(MVC.Authorization.Index());

                context.Logins.Add(model);
                ChangeLogger.UserKey = () => null;
                context.SaveChanges();
            }
            return RedirectToAction(MVC.Authorization.Index());
        }

        public virtual ActionResult Delete(int id)
        {
            using (var context = new SeismitoadDbContext())
            {
                var login = context.Logins.Single(e => e.Id == id);
                context.Logins.Remove(login);
                ChangeLogger.UserKey = () => null;
                context.SaveChanges();
            }
            return RedirectToAction(MVC.Authorization.Index());
        }

    }
}
