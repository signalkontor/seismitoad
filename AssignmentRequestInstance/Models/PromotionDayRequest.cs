﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace AssignmentRequestInstance.Models
{
    public class PromotionDayRequest
    {
        [Display(AutoGenerateFilter = false)]
        public int Id { get; set; }

        [Display(Name = "Markt")]
        public virtual int LocationId { get; set; }

        [Display(Name = "Aktionstag")]
        public virtual int? AssignmentId { get; set; }

        [Display(Name = "Anfrage-Datum")]
        public DateTime RequestDay { get; set; }

        [Display(Name = "Aktions-Start"), DataType(DataType.DateTime)]
        [UIHint("PromotionDateTime")]
        public DateTime AssignmentStartDate { get; set; }

        [Display(Name = "Aktions-Dauer")]
        [UIHint("Duration")]
        public int AssignmentDuration { get; set; }

        [Display(Name = "Aktions-Ende"), DataType(DataType.DateTime)]
        [UIHint("PromotionDateTime")]
        public DateTime AssignmentEndDate { get; set; }

        [Display(Name = "Wunsch-Promoter")]
        public int RequestedPromoterId { get; set; }

        [Display(Name = "Provisionstag")]
        public bool IsProvision { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Rollen")]
        public int[] AssignmentRoleIds { get; set; }

        [Display(Name = "Bemerkung ADM"), DataType(DataType.MultilineText)]
        public string CommentRequest { get; set; }

        [Display(Name = "Bemerkung Agentur",AutoGenerateField = false)]
        public string CommentAnswer { get; set; }

        [Display(Name = "Aktueller Status")]
        public RequestState State { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Benutzer")]
        public string UserCreated { get; set; }

    }

    public enum RequestState
    {
        [Display(Name="Angefragt")]
        Requested = 0,
        [Display(Name = "Bestätigt")]
        Confirmed = 1,
        [Display(Name = "Durchgeführt")]
        Executed = 2,
        [Display(Name = "Ausgefallen")]
        Odd = 3
    }
    
            
            
            
 
}