﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SeismitoadShared.Constants;

namespace AssignmentRequestInstance.Models
{
    public class RequestInstanceDbContext : DbContext
    {
        public DbSet<PromotionDayRequest> PromotionDayRequests { get; set; }
    }

    public static class DB
    {
        public static RequestInstanceDbContext Context
        {
            get { return HttpContext.Current.Items[HttpContextKeys.DbContextKey] as RequestInstanceDbContext; }
        }
    }
}