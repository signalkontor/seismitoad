﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ObjectTemplate;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Constants;
using AssignmentRequestInstance.Models;

namespace AssignmentRequestInstance
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(DateTime), new DEDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DEDateTimeModelBinder());
            
            Configurator.Configure(config =>
            {
                config.SelectListRepository = new SelectListRepository();
                config.InfoIconUrl = Links.Content.icons._16x16.actions.info_png;
                config.ShowValidationSummary = false;
            });

            SeismitoadMembershipProvider.SeismitoadMembershipProvider.CampaignId = Convert.ToInt32(ConfigurationManager.AppSettings["CampaignId"]);
            SeismitoadMembershipProvider.SeismitoadRoleProvider.CampaignId = Convert.ToInt32(ConfigurationManager.AppSettings["CampaignId"]);
            Database.SetInitializer<SeismitoadDbContext>(null);
            HtmlAttributes.Initialize();
        }

        // ReSharper disable InconsistentNaming
        protected virtual void Application_BeginRequest()
        // ReSharper restore InconsistentNaming
        {
            HttpContext.Current.Items[HttpContextKeys.DbContextKey] = new RequestInstanceDbContext();
        }

        // ReSharper disable InconsistentNaming
        protected virtual void Application_EndRequest()
        // ReSharper restore InconsistentNaming
        {
            var dbContext = HttpContext.Current.Items[HttpContextKeys.DbContextKey] as RequestInstanceDbContext;
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}