﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Security;
using SeismitoadModel.DatabaseContext;

namespace AssignmentRequestInstance
{
    public class AssignmentRequestInstanceRoleProvider : SqlRoleProvider
    {
        public override string[] GetRolesForUser(string username)
        {
            var aspnetRoles = base.GetRolesForUser(username);
            var userkey = (Guid) Membership.GetUser(username).ProviderUserKey;
            var cId = Convert.ToInt32(ConfigurationManager.AppSettings["CampaignId"]);
            using (var context = new SeismitoadDbContext())
            {
                var login = context.Logins.SingleOrDefault(e => e.UserKey == userkey && e.CampaignId == cId);
                if (login != null)
                {
                    var campaignRoles = login.CampaignRoles.Split(';');
                    if (campaignRoles.Any())
                    {
                        return aspnetRoles.Concat(campaignRoles).ToArray();
                    }
                }
            }
            return aspnetRoles;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var isInAspNetRole = base.IsUserInRole(username, roleName);
            if (isInAspNetRole)
                return true;

            var userkey = (Guid)Membership.GetUser(username).ProviderUserKey;
            using (var context = new SeismitoadDbContext())
            {
                var login = context.Logins.SingleOrDefault(e => e.UserKey == userkey);
                return login != null && login.CampaignRoles.Split(';').Any(e => e == roleName);
            }

        }
    }
}
