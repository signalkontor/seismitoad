﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SetupCustomerInstance
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void webConfig_Click(object sender, EventArgs e)
        {
            var webConfigStr = Encoding.UTF8.GetString(Properties.Resources.Web_Config);
            webConfigStr = webConfigStr.Replace("${campaignId}", campaignId.Text);
            webConfigStr = webConfigStr.Replace("${campaignTitle}", campaignTitle.Text);
            webConfigStr = webConfigStr.Replace("${hostName}", hostName.Text);
            webConfigStr = webConfigStr.Replace("${elmahMailAddress}", elmahMailAddress.Text);
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Web.Config speichern",
                Filter = "Web.Config|Web.Config"
            };
            saveFileDialog.ShowDialog();
            if(saveFileDialog.FileName != "")
            {
                File.WriteAllText(saveFileDialog.FileName, webConfigStr);
                MessageBox.Show("Web.Config wurde gespeichert. Weitere Einstellungen müssen manuell gemacht werden.");
            }
        }

        private void webReleaseConfig_Click(object sender, EventArgs e)
        {
            var webReleaseConfigStr = Encoding.UTF8.GetString(Properties.Resources.Web_Release_Config);
            webReleaseConfigStr = webReleaseConfigStr.Replace("${inetpubDirectory}", inetpubDirectory.Text);
            webReleaseConfigStr = webReleaseConfigStr.Replace("${dbServer}", dbServer.Text);
            webReleaseConfigStr = webReleaseConfigStr.Replace("${hostName}", hostName.Text);
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Web.Release.Config speichern",
                Filter = "Web.Release.Config|Web.Release.Config"
            };
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                File.WriteAllText(saveFileDialog.FileName, webReleaseConfigStr);
                MessageBox.Show("Web.Release.Config wurde gespeichert. Weitere Einstellungen müssen manuell gemacht werden.");
            }
        }

        private void showHostLine_Click(object sender, EventArgs e)
        {
            var form = new HostFileForm();
            form.textBox1.Text = $"\n{serverIP.Text} {serverName.Text}\n";
            form.ShowDialog();
        }

        private void saveSQL_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented");
        }

        private void createSiteButton_Click(object sender, EventArgs e)
        {
            var createSite = new CreateWebsite(hostName.Text + ".permedia-report.de", inetpubDirectory.Text);
            createSite.Commit();
        }
    }
}
