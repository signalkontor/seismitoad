﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SetupCustomerInstance
{
    internal class CreateWebsite
    {
        private readonly string _websiteName;
        private readonly string _inetPubDir;

        internal CreateWebsite(string webSiteName, string inetPubDir)
        {
            _websiteName = webSiteName;
            _inetPubDir = inetPubDir;
        }

        internal void Commit()
        {
            using (var mgr = new ServerManager())
            {
                var appPool = mgr.ApplicationPools.Add(_websiteName);
                appPool.AutoStart = true;
                appPool.ManagedRuntimeVersion = "v4.0";
                var vhostDir = Path.Combine(_inetPubDir, "vhost", _websiteName);
                Directory.CreateDirectory(vhostDir);
                var vhostDataDir = Path.Combine(_inetPubDir, "vhost-data", _websiteName);
                var newSite = mgr.Sites.Add(_websiteName, vhostDir, 80);
                newSite.Applications[0].ApplicationPoolName = _websiteName;
                newSite.ServerAutoStart = true;
                var webApp = mgr.Sites[_websiteName].Applications[0];
                var downloadsDir = Path.Combine(vhostDataDir, "Downloads");
                var uploadsDir = Path.Combine(vhostDataDir, "Uploads");
                var imagecacheDir = Path.Combine(Path.Combine(_inetPubDir, "vhost-data", "imagecache", _websiteName));
                var downloadsDirInfo = Directory.CreateDirectory(downloadsDir);
                var uploadsDirInfo = Directory.CreateDirectory(Path.Combine(uploadsDir, "Employee")).Parent;
                var imagecacheDirInfo = Directory.CreateDirectory(imagecacheDir);
                webApp.VirtualDirectories.Add("/Downloads", downloadsDir);
                webApp.VirtualDirectories.Add("/Uploads", uploadsDir);
                webApp.VirtualDirectories.Add("/imagecache", imagecacheDir);
                newSite.Bindings.Clear();
                newSite.Bindings.Add($"*:80:{_websiteName}", "http");
                try
                {
                    var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                    store.Open(OpenFlags.OpenExistingOnly);
                    var certificate = store.Certificates.Find(X509FindType.FindBySerialNumber,
                        "‎5E14DBD19A8620700497CDAA", true); // <-- Hier muss die Serialnumber des SSL Zertifikats von *.permedia-report.de stehen
                    newSite.Bindings.Add($"*:443:{_websiteName}", certificate[0].GetCertHash(), "MY");
                }
                catch
                {
                    // TODO
                }
                mgr.CommitChanges();

                var fullControlToAppPoolUser = new FileSystemAccessRule($"IIS AppPool\\{_websiteName}", FileSystemRights.Modify, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);
                var vhostDataDirInfo = new DirectoryInfo(vhostDataDir);
                foreach (var directoryInfo in new[] { vhostDataDirInfo, imagecacheDirInfo })
                {
                    var directorySecurity = directoryInfo.GetAccessControl();
                    directorySecurity.AddAccessRule(fullControlToAppPoolUser);
                    directoryInfo.SetAccessControl(directorySecurity);
                }
            }
        }
    }
}
