﻿namespace SetupCustomerInstance
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.campaignTitle = new System.Windows.Forms.TextBox();
            this.campaignId = new System.Windows.Forms.NumericUpDown();
            this.hostName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.elmahMailAddress = new System.Windows.Forms.TextBox();
            this.dbServer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.inetpubDirectory = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.webConfig = new System.Windows.Forms.Button();
            this.webReleaseConfig = new System.Windows.Forms.Button();
            this.serverIP = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.saveSQL = new System.Windows.Forms.Button();
            this.showHostLine = new System.Windows.Forms.Button();
            this.serverName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.createSiteButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.campaignId)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name (wird im HTML-Title verwendet):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "CampaignId";
            // 
            // campaignTitle
            // 
            this.campaignTitle.Location = new System.Drawing.Point(206, 10);
            this.campaignTitle.Name = "campaignTitle";
            this.campaignTitle.Size = new System.Drawing.Size(226, 20);
            this.campaignTitle.TabIndex = 1;
            // 
            // campaignId
            // 
            this.campaignId.Location = new System.Drawing.Point(206, 38);
            this.campaignId.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.campaignId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.campaignId.Name = "campaignId";
            this.campaignId.Size = new System.Drawing.Size(100, 20);
            this.campaignId.TabIndex = 3;
            this.campaignId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // hostName
            // 
            this.hostName.Location = new System.Drawing.Point(206, 64);
            this.hostName.Name = "hostName";
            this.hostName.Size = new System.Drawing.Size(226, 20);
            this.hostName.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Hostname (ohne .permedia-report.de)";
            // 
            // elmahMailAddress
            // 
            this.elmahMailAddress.Location = new System.Drawing.Point(206, 91);
            this.elmahMailAddress.Name = "elmahMailAddress";
            this.elmahMailAddress.Size = new System.Drawing.Size(226, 20);
            this.elmahMailAddress.TabIndex = 6;
            this.elmahMailAddress.Text = "mikko.jania@signalkontor.com";
            // 
            // dbServer
            // 
            this.dbServer.Location = new System.Drawing.Point(206, 117);
            this.dbServer.Name = "dbServer";
            this.dbServer.Size = new System.Drawing.Size(226, 20);
            this.dbServer.TabIndex = 7;
            this.dbServer.Text = ".\\SQLEXPRESS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "ELMAH Mail Empfänger";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "DB Server Name (UNC)";
            // 
            // inetpubDirectory
            // 
            this.inetpubDirectory.Location = new System.Drawing.Point(206, 197);
            this.inetpubDirectory.Name = "inetpubDirectory";
            this.inetpubDirectory.Size = new System.Drawing.Size(226, 20);
            this.inetpubDirectory.TabIndex = 20;
            this.inetpubDirectory.Text = "C:\\inetpub";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "inetpub Verzeichnis";
            // 
            // webConfig
            // 
            this.webConfig.Location = new System.Drawing.Point(12, 315);
            this.webConfig.Name = "webConfig";
            this.webConfig.Size = new System.Drawing.Size(134, 23);
            this.webConfig.TabIndex = 100;
            this.webConfig.Text = "Web.Config speichern";
            this.webConfig.UseVisualStyleBackColor = true;
            this.webConfig.Click += new System.EventHandler(this.webConfig_Click);
            // 
            // webReleaseConfig
            // 
            this.webReleaseConfig.Location = new System.Drawing.Point(12, 344);
            this.webReleaseConfig.Name = "webReleaseConfig";
            this.webReleaseConfig.Size = new System.Drawing.Size(167, 23);
            this.webReleaseConfig.TabIndex = 101;
            this.webReleaseConfig.Text = "Web.Release.Config speichern";
            this.webReleaseConfig.UseVisualStyleBackColor = true;
            this.webReleaseConfig.Click += new System.EventHandler(this.webReleaseConfig_Click);
            // 
            // serverIP
            // 
            this.serverIP.Location = new System.Drawing.Point(206, 145);
            this.serverIP.Name = "serverIP";
            this.serverIP.Size = new System.Drawing.Size(226, 20);
            this.serverIP.TabIndex = 14;
            this.serverIP.Text = "aaa.bbb.ccc.ddd";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Server IP Adresse";
            // 
            // saveSQL
            // 
            this.saveSQL.Location = new System.Drawing.Point(206, 315);
            this.saveSQL.Name = "saveSQL";
            this.saveSQL.Size = new System.Drawing.Size(89, 23);
            this.saveSQL.TabIndex = 102;
            this.saveSQL.Text = "SQL Speichern";
            this.saveSQL.UseVisualStyleBackColor = true;
            this.saveSQL.Click += new System.EventHandler(this.saveSQL_Click);
            // 
            // showHostLine
            // 
            this.showHostLine.Location = new System.Drawing.Point(206, 344);
            this.showHostLine.Name = "showHostLine";
            this.showHostLine.Size = new System.Drawing.Size(75, 23);
            this.showHostLine.TabIndex = 103;
            this.showHostLine.Text = "HOST-Datei";
            this.showHostLine.UseVisualStyleBackColor = true;
            this.showHostLine.Click += new System.EventHandler(this.showHostLine_Click);
            // 
            // serverName
            // 
            this.serverName.Location = new System.Drawing.Point(206, 171);
            this.serverName.Name = "serverName";
            this.serverName.Size = new System.Drawing.Size(226, 20);
            this.serverName.TabIndex = 18;
            this.serverName.Text = "CT123456";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Server Name";
            // 
            // createSiteButton
            // 
            this.createSiteButton.Location = new System.Drawing.Point(301, 315);
            this.createSiteButton.Name = "createSiteButton";
            this.createSiteButton.Size = new System.Drawing.Size(131, 23);
            this.createSiteButton.TabIndex = 104;
            this.createSiteButton.Text = "Website im IIS erstellen";
            this.createSiteButton.UseVisualStyleBackColor = true;
            this.createSiteButton.Click += new System.EventHandler(this.createSiteButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 384);
            this.Controls.Add(this.createSiteButton);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.serverName);
            this.Controls.Add(this.showHostLine);
            this.Controls.Add(this.saveSQL);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.serverIP);
            this.Controls.Add(this.webReleaseConfig);
            this.Controls.Add(this.webConfig);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.inetpubDirectory);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dbServer);
            this.Controls.Add(this.elmahMailAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hostName);
            this.Controls.Add(this.campaignId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.campaignTitle);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Neue CustomerInstance";
            ((System.ComponentModel.ISupportInitialize)(this.campaignId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox campaignTitle;
        private System.Windows.Forms.NumericUpDown campaignId;
        private System.Windows.Forms.TextBox hostName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox elmahMailAddress;
        private System.Windows.Forms.TextBox dbServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox inetpubDirectory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button webConfig;
        private System.Windows.Forms.Button webReleaseConfig;
        private System.Windows.Forms.TextBox serverIP;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button saveSQL;
        private System.Windows.Forms.Button showHostLine;
        private System.Windows.Forms.TextBox serverName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button createSiteButton;
    }
}

