﻿using AutoMapper;
using SeismitoadModel;
using System.Linq;

namespace PromoterInstance.Models
{
    public class JobDetailsViewModel
    {
        public int Id { get; set; }
        public bool GradualBooking { get; set; }
        public string Name { get; set; }
        public bool IsApplyable { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
        public int EmployeesPerAssignment { get; set; }
        public string Requirements { get; set; }
        public string Times { get; set; }
        public string Days { get; set; }
        public string State { get; set; }
        #region Ansprechpartner

        public string ContactPMName1 { get; set; }
        public string ContactPMPhone1 { get; set; }
        public string ContactPMEmail1 { get; set; }
        public string ContactPMRemark1 { get; set; }
        public string ContactPMName2 { get; set; }
        public string ContactPMPhone2 { get; set; }
        public string ContactPMEmail2 { get; set; }
        public string ContactPMRemark2 { get; set; }
        public string ContactPMName3 { get; set; }
        public string ContactPMPhone3 { get; set; }
        public string ContactPMEmail3 { get; set; }
        public string ContactPMRemark3 { get; set; }
        public string ContactPMName4 { get; set; }
        public string ContactPMPhone4 { get; set; }
        public string ContactPMEmail4 { get; set; }
        public string ContactPMRemark4 { get; set; }
        public string ContactADName1 { get; set; }
        public string ContactADPhone1 { get; set; }
        public string ContactADEmail1 { get; set; }
        public string ContactADRemark1 { get; set; }
        public string ContactADName2 { get; set; }
        public string ContactADPhone2 { get; set; }
        public string ContactADEmail2 { get; set; }
        public string ContactADRemark2 { get; set; }
        public string ContactADName3 { get; set; }
        public string ContactADPhone3 { get; set; }
        public string ContactADEmail3 { get; set; }
        public string ContactADRemark3 { get; set; }
        public string ContactADName4 { get; set; }
        public string ContactADPhone4 { get; set; }
        public string ContactADEmail4 { get; set; }
        public string ContactADRemark4 { get; set; }
        public string InstanceUrl { get; set; }
        #endregion
    }
}