﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class CurrentAssignment
    {
        public int Id { get; set; }
        public string Campaign { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public bool HasCoordinates { get; set; }
        public string Remarks { get; set; }
        public string Contact { get; set; }
        public string Team { get; set; }
        public bool AlreadyCheckedIn { get; set; }
        public bool DidNotAttend { get; set; }
    }
}