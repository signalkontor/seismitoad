﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class CheckInModel
    {
        public int AssignmentId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Accuracy { get; set; }
    }
}