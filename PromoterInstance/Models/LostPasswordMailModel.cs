﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class LostPasswordMailModel
    {
        public string Firstname { get; set; }
        public string Username { get; set; }
        public string PasswordResetCode { get; set; }
    }
}