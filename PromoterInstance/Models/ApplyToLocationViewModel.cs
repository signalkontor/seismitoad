﻿using AutoMapper;
using SeismitoadModel;

namespace PromoterInstance.Models
{
    public class ApplyToLocationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}