﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class StartViewModel
    {
        public string Firstname { get; set; }
        public string StartCode { get; set; }
        public bool Deleteable { get; set; }
        [Required(ErrorMessage = "Bitte ein Passwort eingeben."), StringLength(100, ErrorMessage = "Das Passwort muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Bitte das Passwort bestätigen."), Compare("Password", ErrorMessage = "Passwort und Bestätigung stimmen nicht überein.")]
        public string Password2 { get; set; }
        [Required(ErrorMessage = "Du musst den Datenschutzbestimmungen zustimmen.")]
        public bool? PrivacyTermsAccepted { get; set; }
    }
}