﻿using ObjectTemplate.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace PromoterInstance.Models
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Bitte das alte Passwort eingeben.")]
        [DataType(DataType.Password)]
        [Display(Name = "Aktuelles Passwort")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Bitte ein neues Passwort eingeben.")]
        [StringLength(100, ErrorMessage = "Das Passwort muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Neues Passwort")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Neues Passwort bestätigen")]
        [Compare("NewPassword", ErrorMessage = "Passwort und Bestätigung stimmen nicht überein.")]
        public string ConfirmPassword { get; set; }
    }

    public class SetNewPasswordModel
    {
        [Required(ErrorMessage = "Bitte ein Passwort eingeben.")]
        [StringLength(100, ErrorMessage = "Das Passwort muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Neues Passwort")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Neues Passwort bestätigen")]
        [Compare("NewPassword", ErrorMessage = "Passwort und Bestätigung stimmen nicht überein.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Bitte den Benutzernamen eingeben.")]
        [Display(Name = "Benutzername")]
        [System.Web.Mvc.AllowHtml]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Bitte das Passwort eingeben.")]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        [System.Web.Mvc.AllowHtml]
        public string Password { get; set; }

        [Display(Name = "Angemeldet bleiben")]
        public bool RememberMe { get; set; }

        public bool IsLocked { get; set; }
        public bool IsInactiveOrBlocked { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "Anrede")]
        [UIHint("TitleSelect")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Vorname")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Lastname { get; set; }

        [Display(Name = "Geburtstag")]
        [UIHint("DateIn3Fields")]
        public DateTime Birthday { get; set; }

        [Range(1, 31)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayDay { get; set; }

        [Range(1, 12)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayMonth { get; set; }

        [Range(1900, 9999)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayYear { get; set; }

        [Required]
        [Display(Name = "Benutzername")]
        // Erlaube Buchstaben '\w', Zahlen '\d', Leerzeichen ' ', at '@', Underscrore '_', Minus '-' und Punkt '\.'
        [RegularExpression("[\\w\\d @_\\-\\.]*", ErrorMessage = "Bitte verwende nur Buchstaben, Zahlen, Leerzeichen, Punkte, Unterstriche Minuszeichen oder '@'.")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Bitte ein Passwort eingeben.")]
        [StringLength(100, ErrorMessage = "Das Passwort muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Passwort bestätigen")]
        [Compare("Password", ErrorMessage = "Passwort und Bestätigung stimmen nicht überein.")]
        public string ConfirmPassword { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Bitte stimme den Datenschutzbestimmungen zu.")]
        [Display(Name = "Datenschutzbestimmungen")]
        public bool PrivacyTermsAccepted { get; set; }
    }

    public class PasswordRetrievalModel
    {
        [Required(ErrorMessage = "Bitte gib Deine Email-Adresse ein.")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email-Adresse")]
        public string Email { get; set; }
    }
}
