﻿using SeismitoadModel;
using System.Collections.Generic;

namespace PromoterInstance.Models
{
    public class ExperienceEditModel
    {
        public IEnumerable<Experience> Experiences { get; set; }
        public Experience NewExperience { get; set; }
    }
}