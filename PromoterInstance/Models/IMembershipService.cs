using System;
using System.Web.Security;

namespace PromoterInstance.Models
{
    public interface IMembershipService
    {
        MembershipUser GetUser(string username);
        MembershipUser GetUser(Guid providerUserKey);
        void UpdateUser(MembershipUser membershipUser);
    }
}