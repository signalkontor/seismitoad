﻿using System.ComponentModel.DataAnnotations;

namespace PromoterInstance.Models
{
    public class ApplyableCampaign
    {
        public int Id { get; set; }

        public bool Highlighted { get; set; }

        [Display(Name = "Projekt")]
        public string Name { get; set; }

        [Display(Name = "Aktionszeitraum")]
        public string Duration { get; set; }

        [Display(Name = "Aktionstage")]
        public string Days { get; set; }

        [Display(Name = "Aktionszeiten")]
        public string Times { get; set; }

        [Display(Name = "Status")]
        public string State { get; set; }

    }
}