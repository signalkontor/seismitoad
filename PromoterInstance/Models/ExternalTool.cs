﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class ExternalTool
    {
        public string InstanceUrl { get; set; }
        public string Name { get; set; }
    }
}