﻿using System;

namespace PromoterInstance.Models
{
    public class ProfileCardViewModel
    {
        public string Name { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime? LastProfileChange { get; set; }
        public string PictureUrl { get; set; }
    }
}