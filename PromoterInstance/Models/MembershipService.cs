using System;
using System.Web.Security;

namespace PromoterInstance.Models
{
    class MembershipService : IMembershipService
    {
        public MembershipUser GetUser(string username)
        {
            return Membership.GetUser(username);
        }

        public MembershipUser GetUser(Guid providerUserKey)
        {
            return Membership.GetUser(providerUserKey);
        }

        public void UpdateUser(MembershipUser membershipUser)
        {
            Membership.UpdateUser(membershipUser);
        }
    }
}