﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using AutoMapper;
using SeismitoadShared.Constants;

namespace PromoterInstance.Models
{
    public class PhoneNumber : IValidatableObject
    {
        [RegularExpression(@"[+][0-9]{1,3}", ErrorMessage = "Die Ländervorwahl muss mit + beginnen und darf maximal 4 Zeichen lang sein.")]
        public string CountryCode { get; set; }

        [StringLength(5, MinimumLength = 2, ErrorMessage = "Die Vorwahl muss zwischen 2 und 5 Zeichen lang sein.")]
        public string AreaCode { get; set; }

        [StringLength(10, MinimumLength = 3, ErrorMessage = "Die Durchwahl muss zwischen 3 und 10 Zeichen lang sein.")]
        public string Number { get; set; }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(CountryCode) || string.IsNullOrWhiteSpace(AreaCode) || string.IsNullOrWhiteSpace(Number)
                ? "" 
                : string.Format("{0} {1}-{2}", CountryCode, AreaCode, Number);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var noCountryCode = string.IsNullOrWhiteSpace(CountryCode);
            var noAreaCode = string.IsNullOrWhiteSpace(AreaCode);
            var noNumber = string.IsNullOrWhiteSpace(Number);

            var completelyEmpty = noCountryCode && noAreaCode && noNumber;
            var completelyFilled = !noCountryCode && !noAreaCode && !noNumber;
            if (!(completelyEmpty || completelyFilled))
            {
                yield return new ValidationResult("Bitte alle Felder ausfüllen.", new[] { "CountryCode", "AreaCode", "Number" });
            }
        }
    }

    public class PhoneNumberResolver : ValueResolver<string, PhoneNumber>
    {
        private static readonly Regex PhoneRegex = new Regex(Other.PhoneNumberRegex);

        protected override PhoneNumber ResolveCore(string source)
        {
            return Resolve(source);
        }

        public PhoneNumber Resolve(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;

            var phoneNumber = new PhoneNumber();
            var matches = PhoneRegex.Matches(source);
            if (matches.Count == 1 && matches[0].Groups.Count >= 4)
            {
                phoneNumber.CountryCode = matches[0].Groups[1].Value;
                phoneNumber.AreaCode = matches[0].Groups[2].Value;
                phoneNumber.Number = matches[0].Groups[3].Value;
                if (matches[0].Groups.Count == 5)
                    phoneNumber.Number += matches[0].Groups[4].Value;
            }
            return phoneNumber;
        }
    }
}