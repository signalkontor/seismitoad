﻿namespace PromoterInstance.Models
{
    public class AccountActionsModel
    {
        public bool UserIsAuthenticated { get; set; }
        public int SessionTimeout { get; set; }
        public string Greeting { get; set; }
    }
}