﻿
namespace PromoterInstance.Models
{
    public class AssignmentDetails
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Notes { get; set; }
        public string Contact { get; set; }
        //public string Phone { get; set; } --> (Vorerst?) weglassen lt PM 
        public string Team { get; set; }
    }
}