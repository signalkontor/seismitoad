﻿using System;
using AutoMapper;
using SeismitoadModel;

namespace PromoterInstance.Models
{
    public class ArchivedAssignment
    {
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string CampaignName { get; set; }
        public string LocationName { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}