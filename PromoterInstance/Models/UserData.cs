﻿using System;

namespace PromoterInstance.Models
{
    public class UserData
    {
        public UserData()
        {
        }

        public UserData(string userData)
        {
            var tmp = userData.Split(';');
            EmployeeId = Convert.ToInt32(tmp[0]);
            Name = tmp[1];
        }

        public int EmployeeId { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("{0};{1}", EmployeeId, Name);
        }

        public static bool IsValidUserData(string userData)
        {
            // Es könnte User mit alten (dauerhaften) Cookies geben, bei denen
            // UserData nicht gesetzt ist. Daher prüfen wir hier ob das Cookie
            // einen UserData String enthält. Sollte sich später der Aufbau
            // des Strings ändern (z.B. JSON statt CSV oder mehr Felder) muss
            // diese Methode verfeinert werden.
            return !string.IsNullOrWhiteSpace(userData);
        }
    }
}