﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;

namespace PromoterInstance.Models
{
    public class AppliedCampaign
    {
        public int Id { get; set; }

        [Display(Name = "Projekt")]
        public string Name { get; set; }

        [Display(Name = "Aktionszeitraum")]
        public string Duration { get; set; }

        [Display(Name = "Aktionstage")]
        public string Days { get; set; }

        [Display(Name = "Aktionszeiten")]
        public string Times { get; set; }

        [Display(Name = "Status")]
        public string State { get; set; }
    }

    // Brauchen wir nicht mehr, weil wir nun direkt mit Dapper die Daten lesen
    //public class AppliedCampaignAutmapperProfile : Profile
    //{
    //    protected override void Configure()
    //    {
    //        Mapper.CreateMap<CampaignEmployee, AppliedCampaign>()
    //            .ForMember(e => e.Id, opt => opt.MapFrom(e => e.CampaignId))
    //            .ForMember(e => e.Name, opt => opt.MapFrom(e => e.Campaign.Name))
    //            .ForMember(e => e.Duration, opt => opt.MapFrom(e => e.Campaign.Duration))
    //            .ForMember(e => e.Days, opt => opt.MapFrom(e => e.Campaign.Days))
    //            .ForMember(e => e.Times, opt => opt.MapFrom(e => e.Campaign.Times))
    //            .ForMember(e => e.State, opt => opt.MapFrom(e => e.State));
    //    }
    //}
}