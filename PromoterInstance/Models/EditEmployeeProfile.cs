﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web.Mvc;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadShared.Attributes;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;
using Roles = SeismitoadShared.Constants.Roles;

namespace PromoterInstance.Models
{
    public class EditEmployeeProfile : IValidatableObject
    {
        #region Konstanten
        private const string PersönlicheDaten = "Persönliche Daten";
        private const string BodyprofilUndTalente = "Bodyprofil & Talente";
        private const string VertragUndGewerbe = "Vertrag & Gewerbe";
        private const string Mobilität = "Mobilität";
        private const string AusbildungUndQualifikation = "Ausbildung & Qualifikation";
        private const string Auftragswunsch = "Auftragswunsch";
        public static readonly DateTime EarliestBirthdayAllowed = new DateTime(1900, 1, 1);
        #endregion

        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public bool IncompleteProfile { get; set; }

        #region A. Persönliche Daten
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int EmployeeId { get; set; }

        [Required]
        [Display(Name = "Anrede", Description = PersönlicheDaten)]
        [UIHint("TitleSelect")]
        public string Title2 { get; set; }

        [Required]
        [Display(Name = "Vorname", Description = PersönlicheDaten)]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Name", Description = PersönlicheDaten)]
        public string Lastname { get; set; }

        [Display(Name = "Geburtstag", Description = PersönlicheDaten)]
        [UIHint("DateIn3Fields")]
        public DateTime Birthday { get; set; }

        [Range(1, 31)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayDay { get; set; }

        [Range(1, 12)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayMonth { get; set; }

        [Range(1900, 9999)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayYear { get; set; }

        [Required]
        [Display(Name = "Benutzername", Description = PersönlicheDaten)]
        // Erlaube Buchstaben '\w', Zahlen '\d', Leerzeichen ' ', at '@', Underscrore '_', Minus '-' und Punkt '\.'
        [RegularExpression("[\\w\\d @_\\-\\.]*", ErrorMessage = "Bitte verwende nur Buchstaben, Zahlen, Leerzeichen, Punkte, Unterstriche Minuszeichen oder '@'.")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email 1", Description = PersönlicheDaten)]
        public string Email1 { get; set; }

        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email 2", Description = PersönlicheDaten)]
        public string Email2 { get; set; }

        [Display(Name = "Skype", Description = PersönlicheDaten)]
        public string Skype { get; set; }

        [Display(Name = "Eigene Website", Description = PersönlicheDaten)]
        public string Website { get; set; }

        [Required]
        [Display(Name = "Mobil", Description = PersönlicheDaten)]
        [Format(StartNewColumn = true)]
        [UIHint("PhoneIn3Fields")]
        [CustomValidation(typeof(EditEmployeeProfile), "PhoneMobileNoValidator")]
        public PhoneNumber PhoneMobileNo { get; set; }

        [Display(Name = "Festnetz", Description = PersönlicheDaten)]
        [UIHint("PhoneIn3Fields")]
        public PhoneNumber PhoneNo { get; set; }

        [Display(Name = "Fax", Description = PersönlicheDaten)]
        [UIHint("PhoneIn3Fields")]
        public PhoneNumber FaxNo { get; set; }

        [Required]
        [Display(Name = "Straße", Description = PersönlicheDaten)]
        public string Street { get; set; }

        [Required]
        [Display(Name = "PLZ", Description = PersönlicheDaten)]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Ort", Description = PersönlicheDaten)]
        public string City { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "Land", Description = PersönlicheDaten)]
        public int? Country { get; set; }

        [Required]
        [Display(Name = "Upload Portraitfoto (jpg)", Description = PersönlicheDaten)]
        [UIHint("FileUpload")]
        [Format(StartNewColumn = true)]
        public int UploadPortraitPhoto { get { return EmployeeId; } }

        [Visibility(ShowForEdit = false)]
        public bool PortraitPhotoUploaded { get; set; }

        [Display(Name = "Upload Ganzkörperfoto (jpg)", Description = PersönlicheDaten)]
        [UIHint("FileUpload")]
        public int? UploadFullBodyPhoto { get { return EmployeeId; } }

        [Display(Name = "Upload weiteres Foto (jpg)", Description = PersönlicheDaten)]
        [UIHint("FileUpload")]
        public int? UploadOtherPhoto { get { return EmployeeId; } }
        #endregion

        #region B. Body Profil und Talente
        [Required]
        [Range(140, 220)]
        [UIHint("Integer")]
        [Display(Name = "Körpergröße (in cm)", Description = BodyprofilUndTalente)]
        public int? Height { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Konfektionsgröße", Description = BodyprofilUndTalente)]
        public int? MaleSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Konfektionsgröße", Description = BodyprofilUndTalente)]
        public int? FemaleSize { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "T-Shirt-Größe", Description = BodyprofilUndTalente)]
        public int? ShirtSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeansweite", Description = BodyprofilUndTalente)]
        public int? JeansWidth { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeanslänge", Description = BodyprofilUndTalente)]
        public int? JeansLength { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Schuhgröße", Description = BodyprofilUndTalente)]
        public int? ShoeSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Haarfarbe", Description = BodyprofilUndTalente)]
        [Format(StartNewColumn = true)]
        public int? HairColor { get; set; }

        [Display(Name = "Sichtbare Tattoos", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        public bool? VisibleTattoos { get; set; }

        [Display(Name = "Gesichtspiercing/s", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        public bool? FacialPiercing { get; set; }

        [Format(StartNewColumn = true)]
        [UIHint("MultiSelect")]
        [Display(Name = "Künstlerisches", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] ArtisticAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Gastro", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] GastronomicAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Aktivitäten / Sport", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Sports { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Weitere Talente und Hobbies", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] OtherAbilities { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstiges", Description = BodyprofilUndTalente)]
        public string OtherAbilitiesText { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        [Required]
        [Display(Name = "Bist du berechtigt eigene Rechnungen zu stellen?", Description = VertragUndGewerbe)]
        public bool? ValidTradeLicense { get; set; }

        [Display(Name = "Upload Gewerbeschein (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        [UIHint("FileUpload")]
        public int? UploadTradeLicense { get { return EmployeeId; } }

        [Display(Name = "Rahmenvertrag liegt bei advantage vor:", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string SkeletonContract { get; set; }

        [Display(Name = "Fragebogen zur Selbstständigkeit liegt bei advantage vor:", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string FreelancerQuestionaire { get; set; }

        [Display(Name = "Finanzamt, Sitz/Ort", Description = VertragUndGewerbe)]
        [Format(StartNewColumn = true)]
        public string TaxOfficeLocation { get; set; }

        [Display(Name = "Steuernummer", Description = VertragUndGewerbe)]
        public string TaxNumber { get; set; }

        [Display(Name = "Ust-abzugsfähig?", Description = VertragUndGewerbe)]
        public bool? TurnoverTaxDeductible { get; set; }

        [Display(Name = "Gesundheitszeugnis", Description = VertragUndGewerbe)]
        public bool? HealthCertificate { get; set; }

        [Display(Name = "Upload Gesundheitszeugnis (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        [UIHint("FileUpload")]
        [Format(StartNewColumn = true)]
        public int? UploadHealthCertificate { get { return EmployeeId; } }

        //[Display(Name = "Upload Personalausweis Vorderseite (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        //[UIHint("FileUpload")]
        //[Visibility(ShowForEditRoles = Roles.Reporter)]
        //public int? UploadIdentityCardFront { get { return EmployeeId; } }

        //[Display(Name = "Upload Personalausweis Rückseite (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        //[UIHint("FileUpload")]
        //[Visibility(ShowForEditRoles = Roles.Reporter)]
        //public int? UploadIdentityCardBack { get { return EmployeeId; } }
        #endregion

        #region D. Mobilität
        [Required]
        [Display(Name = "Führerschein vorhanden", Description = Mobilität)]
        public bool? DriversLicense { get; set; }

        [UIHint("DateIn3Fields")]
        [Display(Name = "Führerschein seit", Description = Mobilität)]
        public DateTime? DriversLicenseSince { get; set; }

        [Range(1, 31)]
        [Visibility(ShowForEdit = false)]
        public int? DriversLicenseSinceDay { get; set; }

        [Range(1, 12)]
        [Visibility(ShowForEdit = false)]
        public int? DriversLicenseSinceMonth { get; set; }

        [Range(1900, 9999)]
        [Visibility(ShowForEdit = false)]
        public int? DriversLicenseSinceYear { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (neue Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass1 { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (alte Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass2 { get; set; }

        [Display(Name = "Digitale Fahrerkarte vorhanden", Description = Mobilität)]
        [Format(StartNewColumn = true)]
        public bool? HasDigitalDriversCard { get; set; }

        [Display(Name = "PKW vorhanden", Description = Mobilität)]
        public bool? IsCarAvailable { get; set; }

        [Display(Name = "Bahncard vorhanden", Description = Mobilität)]
        public bool? IsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        [UIHint("SingleSelect")]
        [Display(Name = "Schulbildung", Description = AusbildungUndQualifikation)]
        public int? Education { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Abgeschlossene Berufsausbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Apprenticeships { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Fachrichtung/Abschluss Berufsausbildung", Description = AusbildungUndQualifikation)]
        public string OtherCompletedApprenticeships { get; set; }

        [Display(Name = "Studium", Description = AusbildungUndQualifikation)]
        [ToggleVisibility(new[]{"StudiesDetails"}, new[]{"True"})]
        public bool? Studies { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Fachrichtung/Abschluss Studium", Description = AusbildungUndQualifikation)]
        [SkRequiredIf("Studies", true, Roles = Roles.Reporter)]
        public string StudiesDetails { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Berufserfahrung", Description = AusbildungUndQualifikation)]
        public string WorkExperience { get; set; }

        [Required]
        [Display(Name = "Deutsch", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesGerman { get; set; }

        [Display(Name = "Englisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesEnglish { get; set; }

        [Display(Name = "Spanisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesSpanish { get; set; }

        [Display(Name = "Französisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesFrench { get; set; }

        [Display(Name = "Italienisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesItalian { get; set; }

        [Display(Name = "Türkisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        public int? LanguagesTurkish { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstige", Description = AusbildungUndQualifikation)]
        public string LanguagesOther { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "Software-Kenntnisse", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [EnumerationName("ITKnowledge")]
        public int? SoftwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        public string SoftwareKnownledgeDetails { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "Hardware-Kenntnisse", Description = AusbildungUndQualifikation)]
        [EnumerationName("ITKnowledge")]
        public int? HardwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        public string HardwareKnownledgeDetails { get; set; }

        [Display(Name = "Upload Zeugnisse (pdf, jpg, png)", Description = AusbildungUndQualifikation)]
        [UIHint("ZeugnisUpload")]
        public string[] UploadZeugnisse { get; set; }
        #endregion

        #region G. Auftragswunsch
        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Promotion Aktivitäten", Description = Auftragswunsch)]
        public string[] PreferredTasks { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Promotionarten", Description = Auftragswunsch)]
        public string[] PreferredTypes { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Arbeitszeiten", Description = Auftragswunsch)]
        public string[] PreferredWorkSchedule { get; set; }

        [Display(Name = "Reisebereitschaft", Description = Auftragswunsch)]
        [Format(StartNewColumn = true)]
        public bool? TravelWillingness { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Übernachtung möglich im Großraum", Description = Auftragswunsch)]
        public string[] AccommodationPossible1 { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Übernachtung möglich in folgenden Städten", Description = Auftragswunsch)]
        public string AccommodationPossible2 { get; set; }
        #endregion

        #region Persönlichkeitstest
        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [UIHint("PersonalityTest")]
        [HideSurroundingHtml]
        [Display(Name = "Persönlichkeitstest", Description = "Persönlichkeitstest")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        public string PersonalityTestAnswers { get; set; }
        #endregion

        #region Promotionerfahrung
        [UIHint("Experiences")]
        [Display(Name = "Referenzen", Description = "Referenzen")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [HideSurroundingHtml]
        public string Experiences { get; set; }
        #endregion

        #region Helper
        public void SetComplexProperties()
        {
            ArtisticAbilities = _ArtisticAbilities.SafeSplit(';');
            GastronomicAbilities = _GastronomicAbilities.SafeSplit(';');
            Sports = _Sports.SafeSplit(';');
            OtherAbilities = _OtherAbilities.SafeSplit(';');
            DriversLicenseClass1 = _DriversLicenseClass1.SafeSplit(';');
            DriversLicenseClass2 = _DriversLicenseClass2.SafeSplit(';');
            Apprenticeships = _Apprenticeships.SafeSplit(';');
            PreferredTasks = _PreferredTasks.SafeSplit(';');
            PreferredTypes = _PreferredTypes.SafeSplit(';');
            PreferredWorkSchedule = _PreferredWorkSchedule.SafeSplit(';');
            AccommodationPossible1 = _AccommodationPossible1.SafeSplit(';');
            var resolver = new PhoneNumberResolver();
            PhoneMobileNo = resolver.Resolve(_PhoneMobileNo);
            PhoneNo = resolver.Resolve(_PhoneNo);
            FaxNo = resolver.Resolve(_FaxNo);
        }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _PhoneMobileNo { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _PhoneNo { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _FaxNo { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _ArtisticAbilities { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _GastronomicAbilities { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _Sports { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _OtherAbilities { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _DriversLicenseClass1 { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _DriversLicenseClass2 { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _Apprenticeships { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _PreferredTasks { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _PreferredTypes { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _PreferredWorkSchedule { get; set; }
        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string _AccommodationPossible1 { get; set; }
        #endregion

        #region Validierung
        public static ValidationResult PhoneMobileNoValidator(PhoneNumber number, ValidationContext validationContext)
        {
            return string.IsNullOrWhiteSpace(number?.Number)
                ? new ValidationResult("Bitte eine Mobilnummer eingeben.")
                : ValidationResult.Success;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            const string dateFormat = "{0}-{1}-{2}";
            DateTime tmp;

            if (!DateTime.TryParse(string.Format(dateFormat, BirthdayYear, BirthdayMonth, BirthdayDay), out tmp) ||
                tmp < EarliestBirthdayAllowed)
            {
                yield return
                    new ValidationResult("Bitte ein gültiges Geburtsdatum angeben.",
                        new[] {"Birthday", "BirthdayDay", "BirthdayMonth", "BirthdayYear"});
            }

            if (DriversLicenseSinceDay.HasValue || DriversLicenseSinceMonth.HasValue || DriversLicenseSinceYear.HasValue)
            {
                if (!DateTime.TryParse(string.Format(dateFormat, DriversLicenseSinceYear, DriversLicenseSinceMonth, DriversLicenseSinceDay), out tmp))
                {
                    yield return
                        new ValidationResult("Bitte ein gültiges Datum angeben.",
                            new[] { "DriversLicenseSince", "DriversLicenseSinceDay", "DriversLicenseSinceMonth", "DriversLicenseSinceYear" });
                }
            }
        }
        #endregion
    }

    public class MobileEmployeeProfile
    {
        [Required]
        [Display(Name = "Anrede")]
        [UIHint("TitleSelect")]
        public string Title2 { get; set; }

        [Required]
        [Display(Name = "Vorname")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Lastname { get; set; }

        [Display(Name = "Geburtstag")]
        [UIHint("DateIn3Fields")]
        public DateTime Birthday { get; set; }

        [Range(1, 31)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayDay { get; set; }

        [Range(1, 12)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayMonth { get; set; }

        [Range(1900, 9999)]
        [Visibility(ShowForEdit = false)]
        public int BirthdayYear { get; set; }

        [Required]
        [Display(Name = "Benutzername")]
        // Erlaube Buchstaben '\w', Zahlen '\d', Leerzeichen ' ', at '@', Underscrore '_', Minus '-' und Punkt '\.'
        [RegularExpression("[\\w\\d @_\\-\\.]*", ErrorMessage = "Bitte verwende nur Buchstaben, Zahlen, Leerzeichen, Punkte, Unterstriche Minuszeichen oder '@'.")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email 1")]
        public string Email1 { get; set; }

        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email 2")]
        public string Email2 { get; set; }

        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [Display(Name = "Eigene Website")]
        public string Website { get; set; }

        public string _PhoneMobileNo { get; set; }

        [Required]
        [Display(Name = "Mobil")]
        [Format(StartNewColumn = true)]
        [UIHint("PhoneIn3Fields")]
        [CustomValidation(typeof(EditEmployeeProfile), "PhoneMobileNoValidator")]
        public PhoneNumber PhoneMobileNo { get; set; }

        public void SetComplexProperties()
        {
            var resolver = new PhoneNumberResolver();
            PhoneMobileNo = resolver.Resolve(_PhoneMobileNo);
        }

        [Required]
        [Display(Name = "Straße")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "PLZ")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Ort")]
        public string City { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "Land")]
        public int? Country { get; set; }
    }

    public class EditEmployeeProfileAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            #region MobileEmployeeProfile => Employee, EmployeeProfile
            Mapper.CreateMap<MobileEmployeeProfile, Employee>()
                .ForMember(e => e.Title, opt => opt.MapFrom(e => e.Title2));

            Mapper.CreateMap<MobileEmployeeProfile, EmployeeProfile>()
                .ForMember(e => e.Birthday, opt => opt.ResolveUsing(e => new DateTime(e.BirthdayYear, e.BirthdayMonth, e.BirthdayDay)))
                .ForMember(e => e.PhoneMobileNo, opt => opt.ResolveUsing(e => e.PhoneMobileNo.ToString()));
            #endregion

            #region Employee <=> EditEmployeeProfile
            Mapper.CreateMap<Employee, EditEmployeeProfile>()
                .ForMember(e => e.IncompleteProfile, opt => opt.MapFrom(e => e.Profile == null || e.Profile.SoftwareKnownledge == null))
                .ForMember(e => e.EmployeeId, opt => opt.MapFrom(e => e.Id))
                .ForMember(e => e.Email1, opt => opt.MapFrom(e => e.Email))
                .ForMember(e => e.Title2, opt => opt.MapFrom(e => e.Title))
                .ForSourceMember(e => e.Profile, opt => opt.Ignore());

            Mapper.CreateMap<EditEmployeeProfile, Employee>()
                .ForMember(e => e.Title, opt => opt.MapFrom(e => e.Title2))
                .ForMember(e => e.Experiences, opt => opt.Ignore());
            #endregion

            #region EmployeeProfile <=> EditEmployeeProfile
            Mapper.CreateMap<EmployeeProfile, EditEmployeeProfile>()
                .ForMember(e => e.BirthdayDay, opt => opt.Ignore())
                .ForMember(e => e.BirthdayMonth, opt => opt.Ignore())
                .ForMember(e => e.BirthdayYear, opt => opt.Ignore())
                .ForMember(e => e.DriversLicenseSinceDay, opt => opt.Ignore())
                .ForMember(e => e.DriversLicenseSinceMonth, opt => opt.Ignore())
                .ForMember(e => e.DriversLicenseSinceYear, opt => opt.Ignore())
                .ForMember(e => e.PhoneMobileNo, opt => opt.ResolveUsing<PhoneNumberResolver>().FromMember(e => e.PhoneMobileNo))
                .ForMember(e => e.PhoneNo, opt => opt.ResolveUsing<PhoneNumberResolver>().FromMember(e => e.PhoneNo))
                .ForMember(e => e.FaxNo, opt => opt.ResolveUsing<PhoneNumberResolver>().FromMember(e => e.FaxNo))
                .ForMember(e => e.MaleSize, opt => opt.MapFrom(e => e.Size))
                .ForMember(e => e.FemaleSize, opt => opt.MapFrom(e => e.Size))
                .ForMember(e => e.SkeletonContract, opt => opt.ResolveUsing(e => e.SkeletonContract.GetValueOrDefault() ? "Ja" : "Nein"))
                .ForMember(e => e.FreelancerQuestionaire, opt => opt.ResolveUsing(e => e.FreelancerQuestionaire.GetValueOrDefault() ? "Ja" : "Nein"))
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeSplit(';')))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeSplit(';')))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeSplit(';')))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeSplit(';')))
                .ForMember(e => e.Apprenticeships, opt => opt.ResolveUsing(e => e.Apprenticeships.SafeSplit(';')))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeSplit(';')))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeSplit(';')))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeSplit(';')))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeSplit(';')));

            Mapper.CreateMap<EditEmployeeProfile, EmployeeProfile>()
                .ForMember(e => e.PhoneMobileNo, opt => opt.ResolveUsing(e => e.PhoneMobileNo.ToString()))
                .ForMember(e => e.PhoneNo, opt => opt.ResolveUsing(e => e.PhoneNo != null ? e.PhoneNo.ToString() : null))
                .ForMember(e => e.FaxNo, opt => opt.ResolveUsing(e => e.FaxNo != null ? e.FaxNo.ToString() : null))
                .ForMember(e => e.Birthday, opt => opt.ResolveUsing(e => new DateTime(e.BirthdayYear, e.BirthdayMonth, e.BirthdayDay)))
                .ForMember(e => e.DriversLicenseSince, opt => opt.ResolveUsing<DriversLicenseSinceResolver>())
                .ForMember(e => e.Size, opt => opt.ResolveUsing(e =>
                {
                    switch (e.Title2)
                    {
                        case "Herr":
                            return e.MaleSize;
                        case "Frau":
                            return e.FemaleSize;
                        default:
                            return null;
                    }
                }))
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeJoin(";")))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeJoin(";")))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeJoin(";")))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeJoin(";")))
                .ForMember(e => e.Apprenticeships, opt => opt.ResolveUsing(e => e.Apprenticeships.SafeJoin(";")))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeJoin(";")))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeJoin(";")))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeJoin(";")))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeJoin(";")));
            #endregion
        }

        public class DriversLicenseSinceResolver : ValueResolver<EditEmployeeProfile, DateTime?>
        {
            protected override DateTime? ResolveCore(EditEmployeeProfile e)
            {
                return e.DriversLicenseSinceDay.HasValue && e.DriversLicenseSinceMonth.HasValue && e.DriversLicenseSinceYear.HasValue
                    ? new DateTime(e.DriversLicenseSinceYear.Value, e.DriversLicenseSinceMonth.Value, e.DriversLicenseSinceDay.Value)
                    : (DateTime?) null;
            }
        }
    }
}