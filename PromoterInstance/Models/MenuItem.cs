﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class MenuItem
    {
        // ReSharper disable InconsistentNaming
        public string url { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string color { get; set; }
        // ReSharper enable InconsistentNaming
    }
}