﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromoterInstance.Models
{
    public class MyJobViewModel
    {
        public int CampaignId { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
    }
}