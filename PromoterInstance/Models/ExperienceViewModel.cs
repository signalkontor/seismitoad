﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Remoting.Messaging;
using System.Web.Mvc;
using AutoMapper;
using Kendo.Mvc.Extensions;
using ObjectTemplate.Extensions;
using SeismitoadModel;

namespace PromoterInstance.Models
{
    public class ExperienceViewModel
    {
        /// <summary>
        /// Wert für Tätigkeit "Sonstiges". Wählt der Promoter diese Tätigkeit muss er nähere Angaben in einem Textfeld machen.
        /// </summary>
        public const int OtherTaskValue = 13;

        public int Id { get; set; }
        
        [Display(Name = "Typ")]
        public ExperienceType Type { get; set; }

        [Required, Display(Name = "Zeitraum")]
        public string Timeframe { get; set; }

        [Display(Name = "Branche")]
        public int Sector { get; set; }

        [Display(Name = "Gattung")]
        public int SectorDetails { get; set; }

        [Display(Name = "Tätigkeit")]
        [CustomValidation(typeof(ExperienceViewModel), "TaskValidation")]
        public int Task { get; set; }

        [Display(Name = "Sonstige Tätigkeit")]
        public string OtherTask { get; set; }

        [Required, Display(Name = "Aktionstitel")]
        public string Title { get; set; }
        
        [Required, Display(Name = "Produkt")]
        public string Product { get; set; }
        
        [Required, Display(Name = "Marke/Hersteller")]
        public string Brand { get; set; }
        
        [Required, Display(Name = "Agentur")]
        public string Agency { get; set; }

        public static ValidationResult TaskValidation(int task, ValidationContext validationContext)
        {
            var experience = validationContext.ObjectInstance as ExperienceViewModel;
            var invalid = experience != null && experience.Task == OtherTaskValue && string.IsNullOrWhiteSpace(experience.OtherTask);
            return invalid
                ? new ValidationResult("Bitte genauer spezifizieren", new[] { "Task", "OtherTask" })
                : ValidationResult.Success;
        }
    }

    public class ExperienceViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Experience, ExperienceViewModel>();
            Mapper.CreateMap<ExperienceViewModel, Experience>();
        }
    }
}