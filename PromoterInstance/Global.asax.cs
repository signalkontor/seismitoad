﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;
using PromoterInstance.App_Start;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.App_Start;
using SeismitoadShared.ModelBinders;
using System.Configuration;
using PromoterInstance.Controllers;
using SeismitoadCommon.ModelBinders;

namespace PromoterInstance
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            AutoFacConfig.Configure();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            DefaultRouteConfig.RegisterRoutes(RouteTable.Routes);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMappings();
            ObjectTemplateConfig.Configure();
            Database.SetInitializer<SeismitoadDbContext>(null);
            ModelBinders.Binders.DefaultBinder = new DefaultModelBinderWithHtmlValidation();
            ModelBinders.Binders.Add(typeof(string), new TrimModelBinder());
            ChangeLoggerConfig.Configure();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("de-DE");

            var application = (HttpApplication)sender;
            var username = application.Request.QueryString["username"];
            if(username != null)
            {
                application.Context.Items["username"] = username;
            }
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;
            var redirectLocation = Response.RedirectLocation;
            if(redirectLocation != null)
            {
                var pos = redirectLocation.IndexOf("username%3d");
                if (pos < 0)
                    return;

                redirectLocation = redirectLocation.Substring(0, pos);
                if (redirectLocation.EndsWith("%3f"))
                    redirectLocation = redirectLocation.Substring(0, redirectLocation.IndexOf("%3f"));
                Response.RedirectLocation = redirectLocation + "&username=" + application.Context.Items["username"];
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (custom.ToLowerInvariant() == "ismobile")
            {
                return context.GetVaryByCustomStringForOverriddenBrowser();
            }
            return base.GetVaryByCustomString(context, custom);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            ShowCustomErrorPage(Server.GetLastError());
        }

        private void ShowCustomErrorPage(Exception exception)
        {
            HttpException httpException = exception as HttpException;
            if (httpException == null)
                httpException = new HttpException(500, "Internal Server Error", exception);

            Response.Clear();
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("fromAppErrorEvent", true);

            switch (httpException.GetHttpCode())
            {
                case 404:
                    routeData.Values.Add("action", "NotFound");
                    break;

                case 500:
                default:
                    routeData.Values.Add("action", "Index");
                    break;
            }

            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;

            IController controller = new ErrorController();
            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            Response.End();
        }
    }
}