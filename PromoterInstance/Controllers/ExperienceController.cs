﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using Roles = SeismitoadShared.Constants.Roles;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class ExperienceController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public ExperienceController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        //[OutputCache(CacheProfile = "DynamicDataMedium")]
        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            return request.Json<Experience, ExperienceViewModel>(_dbContext.Experiences.Where(e => e.Employee.Id == employeeId).OrderBy(e => e.Type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, ExperienceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var employeeId = Request.GetUserData().EmployeeId;
                var experience = Mapper.Map<Experience>(model);
                _dbContext.ActiveEmployees.Single(e => e.Id == employeeId).Experiences.Add(experience);
                _dbContext.SaveChanges();
                model.Id = experience.Id;
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }        
        
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, ExperienceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var employeeId = Request.GetUserData().EmployeeId;
                var experience = _dbContext.Experiences.Single(e => e.Id == model.Id && e.Employee.Id == employeeId);
                Mapper.Map(model, experience);
                _dbContext.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, ExperienceViewModel model)
        {
            if (model != null)
            {
                var employeeId = Request.GetUserData().EmployeeId;
                // Nur löschen, wenn der Eintrag dem aktuell eingeloggten User gehört
                var experience = _dbContext.Experiences.SingleOrDefault(e => e.Id == model.Id && e.Employee.Id == employeeId);
                if (experience != null)
                {
                    _dbContext.Experiences.Remove(experience);
                    _dbContext.SaveChanges();
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public virtual JsonResult _SectorDetails(string sector)
        {
            var str = "1" + sector;
            var selectListItems = ObjectTemplate.SelectListRepository.Retrieve("SectorDetails", ViewData)
                .Where(e => e.Value.StartsWith(str));

            return Json(selectListItems, JsonRequestBehavior.AllowGet);
        }

        private static readonly SortedSet<ExperienceType> ExperienceTypesWithSingleTask = new SortedSet<ExperienceType>
        {
            ExperienceType.Merchandising, ExperienceType.Presentation, ExperienceType.MysteryShopper, ExperienceType.Trainer
        };

        public virtual JsonResult _Task(ExperienceType type)
        {
            var selectListItems = ObjectTemplate.SelectListRepository.Retrieve("Task", ViewData);
            var singleTask = ExperienceTypesWithSingleTask.Contains(type);
            var value = ((int)type).ToString();
            selectListItems = singleTask
                ? selectListItems.Where(e => e.Value.StartsWith(value))
                : selectListItems.Where(e => e.Value.StartsWith(value) || e.Value.Length < 3);
            return Json(selectListItems, JsonRequestBehavior.AllowGet);
        }
    }
}
