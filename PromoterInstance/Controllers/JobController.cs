﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using AutoMapper.QueryableExtensions;
using Dapper;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using System;
using System.Configuration;
using System.Web.WebPages;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class JobController : Controller
    {
        public virtual ActionResult Index()
        {
            return HttpContext.GetOverriddenBrowser().IsMobileDevice ? (ActionResult) View() : HttpNotFound();
        }

        // Der SELECT Teil sollte eigentlich klar sein. 
        // FROM und die JOINs sind eigentlich auch klar. Es wird aber ein LEFT OUTER JOIN benutzt, da es für den
        // aktuellen Employee ja noch keinen Eintrag in CampaignEmployees oder CampaignEmployeesExtraData geben muss
        //
        // Das WHERE ist da noch das komplizierteste. Es gibt mehrere Bedingungen unter denen dem Promoter ein Projekt
        // angeboten wird. 
        // 1. Wenn der Promoter schon Teil des Projekts ist (Projektauftrag bestätigt) wird es auf keinen Fall hier angezeigt.
        // 2. Auch wenn der Promoter irgendwie abgelehnt wurde oder selbst kein Interesse hatte wird er nicht angezeigt.
        // 3. Wird das Projekt nur angezeigt, wenn es gerade ausgeschrieben wird oder wenn der Promoter bereits im Team
        // ist oder den Projektauftrag zugeschickt bekommen hat.
        private const string Query =
            @"
SELECT [Id]
        ,CAST(CASE WHEN [CampaignEmployeeExtraData].[CampaignId] IS NULL THEN 1 ELSE 0 END as bit) as Highlighted
        ,[Name]
        ,[Duration]
        ,[Days]
        ,[Locations]
        ,[Times]
        ,[CampaignEmployees].[State] AS State
FROM Campaigns LEFT OUTER JOIN
    [CampaignEmployeeExtraData] ON [CampaignEmployeeExtraData].[CampaignId] = [Campaigns].[Id] AND [CampaignEmployeeExtraData].[EmployeeId] = @EmployeeId LEFT OUTER JOIN
    [CampaignEmployees] ON [CampaignEmployees].[CampaignId] = [Campaigns].[Id] AND [CampaignEmployees].[EmployeeId] = @EmployeeId
WHERE
    (
		[CampaignEmployees].[State] IS NULL OR
		[CampaignEmployees].[State] NOT IN ('NotInterested', 'Rejected', 'InformationRejected', 'ContractRejected', 'ContractAccepted', 'ContractCancelled') 
	)
	AND
    ([Campaigns].[IsApplyable] = 1 OR [CampaignEmployees].[Accepted] IS NOT NULL OR [CampaignEmployees].[ContractSent] IS NOT NULL)
";

        private readonly SeismitoadDbContext _dbContext;

        public JobController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            // Aus Performancegründen verwenden wir hier eine handgeschriebene SQL Query.
            // Außerdem sparen wir es uns so die Tabelle "CampaignEmployeeExtraData" in das Datenmodell aufzunehmen. Das ist
            // auch gut für die Performance, da jede Tabelle Performance frist und inbesondere diese Tabelle nur für diese
            // Funktion hier gebraucht wird.
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<ApplyableCampaign>(Query, new {Request.GetUserData().EmployeeId});
                return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }

        private JobDetailsViewModel GetJobDetails(int campaignId, int employeeId)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                return connection
                    .Query<JobDetailsViewModel>(@"SELECT [Campaigns].[Id]
      ,[Campaigns].[GradualBooking]
      ,[Campaigns].[Name]
      ,[Campaigns].[IsApplyable]
      ,[Customers].[Name] As CustomerName
      ,[Description]
      ,[Duration]
      ,[EmployeesPerAssignment]
      ,[Requirements]
      ,[Times]
      ,[Days]
      ,ISNULL([CampaignEmployees].[State], 'New') AS State
      ,[ContactPMName1]
      ,[ContactPMPhone1]
      ,[ContactPMEmail1]
      ,[ContactPMRemark1]
      ,[ContactPMName2]
      ,[ContactPMPhone2]
      ,[ContactPMEmail2]
      ,[ContactPMRemark2]
      ,[ContactPMName3]
      ,[ContactPMPhone3]
      ,[ContactPMEmail3]
      ,[ContactPMRemark3]
      ,[ContactPMName4]
      ,[ContactPMPhone4]
      ,[ContactPMEmail4]
      ,[ContactPMRemark4]
      ,[ContactADName1]
      ,[ContactADPhone1]
      ,[ContactADEmail1]
      ,[ContactADRemark1]
      ,[ContactADName2]
      ,[ContactADPhone2]
      ,[ContactADEmail2]
      ,[ContactADRemark2]
      ,[ContactADName3]
      ,[ContactADPhone3]
      ,[ContactADEmail3]
      ,[ContactADRemark3]
      ,[ContactADName4]
      ,[ContactADPhone4]
      ,[ContactADEmail4]
      ,[ContactADRemark4]
      ,[InstanceUrl]
  FROM
  [Campaigns] INNER JOIN
  [Customers] ON [Customers].[Id] = [Campaigns].[Customer_Id] LEFT OUTER JOIN
  [CampaignEmployees] ON [CampaignEmployees].[CampaignId] = [Campaigns].[Id] AND [CampaignEmployees].[EmployeeId] = @EmployeeId
  WHERE [Campaigns].[Id] = @CampaignId", new { CampaignId = campaignId, EmployeeId = employeeId })
                    .Single();
            }
        }

        private static readonly string[] AllowedStatesForDetails = { "Accepted", "ContractSent", "ContractAccepted", "ContractCancelled" };

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult Details(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;

            MarkSeen(id, employeeId);

            var viewModel = GetJobDetails(id, employeeId);

            if(viewModel.State == "Rejected")
                RedirectToAction(MVC.Home.Index());

            if (!AllowedStatesForDetails.Contains(viewModel.State) && !viewModel.IsApplyable)
                return RedirectToAction(MVC.Home.Index());

            var trainings = _dbContext.TrainingParticipants
                .Where(e => e.State == "eingeladen" || e.State == "bestätigt" || e.State == "teilgenommen")
                .Where(e => e.CampaignEmployee.EmployeeId == employeeId && e.CampaignEmployee.CampaignId == id)
                .Include(e => e.TrainingDay.Training)
                .ToList();

            ViewBag.Trainings = trainings;
            return View(viewModel);
        }

        public virtual ActionResult NotInterested(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var campaignEmployee = _dbContext.CampaignEmployees.SingleOrDefault(e => e.CampaignId == id && e.EmployeeId == employeeId);

            if (campaignEmployee == null)
            {
                campaignEmployee = new CampaignEmployee
                {
                    EmployeeId = employeeId,
                    CampaignId = id,
                };
                _dbContext.CampaignEmployees.Add(campaignEmployee);
            }
            campaignEmployee.NotInterested = LocalDateTime.Now;
            _dbContext.SaveChanges();

            return RedirectToAction(MVC.Home.Index());
        }


        public virtual ActionResult InformationRequested(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var campaignEmployee = _dbContext.CampaignEmployees.SingleOrDefault(e => e.CampaignId == id && e.EmployeeId == employeeId);
            
            if (campaignEmployee == null)
            {
                campaignEmployee = new CampaignEmployee
                {
                    EmployeeId = employeeId,
                    CampaignId = id,
                };
                _dbContext.CampaignEmployees.Add(campaignEmployee);
            }

            // Alles resetten was verhindern könnte, dass der Promoter nicht den Status "InformationRequested" bekommt.
            campaignEmployee.InformationSent = null;
            campaignEmployee.InformationRejected = null;
            campaignEmployee.ContractSent = null;
            campaignEmployee.ContractCancelled = null;
            campaignEmployee.ContractAccepted = null;
            campaignEmployee.Rejected = null;
            campaignEmployee.Accepted = null;
            campaignEmployee.ContractRejected = null;
            campaignEmployee.NotInterested = null;
            campaignEmployee.InformationRequested = LocalDateTime.Now;
            _dbContext.SaveChanges();

            return View();
        }

        private ActionResult ApplyInternal(int campaignId, int employeeId)
        {
            var viewModel = GetJobDetails(campaignId, employeeId);
            if (viewModel.State != "InformationSent" && ViewBag.SelectAssignments == null)
                return RedirectToAction(MVC.Home.Index());

            return View(Views.ViewNames.Apply, viewModel);
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult Apply(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            if (_dbContext.CampaignEmployees.Any(e => e.CampaignId == id && e.EmployeeId == employeeId && e.Applied.HasValue))
            {
                // Da der User die Bewerbungsfunktion nicht sieht, wenn er sich schon beworben hat sollt er hier nie landen.
                return RedirectToAction(MVC.Job.AlreadyApplied());
            }

            return ApplyInternal(id, employeeId);
        }

        [HttpPost]
        public virtual ActionResult Apply(int id, string locationsText, string locationsText2, int[] l)
        {
            var checkedLocations = l ?? new int[0];
            if (checkedLocations.Length == 0 && string.IsNullOrWhiteSpace(locationsText) && string.IsNullOrWhiteSpace(locationsText2))
            {
                ModelState.AddModelError("General", "Bitte gib an wo wir Dich einsetzten können.");
            }

            var employeeId = Request.GetUserData().EmployeeId;

            if (ModelState.IsValid)
            {
                var campaignEmployee = _dbContext.CampaignEmployees
                    .Include(e => e.Locations)
                    .Include(e => e.Campaign)
                    .Single(e => e.CampaignId == id && e.EmployeeId == employeeId);

                campaignEmployee.Applied = LocalDateTime.Now;
                if (checkedLocations.Length > 0)
                {
                    var locations = _dbContext.Locations.Where(e => checkedLocations.Contains(e.Id)).ToList();
                    campaignEmployee.Locations = locations;
                    // Wir speichern die Bewerber Städte auch noch im String Property, weil wir das so in
                    // der MasterInstance besser im Grid anzeigen können.
                    campaignEmployee.LocationsText = string.Join(", ", locations.Select(e => e.City).Distinct());
                    // Wenn auch noch was im Textfeld steht (möglich bei sukzessiver Buchung) hängen wir den Inhalt des Textfeld noch an.
                    if (!string.IsNullOrWhiteSpace(locationsText2))
                    {
                        campaignEmployee.LocationsText += ". " + locationsText2;
                    }
                }
                else
                {
                    campaignEmployee.LocationsText = string.IsNullOrWhiteSpace(locationsText)
                        ? locationsText2
                        : locationsText;
                }

                _dbContext.SaveChanges();

                if (checkedLocations.Length > 0 && campaignEmployee.Campaign.GradualBooking)
                {
                    if (HttpContext.GetOverriddenBrowser().IsMobileDevice)
                    {
                        return RedirectToAction(MVC.Job.Dates(id));
                    }
                    ViewBag.SelectAssignments = true;
                    return ApplyInternal(id, employeeId);
                }

                return RedirectToAction(MVC.Job.ApplicationReceived());
            }
            return ApplyInternal(id, employeeId);
        }

        public virtual ActionResult Dates(int id)
        {
            return View(GetPotentialAssignments(id));
        }

        [HttpPost]
        public virtual ActionResult Dates(int id, string[] a)
        {
            return _SelectAssignments(id, a);
        }

        public class PotentialAssignmentModel
        {
            public int AssignmentId { get; set; }
            public DateTime Date { get; set; }
            public string Name { get; set; }
            public string Street { get; set; }
            public string PostalCode { get; set; }
            public string City { get; set; }
        }

        private IList<PotentialAssignmentModel> GetPotentialAssignments(int campaignId)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                return connection.Query<PotentialAssignmentModel>(@"SELECT DISTINCT	[Id] as AssignmentId
    ,[DateStart] as Date
    ,[LocationName] as Name
    ,[LocationStreet] as Street
    ,[LocationPostalCode] as PostalCode
    ,[LocationCity] as City
FROM
	[vw_Assignments] INNER JOIN
	[CampaignEmployeeLocations] ON 
		[CampaignEmployeeLocations].[Location_Id] = [vw_Assignments].[LocationId] AND [CampaignEmployeeLocations].[CampaignEmployee_EmployeeId] = @EmployeeId
WHERE
	[State] = 0 AND
	[CampaignId] = @CampaignId AND
	[AssignedEmployeeCount] < [EmployeeCount]
ORDER BY LocationPostalCode, LocationCity, LocationStreet, LocationName, DateStart",
                    new {CampaignId = campaignId, EmployeeId = employeeId}).ToList();
            }
        }

        public virtual ActionResult _SelectAssignments(int campaignId)
        {
            return PartialView(GetPotentialAssignments(campaignId));
        }

        [HttpPost]
        public virtual ActionResult _SelectAssignments(int campaignId, string[] a)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var employee = _dbContext.Employees
                .Where(e => e.Id == employeeId)
                .Select(e => new { e.Firstname, e.Lastname })
                .Single();

            var campaign = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Select(e => new {e.Name, Number = e.Customer.Number + e.Number})
                .Single();

            a = a ?? new[]
            {
                "An den oben angegebenen Tagen kann ich leider keinen Einsatz übernehmen, bin aber grundsätzlich an Einsätzen in den Aktionsorten interessiert."
            };

            new MailController().AppliedAssignments(campaign.Name, campaign.Number, employee.Firstname + " " + employee.Lastname, a).Deliver();
            return RedirectToAction(MVC.Job.ApplicationReceived());
        }

        public virtual ActionResult ApplicationReceived()
        {
            return View();
        }

        public virtual ActionResult AlreadyApplied()
        {
            // Da der User die Bewerbungsfunktion nicht sieht, wenn er sich schon beworben hat sollte er hier eigentlich nie landen.
            // Wir zeigen trotzdem einen Hinweis an.
            return View();
        }

        [OutputCache(CacheProfile = "DynamicDataMedium")]
        public virtual ActionResult _ReadLocations([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<ApplyToLocationViewModel>(@"SELECT
    Locations.Id,
    Locations.Name,
    Locations.Street,
    Locations.PostalCode,
    Locations.City
FROM
    CampaignLocations INNER JOIN
    Locations ON CampaignLocations.Location_Id = Locations.Id
WHERE
    CampaignLocations.Campaign_Id = @CampaignId AND
    (
		-- a) alle Location zeigen die noch gar keine Einsätze haben
		NOT EXISTS (SELECT Id FROM Assignments WHERE Assignments.CampaignLocation_CampaignId = @CampaignId AND Assignments.CampaignLocation_LocationId = Locations.Id AND Assignments.[State] = 0) OR
		-- oder b) die mindestens einen Einsatz haben der noch nicht vollständig besetzt ist
		EXISTS (SELECT Id FROM vw_Assignments v WHERE v.CampaignId = @CampaignId AND v.LocationId = Locations.Id AND v.AssignedEmployeeCount < v.EmployeeCount AND v.[State] = 0)
	)", new {CampaignId = campaignId});
                return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }

        public virtual ActionResult AcceptContract(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var campaignEmployee = _dbContext.CampaignEmployees.Single(e => e.CampaignId == id && e.EmployeeId == employeeId);
            campaignEmployee.ContractAccepted = LocalDateTime.Now;
            _dbContext.SaveChanges();
            return RedirectToAction(MVC.Job.ContractAccepted());
        }

        public virtual ActionResult RejectContract(int id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var campaignEmployee = _dbContext.CampaignEmployees.Single(e => e.CampaignId == id && e.EmployeeId == employeeId);
            campaignEmployee.ContractRejected = LocalDateTime.Now;
            _dbContext.SaveChanges();
            return RedirectToAction(MVC.Home.Index());
        }

        public virtual ActionResult ContractAccepted()
        {
            return View();
        }

        private void MarkSeen(int campaignId, int employeeId)
        {
            const string command = "INSERT INTO CampaignEmployeeExtraData (CampaignId, EmployeeId) VALUES(@p0, @p1) ";
            try
            {
                _dbContext.Database.ExecuteSqlCommand(command, campaignId, employeeId);
            }
            catch
            {
                // Wir ignorieren alle Exception, da eigentlich nur eine Exception fliegen sollte, wenn ein doppelter
                // Schlüssel eingetragen wird. Sollte sichergestellt sein, dass diese Methode nur aufgerufen wird, wenn 
                // es noch keinen Eintrag mit den entsprechenden Ids gibt, dann könnte man diese Exception auch in
                // Elmah loggen. Sollte es aber "normal" sein, dass hier eine Exception fliegt macht das keinen Sinn.
            }
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult DownloadPdf(int campaignId)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            var path = Server.MapPath("~/CampaignAssets/" + campaignId + "/GeneratedContracts");
            var filename = Path.Combine(path, employeeId + ".pdf");
            return File(filename, "application/pdf");
        }

        [OutputCache(CacheProfile = "DynamicDataLongUserAgnostic")]
        public virtual ActionResult Calendar(int id)
        {
            var trainingParticipant = _dbContext.TrainingParticipants.Where(e => e.Id == id)
                .Include(e => e.TrainingDay.Training)
                .Single();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Termin.ics\"");
            return Content(ToICalString(trainingParticipant), "text/calendar");
        }

        private static string ToICalString(TrainingParticipant participant)
        {
            var iCalendar = new iCalendar();
            var employee = participant.CampaignEmployee.Employee;
            var attendee = new Attendee("MAILTO:not-disclosed@example.com")
            {
                Role = "REQ-PARTICIPANT",
                CommonName = employee.Lastname + ", " + employee.Firstname
            };
            var @event = new Event
            {
                Created = iCalDateTime.Now,
                Start = new iCalDateTime(participant.TrainingDay.DateFrom),
                End = new iCalDateTime(participant.TrainingDay.DateTil),
                Description = participant.TrainingDay.Training.Title,
                Summary = participant.TrainingDay.Remarks,
                Location = participant.TrainingDay.Location,
                Attendees = new IAttendee[] { attendee }
            };

            iCalendar.AddChild(@event);

            var iCalendarSerializer = new iCalendarSerializer();
            return iCalendarSerializer.SerializeToString(iCalendar);
        }
    }
}