﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using PromoterInstance.Models;
using SeismitoadModel.DatabaseContext;
using System.Linq;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class FileController : Controller
    {
        public FileController()
        {
        }

        private static readonly string[] ValidExtensions = new[] { ".jpg", ".png", ".gif", ".pdf" };

        public static void GetUrls(int employeeId, string name, HttpServerUtilityBase server, out string displayUrl, out string downloadUrl)
        {
            var appPath = HostingEnvironment.ApplicationVirtualPath;
            if (!appPath.EndsWith("/")) appPath += "/";
            var baseUrl = GetBaseUrl(appPath, employeeId, name);
            foreach (var extension in ValidExtensions)
            {
                var url = baseUrl + extension;
                if (System.IO.File.Exists(server.MapPath(url)))
                {
                    displayUrl = extension == ".pdf"
                        ? appPath + "Images/Icons/pdf.png"
                        : url;

                    downloadUrl = url;
                    return;
                }
            }
            displayUrl = baseUrl + ".jpg";
            downloadUrl = appPath + "Images/404.jpg";
        }

        public static string GetBaseUrl(string appPath, int employeeId, string name)
        {
            return $"{appPath}Uploads/Employee/{employeeId}/{name}";
        }

        ImageFormat GetImageFormat(string filename)
        {
            try
            {
                using (var newImage = Image.FromFile(filename))
                {
                    return newImage.RawFormat;
                }
            }
            catch (OutOfMemoryException)
            {
                //The file does not have a valid image format.
                //-or- GDI+ does not support the pixel format of the file
                return null;
            }
        }

        public virtual ActionResult Save(bool zeugnisse = false)
        {
            if(Request.Files.Count != 1 || Request.Files[0] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var file = Request.Files[0];

            var name = zeugnisse
                ? Path.GetFileNameWithoutExtension(Request.Files[0].FileName)
                : Request.Files.Keys[0];

            var extension = Path.GetExtension(file.FileName).ToLower();
            var notPdf = extension != "pdf";
            var isPhoto = name.Contains("Photo");

            if (isPhoto && extension != ".jpg")
            {
                return Content("Ungültiger Dateityp, erlaubt ist jpg.");
            }
            var validExtension = ValidExtensions.Contains(extension);
            if (!validExtension)
            {
                return Content("Ungültiger Dateityp. Erlaubt sind jpg, png, gif und pdf");
            }

            var directory = GetDirectory(Request.GetUserData().EmployeeId, zeugnisse);
            Directory.CreateDirectory(directory);
            var filename = Path.Combine(directory, name + extension);
            if (!zeugnisse)
            {
                foreach (var fileName in Directory.GetFiles(directory, name + ".*"))
                {
                    System.IO.File.Delete(fileName);
                }
            }
            file.SaveAs(filename);

            if (notPdf)
            {
                var imageFormat = GetImageFormat(filename);
                if (imageFormat == null)
                {
                    return Content("Ungültige Datei.");
                }
                var validFormat = isPhoto
                    ? imageFormat.Equals(ImageFormat.Jpeg)
                    : imageFormat.Equals(ImageFormat.Jpeg) || imageFormat.Equals(ImageFormat.Png) || imageFormat.Equals(ImageFormat.Gif);
                if (!validFormat)
                {
                    return Content("Ungültige Datei.");
                }
            }

            if (notPdf)
            {
                try
                {
                    var processStart = new ProcessStartInfo(Server.MapPath("~/App_Data/mogrify.exe"), $"-resize \"3200x3200>\" \"{filename}\"")
                    {
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        LoadUserProfile = false,
                        RedirectStandardOutput = true
                    };
                    var process = Process.Start(processStart);
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit(20 * 1000);
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                }
            }
            return Content("");
        }

        // Der Kendo-Uploader sendet beim Remove den Dateinamen als fileNames, daher muss dieser Parameter hier so heißen
        public virtual ActionResult Remove(string fileNames, bool zeugnisse = false)
        {
            var id = Request.GetUserData().EmployeeId;
            foreach (var fileName in Directory.GetFiles(GetDirectory(id, zeugnisse), fileNames + ".*"))
            {
                System.IO.File.Delete(fileName);
            }
            return Content("");
        }

        private string GetDirectory(int id, bool zeugnisse)
        {
            return GetDirectory(Server, id, zeugnisse);
        }

        public static string GetDirectory(HttpServerUtilityBase server, int id, bool zeugnisse)
        {
            return server.MapPath(string.Format("~/Uploads/Employee/{0}/{1}", id, zeugnisse ? "Zeugnisse/" : ""));
        }

    }
}
