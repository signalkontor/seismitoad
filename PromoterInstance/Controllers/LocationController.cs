﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Dapper;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class LocationController : Controller
    {
        public LocationController()
        {
        }

        private static string ToVCardString(dynamic location)
        {
            var builder = new StringBuilder();
            builder.AppendLine("BEGIN:VCARD");
            builder.AppendLine("VERSION:2.1");
            // Name
            builder.AppendLine("N:" + location.Name2 + ";" + location.Name);
            // Full name
            builder.AppendLine("FN:" + location.Name + " " + location.Name2);
            // Address
            builder.Append("ADR;WORK;PREF:;;");
            builder.Append(location.Street + ";");
            builder.Append(location.City + ";;");
            builder.Append(location.PostalCode + ";");
            string country;
            if (string.IsNullOrWhiteSpace(location.PostalCode))
            {
                country = "Deutschland";
            }
            else
            {
                switch (((string) location.PostalCode)[0])
                {
                    case 'a':
                    case 'A':
                        country = "Österreich";
                        break;
                    case 'd':
                    case 'D':
                    default:
                        country = "Deutschland";
                        break;
                }
            }
            builder.AppendLine(country);
            // Other data
            builder.AppendLine("ORG:" + (location.LocationGroup ?? ""));
            //builder.AppendLine("NOTE:" + Notes);
            //builder.AppendLine("TEL;WORK;VOICE:" + (contact != null ? contact.Phone : Phone));
            builder.AppendLine("URL;" + location.Website);
            //builder.AppendLine("EMAIL;WORK;INTERNET:" + Email);
            builder.AppendLine("END:VCARD");
            return builder.ToString();
        }

        [OutputCache(CacheProfile = "DynamicDataLongUserAgnostic")]
        public virtual ActionResult VCard(int locationId)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var location = connection.Query(
                    @"SELECT Locations.Name, Name2, Street, City, PostalCode, LocationGroups.Name AS LocationGroup, Website
FROM Locations LEFT OUTER JOIN LocationGroups ON Locations.LocationGroup_Id = LocationGroups.Id
WHERE Locations.Id = @LocationId", new {LocationId = locationId}).Single();

                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + location.Name + ".vcf\"");
                return Content(ToVCardString(location), "text/vcard", Encoding.GetEncoding("windows-1257"));
            }
        }
    }
}
