﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using Autofac.Features.ResolveAnything;
using AutoMapper;
using Dapper;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class ProfileController : Controller
    {
        #region EmployeeProfileQuery
        private const string EmployeeProfileQuery = @"
SELECT [Employees].[Id] as EmployeeId
      ,CASE WHEN [SoftwareKnownledge] IS NULL THEN 1 ELSE 0 END as IncompleteProfile
      ,[Title] as Title2
      ,[Firstname]
      ,[Lastname]
      ,(SELECT [UserName] FROM [seismitoad_aspnet].[dbo].[aspnet_Users] WHERE [UserId] = [ProviderUserKey]) as Username
      ,[Email] as Email1
      ,[Birthday]
      ,[Email2]
      ,[Website]
      ,[PhoneMobileNo] as _PhoneMobileNo
      ,[PhoneNo] as _PhoneNo
      ,[FaxNo] as _FaxNo
      ,[Street]
      ,[PostalCode]
      ,[City]
      ,[Country]
      ,[Height]
      ,[Size] as MaleSize
      ,[Size] as FemaleSize
      ,[ShirtSize]
      ,[JeansWidth]
      ,[JeansLength]
      ,[ShoeSize]
      ,[HairColor]
      ,[VisibleTattoos]
      ,[FacialPiercing]
      ,[OtherAbilitiesText]
      ,CASE WHEN [SkeletonContract] = 1 THEN 'Ja' ELSE 'Nein' END as SkeletonContract
      ,CASE WHEN [FreelancerQuestionaire] = 1 THEN 'Ja' ELSE 'Nein' END as FreelancerQuestionaire
      ,[ValidTradeLicense]
      ,[TaxOfficeLocation]
      ,[TaxNumber]
      ,[TurnoverTaxDeductible]
      ,[TurnoverTaxDeductible]
      ,[HealthCertificate]
      ,[DriversLicense]
      ,[DriversLicenseSince]
      ,[HasDigitalDriversCard]
      ,[IsCarAvailable]
      ,[IsBahncardAvailable]
      ,[Education]
      ,[OtherCompletedApprenticeships]
      ,[Studies]
      ,[StudiesDetails]
      ,[WorkExperience]
      ,[LanguagesGerman]
      ,[LanguagesEnglish]
      ,[LanguagesSpanish]
      ,[LanguagesFrench]
      ,[LanguagesItalian]
      ,[LanguagesTurkish]
      ,[LanguagesOther]
      ,[SoftwareKnownledge]
      ,[SoftwareKnownledgeDetails]
      ,[HardwareKnownledge]
      ,[HardwareKnownledgeDetails]
      ,[TravelWillingness]
      ,[AccommodationPossible2]
      ,[PersonalityTestAnswers]
      ,[ArtisticAbilities] as _ArtisticAbilities
      ,[GastronomicAbilities] as _GastronomicAbilities
      ,[Sports] as _Sports
      ,[OtherAbilities] as _OtherAbilities
      ,[DriversLicenseClass1] as _DriversLicenseClass1
      ,[DriversLicenseClass2] as _DriversLicenseClass2
      ,[Apprenticeships] as _Apprenticeships
      ,[PreferredTasks] as _PreferredTasks
      ,[PreferredTypes] as _PreferredTypes
      ,[PreferredWorkSchedule] as _PreferredWorkSchedule
      ,[AccommodationPossible1] as _AccommodationPossible1
      ,[Skype]
  FROM [Employees] LEFT OUTER JOIN
	   [EmployeeProfiles] ON [Employees].[Id] = [EmployeeProfiles].[Id]
WHERE Employees.Id = @EmployeeId";

        private const string EmployeeProfileMobileQuery = @"
SELECT [Title] as Title2
      ,[Firstname]
      ,[Lastname]
      ,[Birthday]
      ,(SELECT [UserName] FROM [seismitoad_aspnet].[dbo].[aspnet_Users] WHERE [UserId] = [ProviderUserKey]) as Username
      ,[Email] as Email1
      ,[Email2]
      ,[Skype]
      ,[Website]
      ,[Street]
      ,[PostalCode]
      ,[City]
      ,[Country]
      ,[PhoneMobileNo] as _PhoneMobileNo
  FROM [Employees] LEFT OUTER JOIN
	   [EmployeeProfiles] ON [Employees].[Id] = [EmployeeProfiles].[Id]
WHERE Employees.Id = @EmployeeId";

        #endregion


        private readonly SeismitoadDbContext _dbContext;
        private readonly IMembershipService _membershipService;

        public ProfileController(SeismitoadDbContext dbContext, IMembershipService membershipService)
        {
            _dbContext = dbContext;
            _membershipService = membershipService;
        }

        [OutputCache(CacheProfile = "NoCache")]
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Reporter)]
        public virtual ActionResult Index()
        {
            if (HttpContext.GetOverriddenBrowser().IsMobileDevice)
                return RedirectToAction(MVC.Profile.Mobile());

            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<EditEmployeeProfile>(EmployeeProfileQuery, new { Request.GetUserData().EmployeeId }).Single();
                model.SetComplexProperties();
                Zeugnisse(model);
                return View(model);
            }
        }

        private void Zeugnisse(EditEmployeeProfile model)
        {
            var zeugnisDir = FileController.GetDirectory(Server, model.EmployeeId, true);
            if (Directory.Exists(zeugnisDir))
            {
                model.UploadZeugnisse = Directory.GetFiles(zeugnisDir).Select(Path.GetFileName).ToArray();
            }
        }

        [HttpPost]
        public virtual ActionResult Index(EditEmployeeProfile model)
        {
            var employee = this.GetLoggedInEmployee(_dbContext);
            var phoneMobileNoModelState = ModelState["PhoneMobileNo"];
            if (phoneMobileNoModelState != null && phoneMobileNoModelState.Errors.Any())
            {
                ModelState.AddModelError("PhoneMobileNo.CountryCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.AreaCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.Number", "Bitte eine Mobilnummer eingeben.");
            }
            UpdateProfile(model, employee);
            model.EmployeeId = employee.Id;
            Zeugnisse(model);
            var userNameChange = !User.Identity.Name.Equals(model.Username, StringComparison.InvariantCultureIgnoreCase);
            if (userNameChange && _membershipService.GetUser(model.Username) != null)
            {
                ModelState.AddModelError("Username", "Der Benutzername ist bereits vergeben.");
            }
            if(ModelState.IsValid)
            {
                if (userNameChange)
                {
                    _dbContext.Database.ExecuteSqlCommand(@"UPDATE [seismitoad_aspnet].[dbo].[aspnet_Users]
                                                    SET [UserName] = @Username,
                                                        [LoweredUserName] = LOWER(@Username)
                                                    WHERE [UserId] = @UserId",
                    new SqlParameter("@Username", model.Username), new SqlParameter("@UserId", employee.ProviderUserKey));

                    Session.Abandon();
                    var userData = new UserData
                    {
                        EmployeeId = employee.Id,
                        Name = employee.Firstname
                    };
                    Response.SetAuthCookie(model.Username, false, userData);
                    return RedirectToAction(MVC.Profile.Index());
                }
            }
            return View(model);
        }

        private void UpdateMobileProfile(MobileEmployeeProfile model, Employee employee)
        {
            if (employee.Profile == null)
                employee.Profile = new EmployeeProfile();

            if (ModelState["InvalidCharactersInInput"] != null)
            {
                ViewBag.ErrorMessage = "Das Profil wurde nicht gespeichert, da mindestens ein Eingabefeld ein ungültiges Zeichen enthält."; ;
                return;
            }

            Mapper.Map(model, employee);
            Mapper.Map(model, employee.Profile);

            var membershipUser = _membershipService.GetUser(employee.ProviderUserKey);
            membershipUser.Email = model.Email1;
            try
            {
                _membershipService.UpdateUser(membershipUser);
                _dbContext.SaveChanges();
                ViewBag.Message = ModelState.IsValid ? "Profil gespeichert." : "Profil gespeichert. Fehlerhafte Eingaben wurden nicht gespeichert.";
            }
            catch (ProviderException)
            {
                ModelState.AddModelError("Email1", "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e);
            }
        }

        private void UpdateProfile(EditEmployeeProfile model, Employee employee)
        {
            if (employee.Profile == null)
                employee.Profile = new EmployeeProfile();

            TryUpdatePersonalityTest(model, employee);
            var uploadDir = FileController.GetDirectory(Server, employee.Id, false);
            if (!Directory.Exists(uploadDir) || Directory.GetFiles(uploadDir, "UploadPortraitPhoto.*").Length == 0)
            {
                ModelState.AddModelError("UploadPortraitPhoto", "Bitte ein Portraitfoto hochladen.");
            }

            if (!RequiredFieldsValid())
            {
                ViewBag.ErrorMessage = "Das Profil wurde nicht gespeichert, da nicht alle Pflichtfelder ausgefüllt wurden.";
                return;
            }

            if(ModelState["InvalidCharactersInInput"] != null)
            {
                ViewBag.ErrorMessage = "Das Profil wurde nicht gespeichert, da mindestens ein Eingabefeld ein ungültiges Zeichen enthält."; ;
                return;
            }

            var addressChanged = AddressChanged(model, employee.Profile);

            Mapper.Map(model, employee);
            Mapper.Map(model, employee.Profile);

            if (addressChanged)
            {
                // Geokoordinaten resetten, damit sie vom Job neu berechnet werden.
                _dbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[CampaignEmployees] SET [AddressChanged] = 1 WHERE [EmployeeId] = @EmployeeId", new SqlParameter("@EmployeeId", employee.Id));
                employee.Profile.GeographicCoordinates = null;
                employee.Profile.GeocodingFailed = false;
            }

            var membershipUser = _membershipService.GetUser(employee.ProviderUserKey);
            membershipUser.Email = model.Email1;
            try
            {
                _membershipService.UpdateUser(membershipUser);
                _dbContext.SaveChanges();
                ViewBag.Message = ModelState.IsValid ? "Profil gespeichert." : "Profil gespeichert. Fehlerhafte Eingaben wurden nicht gespeichert.";
            }
            catch (ProviderException)
            {
                ModelState.AddModelError("Email1", "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e);
            }
        }

        public virtual ActionResult Delete()
        {
            return View();
        }

        public virtual ActionResult _Delete()
        {
            var employee = this.GetLoggedInEmployee(_dbContext);
            employee.State = EmployeeState.Inactive;
            _dbContext.SaveChanges();

            var mail = new MailController().ProfileDeletedMail(employee);
            mail.Deliver();
            HttpContext.ClearOverriddenBrowser();
            FormsAuthentication.SignOut();
            return Content("OK");
        }

        private static readonly SortedSet<string> RequiredFields = new SortedSet<string>()
        {
            "Title",
            "Firstname",
            "Lastname",
            "Birthday",
            "Email1",
            "PhoneMobileNo.CountryCode",
            "PhoneMobileNo.AreaCode",
            "PhoneMobileNo.Number",
            "Street",
            "PostalCode",
            "City",
            "Country",
            "Height",
            "ShirtSize",
            "ValidTradeLicense",
            "DriversLicense",
            "LanguagesGerman",
            "SoftwareKnownledge",
            "HardwareKnowledge",
            "UploadPortraitPhoto"
        };

        private bool RequiredFieldsValid()
        {
            if (ModelState.IsValid)
                return true;

            var propertiesWithErrors =
                ModelState.Where(e => e.Value.Errors != null && e.Value.Errors.Any()).Select(e => e.Key);

            return !propertiesWithErrors.Intersect(RequiredFields).Any();
        }

        private void TryUpdatePersonalityTest(EditEmployeeProfile employeeProfile, Employee employee)
        {
            // Erstmal alte Testdaten in EditEmployeeProfile kopieren, da diese sonst später vom Automapper gelöscht werden.
            employeeProfile.PersonalityTestAnswers = employee.Profile.PersonalityTestAnswers;

            if (!Request.Unvalidated.Form.AllKeys.Any(e => e.StartsWith("PersonalityTestAnswers.Radio")))
                return;

            var personalityTestAnswers = new int[PersonalityTest.Questions.Length];
            for (var i = 0; i < personalityTestAnswers.Length; i++)
            {
                var paramName = "PersonalityTestAnswers.Radio" + i;
                var paramValue = Request.Params[paramName];
                var valid = int.TryParse(paramValue, out personalityTestAnswers[i]);
                var valueProviderResult = new ValueProviderResult(paramValue, valid ? paramValue : null,
                                                                  System.Threading.Thread.CurrentThread.CurrentCulture);
                ModelState.Add(paramName, new ModelState { Value = valueProviderResult });
                if (!valid)
                {
                    ModelState.AddModelError("PersonalityTestAnswers", "Bitte alle Felder ausfüllen");
                    ModelState.AddModelError(paramName, "Bitte ausfüllen.");
                }
            }

            // Wenn das Profil ungültig ist, brauchen wir den Test und die resultierende Bewertung nicht speichern.
            if (!ModelState.IsValid)
                return;

            employeeProfile.PersonalityTestAnswers = string.Join("", personalityTestAnswers);
            var rating = PersonalityTest.CalculatePersonalityTestRating(employeeProfile.PersonalityTestAnswers);
            employee.Ratings.Add(rating);
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult Mobile()
        {
            if (!HttpContext.GetOverriddenBrowser().IsMobileDevice)
                return RedirectToAction(MVC.Profile.Index());

            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<MobileEmployeeProfile>(EmployeeProfileMobileQuery, new { Request.GetUserData().EmployeeId }).Single();
                model.SetComplexProperties();
                return View(model);
            }
        }

        [HttpPost]
        public virtual ActionResult Mobile(MobileEmployeeProfile model)
        {
            var employee = this.GetLoggedInEmployee(_dbContext);
            var phoneMobileNoModelState = ModelState["PhoneMobileNo"];
            if (phoneMobileNoModelState != null && phoneMobileNoModelState.Errors.Any())
            {
                ModelState.AddModelError("PhoneMobileNo.CountryCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.AreaCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.Number", "Bitte eine Mobilnummer eingeben.");
            }
            UpdateMobileProfile(model, employee);

            var userNameChange = !User.Identity.Name.Equals(model.Username, StringComparison.InvariantCultureIgnoreCase);
            if (userNameChange && _membershipService.GetUser(model.Username) != null)
            {
                ModelState.AddModelError("Username", "Der Benutzername ist bereits vergeben.");
            }
            if (ModelState.IsValid)
            {
                if (userNameChange)
                {
                    _dbContext.Database.ExecuteSqlCommand(@"UPDATE [seismitoad_aspnet].[dbo].[aspnet_Users]
                                                    SET [UserName] = @Username,
                                                        [LoweredUserName] = LOWER(@Username)
                                                    WHERE [UserId] = @UserId",
                    new SqlParameter("@Username", model.Username), new SqlParameter("@UserId", employee.ProviderUserKey));

                    Session.Abandon();
                    var userData = new UserData
                    {
                        EmployeeId = employee.Id,
                        Name = employee.Firstname
                    };
                    Response.SetAuthCookie(model.Username, false, userData);
                    return RedirectToAction(MVC.Profile.Index());
                }
            }


            return View();
        }

        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Incomplete()
        {
            return View();
        }

        public virtual ActionResult _Card()
        {
            var employeeId = Request.GetUserData().EmployeeId;
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                string displayUrl, downloadUrl;
                FileController.GetUrls(employeeId, "UploadPortraitPhoto", Server, out displayUrl, out downloadUrl);

                var model = connection.Query<ProfileCardViewModel>(@"
SELECT TOP 1
	[Employees].[Firstname] as Name,
	[ChangeSets].[Date] as LastProfileChange
FROM
	[Employees] LEFT OUTER JOIN
	[ChangeSets] ON [ChangeSets].[EntityId] = CONVERT(nvarchar, @EmployeeId) AND [ChangeSets].[TableName] = 'EmployeeProfiles' AND [ChangeSets].[UserId] = [Employees].[ProviderUserKey]
WHERE [Employees].[Id] = @EmployeeId
ORDER BY [ChangeSets].[Date] DESC
", new { EmployeeId = employeeId }).Single();
                model.PictureUrl = displayUrl;
                var cookie = Request.Cookies["lastLogin"];
                DateTime lastLogin;
                if (cookie != null && cookie.Value != null && DateTime.TryParse(cookie.Value, out lastLogin))
                {
                    model.LastLogin = lastLogin;
                }
                else
                {
                    model.LastLogin = LocalDateTime.Now;
                }
                return PartialView(model);
            }
        }

        private static bool AddressChanged(EditEmployeeProfile viewModel, EmployeeProfile profile)
        {
            return viewModel.Street != profile.Street || viewModel.PostalCode != profile.PostalCode ||
                   viewModel.City != profile.City;
        }

    }
}
