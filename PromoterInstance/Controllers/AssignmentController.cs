﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Dapper;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class AssignmentController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public AssignmentController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index()
        {
            return HttpContext.GetOverriddenBrowser().IsMobileDevice ? (ActionResult)View() : HttpNotFound();
        }

        public virtual ActionResult Archive()
        {
            return View();
        }


        // Diese Query wird von _Planned verwendet. _Planned wiederum wird an zwei verschiedenen Stellen aufgerufen. Einmal für das Grid (Desktop)
        // und in der ListView (Mobile). Die Desktop-Version lädt Details nach. Die ListView lädt gleich alles. Daher werden mit dieser Query Daten
        // abgerufen, die im Grid gar nicht angezeigt werden (Remarks, Contract und Team).
        private const string Query = @"
SELECT{0} RESULT.* FROM
(
SELECT Assignments.Id AS Id,
    Assignments.DateStart AS DateStart,
    Assignments.DateEnd as DateEnd,
    Campaigns.Name as CampaignName,
    Locations.Id as LocationId,
    Locations.Name as LocationName,
    Locations.Street as Street,
    Locations.PostalCode as PostalCode,
    Locations.City as City,
    'Wird verschoben' as State,
    ISNULL(NULLIF(CampaignLocations.Notes, ''), 'Keine') as Remarks,
    ISNULL(Contacts.Title + ' ' + Contacts.Firstname + ' ' + Contacts.Lastname, 'Keiner') AS Contact,
    (SELECT Firstname + ' ' + Lastname FROM Employees WHERE Id = @EmployeeId) as Team
FROM [OutstandingNotifications] INNER JOIN
    Assignments ON AssignmentIds LIKE '%,' + CONVERT(varchar, Assignments.Id) + '[_]%' INNER JOIN
    Locations ON Locations.Id = Assignments.CampaignLocation_LocationId INNER JOIN
    CampaignLocations ON Assignments.CampaignLocation_CampaignId = CampaignLocations.Campaign_Id AND Assignments.CampaignLocation_LocationId = CampaignLocations.Location_Id LEFT OUTER JOIN
    Contacts ON Contacts.Id = CampaignLocations.Contact_Id INNER JOIN
    Campaigns ON Campaigns.Id = Assignments.CampaignLocation_CampaignId
WHERE
    (
        OutstandingNotifications.Type IN ('Stornierter Aktionstag', 'Stornierte Aktionstage')
        OR
        (OutstandingNotifications.Type IN ('Verschobener Aktionstag', 'Verschobene Aktionstage') AND Assignments.State <> 0)
    ) AND
    OutstandingNotifications.EmployeeId = @EmployeeId
UNION ALL
SELECT
    Assignments.Id AS Id,
    Assignments.DateStart AS DateStart,
    Assignments.DateEnd as DateEnd,
    Campaigns.Name as CampaignName,
    Locations.Id as LocationId,
    Locations.Name as LocationName,
    Locations.Street as Street,
    Locations.PostalCode as PostalCode,
    Locations.City as City,
    case when AssignedEmployeeExtraData.Color = 'green' then 'Bestätigt' when AssignedEmployeeExtraData.Color = 'pink' then 'Unbestätigt' else 'Abgelehnt' end as State,
    ISNULL(NULLIF(CampaignLocations.Notes, ''), 'Keine') as Remarks,
    ISNULL(Contacts.Title + ' ' + Contacts.Firstname + ' ' + Contacts.Lastname, 'Keiner') AS Contact,
    (SELECT STUFF(
    (
        SELECT '<br />' + Employees.Firstname + ' ' + Employees.Lastname + ' (' + (SELECT STUFF(
        (
            SELECT ', ' + AssignmentRoles.Name
            FROM AssignmentRoles INNER JOIN AssignedEmployeeAssignmentRoles ON AssignmentRoles.Id = AssignedEmployeeAssignmentRoles.AssignmentRole_Id
            WHERE AssignedEmployeeAssignmentRoles.AssignedEmployee_EmployeeId = Employees.Id AND AssignedEmployeeAssignmentRoles.AssignedEmployee_AssignmentId = Assignments.Id
            FOR XML PATH('')
        ),
        1,
        2,
        '')) + ')'
        FROM Employees
        INNER JOIN AssignedEmployees
            ON AssignedEmployees.Employee_Id = Employees.Id
        WHERE AssignedEmployees.Assignment_Id = Assignments.Id
        FOR XML PATH(''), TYPE
    ).value('.','varchar(max)'),
    1,
    6,
    '')) AS Team

FROM
    AssignedEmployees INNER JOIN
    Assignments ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
    Locations ON Locations.Id = Assignments.CampaignLocation_LocationId INNER JOIN
    Campaigns ON Campaigns.Id = Assignments.CampaignLocation_CampaignId INNER JOIN
    CampaignLocations ON Assignments.CampaignLocation_CampaignId = CampaignLocations.Campaign_Id AND Assignments.CampaignLocation_LocationId = CampaignLocations.Location_Id LEFT OUTER JOIN
    Contacts ON Contacts.Id = CampaignLocations.Contact_Id INNER JOIN
    AssignedEmployeeExtraData ON AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id
WHERE
    Assignments.State = 0 AND
    AssignedEmployeeExtraData.Color <> 'blue' AND
    AssignedEmployees.Employee_Id = @EmployeeId
) AS RESULT
WHERE CAST(DateStart AS DATE) >= CAST(GETDATE() AS DATE)
ORDER BY DateStart
";

        private const string QueryArchived = @"
SELECT
    Assignments.Id AS Id,
    Assignments.DateStart AS DateStart,
    Assignments.DateEnd as DateEnd,
    Campaigns.Name as CampaignName,
    Locations.Name as LocationName,
    Locations.Street as Street,
    Locations.PostalCode as PostalCode,
    Locations.City as City
FROM
    AssignedEmployees INNER JOIN
    Assignments ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
    Locations ON Locations.Id = Assignments.CampaignLocation_LocationId INNER JOIN
    Campaigns ON Campaigns.Id = Assignments.CampaignLocation_CampaignId INNER JOIN
    AssignedEmployeeExtraData ON AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id
WHERE
    CAST(Assignments.DateStart AS DATE) < CAST(GETDATE() AS DATE) AND
    CAST(Assignments.DateStart AS DATE) >= CAST(DATEADD(month, -3, GETDATE()) AS DATE) AND
    Assignments.State = 0 AND
    AssignedEmployees.Attendance IN (1,2) AND
    AssignedEmployees.Employee_Id = @EmployeeId
ORDER BY Assignments.DateStart DESC
";

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Planned([DataSourceRequest] DataSourceRequest request, int? count)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var data = connection.Query<PlannedAssignment>(string.Format(Query, count.HasValue ? " TOP " + count : ""), new { Request.GetUserData().EmployeeId });
                return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(CacheProfile = "DynamicDataLong")]
        public virtual ActionResult _Archive([DataSourceRequest] DataSourceRequest request)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var data = connection.Query<ArchivedAssignment>(QueryArchived, new { Request.GetUserData().EmployeeId });
                return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(CacheProfile = "DynamicView")]
        public virtual ActionResult _Details(int id)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<AssignmentDetails>(@"
SELECT
    Assignments.Id,
    CampaignLocation_LocationId AS LocationId,
    Locations.Name,
    Locations.Name2,
    Locations.Street,
    Locations.PostalCode,
    Locations.City,
    CampaignLocations.Notes,
    Contacts.Title + ' ' + Contacts.Lastname + ', ' + Contacts.Firstname AS Contact,
    (SELECT STUFF(
    (
        SELECT '<br />' + Employees.Firstname + ' ' + Employees.Lastname + ' (' + (SELECT STUFF(
        (
            SELECT ', ' + AssignmentRoles.Name
            FROM AssignmentRoles INNER JOIN AssignedEmployeeAssignmentRoles ON AssignmentRoles.Id = AssignedEmployeeAssignmentRoles.AssignmentRole_Id
            WHERE AssignedEmployeeAssignmentRoles.AssignedEmployee_EmployeeId = Employees.Id AND AssignedEmployeeAssignmentRoles.AssignedEmployee_AssignmentId = Assignments.Id
            FOR XML PATH('')
        ),
        1,
        2,
        '')) + ')'
        FROM Employees
        INNER JOIN AssignedEmployees
            ON AssignedEmployees.Employee_Id = Employees.Id
        WHERE AssignedEmployees.Assignment_Id = Assignments.Id
        FOR XML PATH(''), TYPE
    ).value('.','varchar(max)'),
    1,
    6,
    '')) AS Team
FROM
    Assignments INNER JOIN
    CampaignLocations ON Assignments.CampaignLocation_CampaignId = CampaignLocations.Campaign_Id AND
    Assignments.CampaignLocation_LocationId = CampaignLocations.Location_Id INNER JOIN
    Locations ON CampaignLocations.Location_Id = Locations.Id LEFT OUTER JOIN
    Contacts ON CampaignLocations.Contact_Id = Contacts.Id AND Locations.Id = Contacts.Location_Id
WHERE Assignments.Id = @AssignmentId
", new {AssignmentId = id}).Single();
                return PartialView(model);
            }
        }

        private const string ConfirmUpdateStatement = "UPDATE AssignedEmployeeExtraData SET Accepted = GETDATE() WHERE AssignmentId in ({0}) AND EmployeeId = {1}";
        private const string DeclineUpdateStatement = "UPDATE AssignedEmployeeExtraData SET Rejected = GETDATE() WHERE AssignmentId in ({0}) AND EmployeeId = {1}";

        private static AssignmentMailData GetAssignmentMailData(string firstname, string email, IEnumerable<ConfirmDeclineAssignmentMailModel> assignments)
        {
            var model = new AssignmentMailData
            {
                Email = email,
                Firstname = firstname,
                Assignments = assignments
                    .Select(e => string.Format("{0} {1:dd.MM.yyyy} Aktionszeiten: {1:HH:mm} - {2:HH:mm}", e.CampaignName, e.DateStart, e.DateEnd))
            };
            return model;
        }

        private IList<ConfirmDeclineAssignmentMailModel> GetAssignments(int[] id)
        {
            return _dbContext.Assignments
                .Where(e => id.Contains(e.Id))
                .OrderBy(e => e.DateStart)
                .Select(e => new ConfirmDeclineAssignmentMailModel
                {
                    Id = e.Id,
                    DateStart = e.DateStart,
                    DateEnd = e.DateEnd,
                    LocationName = e.CampaignLocation.Location.Name,
                    Street = e.CampaignLocation.Location.Street,
                    PostalCode = e.CampaignLocation.Location.PostalCode,
                    City = e.CampaignLocation.Location.City,
                    CampaignName = e.CampaignLocation.Campaign.Name,
                })
                .ToList();
        }

        public virtual ActionResult _Confirm(int[] id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            SendConfirmMail(employeeId, id);
            Confirm(employeeId, id);
            return Content("ok");
        }

        private void SendConfirmMail(int employeeId, int[] id)
        {
            var assignments = GetAssignments(id);
            var employee = _dbContext.Employees.Single(e => e.Id == employeeId);
            var mail = SendConfirmMail(employee.Firstname, employee.Email, assignments);
            var pathToSaveMail = SaveMailToDisk(employeeId, mail);
            var filename = Path.GetFileName(pathToSaveMail);
            foreach (var assignment in assignments)
            {
                var confirmation = _dbContext.ConfirmationEmails
                    .SingleOrDefault(e => e.AssignmentId == assignment.Id && e.EmployeeId == employeeId);
                if (confirmation == null)
                {
                    confirmation = new ConfirmationEmail();
                    _dbContext.ConfirmationEmails.Add(confirmation);
                }
                confirmation.AssignmentId = assignment.Id;
                confirmation.EmployeeId = employeeId;
                confirmation.DateStart = assignment.DateStart;
                confirmation.DateEnd = assignment.DateEnd;
                confirmation.Filename = filename;
                confirmation.LocationName = assignment.LocationName;
                confirmation.Street = assignment.Street;
                confirmation.PostcalCode = assignment.PostalCode;
                confirmation.City = assignment.City;
                confirmation.Firstname = employee.Firstname;
                confirmation.Lastname = employee.Lastname;
            }
            _dbContext.SaveChanges();
        }

        private MailMessage SendConfirmMail(string firstname, string email, IEnumerable<ConfirmDeclineAssignmentMailModel> assignments)
        {
            var model = GetAssignmentMailData(firstname, email, assignments);
            var mail = new MailController().ConfirmedDaysMail(model);
            mail.Deliver();
            return mail.Mail;
        }

        private static string SaveMailToDisk(int employeeId, MailMessage mail)
        {
            var smptClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,
                EnableSsl = false,
                PickupDirectoryLocation = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString())
            };
            Directory.CreateDirectory(smptClient.PickupDirectoryLocation);
            smptClient.Send(mail);
            var mailFile = Directory.GetFiles(smptClient.PickupDirectoryLocation).Single();
            var confirmationMailDirectory = ConfigurationManager.AppSettings.Get("ConfirmationMailDirectory");
            confirmationMailDirectory = Path.Combine(confirmationMailDirectory, "" + employeeId);
            Directory.CreateDirectory(confirmationMailDirectory);
            var destFileName = Path.Combine(confirmationMailDirectory, $"{Guid.NewGuid()}.eml");
            System.IO.File.Copy(mailFile, destFileName, true);
            System.IO.File.Delete(mailFile);
            Directory.Delete(smptClient.PickupDirectoryLocation);
            return destFileName;
        }

        public virtual ActionResult _Decline(int[] id)
        {
            var employeeId = Request.GetUserData().EmployeeId;
            Decline(employeeId, id);
            SendDeclineMail(employeeId, id);
            return Content("ok");
        }

        private void SendDeclineMail(int employeeId, int[] id)
        {
            var employee = _dbContext.Employees.Single(e => e.Id == employeeId);
            var model = GetAssignmentMailData(employee.Firstname, employee.Email, GetAssignments(id));
            new MailController().DeclinedDaysMail(model).Deliver();
        }

        public virtual ActionResult _ConfirmSingle(int[] id)
        {
            if (id.Length != 1)
                throw new ArgumentException("Pass exactly one id", nameof(id));

            var employeeId = Request.GetUserData().EmployeeId;
            Confirm(employeeId, id);
            Remember(employeeId, id[0], "Confirm");
            return Content("ok");
        }
        public virtual ActionResult _DeclineSingle(int[] id)
        {
            if (id.Length != 1)
                throw new ArgumentException("Pass exactly one id", nameof(id));

            var employeeId = Request.GetUserData().EmployeeId;
            Decline(employeeId, id);
            Remember(employeeId, id[0], "Decline");
            return Content("ok");
        }

        private void Remember(int employeeId, int id, string type)
        {
            _dbContext.Database.ExecuteSqlCommand(@"
MERGE DeferredMails AS target
USING (SELECT @EmployeeId, @Type, @AssignmentId) AS source (EmployeeId, Type, AssignmentId)
ON (target.EmployeeId = source.EmployeeId AND target.Type = source.Type AND target.InProgress = 0)
WHEN MATCHED THEN
	UPDATE SET AssignmentIds = AssignmentIds + ',' + source.AssignmentId, LastUpdate = GETDATE()
WHEN NOT MATCHED THEN
	INSERT (LastUpdate, InProgress, EmployeeId, Type, AssignmentIds)
	VALUES (GETDATE(), 0, source.EmployeeId, source.Type, source.AssignmentId);",
    new SqlParameter("@EmployeeId", employeeId), new SqlParameter("@Type", type), new SqlParameter("@AssignmentId", id.ToString()));
        }

        private void Confirm(int employeeId, int[] id)
        {
            _dbContext.Database.ExecuteSqlCommand(string.Format(ConfirmUpdateStatement, string.Join(",", id), employeeId));
        }

        private void Decline(int employeeId, int[] id)
        {
            _dbContext.Database.ExecuteSqlCommand(string.Format(DeclineUpdateStatement, string.Join(",", id), employeeId));
        }

        [OutputCache(CacheProfile = "DynamicDataLongUserAgnostic")]
        public virtual ActionResult Calendar(int id)
        {
            var assignment = _dbContext.Assignments.Single(e => e.Id == id);
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Termin.ics\"");
            return Content(ToICalString(assignment), "text/calendar");
        }

        private static string ToICalString(Assignment assignment)
        {
            var iCalendar = new iCalendar();
            var attendees = assignment.AssignedEmployees
                .Select(e => e.Employee)
                .Select(employee => new Attendee("MAILTO:not-disclosed@example.com")
                {
                    Role = "REQ-PARTICIPANT",
                    CommonName = employee.Lastname + ", " + employee.Firstname
                }).Cast<IAttendee>().ToList();
            var @event = new Event
            {
                Created = iCalDateTime.Now,
                Start = new iCalDateTime(assignment.DateStart),
                End = new iCalDateTime(assignment.DateEnd),
                Summary = "Einsatz in " + assignment.CampaignLocation.Location.City,
                Description = assignment.CampaignLocation.Campaign.Name,
                Location = assignment.CampaignLocation.Location.FriendlyName,
                Attendees = attendees
                // GeographicLocation = TODO,
                // Contacts = new [] {"TODO"},
            };

            iCalendar.AddChild(@event);

            var iCalendarSerializer = new iCalendarSerializer();
            return iCalendarSerializer.SerializeToString(iCalendar);
        }
    }
}
