﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Dapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Models;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class MissingReportsController : Controller
    {
        public MissingReportsController()
        {
        }

        [OutputCache(CacheProfile = "DynamicDataLong")]
        public virtual ActionResult _Urls()
        {
            var employeeId = Request.GetUserData().EmployeeId;

            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var urls = connection.Query<string>(@"SELECT InstanceUrl + 'Report/Missing?id=' + CONVERT(varchar, @EmployeeId) + '&campaignId=' + CONVERT(varchar, Id) + '&campaign=' + Name FROM Campaigns
WHERE [State] < 1000 AND Campaigns.InstanceUrl IS NOT NULL AND Campaigns.InstanceUrl NOT LIKE '_ext_%' AND EXISTS (
	SELECT * FROM AssignedEmployees INNER JOIN
	Assignments ON AssignedEmployees.Assignment_Id = Assignments.Id
	WHERE
        Campaigns.InstanceUrl NOT LIKE '_ext_%' AND
		AssignedEmployees.Employee_Id = @EmployeeId AND
		Assignments.DateStart < GETDATE() AND
		Assignments.[State] = 0
)", new { EmployeeId = employeeId });
                return Json(urls, JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(CacheProfile = "DynamicDataLong")]
        public virtual ActionResult _ExternalReportTool()
        {
            var employeeId = Request.GetUserData().EmployeeId;

            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var externalTools =
                    connection.Query<ExternalTool>(@"SELECT
	[InstanceUrl],
	[Name]
FROM	
	[Campaigns]
WHERE
	[State] < 1000 AND
	[InstanceUrl] LIKE '_ext_%' AND EXISTS 
	(
		SELECT *
		FROM
			[AssignedEmployees] INNER JOIN
			[Assignments] ON [AssignedEmployees].[Assignment_Id] = [Assignments].[Id]
		WHERE
			[Assignments].[CampaignLocation_CampaignId] = [Campaigns].[Id] AND
			[AssignedEmployees].[Employee_Id] = @EmployeeId AND
			[Assignments].[DateStart] < GETDATE() AND
			[Assignments].[State] = 0
	)", new {EmployeeId = employeeId}).ToList();
                return PartialView(externalTools);
            }
        }
    }
}
