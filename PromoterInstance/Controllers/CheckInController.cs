﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dapper;
using PromoterInstance.Models;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Models;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class CheckInController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public CheckInController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _CurrentLocation()
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                // Wir suche hier einen Einsazt der die folgenden Kriterien erfüllt
                // - der Einsatz ist für den aktuell angemeldeten Benutzer
                // - der Einsatz ist noch nicht zu ende
                // - der Einsatz startet in spätestens 30 Minuten (er kann aber auch schon begonnen haben)
                var assignment = connection
                    .Query<CurrentAssignment>(@"DECLARE @Now datetime
SET @Now = GETDATE()

SELECT
    Assignments.Id,
    Campaigns.Name as Campaign,
    Assignments.DateStart,
    Assignments.DateEnd,
	Locations.Name,
	Locations.Street,
	Locations.PostalCode,
	Locations.City,
	CASE WHEN Locations.GeographicCoordinates IS NULL THEN 0 ELSE 1 END AS HasCoordinates,
	CampaignLocations.Notes as Remarks,
	Contacts.Title + ' ' + Contacts.Firstname + ' ' + Contacts.Lastname AS Contact,
	(SELECT STUFF(
    (
        SELECT '<br />' + Employees.Firstname + ' ' + Employees.Lastname + ' (' + (SELECT STUFF(
        (
            SELECT ', ' + AssignmentRoles.Name
            FROM AssignmentRoles INNER JOIN AssignedEmployeeAssignmentRoles ON AssignmentRoles.Id = AssignedEmployeeAssignmentRoles.AssignmentRole_Id
            WHERE AssignedEmployeeAssignmentRoles.AssignedEmployee_EmployeeId = Employees.Id AND AssignedEmployeeAssignmentRoles.AssignedEmployee_AssignmentId = Assignments.Id
            FOR XML PATH('')
        ),
        1,
        2,
        '')) + ')'
        FROM Employees
        INNER JOIN AssignedEmployees
            ON AssignedEmployees.Employee_Id = Employees.Id
        WHERE AssignedEmployees.Assignment_Id = Assignments.Id
        FOR XML PATH(''), TYPE
    ).value('.','varchar(max)'),
    1,
    6,
    '')) AS Team,
    CASE WHEN AssignedEmployees.Attendance <> 0 THEN 1 ELSE 0 END AS AlreadyCheckedIn,
    CASE WHEN AssignedEmployees.Attendance = 3 THEN 1 ELSE 0 END AS DidNotAttend
FROM
	AssignedEmployees INNER JOIN
    Assignments ON AssignedEmployees.Assignment_Id = Assignments.Id INNER JOIN
    CampaignLocations ON Assignments.CampaignLocation_CampaignId = CampaignLocations.Campaign_Id AND Assignments.CampaignLocation_LocationId = CampaignLocations.Location_Id LEFT OUTER JOIN
	Contacts ON Contacts.Id = CampaignLocations.Contact_Id INNER JOIN
    Locations ON CampaignLocations.Location_Id = Locations.Id INNER JOIN
    Campaigns ON Campaigns.Id = CampaignLocations.Campaign_Id 
WHERE
	AssignedEmployees.Employee_Id = @EmployeeId AND
    Assignments.DateEnd > @Now AND
    Assignments.State = 0 AND
	@Now > DATEADD(minute, -30, Assignments.DateStart)",
                        new {Request.GetUserData().EmployeeId})
                    .FirstOrDefault();
                
                if (assignment == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
                if (assignment.DidNotAttend)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Conflict);
                }
                if (assignment.AlreadyCheckedIn)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Gone);
                }
                return Json(assignment);
            }
        }

        public virtual ActionResult _CheckIn(CheckInModel model)
        {
            if (!Request.IsLocal && model.Accuracy > 500)
            {
                return Json(new { property = "errorAccuracy" }, JsonRequestBehavior.AllowGet);
            }

            var wellKnownText = $"POINT({model.Longitude} {model.Latitude})";  //Test POINT(9.98555839061737 53.566366364562107)
            var coordinates = DbGeography.FromText(wellKnownText, 4326);
            var employeeId = Request.GetUserData().EmployeeId;
            var data = _dbContext.AssignedEmployees
                .Where(e => e.EmployeeId == employeeId && e.AssignmentId == model.AssignmentId)
                .Select(e => new
                {
                    AssignedEmployee = e,
                    e.Assignment.DateStart,
                    e.Assignment.CampaignLocation.Location.GeographicCoordinates,
                })
                .Single();

            if (data.GeographicCoordinates.Distance(coordinates) > 500)
                return Json(new { property = "errorTooFarAway", time = LocalDateTime.Now }, JsonRequestBehavior.AllowGet);

            var now = LocalDateTime.Now;
            data.AssignedEmployee.AttendanceType = "Mobile";
            data.AssignedEmployee.AttendanceMobileCheckInDate = now;
            data.AssignedEmployee.AttendanceMobileCheckInCoordinates = coordinates;
            data.AssignedEmployee.Attendance = data.DateStart.AddMinutes(15) < now
                                                ? Attendance.Late
                                                : Attendance.InTime;
            _dbContext.SaveChanges();
            return Json(new { property = "succeeded", time = LocalDateTime.Now }, JsonRequestBehavior.AllowGet);
        }
	}
}