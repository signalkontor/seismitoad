﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace PromoterInstance.Controllers
{
    public partial class ViewSwitcherController : Controller
    {
        public virtual ActionResult SwitchView(bool mobile)
        {
            if (Request.Browser.IsMobileDevice == mobile)
                HttpContext.ClearOverriddenBrowser();
            else
                HttpContext.SetOverriddenBrowser(mobile ? BrowserOverride.Mobile : BrowserOverride.Desktop);

            return RedirectToAction(MVC.Home.Index());
        }
    }
}