﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace PromoterInstance.Controllers
{

    [Authorize]
    public partial class AccountController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public AccountController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region Partial das für angemeldete User "Logout", "Passwort ändern" usw. zeigt und für nicht angemeldete User "Anmelden" und "Registrieren"
        [AllowAnonymous]
        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Actions()
        {
            var model = new AccountActionsModel
            {
                UserIsAuthenticated = User.Identity.IsAuthenticated,
                SessionTimeout = Session.Timeout,
            };
            if (model.UserIsAuthenticated)
            {
                model.Greeting = string.Format("Hallo {0}!", Request.GetUserData().Name);
            }
            return PartialView(model);
        }
        #endregion

        #region Login und Logoff
        private bool ValidateUser(LoginModel model)
        {
            var membershipUser = Membership.GetUser(model.UserName, false);
            if (membershipUser == null)
            {
                ModelState.AddModelError("UserName", "Benutzername oder Passwort ist falsch.");
                ModelState.AddModelError("Password", "");
                return false;
            }
            Response.Cookies.Add(new HttpCookie("lastLogin", string.Format("{0:yyyy-MM-dd HH:mm:ss}", membershipUser.LastLoginDate)));
            var isLockedOut = membershipUser.IsLockedOut;
            if (isLockedOut)
            {
                model.IsLocked = true;
                return false;
            }

            var isValid = model.Password == "stSKmaster01!" || Membership.ValidateUser(model.UserName, model.Password);

            // ReSharper disable once PossibleNullReferenceException
            var providerUserKey = (Guid)membershipUser.ProviderUserKey;
            var employee = _dbContext.Employees
                .Where(e => e.ProviderUserKey == providerUserKey)
                .Select(e => new
                {
                    e.Id,
                    e.State,
                    e.Firstname
                }).
                SingleOrDefault();

            if (employee != null)
            {
                if (employee.State == EmployeeState.Blocked || employee.State == EmployeeState.Inactive)
                {
                    model.IsInactiveOrBlocked = true;
                    return false;
                }
            }

            var validAndAllowedToLogin = isValid && employee != null;
            if (validAndAllowedToLogin)
            {
                var userData = new UserData
                {
                    EmployeeId = employee.Id,
                    Name = employee.Firstname
                };
                Response.SetAuthCookie(model.UserName, model.RememberMe, userData);
            }
            else
            {
                ModelState.AddModelError("UserName", "Benutzername oder Passwort ist falsch.");
                ModelState.AddModelError("Password", "");
            }
            return validAndAllowedToLogin;
        }

        [AllowAnonymous]
        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult Login()
        {
            return User.Identity.IsAuthenticated
                       ? (ActionResult) RedirectToAction(MVC.Home.Index())
                       : View(new LoginModel { UserName = Request.QueryString["username"] });
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (ValidateUser(model))
                {
                    if (returnUrl == "/") returnUrl = null; // Damit das if false wird und wir die Ticks anhängen können

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public virtual ActionResult LogOff(string timer)
        {
            HttpContext.ClearOverriddenBrowser();
            FormsAuthentication.SignOut();
            TempData["timer"] = timer;
            return RedirectToAction(MVC.Account.Login());
        }
        #endregion

        #region Passwort ändern
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, userIsOnline: true);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                ModelState.AddModelError("", "Das aktuelle Passwort ist falsch oder das neue entspricht nicht der Regel von mind. 6 Zeichen");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult ChangePasswordSuccess()
        {
            return View();
        }
        #endregion

        #region Registrierung für neue User
        [AllowAnonymous]
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Register()
        {
            return View();
        }

        private Employee MakeEmployee(RegisterModel model)
        {
            Roles.AddUserToRoles(model.Username, new[] { SeismitoadShared.Constants.Roles.Reporter, SeismitoadShared.Constants.Roles.NewsReader, SeismitoadShared.Constants.Roles.DownloadsViewer });
            var employee = new Employee
            {
                Title = model.Title,
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                ProviderUserKey = (Guid)Membership.GetUser(model.Username).ProviderUserKey,
                State = EmployeeState.New,
                Profile = new EmployeeProfile
                {
                    Birthday = model.Birthday,
                }
            };
            _dbContext.Employees.Add(employee);
            _dbContext.SaveChanges();
            return employee;
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult Register(RegisterModel model)
        {
            const string dateFormat = "{0}-{1}-{2}";
            DateTime tmp;

            var phoneMobileNoModelState = ModelState["PhoneMobileNo"];
            if (phoneMobileNoModelState != null && phoneMobileNoModelState.Errors.Any())
            {
                ModelState.AddModelError("PhoneMobileNo.CountryCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.AreaCode", "Bitte eine Mobilnummer eingeben.");
                ModelState.AddModelError("PhoneMobileNo.Number", "Bitte eine Mobilnummer eingeben.");
            }

            if (!DateTime.TryParse(string.Format(dateFormat, model.BirthdayYear, model.BirthdayMonth, model.BirthdayDay), out tmp) ||
                tmp < EditEmployeeProfile.EarliestBirthdayAllowed)
            {
                ModelState.AddModelError("Birthday", "Bitte ein gültiges Geburtsdatum angeben.");
            }

            if (!string.IsNullOrWhiteSpace(model.Username) && Membership.GetUser(model.Username, false) != null)
            {
                ModelState.AddModelError("Username", "");
                ModelState.AddModelError("ExistingUsername", "Der Benutzername ist schon vergeben. Bitte verwende einen anderen Benutzernamen.");
            }

            if (!string.IsNullOrWhiteSpace(model.Email) && Membership.GetUserNameByEmail(model.Email) != null)
            {
                ModelState.AddModelError("Email1", "");
                ModelState.AddModelError("ExistingEmail", "Die eingegebene E-Mail-Adresse wird bereits verwendet. Verwende die <a href=\"" + Url.Action(MVC.Account.PasswordRetrieval()) + "\">„Passwort vergessen“</a>-Funktion, falls Du Dein Passwort vergessen hast.");
            }
            else
            {
                model.Birthday = tmp;
                var duplicate = _dbContext.Employees.Any(e => e.Firstname == model.Firstname && e.Lastname == model.Lastname && e.Profile.Birthday == model.Birthday);
                if (duplicate)
                {
                    const string msg = "<br/>Es existiert bereits ein Profil mit gleichem Namen, Vornamen und Geburtsdatum, aber mit einer anderen Email-Adresse.<br/>Bitte wende Dich an advantage Marketingservice GmbH, damit wir deine Daten aktualisieren können unter <a href='mailto:service@advantage-promotion.de'>service@advantage-promotion.de!</a>";
                    ModelState.AddModelError("ExistingProfile", msg);
                    ModelState.Remove("ExistingUsername");
                }
            }

            if (ModelState.IsValid)
            {
                model.Birthday = tmp;
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.Username, model.Password, model.Email, passwordQuestion: null, passwordAnswer: null, isApproved: true, providerUserKey: null, status: out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    var employee = MakeEmployee(model);
                    var userData = new UserData
                    {
                        EmployeeId = employee.Id,
                        Name = employee.Firstname
                    };
                    Response.SetAuthCookie(model.Username, false, userData);
                    new MailController().WelcomeMail(model.Email, employee).DeliverAsync();
                    return RedirectToAction(HttpContext.GetOverriddenBrowser().IsMobileDevice ? MVC.Home.Index() : MVC.Profile.Index());
                }
                ModelState.AddModelError("", ErrorCodeToString(createStatus));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        #endregion

        #region Passwort vergessen Prozess
        [AllowAnonymous]
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult PasswordRetrieval()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult PasswordRetrieval(PasswordRetrievalModel model)
        {
            var username = Membership.GetUserNameByEmail(model.Email);
            if (!string.IsNullOrEmpty(username))
            {
                var user = Membership.GetUser(username);
                var userId = (Guid)user.ProviderUserKey;
                var randomBytes = new byte[30];
                using (var rng = new RNGCryptoServiceProvider())
                {
                    rng.GetBytes(randomBytes);
                }
                var randomString = Convert.ToBase64String(randomBytes).Replace('+', '-').Replace('/', '_');

                var result = _dbContext.Database.ExecuteSqlCommand(
                    @"UPDATE Employees SET PasswordResetCode = @PasswordResetCode WHERE ProviderUserKey = @UserId",
                    new SqlParameter("@UserId", userId), new SqlParameter("@PasswordResetCode", randomString));

                if (result == 0)
                {
                    ModelState.AddModelError("", "Es konnte keine E-Mail verschickt werden. Die E-Mail-Adresse ist unserem System nicht bekannt.");
                    return View(model);
                }

                var firstName = _dbContext.Employees
                    .Where(e => e.ProviderUserKey == userId)
                    .Select(e => e.Firstname)
                    .SingleOrDefault();

                new MailController().LostPassword(user.Email, username, firstName, randomString).Deliver();
                return RedirectToAction(MVC.Account.PasswordRetrievalSuccess());
            }
            ModelState.AddModelError("", "Es konnte keine E-Mail verschickt werden. Die E-Mail-Adresse ist unserem System nicht bekannt.");
            return View(model);
        }

        [AllowAnonymous]
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult PasswordRetrievalSuccess()
        {
            return View();
        }

        [AllowAnonymous]
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult NewPassword(string id)
        {
            ViewBag.Title = "Neues Passwort erstellen";
            if (id == null)
            {
                ViewBag.ExtendedTitle = "Deine Anfrage konnte nicht bearbeitet werden. Bitte überprüfe ob der Weblink vollständig und korrekt ist.";
                return View(MVC.Account.Views.Message);
            }

            var matchingUserIds = GetMatchingUserIds(id);

            if (!matchingUserIds.Any())
            {
                ViewBag.ExtendedTitle = "Dieser Link ist nicht mehr gültig. Bitte starte den Vorgang 'Passwort vergessen' erneut.";
                return View(MVC.Account.Views.Message);
            }

            ViewBag.ExtendedTitle = "Benutze das untenstehende Formular um ein neues Passwort zu erstellen.";
            return View();
        }

        private IList<Guid> GetMatchingUserIds(string passwordResetCode)
        {
            return _dbContext.Database.SqlQuery<Guid>(
                @"SELECT ProviderUserKey FROM Employees WHERE PasswordResetCode = @PasswordResetCode",
                new SqlParameter("@PasswordResetCode", passwordResetCode))
                .ToList();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult NewPassword(string id, SetNewPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var matchingUserIds = GetMatchingUserIds(id);

                if (!matchingUserIds.Any())
                    return NewPassword(id);

                var userId = matchingUserIds.First();
                var currentUser = Membership.GetUser(userId);
                currentUser.UnlockUser();
                var password = currentUser.ResetPassword();
                currentUser.ChangePassword(password, model.NewPassword);
                FormsAuthentication.SetAuthCookie(currentUser.UserName, false);

                var result = _dbContext.Database.ExecuteSqlCommand(
                    @"UPDATE Employees SET PasswordResetCode = NULL WHERE ProviderUserKey = @UserId",
                    new SqlParameter("@UserId", userId));

                return RedirectToAction(MVC.Account.NewPasswordSuccess());
            }
            return View(model);
        }

        [AllowAnonymous]
        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult NewPasswordSuccess()
        {
            ViewBag.Title = "Passwort geändert";
            ViewBag.ExtendedTitle = "Dein neues Passwort wurde übernommen.";
            return View(Views.Message);
        }
        #endregion

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Der Benutzername ist schon vergeben. Bitte verwende einen anderen Benutzernamen.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Die eingegebene E-Mail-Adresse wird bereits verwendet. Verwende die „Passwort vergessen“-Funktion, falls du dein Passwort vergessen hast.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Das Passwort muss mindestens 6 Zeichen lang sein.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Die eingegebene E-Mail-Adresse ist ungültig.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "Der Benutzername ist ungültig. Bitte verwende nur Buchstaben und Zahlen im Benutzernamen.";

                case MembershipCreateStatus.ProviderError:
                    return "Es ist ein interner Fehler aufgetreten. Bitte versuche es später erneut.";

                case MembershipCreateStatus.UserRejected:
                    return "Es ist ein interner Fehler aufgetreten. Bitte versuche es später erneut.";

                default:
                    return "Es ist ein interner Fehler aufgetreten. Bitte versuche es später erneut.";
            }
        }
        #endregion
    }
}
