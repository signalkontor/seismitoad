﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using ActionMailer.Net.Mvc;
using SeismitoadModel;
using System.Configuration;
using System.Security.Policy;
using SeismitoadModel.DatabaseContext;
using PromoterInstance.Models;

namespace PromoterInstance.Controllers
{
    public class AssignmentMailData
    {
        public string Email { get; set; }
        public string Firstname { get; set; }
        public IEnumerable<string> Assignments { get; set; }
    }

    public partial class MailController : MailerBase
    {
        private static readonly bool IsStaging = ConfigurationManager.AppSettings.AllKeys.Contains("StagingEnvironment");

        public virtual EmailResult LostPassword(string email, string username, string firstname, string passwordResetCode)
         {
            var model = new LostPasswordMailModel
            {
                Firstname = firstname,
                PasswordResetCode = passwordResetCode,
                Username = username
            };

            To.Add(email);
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = "advantage-promotion: Neues Passwort";

            return Email("LostPassword", model);
        }

        public virtual EmailResult WelcomeMail(string email, Employee employee)
        {
            To.Add(email);
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = "advantage: Danke für deine Registrierung";
            return Email("Welcome", employee);
         }

        // Die selbe Mail wie hier wird auch in der MasterInstance in Service\DeferredMail.cs gesendet.
        // Änderungen am Betreff oder Inhalt müssen daher immer an zwei Stellen gemacht werden.
        public virtual EmailResult ConfirmedDaysMail(AssignmentMailData model)
        {
            To.Add(model.Email);
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = "advantage | bestätigte Aktionstage";
            return Email("ConfirmedDays", model);
        }

        // Die selbe Mail wie hier wird auch in der MasterInstance in Service\DeferredMail.cs gesendet.
        // Änderungen am Betreff oder Inhalt müssen daher immer an zwei Stellen gemacht werden.
        public virtual EmailResult DeclinedDaysMail(AssignmentMailData model)
        {
            To.Add(model.Email);
#if ADVANTAGE
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            foreach (var user in users)
            {
                var userObj = Membership.GetUser(user, false);
                BCC.Add(userObj.Email);
            }
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = "advantage | abgelehnte Aktionstage";
            return Email("RejectedDays", model);
        }

        public virtual EmailResult AppliedAssignments(string campaignTitle, string campaignNumber, string promoter, string[] answers)
        {
#if ADVANTAGE
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            foreach (var user in users)
            {
                var userObj = Membership.GetUser(user, false);
                if (userObj != null && userObj.IsApproved)
                {
                    CC.Add(userObj.Email);
                }
            }
            BCC.Add("mikko.jania@signalkontor.com");
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = $"{campaignTitle}, ({campaignNumber}) | Bewerbung {promoter}";
            ViewBag.Promoter = promoter;
            ViewBag.Campaign = $"{campaignTitle} {campaignNumber}";
            return Email("AppliedAssignments", answers);
        }

        //Mail die advantage bekommt wenn ein Promoter sein Profil löscht
        public virtual EmailResult ProfileDeletedMail(Employee employee)
        {
#if ADVANTAGE
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            foreach (var user in users)
            {
                var userObj = Membership.GetUser(user, false);
                if (userObj != null && userObj.IsApproved)
                {
                    CC.Add(userObj.Email);
                }
            }
            From = ConfigurationManager.AppSettings["PromoterMailsFrom"];
            Subject = $"{employee.Firstname} {employee.Lastname} hat seinen Account gelöscht";
            return Email("ProfileDeleted", employee);
        }

        public override EmailResult Email(string viewName, object model = null, string masterName = null, bool trimBody = true)
        {
            if (IsStaging)
            {
                To.Clear();
                CC.Clear();
                BCC.Clear();
                if (System.Web.HttpContext.Current.Request.IsLocal)
                {
                    CC.Add("mikko.jania@signalkontor.com");
                }
                else
                {
                    To.Add(ConfigurationManager.AppSettings["StagingMailsRecepient"]);
                }
            }
            return base.Email(viewName, model, masterName, trimBody);
        }
    }
}
