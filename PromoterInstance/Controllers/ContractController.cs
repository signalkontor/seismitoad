﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Web.WebPages;
using Dapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class ContractController : Controller
    {
        public virtual ActionResult Index()
        {
            return HttpContext.GetOverriddenBrowser().IsMobileDevice ? (ActionResult)View() : HttpNotFound();
        }

        private const string Query =
            @"
SELECT [Id]
        ,[Name]
        ,[Duration]
        ,[Days]
        ,[Locations]
        ,[Times]
FROM [Campaigns]
WHERE EXISTS (SELECT * FROM [CampaignEmployees] c WHERE c.[CampaignId] = [Campaigns].[Id] AND c.[EmployeeId] = @EmployeeId AND c.[State] = 'ContractSent')
";


        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            // Aus Performancegründen verwenden wir hier eine handgeschriebene SQL Query.
            // Außerdem sparen wir es uns so die Tabelle "CampaignEmployeeExtraData" in das Datenmodell aufzunehmen. Das ist
            // auch gut für die Performance, da jede Tabelle Performance frist und inbesondere diese Tabelle nur für diese
            // Funktion hier gebraucht wird.
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<ApplyableCampaign>(Query, new { Request.GetUserData().EmployeeId });
                return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
    }
}
