﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class DeclinedJobsController : Controller
    {
        // Fast wie JobController.Query, nur das hier ausschließlich die abgelehten (NotInterested) Angebote geholt werden.
        private const string QueryDeclined =
    @"
SELECT [Id]
        ,0 as Highlighted
        ,[Name]
        ,[Description]
        ,[Requirements]
        ,[Duration]
        ,[Days]
        ,[Locations]
        ,[Times]
        ,[CampaignEmployees].[State] AS State
FROM Campaigns INNER JOIN
    [CampaignEmployees] ON [CampaignEmployees].[CampaignId] = [Campaigns].[Id] AND [CampaignEmployees].[EmployeeId] = @EmployeeId
WHERE [Campaigns].[IsApplyable] = 1 AND [CampaignEmployees].[State] IN ('NotInterested', 'Rejected', 'InformationRejected', 'ContractRejected')
";

        public DeclinedJobsController()
        {
        }


        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Index()
        {
            return View();
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            // Aus Performancegründen verwenden wir hier eine handgeschriebene SQL Query.
            // Außerdem sparen wir es uns so die Tabelle "CampaignEmployeeExtraData" in das Datenmodell aufzunehmen. Das ist
            // auch gut für die Performance, da jede Tabelle Performance frist und inbesondere diese Tabelle nur für diese
            // Funktion hier gebraucht wird.
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var model = connection.Query<ApplyableCampaign>(QueryDeclined, new { Request.GetUserData().EmployeeId });
                return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
    }
}