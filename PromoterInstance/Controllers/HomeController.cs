﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using SeismitoadShared.Controllers;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using Dapper;
using Kendo.Mvc.Extensions;
using PromoterInstance.Models;

namespace PromoterInstance.Controllers
{
    public partial class HomeController : Controller
    {
        public HomeController()
        {
        }

        [Authorize]
        // Mit Absicht VaryByParam leer gelassen. Egal wie die Parameter sind, der Server soll die Seite immer cachen, der Client aber nie.
        [OutputCache(CacheProfile = "NoCache")] 
        public virtual ActionResult Index()
        {
            return View();
        }

        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Impressum()
        {
            return View();
        }

        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Kontakt()
        {
            return View();
        }

        [OutputCache(CacheProfile = "StaticView")]
        public virtual ActionResult Hilfe()
        {
            return View();
        }

        [Authorize]
        public virtual ActionResult RedirectTo(string url)
        {
            return SignOnController.To(User, url);
        }

        private const string MenuQuery = @"SELECT COUNT(*)
FROM [Campaigns] LEFT OUTER JOIN
	 [CampaignEmployees] ON [CampaignEmployees].[CampaignId] = [Campaigns].[Id] AND [CampaignEmployees].[EmployeeId] = @EmployeeId
WHERE ([Campaigns].[IsApplyable] = 1 OR [CampaignEmployees].[Invited] IS NOT NULL) AND
	  NOT EXISTS (SELECT * FROM [CampaignEmployeeExtraData] c WHERE c.CampaignId = [Campaigns].[Id] AND c.EmployeeId = @EmployeeId)

SELECT COUNT(*)
FROM [Campaigns]
WHERE EXISTS (SELECT * FROM [CampaignEmployees] c WHERE c.[CampaignId] = [Campaigns].[Id] AND c.[EmployeeId] = @EmployeeId AND c.[State] = 'ContractSent')

SELECT COUNT(*)
FROM AssignedEmployees INNER JOIN
Assignments ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
AssignedEmployeeExtraData ON AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id
WHERE Employee_Id = @EmployeeId AND AssignedEmployeeExtraData.Color = 'pink' AND CAST(DateStart AS DATE) >= CAST(GETDATE() AS DATE)
";

/* QUERY um festzustellen, ob es einen eincheckbaren Einsatz gibt.

DECLARE @Now datetime
SET @Now = GETDATE()

SELECT COUNT(*)
FROM
	AssignedEmployees INNER JOIN
    Assignments ON AssignedEmployees.Assignment_Id = Assignments.Id
WHERE
    AssignedEmployees.Attendance <> 0 AND
	AssignedEmployees.Employee_Id = @EmployeeId AND
    Assignments.DateEnd > @Now AND
	@Now > DATEADD(minute, -30, Assignments.DateStart)
*/


        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Menu()
        {
            var items = new List<MenuItem>();
            if (User.Identity.IsAuthenticated)
            {
                using (var connection = new SqlConnection(Helpers.ConnectionString))
                using (var multi = connection.QueryMultiple(MenuQuery, new {Request.GetUserData().EmployeeId}))
                {
                    var newJobsCount = multi.Read<int>().Single();
                    var openContractsCount = multi.Read<int>().Single();
                    var openAssignmensCount = multi.Read<int>().Single();
                    items.Add(new MenuItem {url = Url.Action(MVC.Profile.Index()), text = "Profil", icon = ""});
                    items.Add(new MenuItem
                    {
                        url = Url.Action(MVC.Job.Index()),
                        text = "Jobangebote",
                        icon = newJobsCount > 0 ? "star" : ""
                    });
                    items.Add(new MenuItem {url = Url.Action(MVC.MyJobs.Index()), text = "Deine Aktionen",});
                        // Immer ohne Stern (laut Mail Sebastian)
                    items.Add(new MenuItem
                    {
                        url = Url.Action(MVC.Contract.Index()),
                        text = "Neue Projektaufträge",
                        icon = openContractsCount > 0 ? "star" : ""
                    });
                    items.Add(new MenuItem
                    {
                        url = Url.Action(MVC.Assignment.Index()),
                        text = "Deine Einsätze",
                        icon = openAssignmensCount > 0 ? "star" : ""
                    });
                    items.Add(new MenuItem {url = Url.Action(MVC.CheckIn.Index()), text = "Mobile Check-in", icon = ""});
                }
            }
            else
            {
                items.Add(new MenuItem { url = Url.Action(MVC.Account.Login()), text = "Login", icon = "" });
            }
            items.Add(new MenuItem { url = Url.Action(MVC.Home.Impressum()), text = "Kontakt / Impressum", color = "grey", icon = "" });
            if (User.Identity.IsAuthenticated)
            {
                items.Add(new MenuItem { url = Url.Action(MVC.Account.ChangePassword()), text = "Passwort ändern", color = "grey", icon = "" });
                items.Add(new MenuItem { url = Url.Action(MVC.Account.LogOff()), text = "Abmelden", color = "grey", icon = "shutdown" });
            }
            items.Add(new MenuItem { url = Url.Action(MVC.ViewSwitcher.SwitchView(false)), text = "zur Desktop-Version", color = "grey", icon = "" });
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }
}
