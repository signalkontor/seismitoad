﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using Dapper;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance.Controllers
{
    public partial class StartController : Controller
    {
        // GET: Start
        public virtual ActionResult Index(string id)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                HttpContext.SetOverriddenBrowser(BrowserOverride.Desktop);

                var startViewModel = connection
                    .Query<StartViewModel>(@"SELECT
	Firstname,
	StartCode,
	CASE WHEN EXISTS (
		SELECT *
		FROM AssignedEmployees ae INNER JOIN Assignments a ON a.Id = ae.Assignment_Id
		WHERE ae.Employee_Id = Employees.Id AND a.DateStart > GETDATE() AND a.[State] = 0
	) THEN 0 ELSE 1 END as Deleteable
FROM Employees
WHERE StartCode = @StartCode", new {StartCode = id})
                    .SingleOrDefault();

                return startViewModel != null 
                    ? (ActionResult) View(startViewModel) 
                    : RedirectToAction(MVC.Account.Login());
            }
        }

        [HttpPost]
        public virtual ActionResult Index(StartViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var connection = new SqlConnection(Helpers.ConnectionString))
                {
                    var data = connection
                        .Query<dynamic>(
                            "SELECT ProviderUserKey, Id as EmployeeId FROM Employees WHERE StartCode = @StartCode",
                            new {StartCode = model.StartCode})
                        .SingleOrDefault();

                    var membershipUser = Membership.GetUser((Guid)data.ProviderUserKey, true);
                    var tmpPwd = membershipUser.ResetPassword();
                    membershipUser.ChangePassword(tmpPwd, model.Password);
                    membershipUser.IsApproved = true;
                    var userData = new UserData
                    {
                        EmployeeId = data.EmployeeId,
                        Name = model.Firstname
                    };
                    Response.SetAuthCookie(membershipUser.UserName, false, userData);
                    connection.Execute("UPDATE Employees SET StartCode = NULL WHERE Id = @EmployeeId", new { EmployeeId = userData.EmployeeId });
                    return RedirectToAction(MVC.Profile.Index());
                }
            }
            return View(model);
        }

        public class DeleteInfo
        {
            public int EmployeeId { get; set; }
            public Guid ProviderUserKey { get; set; }
            public string StateComment { get; set; }
            public string PersonalDataComment { get; set; }
            public string Comment { get; set; }
            public string DriversLicenseRemark { get; set; }
        }

        [HttpPost]
        public virtual ActionResult _DeleteProfile(string id)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                var employeeIdResults = connection
                    .Query<DeleteInfo>(@"
SELECT
    Employees.Id as EmployeeId,
    ProviderUserKey,
    StateComment,
    PersonalDataComment,
    Comment,
    DriversLicenseRemark
FROM Employees LEFT OUTER JOIN
EmployeeProfiles ON Employees.Id = EmployeeProfiles.Id
WHERE StartCode = @StartCode", new {StartCode = id})
                    .ToList();

                if (!employeeIdResults.Any())
                {
                    return Content("OK");
                }

                var tmp = employeeIdResults.Single();
                int employeeId = tmp.EmployeeId;

                connection.Execute("DELETE FROM Experiences WHERE Employee_Id = @EmployeeId", new {EmployeeId = employeeId});
                connection.Execute("DELETE FROM Ratings WHERE Employee_Id = @EmployeeId", new {EmployeeId = employeeId});
                connection.Execute("DELETE FROM EmployeeProfiles WHERE Id = @EmployeeId", new {EmployeeId = employeeId});

                // Wir setzen ganz bewusst, das Gerbutsdatum auf den 2. Januar 1800. So markieren wir, dass dieser Datensatz
                // kürzlich gelöscht wurde. Diese kürzlich gelöschen Datensätze müssen dann noch mal vom Lösch-Job durchlaufen
                // werden, der dann die vom Promoter hochgeladenen Dateien löscht.
                connection.Execute(@"INSERT INTO [dbo].[EmployeeProfiles]
           ([Id]
           ,[Birthday]
           ,[PersonalDataComment]
           ,[Comment]
           ,[DriversLicenseRemark])
     VALUES (@EmployeeId, '1800-01-02', @PersonalDataComment, @Comment, @DriversLicenseRemark)"
                    , new { EmployeeId = employeeId, tmp.PersonalDataComment, tmp.Comment, tmp.DriversLicenseRemark });
                connection.Execute("UPDATE Employees SET State = 500, StartCode = NULL WHERE Id = @EmployeeId", new {EmployeeId = employeeId});

                var user = Membership.GetUser(tmp.ProviderUserKey, false);
                if (user != null) Membership.DeleteUser(user.UserName, true);
            }

            return Content("OK");
        }
    }
}