﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using AutoMapper.QueryableExtensions;
using Dapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;

namespace PromoterInstance.Controllers
{
    [Authorize]
    public partial class MyJobsController : Controller
    {
        public virtual ActionResult Index()
        {
            return HttpContext.GetOverriddenBrowser().IsMobileDevice ? (ActionResult)View() : HttpNotFound();
        }

        private static IList<MyJobViewModel> GetMyJobs(int employeeId)
        {
            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                return connection
                    .Query<MyJobViewModel>(@"
SELECT [CampaignEmployees].[CampaignId] AS CampaignId
	  ,[Name]
	  ,[Duration] 
FROM [CampaignEmployees] INNER JOIN
	 [Campaigns] ON [CampaignEmployees].[CampaignId] = [Campaigns].[Id]
WHERE ([EmployeeId] = @EmployeeId) AND ([ContractAccepted] IS NOT NULL) AND ( NOT ([Campaigns].[State] IN (1000, 1100)))
", new { EmployeeId = employeeId }).ToList();
            }
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Current()
        {
            return PartialView(GetMyJobs(Request.GetUserData().EmployeeId));
        }

        [OutputCache(CacheProfile = "NoCache")]
        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetMyJobs(Request.GetUserData().EmployeeId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}