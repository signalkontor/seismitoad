﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PromoterInstance
{
    public class IncompleteProfileFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;

            // User die nicht eingeloggt sind, sind uns egal und AJAX auch
            if (!request.IsAuthenticated || request.IsAjaxRequest())
                return;

            // Für die Controller Home und Job, MyJobs muss der Promoter ein vollständiges Profil haben
            var controller = request.RequestContext.RouteData.Values["controller"] as string;
            if (_profileRequiredController.Contains(controller))
            {
                if (controller == "Home")
                {
                    var action = request.RequestContext.RouteData.Values["action"] as string;
                    if (action == "Impressum" || action == "Kontakt")
                        return;
                }
                var employeeId = request.GetUserData().EmployeeId;
                if (ProfileIncomplete(employeeId))
                {
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary
                    {
                        { "Controller", "Profile" },
                        { "Action", "Incomplete" },
                    });
                }
            }
        }

        private readonly string[] _profileRequiredController = { "Home", "Job", "MyJobs", "Job", "CheckIn", "Contract", "Assignment" };

        private const string ProfileIncompleteQuery = @"
SELECT CASE WHEN [Incomplete] IS NULL OR [Incomplete] = 1 THEN 1 ELSE 0 END as IncompleteProfile
FROM [Employees] LEFT OUTER JOIN
        [EmployeeProfiles] ON [Employees].[Id] = [EmployeeProfiles].[Id]
WHERE Employees.Id = @EmployeeId";

        private static bool ProfileIncomplete(int employeeId)
        {
            var photoPath = HostingEnvironment.MapPath($"~/Uploads/Employee/{employeeId}/UploadPortraitPhoto");
            if (!File.Exists(photoPath + ".jpg") && !File.Exists(photoPath + ".png") && !File.Exists(photoPath + ".gif"))
                return true;

            using (var connection = new SqlConnection(Helpers.ConnectionString))
            {
                return connection.Query<bool>(ProfileIncompleteQuery, new { EmployeeId = employeeId }).Single();
            }
        }
    }
}