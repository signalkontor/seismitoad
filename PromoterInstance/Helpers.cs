﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace PromoterInstance
{
    public static class Helpers
    {
        /// <summary>
        /// Liefert das UserData-Objekt für den aktuellen Request zurück. Diese Methode kann nur aufgerufen werden
        /// wenn der User eingeloggt ist.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UserData GetUserData(this HttpRequestBase request)
        {
            var cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                var ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (ticket == null || !UserData.IsValidUserData(ticket.UserData))
                {
                    // UserData entspricht nicht dem was wir erwarten. Vermutlich hat der User ein altes Cookie bei dem
                    // UserData noch nicht bzw. anders gefüllt war. Wir loggen den User einfach aus. Wenn er sich neu
                    // einloggt bekommt er dann ein passendes Cookie.
                    request.RequestContext.HttpContext.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName)
                    {
                        Expires = LocalDateTime.Now.AddDays(-1),
                        Value = null,
                    });
                    return new UserData();
                }
                return new UserData(ticket.UserData);
            }
            return new UserData();
        }

        public static Employee GetLoggedInEmployee(this Controller controller, SeismitoadDbContext dbContext)
        {
            var employeeId = controller.Request.GetUserData().EmployeeId;
            // Hier ist es okay nicht ActiveEmployees zu benutzen, da auch ein gesperrter User sein Profil bearbeiten darf.
            return dbContext.Employees
                .Where(e => e.Id == employeeId)
                .Include(e => e.Profile)
                .Include(e => e.Experiences)
                .SingleOrDefault();
        }

        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["SeismitoadDbContext"].ConnectionString;

        public static ActionResult Json<TEntity, TViewModel>([DataSourceRequest] this DataSourceRequest request, IQueryable<TEntity> entities)
            where TEntity : class
            where TViewModel : class
        {
            var model = entities.Project().To<TViewModel>();
            return new JsonResult
            {
                Data = model.ToDataSourceResult(request),
                ContentType = null,
                ContentEncoding = null,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }

    public static class FindTypes
    {
        public static AssemblyNavigator InAssembly(Assembly assembly)
        {
            return new AssemblyNavigator(assembly);
        }
    }

    public class AssemblyNavigator
    {
        private readonly Assembly _assembly;

        public AssemblyNavigator(Assembly assembly)
        {
            _assembly = assembly;
        }

        public IEnumerable<T> Implementing<T>() where T : class
        {
            return 
                from type in _assembly.GetTypes()
                where typeof (T).IsAssignableFrom(type) && type.IsClass
                select Activator.CreateInstance(type) as T;
        }
    }

    public static class HttpResponseBaseExtensions
    {
        public static int SetAuthCookie(this HttpResponseBase responseBase, string name, bool rememberMe, UserData userData)
        {
            // In order to pickup the settings from config, we create a default cookie and use its values to create a new one
            var cookie = FormsAuthentication.GetAuthCookie(name, rememberMe);
            var ticket = FormsAuthentication.Decrypt(cookie.Value);
            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration,
                ticket.IsPersistent, userData.ToString(), ticket.CookiePath);
            var encTicket = FormsAuthentication.Encrypt(newTicket);

            // Use existing cookie. Could create new one but would have to copy settings over...
            cookie.Value = encTicket;

            responseBase.Cookies.Add(cookie);

            return encTicket.Length;
        }
    }
}