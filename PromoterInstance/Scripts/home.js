/// <reference path="typings/jquery/jquery.d.ts" /> 
/// <reference path="typings/jquery.cookie/jquery.cookie.d.ts" />
/// <reference path="kendo.all.d.ts" /> 
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var HomeViewModel = (function (_super) {
    __extends(HomeViewModel, _super);
    function HomeViewModel() {
        var _this = _super.call(this) || this;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    HomeViewModel.prototype.showAllClick = function (e) {
        var gridElement = $(e.currentTarget).prevAll(".k-grid");
        gridElement.data("showAll", true);
        var grid = gridElement.data("kendoGrid");
        $(e.currentTarget).hide();
        grid.dataSource.read();
    };
    HomeViewModel.prototype.gridParams = function (selector) {
        var showAll = $(selector).data("showAll") == true;
        return showAll ? null : { count: 5 };
    };
    return HomeViewModel;
}(kendo.data.ObservableObject));
function onDataBound() {
    var self = this, gridParent = self.element.parent(), alternateElement = gridParent.next(".show-on-hidden"), h1Element, hideLink, localStorageKey;
    h1Element = gridParent.find("h1");
    if (h1Element.length === 0) {
        h1Element = gridParent.prevAll("h1");
    }
    localStorageKey = h1Element.attr("id");
    if (self.dataSource.total() === 0) {
        gridParent.hide();
        alternateElement.show();
        hideLink = alternateElement.length === 0;
        if (!hideLink) {
            // Es gibt ein alternatives Element zur Tabelle (.show-on-hide), daher müssen wir den Navigationslink nicht ausblenden.
            window.localStorage.setItem(localStorageKey, JSON.stringify(false));
            return;
        }
        window.localStorage.setItem(localStorageKey, JSON.stringify(true));
        $("a[href$=#" + localStorageKey + "]").parent().hide();
    }
    else {
        window.localStorage.setItem(localStorageKey, JSON.stringify(false));
    }
    if (typeof (self.pager) != "undefined") {
        if (self.dataSource.totalPages() < 2) {
            self.pager.element.hide();
        }
        else {
            self.pager.element.show();
        }
    }
}
var viewModel = new HomeViewModel();
var missingReports = new kendo.data.ObservableArray([]);
$(document).ready(function () {
    var profileElement = $("#profile"), currentCampaignElement = $("#currentCampaigns"), loadProfileCard = function () {
        profileElement
            .html("Profil wird geladen...")
            .load(appPath + "Profile/_Card?_" + new Date().getTime(), function (response, status) {
            if (status == "error") {
                profileElement.html("Ladevorgang fehlgeschlagen. <a href='javascript:void(0)'>Erneut versuchen.</a>");
                profileElement.one("click", "a", loadProfileCard);
            }
        });
    }, loadCurrentCampaign = function () {
        currentCampaignElement.html("Wird geladen...")
            .load(appPath + "MyJobs/_Current", function (response, status) {
            if (status == "error") {
                currentCampaignElement.html("Ladevorgang fehlgeschlagen. <a href='javascript:void(0)'>Erneut versuchen.</a>");
                currentCampaignElement.one("click", "a", loadCurrentCampaign);
            }
            else {
                if (currentCampaignElement.find(".wrapper").length === 0) {
                    $("#Aktionen").hide();
                    $("a[href$=#Aktionen]").parent().hide();
                    window.localStorage.setItem("Aktionen", JSON.stringify(true));
                }
                else {
                    window.localStorage.setItem("Aktionen", JSON.stringify(false));
                }
            }
        });
    };
    loadProfileCard();
    loadCurrentCampaign();
    kendo.bind(".data-bound", viewModel);
    $("#m").kendoGrid({
        "columns": [
            { "title": "Aktion", "field": "Campaign", "encoded": true },
            { "title": "Datum", "width": "90px", "template": "#= kendo.format(\u0027{0:dd.MM.yyyy}\u0027, Date) #", "field": "Date", "encoded": true },
            { "title": "Einsatzort", "field": "LocationName", "encoded": true },
            { "title": "Straße", "field": "Street", "encoded": true },
            { "title": "PLZ", "width": "80px", "field": "PostalCode", "encoded": true },
            { "title": "Ort", "field": "City", "encoded": true },
            { "width": "255px", "template": "\u003ca class=\u0027k-button\u0027 href=\u0027/Home/RedirectTo?url=#= Url #\u0027\u003eZur Berichtseingabe\u003c/a\u003e" }
        ],
        "scrollable": false,
        "autoBind": false,
        "dataSource": {
            "data": [],
            "serverPaging": false,
            "serverSorting": false,
            "serverFiltering": false,
            "serverGrouping": false,
            "serverAggregates": false,
            "filter": [],
            "sort": {
                field: "Date",
                dir: "asc"
            },
            "schema": {
                "data": "Data",
                "total": "Total",
                "errors": "Errors",
                "model": {
                    "fields": {
                        "Url": { "type": "string" },
                        "Campaign": { "type": "string" },
                        "Date": { "type": "date" },
                        "LocationName": { "type": "string" },
                        "Street": { "type": "string" },
                        "PostalCode": { "type": "string" },
                        "City": { "type": "string" }
                    }
                }
            }
        },
        dataBound: onDataBound
    });
    kendo.ui.progress($("#m"), true);
    $("#externalReportTool").load(appPath + "MissingReports/_ExternalReportTool");
    $.getJSON(appPath + "MissingReports/_Urls")
        .done(function (urls) {
        var ajaxRequests = [];
        $.each(urls, function (index, value) {
            // Unser AJAX-Request kann entweder erfolgreich sein oder fehlschlagen. Dummerweise
            // Feuert $.when aber immer genau dann, wenn entweder alle erfolgreich waren oder sobald
            // der erste Request fehlgeschlagen ist. Das ist aber nicht was wir wollen, weshalb wir
            // ein paar Hilfs-Deferred-Objekte erzeugen.
            var deferred = $.Deferred();
            ajaxRequests.push(deferred);
            $.ajax({
                url: value,
                dataType: "json",
                global: false,
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        data[i].Date = new Date(parseInt(data[i].Date.replace("/Date(", "").replace(")/", ""), 10));
                        missingReports.push(data[i]);
                    }
                } //,
            }).always(deferred.resolve);
        });
        $.when.apply($, ajaxRequests).done(function () {
            var grid = $("#m").data("kendoGrid");
            grid.dataSource.data(missingReports);
            kendo.ui.progress($("#m"), false);
        });
    });
});
