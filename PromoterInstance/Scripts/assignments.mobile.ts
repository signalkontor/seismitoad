﻿$(document).ready(() => {
    // Das Schema für die Datasources unserer beiden Listviews
    var schema = {
        "data": "Data",
        "total": "Total",
        "errors": "Errors",
        "model": {
            "fields": {
                "Id": { "type": "number" },
                "DateStart": { "type": "date" },
                "DateEnd": { "type": "date" },
                "CampaignName": { "type": "string" },
                "LocationId": { "type": "number" },
                "LocationName": { "type": "string" },
                "Street": { "type": "string" },
                "PostalCode": { "type": "string" },
                "City": { "type": "string" },
                "State": { "type": "string" },
                "Remarks": { "type": "string" },
                "Contact": { "type": "string" },
                "Team": { "type": "string" }
            }
        }
    };
    // Das ist die DataSource mit den unbestätigten geplanten Einsätzen. Das passiert durch einen clientseitigen Filter.
    var dataSource2 = new kendo.data.DataSource({
        data: [],
        type: "aspnetmvc-ajax",
        serverFiltering: false,
        serverPaging: false,
        serverSorting: false,
        schema: schema,
        filter: { field: "State", operator: "equals", value: "Unbestätigt" }
    });
    // Das ist die DataSource mit allen geplanten Einsätzen.
    var dataSource1 = new kendo.data.DataSource({
        type: "aspnetmvc-ajax",
        serverFiltering: false,
        serverPaging: false,
        serverSorting: false,
        pageSize: 10,
        transport: {
            read: {
                url: appPath + "Assignment/_Planned",
                type: "POST"
            },
        },
        schema: schema
    });
    // Ändert sich die gesamt DataSource soll auch die gefilterte upgedatet werden.
    dataSource1.bind("change", function() {
        dataSource2.data(this.data());
    });

    // Hier erzeugen wir die beiden Listviews
    jQuery("#oa").kendoListView({
        "dataBound": function() { // Muss eine Funktion sein, weil der TS Compiler da sonst irgendwas vermasselt
            $("#no-items1").toggle(this.dataSource.total() === 0);
            $("#has-items1").toggle(this.dataSource.total() > 0);
        },
        "dataSource": dataSource2,
        "template": kendo.template(jQuery("#template").html())
    });
    $("#aa").kendoListView({
        "dataBound": function() { // Muss eine Funktion sein, weil der TS Compiler da sonst irgendwas vermasselt
            $("#no-items2").toggle(this.dataSource.total() === 0);
        },
        "dataSource": dataSource1,
        "template": kendo.template(jQuery("#template").html())
    });
    // Nun kommen ein paar Click-Handler
    $(".assignments-header").click(function() {
        $(this).toggleClass("open").next().toggle(); // Öffnen/Schließen der Listen
    });
    $("#oa, #aa").on("click", ".toggle-icon", function() {
        $(this).toggleClass("open").nextAll(".details").toggle(); // Öffnen/Schließen der Details
    }).on("click", ".confirm", function () {
        var uid = $(this).closest("div").attr("data-uid"),
            item = dataSource2.getByUid(uid);
        postHelper(appPath + "Assignment/_ConfirmSingle", [ item["Id"] ]);
    }).on("click", ".decline", function () {
        var uid = $(this).closest("div").attr("data-uid"),
            item = dataSource2.getByUid(uid);
        postHelper(appPath + "Assignment/_DeclineSingle", [item["Id"]]);
    });
    $("#confirmAll, #declineAll").click(function () {
        var action = this.id, // Entwender "confirmAll" oder "declineAll"
            ids = dataSource2.view().map((e: any) => e.Id);
        postHelper(appPath + "Assignment/_" + action.replace("All", ""), ids);
    });

    function postHelper(url: string, ids: number[]) : void {
        $.blockUI({ message: "Bitte warten..." });
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(ids),
            error: () => {
                alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch mal.");
                $.unblockUI();
            },
            success: () => {
                // Mit leichter verzögerung senden, da sonst komischerweise der Status noch nicht gesetzt ist.
                // Eigentlich wird das SQL Update in _Decline und _Confirm synchron ausgeführt, vielleicht
                // braucht der Trigger der abläuft Zeit?
                window.setTimeout(() => {
                    dataSource1.read();
                    $.unblockUI();
                }, 200);
            }
        });        
    }
});