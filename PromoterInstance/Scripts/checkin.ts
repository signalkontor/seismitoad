﻿

class CheckInViewModel extends kendo.data.ObservableObject {
    assignmentId: number;
    campaign: string;
    dateStart: Date;
    dateEnd: Date;
    name: string;
    street: string;
    postalCode: string;
    city: string;
    remkars: string;
    contact: string;
    team: string;
    time: string;
    lng: number;
    lat: number;

    firstTry = true;
    noCoordinates = false;
    errorNoSignalerrorNoSignal = false;
    errorAccuracy = false;
    errorTooFarAway = false;
    errorService = false;
    succeeded = false;

    constructor(data: any) {
        super();
        this.set("assignmentId", data.Id);
        this.set("campaign", data.Campaign);
        this.set("dateStart", kendo.parseDate(data.DateStart));
        this.set("dateEnd", kendo.parseDate(data.DateEnd));
        this.set("name", data.Name);
        this.set("street", data.Street);
        this.set("postalCode", data.PostalCode);
        this.set("city", data.City);
        if (data.HasCoordinates === false) {
            this.set("noCoordinates", true);
        }
        this.set("remarks", data.Remarks);
        this.set("contact", data.Contact);
        this.set("team", data.Team);
        super.init(this);
    }

    date() {
        return kendo.toString(this.get("dateStart"), "ddd. dd.MM.yyyy");
    }

    from() {
        return kendo.toString(this.get("dateStart"), "HH:mm");
    }

    til() {
        return kendo.toString(this.get("dateEnd"), "HH:mm");
    }

    lngFormat() {
        return this.formatCoordinate(this.get("lng"));
    }

    latFormat() {
        return this.formatCoordinate(this.get("lat"));
    }

    private formatCoordinate(coordinate: number) {
        var degrees = Math.floor(coordinate),
            minutes = Math.floor((coordinate - degrees) * 60),
            seconds = ((coordinate - degrees) * 60 - minutes) * 60;
        return kendo.format("{0}° {1}' {2:0.00}\"", degrees, minutes, seconds);
    }

    hasErrors() {
        if (this.get("errorSignal") === true)
            return true;
        if (this.get("errorAccuracy") === true)
            return true;
        if (this.get("errorTooFarAway") === true)
            return true;
        if (this.get("errorService") === true)
            return true;
        return false;
    }

    checkIn() {
        this.set("firstTry", false);
        this.set("errorNoSignal", false);
        this.set("errorAccuracy", false);
        this.set("errorTooFarAway", false);
        this.set("errorService", false);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition($.proxy(this.handleSuccess, this), $.proxy(this.handleError, this));
        }
        else {
            this.set("errorNoSignal", true);
        }
    }

    private handleSuccess(position: Position) {
        if (typeof (position) === "undefined" ||
            typeof (position.coords) === "undefined" ||
            typeof (position.coords.latitude) === "undefined") {
            this.set("errorNoSignal", true);
            return;
        }
        var self = this;
        self.set("lng", position.coords.longitude);
        self.set("lat", position.coords.latitude);
        $.post(appPath + "CheckIn/_CheckIn", {
                assignmentId: self.get("assignmentId"),
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                accuracy: position.coords.accuracy
            })
            .done(data => {
                self.set("time", kendo.toString(kendo.parseDate(data.time), "HH:mm"));
                self.set(data.property, true);
            })
            .fail(() => {
                self.set("errorService", true);
            });
    }

    private handleError() {
        this.set("errorNoSignal", true);
    }
}

$(document).ready(() => {
    var opts = {
        url: appPath + "CheckIn/_CurrentLocation",
        success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
            var element = $("#mvvm-container"),
                checkInViewModel = new CheckInViewModel(data);

            kendo.bind(element, checkInViewModel);
            element.show();
        },
        error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
            switch (jqXHR.status) {
            case 404: // Not Found
                $("#no-assignment").show();
                break;
            case 409: // Conflict
                $("#did-not-attend").show();
                break;
            case 410: // Gone
                $("#already-checked-in").show();
                break;
            default:
                window.location.href = appPath;
                break;
            }
        },
        method: "POST"
    };
    $.ajax(opts);
});