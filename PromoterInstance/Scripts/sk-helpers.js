﻿/*
 * Dieses Skript stellt folgende Funktionen bereit.
 * Vorrausetzungen: jQuery, jQueryUI, Eric Hynds Multiselect + Multiselect-Filter
 *
 * Submitting-Checkboxes:
 *   Checkboxen mit der Klasse "submitting-checkbox" schicken automatisch 
 *   das Formular zu dem sie gehören ab. Dies erfolgt zur Zeit immer per POST.
 *
 * Multiselect:
 *   Macht aus allen <select multiple="multiple" /> Elementen schöne Multiselects.
 *   Hat das Element die CSS-Klasse "filter" ist das Multiselect filterbar.
 *   Ist keine der Optionen ausgewählt zeigt das Multiselect standardmäßig
 *   "Bitte wählen..." an. Dieser Text kann geändert werden indem man dem Element
 *   ein "label"-Attribute gibt und darüber den Text setzt.
 *   Beispiel: <select multiple="multiple" label="Noch kein Wert gewählt" />
 *
 * Confirm:
 *   Elemente die ein "confirm" Attribut haben fragen bei Kicks nach einer Bestätigung.
 *   Als Meldung wird dabei der Wert des "confirm" Attributes genommen. Gibt der User
 *   die negative Antwort wird "false" zurückgegeben. So kann man z.B. bei einem Submit-
 *   Button das senden des Formulars verhindern.
 *
 * Toggle:
 *   Elemente die ein "toggle" Attribut haben blenden nach dem Anklicken das - über den
 *   Selector im "toggle" Attribut spezifitierte - Element ein bzw. aus.
 *
 * registerToggle: (Diese Funktion hat nichts mit der o.g. Toggle-Funktion zu tun.)
 *   Mittels registerToggle kann man beliebige DOM-Elemente abhängig von dem Wert
 *   eines Input-Elements sichtbar bzw. unsichtbar machen.
 *
 *   Der erste Parameter ist ein jQuery-Selektor der das Input-Element angibt, dessen
 *   Wert für die Sichtbarkeit ausschlaggebend ist. Der zweite Parameter ist ein
 *   jQuery-Selektor der die Zielelemente angibt. Hier ist zu beachten, dass nicht
 *   direkt die Zielelement ein- und ausgeblendet werden, sondern ihr Elternelement,
 *   sowie dessen Vorgänger (auf gleicher Ebene). Der Grund ist hierfür, dass unsere
 *   input Elemente immer in einem <div class="editor-field"> stehen und es zu jedem
 *   Feld ein <div class="editor-label"> Element gibt, welches mit ausgeblendet werden
 *   soll. Der dritte Parameter ist ein Array mit den Werten, für die die Zielelemente
 *   sichtbar sein sollen.
 *
 *   Beispiel (ungeprüft):
 *   <div class="editor-label">
 *       <label for="someField">A label</label>
 *   </div>
 *   <div class="editor-field">
 *       <input id="someField" name="someField type="text" />
 *   </div>
 *   <div class="editor-label">
 *       <label for="anotherField">A label</label>
 *   </div>
 *   <div class="editor-field">
 *       <input id="anotherField" name="anotherField type="text" />
 *   </div>
 *   <script>
 *      registerToggle("#someField", "#anotherField", [ "abc", "xyz" ]);
 *   </script>
 *   
 *   Wenn der Wert des Textfeldes "someField" "abc" oder "xyz" ist wird das andere Textfeld
 *   ausgeblendet. Hat die Textbox einen anderen Wert wird es angezeigt (wieder eingeblendet).
 *
 *  Close-Window-On-Escape:
 *    Telerik-Windows werden bei drücken von Escape geschlossen. Diese Funktion funktioniert
 *    nur, wenn das jQuery Hotkeys Plugin eingebunden wurde.
 *
 * String.startsWith:
 *   Erweitert den String-Datentyp um die Methode startsWith().
 *
 * String.contains:
 *   Erweitert den String-Datentyp um die Methode contains().
 */


/*
 * Wir packen einige Sachen in diese Funktion, damit diese Funktion auch in Partials
 * aufgerufen werden kann, wenn z.B. ein Form Submit ein Partial zurückliefert in dem wiederum 
 * Elemente mit jQuery behandelt werden müssen. Eine Form #myForm würde also loadLive('#myForm')
 * im Result eines Ajax Requests aufrufen.
 */
// TODO diese Methode in eine Klasse packen
function loadLive(context) {
    $("input[type=checkbox].submitting-checkbox", context).click(function () {
        var form = $(this).closest("form");
        // TODO Anhand "method" der Form get oder post verwenden
        $.post(form.attr("action"), form.serialize());
    });

    $("[confirm]").click(function (event) {
        if (!confirm($(this).attr("confirm"))) {
            event.preventDefault();
            return false;
        }
        return true;
    });

    $("[toggle]", context).click(function (e) {
        e.preventDefault();
        $($(this).attr("toggle")).toggle('slow');
    });
}

$(document).ready(function () {
    //$.ajaxSetup({ cache: false });
    loadLive();
});

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.contains != 'function') {
    String.prototype.contains = function (str) {
        return this.indexOf(str) >= 0;
    };
}

function registerToggle(toggleSourceSelector, toggleDestinationSelector, toggleValues) {
    var toggleSource = $(toggleSourceSelector);
    var toggleDestination = $(toggleDestinationSelector).parents(".editor-field");

    var checkAndToggle = function (value, speed) {
        for (var i = 0; i < toggleValues.length; i++) {
            if (toggleValues[i] == value) {
                toggleDestination.show(speed);
                toggleDestination.prev().show(speed);
                return;
            }
        }
        toggleDestination.hide(speed);
        toggleDestination.prev().hide(speed);
    }

    checkAndToggle(toggleSource.val(), null);

    toggleSource.bind("change", function (e) {
        checkAndToggle(e.target.value, "slow");
    });
}
