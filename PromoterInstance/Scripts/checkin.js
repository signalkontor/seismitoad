var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CheckInViewModel = (function (_super) {
    __extends(CheckInViewModel, _super);
    function CheckInViewModel(data) {
        var _this = _super.call(this) || this;
        _this.firstTry = true;
        _this.noCoordinates = false;
        _this.errorNoSignalerrorNoSignal = false;
        _this.errorAccuracy = false;
        _this.errorTooFarAway = false;
        _this.errorService = false;
        _this.succeeded = false;
        _this.set("assignmentId", data.Id);
        _this.set("campaign", data.Campaign);
        _this.set("dateStart", kendo.parseDate(data.DateStart));
        _this.set("dateEnd", kendo.parseDate(data.DateEnd));
        _this.set("name", data.Name);
        _this.set("street", data.Street);
        _this.set("postalCode", data.PostalCode);
        _this.set("city", data.City);
        if (data.HasCoordinates === false) {
            _this.set("noCoordinates", true);
        }
        _this.set("remarks", data.Remarks);
        _this.set("contact", data.Contact);
        _this.set("team", data.Team);
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    CheckInViewModel.prototype.date = function () {
        return kendo.toString(this.get("dateStart"), "ddd. dd.MM.yyyy");
    };
    CheckInViewModel.prototype.from = function () {
        return kendo.toString(this.get("dateStart"), "HH:mm");
    };
    CheckInViewModel.prototype.til = function () {
        return kendo.toString(this.get("dateEnd"), "HH:mm");
    };
    CheckInViewModel.prototype.lngFormat = function () {
        return this.formatCoordinate(this.get("lng"));
    };
    CheckInViewModel.prototype.latFormat = function () {
        return this.formatCoordinate(this.get("lat"));
    };
    CheckInViewModel.prototype.formatCoordinate = function (coordinate) {
        var degrees = Math.floor(coordinate), minutes = Math.floor((coordinate - degrees) * 60), seconds = ((coordinate - degrees) * 60 - minutes) * 60;
        return kendo.format("{0}° {1}' {2:0.00}\"", degrees, minutes, seconds);
    };
    CheckInViewModel.prototype.hasErrors = function () {
        if (this.get("errorSignal") === true)
            return true;
        if (this.get("errorAccuracy") === true)
            return true;
        if (this.get("errorTooFarAway") === true)
            return true;
        if (this.get("errorService") === true)
            return true;
        return false;
    };
    CheckInViewModel.prototype.checkIn = function () {
        this.set("firstTry", false);
        this.set("errorNoSignal", false);
        this.set("errorAccuracy", false);
        this.set("errorTooFarAway", false);
        this.set("errorService", false);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition($.proxy(this.handleSuccess, this), $.proxy(this.handleError, this));
        }
        else {
            this.set("errorNoSignal", true);
        }
    };
    CheckInViewModel.prototype.handleSuccess = function (position) {
        if (typeof (position) === "undefined" ||
            typeof (position.coords) === "undefined" ||
            typeof (position.coords.latitude) === "undefined") {
            this.set("errorNoSignal", true);
            return;
        }
        var self = this;
        self.set("lng", position.coords.longitude);
        self.set("lat", position.coords.latitude);
        $.post(appPath + "CheckIn/_CheckIn", {
            assignmentId: self.get("assignmentId"),
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            accuracy: position.coords.accuracy
        })
            .done(function (data) {
            self.set("time", kendo.toString(kendo.parseDate(data.time), "HH:mm"));
            self.set(data.property, true);
        })
            .fail(function () {
            self.set("errorService", true);
        });
    };
    CheckInViewModel.prototype.handleError = function () {
        this.set("errorNoSignal", true);
    };
    return CheckInViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    var opts = {
        url: appPath + "CheckIn/_CurrentLocation",
        success: function (data, textStatus, jqXHR) {
            var element = $("#mvvm-container"), checkInViewModel = new CheckInViewModel(data);
            kendo.bind(element, checkInViewModel);
            element.show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (jqXHR.status) {
                case 404:
                    $("#no-assignment").show();
                    break;
                case 409:
                    $("#did-not-attend").show();
                    break;
                case 410:
                    $("#already-checked-in").show();
                    break;
                default:
                    window.location.href = appPath;
                    break;
            }
        },
        method: "POST"
    };
    $.ajax(opts);
});
