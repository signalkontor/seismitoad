﻿function onGridEdit(editEvent) {
    var container = $(editEvent.container[0]),
        otherTaskElements = container.find("input[name=OtherTask], label[for=OtherTask]"),
        uid = editEvent.model.uid;

    editEvent.container.find(".k-edit-buttons .k-grid-update").after("<button class='k-button' id='delete-button'>Löschen</button>");
    $("#delete-button").click((e) => {
        var element = $(editEvent.sender.table).find("tr[data-uid='" + uid + "']");
        e.preventDefault();
        editEvent.sender.cancelRow();
        editEvent.sender.removeRow(element);
    });

    if (editEvent.model.get("Task") !== 13) {
        otherTaskElements.hide();
        otherTaskElements.filter("input").removeAttr("data-val-required");
    } else {
        otherTaskElements.filter("input").attr("data-val-required", "Das Feld \"Sonstige Tätigkeit\" ist erforderlich.");
    }

    editEvent.model.bind("change", e => {
        var model = e.sender.source || e.sender;
        switch (e.field) {
        case "Task":
            if (model.get(e.field) === 13) {
                otherTaskElements.slideDown(500);
                otherTaskElements.filter("input").attr("data-val-required", "Das Feld \"Sonstige Tätigkeit\" ist erforderlich.");
            } else {
                otherTaskElements.filter("input").removeAttr("data-val-required");
                otherTaskElements.slideUp(500);
                model.set("OtherTask", null);
            }
            break;
        }
    });
}

function textFor(field: string, value: number) {
    var grid = <kendo.ui.Grid>$("#e").data("kendoGrid"),
        column = $.grep(grid.columns, element => (<kendo.ui.GridColumn>element).field == field)[0];
    return $.grep(<any[]>column.values, item => item.value == value)[0].text;
}