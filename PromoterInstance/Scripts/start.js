/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jquery.blockUI/jquery.blockUI.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var StartViewModel = (function (_super) {
    __extends(StartViewModel, _super);
    function StartViewModel() {
        var _this = _super.call(this) || this;
        _this.termsChecked = false;
        _this.deleteChecked = false;
        _this.deleteVisible = false;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    StartViewModel.prototype.password1Color = function () {
        var password = this.get("password");
        if (typeof (password) === "string") {
            if (password.length === 0) {
                return "";
            }
            return password.length >= 6 ? "green" : "red";
        }
        return "";
    };
    StartViewModel.prototype.password2Color = function () {
        var password = this.get("password");
        if (typeof (password) !== "string" || password.length === 0) {
            return "";
        }
        return this.passwordsMatch() ? "green" : "red";
    };
    StartViewModel.prototype.submitColor = function () {
        return this.continueToProfileEnabled() ? "" : "lightcoral";
    };
    StartViewModel.prototype.deleteColor = function () {
        return this.get("deleteChecked") ? "" : "lightcoral";
    };
    StartViewModel.prototype.passwordLengthValid = function () {
        var password = this.get("password");
        return typeof (password) === "string" && password.length >= 6;
    };
    StartViewModel.prototype.passwordsMatch = function () {
        return this.passwordLengthValid() && this.get("password") === this.get("password2");
    };
    StartViewModel.prototype.continueToProfileEnabled = function () {
        return this.get("termsChecked") && this.passwordsMatch();
    };
    StartViewModel.prototype.showDelete = function () {
        this.set("deleteVisible", true);
    };
    StartViewModel.prototype.cancel = function () {
        this.set("deleteVisible", false);
    };
    StartViewModel.prototype.closeClick = function () {
        $("#popup").data("kendoWindow").close();
    };
    StartViewModel.prototype.submit = function () {
        $(".k-window").block({ message: "Bitte warten..." });
        $.post(appPath + "Start/_DeleteProfile/" + $("#StartCode").val())
            .fail(function () {
            alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch einmal.");
        }).always(function () {
            $(".k-window").unblock();
        }).done(function () {
            $("#popup").data("kendoWindow").close();
            $("#deleted-popup").data("kendoWindow").center().open();
        });
    };
    return StartViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    var viewModel = new StartViewModel();
    kendo.bind("#data-bound", viewModel);
    viewModel.set("password", $("#Password").val());
    viewModel.set("password2", $("#Password2").val());
    viewModel.bind("change", function (e) {
        if (e.field === "deleteVisible") {
            $("#popup").data("kendoWindow").center();
        }
    });
});
