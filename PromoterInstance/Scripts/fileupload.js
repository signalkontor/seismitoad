/// <reference path="typings/jquery/jquery.d.ts" /> 
/// <reference path="typings/jquery.cookie/jquery.cookie.d.ts" />
/// <reference path="kendo.all.d.ts" /> 
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var FileUploadViewModel = (function (_super) {
    __extends(FileUploadViewModel, _super);
    function FileUploadViewModel(filename, displayUrl, exists) {
        var _this = _super.call(this) || this;
        _this.filename = filename;
        _this.displayUrl = displayUrl;
        _this.exists = exists;
        _this.selector = "#" + filename;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    FileUploadViewModel.prototype.upload = function (e) {
        e.preventDefault();
        console.log("upload " + this.selector);
        $(this.selector + "Window").data("kendoWindow").close();
        $(this.selector).find(".k-upload input").last().click();
    };
    FileUploadViewModel.prototype.remove = function (e) {
        var _this = this;
        e.preventDefault();
        $.post(appPath + "File/Remove?filenames=" + this.filename)
            .done(function () {
            $(_this.selector).find("a.fancybox")
                .attr("href", appPath + "Images/404.jpg")
                .find("img").attr("src", appPath + "Images/404.jpg?width=200&height=200&mode=max");
            _this.set("exists", false);
            $(_this.selector + "Window").data("kendoWindow").close();
        })
            .fail(function () {
            alert("Es ist ein Fehler aufgetreten, versuche es später noch mal.");
        });
    };
    FileUploadViewModel.prototype.cancel = function (e) {
        e.preventDefault();
        $(this.selector + "Window").data("kendoWindow").close();
    };
    FileUploadViewModel.prototype.error = function (e) {
        alert(e.XMLHttpRequest.responseText);
    };
    FileUploadViewModel.prototype.success = function (e) {
        var extension = e.files[0].extension.toLowerCase(), isPdf = extension === ".pdf", newUrl = isPdf
            ? appPath + "Images/Icons/pdf.png"
            : this.displayUrl.replace(/\.(png|jpg|pdf)/i, extension) + "?_" + new Date().getTime() + "=x";
        this.set("exists", true);
        $(this.selector).find("a.fancybox")
            .attr("href", newUrl)
            .find("img").attr("src", newUrl + (isPdf ? "" : "&width=200&height=200&mode=max"));
    };
    FileUploadViewModel.prototype.buttonText = function () {
        return this.get("exists") === true ? "Bearbeiten" : "Hinzufügen";
    };
    FileUploadViewModel.prototype.buttonClick = function (e) {
        var position, wnd;
        e.preventDefault();
        if (this.get("exists") === true) {
            position = $(this.selector + " .action-button").offset();
            wnd = $(this.selector + "Window").data("kendoWindow");
            wnd.setOptions({ position: position });
            wnd.open();
        }
        else {
            $(this.selector).find(".k-upload input").last().click();
        }
    };
    FileUploadViewModel.prototype.deleteVisible = function () {
        return this.filename !== "UploadPortraitPhoto";
    };
    return FileUploadViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    $(".fancybox")["fancybox"]();
});
