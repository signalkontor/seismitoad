﻿/// <reference path="typings/jquery/jquery.d.ts" /> 
/// <reference path="typings/jquery.cookie/jquery.cookie.d.ts" />
/// <reference path="kendo.all.d.ts" /> 

declare var appPath: string;

class FileUploadViewModel extends kendo.data.ObservableObject {
    private selector: string;

    constructor(private filename: string, private displayUrl: string, public exists: boolean) {
        super();
        this.selector = "#" + filename;
        super.init(this);
    }

    upload(e: JQueryEventObject) {
        e.preventDefault();
        console.log("upload " + this.selector);
        $(this.selector + "Window").data("kendoWindow").close();
        $(this.selector).find(".k-upload input").last().click();
    }

    remove(e: JQueryEventObject) {
        e.preventDefault();
        $.post(appPath + "File/Remove?filenames=" + this.filename)
            .done(() => {
                $(this.selector).find("a.fancybox")
                    .attr("href", appPath + "Images/404.jpg")
                    .find("img").attr("src", appPath + "Images/404.jpg?width=200&height=200&mode=max");
                this.set("exists", false);
                $(this.selector + "Window").data("kendoWindow").close();
            })
            .fail(() => {
                alert("Es ist ein Fehler aufgetreten, versuche es später noch mal.");
            });
    }

    cancel(e: JQueryEventObject) {
        e.preventDefault();
        $(this.selector + "Window").data("kendoWindow").close();
    }

    error(e) {
        alert(e.XMLHttpRequest.responseText);
    }

    success(e: kendo.ui.UploadUploadEvent) {
        var extension = (<string>e.files[0].extension).toLowerCase(),
            isPdf = extension === ".pdf",
            newUrl = isPdf
                ? appPath + "Images/Icons/pdf.png"
                : this.displayUrl.replace(/\.(png|jpg|pdf)/i, extension) + "?_" + new Date().getTime() + "=x";

        this.set("exists", true);

        $(this.selector).find("a.fancybox")
            .attr("href", newUrl)
            .find("img").attr("src", newUrl + (isPdf ? "" : "&width=200&height=200&mode=max"));
    }

    buttonText() {
        return this.get("exists") === true ? "Bearbeiten" : "Hinzufügen";
    }

    buttonClick(e: JQueryEventObject) {
        var position: JQueryCoordinates,
            wnd: kendo.ui.Window;
        e.preventDefault();
        if (this.get("exists") === true) {
            position = $(this.selector + " .action-button").offset();
            wnd = $(this.selector + "Window").data("kendoWindow");
            wnd.setOptions({ position: position });
            wnd.open();
        } else {
            $(this.selector).find(".k-upload input").last().click();
        }
    }

    deleteVisible() {
        return this.filename !== "UploadPortraitPhoto";
    }
}

$(document).ready(() => {
    $(".fancybox")["fancybox"]();
});