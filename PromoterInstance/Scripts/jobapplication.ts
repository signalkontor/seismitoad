﻿/// <reference path="typings/jquery/jquery.d.ts" />

class JobApplicationViewModel extends kendo.data.ObservableObject {
    locationSelectionVisible: boolean = false;
    hasLocations: boolean = false;
    hasNoLocations: boolean = false;

    constructor() {
        super();
        super.init(this);
    }

    checkedIds = {};

    selectRow(checkbox: HTMLInputElement) {
        var checked = checkbox.checked,
            row = $(checkbox).closest("tr"),
            grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        this.checkedIds[dataItem["Id"]] = checked;
    }
}

$(document).ready(() => {
    var viewModel = new JobApplicationViewModel();
    var grid = <kendo.ui.Grid>$("#grid").data("kendoGrid");
    var firstDataBoundEvent = true;

    grid.bind("dataBound", e => {
        var view = e.sender.dataSource.view();
        for (var i = 0; i < view.length; i++) {
            if (viewModel.checkedIds[view[i].Id]) {
                e.sender.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    .find(".checkbox")
                    .attr("checked", "checked");
            }
        }

        window.setTimeout(() => {
            if ($("#grid .k-grid-content table").height() <= $("#grid .k-grid-content").height()) {
                $("#grid .k-grid-header").css("padding-right", 0);
            }
        }, 50);

        if (!firstDataBoundEvent) {
            return;
        }
        firstDataBoundEvent = false;
        viewModel.set("locationSelectionVisible", true);
        if ((<kendo.ui.Grid>e.sender).dataSource.total() > 0) {
            viewModel.set("hasLocations", true);
        } else {
            viewModel.set("hasNoLocations", true);
        }
    });

    grid.table.on("click", ".checkbox", function() { viewModel.selectRow(this); });
    kendo.bind("#locations", viewModel);
});