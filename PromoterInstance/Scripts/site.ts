﻿interface IMenuItem {
    url: string;
    text: string;
    icon?: string;
    color?: string;
}
var menuLoaded = false;

function loadMenu(alwaysCallback: () => void = () => { }) {
    $.post(appPath + "Home/_Menu")
        .done((menuItems: IMenuItem[]) => {
            var menu = $("#menu"),
                ul = $("<ul></ul>"),
                menuItem: IMenuItem;
            menuLoaded = true;

            for (var i = 0; i < menuItems.length; i++) {
                menuItem = menuItems[i];
                ul.append(`<li class="${menuItem.color}"><a href="${menuItem.url}"><span class="icon ${menuItem.icon}"></span>${menuItem.text}<span style="width: 100%; display: inline-block;"><span></a></li>`);
            }
            menu.children().remove();
            menu.append(ul);

            $("#menu li").on("click", function (e: JQueryEventObject) {
                e.preventDefault();
                e.stopPropagation();
                window.location.href = $(this).find("a").attr("href");
            });
        })
        .always(alwaysCallback);
}

$(() => {
    $("#login").load(`${appPath}Account/_Actions?_${new Date().getTime()}`);
    $("#menuIcon").click((e: JQueryEventObject) => {
        var toggle = () => {
            var menu = $("#menu");
            if (menu.is(":hidden")) {
                menu.show().animate({ right: "0" }, 300);
            } else {
                menu.animate({ right: "-100%" }, 300, () => { menu.hide(); });
            }
        };
        e.preventDefault();
        if (!menuLoaded) {
            loadMenu(toggle);
        } else {
            toggle();
        }
    }).each(() => {
        // Nur wenn es ein Menü gibt müssen wir das hier ausführen
        window.setTimeout(loadMenu, 150);
    });
});