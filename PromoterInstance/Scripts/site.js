var menuLoaded = false;
function loadMenu(alwaysCallback) {
    if (alwaysCallback === void 0) { alwaysCallback = function () { }; }
    $.post(appPath + "Home/_Menu")
        .done(function (menuItems) {
        var menu = $("#menu"), ul = $("<ul></ul>"), menuItem;
        menuLoaded = true;
        for (var i = 0; i < menuItems.length; i++) {
            menuItem = menuItems[i];
            ul.append("<li class=\"" + menuItem.color + "\"><a href=\"" + menuItem.url + "\"><span class=\"icon " + menuItem.icon + "\"></span>" + menuItem.text + "<span style=\"width: 100%; display: inline-block;\"><span></a></li>");
        }
        menu.children().remove();
        menu.append(ul);
        $("#menu li").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            window.location.href = $(this).find("a").attr("href");
        });
    })
        .always(alwaysCallback);
}
$(function () {
    $("#login").load(appPath + "Account/_Actions?_" + new Date().getTime());
    $("#menuIcon").click(function (e) {
        var toggle = function () {
            var menu = $("#menu");
            if (menu.is(":hidden")) {
                menu.show().animate({ right: "0" }, 300);
            }
            else {
                menu.animate({ right: "-100%" }, 300, function () { menu.hide(); });
            }
        };
        e.preventDefault();
        if (!menuLoaded) {
            loadMenu(toggle);
        }
        else {
            toggle();
        }
    }).each(function () {
        // Nur wenn es ein Menü gibt müssen wir das hier ausführen
        window.setTimeout(loadMenu, 150);
    });
});
