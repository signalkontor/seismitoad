﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="kendo.all.d.ts" /> 

module KendoWidgets {

    // Extend the default widget options (optional)
    export interface CheckboxMultiSelectOptions extends kendo.ui.ComboBoxOptions {
        selectAllText?: string;
        selectNoneText?: string;
        noneSelectedText?: string;
        singleSelectedText?: string;
        selectedText?: string;
    }

    // Create a class which inherits from the Kendo UI widget
    export class CheckboxMultiSelect extends kendo.ui.ComboBox {
        private _selectedValues: Object;
        private _headerText: string;
        private _inputName: string;

        constructor(element: Element, options?: CheckboxMultiSelectOptions) {
            // Set templates
            options.template = options.template || "<label style='display: block'><input type='checkbox' value='#= " + options.dataValueField + " #' />#= " + options.dataTextField + " #</label>";
            options.headerTemplate = options.headerTemplate || 
                "<div><a href='#' rel='all'><span class='k-icon k-i-tick' />" + (options.selectAllText || "Alle") + "</a>" +
                "<a href='#' rel='none'><span class='k-icon k-i-close' />" + (options.selectNoneText || "Keins") + "</a></div>";

            // Initialize base-class
            super(element, options);

            // Initialize
            this._inputName = this.element.attr("name");
            this.element.removeAttr("name");
            this._selectedValues = {};
            this._updateHeaderText();
            this.input.val(this._headerText);
            this["ns"] = ".kendoCheckBoxMultiSelect";

            // Set self to this for use in closures
            var self = this;

            // Event-Handling
            this["popup"].bind("close", event=> {
                self.input.val(self._headerText);
            });
            this.input
                .focus(event => { self.input.val(""); })
                .blur(event => {
                    if (self["popup"].visible() == false) {
                        self.input.val(self._headerText);
                    }   
                });
            this["header"].find("a").click(event=> {
                event.preventDefault();
                var action = $(event.target).attr("rel");
                switch(action) {
                    case "all":
                        self._checkAll();
                        break;
                    case "none":
                        self._uncheckAll();
                        break;
                }
            });
        }
        
        private _updateHeaderText() {
            var selectedCount = Object.keys(this._selectedValues).length;
            var options = (<CheckboxMultiSelectOptions>this.options);

            if (selectedCount == 0) {
                this._headerText = options.noneSelectedText || "Nichts gewählt";
            } else {
                var totalCount = this.dataSource.data().length;
                this._headerText = totalCount == 0
                    ? kendo.format(options.singleSelectedText || "{0} gewählt", selectedCount)
                    : kendo.format(options.selectedText || "{0} von {1} gewählt", selectedCount, totalCount);
            }
        }

        private _getDataItem(value: any) {
            var items = this.dataSource.view();
            for (var index = 0; index < items.length; index++) {
                if (items[index][this.options.dataValueField] == value) {
                    return items[index];
                }
            }
        }

        private _checkAll() {
            var self = this;
            this.ul.find("input[type=checkbox]").each((index, element) => {
                var checkbox = <HTMLInputElement>element;
                var dataItem = self._getDataItem(checkbox.value);
                checkbox.checked = true;
                self._selectedValues[checkbox.value] = dataItem;
            });
            this._change();
        }

        private _uncheckAll() {
            var self = this;
            this.ul.find(":checked").each((index, element)=> {
                var checkbox = <HTMLInputElement>element;
                checkbox.checked = false;
            });
            self._selectedValues = {};
            this._change();
        }

        private _updateHiddenInputs() {
            if (!this._inputName)
                return;

            var container = this.input.parent();
            container.find("input[type=hidden]").remove();
            for (var selectedValue in this._selectedValues) {
                if (this._selectedValues.hasOwnProperty(selectedValue)) {
                    container.append("<input type='hidden' name='" + this._inputName + "' value='" + selectedValue + "' />");
                }
            }
        }

        private _change() {
            this._updateHiddenInputs();
            this._updateHeaderText();
            this.trigger("change");
        }

        // Overridden methods
        _select(li: any) { }
        _blur() { }
       
        open() {
            var self = this;
            super.open();
            this.ul.find("input[type=checkbox]")
                .change(event => {
                    var checkbox = <HTMLInputElement>event.target;
                    if (checkbox.checked) {
                        var dataItem = self._getDataItem(checkbox.value);
                        self._selectedValues[checkbox.value] = dataItem;
                    } else {
                        delete self._selectedValues[checkbox.value];
                    }
                    self._change();
                })
            .each((index, element)=> {
                var checkbox = <HTMLInputElement>element;
                checkbox.checked = checkbox.value in self._selectedValues;
            });
        }

        dataItems(): any[] {
            var result = [];
            for (var selectedValue in this._selectedValues) {
                if (this._selectedValues.hasOwnProperty(selectedValue)) {
                    result.push(this._selectedValues[selectedValue]);
                }
            }
            return result;
        }

        value(): any;
        value(value: any[]): void;
        value(value?: any[]): any[] {
            if (value === undefined) {
                // Getter
                return this.dataItems();
            }

            if (value !== null && ($.isArray(value) || value instanceof kendo.data.ObservableArray)) {
                // Setter
                this._selectedValues = [];
                for (var i = 0; i < value.length; i++) {
                    var v = value[i].get ? value[i].get(this.options.dataValueField) : value[i];
                    this._selectedValues[v] = value[i];
                }
                var self = this;
                this.ul.find("input[type=checkbox]").each((index, element) => {
                    var checkbox = <HTMLInputElement>element;
                    checkbox.checked = checkbox.value in self._selectedValues;
                });
                this._updateHeaderText();
                this._updateHiddenInputs();
                this.input.val(this._headerText);
            }
        }
    }

    // Create an alias of the prototype (required by kendo.ui.plugin)
    CheckboxMultiSelect.fn = CheckboxMultiSelect.prototype;

    // Deep clone the widget default options
    CheckboxMultiSelect.fn.options = $.extend(true, {}, kendo.ui.ComboBox.fn.options);

    // Specify the name of your Kendo UI widget. Used to create the corresponding jQuery plugin.
    CheckboxMultiSelect.fn.options.name = "CheckBoxMultiSelect";
    
    // Create a jQuery plugin.
    kendo.ui.plugin(CheckboxMultiSelect);
}

interface JQuery {
    kendoCheckboxMultiSelect(options?: KendoWidgets.CheckboxMultiSelectOptions) : JQuery;
}

// Kendo should use the same binder for checkboxmultiselect as for the ordinary multiselect
(<any>kendo.data).binders.widget.checkboxmultiselect = (<any>kendo.data).binders.widget.multiselect;