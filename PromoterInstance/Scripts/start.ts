﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jquery.blockUI/jquery.blockUI.d.ts" />

class StartViewModel extends kendo.data.ObservableObject {
    password: string;
    password2: string;
    termsChecked: boolean = false;
    deleteChecked: boolean = false;
    deleteVisible: boolean = false;

    constructor() {
        super();
        super.init(this);
    }

    password1Color() {
        var password = this.get("password");
        if (typeof (password) === "string") {
            if (password.length === 0) {
                return "";
            }
            return password.length >= 6 ? "green" : "red";
        }
        return "";
    }

    password2Color() {
        var password = this.get("password");
        if (typeof (password) !== "string" || password.length === 0) {
            return "";
        }
        return this.passwordsMatch() ? "green" : "red";
    }

    submitColor() {
        return this.continueToProfileEnabled() ? "" : "lightcoral";
    }

    deleteColor() {
        return this.get("deleteChecked") ? "" : "lightcoral";
    }

    passwordLengthValid() {
        var password = this.get("password");
        return typeof (password) === "string" && password.length >= 6;
    }

    passwordsMatch() {
        return this.passwordLengthValid() && this.get("password") === this.get("password2");
    }

    continueToProfileEnabled() {
        return this.get("termsChecked") && this.passwordsMatch();
    }

    showDelete() {
        this.set("deleteVisible", true);
    }

    cancel() {
        this.set("deleteVisible", false);
    }

    closeClick() {
        $("#popup").data("kendoWindow").close();
    }

    submit() {
        $(".k-window").block({ message: "Bitte warten..." });

        $.post(appPath + "Start/_DeleteProfile/" + $("#StartCode").val())
            .fail(() => {
            alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch einmal.");
        }).always(() => {
                $(".k-window").unblock();
            }).done(() => {
                $("#popup").data("kendoWindow").close();
                $("#deleted-popup").data("kendoWindow").center().open();
            });
    }
}

$(document).ready(() => {
    var viewModel = new StartViewModel();
    kendo.bind("#data-bound", viewModel);
    viewModel.set("password", $("#Password").val());
    viewModel.set("password2", $("#Password2").val());
    viewModel.bind("change", function (e) {
        if (e.field === "deleteVisible") {
            $("#popup").data("kendoWindow").center();
        }
    });
});