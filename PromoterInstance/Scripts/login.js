/// <reference path="typings/jquery/jquery.d.ts" />
$(document).ready(function () {
    $("#slider").excoloSlider({
        interval: 5000,
        width: 1376,
    });
    $("#login").on("click", ".toggle-login-box", function (e) {
        e.preventDefault();
        $("#login-container").toggle();
        $("#UserName").focus();
    });
});
