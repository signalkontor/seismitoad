/// <reference path="typings/jquery/jquery.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var JobApplicationViewModel = (function (_super) {
    __extends(JobApplicationViewModel, _super);
    function JobApplicationViewModel() {
        var _this = _super.call(this) || this;
        _this.locationSelectionVisible = false;
        _this.hasLocations = false;
        _this.hasNoLocations = false;
        _this.checkedIds = {};
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    JobApplicationViewModel.prototype.selectRow = function (checkbox) {
        var checked = checkbox.checked, row = $(checkbox).closest("tr"), grid = $("#grid").data("kendoGrid"), dataItem = grid.dataItem(row);
        this.checkedIds[dataItem["Id"]] = checked;
    };
    return JobApplicationViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    var viewModel = new JobApplicationViewModel();
    var grid = $("#grid").data("kendoGrid");
    var firstDataBoundEvent = true;
    grid.bind("dataBound", function (e) {
        var view = e.sender.dataSource.view();
        for (var i = 0; i < view.length; i++) {
            if (viewModel.checkedIds[view[i].Id]) {
                e.sender.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    .find(".checkbox")
                    .attr("checked", "checked");
            }
        }
        window.setTimeout(function () {
            if ($("#grid .k-grid-content table").height() <= $("#grid .k-grid-content").height()) {
                $("#grid .k-grid-header").css("padding-right", 0);
            }
        }, 50);
        if (!firstDataBoundEvent) {
            return;
        }
        firstDataBoundEvent = false;
        viewModel.set("locationSelectionVisible", true);
        if (e.sender.dataSource.total() > 0) {
            viewModel.set("hasLocations", true);
        }
        else {
            viewModel.set("hasNoLocations", true);
        }
    });
    grid.table.on("click", ".checkbox", function () { viewModel.selectRow(this); });
    kendo.bind("#locations", viewModel);
});
