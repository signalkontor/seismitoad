﻿/// <reference path="typings/jquery.blockUI/jquery.blockUI.d.ts" />

function onDetailInit(e) {
    e.detailCell.load(appPath + "Assignment/_Details/" + e.data.Id);
}

class PlannedAssignmentsViewModel extends kendo.data.ObservableObject {
    selected: { [index: string]: boolean } = {};

    constructor() {
        super();
        super.init(this);
    }

    private getSelected(): string[] {
        var result = new Array<string>();
        for (var property in this.selected) {
            if (this.selected[property]) {
                result.push(property);
            }
        }
        return result;
    }

    private postHelper(url: string) {
        var selected = this.getSelected();
        if (selected.length === 0) {
            return;
        }
        $.blockUI({ message: "Bitte warten..." });
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(selected),
            error: () => {
                alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch mal.");
                $.unblockUI();
            },
            success: () => {
                // Mit leichter verzögerung senden, da sonst komischerweise der Status noch nicht gesetzt ist.
                // Eigentlich wird das SQL Update in _Decline und _Confirm synchron ausgeführt, vielleicht
                // braucht der Trigger der abläuft Zeit?
                window.setTimeout(() => {
                    $("#a").data("kendoGrid").dataSource.read();
                    $.unblockUI();
                }, 200);
            }
        });
    }

    confirm() {
        this.postHelper(appPath + "Assignment/_Confirm");
    }

    decline() {
        this.postHelper(appPath + "Assignment/_Decline");
    }

    updateSelected(checkbox: HTMLInputElement) {
        var assignmentId = checkbox.value,
            state = checkbox.checked;
        this.selected[assignmentId] = state;
    }
}

$(document).ready(() => {
    var checkAllOnNextDataBind = false;

    var plannedAssignmentsViewModel = new PlannedAssignmentsViewModel(),
        grid = <kendo.ui.Grid>$("#a").data("kendoGrid"),
        checkAllElement = $("#checkAll");

    grid.bind("dataBound", () => {
        checkAllElement.prop("checked", false);
        plannedAssignmentsViewModel.selected = {};
        if (checkAllOnNextDataBind) {
            checkAllOnNextDataBind = false;
            checkAllElement.click();
        }
    });

    kendo.bind("#planned-assignment-actions", plannedAssignmentsViewModel);
    checkAllElement.on("change", event => {
        var checkbox = <HTMLInputElement>event.target,
            showAllButton = $("#show-all-assigments-button");
        if (showAllButton.is(":visible")) {
            checkAllOnNextDataBind = true;
            showAllButton.click();
        } else {
            $("#a tbody :checkbox").prop("checked", checkbox.checked).change();
        }
    });

    // The Event-Handlers needs to be a function and not a lamdba expression.
    // This is because TypeScript will change this to _this in lambdas
    grid.tbody.on("click", "tr", function (event) {
        var element = $(event.target);
        if (element.is(":checkbox") ||
            element.hasClass("k-hierarchy-cell") ||
            element.parent().hasClass("k-hierarchy-cell")) {
            return;
        }
        $(":checkbox", this).click();
    });
    grid.tbody.on("change", "input[type=checkbox]", event => {
        plannedAssignmentsViewModel.updateSelected(<HTMLInputElement>event.target);
    });
});

