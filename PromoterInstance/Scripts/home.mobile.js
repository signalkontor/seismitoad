/// <reference path="typings/jquery/jquery.d.ts" /> 
/// <reference path="kendo.all.d.ts" /> 
$(document).ready(function () {
    var profileElement = $("#profile"), loadProfileCard = function () {
        profileElement
            .html("Wird geladen...")
            .load(appPath + "Profile/_Card?_" + new Date().getTime(), function (response, status) {
            if (status == "error") {
                profileElement.html("Ladevorgang fehlgeschlagen. <a href='javascript:void(0)'>Erneut versuchen.</a>");
                profileElement.one("click", "a", loadProfileCard);
            }
        });
    };
    loadProfileCard();
    loadMenu(function () { $("#menu").css("right", "0").css("display", "block"); });
});
function dataBound() {
    var self = this, gridParent = self.element.parent(), alternateElement = gridParent.next(".show-on-hidden");
    if (self.dataSource.total() === 0) {
        gridParent.hide();
        alternateElement.show();
    }
    if (typeof (self.pager) != "undefined") {
        if (self.dataSource.totalPages() < 2) {
            self.pager.element.hide();
        }
        else {
            self.pager.element.show();
        }
    }
}
function confirmDeclineHelper(url, id, msg) {
    if (confirm(msg)) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify([id]),
            error: function () {
                alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch mal.");
            },
            success: function () {
                // Mit leichter verzögerung senden, da sonst komischerweise der Status noch nicht gesetzt ist.
                // Eigentlich wird das SQL Update in _Decline und _Confirm synchron ausgeführt, vielleicht
                // braucht der Trigger der abläuft Zeit?
                window.setTimeout(function () {
                    $("#a").data("kendoGrid").dataSource.read();
                }, 200);
            }
        });
    }
}
function confirmClick(e) {
    e.preventDefault();
    confirmDeclineHelper(appPath + "Assignment/_Confirm", $(this).attr("value"), "Einsatz übernehmen");
}
function declineClick(e) {
    e.preventDefault();
    confirmDeclineHelper(appPath + "Assignment/_Decline", $(this).attr("value"), "Einsatz ablehnen");
}
function detailClick() {
    $(this).find("td.k-hierarchy-cell .k-icon").click();
}
$(document).ready(function () {
    var gridElement = $("#a");
    gridElement
        .on("click", "tr", detailClick)
        .on("click", ".confirm", confirmClick)
        .on("click", ".decline", declineClick);
});
