/// <reference path="typings/jquery.blockUI/jquery.blockUI.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function onDetailInit(e) {
    e.detailCell.load(appPath + "Assignment/_Details/" + e.data.Id);
}
var PlannedAssignmentsViewModel = (function (_super) {
    __extends(PlannedAssignmentsViewModel, _super);
    function PlannedAssignmentsViewModel() {
        var _this = _super.call(this) || this;
        _this.selected = {};
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    PlannedAssignmentsViewModel.prototype.getSelected = function () {
        var result = new Array();
        for (var property in this.selected) {
            if (this.selected[property]) {
                result.push(property);
            }
        }
        return result;
    };
    PlannedAssignmentsViewModel.prototype.postHelper = function (url) {
        var selected = this.getSelected();
        if (selected.length === 0) {
            return;
        }
        $.blockUI({ message: "Bitte warten..." });
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(selected),
            error: function () {
                alert("Es ist ein Fehler aufgetreten. Bitte versuche es später noch mal.");
                $.unblockUI();
            },
            success: function () {
                // Mit leichter verzögerung senden, da sonst komischerweise der Status noch nicht gesetzt ist.
                // Eigentlich wird das SQL Update in _Decline und _Confirm synchron ausgeführt, vielleicht
                // braucht der Trigger der abläuft Zeit?
                window.setTimeout(function () {
                    $("#a").data("kendoGrid").dataSource.read();
                    $.unblockUI();
                }, 200);
            }
        });
    };
    PlannedAssignmentsViewModel.prototype.confirm = function () {
        this.postHelper(appPath + "Assignment/_Confirm");
    };
    PlannedAssignmentsViewModel.prototype.decline = function () {
        this.postHelper(appPath + "Assignment/_Decline");
    };
    PlannedAssignmentsViewModel.prototype.updateSelected = function (checkbox) {
        var assignmentId = checkbox.value, state = checkbox.checked;
        this.selected[assignmentId] = state;
    };
    return PlannedAssignmentsViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    var checkAllOnNextDataBind = false;
    var plannedAssignmentsViewModel = new PlannedAssignmentsViewModel(), grid = $("#a").data("kendoGrid"), checkAllElement = $("#checkAll");
    grid.bind("dataBound", function () {
        checkAllElement.prop("checked", false);
        plannedAssignmentsViewModel.selected = {};
        if (checkAllOnNextDataBind) {
            checkAllOnNextDataBind = false;
            checkAllElement.click();
        }
    });
    kendo.bind("#planned-assignment-actions", plannedAssignmentsViewModel);
    checkAllElement.on("change", function (event) {
        var checkbox = event.target, showAllButton = $("#show-all-assigments-button");
        if (showAllButton.is(":visible")) {
            checkAllOnNextDataBind = true;
            showAllButton.click();
        }
        else {
            $("#a tbody :checkbox").prop("checked", checkbox.checked).change();
        }
    });
    // The Event-Handlers needs to be a function and not a lamdba expression.
    // This is because TypeScript will change this to _this in lambdas
    grid.tbody.on("click", "tr", function (event) {
        var element = $(event.target);
        if (element.is(":checkbox") ||
            element.hasClass("k-hierarchy-cell") ||
            element.parent().hasClass("k-hierarchy-cell")) {
            return;
        }
        $(":checkbox", this).click();
    });
    grid.tbody.on("change", "input[type=checkbox]", function (event) {
        plannedAssignmentsViewModel.updateSelected(event.target);
    });
});
