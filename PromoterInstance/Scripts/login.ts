﻿/// <reference path="typings/jquery/jquery.d.ts" />

interface ExcoloOptions {
    width?: number;
    height?: number;
    autoSize?: boolean;
    touchNav?: boolean;
    mouseNav?: boolean;
    prevnextNav?: boolean;
    prevnextAutoHide?: boolean;
    pagerNav?: boolean;
    startSlide?: number;
    autoPlay?: boolean;
    delay?: number;
    interval?: number;
    repeat?: boolean;
    playReverse?: boolean;
    hoverPause?: boolean;
    captionAutoHide?: boolean;
    animationCssTransitions?: boolean;
    animationDuration?: number;
    animationTimingFunction?: string;
    prevButtonClass?: string;
    nextButtonClass?: string;
    prevButtonImage?: string;
    nextButtonImage?: string;
    activeSlideClass?: string;
    slideCaptionClass?: string;
    pagerClass?: string;
    pagerImage?: string;
}

interface excoloSlider {
    (): any;
    (options: ExcoloOptions): any;
}

interface JQuery {
    excoloSlider?: excoloSlider;
}

$(document).ready(() => {
    $("#slider").excoloSlider({
        interval: 5000,
        width: 1376,
    });

    $("#login").on("click", ".toggle-login-box", (e) => {
        e.preventDefault();
        $("#login-container").toggle()
        $("#UserName").focus();
    });
});