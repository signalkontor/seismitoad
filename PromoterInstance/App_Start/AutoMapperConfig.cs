﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace PromoterInstance.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                var autoMapperProfiles = FindTypes
                    .InAssembly(typeof (AutoMapperConfig).Assembly)
                    .Implementing<Profile>();

                foreach (var autoMapperProfile in autoMapperProfiles)
                {
                    cfg.AddProfile(autoMapperProfile);
                }
            });
        }
    }
}