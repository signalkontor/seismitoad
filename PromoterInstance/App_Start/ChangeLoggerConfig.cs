﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance.App_Start
{
    internal static class ChangeLoggerConfig
    {
        public static void Configure()
        {
            ChangeLogger.UserKey = () =>
            {
                var user = HttpContext.Current.User;
                return user.Identity.IsAuthenticated
                    ? (Guid?)Membership.GetUser(user.Identity.Name).ProviderUserKey
                    : (Guid?)null;
            };

            ChangeLogger.DbContext = () => new SeismitoadDbContext();
        }
    }
}