﻿using System.Web;
using System.Web.Optimization;

namespace PromoterInstance
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                        "~/Scripts/jquery.blockUI.js",
                        "~/Scripts/jquery.fancybox.pack.js",
                        "~/Scripts/checkboxmultiselect.js",
                        "~/Scripts/sk-helpers.js",
                        "~/Scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/jquery.fancybox.css",
                "~/Content/style.css"));
        }
    }
}