﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Links;
using ObjectTemplate;
using SeismitoadShared.Constants;

namespace PromoterInstance.App_Start
{
    public class ObjectTemplateConfig
    {
        public static void Configure()
        {
            Configurator.Configure(config =>
            {
                config.SelectListRepository = new SelectListRepository();
                config.InfoIconUrl = Images.Icons.info_png;
                config.ShowValidationSummary = false;
                config.AddAllowedComplexTypes("Experiences", "PhoneIn3Fields", "ZeugnisUpload");
            });

            HtmlAttributes.Initialize();
        }
    }
}