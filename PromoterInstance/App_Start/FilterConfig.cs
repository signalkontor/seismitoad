﻿using SeismitoadShared.Filters;
using System.Web;
using System.Web.Mvc;

namespace PromoterInstance
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorLoggerFilter());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new IncompleteProfileFilter());
        }
    }
}