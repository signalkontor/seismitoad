using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using PromoterInstance.Models;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance
{
    public class AutoFacConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.Register((_) => new SeismitoadDbContext()).InstancePerHttpRequest();
            builder.Register<IMembershipService>((_) => new MembershipService()).SingleInstance();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}

