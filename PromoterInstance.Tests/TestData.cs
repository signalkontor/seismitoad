﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using AutoMapper;
using PromoterInstance.Models;
using SeismitoadModel;
using SeismitoadShared;
using SeismitoadShared.Models;

namespace PromoterInstance.Tests
{
    internal class TestData
    {
        internal const string TestStringValue = "TestString";
        internal const string TestPhoneNumber = "+49 40-123456";
        internal const string Email = "testuser@example.com";
        internal static readonly Guid ProviderUserKey = new Guid("5e8dcf6f-efa4-4219-94b6-03b930487c6e");
        public static readonly Guid NewUserProviderUserKey = new Guid("262b4afa-ac3e-4739-90ad-9d1cd47519a7");

        #region Listen die als DbSet gemockt werden können
        public List<Assignment> Assignments = new List<Assignment>();
        public List<Campaign> Campaigns = new List<Campaign>();
        public List<CampaignLocation> CampaignLocations = new List<CampaignLocation>();
        public List<Employee> Employees = new List<Employee>();
        public List<Location> Locations = new List<Location>();
        public List<Experience> Experiences = new List<Experience>();
        #endregion

        #region Ein paar Entitäten 
        private readonly AssignmentRole _rolleFachberater = new AssignmentRole
        {
            Id = 0,
            Name = "Fachberater/in*",
            ShortName = "FB*"
        };

        private readonly AssignmentRole _rolleModerator = new AssignmentRole
        {
            Id = 1,
            Name = "Moderator/in",
            ShortName = "M"
        };

        private readonly Employee _employee = new Employee
        {
            Id = 0,
            Firstname = "Test",
            Lastname = "User",
            Title = "Herr",
            ProviderUserKey = ProviderUserKey,
            AssignedEmployees = new Collection<AssignedEmployee>()
        };

        private readonly Employee _newEmployee = new Employee
        {
            Id = 1,
            Firstname = "New",
            Lastname = "User",
            Title = "Herr",
            ProviderUserKey = NewUserProviderUserKey
        };

        private readonly EmployeeProfile _employeeProfile = new EmployeeProfile
        {
            Id = 0,
            AccommodationPossible1 = "1;2",
            AccommodationPossible2 = TestStringValue,
            Apprenticeships = "1;2",
            ArtisticAbilities = "1;2",
            Birthday = new DateTime(1980, 1, 1),
            City = TestStringValue,
            Comment = TestStringValue,
            CopyHealthCertificate = null,
            CopyIdentityCard = 0,
            CopyTradeLicense = true,
            Country = 1,
            DriversLicense = null,
            DriversLicenseClass1 = null,
            DriversLicenseClass2 = null,
            DriversLicenseSince = null,
            Education = 1,
            Email2 = TestStringValue,
            ExternalTraining = TestStringValue,
            ExternalUntil = new DateTime(2000, 1, 1),
            FacialPiercing = null,
            FaxNo = null,
            FreelancerQuestionaire = null,
            GastronomicAbilities = "1;2",
            GeographicCoordinates = null,
            HairColor = 1,
            HardwareKnownledge = 1,
            HardwareKnownledgeDetails = TestStringValue,
            HasDigitalDriversCard = null,
            HealthCertificate = null,
            Height = 180,
            InternalTraining = TestStringValue,
            IsBahncardAvailable = null,
            IsCarAvailable = null,
            JeansLength = null,
            JeansWidth = null,
            LanguagesEnglish = 1,
            LanguagesFrench = 2,
            LanguagesGerman = 3,
            LanguagesItalian = 4,
            LanguagesOther = TestStringValue,
            LanguagesSpanish = 3,
            LanguagesTurkish = 2,
            OtherAbilities = "1;2",
            OtherAbilitiesText = TestStringValue,
            OtherCompletedApprenticeships = TestStringValue,
            PersonalDataComment = TestStringValue,
            PersonalityTestAnswers = TestStringValue,
            PhoneMobileNo = TestPhoneNumber,
            PhoneNo = TestPhoneNumber,
            PostalCode = TestStringValue,
            PreferredTasks = "1;2",
            PreferredTypes = "1;2",
            PreferredWorkSchedule = "1;2",
            ShirtSize = 1,
            Size = 52,
            SkeletonContract = true,
            Skype = TestStringValue,
            SoftwareKnownledge = 1,
            SoftwareKnownledgeDetails = TestStringValue,
            Sports = "1;2",
            Street = TestStringValue,
            Studies = true,
            StudiesDetails = TestStringValue,
            TaxNumber = TestStringValue,
            TaxOfficeLocation = TestStringValue,
            TravelWillingness = true,
            TurnoverTaxDeductible = false,
            ValidTradeLicense = true,
            VisibleTattoos = false,
            Website = TestStringValue,
            WorkExperience = TestStringValue
        };

        private Assignment CreateAssignment(int id, AssignmentState assignmentState, DateTime date)
        {
            var assignment = new Assignment
            {
                Id = id,
                DateStart = date,
                DateEnd = date.AddMinutes(1),
                State = assignmentState,
                AssignedEmployees = new Collection<AssignedEmployee>(),
                CampaignLocation = CampaignLocations.First()
            };

            var assignedEmployee = new AssignedEmployee
            {
                Assignment = assignment,
                Employee = _employee,
                AssignmentRoles = new Collection<AssignmentRole>() { _rolleFachberater }
            };

            assignment.AssignedEmployees.Add(assignedEmployee);
            return assignment;
        }
        #endregion

        public static readonly int[] PostponedCancelledDeletedAssignments = { 100, 101, 102, 103, 104, 105, 106, 107 };

        static TestData()
        {
            DependencyResolver.SetResolver(
                t => t == typeof (IMembershipService) ? new MockMembershipService(ProviderUserKey, Email) : null,
                t => t == typeof(IMembershipService) ? new [] { new MockMembershipService(ProviderUserKey, Email) } : null);
            InitializeAutomapper();
        }

        public TestData()
        {
            _employee.Profile = _employeeProfile;
            _employeeProfile.Employee = _employee;
            Employees.Add(_employee);
            Employees.Add(_newEmployee);

            CreateExperiences();
            CreateCampaigns();
            CreateLocationsAndAssignments();

            #region Create Special Assignments
            var tomorrow = LocalDateTime.Now.AddDays(1);
            var yesterday = LocalDateTime.Now.AddDays(-1);
            Assignments.Add(CreateAssignment(100, AssignmentState.PostponedLocation, tomorrow));
            Assignments.Add(CreateAssignment(101, AssignmentState.PostponedLocation, yesterday));
            Assignments.Add(CreateAssignment(102, AssignmentState.PostponedPromoter, tomorrow));
            Assignments.Add(CreateAssignment(103, AssignmentState.PostponedPromoter, yesterday));
            Assignments.Add(CreateAssignment(104, AssignmentState.Cancelled, tomorrow));
            Assignments.Add(CreateAssignment(105, AssignmentState.Cancelled, yesterday));
            Assignments.Add(CreateAssignment(106, AssignmentState.Deleted, tomorrow));
            Assignments.Add(CreateAssignment(107, AssignmentState.Deleted, yesterday));
            #endregion

            #region Create old Assignment

            var assignment = CreateAssignment(200, AssignmentState.Planned, LocalDateTime.Now.AddMonths(-3).AddHours(-1));
            assignment.AssignedEmployees.First().Attendance = Attendance.InTime;
            Assignments.Add(assignment);
            #endregion

        }

        private static readonly ExperienceType[] ExperienceTypes = new []
        {
            ExperienceType.Trainer,
            ExperienceType.MysteryShopper,
            ExperienceType.Merchandising,
            ExperienceType.MysteryShopper, 
            ExperienceType.Presentation,
            ExperienceType.Roadshow, 
            ExperienceType.TradeShow, 
            ExperienceType.Promotion, 
            ExperienceType.Merchandising,
            ExperienceType.Trainer, 
        };

        private void CreateExperiences()
        {
            Experiences.AddRange(Enumerable.Range(0, 10).Select(i => new Experience
            {
                Agency = "Agency " + i,
                Brand = "Bran " + i,
                Id = i,
                Employee = new Employee { Id = 1 },
                Product = "Product " + i,
                Sector = 2,
                SectorDetails = 1201,
                Timeframe = "Timeframe " + i,
                Type = ExperienceTypes[i],
            }));
        }

        private void CreateLocationsAndAssignments()
        {
            var baseDate = LocalDateTime.Now.AddDays(-6).AddHours(0.5);

            for (var i = 0; i < 20; i++)
            {
                var location = new Location
                {
                    Id = i,
                    Name = "Location " + i,
                    Street = "Street " + i,
                    City = "City " + i,
                    PostalCode = "D-1234" + i,
                    CampaignLocations = new Collection<CampaignLocation>()
                };

                var campaign = Campaigns[i % 3];
                var campaignLocation = new CampaignLocation
                {
                    Campaign = campaign,
                    Location = location
                };

                var assignment = new Assignment
                {
                    Id = i,
                    CampaignLocation = campaignLocation,
                    EmployeeCount = 1,
                    DateStart = baseDate.AddDays(i),
                    DateEnd = baseDate.AddDays(i).AddHours(5),
                    AssignedEmployees = new Collection<AssignedEmployee>()
                };

                // AssignemntId%4 = 2 oder 3 => Darf im Archiv angezeigt werden
                var attendance = Attendance.Unknown;
                switch (i % 4)
                {
                    case 0:
                        attendance = Attendance.Unknown;
                        break;
                    case 1:
                        attendance = Attendance.DidNotAttend;
                        break;
                    case 2:
                        attendance = Attendance.InTime;
                        break;
                    case 3:
                        attendance = Attendance.Late;
                        break;
                }

                var assignedEmployee = new AssignedEmployee
                {
                    Employee = _employee,
                    Assignment = assignment,
                    Attendance = attendance,
                    AssignmentRoles = new Collection<AssignmentRole>()
                };

                assignedEmployee.AssignmentRoles.Add(_rolleFachberater);
                assignedEmployee.AssignmentRoles.Add(_rolleModerator);
                assignment.AssignedEmployees.Add(assignedEmployee);
                _employee.AssignedEmployees.Add(assignedEmployee);
                location.CampaignLocations.Add(campaignLocation);
                Locations.Add(location);
                CampaignLocations.Add(campaignLocation);
                Assignments.Add(assignment);
            }
        }

        private void CreateCampaigns()
        {
            for (var i = 0; i < 3; i++)
            {
                Campaigns.Add(new Campaign
                {
                    Id = i,
                    Name = "Campaign " + i,
                    CampaignLocations = new Collection<CampaignLocation>()
                });
            }
        }

        private static void InitializeAutomapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<EditEmployeeProfileAutomapperProfile>();
                cfg.AddProfile<ExperienceViewModelAutomapperProfile>();
            });
        }
    }
}
