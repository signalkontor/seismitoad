using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Moq;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace PromoterInstance.Tests
{
    internal class MockDbContext
    {
        public bool ChangesSaved { get; private set; }

        private Mock<DbSet<T>> MockDbSet<T>(IEnumerable<T> testData) where T : class
        {
            var data = testData.AsQueryable();
            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IDbAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<T>(data.GetEnumerator()));

            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(new TestDbAsyncQueryProvider<Experience>(data.Provider));
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            return dbSet;
        }

        public MockDbContext()
        {
            var context = new Mock<SeismitoadDbContext>();
            var testData = new TestData();

            #region Assignments
            var assignments = MockDbSet(testData.Assignments);
            context.Setup(c => c.Assignments).Returns(assignments.Object);
            #endregion

            #region Employees
            var employees = MockDbSet(testData.Employees);
            context.Setup(c => c.Employees).Returns(employees.Object);
            #endregion

            #region Locations
            var locations = MockDbSet(testData.Locations);
            context.Setup(c => c.Locations).Returns(locations.Object);
            #endregion

            #region Experiences

            var experiences = MockDbSet(testData.Experiences);
            context.Setup(c => c.Experiences).Returns(experiences.Object);
            #endregion

            context.Setup(m => m.SaveChanges()).Callback(() => ChangesSaved = true);
            Object = context.Object;
        }

        public SeismitoadDbContext Object { get; private set; }
    }
}