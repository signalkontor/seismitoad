﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PromoterInstance.Controllers;

namespace PromoterInstance.Tests.Controllers
{
    [TestClass]
    public class LocationControllerTest
    {
        private static LocationController GetController()
        {
            return new LocationController()
            {
                ControllerContext = new MockControllerContext("some username").Object
            };
        }

        [TestMethod]
        public void VCard()
        {
            var controller = GetController();
            var result = controller.VCard(0) as ContentResult;
            Assert.IsNotNull(result, "Not of type ContentResult");
            Assert.AreEqual("text/vcard", result.ContentType);
            Assert.IsTrue(result.Content.StartsWith("BEGIN:VCARD"));
            Assert.IsTrue(result.Content.Trim().EndsWith("END:VCARD"));
            Assert.IsTrue(result.Content.Contains("Location 0"));
            Assert.IsTrue(result.Content.Contains("Street 0"));
            Assert.IsTrue(result.Content.Contains("City 0"));
        }
    }
}
