﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObjectTemplate.Extensions;
using PromoterInstance.Controllers;
using PromoterInstance.Models;

namespace PromoterInstance.Tests.Controllers
{
    [TestClass]
    public class ExperienceControllerTest
    {
        private static readonly UserData UserData = new UserData
        {
            EmployeeId = 1,
            Name = "Test"
        };

        private static ExperienceController GetController()
        {
            var mockContext = new MockDbContext();
            var mockControllerContext = new MockControllerContext("some username");
            mockControllerContext.AddAuthCookie(UserData.ToString());
            return new ExperienceController(mockContext.Object)
            {
                ControllerContext = mockControllerContext.Object
            };
        }

        [TestMethod]
        public void _Read()
        {
            var controller = GetController();
            var result = controller._Read(new DataSourceRequest()) as JsonResult;
            Assert.IsNotNull(result, "Not of type JsonResult");
            var experiences = (result.Data as DataSourceResult).Data as IEnumerable<ExperienceViewModel>;
            Assert.IsNotNull(experiences);
            var displayNames = experiences.Select(e => e.Type.GetDisplayName()).ToArray();
            Assert.IsTrue(displayNames.SequenceEqual(displayNames.OrderBy(e => e)));
        }
    }
}
