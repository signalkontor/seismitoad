﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PromoterInstance.Controllers;
using PromoterInstance.Models;

namespace PromoterInstance.Tests.Controllers
{
    [TestClass]
    public class ProfileControllerTest
    {
        private static readonly UserData _userData = new UserData
        {
            EmployeeId = 0,
            Name = "Test"
        };

        private static ProfileController GetController()
        {
            var mockDbContext = new MockDbContext();
            var mockControllerContext = new MockControllerContext("some username");
            mockControllerContext.AddAuthCookie(_userData.ToString());
            return new ProfileController(mockDbContext.Object, new MockMembershipService(TestData.ProviderUserKey, TestData.Email))
            {
                ControllerContext = mockControllerContext.Object
            };
        }

        [TestMethod]
        public void Index()
        {
            var multiSelectValues = new[] {"1", "2"};

            var controller = GetController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result, "Not of type ViewResult");
            var employeeProfile = result.Model as EditEmployeeProfile;
            Assert.IsNotNull(employeeProfile, "Must be of type EditEmployeeProfile");
            #region Test der AutoMapper-Mappings
            Assert.IsTrue(employeeProfile.AccommodationPossible1.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.AccommodationPossible2);
            Assert.IsTrue(employeeProfile.Apprenticeships.Intersect(multiSelectValues).Count() == 2);
            Assert.IsTrue(employeeProfile.ArtisticAbilities.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(new DateTime(1980, 1, 1), employeeProfile.Birthday);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.City);
            Assert.AreEqual(1, employeeProfile.Country);
            Assert.AreEqual(null, employeeProfile.DriversLicense);
            Assert.AreEqual(null, employeeProfile.DriversLicenseClass1);
            Assert.AreEqual(null, employeeProfile.DriversLicenseClass2);
            Assert.AreEqual(null, employeeProfile.DriversLicenseSince);
            Assert.AreEqual(null, employeeProfile.DriversLicenseSinceDay);
            Assert.AreEqual(null, employeeProfile.DriversLicenseSinceMonth);
            Assert.AreEqual(null, employeeProfile.DriversLicenseSinceYear);
            Assert.AreEqual(1, employeeProfile.Education);
            Assert.AreEqual(TestData.Email, employeeProfile.Email1);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.Email2);
            Assert.AreEqual(null, employeeProfile.FacialPiercing);
            Assert.AreEqual(null, employeeProfile.FaxNo);
            Assert.AreEqual(52, employeeProfile.FemaleSize);
            Assert.AreEqual("Test", employeeProfile.Firstname);
            Assert.AreEqual("Nein", employeeProfile.FreelancerQuestionaire);
            Assert.IsTrue(employeeProfile.GastronomicAbilities.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(1, employeeProfile.HairColor);
            Assert.AreEqual(1, employeeProfile.HardwareKnownledge);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.HardwareKnownledgeDetails);
            Assert.AreEqual(null, employeeProfile.HasDigitalDriversCard);
            Assert.AreEqual(null, employeeProfile.HealthCertificate);
            Assert.AreEqual(180, employeeProfile.Height);
            Assert.AreEqual(null, employeeProfile.IsBahncardAvailable);
            Assert.AreEqual(null, employeeProfile.IsCarAvailable);
            Assert.AreEqual(null, employeeProfile.JeansLength);
            Assert.AreEqual(null, employeeProfile.JeansWidth);
            Assert.AreEqual(1, employeeProfile.LanguagesEnglish);
            Assert.AreEqual(2, employeeProfile.LanguagesFrench);
            Assert.AreEqual(3, employeeProfile.LanguagesGerman);
            Assert.AreEqual(4, employeeProfile.LanguagesItalian);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.LanguagesOther);
            Assert.AreEqual(3, employeeProfile.LanguagesSpanish);
            Assert.AreEqual(2, employeeProfile.LanguagesTurkish);
            Assert.AreEqual("User", employeeProfile.Lastname);
            Assert.AreEqual(52, employeeProfile.MaleSize);
            Assert.IsTrue(employeeProfile.OtherAbilities.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.OtherAbilitiesText);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.OtherCompletedApprenticeships);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.PersonalityTestAnswers);
            Assert.AreEqual(TestData.TestPhoneNumber, employeeProfile.PhoneMobileNo.ToString());
            Assert.AreEqual(TestData.TestPhoneNumber, employeeProfile.PhoneNo.ToString());
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.PostalCode);
            Assert.IsTrue(employeeProfile.PreferredTasks.Intersect(multiSelectValues).Count() == 2);
            Assert.IsTrue(employeeProfile.PreferredTypes.Intersect(multiSelectValues).Count() == 2);
            Assert.IsTrue(employeeProfile.PreferredWorkSchedule.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(1, employeeProfile.ShirtSize);
            Assert.AreEqual("Ja", employeeProfile.SkeletonContract);
            Assert.AreEqual(1, employeeProfile.SoftwareKnownledge);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.SoftwareKnownledgeDetails);
            Assert.IsTrue(employeeProfile.Sports.Intersect(multiSelectValues).Count() == 2);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.Street);
            Assert.AreEqual(true, employeeProfile.Studies);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.StudiesDetails);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.TaxNumber);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.TaxOfficeLocation);
            Assert.AreEqual("Herr", employeeProfile.Title2);
            Assert.AreEqual(true, employeeProfile.TravelWillingness);
            Assert.AreEqual(false, employeeProfile.TurnoverTaxDeductible);
            Assert.AreEqual(true, employeeProfile.ValidTradeLicense);
            Assert.AreEqual(false, employeeProfile.VisibleTattoos);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.Website);
            Assert.AreEqual(TestData.TestStringValue, employeeProfile.WorkExperience);
            #endregion
        }

        [TestMethod]
        public void InvalidRequiredFields()
        {
            var model = new EditEmployeeProfile();
            SetBasicButIncompleteData(model);

            var mockDbContext = new MockDbContext();
            var controllerContext = new MockControllerContext("newuser");
            var userData = new UserData
            {
                EmployeeId = 1,
                Name = "Newuser"
            };
            controllerContext.AddAuthCookie(userData.ToString());
            var controller = new ProfileController(mockDbContext.Object, new MockMembershipService(TestData.NewUserProviderUserKey, "newuser@example.com"));
            controller.ControllerContext = controllerContext.Object;
            controller.ModelState.AddModelError("Street", "Street is required");
            PersonalityTest(controller.Request.Params, null);

            var result = controller.Index(model) as ViewResult;
            Assert.IsNotNull(result, "Not of type ViewResult");
            var employeeProfile = result.Model as EditEmployeeProfile;
            Assert.IsNotNull(employeeProfile, "Must be of type EditEmployeeProfile");
            Assert.IsFalse(mockDbContext.ChangesSaved);
        }

        public void RequiredFieldsValidOtherFieldsInvalid()
        {
            
        }

        public void FielsValid()
        {
            
        }

        [TestMethod]
        public void NewProfile()
        {
            var model = new EditEmployeeProfile();
            SetBasicButIncompleteData(model);
            AddUntilMinimalRequiredDataIsSet(model);

            var mockDbContext = new MockDbContext();
            var controllerContext = new MockControllerContext("newuser");
            var userData = new UserData
            {
                EmployeeId = 1,
                Name = "Newuser"
            };
            controllerContext.AddAuthCookie(userData.ToString());
            var controller = new ProfileController(mockDbContext.Object, new MockMembershipService(TestData.NewUserProviderUserKey, "newuser@example.com"));
            controller.ControllerContext = controllerContext.Object;
            PersonalityTest(controller.Request.Params, null);

            var result = controller.Index(model) as ViewResult;
            Assert.IsNotNull(result, "Not of type ViewResult");
            var employeeProfile = result.Model as EditEmployeeProfile;
            Assert.IsNotNull(employeeProfile, "Must be of type EditEmployeeProfile");
            Assert.AreEqual(model.Email1, MockMembershipService.LastUpdatedUser.Email);
            Assert.IsNotNull(mockDbContext.Object.Employees.Single(e => e.Id == 1).Profile);
            Assert.IsTrue(mockDbContext.ChangesSaved);
        }

        private static void SetBasicButIncompleteData(EditEmployeeProfile profile)
        {
            profile.Title2 = "Herr";
            profile.Firstname = "New";
            profile.Lastname = "User";
            profile.BirthdayDay = 1;
            profile.BirthdayMonth = 1;
            profile.BirthdayYear = 1980;
            profile.PhoneMobileNo = new PhoneNumber();
        }

        private static void AddUntilMinimalRequiredDataIsSet(EditEmployeeProfile profile)
        {
            profile.Email1 = "newuser@example.com";
            profile.PhoneMobileNo = new PhoneNumber
            {
                CountryCode = "+49",
                AreaCode = "040",
                Number = "123456"
            };
            profile.Street = "Street";
            profile.PostalCode = "D-12345";
            profile.City = "City";
            profile.Country = 1;
            profile.Height = 180;
            profile.ShirtSize = 1;
            profile.ValidTradeLicense = true;
            profile.DriversLicense = false;
            profile.LanguagesGerman = 1;
            profile.SoftwareKnownledge = 1;
            profile.HardwareKnownledge = 1;
        }

        private static void PersonalityTest(NameValueCollection requestParams, string value)
        {
            for(var i = 0; i < 52; i++)
                requestParams.Add("PersonalityTestAnswers.Radio0", value);
        }
    }
}
