﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PromoterInstance.Controllers;
using PromoterInstance.Models;

namespace PromoterInstance.Tests.Controllers
{
    [TestClass]
    public class AssignmentControllerTest
    {
        private static readonly UserData UserData = new UserData
        {
            EmployeeId = 0,
            Name = "Test"
        };

        private static AssignmentController GetController()
        {
            var mockContext = new MockDbContext();
            var mockControllerContext = new MockControllerContext("some username");
            mockControllerContext.AddAuthCookie(UserData.ToString());
            return new AssignmentController(mockContext.Object)
            {
                ControllerContext = mockControllerContext.Object
            };
        }

        // Dies kann nicht gut getestet werden, da _Planned() direkt SQL-Queries an die DB schickt und wir diese nicht mit Moq mocken können.
        //[TestMethod]
        //public void _Planned()
        //{
        //    var controller = GetController();
        //    var result = controller._Planned(new DataSourceRequest(), null) as JsonResult;
        //    Assert.IsNotNull(result, "Not of type JsonResult");
        //    Assert.AreEqual(JsonRequestBehavior.AllowGet, result.JsonRequestBehavior);
        //    var data = result.Data as DataSourceResult;
        //    Assert.IsNotNull(data, "Not of type DataSourceResult");
        //    var plannedAssignments = data.Data as IEnumerable<PlannedAssignment>;
        //    Assert.IsNotNull(plannedAssignments, "Not of type IEnumerable<PlannedAssignment>");
        //    Assert.IsTrue(plannedAssignments.All(e => e.DateStart > DateTime.Now), "DateStart of planned assignments must be in future.");
        //    Assert.IsFalse(plannedAssignments.Any(e => TestData.PostponedCancelledDeletedAssignments.Contains(e.Id)), "Cancelled, deleted or postponed assignments should not be listed");

        //    // TODO Testen, dass nur Aktionstage für den eingeloggten User angezeigt werden
        //    // Dazu müssen zuerst Testdaten für andere User angelegt werden
        //}

        [TestMethod]
        public void _Archive()
        {
            var controller = GetController();
            var result = controller._Archive(new DataSourceRequest()) as JsonResult;
            Assert.IsNotNull(result, "Not of type JsonResult");
            Assert.AreEqual(JsonRequestBehavior.AllowGet, result.JsonRequestBehavior);
            var data = result.Data as DataSourceResult;
            Assert.IsNotNull(data, "Not of type DataSourceResult");
            var archivedAssignments = data.Data as IEnumerable<ArchivedAssignment>;
            Assert.IsNotNull(archivedAssignments, "Not of type IEnumerable<ArchivedAssignment>");
            Assert.IsTrue(archivedAssignments.All(e => e.DateStart < DateTime.Now), "DateStart of archived Assignments must be in past.");
            Assert.IsTrue(archivedAssignments.All(e => e.DateStart > DateTime.Now.AddMonths(-3)), "Archived Assignments must not be older than three month.");
            // Anhand der AssignmentId kann man erkennen welche Assignments InTime oder Late waren (siehe TestData())
            Assert.IsTrue(archivedAssignments.Select(e => e.Id % 4).All(e => e == 2 || e == 3), "Only InTime or Late assignments are allowed.");
            Assert.AreEqual(2, archivedAssignments.Count(), "Testdata contains 2 assignments that should be shown in archive.");
            Assert.IsFalse(archivedAssignments.Any(e => TestData.PostponedCancelledDeletedAssignments.Contains(e.Id)), "Cancelled, deleted or postponed assignments should not be listed");
            Assert.IsTrue(archivedAssignments.First().DateStart > archivedAssignments.Last().DateStart, "Order should be newest to oldest Assignment");

            // TODO Testen, dass nur Aktionstage für den eingeloggten User angezeigt werden
            // Dazu müssen zuerst Testdaten für andere User angelegt werden
        }

        [TestMethod]
        public void _Details()
        {
            var controller = GetController();
            var result = controller._Details(0) as PartialViewResult;
            Assert.IsNotNull(result, "Not of type PartialViewResult");
            var assignmentDetails = result.Model as AssignmentDetails;
            Assert.IsNotNull(assignmentDetails, "Must be of type AssignmentDetails");
            
            Assert.AreEqual("City 0", assignmentDetails.City);
            Assert.AreEqual("Keiner", assignmentDetails.Contact);
            Assert.AreEqual(0, assignmentDetails.Id);
            Assert.AreEqual(0, assignmentDetails.LocationId);
            Assert.AreEqual("Location 0", assignmentDetails.Name);
            Assert.AreEqual(null, assignmentDetails.Name2);
            Assert.AreEqual(null, assignmentDetails.Notes);
            //Assert.AreEqual(null, assignmentDetails.Phone);  --> Nicht mehr vorhanden
            Assert.AreEqual("D-12340", assignmentDetails.PostalCode);
            Assert.AreEqual("Street 0", assignmentDetails.Street);
            Assert.AreEqual("Test User (Fachberater/in*, Moderator/in)", assignmentDetails.Team);
        }

        [TestMethod]
        public void Calendar()
        {
            var controller = GetController();
            var result = controller.Calendar(0) as ContentResult;
            Assert.IsNotNull(result, "Not of type ContentResult");
            Assert.AreEqual("text/calendar", result.ContentType);
            Assert.IsTrue(result.Content.StartsWith("BEGIN:VCALENDAR"));
            Assert.IsTrue(result.Content.Trim().EndsWith("END:VCALENDAR"));
            Assert.IsTrue(result.Content.Contains("Location 0"));
            Assert.IsTrue(result.Content.Contains("Street 0"));
            Assert.IsTrue(result.Content.Contains("City 0"));
        }
    }
}
