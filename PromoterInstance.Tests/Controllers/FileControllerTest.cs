﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PromoterInstance.Controllers;
using PromoterInstance.Models;

namespace PromoterInstance.Tests.Controllers
{
    [TestClass]
    public class FileControllerTest
    {
        private static FileController GetController()
        {
            var mockContext = new MockDbContext();
            return new FileController()
            {
                ControllerContext = new MockControllerContext("some username").Object
            };
        }

        [TestMethod]
        public void Save()
        {
            var controller = GetController();
        }

        [TestMethod]
        public void GetUrls()
        {
            var existingFile = Path.GetTempFileName();
            File.Move(existingFile, existingFile + ".jpg");
            try
            {
                var basePath = Path.GetDirectoryName(existingFile);
                var notExistingFile = existingFile + ".doesnotexist";

                var server = new Mock<HttpServerUtilityBase>();
                server.Setup(m => m.MapPath(It.IsAny<string>())).Returns<string>(path => Path.Combine(basePath, Path.GetFileName(path)));

                string displayUrl, downloadUrl;
                var fileName = Path.GetFileName(existingFile);
                FileController.GetUrls(0, fileName, server.Object, out displayUrl, out downloadUrl);

                Assert.AreEqual("/Uploads/Employee/0/" + fileName + ".jpg", displayUrl);
                Assert.AreEqual("/Uploads/Employee/0/" + fileName + ".jpg", downloadUrl);

                FileController.GetUrls(0, Path.GetFileName(notExistingFile), server.Object, out displayUrl, out downloadUrl);
                Assert.AreEqual("/Images/404.jpg", displayUrl);
                Assert.AreEqual(null, downloadUrl);
            }
            finally
            {
                File.Delete(existingFile + ".jpg");
            }
        }
    }
}
