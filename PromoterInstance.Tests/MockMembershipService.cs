using System;
using System.Linq;
using System.Web.Security;
using PromoterInstance.Models;

namespace PromoterInstance.Tests
{
    internal class MockMembershipService : IMembershipService
    {
        private readonly string _email;
        private readonly Guid _providerUserKey;

        public static MembershipUser LastUpdatedUser { get; private set; }

        public MockMembershipService(Guid providerUserKey, string email)
        {
            _providerUserKey = providerUserKey;
            _email = email;
        }

        public MembershipUser GetUser(string username)
        {
            var date = DateTime.Now;
            return new MembershipUser("AspNetSqlMembershipProvider", username, _providerUserKey, _email, null, null, true, false, date, date, date, date, date);
            
        }

        public MembershipUser GetUser(Guid providerUserKey)
        {
            var date = DateTime.Now;
            return new MembershipUser("AspNetSqlMembershipProvider", "username", providerUserKey, _email, null, null, true, false, date, date, date, date, date);
        }

        public void UpdateUser(MembershipUser membershipUser)
        {
            LastUpdatedUser = membershipUser;
        }
    }
}