﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Moq;

namespace PromoterInstance.Tests
{
    internal class MockControllerContext
    {
        private readonly string _username;

        public MockControllerContext(string username)
        {
            _username = username;

            var response = new Mock<HttpResponseBase>();
            response.SetupGet(m => m.Headers).Returns(new WebHeaderCollection());

            var request = new Mock<HttpRequestBase>();
            RequestParams = new NameValueCollection();
            RequestCookies = new HttpCookieCollection();
            request.SetupGet(m => m.Params).Returns(RequestParams);
            request.SetupGet(m => m.Cookies).Returns(RequestCookies);

            var context = new Mock<ControllerContext>();
            context.SetupGet(m => m.HttpContext.User.Identity.Name).Returns(username);
            context.SetupGet(m => m.HttpContext.User.Identity.IsAuthenticated).Returns(true);
            context.SetupGet(m => m.HttpContext.Response).Returns(response.Object);
            context.SetupGet(m => m.HttpContext.Request).Returns(request.Object);
            Object = context.Object;            
        }

        public ControllerContext Object { get; private set; }
        public NameValueCollection RequestParams { get; private set; }
        public HttpCookieCollection RequestCookies { get; private set; }

        public void AddAuthCookie(string userData)
        {
            var cookie = FormsAuthentication.GetAuthCookie(_username, false);
            var ticket = FormsAuthentication.Decrypt(cookie.Value);
            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration,
                ticket.IsPersistent, userData, ticket.CookiePath);
            var encTicket = FormsAuthentication.Encrypt(newTicket);

            // Use existing cookie. Could create new one but would have to copy settings over...
            cookie.Value = encTicket;
            RequestCookies.Add(cookie);
        }
    }
   
}
