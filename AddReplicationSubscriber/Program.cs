﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AddReplicationSubscriber.Models;
using Dapper;

namespace AddReplicationSubscriber
{
    class Program
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["SeismitoadDbContext"].ConnectionString;

        static void Main(string[] args)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                const string query = @"SELECT
	Campaigns.Id,
	Customers.Name AS Customer,
	Campaigns.Name,
	InstanceUrl
FROM Campaigns INNER JOIN
Customers ON Campaigns.Customer_Id = Customers.Id
WHERE
	Campaigns.InstanceUrl LIKE 'http%' AND
	UsesSqlReplication = 1 AND
	NOT EXISTS (SELECT * FROM [dbo].[MSmerge_partition_groups] WHERE HOST_NAME_FN = Campaigns.Id)
";
                var campaigns = connection.Query<Campaign>(query).ToArray();
                Console.WriteLine("Bitte Aktion (Campagin) wählen für die die Replikation eingerichtet werden soll.");
                
                for (int i = 0; i < campaigns.Length; i++)
                {
                    var campaign = campaigns[i];
                    Console.WriteLine("[{0}] {1} - {2} ({3})", i, campaign.Customer, campaign.Name, campaign.InstanceUrl);
                }
                Console.WriteLine("[q] zum beenden");
                Console.Write("Nummer + [Enter]: ");
                var answer = Console.ReadLine();
                if (answer.Contains("q"))
                    return;

                int index;
                if (!int.TryParse(answer, out index) || index >= campaigns.Length)
                {
                    Console.WriteLine("Ungültige Eingabe");
                    return;
                }

                var selected = campaigns[index];
                selected.InstanceUrl = "http://ttv.permedia-report.de";
                Console.Write("Bitte den Computernamen des Abonnenten eingeben (findet man z.B. unter Systemsteuerung -> System): ");
                var computerName = Console.ReadLine();
                Console.Write("Bitte den Instanznamen des Datenbankservers des Abonnenten eingeben: ");
                var instanceName = Console.ReadLine();
                Console.Write("Bitte das Passwort des 'sa' Users auf dem Abonnenten eingeben: ");
                var saPassword = Console.ReadLine();
                Console.Write("Bitte den Datenbanknamen eingeben: ");
                var dbName = Console.ReadLine();

                var hostEntry = Dns.GetHostEntry(new Uri(selected.InstanceUrl).Host);

                Console.WriteLine("Folgende Einstellungen werden verwendet, bitte genau prüfen!");
                Console.WriteLine();
                Console.WriteLine("Aktion                           : {0} {1}", selected.Customer, selected.Name);
                Console.WriteLine("DNS Name des Abonnenten          : {0}", selected.InstanceUrl);
                Console.WriteLine("IP Adresse des Abonnenten        : {0}", hostEntry.AddressList.First());
                Console.WriteLine("Computername des Abonnenten      : {0}", computerName);
                Console.WriteLine("SQL Server Instanzname (Abonnent): {0}", instanceName);
                Console.WriteLine("sa-Passwort des Abonnenten       : {0}", saPassword);
                Console.WriteLine("Datenbankname auf dem Abonnenten : {0}", dbName);
                Console.WriteLine();
                Console.Write("Mit diesen Werten [f]ortsetzen oder [a]bbrechen?");
                switch(Console.ReadKey().KeyChar)
                {
                    case 'f':
                        break;
                    default:
                        return;
                }

                var serverLogin = ConfigurationManager.AppSettings["ServerLogin"];
                File.Copy(@"C:\Windows\System32\drivers\etc\hosts", @"C:\Windows\System32\drivers\etc\hosts.bak");
                File.AppendAllText(@"C:\Windows\System32\drivers\etc\hosts", string.Format("\n{0} {1}\n", hostEntry.AddressList.First(), computerName));

                using (var c = new SqlConnection(string.Format(@"Data Source={0}\{3};Initial Catalog={1};Persist Security Info=True;MultipleActiveResultSets=True;User ID=sa;Password={2}", computerName, dbName, saPassword, instanceName)))
                {
                    try
                    {
                        c.Open();
                    }
                    catch
                    {
                        Console.WriteLine();
                        Console.WriteLine("Kann keine Verbindung mit o.g. Zugangsdaten herstellen. Stimmen die Zugangdaten?");
                        Console.WriteLine("Auch die Firewall auf dem Abonnenten muss Verbindungen von {0} zulassen (für den SQL Port und den SQL Browser Port)", serverLogin.Substring(0, serverLogin.IndexOf('\\')));
                        File.Copy(@"C:\Windows\System32\drivers\etc\hosts.bak", @"C:\Windows\System32\drivers\etc\hosts", true);
                        File.Delete(@"C:\Windows\System32\drivers\etc\hosts.bak");
                        return;
                    }
                }
                File.Delete(@"C:\Windows\System32\drivers\etc\hosts.bak");

                const string command = @"
exec sp_addmergesubscription @publication = N'masterinstance_pub', @subscriber = @SubscriberHostName, @subscriber_db = @SubscriberDatabaseName, @subscription_type = N'Push', @sync_type = N'Automatic', @subscriber_type = N'Local', @subscription_priority = 0, @description = N'', @use_interactive_resolver = N'False', @hostname = @CampaignId
exec sp_addmergepushsubscription_agent @publication = N'masterinstance_pub', @subscriber = @SubscriberHostName, @subscriber_db = @SubscriberDatabaseName, @job_login = @JobLogin, @job_password = @JobPassword, @subscriber_security_mode = 0, @subscriber_login = @SubscriberLogin, @subscriber_password = @SubscriberPassword, @publisher_security_mode = 1, @frequency_type = 4, @frequency_interval = 1, @frequency_relative_interval = 1, @frequency_recurrence_factor = 0, @frequency_subday = 4, @frequency_subday_interval = 5, @active_start_time_of_day = 100, @active_end_time_of_day = 235959, @active_start_date = 0, @active_end_date = 0
exec sp_addmergepartition @publication = N'masterinstance_pub', @suser_sname = N'', @host_name = @CampaignId
";

                connection.Execute(command, new
                {
                    CampaignId = selected.Id.ToString(CultureInfo.InvariantCulture),
                    SubscriberHostName = computerName,
                    SubscriberDatabaseName = dbName,
                    JobLogin = serverLogin,
                    JobPassword = ConfigurationManager.AppSettings["ServerPassword"],
                    SubscriberLogin = "sa",
                    SubscriberPassword = saPassword,
                });

            }
        }
    }
}
