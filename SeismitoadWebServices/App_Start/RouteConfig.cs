﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SeismitoadWebServices
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "UrlBuilder",
                url: "UrlBuilder/{id}",
                defaults: new { controller = "UrlBuilder", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Attendance",
                url: "{apiKey}/{securityToken}/{ts}/Attendance/{assignmentId}/{employeeId}",
                defaults: new { controller = "Attendance", action = "Index" }
            );

            routes.MapRoute(
                name: "Assignments",
                url: "{apiKey}/{securityToken}/{ts}/Assignments/{employeeId}",
                defaults: new { controller = "Assignments", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{apiKey}/{securityToken}/{ts}/{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}