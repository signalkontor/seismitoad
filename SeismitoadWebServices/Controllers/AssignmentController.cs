﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadWebServices.Models;

namespace SeismitoadWebServices.Controllers
{
    public class AssignmentController : ApiKeyController
    {
        // Beispiel: http://localhost:53177/xxx/xxx/xxx/Assignment/Create?campaignId=1&locationId=2&employeeId=46&assignmentRoleIds=1&assignmentRoleIds=8&dateStart=10.02.2014%2010%3A00&dateEnd=10.02.2014%2014%3A30
        // Die Werte für xxx müssen anhand des UrlBuilders bestimmt werden.
        public ActionResult Create(AssignmentCreateModel model)
        {
            if(!ModelState.IsValid)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int assignmentId;

            using (var context = new SeismitoadDbContext())
            {
                var campaignLocation = context.CampaignsLocations.SingleOrDefault(e => e.CampaignId == model.CampaignId && e.LocationId == model.LocationId);
                var employee = context.ActiveEmployees.SingleOrDefault(e => e.Id == model.EmployeeId);
                var assignmentRoleIds = model.AssignmentRoleIds ?? new int[0];
                var assignmentRoles = context.AssignmentRoles.Where(e => assignmentRoleIds.Contains(e.Id));
                if (campaignLocation == null || employee == null || assignmentRoles.Count() != assignmentRoleIds.Length)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var assignment = new Assignment
                    {
                        DateStart = model.DateStart,
                        DateEnd = model.DateEnd,
                        CampaignLocation = campaignLocation,
                        EmployeeCount = 1,
                    };

                var assignedEmployee = new AssignedEmployee
                    {
                        Employee = employee,
                        Assignment = assignment,
                        AssignmentRoles = assignmentRoles.ToList()
                    };

                context.AssignedEmployees.Add(assignedEmployee);
                context.SaveChanges();
                assignmentId = assignment.Id;
            }
            return Json(assignmentId, JsonRequestBehavior.AllowGet);
        }
    }
}
