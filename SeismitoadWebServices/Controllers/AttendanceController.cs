﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Models;

namespace SeismitoadWebServices.Controllers
{
    public class AttendanceController : ApiKeyController
    {
        private static readonly Attendance[] ReportingAllowed = new[] {Attendance.InTime, Attendance.Late};

        [HttpGet]
        public ActionResult Index(int assignmentId, int employeeId)
        {
            using (var context = new SeismitoadDbContext())
            {
                var mayEnterReport = context.AssignedEmployees.Any(
                    e =>
                    e.AssignmentId == assignmentId &&
                    e.EmployeeId == employeeId &&
                    ReportingAllowed.Contains(e.Attendance));
                return Content(mayEnterReport.ToString());
            }
        }
    }
}
