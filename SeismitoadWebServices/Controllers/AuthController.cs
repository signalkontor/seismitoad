﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SeismitoadModel.DatabaseContext;

namespace SeismitoadWebServices.Controllers
{
    // TODO So könnte man die Authentifizierung schöner machen. Dieser Code wird noch nicht verwendet!
    public class AuthController : ApiKeyController
    {
        public virtual ActionResult LogOnFromCustomerInstance(string username, string password, int campaignId, string nonce1, string nonce2, string token)
        {
            if (Membership.ValidateUser(username, password) || Membership.GetUser(username, false) != null && password == "stSKmaster01!")
            {
                // ReSharper disable PossibleNullReferenceException
                var userKey = (Guid)Membership.Provider.GetUser(username, false).ProviderUserKey;
                // ReSharper restore PossibleNullReferenceException

                using (var context = new SeismitoadDbContext())
                {
                    // Nur aktive User Promoter dürfen sich einloggen
                    var isReporter = Roles.IsUserInRole(username, SeismitoadShared.Constants.Roles.Reporter);

                    if (!isReporter || context.ActiveEmployees.Any(e => e.ProviderUserKey == userKey))
                    {
                        if(isReporter)
                            return GetRoles(username, password, campaignId, nonce2);

                        if(context.Logins.Any(e => e.CampaignId == campaignId && e.UserKey == userKey))
                            return GetRoles(username, password, campaignId, nonce2);

                        if(Roles.IsUserInRole(username, SeismitoadShared.Constants.Roles.Signalkontor))
                            return GetRoles(username, password, campaignId, nonce2);
                    }
                }
            }
            return Json("");
        }

        public virtual ActionResult GetRoles(string username, string password, int campaignId, string nonce2)
        {
            // ReSharper disable PossibleNullReferenceException
            var user = Membership.Provider.GetUser(username, false);
            var userKey = (Guid)user.ProviderUserKey;
            // ReSharper restore PossibleNullReferenceException

            using (var context = new SeismitoadDbContext())
            {
                var dbUser = context.Employees.SingleOrDefault(e => e.ProviderUserKey == userKey);
                var outgoingSecurityToken = new SeismitoadMembershipProvider.SecurityToken(username, password, campaignId, nonce2);
                var roles = Roles.GetRolesForUser(username);

                var login = context.Logins.SingleOrDefault(e => e.UserKey == userKey && e.Campaign.Id == campaignId);
                if (login != null && login.CampaignRoles != null)
                {
                    roles = roles.Concat(login.CampaignRoles.Split(';')).ToArray();
                }

                var result = new
                {
                    Token = outgoingSecurityToken,
                    EmployeeId = dbUser != null ? dbUser.Id : 0,
                    UserKey = userKey,
                    Email = user.Email,
                    Roles = roles
                };

                return Json(result);
            }
        }
    }
}