﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SeismitoadMembershipProvider;
using SeismitoadShared;

namespace SeismitoadWebServices.Controllers
{
    public abstract class ApiKeyController : Controller
    {
        // Dictionary<API-Schlüssel, Name des "Benutzers">
        private static readonly Dictionary<string, string> ApiKeys;

        static ApiKeyController()
        {
            var apiKeys = ConfigurationManager.AppSettings.AllKeys.Where(e => e.StartsWith("ApiKey["));
            ApiKeys = apiKeys.Select(appSettingKey => ConfigurationManager.AppSettings[appSettingKey].Split('='))
                             .ToDictionary(e => e[1].Trim(), e => e[0].Trim());
        }

        private static readonly string[] IrrelevantRouteValueKeys = {"apiKey", "securityToken", "controller", "action"};

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var routeValues = filterContext.RequestContext.RouteData.Values;

            var apiKey = routeValues["apiKey"] as string;
            if (string.IsNullOrWhiteSpace(apiKey) || !ApiKeys.ContainsKey(apiKey))
            {
                filterContext.Result = new HttpUnauthorizedResult("Unknown API-Key");
                return;
            }

            var timestamp = new DateTime(long.Parse(routeValues["ts"] as string));
            if (Math.Abs(timestamp.Subtract(LocalDateTime.Now).TotalMinutes) > 3)
            {
                filterContext.Result = new HttpUnauthorizedResult("Timestamp two old");
                return;
            }
            
            var securityTokenParam = routeValues["securityToken"] as string;
            var relevantRouteValues = RelevantRouteValues(routeValues);

            var securityToken = SecurityToken.SecurityToken.Generate(apiKey, relevantRouteValues);
            if (securityToken != securityTokenParam)
            {
                filterContext.Result = new HttpUnauthorizedResult("Security token mismatch");
                return;
            }

            base.OnActionExecuting(filterContext);
        }

        public static string[] RelevantRouteValues(RouteValueDictionary routeValues)
        {
            var relevantRouteValues = routeValues
                .Where(e => !IrrelevantRouteValueKeys.Contains(e.Key))
                .OrderBy(e => e.Key)
                .Select(e => e.Value.ToString())
                .ToArray();
            return relevantRouteValues;
        }
    }
}
