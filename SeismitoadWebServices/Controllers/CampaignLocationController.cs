﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SeismitoadWebServices.Controllers
{
    public class CampaignLocationController : ApiKeyController
    {
        //
        // GET: /CampaignLocations/

        public ActionResult Index(int id)
        {
            using (var context = new SeismitoadModel.DatabaseContext.SeismitoadDbContext())
            {
                return Json(context.CampaignsLocations
                                   .Where(e => e.CampaignId == id)
                                   .Select(e => new
                                   {
                                       e.Location.Id,
                                       e.Location.Name,
                                       e.Location.Street,
                                       e.Location.PostalCode,
                                       e.Location.City,
                                       LocationGroup =
                                                e.Location.LocationGroup != null
                                                    ? e.Location.LocationGroup.Name
                                                    : null,
                                       e.SalesRegion,
                                   })
                                   .ToList(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}
