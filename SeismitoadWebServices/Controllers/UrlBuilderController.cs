﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeismitoadWebServices.Models;

namespace SeismitoadWebServices.Controllers
{
    public class UrlBuilderController : Controller
    {
        //
        // GET: /UrlBuilder/

        public ActionResult Index()
        {
            return Content(new UrlBuilderModel().GetUrl(RouteData.Values));
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.IsLocal)
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            filterContext.Result = new HttpUnauthorizedResult();
        }

    }
}
