﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SeismitoadModel.DatabaseContext;

namespace SeismitoadWebServices
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Damit nicht bei jeder kleinen Schema-Änderung der Service "kaputt" geht, schalten wir die Schemaprüfung aus.
            Database.SetInitializer<SeismitoadDbContext>(null);

            ChangeLogger.UserKey = () => null; // disable change logging
        }
    }
}