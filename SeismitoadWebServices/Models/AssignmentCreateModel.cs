﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeismitoadWebServices.Models
{
    public class AssignmentCreateModel
    {
        public int CampaignId { get; set; }
        public int LocationId { get; set; }
        public int EmployeeId { get; set; }
        public int[] AssignmentRoleIds { get; set; }
        [CustomValidation(typeof(AssignmentCreateModel), "ValidateStartBeforeEnd")]
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public static ValidationResult ValidateStartBeforeEnd(object value, ValidationContext context)
        {
            var _this = context.ObjectInstance as AssignmentCreateModel;
            if (_this == null || _this.DateStart < _this.DateEnd)
                return ValidationResult.Success;
            return new ValidationResult("DateStart must be before DateEnd");
        }
    }
}