﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SeismitoadShared;
using SeismitoadWebServices.Controllers;

namespace SeismitoadWebServices.Models
{
    public class UrlBuilderModel
    {
        private static readonly Dictionary<string, string> ApiKeys;

        static UrlBuilderModel()
        {
            var apiKeys = ConfigurationManager.AppSettings.AllKeys.Where(e => e.StartsWith("ApiKey["));
            ApiKeys = apiKeys.Select(appSettingKey => ConfigurationManager.AppSettings[appSettingKey].Split('='))
                             .ToDictionary(e => e[1].Trim(), e => e[0].Trim());
        }
        private readonly Dictionary<string, string[]> _controllers;

        public UrlBuilderModel()
        {
            var baseType = typeof(ApiKeyController);
            var assembly = baseType.Assembly;
            var types = assembly.GetTypes().Where(t => t.IsSubclassOf(baseType));
            _controllers = new Dictionary<string, string[]>();
            foreach (var type in types)
            {
                var fullname = type.Name;
                var controllerName = fullname.Remove(fullname.IndexOf("Controller"));

                var actions = type
                    .GetMethods(BindingFlags.Instance | BindingFlags.Public)
                    .Where(m => m.ReturnType == typeof(ActionResult) || m.ReturnType.IsSubclassOf(typeof(ActionResult)))
                    .Select(m => m.Name)
                    .ToArray();

                _controllers.Add(controllerName, actions);
            }
        }

        public string GetUrl(RouteValueDictionary routeValueDictionary)
        {
            var apiKey = ApiKeys.First().Key;

            var timestamp = LocalDateTime.Now.Ticks.ToString();

            var relevantRouteValues = ApiKeyController.RelevantRouteValues(routeValueDictionary);
            routeValueDictionary.Add("ts", timestamp);
            var relevantRouteValuesForToken = ApiKeyController.RelevantRouteValues(routeValueDictionary);
            var securityToken = SecurityToken.SecurityToken.Generate(apiKey, relevantRouteValuesForToken);

            var stringBuilder = new StringBuilder();
            foreach (var controller in _controllers)
            {
                foreach (var action in controller.Value)
                {
                    stringBuilder.AppendFormat("/{0}/{1}/{2}/{3}/{4}/{5}\n",
                                               apiKey, securityToken, timestamp, controller.Key, action,
                                               string.Join("/", relevantRouteValues));
                }
            }
            return stringBuilder.ToString();
        }
    }
}