﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadServiceModels
{
    [Serializable]
    public class CampaignLocationUpdate
    {
        public int LocationId;
        public string SalesRegion;
    }
}
