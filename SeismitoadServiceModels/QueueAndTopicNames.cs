﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadServiceModels
{
    public static class QueueAndTopicNames
    {
        public const string LocationUpdateTopic = "locationupdates";
        public const string CampaignQueueFormat = "campaign{0:0000}";
    }
}
