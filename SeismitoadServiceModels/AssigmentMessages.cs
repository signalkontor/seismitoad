﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadServiceModels
{
    [Serializable]
    public class AssignmentCreate
    {
        public int Id;
        public DateTime DateStart;
        public DateTime DateEnd;
        public int LocationId;
        public string Name;
        public string PostalCode;
        public string City;
        public string Street;
        public string LocationGroup;
        public string SalesRegion;
    }

    [Serializable]
    public class AssignmentDelete
    {
        public int Id;
        public int? EmployeeId;
        public string Reason;
        public string Username;
    }
    
    [Serializable]
    public class AssignmentUnDelete
    {
        public int Id;
        public int EmployeeId;
    }

    [Serializable]
    public class AssignmentMove
    {
        public int Id;
        public DateTime NewDateStart;
        public DateTime NewDateEnd;
    }

    [Serializable]
    public class AssignmentAddEmployee
    {
        public int Id;
        public int EmployeeId;
        public string Firstname;
        public string Lastname;
        public bool OnlySalesReport;
    }

    [Serializable]
    public class AssignmentRemoveEmployee
    {
        public int Id;
        public int EmployeeId;
    }

    [Serializable]
    public class AssignmentSetAttendance
    {
        public int Id;
        public int EmployeeId;
        public int Attendance;
    }
}
