﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadServiceModels
{
    [Serializable]
    public class LocationUpdate
    {
        public int Id;
        public string Name;
        public string PostalCode;
        public string City;
        public string Street;
        public string LocationGroup;
    }
}
