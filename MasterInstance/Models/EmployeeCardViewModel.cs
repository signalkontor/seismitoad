﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Models
{
    public class EmployeeCardViewModel
    {
        public int EmployeeId { get; set; }

        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        
        [Display(Name = "Alter")]
        public int Age { get; set; }

        [Display(Name = "Straße")]
        public string Street { get; set; }

        [Display(Name = "PLZ")]
        public string PostalCode { get; set; }

        [Display(Name = "Ort")]
        public string City { get; set; }

        [Display(Name = "Land")]
        public string Country { get; set; }

        [Display(Name = "Mobil")]
        public string PhoneMobileNo { get; set; }

        [Display(Name = "Festnetz")]
        public string PhoneNo { get; set; }

        [Display(Name = "Fax")]
        public string FaxNo { get; set; }

        [Display(Name = "Email 1")]
        public string Email1 { get; set; }

        [Display(Name = "Email 2")]
        public string Email2 { get; set; }

        public string RelativeImageUrl { get; set; }
        public bool? AddressChanged { get; set; }
        public string AddressChangeAckUrl { get; set; }
    }

    public class EmployeeCardViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Employee, EmployeeCardViewModel>()
                .ForMember(e => e.EmployeeId, opt => opt.MapFrom(e => e.Id))
                .ForMember(e => e.Email1, opt => opt.MapFrom(e => e.Email));
            Mapper.CreateMap<EmployeeProfile, EmployeeCardViewModel>()
                .ForMember(e => e.EmployeeId, opt => opt.Ignore())
                .ForMember(e => e.Country, opt => opt.ResolveUsing(e =>
                {
                    if (e.Country.HasValue)
                    {
                        return SelectListRepository.Retrieve("Country", null).First(i => i.Value == "" + e.Country).Text;
                    }
                    return "-";
                }))
                .ForMember(e => e.Age, opt => opt.ResolveUsing(e =>
                {
                    var now = DateTime.UtcNow;
                    var age = now.Year - e.Birthday.Year;
                    if (now.Month < e.Birthday.Month || (now.Month == e.Birthday.Month && now.Day < e.Birthday.Day))
                    {
                        age--;
                    }
                    return age;
                }));
        }
    } 
}