﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Models
{
    public class OptionRecompileHintDbCommandInterceptor : IDbCommandInterceptor
    {
        public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
        }

        public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
        }

        public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            var dbContext = interceptionContext.DbContexts.FirstOrDefault() as SeismitoadDbContext;
            if (dbContext?.RecompileAllQueries == true)
            {
                AddQueryHint(command);
            }
        }

        public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
        }

        public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            var dbContext = interceptionContext.DbContexts.FirstOrDefault() as SeismitoadDbContext;
            if (dbContext?.RecompileAllQueries == true)
            {
                AddQueryHint(command);
            }
        }

        public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
        }

        private static void AddQueryHint(IDbCommand command)
        {
            if (command.CommandType != CommandType.Text)
            {
                return;
            }

            if (command.CommandText.StartsWith("select", StringComparison.OrdinalIgnoreCase) &&
                !command.CommandText.Contains("option(recompile)"))
            {
                command.CommandText = command.CommandText + " option(recompile)";
            }
        }
    }
}