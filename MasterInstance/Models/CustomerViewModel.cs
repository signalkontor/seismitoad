﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Models
{
    public class CustomerViewModel
    {
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Kundennummer", Description = "Kundendaten")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Kunde", Description = "Kundendaten")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Straße", Description = "Anschrift")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "PLZ", Description = "Anschrift")]
        public string ZipCode { get; set; }

        [Required]
        [Display(Name = "Ort", Description = "Anschrift")]
        public string City { get; set; }
    }

    public class CustomerViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Customer, CustomerViewModel>();
        }
    }
}