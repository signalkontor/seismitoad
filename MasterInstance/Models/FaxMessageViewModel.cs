﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Models
{
    public class FaxMessageViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Absender")]
        public string Sender { get; set; }

        [Required, Display(Name = "Betreff")]
        public string Subject { get; set; }

        [Required, Display(Name = "Nachricht")]
        public string Body { get; set; }
        public string AttachementFile { get; set; }

        [Required, Display(Name = "Empfangen am")]
        public DateTime Received { get; set; }

        [Display(Name = "Status")]
        public FaxMessageState State { get; set; }
    }

    public class FaxMessageViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<FaxMessage, FaxMessageViewModel>();
        }
    }
}