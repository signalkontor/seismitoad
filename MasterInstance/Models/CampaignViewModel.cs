﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Models
{
    public class CampaignViewModel
    { 
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public int Id { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Column("Customer_Id")]
        [Display(Name = "Kunde", Description = "Basisdaten")]
        public int? CustomerId { get; set; }

        [NotMapped]
        [UIHint("PopupWindow")]
        [WindowSize(185, 220)]
        [Display(Name = "Neuen Kunden anlegen", Description = "Basisdaten")]
        [Format(HintText = "Achtung! Vor dem Anlegen eines neuen Kunden sollten die Projektdaten gespeichert werden, da nicht gespeicherte Daten dabei verloren gehen.")]
        public string CreateCustomer { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2)]
        [Display(Name = "Projektnummer", Description = "Basisdaten")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Projektleiter", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public Guid ManagerId { get; set; }

        [Required]
        [Display(Name = "Projekttitel", Description = "Basisdaten")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Inhalt", Description = "Basisdaten")]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true)]
        public string Description { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Display(Name = "Teamgröße pro Aktionsort", Description = "Basisdaten")]
        public int EmployeesPerAssignment { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [Display(Name = "Status", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        public int State { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [Display(Name = "Ausschreibung aktiv?", Description = "Basisdaten")]
        [UIHint("Bool")]
        public bool IsApplyable { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [Display(Name = "Externes Reportingtool", Description = "Basisdaten")]
        [UIHint("Bool")]
        [Format(StartNewColumn = true)]
        public bool ExternalReportTool { get; set; }

        [Display(Name = "Externe Tool URL", Description = "Basisdaten")]
        public string ExternalReportToolUrl { get; set; }

        [Display(Name = "Aktionszeitraum", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Duration { get; set; }

        [Display(Name = "sukzessive Buchung", Description = "Zusatzinformationen")]
        public bool GradualBooking { get; set; }

        [Display(Name = "AP Kunde", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Contact { get; set; }

        [Display(Name = "Aktionstage", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Days { get; set; }

        [Display(Name = "Aktionsorte", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Locations { get; set; }

        [Display(Name = "Anforderungsprofil", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true)]
        public string Requirements { get; set; }

        [Display(Name = "Aktionszeiten", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Times { get; set; }

        [Display(Name = "Budget (Ablage KVA)", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Budget { get; set; }

        [Display(Name = "Klassifizierung der Aktionsorte", Description = "Zusatzinformationen")]
        [DataType(DataType.MultilineText)]
        public string Classification { get; set; }

        #region Ansprechpartner

        [NotMapped]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Per Media Ansprechpartner", Description = "ASP")]
        public string LabelPM { get; set; }

        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName1 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone1 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail1 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark1 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName2 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone2 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail2 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark2 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName3 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone3 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail3 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark3 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactPMName4 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactPMPhone4 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactPMEmail4 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactPMRemark4 { get; set; }


        [NotMapped]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Advantage Ansprechpartner", Description = "ASP")]
        public string LabelAD { get; set; }

        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName1 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone1 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail1 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark1 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName2 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone2 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail2 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark2 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName3 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone3 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail3 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark3 { get; set; }


        [Display(Name = "Name", Description = "ASP")]
        public string ContactADName4 { get; set; }

        [Display(Name = "Telefon", Description = "ASP")]
        public string ContactADPhone4 { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "E-Mail", Description = "ASP")]
        public string ContactADEmail4 { get; set; }

        [Display(Name = "Bemerkung", Description = "ASP")]
        public string ContactADRemark4 { get; set; }

        #endregion
    }

    public class CampaignViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Campaign, CampaignViewModel>()
                .ForMember(e => e.ExternalReportTool, opt => opt.MapFrom(e => e.InstanceUrl.StartsWith("_ext_")))
                .ForMember(e => e.ExternalReportToolUrl, opt => opt.ResolveUsing(e => e.InstanceUrl != null && e.InstanceUrl.StartsWith("_ext_") ? e.InstanceUrl.Replace("_ext_", "") : ""));
            
            Mapper.CreateMap<CampaignViewModel, Campaign>()
                .ForMember(e => e.InstanceUrl, opt => opt.ResolveUsing(e => e.ExternalReportTool ? "_ext_" + e.ExternalReportToolUrl : null));
        }
    }
}