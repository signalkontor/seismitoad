﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadShared;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;

namespace MasterInstance.Models.ViewModels
{
    //========================================
    //===     Model für /Search/Employee  ====
    public class SearchEmployeeModel
    {      
        private const string PersönlicheDaten = "Persönliche Daten";
        private const string BodyprofilUndTalente = "Bodyprofil und Talente";
        private const string VertragUndGewerbe = "Vertrag und Gewerbe";
        private const string Mobilität = "Mobilität";
        private const string AusbildungUndQualifikation = "Ausbildung und Qualifikation";
        private const string Auftragswunsch = "Auftragswunsch";
        private const string Bewertungen = "Bewertungen"; 
        private const string ProjekteStandort = "Projekte / Standort";

        private const string NoAdvantageProfile = "Kein altes Profil vorhanden";

        public SearchEmployeeModel()
        {
            ReferenceAdvice = NoAdvantageProfile;
            ReferenceGastronomicAbilities = NoAdvantageProfile;
            ReferenceInstallationHand = NoAdvantageProfile;
            ReferenceModel = NoAdvantageProfile;
            ReferenceModeration = NoAdvantageProfile;
            ReferenceMysteryshopper = NoAdvantageProfile;
            ReferenceOther = NoAdvantageProfile;
            ReferenceSampling = NoAdvantageProfile;
            ReferenceTasting = NoAdvantageProfile;
            ReferenceTradeShow = NoAdvantageProfile;
            ReferenceVIP = NoAdvantageProfile;
        }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int EmployeeId { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public string Username { get; set; }

        //public EmployeeProfile Profile { get; set; }
        [Visibility(ShowForDisplay =  false, ShowForEdit = false)]
        public IEnumerable<EmployeeProfile> Profiles { get; set; }

        //public SearchExperience Experience { get; set; }
        //public Rating Rating { get; set; }
        
        #region A. Persönliche Daten
        [Display(Name = "Anrede", Description = PersönlicheDaten)]
        [UIHint("SingleSelect")]
        [EnumerationName("Title")]
        [FilterType(FilterType.Equals)]
        public string EmployeeTitle { get; set; }

        [Display(Name = "Vorname", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Firstname { get; set; }

        [Display(Name = "Name", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Lastname { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Geburtstag", Description = PersönlicheDaten)]
        [FilterType(FilterType.Equals)]
        [UIHint("Date")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Alter", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string Age
        {
            get
            {
                var now = DateTime.UtcNow;
                var age = now.Year - Birthday.Year;
                if (now.Month < Birthday.Month || (now.Month == Birthday.Month && now.Day < Birthday.Day))
                {
                    age--;
                }
                return string.Format("{0} Jahre", age);
            }
        }

        //[NotMapped]
        //[Display(Name = "Alter", Description = PersönlicheDaten)]
        //[Visibility(ShowForEditRoles = AdminRoles, ShowForDisplay = false)]
        //[FilterType(FilterType.Between)]
        //public int? AgeFilter { get; set; }


        [Display(Name = "Letzter Login", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public DateTime LastLogin { get; set; }

        [Display(Name = "Letztes Profilupdate", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string LastProfileChange {
            get
            {
                var idAsString = EmployeeId.ToString(CultureInfo.InvariantCulture);
                var tableName = DB.Context.GetTableName<EmployeeProfile>();
                var newestChangeSet = DB.Context.ChangeSets
                                        .Where(e => e.TableName == tableName && e.EntityId == idAsString)
                                        .OrderByDescending(e => e.Date)
                                        .FirstOrDefault();

                return newestChangeSet != null
                    ? string.Format("am {0:dd.MM.yyyy HH:mm:ss} durch {1}", newestChangeSet.Date, Membership.GetUser(newestChangeSet.UserId).UserName)
                    : "Nie";
            }
        }

        [EmailAddress]
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "In Email enthalten", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Email2 { get; set; }

        //[EmailAddress]
        //[DataType(DataType.EmailAddress)]
        //[Display(Name = "Email 2", Description = PersönlicheDaten)]
        //public string Email2 { get; set; }

        [Display(Name = "Skype", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Skype { get; set; }

        [Display(Name = "Eigene Website", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Website { get; set; }

        [Display(Name = "Fremdpersonal bis", Description = PersönlicheDaten)]
        public DateTime? ExternalUntil { get; set; }

        [Display(Name = "Fremdpersonal", Description = PersönlicheDaten)]
        [Visibility(ShowForEdit = false)]
        public bool IsExternal
        {
            get { return ExternalUntil > LocalDateTime.Now; }
        }

        [Display(Name = "Fremdpersonal", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string IsExternalDisplay
        {
            get { return ExternalUntil > LocalDateTime.Now ? "Ja" : "Nein"; }
        }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobil", Description = PersönlicheDaten)]
        [Format(StartNewColumn = true)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string PhoneMobileNo { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Festnetz", Description = PersönlicheDaten)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string PhoneNo { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Fax", Description = PersönlicheDaten)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string FaxNo { get; set; }

        [Display(Name = "Straße", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Street { get; set; }

        [Display(Name = "PLZ", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string PostalCode { get; set; }

        [Display(Name = "Ort", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string City { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Land", Description = PersönlicheDaten)]
        [FilterType(FilterType.Equals)]
        public int? Country { get; set; }

        [Display(Name = "Koordinaten", Description = PersönlicheDaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("DbGeography")]
        public DbGeography GeographicCoordinates { get; set; }

        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string ImageBaseUrl { get; set; }
        
        [Display(Name = "Portraitfoto", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        [Format(StartNewColumn = true)]
        public string DisplayPortraitPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadPortraitPhoto", 120, 120); } }

        [Display(Name = "Ganzkörperfoto", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        public string DisplayFullBodyPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadFullBodyPhoto", 120, 120); } }

        [Display(Name = "Weiteres Foto", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        public string DisplayOtherPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadOtherPhoto", 120, 120); } }

        [Display(Name = "Offenes Bemerkungsfeld", Description = PersönlicheDaten)]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [FilterType(FilterType.Contains)]
        public string PersonalDataComment { get; set; }
        #endregion

        #region B. Body Profil und Talente
        [Range(140, 220)]
        [Display(Name = "Körpergröße (in cm)", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Between)]
        public int? Height { get; set; }

        [Display(Name = "Konfektionsgröße", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? Size { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "T-Shirt-Größe", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? ShirtSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeansweite", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? JeansWidth { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeanslänge", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? JeansLength { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Haarfarbe", Description = BodyprofilUndTalente)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Equals)]
        public int? HairColor { get; set; }

        [Display(Name = "Sichtbare Tattoos", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        [FilterType(FilterType.Equals)]
        public bool? VisibleTattoos { get; set; }

        [Display(Name = "Gesichtspiercing/s", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        [FilterType(FilterType.Equals)]
        public bool? FacialPiercing { get; set; }

        [Format(StartNewColumn = true)]
        [UIHint("MultiSelect")]
        [Display(Name = "Künstlerisches", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] ArtisticAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Gastro", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] GastronomicAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Aktivitäten / Sport", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Sports { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Weitere Talente und Hobbies", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] OtherAbilities { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstiges", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Contains)]
        public string OtherAbilitiesText { get; set; }

        [Display(Name = "Referenzen aus alter Datenbank", Description = BodyprofilUndTalente)]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        public string Header { get; set; }

        [Display(Name = "Gastronomie", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceGastronomicAbilities { get; set; }

        [Display(Name = "Sampling", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceSampling { get; set; }

        [Display(Name = "Fachberatung", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceAdvice { get; set; }

        [Display(Name = "Messe/Event", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceTradeShow { get; set; }

        [Display(Name = "Verkostung", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceTasting { get; set; }

        [Display(Name = "Sonstige", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceOther { get; set; }

        [Display(Name = "Moderation", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceModeration { get; set; }

        [Display(Name = "Aufbauhelfer", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceInstallationHand { get; set; }

        [Display(Name = "VIP-Einsatz", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceVIP { get; set; }

        [Display(Name = "Mysteryshopper", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceMysteryshopper { get; set; }

        [Display(Name = "Model", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceModel { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        [Display(Name = "Rahmenvertrag", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? SkeletonContract { get; set; }

        [Display(Name = "Kopie Gewerbeschein vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? CopyTradeLicense { get; set; }

        [Display(Name = "Bist du berechtigt eigene Rechnungen zu stellen?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? ValidTradeLicense { get; set; }

        [Display(Name = "Finanzamt, Sitz/Ort", Description = VertragUndGewerbe)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Contains)]
        public string TaxOfficeLocation { get; set; }

        [Display(Name = "Steuernummer", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Contains)]
        public string TaxNumber { get; set; }

        [Display(Name = "Ust-abzugsfähig?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? TurnoverTaxDeductible { get; set; }

        [Display(Name = "Fragebogen zur Selbstständigkeit vorhanden", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? FreelancerQuestionaire { get; set; }

        [Display(Name = "Gesundheitszeugnis", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? HealthCertificate { get; set; }

        [Display(Name = "Kopie Gesundheitszeugnis vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? CopyHealthCertificate { get; set; }

        [Display(Name = "Kopie Personalausweis vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public int? CopyIdentityCard { get; set; }

        [Display(Name = "Gewerbeschein", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        [Format(StartNewColumn = true)]
        public string DisplayTradeLicense { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadTradeLicense", 120, 120); } }

        [Display(Name = "Gesundheitszeugnis", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        public string DisplayHealthCertificate { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadHealthCertificate", 120, 120); } }

        [Display(Name = "Personalausweis", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        public string DisplayIdentityCard { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadIdentityCard", 120, 120); } }

        [Display(Name = "Bemerkungen", Description = VertragUndGewerbe)]
        [DataType(DataType.MultilineText)]
        [FilterType(FilterType.Contains)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        public string Comment { get; set; }
        #endregion

        #region D. Mobilität
        [Display(Name = "Führerschein vorhanden", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? DriversLicense { get; set; }

        [UIHint("Date")]
        [Display(Name = "Führerschein seit", Description = Mobilität)]
        public DateTime? DriversLicenseSince { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (neue Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass1 { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (alte Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass2 { get; set; }

        [Display(Name = "Digitale Fahrerkarte vorhanden", Description = Mobilität)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Equals)]
        public bool? HasDigitalDriversCard { get; set; }

        [Display(Name = "PKW vorhanden", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? IsCarAvailable { get; set; }

        [Display(Name = "Bahncard vorhanden", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? IsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        [UIHint("SingleSelect")]
        [Display(Name = "Schulbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? Education { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Abgeschlossene Berufsausbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Apprenticeships { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstige abgeschlossene Berufsausbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string OtherCompletedApprenticeships { get; set; }

        [Display(Name = "Studium", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Equals)]
        [Toggles(".sk-toggle-1")]
        public bool? Studies { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Details zum Studium", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        [Format(EditorLabelHtmlAttributesIndex = HtmlAttributes.SkToggle1, EditorFieldHtmlAttributesIndex = HtmlAttributes.SkToggle1)]
        public string StudiesDetails { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Berufserfahrung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string WorkExperience { get; set; }

        [Display(Name = "Deutsch", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesGerman { get; set; }

        [Display(Name = "Englisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesEnglish { get; set; }

        [Display(Name = "Spanisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesSpanish { get; set; }

        [Display(Name = "Französisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesFrench { get; set; }

        [Display(Name = "Italienisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesItalian { get; set; }

        [Display(Name = "Türkisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesTurkish { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstige", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string LanguagesOther { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Software-Kentnisse", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [EnumerationName("ITKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? SoftwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string SoftwareKnownledgeDetails { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Hardware-Kentnisse", Description = AusbildungUndQualifikation)]
        [EnumerationName("ITKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? HardwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string HardwareKnownledgeDetails { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Schulungen intern", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string InternalTraining { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Schulungen extern", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string ExternalTraining { get; set; }
        #endregion

        #region G. Auftragswunsch
        [UIHint("MultiSelect")]
        [FilterType(FilterType.AnyOrAll)]
        [Display(Name = "Bevorzugte Promotion Aktivitäten", Description = Auftragswunsch)]
        public string[] PreferredTasks { get; set; }

        [UIHint("MultiSelect")]
        [FilterType(FilterType.AnyOrAll)]
        [Display(Name = "Bevorzugte Promotionarten", Description = Auftragswunsch)]
        public string[] PreferredTypes { get; set; }

        [UIHint("MultiSelect")]
        [FilterType(FilterType.AnyOrAll)]
        [Display(Name = "Bevorzugte Arbeitszeiten", Description = Auftragswunsch)]
        public string[] PreferredWorkSchedule { get; set; }

        [FilterType(FilterType.Equals)]
        [Display(Name = "Reisebereitschaft", Description = Auftragswunsch)]
        [Format(StartNewColumn = true)]
        public bool? TravelWillingness { get; set; }

        [UIHint("MultiSelect")]
        [FilterType(FilterType.AnyOrAll)]
        [Display(Name = "Übernachtung möglich im Großraum", Description = Auftragswunsch)]
        public string[] AccommodationPossible1 { get; set; }

        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Übernachtung möglich in folgenden Städten", Description = Auftragswunsch)]
        //public string AccommodationPossible2 { get; set; }
        #endregion

        #region Projekte / Standort

        [Display(Name = "Durchgeführtes Projekt", Description = ProjekteStandort)]
        [FilterType(FilterType.Contains)]
        public string Project { get; set; }

        [CustomValidation(typeof(SearchEmployeeModel), "ValidateLocation")]
        [Display(Name = "Standort", Description = ProjekteStandort)]
        [FilterType(FilterType.Equals)]
        public int? LocationId { get; set; }
        public static ValidationResult ValidateLocation(object value, ValidationContext context)
        {
            var model = context.ObjectInstance as AcquisitionModel;

            if (model != null && model.LocationId != null && model.Distance == null)
                return new ValidationResult("Bitte geben Sie einen Umkreis zum Standort an!", new[] { "LocationId", "Distance" });

            return ValidationResult.Success;
        }

        [CustomValidation(typeof(SearchEmployeeModel), "ValidateDistance")]
        [Display(Name = "Umkreis", Description = ProjekteStandort)]
        [FilterType(FilterType.Equals)]
        public int? Distance { get; set; }
        public static ValidationResult ValidateDistance(object value, ValidationContext context)
        {
            var model = context.ObjectInstance as AcquisitionModel;

            if (model != null && model.LocationId == null && model.Distance != null)
                return new ValidationResult("Bitte geben Sie einen Standort zum Umkreis an!", new[] { "Distance", "LocationId" });

            return ValidationResult.Success;
        }

        public Location Location { get; set; }
        public IEnumerable<Location> Locations { get; set; }

        #endregion

        //#region Bewertungen
        //[UIHint("Ratings")]
        //[FilterType(FilterType.Equals)]
        //[Display(Name = "Bewertungen", Description = Bewertungen)]
        //[Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        //public int? RatingsForEmployeeId { get { return EmployeeId; } }
        //#endregion

        //#region Referenzen
        //[UIHint("Experiences")]
        //[Display(Name = "Referenzen", Description = "Referenzen")]
        //[FilterType(FilterType.Contains)]
        //[Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        //[HideSurroundingHtml]
        //public IEnumerable<ExperienceViewModel> Experiences { get; set; }
        //#endregion

    }

    public class EmployeeProfileViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            #region Employee <=> EmployeeProfileViewModel

            Mapper.CreateMap<Employee, SearchEmployeeModel>()
                .ForSourceMember(e => e.Profile, opt => opt.Ignore());

            Mapper.CreateMap<SearchEmployeeModel, Employee>();
            #endregion

             #region Person <=> EmployeeProfileViewModel

            Mapper.CreateMap<Person, SearchEmployeeModel>()
                .ForMember(e => e.Firstname, opt => opt.MapFrom(i => i.Firstname));

            Mapper.CreateMap<SearchEmployeeModel, Person>();
            #endregion

            #region EmployeeProfile <=> SearchEmployeeModel
            Mapper.CreateMap<EmployeeProfile, SearchEmployeeModel>()
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeSplit(';')))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeSplit(';')))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeSplit(';')))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeSplit(';')))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeSplit(';')))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeSplit(';')))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeSplit(';')))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeSplit(';')));

            Mapper.CreateMap<SearchEmployeeModel, EmployeeProfile>()
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeJoin(";")))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeJoin(";")))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeJoin(";")))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeJoin(";")))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeJoin(";")))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeJoin(";")))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeJoin(";")))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeJoin(";")));
            #endregion

            #region MembershipUser => SearchEmployeeModel
            Mapper.CreateMap<MembershipUser, SearchEmployeeModel>()
                .ForMember(e => e.Username, opt => opt.MapFrom(e => e.UserName))
                .ForMember(e => e.Email2, opt => opt.MapFrom(e => e.Email))
                .ForMember(e => e.LastLogin, opt => opt.MapFrom(e => e.LastLoginDate));
            #endregion

            #region doa_login => SearchEmployeeModel
            Mapper.CreateMap<AdvantageModel.Models.doa_login, SearchEmployeeModel>()
                            .ForMember(e => e.ReferenceGastronomicAbilities, opt => opt.MapFrom(e => e.doa_qualifikation.gastro))
                            .ForMember(e => e.ReferenceSampling, opt => opt.MapFrom(e => e.doa_qualifikation.sampling))
                            .ForMember(e => e.ReferenceAdvice, opt => opt.MapFrom(e => e.doa_qualifikation.fachberatung))
                            .ForMember(e => e.ReferenceTradeShow, opt => opt.MapFrom(e => e.doa_qualifikation.messe_event))
                            .ForMember(e => e.ReferenceTasting, opt => opt.MapFrom(e => e.doa_qualifikation.verkostung))
                            .ForMember(e => e.ReferenceOther, opt => opt.MapFrom(e => e.doa_qualifikation.sonstige))
                            .ForMember(e => e.ReferenceModeration, opt => opt.MapFrom(e => e.doa_qualifikation.moderation))
                            .ForMember(e => e.ReferenceInstallationHand, opt => opt.MapFrom(e => e.doa_qualifikation.aufbauhelfer))
                            .ForMember(e => e.ReferenceVIP, opt => opt.MapFrom(e => e.doa_qualifikation.vip_einsatz))
                            .ForMember(e => e.ReferenceMysteryshopper, opt => opt.MapFrom(e => e.doa_qualifikation.mysteryshopper))
                            .ForMember(e => e.ReferenceModel, opt => opt.MapFrom(e => e.doa_qualifikation.model));
            #endregion
        }
    }

    //========================================
    //===     Model für /Search/Location  ====
    public class SearchLocationModel
    {
        public Location Location{ get; set; }
        public IEnumerable<Location> Locations { get; set; }

        [Display(Name = "Durchgeführte Projekte")]
        public string Project { get; set; }
    }


    //[MetadataType(typeof(SearchExperienceMetadata))]
    public class SearchExperience : Experience
    {
        [UIHint("SingleSelect")]
        [EnumerationType(typeof(ExperienceType))]
        [Display(Name = "Typ")]
        public new int? Type { get; set; }
    }
}