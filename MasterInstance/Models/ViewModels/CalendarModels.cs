using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using SeismitoadModel;
using SeismitoadShared.Extensions;

namespace MasterInstance.Models.ViewModels
{
    public class CalendarFilterModel
    {
        public CalendarFilterModel()
        {
            ShowAllAssignments = false;
            CampaignId = -1;
            Distance = -1;
        }

        public bool ShowAllAssignments { get; set; }
        public int CampaignId { get; set; }
        public int Distance { get; set; }
    }

    public class CalendarModel : CalendarFilterModel
    {
        public DateTime CalendarDefaultDate { get; set; }
        public string CalendarUrl { get; set; }
        public IEnumerable<SelectListItem> Campaigns { get; set; }
    }

    public class CalendarEvent
    {
// Inconsistent naming is needed for compatibility with fullcalendar.js
// ReSharper disable InconsistentNaming
        public int id { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string borderColor { get; set; }
        public string textColor { get; set; }
        public string tooltip { get; set; }
        public bool isReadonly { get; set; }
// ReSharper restore InconsistentNaming

        public static IEnumerable<CalendarEvent> CreateEvents(long start, long end, int employeeId, int campaignId, int locationId, bool onlyIncomplete, Func<Assignment, string> titleFunc, Func<Assignment, bool, IEnumerable<Assignment>, string> toolTipFunc)
        {
            var dateStart = start.FromUnixTimeStampToDateTime();
            var dateEnd = end.FromUnixTimeStampToDateTime();

            var assignments = DB.Context.Assignments
                .Where(e =>
                       e.DateStart >= dateStart &&
                       e.DateEnd <= dateEnd &&
                       e.State == AssignmentState.Planned &&
                       (campaignId == -1 || e.CampaignLocation.CampaignId == campaignId) &&
                       (locationId == -1 || e.CampaignLocation.LocationId == locationId) &&
                       (onlyIncomplete == false || e.AssignedEmployees.Count() < e.EmployeeCount));

            return assignments
                .ToList()
                .Select(e =>
                            {
                                var conflictingAssignments = DB.Context.GetConflictingAssignments(e.DateStart, e.DateEnd, employeeId);
                                var hasConflictingAssignments = conflictingAssignments.Any();
                                return new CalendarEvent
                                           {
                                               id = e.Id,
                                               title = titleFunc(e),
                                               allDay = false,
                                               start = e.DateStart.ToString("s"),
                                               end = e.DateEnd.ToString("s"),
                                               isReadonly = hasConflictingAssignments,
                                               borderColor = e.TrafficLight,
                                               textColor = hasConflictingAssignments ? "red" : "inherit",
                                               tooltip = toolTipFunc(e, hasConflictingAssignments, conflictingAssignments)
                                           };
                            });
        }
    }
}