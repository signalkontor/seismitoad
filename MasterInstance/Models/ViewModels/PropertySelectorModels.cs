﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MasterInstance.Models.ViewModels
{
    public interface IPropertySelectorModel
    {
        SelectListItem[] Properties { get; set; }
        string[] VisibleProperties { get; set; }
        string ControllerName { get; }
    }

    public class VisiblePropertiesModel
    {
        public bool Reset { get; set; }
        public string[] P { get; set; }
    }
}