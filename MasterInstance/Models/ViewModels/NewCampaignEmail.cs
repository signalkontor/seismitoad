﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Models.ViewModels
{
    public class NewCampaignEmail : NotificationEmailBase
    {
        public string Campaign { get; set; }

        public NewCampaignEmail(string viewName, string userNameOfSender, string campaign)
            : base(viewName, userNameOfSender)
        {
            Campaign = campaign;
        }
    }
}