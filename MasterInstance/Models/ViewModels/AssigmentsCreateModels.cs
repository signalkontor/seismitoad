﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using Telerik.Web.Mvc;

namespace MasterInstance.Models.ViewModels
{
    public class AssigmentsCreateModel : IPropertySelectorModel
    {
        public int CampaignId { get; set; }
        public string CampaignClassification { get; set; }
        public GridModel GridModel { get; set; }
        public AssignmentDateGeneratorModel AssignmentDateGeneratorModel { get; set; }
        public SelectListItem[] Properties { get; set; }
        public string[] VisibleProperties { get; set; }

        public string ControllerName
        {
            get { return MVC.Assignment.Name; }
        }
    }

    public class AssignmentDateGeneratorModel : IValidatableObject
    {
        [Display(Name = "Startdatum")]
        [UIHint("Date")]
        public DateTime DateStart { get; set; }

        [Display(Name = "Enddatum")]
        [UIHint("Date")]
        public DateTime DateEnd { get; set; }

        [Display(Name = "Intervall")]
        [UIHint("SingleSelect")]
        public int Interval { get; set; }

        public Day Monday { get; set; }
        public Day Tuesday { get; set; }
        public Day Wednesday { get; set; }
        public Day Thursday { get; set; }
        public Day Friday { get; set; }
        public Day Saturday { get; set; }
        public Day Sunday { get; set; }

        public Day[] Dates { get; set; }

        public int DateCount { get { return Dates != null ? Dates.Length : 0; } }

        public void CalculateDates()
        {
            Dates = GetDates().ToArray();
        }

        //public void Store(int id)
        //{
        //    HttpContext.Current.Session[string.Format("ADGM_{0}", id)] = this;
        //}

        //public static AssignmentDateGeneratorModel Restore(int id)
        //{
        //    var employeesPerAssignment = DB.Context.Campaigns.Single(e => e.Id == id).EmployeesPerAssignment;
        //    var assignmentDateGeneratorModel = HttpContext.Current.Session[string.Format("ADGM_{0}", id)] as AssignmentDateGeneratorModel ??
        //                                       new AssignmentDateGeneratorModel
        //                                           {
        //                                               Monday = new Day {EmployeeCount = employeesPerAssignment},
        //                                               Tuesday = new Day { EmployeeCount = employeesPerAssignment },
        //                                               Wednesday = new Day { EmployeeCount = employeesPerAssignment },
        //                                               Thursday = new Day { EmployeeCount = employeesPerAssignment },
        //                                               Friday = new Day { EmployeeCount = employeesPerAssignment },
        //                                               Saturday = new Day { EmployeeCount = employeesPerAssignment },
        //                                               Sunday = new Day { EmployeeCount = employeesPerAssignment }
        //                                           };
        //    return assignmentDateGeneratorModel;
        //}

        private IEnumerable<Day> GetDates()
        {
            var dayCount = 0;
            for (var day = DateStart; day <= DateEnd && Interval != 0; day = day.AddDays(1), dayCount++)
            {
                if (dayCount == 7)
                {
                    day = day.AddDays(Interval - 7);
                    dayCount = 0;
                    if (day > DateEnd)
                        break;
                }

                TimeSpan start, end;
                int employeeCount;
                switch (day.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Monday?.Selected != true || Monday?.EmployeeCount == null) continue;
                        start = Monday.Start.GetValueOrDefault().TimeOfDay;
                        end = Monday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Monday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Tuesday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Tuesday?.Selected != true || Tuesday?.EmployeeCount == null) continue;
                        start = Tuesday.Start.GetValueOrDefault().TimeOfDay;
                        end = Tuesday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Tuesday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Wednesday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Wednesday?.Selected != true || Wednesday?.EmployeeCount == null) continue;
                        start = Wednesday.Start.GetValueOrDefault().TimeOfDay;
                        end = Wednesday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Wednesday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Thursday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Thursday?.Selected != true || Thursday?.EmployeeCount == null) continue;
                        start = Thursday.Start.GetValueOrDefault().TimeOfDay;
                        end = Thursday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Thursday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Friday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Friday?.Selected != true || Friday?.EmployeeCount == null) continue;
                        start = Friday.Start.GetValueOrDefault().TimeOfDay;
                        end = Friday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Friday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Saturday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Saturday?.Selected != true || Saturday?.EmployeeCount == null) continue;
                        start = Saturday.Start.GetValueOrDefault().TimeOfDay;
                        end = Saturday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Saturday.EmployeeCount.Value;
                        break;
                    case DayOfWeek.Sunday:
                        // Da x?.Selected NULL sein kann und NULL nicht das selbe wie false ist, ist der richtige Vergleich hier "!= true" und nicht "== false".
                        if (Sunday?.Selected != true || Sunday?.EmployeeCount == null) continue;
                        start = Sunday.Start.GetValueOrDefault().TimeOfDay;
                        end = Sunday.End.GetValueOrDefault().TimeOfDay;
                        employeeCount = Sunday.EmployeeCount.Value;
                        break;
                    default:
                        continue;
                }

                var result = new Day { Start = day.Add(start), End = day.Add(end), EmployeeCount = employeeCount };
                if(result.Start.Value.Hour < 5)
                {
                    result.Start = result.Start.Value.AddDays(1);
                }
                if (result.End.Value.Hour < 5)
                {
                    result.End = result.End.Value.AddDays(1);
                }
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateStart > DateEnd)
            {
                yield return new ValidationResult("Startdatum darf nicht nach Enddatum liegen.", new[] { "DateStart", "DateEnd" });
            }
        }
    }

    public class SelectEmployeeModel
    {
        public Assignment Assignment { get; set; }
        public IEnumerable<Employee> CampaignEmployees { get; set; }
        public IEnumerable<Employee> NearByEmployees { get; set; }

        public int EmployeeId { get; set; }
        
        [UIHint("MultiSelect")]
        [Display(Name = "Rollen")]
        public int[] AssignmentRoleIds { get; set; }

        [Display(Name = "Wiederholung?")]
        [UIHint("SingleSelect")]
        public AssignmentRepeatType AssignmentRepeatType { get; set; }

        public int[] AssignmentIds { get; set; }
    }

    public class AssignToPromoterModel
    {
        public Campaign Campaign { get; set; }
        public IEnumerable<IGrouping<Location, Assignment>> Assignments { get; set; }

        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public DateTime DisplayStartDate { get; set; }
        public DateTime DisplayEndDate { get; set; }

        public IEnumerable<DateTime> WeekStartDates
        {
            get
            {
                for (var date = MinDate; date <= MaxDate; date = date.AddDays(7))
                {
                    yield return date;
                }
            }
        }

        [Required]
        [UIHint("SingleSelect")]
        public AssignmentFilterType AssignmentFilterType { get; set; }

        [UIHint("MultiSelect")]
        public int[] FilterIds { get; set; }

        [UIHint("SingleSelect")]
        [EnumerationType(typeof(AssignmentType))]
        public AssignmentType AssignmentType { get; set; }

        public bool? Goto { get; set; }

        public DateTime? GoToDate { get; set; }
        public int Week { get; set; }
        public int MaxWeekNumber { get; set; }
    }

    public class Day : IValidatableObject
    {
        [Visibility(ShowForDisplay = false)]
        public bool Selected { get; set; }
        [Display(Name = "Anfang")]
        public DateTime? Start { get; set; }
        [Display(Name = "Ende")]
        public DateTime? End { get; set; }

        [Range(1, 100)]
        [Display(Name = "Anzahl Promoter")]
        public int? EmployeeCount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Selected && (EmployeeCount == null || EmployeeCount < 1))
            {
                yield return new ValidationResult("Bitte Promoteranzahl angeben", new[] { "EmployeeCount" });
            }
            if (Selected && !Start.HasValue)
            {
                yield return new ValidationResult("Bitte Startzeit angeben", new[] { "Start" });
            }
            if (Selected && !End.HasValue)
            {
                yield return new ValidationResult("Bitte Startzeit angeben", new[] { "End" });
            }
            if (!Start.HasValue || !End.HasValue)
            {
                yield break;
            }

            var start = Start.Value;
            if(start.Hour < 5)
            {
                start = start.AddDays(1);
            }
            var end = End.Value;
            if (end.Hour < 5)
            {
                end = end.AddDays(1);
            }

            if (start >= end)
            {
                yield return new ValidationResult("Anfang muss vor Ende liegen.", new[] {"Start", "End"});
            }
        }
    }

    public class Pair<T1, T2>
    {
        public Pair() {}

        public Pair(T1 _1, T2 _2)
        {
            this._1 = _1;
            this._2 = _2;
        }

        public T1 _1 { get; set; }
        public T2 _2 { get; set; }

        public bool Equals(Pair<T1, T2> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._1, _1) && Equals(other._2, _2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Pair<T1, T2>)) return false;
            return Equals((Pair<T1, T2>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_1.GetHashCode()*397) ^ _2.GetHashCode();
            }
        }
    }

    public enum AssignmentRepeatType
    {
        [Display(Name = "Nicht wiederholen")]
        NoRepeat = 0,

        [Display(Name = "Am selben Wochentag")]
        SameWeekday = 1,

        [Display(Name = "An allen folgenden Aktionstagen")]
        Everyday = 2,

        [Display(Name = "Manuelle Auswahl")]
        Custom = 3,
    }

    public enum AssignmentEditAction
    {
        Insert = 0,
        Delete = 1,
        InsertForAll = 2
    }

    public enum AssignmentType
    {
        [Display(Name = "Alle zeigen")]
        Any = 1,
        [Display(Name = "Nur unterbesetzte")]
        Incomplete = 2,
        [Display(Name = "Nur gelöschte")]
        Deleted = 3,
    }


    public enum AssignmentFilterType
    {
        [Display(Name = "Ort")]
        Location = 1,
        [Display(Name = "Promoter")]
        Employee = 2
    }
}