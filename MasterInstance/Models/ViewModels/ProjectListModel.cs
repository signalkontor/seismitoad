﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterInstance.Models.ViewModels
{
    public class ProjectListModel
    {
        [Display(Name = "Kunde")]
        public string Customer { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}