﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ObjectTemplate.Attributes;
using SeismitoadShared.Attributes;

namespace MasterInstance.Models.ViewModels
{
    public class UserViewModel
    {
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public Guid ProviderUserkey { get; set; }

        [Required]
        [Display(Name = "Zugangsart (kann nicht nachträglich geändert werden)", Description = "Basisdaten")]
        [UIHint("SingleSelect")]
        [ToggleVisibility(new[] { "CustomerId" }, new[] { "Kunde" })]
        public string AccountType { get; set; }

        [Display(Name = "Kunde", Description = "Basisdaten")]
        [SkRequiredIf("AccountType", "Kunde")]
        [UIHint("SingleSelect")]
        public int? CustomerId { get; set; }

        [Required]
        [Display(Name = "Benutzername", Description = "Basisdaten")]
        public string UserName { get; set; }

        [SkRequiredIf("IsCreate", true)]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort", Description = "Basisdaten")]
        public string Password { get; set; }

        [EmailAddress]
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-Mail", Description = "Basisdaten")]
        public string Email { get; set; }

        [Display(Name = "Letzter Login")]
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public DateTime LastLoginDate { get; set; }

        [Display(Name = "Letzte Aktivität")]
        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public DateTime LastActivityDate { get; set; }

        [Required]
        [UIHint("SingleSelect")]
        [Display(Name = "Anrede", Description = "Basisdaten")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Vorname", Description = "Basisdaten")]
        public string Firstname { get; set; }
        
        [Required]
        [Display(Name = "Nachname", Description = "Basisdaten")]
        public string Lastname { get; set; }

        [Display(Name = "Aktiv", Description = "Basisdaten")]
        public bool IsActive { get; set; }

        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public bool IsCreate { get; set; }
    }
}