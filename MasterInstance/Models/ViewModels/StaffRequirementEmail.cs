﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Models.ViewModels
{
    public class StaffRequirementEmail : NotificationEmailBase
    {
        public string Campaign { get; set; }
        public bool IsUpdate { get; set; }

        public StaffRequirementEmail(string viewName, string userNameOfSender, string campaign, bool isUpdate)
            : base(viewName, userNameOfSender)
        {
            Campaign = campaign;
            IsUpdate = isUpdate;
        }
    }
}