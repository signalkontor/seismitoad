﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class AssignmentDeleteModel
    {
        [Display(Name = "Ab Datum")]
        [UIHint("Date")]
        public DateTime Start { get; set; }
        
        [Display(Name = "Bis Datum (optional)")]
        [UIHint("Date")]
        public DateTime? End { get; set; }

        [Display(Name = "Locations")]
        public int[] LocationIds { get; set; }

        [Display(Name = "Promoter")]
        public int[] EmployeeIds { get; set; }

        [Display(Name = "Wochentage")]
        public int[] DaysOfWeek { get; set; }

        public IEnumerable<Assignment> Assignments { get; set; }
    }
}