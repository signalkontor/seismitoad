﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Models.ViewModels
{
    public abstract class CreateModelWithTemplate<T> where T : class
    {
        public T Template { get; set; }

        public void StoreTemplate()
        {
            HttpContext.Current.Session[GetType().Name] = Template;
        }

        public void RestoreTemplate()
        {
            Template = HttpContext.Current.Session[GetType().Name] as T;
        }
    }
}