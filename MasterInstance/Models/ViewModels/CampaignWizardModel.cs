﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class CampaignWizardModel
    {
        public int CampaignId { get; set; }
        public int Step { get; set; }
        public int MaxStep { get; set; }
        public ActionResult ActionResult { get; set; } 
    }
}