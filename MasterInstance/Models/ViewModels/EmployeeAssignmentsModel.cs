﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class EmployeeAssignmentsModel
    {
        public Employee Employee { get; set; }
        public Assignment Assignment { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }
    }
}