﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadShared.Attributes;

namespace MasterInstance.Models.ViewModels
{
    public class EmployeeAddModel
    {
        [Required]
        [EnumerationName("Title")]
        [Display(Name = "Anrede", Description = "Neuer Promoter")]
        [UIHint("SingleSelect")]
        public string EmployeeTitle { get; set; }

        [Required]
        [Display(Name = "Vorname", Description = "Neuer Promoter")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Name", Description = "Neuer Promoter")]
        public string Lastname { get; set; }

        [EmailAddress]
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", Description = "Neuer Promoter")]
        public string Email { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public IEnumerable<Employee> Employees { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int Step { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public EmployeeAddAllModel AddAllModel { get; set; }

    }

    public class EmployeeAddAllModel
    {
        [Required]
        [RegularExpression("[A-Za-z][A-Za-z0-9]{4,14}", ErrorMessage = "Kein gültiger Benutzername! Erlaubt sind Buchstaben a-z und die Zahlen 0-9 (min. 5 Zeichen / max. 15 Zeichen)")]
        [Display(Name = "Benutzername", Description = "Neuer Promoter2")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Passwort", Description = "Neuer Promoter2")]
        public string Password { get; set; }

        [Required]
        [EnumerationType(typeof(EmployeeState))]
        [UIHint("SingleSelect")]
        [Display(Name = "Status", Description = "Neuer Promoter2")]
        public EmployeeState State { get; set; }
    }
}
