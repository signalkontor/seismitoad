﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class HomeCampaignViewModel
    {
        public int Id { get; set; }
        public CampaignState State { get; set; }
        public string CustomerName { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public Guid ManagerId { get; set; }
    }

    public class HomeCampaignViewModelAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Campaign, HomeCampaignViewModel>()
                .ForMember(e => e.Number, opt => opt.MapFrom(e => e.Customer.Number + e.Number));
        }
    }
}