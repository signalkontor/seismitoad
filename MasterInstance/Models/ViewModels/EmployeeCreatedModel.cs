﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class EmployeeCreatedModel
    {
        public string Password { get; set; }
        public MembershipUser MembershipUser { get; set; }
        public Employee Employee { get; set; }
    }
}