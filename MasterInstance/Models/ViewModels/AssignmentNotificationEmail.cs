﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using System.Web.Security;

namespace MasterInstance.Models.ViewModels
{
    public class AssignmentNotificationEmail : NotificationEmailBase
    {
        public AssignmentNotificationEmail(PromoterNotificationData data) : base(data.Type, data.UserNameOfSender)
        {
            // Diese Sortierung ist nur "Just in case". Eigentlich sollten die Tage schon sortiert sein.
            // Wir dürfen aber auf keinen Fall sortieren wenn es Verschiebungen sind, denn dann ist immer am geraden index der original
            // Einsatz und am ungeraden der Ersatztermin. Wenn sich nun mehrere Verschiebungen überschneiden würden durch die Sortierung
            // diese Ordnung verloren gehen. Also wenn der erste Einsatz der verschoben wird einen Ersatztermin hat der nach einem der
            // anderen verschobenen Termine liegt. Daher sortieren wir bei Move einfach nihts.
            Assignments = data.Type == PromoterNotification.TypeMovedAssignment ? data.Assignments : data.Assignments.OrderBy(e => e.DateStart).ToArray();
            using (var dbContext = new SeismitoadDbContext())
            {
                var assignmentId = data.Assignments[0].AssignmentId;
                var assignment = dbContext.Assignments
                    .Where(e => e.Id == assignmentId)
                    .Include(e => e.CampaignLocation.Location)
                    .Include(e => e.CampaignLocation.Campaign)
                    .First();

                CampaignName = assignment.CampaignLocation.Campaign.Name;
                var employee = dbContext.Employees.Single(e => e.Id == data.EmployeeId);
                var location = assignment.CampaignLocation.Location;
                Firstname = employee.Firstname;
                LocationName = location.Name;
                LocationStreet = location.Street;
                LocationCity = location.City;
                LocationPostalCode = location.PostalCode;
                var user = Membership.GetUser(employee.ProviderUserKey, false);
                Username = user.UserName;
                AddRecipient(employee.Email);
            }
        }

        public string Username { get; set; }
        public string Firstname { get; private set; }
        public string CampaignName { get; private set; }
        public AssignmentData[] Assignments { get; private set; }
        public string LocationName { get; private set; }
        public string LocationStreet { get; private set; }
        public string LocationCity { get; private set; }
        public string LocationPostalCode { get; private set; }
    }
}