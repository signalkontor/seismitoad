﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using SeismitoadModel;
using Elmah;

namespace MasterInstance.Models.ViewModels
{
    public class AcquisitionEmail : NotificationEmailBase
    {
        public AcquisitionEmail(CampaignEmployee campaignEmployee, string viewName)
            : this(campaignEmployee, viewName, HttpContext.Current.User.Identity.Name)
        { }

        public AcquisitionEmail(CampaignEmployee campaignEmployee, string viewName, string userNameOfSender)
            : base(viewName, userNameOfSender)
        {
            CampaignEmployee = campaignEmployee;
            var user = Membership.GetUser(campaignEmployee.Employee.ProviderUserKey, false);
            if (string.IsNullOrWhiteSpace(user?.Email))
            {
                Error = "Es liegt keine E-Mail Adresse vor.";
                return;
            }
            Username = user.UserName;
            AddRecipient(user.Email);
        }

        public CampaignEmployee CampaignEmployee { get; private set; }
        public string Username { get; private set; }
        public string Error { get; private set; }
    }
}