﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using Postal;

namespace MasterInstance.Models.ViewModels
{
    public class NotificationEmailBase : Email
    {
        private readonly List<string> _recpients = new List<string>();
        private readonly string _userNameOfSender;

        public NotificationEmailBase(string viewName, string userNameOfSender) : base(viewName)
        {
            _userNameOfSender = userNameOfSender;
        }

        public void AddRecipient(string recipient)
        {
            _recpients.Add(recipient);
        }

        private string EMailAddressOfSender
        {
            get
            {
                var user = Membership.GetUser(_userNameOfSender);
                return user != null ? user.Email : "";
            }
        }

        public string To
        {
            get { return MvcApplication.IsStaging ? EMailAddressOfSender : string.Join(", ", _recpients); }
        }
    }
}