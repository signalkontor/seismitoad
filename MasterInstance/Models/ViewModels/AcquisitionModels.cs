﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Metadata.Edm;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class AcquisitionModel
    {
        public EmployeeProfileViewModel Profile { get; set; }
        public AcquisitionExperience Experience { get; set; }
        public Rating Rating { get; set; }

        [Display(Name = "Durchgeführtes Projekt")]
        public string Project { get; set; }

        [CustomValidation(typeof(AcquisitionModel), "ValidateLocation")]
        [Display(Name = "Standort")]
        public int? LocationId { get; set; }
        public static ValidationResult ValidateLocation(object value, ValidationContext context)
        {
            var model = context.ObjectInstance as AcquisitionModel;

            if (model.LocationId != null && model.Distance == null)
                return new ValidationResult("Bitte geben Sie einen Umkreis zum Standort an!", new[] { "LocationId", "Distance" });

            return ValidationResult.Success;
        }

        [CustomValidation(typeof(AcquisitionModel), "ValidateDistance")]
        [Display(Name = "Umkreis")]
        public int? Distance { get; set; }
        public static ValidationResult ValidateDistance(object value, ValidationContext context)
        {
            var model = context.ObjectInstance as AcquisitionModel;

            if (model.LocationId == null && model.Distance != null)
                return new ValidationResult("Bitte geben Sie einen Standort zum Umkreis an!", new[] { "Distance", "LocationId" });

            return ValidationResult.Success;
        }

        public IEnumerable<EmployeeProfile> Profiles { get; set; }


    }

    //[MetadataType(typeof(AcquisitionExperienceMetadata))]
    public class AcquisitionExperience : Experience
    {
        [UIHint("SingleSelect")]
        [EnumerationType(typeof(ExperienceType))]
        [Display(Name = "Typ")]
        public new int? Type { get; set; }

        //public class AcquisitionExperienceMetadata
        //{
        //    [Visibility(ShowForEdit = false)]
        //    public object Timeframe { get; set; }
        //}
    }
}