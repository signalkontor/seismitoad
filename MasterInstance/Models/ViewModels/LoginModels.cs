﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeismitoadShared.Constants;

namespace MasterInstance.Models.ViewModels
{
    public class LoginViewModel
    {
        public IEnumerable<FriendlyLoginModel> Logins { get; set; }
        public System.Web.Security.MembershipUserCollection Users { get; set; }
    }

    public class FriendlyLoginModel
    {
        private readonly SortedSet<string> _roles;

        public FriendlyLoginModel() { _roles = new SortedSet<string>(); }
        public FriendlyLoginModel(string roles)
        {
            _roles = new SortedSet<string>(roles != null ? roles.Split(';') : new string[0]);
        }

        public string CampaignRoles
        {
            get { return string.Join(";", _roles); }
        }

        private void Set(string role, bool value)
        {
            if (value)
                _roles.Add(role);
            else
                _roles.Remove(role);
        }

        [Display(Name = "Benutzername")]
        public Guid UserKey { get; set; }

        public string FriendlyName { get; set; }

        public int Id { get; set; }

        [UIHint("SimpleBoolean")]
        [Display(Name = "Administrator")]
        public bool IsAdmin
        {
            get { return _roles.Contains(Roles.Admin); }
            set { Set(Roles.Admin, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "Analyse sichtbar")]
        public bool IsAnalyst
        {
            get { return _roles.Contains(Roles.Analyst); }
            set { Set(Roles.Analyst, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "Downloads bearbeiten")]
        public bool IsDownloadsEditor
        {
            get { return _roles.Contains(Roles.DownloadsEditor); }
            set { Set(Roles.DownloadsEditor, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "Downloads sichtbar")]
        public bool IsDownloadsViewer
        {
            get { return _roles.Contains(Roles.DownloadsViewer); }
            set { Set(Roles.DownloadsViewer, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "News bearbeiten")]
        public bool IsNewsEditor
        {
            get { return _roles.Contains(Roles.NewsEditor); }
            set { Set(Roles.NewsEditor, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "News sichtbar")]
        public bool IsNewsReader
        {
            get { return _roles.Contains(Roles.NewsReader); }
            set { Set(Roles.NewsReader, value); }
        }

        [UIHint("SimpleBoolean")]
        [Display(Name = "Reports bearbeiten")]
        public bool IsReportEditor
        {
            get { return _roles.Contains(Roles.ReportEditor); }
            set { Set(Roles.ReportEditor, value); }
        }

        public bool IsApproved { get; set; }
    }
}