using System.Collections.Generic;
using System.Web.Mvc;
using SeismitoadModel;

namespace MasterInstance.Models.ViewModels
{
    public class CheckCreatedAssignmentsModel
    {
        public IEnumerable<AssignedEmployee> AssignedEmployees { get; set; }
        public IEnumerable<Assignment> ConflictingAssignments { get; set; }
        public bool ShowCampaignAndLocationInGrid { get; set; }
        public string FinishUrl { get; set; }
    }
}