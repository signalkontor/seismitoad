﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Postal;

namespace MasterInstance.Models.ViewModels
{
    public class DeferredAssignmentMail : NotificationEmailBase
    {
        public DeferredAssignmentMail(string viewName, string userNameOfSender) : base(viewName, userNameOfSender)
        {
        }

        public string Firstname { get; set; }
        public IEnumerable<string> Assignments { get; set; }
        public List<string> Bcc { get; set; } = new List<string>();
    }
}