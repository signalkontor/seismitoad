﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadShared;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;
using System.Data.SqlClient;

namespace MasterInstance.Models
{
    public class EmployeeProfileViewModel
    {
        private const string PersönlicheDaten = "Persönliche Daten";
        private const string BodyprofilUndTalente = "Bodyprofil und Talente";
        private const string VertragUndGewerbe = "Vertrag und Gewerbe";
        private const string Mobilität = "Mobilität";
        private const string AusbildungUndQualifikation = "Ausbildung und Qualifikation";
        private const string Auftragswunsch = "Auftragswunsch";
        private const string Bewertungen = "Bewertungen";

        private const string NoAdvantageProfile = "Kein altes Profil vorhanden";

        public EmployeeProfileViewModel()
        {
            ReferenceAdvice = NoAdvantageProfile;
            ReferenceGastronomicAbilities = NoAdvantageProfile;
            ReferenceInstallationHand = NoAdvantageProfile;
            ReferenceModel = NoAdvantageProfile;
            ReferenceModeration = NoAdvantageProfile;
            ReferenceMysteryshopper = NoAdvantageProfile;
            ReferenceOther = NoAdvantageProfile;
            ReferenceSampling = NoAdvantageProfile;
            ReferenceTasting = NoAdvantageProfile;
            ReferenceTradeShow = NoAdvantageProfile;
            ReferenceVIP = NoAdvantageProfile;
        }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public int EmployeeId { get; set; }

        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public string Username { get; set; }

        #region A. Persönliche Daten
        [Display(Name = "Anrede*", Description = PersönlicheDaten)]
        [UIHint("SingleSelect")]
        [EnumerationName("Title")]
        [FilterType(FilterType.Equals)]
        public string EmployeeTitle { get; set; }

        [Display(Name = "Vorname*", Description = PersönlicheDaten)]
        public string Firstname { get; set; }

        [Display(Name = "Name*", Description = PersönlicheDaten)]
        public string Lastname { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Geburtstag", Description = PersönlicheDaten)]
        [UIHint("Date")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Alter", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string Age
        {
            get
            {
                var now = DateTime.UtcNow;
                var age = now.Year - Birthday.Year;
                if (now.Month < Birthday.Month || (now.Month == Birthday.Month && now.Day < Birthday.Day))
                {
                    age--;
                }
                return string.Format("{0} Jahre", age);
            }
        }

        //[NotMapped]
        //[Display(Name = "Alter", Description = PersönlicheDaten)]
        //[Visibility(ShowForEditRoles = AdminRoles, ShowForDisplay = false)]
        //[FilterType(FilterType.Between)]
        //public int? AgeFilter { get; set; }


        [Display(Name = "Letzter Login", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public DateTime LastLogin { get; set; }

        [Display(Name = "Letztes Profilupdate", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string LastProfileChange {
            get
            {
                var idAsString = EmployeeId.ToString(CultureInfo.InvariantCulture);
                var tableName = DB.Context.GetTableName<EmployeeProfile>();
                var adminOnlyProps = new[] { "SkeletonContract", "CopyTradeLicense", "FreelancerQuestionaire", "CopyIdentityCard" };
                var newestChangeSet = DB.Context.ChangeSets
                                        .Where(e => e.TableName == tableName && e.EntityId == idAsString && e.Changes.Any(i => !adminOnlyProps.Contains(i.PropName)))
                                        .OrderByDescending(e => e.Date)
                                        .FirstOrDefault();

                return newestChangeSet != null
                    ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                    : "Nie";
            }
        }

        [EmailAddress]
        [Required]
        [Display(Name = "Email 1", Description = PersönlicheDaten)]
        [UIHint("String")]
        public string Email1 { get; set; }

        [EmailAddress]
        [Display(Name = "Email 2", Description = PersönlicheDaten)]
        [UIHint("String")]
        public string Email2 { get; set; }

        [Display(Name = "Skype", Description = PersönlicheDaten)]
        public string Skype { get; set; }

        [Display(Name = "Eigene Website", Description = PersönlicheDaten)]
        public string Website { get; set; }

        [Display(Name = "Fremdpersonal bis", Description = PersönlicheDaten)]
        public DateTime? ExternalUntil { get; set; }

        [Display(Name = "Fremdpersonal", Description = PersönlicheDaten)]
        [Visibility(ShowForEdit = false)]
        public bool IsExternal
        {
            get { return ExternalUntil > LocalDateTime.Now; }
        }

        [Display(Name = "Fremdpersonal", Description = PersönlicheDaten)]
        [UIHint("DisplayOnly")]
        public string IsExternalDisplay
        {
            get { return ExternalUntil > LocalDateTime.Now ? "Ja" : "Nein"; }
        }

        [Display(Name = "Mobil*", Description = PersönlicheDaten)]
        [Format(StartNewColumn = true)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string PhoneMobileNo { get; set; }

        [Display(Name = "Festnetz", Description = PersönlicheDaten)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string PhoneNo { get; set; }

        [Display(Name = "Fax", Description = PersönlicheDaten)]
        [RegularExpression(Other.PhoneNumberRegex, ErrorMessage = Other.PhoneNumberExample)]
        [FilterType(FilterType.Contains)]
        public string FaxNo { get; set; }

        [Display(Name = "Straße*", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string Street { get; set; }

        [Display(Name = "PLZ*", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string PostalCode { get; set; }

        [Display(Name = "Ort*", Description = PersönlicheDaten)]
        [FilterType(FilterType.Contains)]
        public string City { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Land*", Description = PersönlicheDaten)]
        [FilterType(FilterType.Equals)]
        public int? Country { get; set; }

        [Display(Name = "Koordinaten", Description = PersönlicheDaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("DbGeography")]
        public DbGeography GeographicCoordinates { get; set; }

        [Display(Name = "Promoter Status", Description = PersönlicheDaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("EmployeeState")]
        [EnumerationType(typeof(EmployeeState))]
        public EmployeeState State { get; set; }

        [Display(Name = "Status Bemerkung", Description = PersönlicheDaten)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("MultilineText")]
        public string StateComment { get; set; }


        [Visibility(ShowForEdit = false, ShowForDisplay = false)]
        public string ImageBaseUrl { get; set; }
        
        [Display(Name = "Portraitfoto*", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        [Format(StartNewColumn = true)]
        public string DisplayPortraitPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadPortraitPhoto", 120, 120); } }

        [Display(Name = "Ganzkörperfoto", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        public string DisplayFullBodyPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadFullBodyPhoto", 120, 120); } }

        [Display(Name = "Weiteres Foto", Description = PersönlicheDaten)]
        [UIHint("FileDisplay")]
        public string DisplayOtherPhoto { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadOtherPhoto", 120, 120); } }

        [Display(Name = "Offenes Bemerkungsfeld", Description = PersönlicheDaten)]
        [DataType(DataType.MultilineText)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [FilterType(FilterType.Contains)]
        public string PersonalDataComment { get; set; }
        #endregion

        #region B. Body Profil und Talente
        [Range(140, 220)]
        [Display(Name = "Körpergröße (in cm)*", Description = BodyprofilUndTalente)]
        [UIHint("Integer")]
        [FilterType(FilterType.Between)]
        public int? Height { get; set; }

        [Display(Name = "Konfektionsgröße", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        [UIHint("Integer")]
        public int? Size { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "T-Shirt-Größe*", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? ShirtSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeansweite", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? JeansWidth { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Jeanslänge", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Equals)]
        public int? JeansLength { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Schuhgröße", Description = BodyprofilUndTalente)]
        public int? ShoeSize { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Haarfarbe", Description = BodyprofilUndTalente)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Equals)]
        public int? HairColor { get; set; }

        [Display(Name = "Sichtbare Tattoos", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        [FilterType(FilterType.Equals)]
        public bool? VisibleTattoos { get; set; }

        [Display(Name = "Gesichtspiercing/s", Description = BodyprofilUndTalente)]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelect")]
        [FilterType(FilterType.Equals)]
        public bool? FacialPiercing { get; set; }

        [Format(StartNewColumn = true)]
        [UIHint("MultiSelect")]
        [Display(Name = "Künstlerisches", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] ArtisticAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Gastro", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] GastronomicAbilities { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Aktivitäten / Sport", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Sports { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Weitere Talente und Hobbies", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] OtherAbilities { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstiges", Description = BodyprofilUndTalente)]
        [FilterType(FilterType.Contains)]
        public string OtherAbilitiesText { get; set; }

        [Display(Name = "Referenzen aus alter Datenbank", Description = BodyprofilUndTalente)]
        [UIHint("ExtraLabel")]
        [Format(StartNewColumn = true)]
        public string Header { get; set; }

        [Display(Name = "Gastronomie", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceGastronomicAbilities { get; set; }

        [Display(Name = "Sampling", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceSampling { get; set; }

        [Display(Name = "Fachberatung", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceAdvice { get; set; }

        [Display(Name = "Messe/Event", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceTradeShow { get; set; }

        [Display(Name = "Verkostung", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceTasting { get; set; }

        [Display(Name = "Sonstige", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceOther { get; set; }

        [Display(Name = "Moderation", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceModeration { get; set; }

        [Display(Name = "Aufbauhelfer", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceInstallationHand { get; set; }

        [Display(Name = "VIP-Einsatz", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceVIP { get; set; }

        [Display(Name = "Mysteryshopper", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceMysteryshopper { get; set; }

        [Display(Name = "Model", Description = BodyprofilUndTalente)]
        [UIHint("DisplayOnly")]
        public string ReferenceModel { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        [Display(Name = "Rahmenvertrag", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? SkeletonContract { get; set; }

        [Display(Name = "Letzte Änderung", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string LastChangeSkeletonContract { get; set; }

        [Display(Name = "Kopie Gewerbeschein vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? CopyTradeLicense { get; set; }

        [Display(Name = "Letzte Änderung", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string LastChangeCopyTradeLicense { get; set; }

        [Display(Name = "Bist du berechtigt eigene Rechnungen zu stellen?*", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? ValidTradeLicense { get; set; }

        [Display(Name = "Finanzamt, Sitz/Ort", Description = VertragUndGewerbe)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Contains)]
        public string TaxOfficeLocation { get; set; }

        [Display(Name = "Steuernummer", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Contains)]
        public string TaxNumber { get; set; }

        [Display(Name = "Ust-abzugsfähig?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? TurnoverTaxDeductible { get; set; }

        [Display(Name = "Fragebogen zur Selbstständigkeit vorhanden", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? FreelancerQuestionaire { get; set; }

        [Display(Name = "Letzte Änderung", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string LastChangeFreelancerQuestionaire { get; set; }

        [Display(Name = "Gesundheitszeugnis", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? HealthCertificate { get; set; }

        [Display(Name = "Kopie Gesundheitszeugnis vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        public bool? CopyHealthCertificate { get; set; }

        [Display(Name = "Kopie Personalausweis vorhanden?", Description = VertragUndGewerbe)]
        [FilterType(FilterType.Equals)]
        [UIHint("SingleSelect")]
        public int? CopyIdentityCard { get; set; }

        [Display(Name = "Letzte Änderung", Description = VertragUndGewerbe)]
        [UIHint("DisplayOnly")]
        public string LastChangeCopyIdentityCard { get; set; }

        [Display(Name = "Gewerbeschein", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        [Format(StartNewColumn = true)]
        public string DisplayTradeLicense { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadTradeLicense", 120, 120); } }

        [Display(Name = "Gesundheitszeugnis", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        public string DisplayHealthCertificate { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadHealthCertificate", 120, 120); } }

        [Display(Name = "Personalausweis Vorderseite (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        public string UploadIdentityCardFront { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadIdentityCardFront", 120, 120); } }

        [Display(Name = "Personalausweis Rückseite (jpg, png, gif, pdf)", Description = VertragUndGewerbe)]
        [UIHint("FileDisplay")]
        public string UploadIdentityCardBack { get { return Helpers.GetImageUrl(ImageBaseUrl, "UploadIdentityCardBack", 120, 120); } }

        [Display(Name = "Bemerkungen", Description = VertragUndGewerbe)]
        [DataType(DataType.MultilineText)]
        [FilterType(FilterType.Contains)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        public string Comment { get; set; }
        #endregion

        #region D. Mobilität
        [Display(Name = "Führerschein vorhanden*", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? DriversLicense { get; set; }

        [UIHint("Date")]
        [Display(Name = "Führerschein seit", Description = Mobilität)]
        public DateTime? DriversLicenseSince { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (neue Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass1 { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Führerscheinklasse (alte Klassen)", Description = Mobilität)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] DriversLicenseClass2 { get; set; }

        [Display(Name = "Bemerkung Führerschein/Fahrpraxis", Description = Mobilität)]
        [Visibility(ShowForDisplay = false, ShowForEdit = true)]
        [UIHint("MultilineText")]
        public string DriversLicenseRemark { get; set; }

        [Display(Name = "Digitale Fahrerkarte vorhanden", Description = Mobilität)]
        [Format(StartNewColumn = true)]
        [FilterType(FilterType.Equals)]
        public bool? HasDigitalDriversCard { get; set; }

        [Display(Name = "PKW vorhanden", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? IsCarAvailable { get; set; }

        [Display(Name = "Bahncard vorhanden", Description = Mobilität)]
        [FilterType(FilterType.Equals)]
        public bool? IsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        [UIHint("SingleSelect")]
        [Display(Name = "Schulbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? Education { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Abgeschlossene Berufsausbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.AnyOrAll)]
        public string[] Apprenticeships { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Fachrichtung/Abschluss Berufsausbildung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string OtherCompletedApprenticeships { get; set; }

        [Display(Name = "Studium", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Equals)]
        [Toggles(".sk-toggle-1")]
        public bool? Studies { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Fachrichtung/Abschluss Studium", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        [Format(EditorLabelHtmlAttributesIndex = HtmlAttributes.SkToggle1, EditorFieldHtmlAttributesIndex = HtmlAttributes.SkToggle1)]
        public string StudiesDetails { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Berufserfahrung", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string WorkExperience { get; set; }

        [Display(Name = "Deutsch*", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesGerman { get; set; }

        [Display(Name = "Englisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesEnglish { get; set; }

        [Display(Name = "Spanisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesSpanish { get; set; }

        [Display(Name = "Französisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesFrench { get; set; }

        [Display(Name = "Italienisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesItalian { get; set; }

        [Display(Name = "Türkisch", Description = AusbildungUndQualifikation)]
        [UIHint("SingleSelect")]
        [EnumerationName("LanguageKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? LanguagesTurkish { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Sonstige", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string LanguagesOther { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Software-Kentnisse*", Description = AusbildungUndQualifikation)]
        [Format(StartNewColumn = true)]
        [EnumerationName("ITKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? SoftwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string SoftwareKnownledgeDetails { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Hardware-Kentnisse*", Description = AusbildungUndQualifikation)]
        [EnumerationName("ITKnowledge")]
        [FilterType(FilterType.GreaterThanOrEqual)]
        public int? HardwareKnownledge { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Welche?", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string HardwareKnownledgeDetails { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Schulungen intern", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string InternalTraining { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Schulungen extern", Description = AusbildungUndQualifikation)]
        [FilterType(FilterType.Contains)]
        public string ExternalTraining { get; set; }

        [UIHint("Zeugnisse")]
        [Format(StartNewColumn = true)]
        [Display(Name = "Zeugnisse", Description = AusbildungUndQualifikation)]
        public string[] Zeugnisse { get; set; }

        [UIHint("SingleSelect")]
        [Display(Name = "Eignung als Teamleiter", Description = AusbildungUndQualifikation)]
        [EnumerationName("YesNo")]
        public bool? TeamleaderQualification { get; set; }
        #endregion

        #region G. Auftragswunsch
        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Promotion Aktivitäten", Description = Auftragswunsch)]
        public string[] PreferredTasks { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Promotionarten", Description = Auftragswunsch)]
        public string[] PreferredTypes { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Bevorzugte Arbeitszeiten", Description = Auftragswunsch)]
        public string[] PreferredWorkSchedule { get; set; }

        [Display(Name = "Reisebereitschaft", Description = Auftragswunsch)]
        [Format(StartNewColumn = true)]
        public bool? TravelWillingness { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "Übernachtung möglich im Großraum", Description = Auftragswunsch)]
        public string[] AccommodationPossible1 { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Übernachtung möglich in folgenden Städten", Description = Auftragswunsch)]
        public string AccommodationPossible2 { get; set; }
        #endregion

        #region Bewertungen
        [Display(Name = "Bewertungen", Description = Bewertungen)]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [UIHint("Ratings")]
        [HideSurroundingHtml]
        public int? Ratings => EmployeeId;

        #endregion

        #region Persönlichkeitstest
        [Visibility(ShowForDisplay = true, ShowForEdit = true)]
        [UIHint("PersonalityTest")]
        [HideSurroundingHtml]
        [Display(Name = "Persönlichkeitstest", Description = "Persönlichkeitstest")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        public string PersonalityTestAnswers { get; set; }
        #endregion

        #region Promotionerfahrung
        [Display(Name = "Referenzen", Description = "Referenzen")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [UIHint("Experiences")]
        [HideSurroundingHtml]
        public int Experiences => EmployeeId;
        #endregion

        #region Durchgeführte Projekte
        [Display(Name = "Projektliste", Description = "Projektliste")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [UIHint("Projects")]
        [HideSurroundingHtml]
        public int Projects => EmployeeId;

        #endregion

        #region Besuchte Schulungen
        [Display(Name = "Schulungsliste", Description = "Schulungsliste")]
        [Format(StartNewColumn = true, ColumnHtmlAttributesIndex = HtmlAttributes.FullWidthColumnIndex)]
        [UIHint("Trainings")]
        [HideSurroundingHtml]
        public int Trainings => EmployeeId;

        #endregion
    }

    public class EmployeeProfileViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            #region Employee <=> EmployeeProfileViewModel
            Mapper.CreateMap<Employee, EmployeeProfileViewModel>()
                .ForSourceMember(e => e.Profile, opt => opt.Ignore())
                .ForMember(e => e.EmployeeId, opt => opt.MapFrom(e => e.Id))
                .ForMember(e => e.EmployeeTitle, opt => opt.MapFrom(i => i.Title));

            Mapper.CreateMap<EmployeeProfileViewModel, Employee>()
                .ForMember(e => e.Experiences, opt => opt.Ignore())
                .ForMember(e => e.Ratings, opt => opt.Ignore())
                .ForMember(e => e.Title, opt => opt.MapFrom(i => i.EmployeeTitle)); ;
            #endregion

            #region EmployeeProfile <=> EmployeeProfileViewModel

            Mapper.CreateMap<EmployeeProfile, EmployeeProfileViewModel>()
                .ForMember(e => e.EmployeeId, opt => opt.Ignore())
                .ForMember(e => e.Comment, opt => opt.MapFrom(e => e.Comment))
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeSplit(';')))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeSplit(';')))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeSplit(';')))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeSplit(';')))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeSplit(';')))
                .ForMember(e => e.Apprenticeships, opt => opt.ResolveUsing(e => e.Apprenticeships.SafeSplit(';')))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeSplit(';')))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeSplit(';')))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeSplit(';')))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeSplit(';')))
                .ForMember(e => e.LastProfileChange, opt => opt.ResolveUsing((EmployeeProfile e) =>
                {
                    var idAsString = e.Id.ToString(CultureInfo.InvariantCulture);
                    var tableName = DB.Context.GetTableName<EmployeeProfile>();

                    var newestChangeSet = DB.Context.Database.SqlQuery<ChangeSet>(@"
SELECT * FROM ChangeSets
WHERE TableName = @tableName AND EntityId = @entityId AND EXISTS (SELECT * FROM Changes WHERE Changes.ChangeSet_Id = ChangeSets.Id AND Changes.PropName NOT IN ('SkeletonContract', 'CopyTradeLicense', 'FreelancerQuestionaire', 'CopyIdentityCard'))
ORDER BY Date DESC", new SqlParameter("@tableName", tableName), new SqlParameter("@entityId", idAsString))
                        .FirstOrDefault();
                    return newestChangeSet != null
                        ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                        : "Nie";
                }))                  
                .ForMember(e => e.LastChangeSkeletonContract, opt => opt.ResolveUsing((EmployeeProfile e) =>
                {
                    var idAsString = e.Id.ToString(CultureInfo.InvariantCulture);
                    var tableName = DB.Context.GetTableName<EmployeeProfile>();
                    
                    var newestChangeSet = DB.Context.Database.SqlQuery<ChangeSet>(@"
SELECT * FROM ChangeSets
WHERE TableName = @tableName AND EntityId = @entityId AND EXISTS (SELECT * FROM Changes WHERE Changes.ChangeSet_Id = ChangeSets.Id AND Changes.PropName = @propName)
ORDER BY Date DESC", new SqlParameter("@tableName", tableName), new SqlParameter("@entityId", idAsString), new SqlParameter("@propName", "SkeletonContract"))
                        .FirstOrDefault();
                    return newestChangeSet != null
                        ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                        : "Nie";
                }))                  
                .ForMember(e => e.LastChangeCopyTradeLicense, opt => opt.ResolveUsing((EmployeeProfile e) =>
                {
                    var idAsString = e.Id.ToString(CultureInfo.InvariantCulture);
                    var tableName = DB.Context.GetTableName<EmployeeProfile>();
                    
                    var newestChangeSet = DB.Context.Database.SqlQuery<ChangeSet>(@"
SELECT * FROM ChangeSets
WHERE TableName = @tableName AND EntityId = @entityId AND EXISTS (SELECT * FROM Changes WHERE Changes.ChangeSet_Id = ChangeSets.Id AND Changes.PropName = @propName)
ORDER BY Date DESC", new SqlParameter("@tableName", tableName), new SqlParameter("@entityId", idAsString), new SqlParameter("@propName", "CopyTradeLicense"))
                        .FirstOrDefault();
                    return newestChangeSet != null
                        ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                        : "Nie";
                }))   
                .ForMember(e => e.LastChangeFreelancerQuestionaire, opt => opt.ResolveUsing((EmployeeProfile e) =>
                {
                    var idAsString = e.Id.ToString(CultureInfo.InvariantCulture);
                    var tableName = DB.Context.GetTableName<EmployeeProfile>();
                    
                    var newestChangeSet = DB.Context.Database.SqlQuery<ChangeSet>(@"
SELECT * FROM ChangeSets
WHERE TableName = @tableName AND EntityId = @entityId AND EXISTS (SELECT * FROM Changes WHERE Changes.ChangeSet_Id = ChangeSets.Id AND Changes.PropName = @propName)
ORDER BY Date DESC", new SqlParameter("@tableName", tableName), new SqlParameter("@entityId", idAsString), new SqlParameter("@propName", "FreelancerQuestionaire"))
                        .FirstOrDefault();
                    return newestChangeSet != null
                        ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                        : "Nie";
                }))                
                .ForMember(e => e.LastChangeCopyIdentityCard, opt => opt.ResolveUsing((EmployeeProfile e) =>
                {
                    var idAsString = e.Id.ToString(CultureInfo.InvariantCulture);
                    var tableName = DB.Context.GetTableName<EmployeeProfile>();
                    
                    var newestChangeSet = DB.Context.Database.SqlQuery<ChangeSet>(@"
SELECT * FROM ChangeSets
WHERE TableName = @tableName AND EntityId = @entityId AND EXISTS (SELECT * FROM Changes WHERE Changes.ChangeSet_Id = ChangeSets.Id AND Changes.PropName = @propName)
ORDER BY Date DESC", new SqlParameter("@tableName", tableName), new SqlParameter("@entityId", idAsString), new SqlParameter("@propName", "CopyIdentityCard"))
                        .FirstOrDefault();
                    return newestChangeSet != null
                        ? $"am {newestChangeSet.Date:dd.MM.yyyy HH:mm:ss} durch {Membership.GetUser(newestChangeSet.UserId)?.UserName ?? "(gelöschter User)"}"
                        : "Nie";
                }));

            Mapper.CreateMap<EmployeeProfileViewModel, EmployeeProfile>()
                .ForMember(e => e.PersonalityTestAnswers, opt => opt.Ignore())
                .ForMember(e => e.Comment, opt => opt.MapFrom(e => e.Comment))
                .ForMember(e => e.ArtisticAbilities, opt => opt.ResolveUsing(e => e.ArtisticAbilities.SafeJoin(";")))
                .ForMember(e => e.GastronomicAbilities, opt => opt.ResolveUsing(e => e.GastronomicAbilities.SafeJoin(";")))
                .ForMember(e => e.Sports, opt => opt.ResolveUsing(e => e.Sports.SafeJoin(";")))
                .ForMember(e => e.OtherAbilities, opt => opt.ResolveUsing(e => e.OtherAbilities.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass1, opt => opt.ResolveUsing(e => e.DriversLicenseClass1.SafeJoin(";")))
                .ForMember(e => e.DriversLicenseClass2, opt => opt.ResolveUsing(e => e.DriversLicenseClass2.SafeJoin(";")))
                .ForMember(e => e.Apprenticeships, opt => opt.ResolveUsing(e => e.Apprenticeships.SafeJoin(";")))
                .ForMember(e => e.PreferredTasks, opt => opt.ResolveUsing(e => e.PreferredTasks.SafeJoin(";")))
                .ForMember(e => e.PreferredTypes, opt => opt.ResolveUsing(e => e.PreferredTypes.SafeJoin(";")))
                .ForMember(e => e.PreferredWorkSchedule, opt => opt.ResolveUsing(e => e.PreferredWorkSchedule.SafeJoin(";")))
                .ForMember(e => e.AccommodationPossible1, opt => opt.ResolveUsing(e => e.AccommodationPossible1.SafeJoin(";")));
            #endregion

            #region MembershipUser => EmployeeProfileViewModel
            Mapper.CreateMap<MembershipUser, EmployeeProfileViewModel>()
                // Wir ignorieren hier mal Comment, da wir im EmployeeProfile auch noch ein Comment Property haben, welches sonst nicht gemappt würde.
                .ForMember(e => e.Comment, opt => opt.Ignore())
                .ForMember(e => e.Username, opt => opt.MapFrom(e => e.UserName))
                .ForMember(e => e.Email1, opt => opt.MapFrom(e => e.Email))
                .ForMember(e => e.LastLogin, opt => opt.MapFrom(e => e.LastLoginDate));
            #endregion

            #region doa_login => EmployeeProfileViewModel
            Mapper.CreateMap<AdvantageModel.Models.doa_login, EmployeeProfileViewModel>()
                            .ForMember(e => e.ReferenceGastronomicAbilities, opt => opt.MapFrom(e => e.doa_qualifikation.gastro))
                            .ForMember(e => e.ReferenceSampling, opt => opt.MapFrom(e => e.doa_qualifikation.sampling))
                            .ForMember(e => e.ReferenceAdvice, opt => opt.MapFrom(e => e.doa_qualifikation.fachberatung))
                            .ForMember(e => e.ReferenceTradeShow, opt => opt.MapFrom(e => e.doa_qualifikation.messe_event))
                            .ForMember(e => e.ReferenceTasting, opt => opt.MapFrom(e => e.doa_qualifikation.verkostung))
                            .ForMember(e => e.ReferenceOther, opt => opt.MapFrom(e => e.doa_qualifikation.sonstige))
                            .ForMember(e => e.ReferenceModeration, opt => opt.MapFrom(e => e.doa_qualifikation.moderation))
                            .ForMember(e => e.ReferenceInstallationHand, opt => opt.MapFrom(e => e.doa_qualifikation.aufbauhelfer))
                            .ForMember(e => e.ReferenceVIP, opt => opt.MapFrom(e => e.doa_qualifikation.vip_einsatz))
                            .ForMember(e => e.ReferenceMysteryshopper, opt => opt.MapFrom(e => e.doa_qualifikation.mysteryshopper))
                            .ForMember(e => e.ReferenceModel, opt => opt.MapFrom(e => e.doa_qualifikation.model));
            #endregion

        }
    }
}