﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Models
{
    public class CampaignEmployeeMail
    {
        public string JobId { get; set; }
        public string MailType { get; set; }
        public int CampaignId { get; set; }
        public int EmployeeId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public bool Done { get; set; }
        public DateTime QueuedAt { get; set; }
        public string Error { get; set; }
    }
}