﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Models
{
    /// <summary>
    /// Je 5 Unterkategorien zu den Kategorien Engagement, Zuverlässigkeit, Belastbarkeit, Teamfähigkeit, Kundendialog, Erscheinug definieren
    /// Es kann Projekte geben, bei denen nicht alle Kriterien ausgefüllt werden müssen.
    /// Je Kategorie erhält der Promotor dann die Durchschnittsnote all seiner Bewertungen.
    /// Bewertungen werden über den Selftest, Misterie-Shopper, PER Media durchgeführt
    ///
    /// Alle Kennzahlen werden in einer gesonderten Tabelle geschrieben
    /// Es wird eine Verlinkung zum Projekt gespeichert
    /// Es kann noch ein Kommentar hinzugefügt werden
    /// </summary>
    public class RatingViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Campaign { get; set; }
        public decimal? Appearance { get; set; }
        public decimal? Customerdialog { get; set; }
        public decimal? KnowledgeAndSales { get; set; }
        public decimal? Engagement { get; set; }
        public decimal? Reliability { get; set; }
        public decimal? Teamwork { get; set; }
        public decimal? Toughness { get; set; }
        [EnumerationName("RatingType")]
        public int RatingType { get; set; }
    }

    public class RatingViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Rating, RatingViewModel>()
                .ForMember(e => e.Campaign, opt => opt.MapFrom(e => e.RatingType == 1 ? "Selftest" : e.Campaign.Name))
                ;
            Mapper.CreateMap<RatingViewModel, Rating>();
        }
    }

    public class RatingDetailsViewModel
    {
        public decimal? Appearance01 { get; set; }
        public decimal? Appearance02 { get; set; }
        public decimal? Appearance03 { get; set; }
        public decimal? Appearance04 { get; set; }
        public decimal? Appearance05 { get; set; }
        public decimal? Appearance06 { get; set; }
        public decimal? Appearance07 { get; set; }
        public decimal? Appearance08 { get; set; }
        public decimal? Appearance09 { get; set; }
        public decimal? CustomerDialog01 { get; set; }
        public decimal? CustomerDialog02 { get; set; }
        public decimal? CustomerDialog03 { get; set; }
        public decimal? CustomerDialog04 { get; set; }
        public decimal? CustomerDialog05 { get; set; }
        public decimal? CustomerDialog06 { get; set; }
        public decimal? CustomerDialog07 { get; set; }
        public decimal? CustomerDialog08 { get; set; }
        public decimal? CustomerDialog09 { get; set; }
        public decimal? CustomerDialog10 { get; set; }
        public decimal? KnowledgeAndSales01 { get; set; }
        public decimal? KnowledgeAndSales02 { get; set; }
        public decimal? KnowledgeAndSales03 { get; set; }
        public decimal? KnowledgeAndSales04 { get; set; }
        public decimal? KnowledgeAndSales05 { get; set; }
        public decimal? KnowledgeAndSales06 { get; set; }
        public decimal? KnowledgeAndSales07 { get; set; }
        public decimal? KnowledgeAndSales08 { get; set; }
        public decimal? KnowledgeAndSales09 { get; set; }
        public decimal? KnowledgeAndSales10 { get; set; }
        public decimal? Engagement01 { get; set; }
        public decimal? Engagement02 { get; set; }
        public decimal? Engagement03 { get; set; }
        public decimal? Engagement04 { get; set; }
        public decimal? Engagement05 { get; set; }
        public decimal? Engagement06 { get; set; }
        public decimal? Engagement07 { get; set; }
        public decimal? Reliability01 { get; set; }
        public decimal? Reliability02 { get; set; }
        public decimal? Reliability03 { get; set; }
        public decimal? Reliability04 { get; set; }
        public decimal? Reliability05 { get; set; }
        public decimal? Reliability06 { get; set; }
        public decimal? Reliability07 { get; set; }
        public decimal? Reliability08 { get; set; }
        public decimal? Reliability09 { get; set; }
        public decimal? Reliability10 { get; set; }
        public decimal? Reliability11 { get; set; }
        public decimal? Reliability12 { get; set; }
        public decimal? Teamwork01 { get; set; }
        public decimal? Teamwork02 { get; set; }
        public decimal? Teamwork03 { get; set; }
        public decimal? Teamwork04 { get; set; }
        public decimal? Teamwork05 { get; set; }
        public decimal? Teamwork06 { get; set; }
        public decimal? Teamwork07 { get; set; }
        public decimal? Teamwork08 { get; set; }
        public decimal? Teamwork09 { get; set; }
        public decimal? Toughness01 { get; set; }
        public decimal? Toughness02 { get; set; }
        public decimal? Toughness03 { get; set; }
        public bool? Teamleader01 { get; set; }
        public bool? Teamleader02 { get; set; }
        public bool? Teamleader03 { get; set; }
        public bool? Teamleader04 { get; set; }
        public string AppearenceRemark01 { get; set; }
        public string AppearenceRemark02 { get; set; }
        public string CustomerDialogRemark01 { get; set; }
        public string CustomerDialogRemark02 { get; set; }
        public string KnowledgeAndSalesRemark01 { get; set; }
        public string KnowledgeAndSalesRemark02 { get; set; }
        public string EngagementRemark01 { get; set; }
        public string EngagementRemark02 { get; set; }
        public string ReliabilityRemark01 { get; set; }
        public string ReliabilityRemark02 { get; set; }
        public string ReliabilityRemark03 { get; set; }
        public string TeamworkRemark01 { get; set; }
        public string TeamworkRemark02 { get; set; }
        public string TeamleaderRemark01 { get; set; }
        public string ToughnessRemark01 { get; set; }
        public string PositiveAttributes { get; set; }
        public string PossibleImprovements { get; set; }
    }
}
