﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Models
{
    public class ExperienceViewModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Typ")]
        public ExperienceType Type { get; set; }
        
        [Display(Name = "Zeitraum")]
        public string Timeframe { get; set; }

        [Display(Name = "Branche")]
        public int Sector { get; set; }

        [Display(Name = "Gattung")]
        public int SectorDetails { get; set; }

        [Display(Name = "Tätigkeit")]
        public int Task { get; set; }

        [Display(Name = "Sonstige Tätigkeit")]
        public string OtherTask { get; set; }
        
        [Display(Name = "Aktionstitel")]
        public string Title { get; set; }
        
        [Display(Name = "Produkt")]
        public string Product { get; set; }
        
        [Display(Name = "Marke/Hersteller")]
        public string Brand { get; set; }
        
        [Display(Name = "Agentur")]
        public string Agency { get; set; }
    }

    public class ExperienceViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Experience, ExperienceViewModel>();
        }
    }
}