﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SeismitoadModel;

namespace MasterInstance.Extensions
{
    public static class MembershipUserExtensions
    {
        public static MembershipUser GetMemberShipUser(this Employee employee)
        {
            return Membership.GetUser(employee.ProviderUserKey);
        }
    }
}