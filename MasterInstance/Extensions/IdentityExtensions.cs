﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SeismitoadModel;
using SeismitoadShared.Models.ViewModels;

namespace MasterInstance.Extensions
{
    public static class IdentityExtensions
    {
        private static bool IsUserInRole(IIdentity identity, string role)
        {
            return Roles.Provider.IsUserInRole(identity.Name, role);
        }

        public static IQueryable<Campaign> GetCampaigns(this IIdentity identity)
        {
            var userkey = (Guid)Membership.GetUser(identity.Name).ProviderUserKey;
            var isAdmin = Roles.IsUserInRole(identity.Name, SeismitoadShared.Constants.Roles.Admin);
            return DB.Context.Campaigns.Where(e => isAdmin || e.Logins.Any(i => i.UserKey == userkey));
        }

        public static IEnumerable<SkMenuItem> GetMenu(this IIdentity identity, MenuArea menuArea)
        {
            var httpRequest = HttpContext.Current.Request;
            var menu = new List<SkMenuItem>();
            var isAdmin = IsUserInRole(identity, SeismitoadShared.Constants.Roles.Admin);
#if ADVANTAGE
            var isPermedia = IsUserInRole(identity, SeismitoadShared.Constants.Roles.Permedia);
            var isAdvantage = IsUserInRole(identity, SeismitoadShared.Constants.Roles.Advantage);
#else
            var isPermedia = true;
            var isAdvantage = true;
#endif

            #region Home
            // ReSharper disable UseObjectOrCollectionInitializer
            var homeItems = new List<SkSubMenuItem>();
// ReSharper restore UseObjectOrCollectionInitializer
            homeItems.Add(new SkSubMenuItem { Name = "Zur Startseite", RouteValueDictionary = MVC.Home.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.filesystems.folder_home_png });
            homeItems.Add(identity.IsAuthenticated
                              ? new SkSubMenuItem { Name = "Abmelden", RouteValueDictionary = MVC.Account.LogOff().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.exit_png }
                              : new SkSubMenuItem { Name = "Anmelden", RouteValueDictionary = MVC.Account.LogOn().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.logout_png });
            menu.Add(new SkMenuItem { Name = "Home", Items = homeItems });
#endregion

            if (!identity.IsAuthenticated)
                return menu;

#region Aktionen
            var campaignGenericItems = new List<SkSubMenuItem>
            {
                new SkSubMenuItem { Name = "Aktion anlegen", RouteValueDictionary = MVC.CampaignWizard.Step(-1, 1).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.bookmark_add_png },
                new SkSubMenuItem { Name = "Aktionen verwalten", RouteValueDictionary = MVC.Campaign.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.kfm_png }
            };
            menu.Add(new SkMenuItem { Name = "Aktionen", Items = campaignGenericItems });

            var campaigns = identity.GetCampaigns()
                .Where(e => e.State < CampaignState.Archived)
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    CustomerName = e.Customer.Name,
                })
                .GroupBy(e => e.CustomerName)
                .OrderBy(e => e.Key)
                .ToList();

            foreach (var campaign in campaigns)
            {
                var campaignItems = new List<SkSubMenuItem>();
                campaignItems.AddRange(campaign
                    .OrderBy(e => e.Name)
                    .Select(e => new SkSubMenuItem
                    {
                        Name = e.Name,
                        RouteValueDictionary =
                            (isAdvantage || isAdmin ? MVC.CM.Dashboard.Index(e.Id) : MVC.Campaign.Link(e.Id))
                                .GetRouteValueDictionary(),
                        Image = Links.Content.icons._16x16.actions.bookmark_png
                    }));
                menu.Add(new SkMenuItem { Name = campaign.Key, Items = campaignItems, SaveState = true});
            }           

#endregion

            switch(menuArea)
            {
                case MenuArea.Default:
#region Orte
                    if (isAdmin)
                    {
                        var locationItems = new List<SkSubMenuItem>();
                        locationItems.Add(new SkSubMenuItem { Name = "Ort erstellen", RouteValueDictionary = MVC.Location.Create().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.web_png });
                        locationItems.Add(new SkSubMenuItem { Name = "Orte verwalten", RouteValueDictionary = MVC.MD.Location.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.web_png });
                        locationItems.Add(new SkSubMenuItem { Name = "Ortstypen verwalten", RouteValueDictionary = MVC.MD.LocationType.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.web_png });
                        locationItems.Add(new SkSubMenuItem { Name = "Channels verwalten", RouteValueDictionary = MVC.MD.LocationGroup.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.web_png });
                        menu.Add(new SkMenuItem { Name = "Orte", Items = locationItems });
                    }
#endregion

#region Promoter / Benutzer
                    var employeeItems = new List<SkSubMenuItem>();
                    employeeItems.Add(new SkSubMenuItem { Name = "Promoter verwalten", RouteValueDictionary = MVC.MD.Employee.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.agt_family_png });
                    employeeItems.Add(new SkSubMenuItem { Name = "Promoteranwesenheit", RouteValueDictionary = MVC.CM.Attendance.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions._1day_png });
                    if (isAdmin)
                    {
                        employeeItems.Add(new SkSubMenuItem { Name = "Mögliche Dubletten", RouteValueDictionary = MVC.Migration.Duplicates.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.no_png });
                        employeeItems.Add(new SkSubMenuItem { Name = "Bestätigungsemails", RouteValueDictionary = MVC.Admin.Confirmations.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.mimetypes.txt_png});
                    }
                    employeeItems.Add(new SkSubMenuItem { Name = "Eingegangene Faxe", RouteValueDictionary = MVC.CM.FaxMessages.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.kfax_png });
                    employeeItems.Add(new SkSubMenuItem { Name = "Schulungen", RouteValueDictionary = MVC.MD.Training.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.kspread_png});

                    if (isAdmin)
                    {
                        employeeItems.Add(new SkSubMenuItem { Name = "Benutzer verwalten", RouteValueDictionary = MVC.Admin.User.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.agt_family_png });
                    }
                    menu.Add(new SkMenuItem { Name = "Promoter / Benutzer", Items = employeeItems });
#endregion

#region Kunden
                    if (isAdmin)
                    {
                        var customerItems = new List<SkSubMenuItem>();
                        customerItems.Add(new SkSubMenuItem { Name = "Kunden verwalten", RouteValueDictionary = MVC.Customer.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.agt_family_png });
                        menu.Add(new SkMenuItem { Name = "Kunden", Items = customerItems });
                    }
#endregion
                    break;

                case MenuArea.Project:
                    int? wizardId = null;
                    if (httpRequest.RequestContext.RouteData.Values["Controller"].ToString() == "CampaignWizard")
                    {
                        wizardId = Convert.ToInt32(httpRequest.RequestContext.RouteData.Values["campaignId"]);
                    }
                    var id = wizardId ?? Convert.ToInt32(httpRequest.RequestContext.RouteData.Values["id"] ?? httpRequest.QueryString.Get("campaignId"));
                    var campaign = DB.Context.Campaigns
                        .Where(e => e.Id == id)
                        .Select(e => new
                        {
                            e.Name,
                            HasAssignments = e.CampaignLocations.Any(i => i.Assignments.Any())
                        })
                        .SingleOrDefault();

                    if (campaign == null)
                    {
                        menu.Add(new SkMenuItem { Name = "Neue Aktion" });
                        break;
                    }

                    var items = new List<SkSubMenuItem>
                                    {
                                        new SkSubMenuItem {
                                            Name = "Dashboard",
                                            RouteValueDictionary = (isAdmin || isAdvantage 
                                                    ? MVC.CM.Dashboard.Index(id)
                                                    : MVC.Campaign.Link(id)
                                                ).GetRouteValueDictionary(),
                                                Image = Links.Content.icons._16x16.actions.chart_png },
                                        new SkSubMenuItem { Name = "Statistiken", RouteValueDictionary = MVC.CM.Statistics.Index(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.chart_png },
                                        new SkSubMenuItem { Name = "Projektdaten", RouteValueDictionary = MVC.Campaign.Edit(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.db_png },
                                        new SkSubMenuItem { Name = "Personalanforderung", RouteValueDictionary = MVC.CM.StaffRequirement.Index(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.db_png },
                                    };

                    if (isAdmin || isAdvantage)
                    {
                        items.Add(new SkSubMenuItem
                        {
                            Name = "Projektaufträge",
                            RouteValueDictionary = MVC.CM.Contract.Index(id).GetRouteValueDictionary(),
                            Image = Links.Content.icons._16x16.actions.db_png
                        });
                    }

                    var isCampaignAdmin = isAdmin;
                    if (!isCampaignAdmin)
                    {
                        var userkey = (Guid)Membership.GetUser(identity.Name).ProviderUserKey;
                        isCampaignAdmin =
                            DB.Context.Logins.Any(
                                e =>
                                    e.CampaignId == id && e.UserKey == userkey &&
                                    e.CampaignRoles.Contains(SeismitoadShared.Constants.Roles.Admin));
                    }

                    if (isCampaignAdmin)
                    {
                        items.Add(new SkSubMenuItem
                            {
                                Name = "Berechtigungen",
                                RouteValueDictionary = MVC.Campaign.Authorization(id).GetRouteValueDictionary(),
                                Image = Links.Content.icons._16x16.actions.agt_family_png
                            });
                    }
                    if (isAdvantage || isAdmin)
                    {
                        items.Add(new SkSubMenuItem
                        {
                            Name = "Akquiseliste",
                            RouteValueDictionary = MVC.CM.Acquisition.Index(id).GetRouteValueDictionary(),
                            Image = Links.Content.icons._16x16.actions.agt_family_png
                        });
                    }
                    items.Add(new SkSubMenuItem
                    {
                        Name = "Personalliste",
                        RouteValueDictionary = MVC.CM.Promoter.Index(id).GetRouteValueDictionary(),
                        Image = Links.Content.icons._16x16.actions.agt_family_png
                    });
                    items.Add(new SkSubMenuItem { Name = "Personalliste exportieren", RouteValueDictionary = MVC.CM.Export.PromoterList(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.db_png });

                    if (isPermedia || isAdmin)
                    {
                        items.Add(new SkSubMenuItem
                        {
                            Name = "Aktionsorte",
                            RouteValueDictionary = MVC.CM.CampaignLocation.Index(id).GetRouteValueDictionary(),
                            Image = Links.Content.icons._16x16.apps.web_png
                        });
                    }
                    items.Add(new SkSubMenuItem
                    {
                        Name = "Aktionstage erstellen",
                        RouteValueDictionary = MVC.Assignment.Index(id, null).GetRouteValueDictionary(),
                        Image = Links.Content.icons._16x16.actions._1day_png
                    });
                    items.Add(new SkSubMenuItem
                    {
                        Name = "Aktionstage zuweisen",
                        RouteValueDictionary = MVC.CM.Assign.Index(id).GetRouteValueDictionary(),
                        Image = Links.Content.icons._16x16.actions._1day_png
                    });
                    
                    if(campaign.HasAssignments)
                    {
                        if (isAdvantage)
                        {
                            items.Add(new SkSubMenuItem
                            {
                                Name = "Aktionstage löschen",
                                RouteValueDictionary =
                                    MVC.CM.DeleteAssignments.Index()
                                        .AddRouteValue("campaignId", id)
                                        .GetRouteValueDictionary(),
                                Image = Links.Content.icons._16x16.actions.delete_table_png
                            });
                        }
                        items.Add(new SkSubMenuItem { Name = "Aktionstage exportieren", RouteValueDictionary = MVC.CM.Export.Assignments(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.db_png });
                        items.Add(new SkSubMenuItem { Name = "Promoteranwesenheit", RouteValueDictionary = MVC.CM.Attendance.Campaign(id).GetRouteValueDictionary(), Image = Links.Content.icons._16x16.actions.agt_family_png });
                    }
                    items.Add(
                        new SkSubMenuItem
                        {
                            Name = "Schulungen",
                            RouteValueDictionary = MVC.MD.Training.Trainings(id).GetRouteValueDictionary(),
                            Image = Links.Content.icons._16x16.apps.kspread_png
                        });
                    menu.Add(new SkMenuItem { Name = campaign.Name, Items = items });
                    break;
            }

#region Signalkontor
            if (IsUserInRole(identity, SeismitoadShared.Constants.Roles.Signalkontor))
            {
                menu.Add(new SkMenuItem
                             {
                                 Name = "signalkontor",
                                 Items = new[]
                                             {
                                                new SkSubMenuItem { Name = "signalkontor", RouteValueDictionary = MVC.Admin.Signalkontor.Index().GetRouteValueDictionary(), Image = Links.Content.icons._16x16.apps.terminal_png },
                                             }
                             });

            }
#endregion

            return menu;
        }
    }

    public enum MenuArea
    {
        Default,
        Project
    }

    public class MenuAreaAttribute : FilterAttribute, IResultFilter
    {
        private readonly MenuArea _menuArea;

        public MenuAreaAttribute(MenuArea menuArea)
        {
            _menuArea = menuArea;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.MenuArea = _menuArea;
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}