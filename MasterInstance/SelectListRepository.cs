using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;
using SeismitoadModel;

#if MASTERINSTANCE
namespace MasterInstance
#else
namespace PromoterInstance
#endif
{
    public class SelectListRepository : ObjectTemplate.SelectListRepository
    {
        static SelectListRepository()
        {
            if(File.Exists(HostingEnvironment.MapPath("~/App_Data/SelectListRepository.txt")))
            {
                AddSelectListItemsFromFile(HostingEnvironment.MapPath("~/App_Data/SelectListRepository.txt"));
            }
            else
            {
                // Workaround because PromoterInstance\App_Data\SelectListRepository.txt ist only a Link
                AddSelectListItemsFromFile(HostingEnvironment.MapPath("~") + "/../MasterInstance/App_Data/SelectListRepository.txt");
            }

            var femaleSizes = new List<int>(36);
            femaleSizes.AddRange(Range(32, 54, 2));
            femaleSizes.AddRange(Range(16, 27, 1));
            femaleSizes.AddRange(Range(64, 108, 4));

            var maleSizes = new List<int>(30);
            maleSizes.AddRange(Range(44, 62, 2));
            maleSizes.AddRange(Range(22, 31, 1));
            maleSizes.AddRange(Range(90, 122, 4));

            AddSelectListItems("FemaleSize", items => items.AddRange(femaleSizes.Select(e => new SelectListItem {Text = e.ToString(CultureInfo.InvariantCulture), Value = e.ToString(CultureInfo.InvariantCulture)})));
            AddSelectListItems("MaleSize", items => items.AddRange(maleSizes.Select(e => new SelectListItem { Text = e.ToString(CultureInfo.InvariantCulture), Value = e.ToString(CultureInfo.InvariantCulture) })));
            AddSelectListItems("JeansWidth", items => ForRange(25, 38, 1, i => items.Add(i, i.ToString(CultureInfo.InvariantCulture))));
            AddSelectListItems("JeansLength", items => ForRange(28, 38, 2, i => items.Add(i, i.ToString(CultureInfo.InvariantCulture))));
            AddSelectListItems("ShoeSize", items => ForRange(32, 54, 1, i => items.Add(i, i.ToString(CultureInfo.InvariantCulture))));
        }

        public static new IEnumerable<SelectListItem> Retrieve(string propertyName, ViewDataDictionary viewDataDictionary, Type type = null)
        {
            return ObjectTemplate.SelectListRepository.Retrieve(propertyName, viewDataDictionary, type);
        }

        private const int OthersLocationGroupId = 8;

        protected override IEnumerable<SelectListItem> GetSpecial(string propertyName, Type type = null)
        {
#if MASTERINSTANCE
            switch(propertyName)
            {
                case "StateProvinceId":
                    return DB.Context.StateProvinces.ToList().Select(e => new SelectListItem { Text = e.Name, Value = e.Id.ToString(CultureInfo.InvariantCulture) });
                case "LocationGroupId":
                    var locationGroups = DB.Context.LocationGroups
                        .Where(e => e.Id != OthersLocationGroupId)
                        .OrderBy(e => e.Name)
                        .Select(e => new { e.Id, e.Name })
                        .ToList();
                    
                    locationGroups.Add(new { Id = OthersLocationGroupId, Name = "Sonstige" });
                    return locationGroups.Select(e => new SelectListItem { Text = e.Name, Value = e.Id.ToString(CultureInfo.InvariantCulture) });
                case "LocationTypeId":
                    return DB.Context.LocationTypes
                        .OrderBy(e => e.Name)
                        .Select(e => new { e.Id, e.Name })
                        .ToList().Select(e => new SelectListItem { Text = e.Name, Value = e.Id.ToString(CultureInfo.InvariantCulture) });
                case "CustomerId":
                    return DB.Context.Customers
                        .Select(e => new SelectListItem {Text = e.Name, Value = "" + e.Id})
                        .OrderBy(e => e.Text);
                case "ManagerId":
                    return DB.Context.Database.SqlQuery<SelectListItem>(@"SELECT
	LOWER(CONVERT(nvarchar(36), ProviderUserKey)) AS Value,
	Firstname + ' ' + Lastname AS Text
FROM
	Accounts
INNER JOIN
	[seismitoad_aspnet].[dbo].[aspnet_Membership] m ON m.UserId = Accounts.ProviderUserKey AND m.IsApproved = 1");
                case "AssignmentRoleIds":
                    return DB.Context.AssignmentRoles.OrderBy(e => e.Id).ToList().Select(e => new SelectListItem
                                                                                                  {
                                                                                                      Text = string.Format("{0} ({1})", e.Name, e.ShortName),
                                                                                                      Value = e.Id.ToString(CultureInfo.InvariantCulture),
                                                                                                      Selected = e.Id == 1
                                                                                                  });
                case "AssignmentRoleIds2":
                    return DB.Context.AssignmentRoles.OrderBy(e => e.Id).ToList().Select(e => new SelectListItem
                    {
                        Text = string.Format("{0}", e.Name),
                        Value = e.Id.ToString(CultureInfo.InvariantCulture),
                        Selected = e.Id == 1
                    });
                case "ProjectNumbers":
                    return DB.Context.Campaigns
                        .Where(e => e.State != CampaignState.Archived && e.State != CampaignState.Deleted)
                        .Select(e => new SelectListItem
                                {
                                    Value = e.Customer.Number + e.Number,
                                    Text = e.Customer.Number + e.Number + " (" + e.Customer.Name + " - " + e.Name + ")"
                                })
                        .OrderBy(e => e.Text);
            }
#endif
            return null;
        }
    }
}