﻿if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

//Fix from Telerik to get the charts working with jQuery UI (see http://www.telerik.com/community/forums/aspnet-mvc/general/pie-chart.aspx#1862840)
jQuery.extend(jQuery.easing, {
    swing: function(p, n, firstNum, diff) {
        return ((-Math.cos(p * Math.PI) / 2) + 0.5) * diff + firstNum;
    }
});