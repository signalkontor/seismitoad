var GridStateSaver = (function () {
    function GridStateSaver(grid, restoreGridState, afterOptionsSet, applyAdditionalState, ignoreToolbar) {
        if (afterOptionsSet === void 0) { afterOptionsSet = function () { }; }
        if (applyAdditionalState === void 0) { applyAdditionalState = function (additionalState) { }; }
        if (ignoreToolbar === void 0) { ignoreToolbar = false; }
        this.grid = grid;
        this.restoreGridState = restoreGridState;
        this.afterOptionsSet = afterOptionsSet;
        this.applyAdditionalState = applyAdditionalState;
        this.ignoreToolbar = ignoreToolbar;
        this.localStorageKey = document.location.pathname;
        this.restoredData = false;
        this.currentVersion = "3";
        var additionalState = localStorage.getItem(this.localStorageKey + "_extra");
        grid.one("dataBinding", $.proxy(this.dataBinding, this));
        grid.bind("dataBound", $.proxy(this.saveState, this));
        grid.bind("columnShow", $.proxy(this.saveState, this));
        grid.bind("columnHide", $.proxy(this.saveState, this));
        grid.bind("columnResize", $.proxy(this.saveState, this));
        grid.bind("columnReorder", $.proxy(this.saveStateAfterReoderEvent, this));
        if (additionalState) {
            applyAdditionalState(JSON.parse(additionalState));
        }
        this.restoredData = restoreGridState && localStorage.getItem(this.localStorageKey) !== null;
        this.toolbar = grid.element.find(".k-grid-toolbar").html();
    }
    GridStateSaver.prototype.dataBinding = function (e) {
        var optionsJson, version;
        if (this.restoreGridState) {
            version = localStorage.getItem(this.localStorageKey + "_version");
            if (version != this.currentVersion) {
                localStorage.removeItem(this.localStorageKey);
            }
            optionsJson = localStorage.getItem(this.localStorageKey);
            if (optionsJson) {
                var options = JSON.parse(optionsJson);
                if (!this.ignoreToolbar) {
                    options.toolbar = [
                        { template: this.toolbar }
                    ];
                }
                e.sender.setOptions(options);
            }
            this.afterOptionsSet();
        }
        e.sender.dataSource.read();
    };
    GridStateSaver.prototype.saveState = function (e) {
        var options = e.sender.getOptions();
        localStorage.setItem(this.localStorageKey, JSON.stringify(options));
        localStorage.setItem(this.localStorageKey + "_version", this.currentVersion);
    };
    GridStateSaver.prototype.saveStateAfterReoderEvent = function (e) {
        var _this = this;
        window.setTimeout(function () {
            var options = e.sender.getOptions();
            localStorage.setItem(_this.localStorageKey, JSON.stringify(options));
            localStorage.setItem(_this.localStorageKey + "_version", _this.currentVersion);
        }, 0);
    };
    GridStateSaver.prototype.saveAdditionalState = function (additionalState) {
        localStorage.setItem(this.localStorageKey + "_extra", JSON.stringify(additionalState));
    };
    GridStateSaver.prototype.clearState = function () {
        localStorage.removeItem(this.localStorageKey);
        localStorage.removeItem(this.localStorageKey + "_extra");
    };
    return GridStateSaver;
}());
//# sourceMappingURL=GridStateSaver.js.map