﻿var deleteUrl, elementToReplace;
var cardPos, tableWidth, tableHeight, fixedHeaderTableCreated = false;
var draggableOpts = { revert: "invalid", containment: "document", cursor: 'crosshair', helper: 'clone' };
var droppableOpts = {
    hoverClass: "ui-state-active",
    drop: function (event, ui) {
        var source = $(ui.draggable);
        var target = $(this);
        var newAssignmentId = target.attr("aid");
        var oldAssignmentId = source.attr("aid");
        var employeeId = source.attr("eid");
        var postData = "&oldAssignmentId=" + oldAssignmentId + "&newAssignmentId=" + newAssignmentId + "&employeeId=" + employeeId;
        $.ajax({
            url: window.moveUrl,
            data: postData,
            type: "POST",
            success: function (newTargetHtml) {
                var targetToReplace = target.parent();
                var parentOfTarget = targetToReplace.parent();
                targetToReplace.replaceWith(newTargetHtml);
                $(".draggable", parentOfTarget).draggable(draggableOpts);
                $(".droppable", parentOfTarget).droppable(droppableOpts);
                $.post(window.cardUrl, "&assignmentId=" + oldAssignmentId, function (newSourceHtml) {
                    var sourceToReplace = source.closest("div.assignment-card");
                    var parentOfSource = sourceToReplace.parent();
                    sourceToReplace.replaceWith(newSourceHtml);
                    $(".draggable", parentOfSource).draggable(draggableOpts);
                    $(".droppable", parentOfSource).droppable(droppableOpts);
                    _onResize();
                });
            },
            error: function (xhr) {
                alert(xhr.status === 409 ? xhr.responseText : "Verschieben nicht möglich.");
            }
        });
    }
};

function disableOtherEmployeeDropDowns(activeDropDown) {
    $(".sk-color-multiselect").multiselect("disable");
    $("#EmployeeId").data("tComboBox").disable();
    if (activeDropDown.id != "EmployeeId") {
        $(activeDropDown).multiselect("enable");
    } else {
        $(activeDropDown).data("tComboBox").enable();
    }
}

function enableAllEmployeeDropDowns() {
    $(".sk-color-multiselect").multiselect("enable");
    $("#EmployeeId").data("tComboBox").enable();
}

function _onResize() {
    if (fixedHeaderTableCreated) {
        $("#assignments-table").fixedHeaderTable("destroy");
    }
    fixedHeaderTableCreated = true;
    tableWidth = $("#main").width();
    tableHeight = $("#hSplitter").height() - 220;
    $("#assignments-table").fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumns: 1, width: "" + tableWidth, height: "" + tableHeight });
}

function remove(reload) {
    $.post(deleteUrl, null, function (data) {
        var parent = elementToReplace.parent();
        elementToReplace.replaceWith(data);
        $(".draggable", parent).draggable(window.draggableOpts);
        $(".droppable", parent).droppable(window.droppableOpts);
        _onResize();
        $('#dw').data('t-window').close();
        if(reload) {
            document.location.reload(true);
        }
    });
}

function removeAll() {
    window.deleteUrl = window.deleteUrl + "&action=all";
    remove(true);
}

function removeDayOfWeek() {
    window.deleteUrl = window.deleteUrl + "&action=dayOfWeek";
    remove(false);
}

function removeButtonClick(e) {
    var self = $(this);
    var employeeName = self.closest("li").children().first().html();
    var deleteWindow = $('#dw').data('t-window');
    elementToReplace = self.closest("div.assignment-card");
    window.deleteUrl = self.attr("href");
    e.preventDefault();
    deleteWindow.content("Entfernen von " + employeeName + ":<br/><button onclick='remove()'>Nur dieser Einsatz</button><button onclick='removeAll()'>Alle folgenden</button><button onclick='removeDayOfWeek()'>Alle folgenden am selben Wochentag</button><br/>Anschließend muss möglicherweise das Laden der Seite bestätigt werden.");
    deleteWindow.open();
}

function editorButtonClick(e) {
    var wnd = $("#w").data("t-window");
    var elem = $(this);
    cardPos = elem.closest("div.assignment-card");
    e.preventDefault();
    wnd.title(elem.attr("windowTitle"));
    wnd.contentUrl = elem.attr("href");
    $(wnd.element).find(".t-window-content").css("height", "auto").css("width", "auto");
    wnd.refresh();
}

function assignButtonClick() {
    var form = $("#assignPromoterForm");
    for (var id in window.assignmentIds) {
        form.append('<input type="hidden" name="AssignmentIds", value="' + id + '" />');
    }
}

function cancelButtonClick(e) {
    e.preventDefault();
    $("#w").data("t-window").close();
}


function saveScrollState() {
    $.cookie("scrollPosX", $(".fht-fixed-body > .fht-tbody").scrollLeft());
    $.cookie("scrollPosY", $(".fht-fixed-body > .fht-tbody").scrollTop());
}
function scrollToState() {
    $(".fht-fixed-body > .fht-tbody").scrollLeft($.cookie("scrollPosX"));
    $(".fht-fixed-body > .fht-tbody").scrollTop($.cookie("scrollPosY"));
}


$(function () {
    $("a.employee-remove-button").live('click', removeButtonClick);
    $("a.employee-editor-button").live('click', editorButtonClick);
    $("#assign-button").live('click', assignButtonClick);
    $("#cancel-button").live('click', cancelButtonClick);
    $(".draggable").draggable(window.draggableOpts);
    $(".droppable").droppable(window.droppableOpts);
    $(document).live('mouseup', saveScrollState);
    $(document).live('ready', scrollToState);

    $("#GoToDate").datepicker({
        firstDay: 1,
        dateFormat: "dd.mm.yy",
        changeMonth: true,
        changeYear: true
    });
});