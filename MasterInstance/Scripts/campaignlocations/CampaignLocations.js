var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var locationId = 0;
var currentModel = null;
var CampaignLocationSelectModel = (function (_super) {
    __extends(CampaignLocationSelectModel, _super);
    function CampaignLocationSelectModel() {
        var _this = _super.call(this) || this;
        _this.amount = 0;
        _this.onlyCampaignLocations = false;
        _this.action = null;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    CampaignLocationSelectModel.prototype.reset = function () {
        gridStateSaver.clearState();
        window.location.reload();
    };
    CampaignLocationSelectModel.prototype.add = function () {
        this.action = "add";
        $("#g").data("kendoGrid").dataSource.read();
    };
    CampaignLocationSelectModel.prototype.remove = function () {
        this.action = "remove";
        $("#g").data("kendoGrid").dataSource.read();
    };
    CampaignLocationSelectModel.prototype.showAllLocations = function () {
        this.set("onlyCampaignLocations", false);
        $("#g").data("kendoGrid").dataSource.page(1);
    };
    CampaignLocationSelectModel.prototype.showOnlyCampaignLocations = function () {
        this.set("onlyCampaignLocations", true);
        $("#g").data("kendoGrid").dataSource.page(1);
    };
    CampaignLocationSelectModel.prototype.buttonsEnabled = function () {
        return this.get("amount") > 0;
    };
    CampaignLocationSelectModel.prototype.gridParams = function () {
        var params;
        if (this.action != null && this.action === "add") {
            params = {
                onlyCampaignLocations: this.get("onlyCampaignLocations"),
                NetworkConnectionAvailable: $("#Template_NetworkConnectionAvailable").val(),
                state: $("#Template_State").val(),
                classification: $("#Template_Classification").val(),
                salesRegion: $("#Template_SalesRegion").val(),
                notes: $("#Template_Notes").val(),
                contactId: $("#Template_ContactId").val(),
                addOrRemove: this.action
            };
        }
        else {
            params = {
                onlyCampaignLocations: this.get("onlyCampaignLocations"),
                addOrRemove: this.action
            };
        }
        this.action = null;
        return params;
    };
    return CampaignLocationSelectModel;
}(kendo.data.ObservableObject));
function updateRow(row, isCampaignLocation, deleteable) {
    row
        .toggleClass("campaign-location", isCampaignLocation)
        .find(".k-grid-edit").toggle(isCampaignLocation);
    row.find(".k-grid-toggle")
        .text(isCampaignLocation ? "Entfernen" : "Hinzufügen")
        .prop("enabled", deleteable)
        .toggleClass("k-state-disabled", !deleteable);
}
function gridDataBound(e) {
    var grid = e.sender, records = grid.dataSource.view(), record;
    campaignLocationSelectModel.set("amount", grid.dataSource.total());
    for (var i = 0; i < records.length; i++) {
        record = records[i];
        updateRow(grid.tbody.find("tr[data-uid='" + records[i].uid + "']"), record.IsCampaignLocation, record.Deleteable);
    }
}
function changeHandler() {
    updateRow($("#g tr[data-uid=" + currentModel.uid + "]"), true, currentModel.get("Deleteable"));
}
function gridEdit(e) {
    locationId = e.model.id;
    currentModel = e.model;
    e.model.bind("change", changeHandler);
}
function restoreColor(e) {
    e.model.unbind("change", changeHandler);
    window.setTimeout(changeHandler, 0);
}
function toggleLocation(e) {
    var button = $(e.target), row = button.closest("tr"), grid = $("#g").data("kendoGrid"), dataItem = grid.dataItem(row), postData = $("#template").serializeArray();
    e.preventDefault();
    if (button.prop("enabled") === false) {
        return;
    }
    postData.push({ name: "campaignId", value: "" + campaignId });
    postData.push({ name: "locationId", value: dataItem["LocationId"] });
    $.blockUI({ message: "Bitte warten..." });
    $.post(appPath + "CM/CampaignLocation/_Toggle", postData, function (data, textStatus, jqXHR) {
        var isCampaignLocation = data === "Inserted", networkConnectionAvailable;
        dataItem = grid.dataItem(row);
        if (isCampaignLocation) {
            networkConnectionAvailable = $("#Template_NetworkConnectionAvailable").val();
            dataItem.set("WiFiAvailableCampaign", networkConnectionAvailable);
            if (networkConnectionAvailable != null) {
                dataItem.set("WifiAvailable", networkConnectionAvailable);
            }
            dataItem.set("State", $("#Template_State").val());
            dataItem.set("Classification", $("#Template_Classification").val());
            dataItem.set("SalesRegion", $("#Template_SalesRegion").val());
            dataItem.set("RemarkCampaign", $("#Template_Notes").val());
            dataItem.set("ContactId", $("#Template_ContactId").val());
        }
        else {
            dataItem.set("WiFiAvailableCampaign", null);
            dataItem.set("WifiAvailable", dataItem.get("WifiAvailableLocation"));
            dataItem.set("State", null);
            dataItem.set("Classification", null);
            dataItem.set("SalesRegion", null);
            dataItem.set("RemarkCampaign", null);
            dataItem.set("ContactId", null);
        }
        dataItem.set("IsCampaignLocation", isCampaignLocation);
        updateRow(row, isCampaignLocation, true);
    }).done(function () { $.unblockUI(); });
}
function resetNewContactForm() {
    $("#newContactForm input").val("");
    $("#Title").data("kendoDropDownList").value("");
}
var campaignLocationSelectModel = new CampaignLocationSelectModel();
$(document).ready(function () {
    gridStateSaver = new GridStateSaver($("#g").data("kendoGrid"), true, function () {
        $("#g .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
        $("#additional-toolbar-items-template").remove();
        kendo.bind("#actions", campaignLocationSelectModel);
    }, function (additionalState) { campaignLocationSelectModel.set("onlyCampaignLocations", additionalState.onlyCampaignLocations); }, true);
    window.setTimeout(function () {
        var referenceElement = $("#hSplitter");
        if (referenceElement.length == 0) {
            referenceElement = $("#main");
        }
        var gridResizer = new GridResizer(referenceElement, $("#g"), function (height) {
            var grid = $("#g").data("kendoGrid"), total, rows;
            total = $("#g .k-grid-content").height();
            rows = Math.floor(total / 32) - 2;
            grid.dataSource.pageSize(rows);
            grid.refresh();
        });
        gridResizer.resize();
        $("#g").on("click", "a.k-grid-toggle", toggleLocation);
    }, 0);
    $("#newContactForm").on("submit", function (e) {
        var postData = $("#newContactForm").serializeArray();
        postData.push({ name: "locationId", value: "" + locationId });
        e.preventDefault();
        $.post(appPath + "CM/CampaignLocation/_AddContact", postData).done(function () {
            $("#ContactIdHelper").data("kendoDropDownList").dataSource.read();
            $("#newContactWindow").data("kendoWindow").close();
        });
    });
    function saveState() {
        gridStateSaver.saveAdditionalState({
            onlyCampaignLocations: campaignLocationSelectModel.get("onlyCampaignLocations"),
        });
    }
    campaignLocationSelectModel.bind("change", saveState);
});
//# sourceMappingURL=CampaignLocations.js.map