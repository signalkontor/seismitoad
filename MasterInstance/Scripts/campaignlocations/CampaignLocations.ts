﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
declare var gridStateSaver: GridStateSaver;
var locationId = 0;
var currentModel: any = null;

class CampaignLocationSelectModel extends kendo.data.ObservableObject {
    amount: number = 0;
    onlyCampaignLocations: boolean = false;
    action: string = null;

    reset() {
        gridStateSaver.clearState();
        window.location.reload();
    }

    add() {
        this.action = "add";
        $("#g").data("kendoGrid").dataSource.read();
    }

    remove() {
        this.action = "remove";
        $("#g").data("kendoGrid").dataSource.read();
    }

    showAllLocations() {
        this.set("onlyCampaignLocations", false);
        $("#g").data("kendoGrid").dataSource.page(1);
    }

    showOnlyCampaignLocations() {
        this.set("onlyCampaignLocations", true);
        $("#g").data("kendoGrid").dataSource.page(1);
    }

    buttonsEnabled() {
        return this.get("amount") > 0;
    }

    gridParams() {
        var params: any;

        if (this.action != null && this.action === "add") {
            params = {
                onlyCampaignLocations: this.get("onlyCampaignLocations"),
                NetworkConnectionAvailable: $("#Template_NetworkConnectionAvailable").val(),
                state: $("#Template_State").val(),
                classification: $("#Template_Classification").val(),
                salesRegion: $("#Template_SalesRegion").val(),
                notes: $("#Template_Notes").val(),
                contactId: $("#Template_ContactId").val(),
                addOrRemove: this.action
            }
        } else {
            params = {
                onlyCampaignLocations: this.get("onlyCampaignLocations"),
                addOrRemove: this.action
            };
        }
        this.action = null;
        return params;
    }

    constructor() {
        super();
        super.init(this);
    }
}

function updateRow(row: JQuery, isCampaignLocation: boolean, deleteable: boolean) {
    row
        .toggleClass("campaign-location", isCampaignLocation)
        .find(".k-grid-edit").toggle(isCampaignLocation);

    row.find(".k-grid-toggle")
        .text(isCampaignLocation ? "Entfernen" : "Hinzufügen")
        .prop("enabled", deleteable)
        .toggleClass("k-state-disabled", !deleteable);
}

function gridDataBound(e: kendo.ui.GridDataBoundEvent) {
    var grid = e.sender,
        records = grid.dataSource.view(),
        record;

    campaignLocationSelectModel.set("amount", grid.dataSource.total());

    for (var i = 0; i < records.length; i++) {
        record = records[i];
        updateRow(grid.tbody.find("tr[data-uid='" + records[i].uid + "']"), record.IsCampaignLocation, record.Deleteable);
    }
}

function changeHandler() {
    // IsCampaignLocation setzen wir fest auf true, da komischerweise currentModel.IsCampaignLocation false ist
    // und wir nicht wissen warum. Aber diese Funktion wird nur aufgerufen wenn es true ist.
    updateRow($("#g tr[data-uid=" + currentModel.uid + "]"), true, currentModel.get("Deleteable"));
}

function gridEdit(e: kendo.ui.GridEditEvent) {
    locationId = e.model.id;
    currentModel = e.model;

    // Wir müssen die Zeile jedes mal auf's neue holen, denn Kendo erzeugt anscheinend immer wieder neue Zeilen
    e.model.bind("change", changeHandler);
}

/* or kendo.ui.GridSaveEvent */
function restoreColor(e: kendo.ui.GridCancelEvent) {
    e.model.unbind("change", changeHandler);
    // Verzögert ausführen, denn das Cancel und Save event kommen bevor Kendo die Zeile neu anlegt und somit zu früh.
    window.setTimeout(changeHandler, 0);
}

function toggleLocation(e: JQueryEventObject) {
    var button = $(e.target),
        row = button.closest("tr"),
        grid = $("#g").data("kendoGrid"),
        dataItem = grid.dataItem(row),
        postData = $("#template").serializeArray();
    e.preventDefault();

    if (button.prop("enabled") === false) {
        return;
    }

    postData.push({ name: "campaignId", value: `${campaignId}` });
    postData.push({ name: "locationId", value: dataItem["LocationId"] });

    $.blockUI({ message: "Bitte warten..." });

    $.post(appPath + "CM/CampaignLocation/_Toggle", postData, (data, textStatus, jqXHR) => {
        var isCampaignLocation = data === "Inserted",
            networkConnectionAvailable;


        // Wir holen uns das DataItem lieber noch mal, es scheitn manchmal sonst nicht aktuell zu sein
        dataItem = grid.dataItem(row);

        if (isCampaignLocation) {
            networkConnectionAvailable = $("#Template_NetworkConnectionAvailable").val();
            dataItem.set("WiFiAvailableCampaign", networkConnectionAvailable);
            if (networkConnectionAvailable != null) {
                dataItem.set("WifiAvailable", networkConnectionAvailable);
            }
            dataItem.set("State", $("#Template_State").val());
            dataItem.set("Classification", $("#Template_Classification").val());
            dataItem.set("SalesRegion", $("#Template_SalesRegion").val());
            dataItem.set("RemarkCampaign", $("#Template_Notes").val());
            dataItem.set("ContactId", $("#Template_ContactId").val());
        } else {
            dataItem.set("WiFiAvailableCampaign", null);
            dataItem.set("WifiAvailable", dataItem.get("WifiAvailableLocation"));
            dataItem.set("State", null);
            dataItem.set("Classification", null);
            dataItem.set("SalesRegion", null);
            dataItem.set("RemarkCampaign", null);
            dataItem.set("ContactId", null);
        }
        dataItem.set("IsCampaignLocation", isCampaignLocation);
        updateRow(row, isCampaignLocation, true);
    }).done(() => { $.unblockUI(); });
}

function resetNewContactForm() {
    $("#newContactForm input").val("");
    $("#Title").data("kendoDropDownList").value("");
}

var campaignLocationSelectModel = new CampaignLocationSelectModel();

$(document).ready(() => {
    gridStateSaver = new GridStateSaver($("#g").data("kendoGrid"), true,
        // wird ausgeführt nachdem der GridState wiederhergestellt wurde
        // wir benutzes es um die Toolbar zu fixen, da Kendo die nicht speichern kann
        () => {
            $("#g .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
            $("#additional-toolbar-items-template").remove();
            kendo.bind("#actions", campaignLocationSelectModel);
        },
        // wird sofort ausgeführt, wenn es zusätzlichen state gibt.
        (additionalState: any) => { campaignLocationSelectModel.set("onlyCampaignLocations", additionalState.onlyCampaignLocations); },
        true);

    window.setTimeout(() => {
        var referenceElement = $("#hSplitter");
        if (referenceElement.length == 0) {
            referenceElement = $("#main");
        }
        var gridResizer = new GridResizer(referenceElement, $("#g"), height => {
            var grid = $("#g").data("kendoGrid"),
                total: number,
                rows: number;

            //if (!gridStateSaver.restoredData) {
                total = $("#g .k-grid-content").height();
                rows = Math.floor(total / 32) - 2;
                grid.dataSource.pageSize(rows);
            //}
            grid.refresh();
        });
        gridResizer.resize();

        $("#g").on("click", "a.k-grid-toggle", toggleLocation);
    }, 0);

    $("#newContactForm").on("submit", (e: JQueryEventObject) => {
        var postData = $("#newContactForm").serializeArray();
        postData.push({ name: "locationId", value: `${locationId}` });
        e.preventDefault();
        $.post(appPath + "CM/CampaignLocation/_AddContact", postData).done(() => {
            $("#ContactIdHelper").data("kendoDropDownList").dataSource.read();
            $("#newContactWindow").data("kendoWindow").close();
        });
    });

    function saveState() {
        gridStateSaver.saveAdditionalState({
            onlyCampaignLocations: campaignLocationSelectModel.get("onlyCampaignLocations"),
        });
    }

    campaignLocationSelectModel.bind("change", saveState);
});