﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.blockUI/jquery.blockUI.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

class DateGeneratorViewModel extends kendo.data.ObservableObject {
    constructor() {
        super();
        super.init(this);
    }

    dateStart: Date;
    dateEnd: Date;
    interval: number;
    mondayChecked: boolean;
    tuesdayChecked: boolean;
    wednesdayChecked: boolean;
    thursdayChecked: boolean;
    fridayChecked: boolean;
    saturdayChecked: boolean;
    sundayChecked: boolean;
    mondayStart: number;
    tuesdayStart: number;
    wednesdayStart: number;
    thursdayStart: number;
    fridayStart: number;
    saturdayStart: number;
    sundayStart: number;
    mondayEnd: number;
    tuesdayEnd: number;
    wednesdayEnd: number;
    thursdayEnd: number;
    fridayEnd: number;
    saturdayEnd: number;
    sundayEnd: number;
    mondayAmount: number;
    tuesdayAmount: number;
    wednesdayAmount: number;
    thursdayAmount: number;
    fridayAmount: number;
    saturdayAmount: number;
    sundayAmount: number;

    generatedDates = new kendo.data.DataSource({ data: [{ start: new Date(), end: new Date(), amount: 1 }]});

    canGenerate() {
        return this.get("dateStart") instanceof Date && this.get("dateEnd") instanceof Date && this.get("interval") != null;
    }

    generate() {
        var current = new Date((<Date>this.get("dateStart")).valueOf()),
            end = new Date((<Date>this.get("dateEnd")).valueOf()),
            step = 7 * parseInt(this.get("interval").value),
            data = [],
            days = ["monday", "tuesday", "wednesday", "thursday", "firday", "saturday", "sunday"]
                .filter(value => this.get(value + "Checked") === true)
                .map((value, index) => {
                    return { offset: index, start: <Date>this.get(value + "Start"), end: <Date>this.get(value + "End"), amount: <Number>this.get(value + "Amount") }
                })
                .filter(day => day.start != null && day.end != null && day.amount != null),
            tmpStartDate: Date,
            tmpEndDate: Date;

        while (current <= end) {
            for (var i = 0; i < days.length; i++) {
                tmpStartDate = new Date(current.valueOf());
                tmpStartDate.setDate(tmpStartDate.getDate() + days[i].offset);
                this.setTime(tmpStartDate, days[i].start);

                tmpEndDate = new Date(current.valueOf());
                tmpEndDate.setDate(tmpEndDate .getDate() + days[i].offset);
                this.setTime(tmpEndDate, days[i].end);

                data.push({
                    start: tmpStartDate,
                    end: tmpEndDate,
                    amount: days[i].amount
                });
            }
            current.setDate(current.getDate() + step);
        }

        this.generatedDates.data(data);
    }

    private setTime(date: Date, time: Date) {
        var hours = time.getHours();
        date.setHours(hours);
        if (hours < 5) {
            // Zeiten vor 5 Uhr meinen den nächsten Tag
            date.setDate(date.getDate() + 1);
        }
        date.setMinutes(time.getMinutes());
        date.setSeconds(time.getSeconds());
        date.setMilliseconds(time.getMilliseconds());
    }
}

class CampaignLocationGridModel extends kendo.data.ObservableObject {
    action: string = null;
    locationId: Number = null;
    amount: Number = 0;
    locationsCount: Number = 0;

    constructor(private dateGenerator: DateGeneratorViewModel) {
        super();
        super.init(this);
        dateGenerator.bind("change", $.proxy(this.dateGeneratorChanged, this));
    }

    dateGeneratorChanged(e: kendo.data.ObservableObjectEvent) {
        var data = this.dateGenerator.generatedDates.data();
        this.set("amount", data.length);
    }

    add() {
        this.action = "add";
        $("#g").data("kendoGrid").dataSource.read();
    }

    gridParams() {
        var params: any;
        jQuery.ajaxSettings.traditional = false;
        if (this.action != null) {
            params = {
                addOrRemove: this.action,
                locationId: this.locationId,
                start: $.map(this.dateGenerator.generatedDates.data(), e => e.start),
                end: $.map(this.dateGenerator.generatedDates.data(), e => e.end),
                amount: $.map(this.dateGenerator.generatedDates.data(), e => e.amount)
        }
        } else {
            params = {}
        }
        this.action = null;
        this.locationId = null;
        return params;
    }

}

function edit(e: JQueryEventObject) {
    var tr = $(e.target).closest("tr"),
        data = this.dataItem(tr);
    e.preventDefault();
}

function add(e: JQueryEventObject) {
    var tr = $(e.target).closest("tr"),
        data = this.dataItem(tr);
    e.preventDefault();
    campaignLocationGridModel.action = "add";
    campaignLocationGridModel.locationId = data.LocationId;
    $("#g").data("kendoGrid").dataSource.read();
}

function remove(e: JQueryEventObject) {
    var tr = $(e.target).closest("tr"),
        data = this.dataItem(tr);
    e.preventDefault();
    campaignLocationGridModel.action = "remove";
    campaignLocationGridModel.locationId = data.LocationId;
    $("#g").data("kendoGrid").dataSource.read();
}

var dateGeneratorViewModel = new DateGeneratorViewModel();
var campaignLocationGridModel = new CampaignLocationGridModel(dateGeneratorViewModel);

$(document).ready(() => {
    kendo.bind("#date-generator", dateGeneratorViewModel);
    kendo.bind("#actions", campaignLocationGridModel);

    window.setTimeout(() => {
        var referenceElement = $("#hSplitter");
        if (referenceElement.length === 0) {
            referenceElement = $("#main");
        }
        var gridResizer = new GridResizer(referenceElement, $("#g"), () => {}, 300);
        gridResizer.resize();
    }, 0);

    $("#g").data("kendoGrid").bind("dataBound", e => {
        var length = (<kendo.ui.Grid>e.sender).dataSource.data().length;
        campaignLocationGridModel.set("locationsCount", length);
    });
});