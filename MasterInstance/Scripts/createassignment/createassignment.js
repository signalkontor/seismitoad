var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DateGeneratorViewModel = (function (_super) {
    __extends(DateGeneratorViewModel, _super);
    function DateGeneratorViewModel() {
        var _this = _super.call(this) || this;
        _this.generatedDates = new kendo.data.DataSource({ data: [{ start: new Date(), end: new Date(), amount: 1 }] });
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    DateGeneratorViewModel.prototype.canGenerate = function () {
        return this.get("dateStart") instanceof Date && this.get("dateEnd") instanceof Date && this.get("interval") != null;
    };
    DateGeneratorViewModel.prototype.generate = function () {
        var _this = this;
        var current = new Date(this.get("dateStart").valueOf()), end = new Date(this.get("dateEnd").valueOf()), step = 7 * parseInt(this.get("interval").value), data = [], days = ["monday", "tuesday", "wednesday", "thursday", "firday", "saturday", "sunday"]
            .filter(function (value) { return _this.get(value + "Checked") === true; })
            .map(function (value, index) {
            return { offset: index, start: _this.get(value + "Start"), end: _this.get(value + "End"), amount: _this.get(value + "Amount") };
        })
            .filter(function (day) { return day.start != null && day.end != null && day.amount != null; }), tmpStartDate, tmpEndDate;
        while (current <= end) {
            for (var i = 0; i < days.length; i++) {
                tmpStartDate = new Date(current.valueOf());
                tmpStartDate.setDate(tmpStartDate.getDate() + days[i].offset);
                this.setTime(tmpStartDate, days[i].start);
                tmpEndDate = new Date(current.valueOf());
                tmpEndDate.setDate(tmpEndDate.getDate() + days[i].offset);
                this.setTime(tmpEndDate, days[i].end);
                data.push({
                    start: tmpStartDate,
                    end: tmpEndDate,
                    amount: days[i].amount
                });
            }
            current.setDate(current.getDate() + step);
        }
        this.generatedDates.data(data);
    };
    DateGeneratorViewModel.prototype.setTime = function (date, time) {
        var hours = time.getHours();
        date.setHours(hours);
        if (hours < 5) {
            date.setDate(date.getDate() + 1);
        }
        date.setMinutes(time.getMinutes());
        date.setSeconds(time.getSeconds());
        date.setMilliseconds(time.getMilliseconds());
    };
    return DateGeneratorViewModel;
}(kendo.data.ObservableObject));
var CampaignLocationGridModel = (function (_super) {
    __extends(CampaignLocationGridModel, _super);
    function CampaignLocationGridModel(dateGenerator) {
        var _this = _super.call(this) || this;
        _this.dateGenerator = dateGenerator;
        _this.action = null;
        _this.locationId = null;
        _this.amount = 0;
        _this.locationsCount = 0;
        _super.prototype.init.call(_this, _this);
        dateGenerator.bind("change", $.proxy(_this.dateGeneratorChanged, _this));
        return _this;
    }
    CampaignLocationGridModel.prototype.dateGeneratorChanged = function (e) {
        var data = this.dateGenerator.generatedDates.data();
        this.set("amount", data.length);
    };
    CampaignLocationGridModel.prototype.add = function () {
        this.action = "add";
        $("#g").data("kendoGrid").dataSource.read();
    };
    CampaignLocationGridModel.prototype.gridParams = function () {
        var params;
        jQuery.ajaxSettings.traditional = false;
        if (this.action != null) {
            params = {
                addOrRemove: this.action,
                locationId: this.locationId,
                start: $.map(this.dateGenerator.generatedDates.data(), function (e) { return e.start; }),
                end: $.map(this.dateGenerator.generatedDates.data(), function (e) { return e.end; }),
                amount: $.map(this.dateGenerator.generatedDates.data(), function (e) { return e.amount; })
            };
        }
        else {
            params = {};
        }
        this.action = null;
        this.locationId = null;
        return params;
    };
    return CampaignLocationGridModel;
}(kendo.data.ObservableObject));
function edit(e) {
    var tr = $(e.target).closest("tr"), data = this.dataItem(tr);
    e.preventDefault();
}
function add(e) {
    var tr = $(e.target).closest("tr"), data = this.dataItem(tr);
    e.preventDefault();
    campaignLocationGridModel.action = "add";
    campaignLocationGridModel.locationId = data.LocationId;
    $("#g").data("kendoGrid").dataSource.read();
}
function remove(e) {
    var tr = $(e.target).closest("tr"), data = this.dataItem(tr);
    e.preventDefault();
    campaignLocationGridModel.action = "remove";
    campaignLocationGridModel.locationId = data.LocationId;
    $("#g").data("kendoGrid").dataSource.read();
}
var dateGeneratorViewModel = new DateGeneratorViewModel();
var campaignLocationGridModel = new CampaignLocationGridModel(dateGeneratorViewModel);
$(document).ready(function () {
    kendo.bind("#date-generator", dateGeneratorViewModel);
    kendo.bind("#actions", campaignLocationGridModel);
    window.setTimeout(function () {
        var referenceElement = $("#hSplitter");
        if (referenceElement.length === 0) {
            referenceElement = $("#main");
        }
        var gridResizer = new GridResizer(referenceElement, $("#g"), function () { }, 300);
        gridResizer.resize();
    }, 0);
    $("#g").data("kendoGrid").bind("dataBound", function (e) {
        var length = e.sender.dataSource.data().length;
        campaignLocationGridModel.set("locationsCount", length);
    });
});
//# sourceMappingURL=createassignment.js.map