﻿var detailsToggled = false;
function toggleDetails(gridSelector) {
    var grid = $(gridSelector).data("tGrid");
    var rows = $(gridSelector + " tbody > .t-master-row");
    detailsToggled = !detailsToggled;
    if (detailsToggled) {
        grid.expandRow(rows);
    } else {
        grid.collapseRow(rows);
    }
}