var GridResizer = (function () {
    function GridResizer(hSplitter, gridElement, callback, correction) {
        if (callback === void 0) { callback = function (height) { }; }
        if (correction === void 0) { correction = 60; }
        this.hSplitter = hSplitter;
        this.gridElement = gridElement;
        this.callback = callback;
        this.correction = correction;
        window["_onResize"] = $.proxy(this.resize, this);
    }
    GridResizer.prototype.resize = function () {
        var height = this.hSplitter.height() - this.correction;
        this.gridElement.find(".k-grid-content").height(height - 65);
        this.gridElement.height(height);
        this.gridElement.data("kendoGrid").refresh();
        this.callback(height);
    };
    return GridResizer;
}());
//# sourceMappingURL=GridResizer.js.map