Date.prototype["getWeek"] = function () {
    var target = new Date(this.valueOf());
    var dayNr = (this.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    var firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() != 4) {
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
    }
    return 1 + Math.ceil((firstThursday - target.valueOf()) / 604800000);
};
function skBlockUI() {
    if ($.blockUI) {
        $.blockUI({ message: '<div style="padding:15px;"><img src="' + appPath + 'Content/images/ajax-loader.gif" alt="loading..."/><br/><strong>Bitte warten...</strong></div>', baseZ: 20000 });
    }
}
function _onResize() {
    $("iframe").height($("#hSplitter").height() - 100);
}
$(window).bind('resize', _onResize);
if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [], k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys;
    };
}
function logMessage(msg) {
    $.ajax({
        type: 'POST',
        url: '/Log',
        data: { message: msg }
    });
}
$(document).ajaxError(function (event, xhr, ajaxSetting, thrownError) {
    if (ajaxSetting.url.indexOf("/Log") === -1 && xhr.getAllResponseHeaders()) {
        logMessage(ajaxSetting.type + " " + ajaxSetting.url + "[" + ajaxSetting.data + "]: " + xhr.statusText);
        alert("Es ist ein Fehler bei der Kommunikation mit dem Server aufgetreten.");
    }
});
function saveMenuState() {
    window.setTimeout(function () {
        var expanded = $("#Menu > li.save-state").map(function (index, domElement) {
            var el = $(domElement);
            return el.hasClass("t-state-active") || el.hasClass("k-state-active");
        }).get();
        try {
            localStorage.setItem("menuState", JSON.stringify(expanded));
        }
        catch (ignored) {
        }
    }, 100);
}
function applyMenuState() {
    try {
        var menuElement = $("#Menu"), menuWidget = menuElement.data("kendoPanelBar") || menuElement.data("tPanelBar"), menuState = JSON.parse(localStorage.getItem("menuState")), item;
        for (var i = 0; i < menuState.length; i++) {
            item = $("#Menu > li.save-state:eq(" + i + ")");
            if (menuState[i]) {
                menuWidget.expand(item);
            }
            else {
                menuWidget.collapse(item);
            }
        }
    }
    catch (ignored) {
    }
}
$(document).ready(function () {
    window.setTimeout(applyMenuState, 200);
});
//# sourceMappingURL=site.js.map