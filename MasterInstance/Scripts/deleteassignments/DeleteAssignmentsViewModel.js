var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DeleteAssignmentsViewModel = (function (_super) {
    __extends(DeleteAssignmentsViewModel, _super);
    function DeleteAssignmentsViewModel(gridSelector, waitWindowSelector, destroyAllUrl, unassignAllUrl) {
        var _this = _super.call(this) || this;
        _this.gridSelector = gridSelector;
        _this.waitWindowSelector = waitWindowSelector;
        _this.destroyAllUrl = destroyAllUrl;
        _this.unassignAllUrl = unassignAllUrl;
        _this.visible = false;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    DeleteAssignmentsViewModel.prototype.destroyAllClick = function () {
        var _this = this;
        var win = $(this.waitWindowSelector).data("kendoWindow");
        win.content("Aktionstage werden gelöscht.");
        win.open().center();
        $.post(this.destroyAllUrl)
            .done(function (data) {
            win.close();
            if (data != "ok") {
                alert(data);
            }
            else {
                $(_this.gridSelector).data("kendoGrid").dataSource.read();
            }
        });
    };
    DeleteAssignmentsViewModel.prototype.unassignAllClick = function () {
        var _this = this;
        var win = $(this.waitWindowSelector).data("kendoWindow");
        win.content("Zuweisungen werden entfernt.");
        win.open().center();
        $.post(this.unassignAllUrl)
            .done(function (data) {
            win.close();
            if (data != "ok") {
                alert(data);
            }
            else {
                $(_this.gridSelector).data("kendoGrid").dataSource.read();
            }
        });
    };
    return DeleteAssignmentsViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=DeleteAssignmentsViewModel.js.map