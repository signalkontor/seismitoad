﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

class DeleteAssignmentsViewModel extends kendo.data.ObservableObject {
    visible: boolean = false;

    constructor(
        private gridSelector: string,
        private waitWindowSelector: string,
        private destroyAllUrl: string,
        private unassignAllUrl: string) {
        super();
        super.init(this);
    }

    destroyAllClick() {
        var win = <kendo.ui.Window>$(this.waitWindowSelector).data("kendoWindow");
        win.content("Aktionstage werden gelöscht.")
        win.open().center();
        $.post(this.destroyAllUrl)
            .done((data) => {
                win.close();
                if (data != "ok") {
                    alert(data);
                } else {
                    $(this.gridSelector).data("kendoGrid").dataSource.read();
                }
            });
    }

    unassignAllClick() {
        var win = <kendo.ui.Window>$(this.waitWindowSelector).data("kendoWindow");
        win.content("Zuweisungen werden entfernt.")
        win.open().center();
        $.post(this.unassignAllUrl)
            .done((data) => {
                win.close();
                if (data != "ok") {
                    alert(data);
                } else {
                    $(this.gridSelector).data("kendoGrid").dataSource.read();
                }
            });
    }
}