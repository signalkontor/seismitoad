﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

interface IFaxMessagesFilterViewModel {
    DateFilter: number;
}

//interface ISelectedItem {
//    Text: string;
//    Value: number;
//}

class FaxMessagesSelectionViewModel extends kendo.data.ObservableObject {

    dateFilter: ISelectedItem = { Value: 0, Text: "Heute" };
    dateSource = new kendo.data.DataSource({
        data: [
            { Value: 1, Text: "Alle" },
            { Value: 0, Text: "Heute" },
            { Value: -1, Text: "Gestern" },
            { Value: -2, Text: "Vorgestern" },
            { Value: -3, Text: "Vor 3 Tagen" }
        ]
    });

    constructor() {
        super();
        super.init(this);
    }

    gridParams(): IFaxMessagesFilterViewModel {
        var selectedDate = <ISelectedItem>this.get("dateFilter");
        return { DateFilter: typeof (selectedDate) == "undefined" ? 1 : selectedDate.Value };
    }

    onItemsChange() {
        var grid = <kendo.ui.Grid>$("#faxMessages").data("kendoGrid");
        grid.dataSource.read();
    }
}