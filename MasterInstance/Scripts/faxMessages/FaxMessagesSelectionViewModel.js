var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var FaxMessagesSelectionViewModel = (function (_super) {
    __extends(FaxMessagesSelectionViewModel, _super);
    function FaxMessagesSelectionViewModel() {
        var _this = _super.call(this) || this;
        _this.dateFilter = { Value: 0, Text: "Heute" };
        _this.dateSource = new kendo.data.DataSource({
            data: [
                { Value: 1, Text: "Alle" },
                { Value: 0, Text: "Heute" },
                { Value: -1, Text: "Gestern" },
                { Value: -2, Text: "Vorgestern" },
                { Value: -3, Text: "Vor 3 Tagen" }
            ]
        });
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    FaxMessagesSelectionViewModel.prototype.gridParams = function () {
        var selectedDate = this.get("dateFilter");
        return { DateFilter: typeof (selectedDate) == "undefined" ? 1 : selectedDate.Value };
    };
    FaxMessagesSelectionViewModel.prototype.onItemsChange = function () {
        var grid = $("#faxMessages").data("kendoGrid");
        grid.dataSource.read();
    };
    return FaxMessagesSelectionViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=FaxMessagesSelectionViewModel.js.map