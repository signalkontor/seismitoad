var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function toYesNo(value) {
    return value !== null ? value === true ? "Ja" : "Nein" : "";
}
var SearchViewModel = (function (_super) {
    __extends(SearchViewModel, _super);
    function SearchViewModel(searchGridSelector, searchWindowSelector) {
        var _this = _super.call(this) || this;
        _this.searchGridSelector = searchGridSelector;
        _this.searchWindowSelector = searchWindowSelector;
        _this.countries = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Deutschland" },
            { value: 8, text: "Großbritannien" },
            { value: 6, text: "Italien" },
            { value: 4, text: "Niederlande" },
            { value: 5, text: "Polen" },
            { value: 3, text: "Schweiz" },
            { value: 7, text: "Spanien" },
            { value: 9, text: "Türkei" },
            { value: 2, text: "Österreich" },
            { value: 1000, text: "Anderes Land" }
        ];
        _this.shirtSizes = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "XS" },
            { value: 2, text: "S" },
            { value: 3, text: "M" },
            { value: 4, text: "L" },
            { value: 5, text: "XL" },
            { value: 6, text: "XXL" }
        ];
        _this.jeansWidths = [];
        _this.jeansLengths = [];
        _this.shoeSizes = [];
        _this.hairColors = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Blond" },
            { value: 2, text: "Braun" },
            { value: 3, text: "Grau" },
            { value: 4, text: "Rot" },
            { value: 5, text: "Schwarz" },
            { value: 6, text: "Keine" }
        ];
        _this.artisticAbilitiesSource = [
            { value: 0, text: "Nicht benötigt" },
            { value: 6, text: "Fotografieren" },
            { value: 7, text: "Graphic-Design" },
            { value: 3, text: "Jonglieren" },
            { value: 1, text: "Malen" },
            { value: 4, text: "Schauspielern" },
            { value: 5, text: "Singen" },
            { value: 2, text: "Zaubern" },
            { value: 8, text: "Sonstiges" }
        ];
        _this.gastronomicAbilitiesSource = [
            { value: 0, text: "Nicht benötigt" },
            { value: 3, text: "Barkeeper" },
            { value: 1, text: "Kellnern" },
            { value: 2, text: "Kochen" }
        ];
        _this.sportsSource = [
            { value: 0, text: "Nicht benötigt" },
            { value: 14, text: "Basketball" },
            { value: 11, text: "Bodybuilding" },
            { value: 7, text: "Fitness/Aerobic" },
            { value: 2, text: "Fußball" },
            { value: 6, text: "Golf" },
            { value: 5, text: "Inline skaten" },
            { value: 16, text: "Joggen" },
            { value: 15, text: "Kampfsport" },
            { value: 13, text: "Motorsport" },
            { value: 12, text: "Radfahren" },
            { value: 8, text: "Reiten" },
            { value: 3, text: "Schwimmen" },
            { value: 10, text: "Surfen" },
            { value: 4, text: "Snowboard" },
            { value: 1, text: "Tanzen" },
            { value: 9, text: "Tennis" }
        ];
        _this.otherAbilitiesSource = [
            { value: 0, text: "Nicht benötigt" },
            { value: 3, text: "Auto" },
            { value: 7, text: "DJ" },
            { value: 6, text: "Kinder-Animation" },
            { value: 4, text: "Kosmetik" },
            { value: 8, text: "Moderation" },
            { value: 2, text: "PC" },
            { value: 1, text: "Security" },
            { value: 5, text: "Tiere" }
        ];
        _this.driversLicenseClass1Source = [
            { value: "", text: "Nicht benötigt" },
            { value: "B", text: "B" },
            { value: "BE", text: "BE" },
            { value: "S", text: "S" },
            { value: "A", text: "A" },
            { value: "A1", text: "A1" },
            { value: "M", text: "M" },
            { value: "C", text: "C" },
            { value: "CE", text: "CE" },
            { value: "C1", text: "C1" },
            { value: "C1E", text: "C1E" },
            { value: "D", text: "D" },
            { value: "DE", text: "DE" },
            { value: "D1", text: "D1" },
            { value: "D1E", text: "D1E" },
            { value: "T", text: "T" },
            { value: "L", text: "L" }
        ];
        _this.driversLicenseClass2Source = [
            { value: "", text: "Nicht benötigt" },
            { value: "1", text: "1" },
            { value: "1A", text: "1A" },
            { value: "1B", text: "1B" },
            { value: "2", text: "2" },
            { value: "3", text: "3" },
            { value: "4", text: "4" },
            { value: "5", text: "5" }
        ];
        _this.educationSource = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Hauptschulabschluss" },
            { value: 2, text: "Realschulabschluss" },
            { value: 3, text: "Fachhochschulreife" },
            { value: 4, text: "Abitur" },
            { value: 5, text: "Kein Abschluss" }
        ];
        _this.apprenticeshipsSource = [
            { value: 0, text: "Beliebig" },
            { value: 8, text: "Keine Berufsausbildung" },
            { value: 4, text: "Gastronomisch" },
            { value: 1, text: "Kaufmännisch" },
            { value: 3, text: "Künstlerisch" },
            { value: 5, text: "Sozial" },
            { value: 6, text: "Technisch" },
            { value: 7, text: "Sonstiges" }
        ];
        _this.languageKnowledgeSource = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Keine" },
            { value: 2, text: "Grundkentnisse" },
            { value: 3, text: "Fortgeschritten" },
            { value: 4, text: "Fließend" },
            { value: 5, text: "Muttersprache" }
        ];
        _this.itKnowledgeSource = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Keine" },
            { value: 2, text: "Anwenderkenntnisse" },
            { value: 3, text: "Gute Kenntnisse" },
            { value: 4, text: "Sehr gute Kenntnisse" },
            { value: 5, text: "Experte" },
        ];
        _this.preferreTasksSource = [
            { value: 0, text: "Beliebig" },
            { value: 4, text: "Abverkauf" },
            { value: 7, text: "Animation" },
            { value: 10, text: "Auf/-Abbau" },
            { value: 12, text: "Barkeeping" },
            { value: 3, text: "Fachberatung" },
            { value: 5, text: "Gäste-/ Kundenbetreuung" },
            { value: 13, text: "Koch" },
            { value: 14, text: "Künstler" },
            { value: 6, text: "Messehost/ -ess" },
            { value: 9, text: "Model" },
            { value: 8, text: "Moderation" },
            { value: 1, text: "Sampling" },
            { value: 11, text: "Servicekraft" },
            { value: 2, text: "Verkostung" }
        ];
        _this.preferreTypesSource = [
            { value: 0, text: "Beliebig" },
            { value: 4, text: "Einzelpromotion" },
            { value: 1, text: "Messe/ Event" },
            { value: 5, text: "Mystery Shopping" },
            { value: 2, text: "Roadshow" },
            { value: 3, text: "Teampromotion" }
        ];
        _this.preferredWorkScheduleSource = [
            { value: 0, text: "Beliebig" },
            { value: 1, text: "Werktags" },
            { value: 2, text: "Abends" },
            { value: 3, text: "Am Wochenende" },
            { value: 4, text: "Ferien-/ Semesterferien" },
            { value: 5, text: "Immer" }
        ];
        _this.accommodationPossible1Source = [
            { value: 0, text: "Beliebig" },
            { value: 5, text: "Berlin" },
            { value: 8, text: "Frankfurt" },
            { value: 4, text: "Hamburg" },
            { value: 3, text: "Hannover" },
            { value: 2, text: "Köln" },
            { value: 9, text: "Leipzig" },
            { value: 6, text: "München" },
            { value: 1, text: "Ruhrgebiet" },
            { value: 7, text: "Stuttgart" }
        ];
        _this.trainingSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "MD/Training/_ReadCombobox",
                }
            },
        });
        _this.radiusSource = [
            { value: null, text: "keine Umkreissuche" },
            { value: 5, text: "bis 5 KM" },
            { value: 10, text: "bis 10 KM" },
            { value: 15, text: "bis 15 KM" },
            { value: 25, text: "bis 25 KM" },
            { value: 50, text: "bis 50 KM" },
            { value: 100, text: "bis 100 KM" },
            { value: 200, text: "bis 200 KM" },
            { value: 500, text: "bis 500 KM" }
        ];
        _this.selected = {};
        _this.preselectButtonEnabled = false;
        _this.searchResultLength = 0;
        _this.pc1Color = "#000";
        _this.pc2Color = "#ccc";
        _this.pc1Visible = true;
        var i;
        _this.campaignId = campaignId;
        _this.jeansWidths.push({ value: 0, text: "Beliebig" });
        for (i = 25; i <= 39; i++) {
            _this.jeansWidths.push({ value: i, text: "" + i });
        }
        _this.jeansLengths.push({ value: 0, text: "Beliebig" });
        for (i = 28; i <= 38; i += 2) {
            _this.jeansLengths.push({ value: i, text: "" + i });
        }
        _this.shoeSizes.push({ value: 0, text: "Beliebig" });
        for (i = 32; i <= 54; i++) {
            _this.shoeSizes.push({ value: i, text: "" + i });
        }
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    SearchViewModel.prototype.search = function () {
        var grid = $(this.searchGridSelector).data("kendoGrid");
        this.selected = {};
        grid.dataSource.query({ page: 1, pageSize: grid.dataSource.pageSize() });
    };
    SearchViewModel.prototype.preselect = function () {
        var _this = this;
        var parameters = this.searchParams();
        $(this.searchWindowSelector).block({ message: "Vorauswahl wird aktualisiert..." });
        $.post(appPath + "CM/CampaignEmployees/_Preselect", parameters)
            .fail(function () {
            alert("Es ist ein Fehler aufgetreten.");
        })
            .always(function () {
            $(_this.searchWindowSelector).unblock();
            var state = viewModel.get("state");
            _this.search();
            if (state === "Any" || state === "New") {
                viewModel.applyFilter(null);
            }
        });
    };
    SearchViewModel.prototype.close = function () {
        $(this.searchWindowSelector).data("kendoWindow").close();
    };
    SearchViewModel.prototype.pc1Click = function () {
        this.set("pc1Visible", true);
        this.set("pc1Color", "#000");
        this.set("pc2Color", "#ccc");
        this.set("postalCode", "");
    };
    SearchViewModel.prototype.pc2Click = function () {
        this.set("pc1Visible", false);
        this.set("pc1Color", "#ccc");
        this.set("pc2Color", "#000");
        this.set("postalCodeMin", "");
        this.set("postalCodeMax", "");
    };
    SearchViewModel.prototype.searchParams = function () {
        var params = this.toJSON();
        delete params["countries"];
        delete params["shirtSizes"];
        delete params["jeansWidths"];
        delete params["jeansLengths"];
        delete params["hairColors"];
        delete params["artisticAbilitiesSource"];
        delete params["gastronomicAbilitiesSource"];
        delete params["sportsSource"];
        delete params["otherAbilitiesSource"];
        delete params["driversLicenseClass1Source"];
        delete params["driversLicenseClass2Source"];
        delete params["educationSource"];
        delete params["apprenticeshipsSource"];
        delete params["languageKnowledgeSource"];
        delete params["itKnowledgeSource"];
        delete params["preferreTasksSource"];
        delete params["preferreTypesSource"];
        delete params["preferredWorkScheduleSource"];
        delete params["accommodationPossible1Source"];
        delete params["trainingSource"];
        delete params["radiusSource"];
        delete params["searchWindowSelector"];
        delete params["searchGridSelector"];
        delete params["selected"];
        delete params["preselectButtonEnabled"];
        delete params["searchResultLength"];
        return params;
    };
    SearchViewModel.prototype.gridDataBound = function (e) {
        var grid = e.sender;
        var currentRecords = grid.dataSource.view();
        this.set("searchResultLength", grid.dataSource.total());
        for (var i = 0; i < currentRecords.length; i++) {
            var uid = currentRecords[i].uid;
            var employeeId = currentRecords[i].EmployeeId;
            var checkbox = grid.tbody.find("tr[data-uid='" + uid + "'] input[type=checkbox]");
            if (this.selected.hasOwnProperty(employeeId)) {
                var checked = this.selected[employeeId];
                if (checked) {
                    checkbox.prop("checked", true);
                }
            }
        }
        this.set("preselectButtonEnabled", currentRecords.length > 0);
    };
    SearchViewModel.prototype.gridColumnChanged = function () {
        var grid = $(this.searchGridSelector).data("kendoGrid"), columns = $.grep(grid.columns, function (e) { return typeof (e.hidden) === "undefined" || e.hidden === false; });
        $.ajax({
            type: "POST",
            dataType: "json",
            url: appPath + "CampaignEmployees/_SaveColumns/" + campaignId,
            data: "columns=" + $.map(columns, function (e) { return e.field; }).join(","),
            cache: false,
            global: false,
        });
    };
    SearchViewModel.prototype.formatMultiSelect = function (value, dataSource) {
        var splitted;
        if (typeof (value) === "undefined" || value == null) {
            return "";
        }
        splitted = $.map($.grep(value.split(";"), function (element) { return element !== ""; }), function (element) { return parseInt(element); });
        return $.map($.grep(dataSource, function (element) { return $.inArray(element.value, splitted) > -1; }), function (element) { return element.text; }).join(", ");
    };
    SearchViewModel.prototype.formatSingleSelect = function (value, dataSource) {
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].value == value) {
                return dataSource[i].text;
            }
        }
        return "";
    };
    SearchViewModel.prototype.formatMultiSelectString = function (value, dataSource) {
        var splitted;
        if (typeof (value) === "undefined" || value == null) {
            return "";
        }
        splitted = $.grep(value.split(";"), function (element) { return element !== ""; });
        return $.map($.grep(dataSource, function (element) { return $.inArray("" + element.value, splitted) > -1; }), function (element) { return element.text; }).join(", ");
    };
    SearchViewModel.prototype.formatCountry = function (value) {
        return this.formatSingleSelect(value, this.countries);
    };
    SearchViewModel.prototype.formatShirtSize = function (value) {
        return this.formatSingleSelect(value, this.shirtSizes);
    };
    SearchViewModel.prototype.formatHairColor = function (value) {
        return this.formatSingleSelect(value, this.hairColors);
    };
    SearchViewModel.prototype.formatArtisticAbilities = function (value) {
        return this.formatMultiSelect(value, this.artisticAbilitiesSource);
    };
    SearchViewModel.prototype.formatGastronomicAbilities = function (value) {
        return this.formatMultiSelect(value, this.gastronomicAbilitiesSource);
    };
    SearchViewModel.prototype.formatSports = function (value) {
        return this.formatMultiSelect(value, this.sportsSource);
    };
    SearchViewModel.prototype.formatOtherAbilities = function (value) {
        return this.formatMultiSelect(value, this.otherAbilitiesSource);
    };
    SearchViewModel.prototype.formatDriversLicenseClass1 = function (value) {
        return this.formatMultiSelectString(value, this.driversLicenseClass1Source);
    };
    SearchViewModel.prototype.formatDriversLicenseClass2 = function (value) {
        return this.formatMultiSelectString(value, this.driversLicenseClass2Source);
    };
    SearchViewModel.prototype.formatEducation = function (value) {
        return this.formatSingleSelect(value, this.educationSource);
    };
    SearchViewModel.prototype.formatApprenticeships = function (value) {
        return this.formatSingleSelect(value, this.apprenticeshipsSource);
    };
    SearchViewModel.prototype.formatLanguage = function (value) {
        return this.formatSingleSelect(value, this.languageKnowledgeSource);
    };
    SearchViewModel.prototype.formatIT = function (value) {
        return this.formatSingleSelect(value, this.itKnowledgeSource);
    };
    SearchViewModel.prototype.formatPreferredTasks = function (value) {
        return this.formatSingleSelect(value, this.preferreTasksSource);
    };
    SearchViewModel.prototype.formatPreferredTypes = function (value) {
        return this.formatSingleSelect(value, this.preferreTypesSource);
    };
    SearchViewModel.prototype.formatPreferredWorkSchedule = function (value) {
        return this.formatSingleSelect(value, this.preferredWorkScheduleSource);
    };
    SearchViewModel.prototype.formatAccommodationPossible1 = function (value) {
        return this.formatSingleSelect(value, this.accommodationPossible1Source);
    };
    return SearchViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=employee.js.map