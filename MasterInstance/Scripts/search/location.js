var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var LocationSearchViewModel = (function (_super) {
    __extends(LocationSearchViewModel, _super);
    function LocationSearchViewModel(searchGridSelector, searchWindowSelector) {
        var _this = _super.call(this) || this;
        _this.searchGridSelector = searchGridSelector;
        _this.searchWindowSelector = searchWindowSelector;
        _this.locationTypeSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "MD/Location/_LocationTypes",
                }
            },
        });
        _this.stateProvinceSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "MD/Location/_StateProvinces",
                }
            },
        });
        _this.locationGroupSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "MD/Location/_LocationGroups",
                }
            },
        });
        _this.radiusSource = [
            { value: null, text: "keine Umkreissuche" },
            { value: 5, text: "bis 5 KM" },
            { value: 10, text: "bis 10 KM" },
            { value: 15, text: "bis 15 KM" },
            { value: 25, text: "bis 25 KM" },
            { value: 50, text: "bis 50 KM" },
            { value: 100, text: "bis 100 KM" },
            { value: 200, text: "bis 200 KM" },
            { value: 500, text: "bis 500 KM" }
        ];
        _this.selected = {};
        _this.preselectButtonEnabled = false;
        _this.pc1Color = "#000";
        _this.pc2Color = "#ccc";
        _this.pc1Visible = true;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    LocationSearchViewModel.prototype.search = function () {
        var grid = $(this.searchGridSelector).data("kendoGrid");
        this.selected = {};
        grid.dataSource.query({ page: 1, pageSize: grid.dataSource.pageSize() });
    };
    LocationSearchViewModel.prototype.close = function () {
        $(this.searchWindowSelector).data("kendoWindow").close();
    };
    LocationSearchViewModel.prototype.pc1Click = function () {
        this.set("pc1Visible", true);
        this.set("pc1Color", "#000");
        this.set("pc2Color", "#ccc");
        this.set("postalCode", "");
    };
    LocationSearchViewModel.prototype.pc2Click = function () {
        this.set("pc1Visible", false);
        this.set("pc1Color", "#ccc");
        this.set("pc2Color", "#000");
        this.set("postalCodeMin", "");
        this.set("postalCodeMax", "");
    };
    LocationSearchViewModel.prototype.searchParams = function () {
        var params = this.toJSON();
        delete params["locationTypeSource"];
        delete params["stateProvinceSource"];
        delete params["locationGroupSource"];
        delete params["radiusSource"];
        delete params["searchWindowSelector"];
        delete params["searchGridSelector"];
        delete params["selected"];
        return params;
    };
    return LocationSearchViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=location.js.map