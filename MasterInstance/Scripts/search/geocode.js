function initializeGeocoding() {
    var input = document.getElementById('location-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }
        if (typeof (searchViewModel) !== "undefined") {
            searchViewModel.set("lng", place.geometry.location.lng());
            searchViewModel.set("lat", place.geometry.location.lat());
            searchViewModel.set("location", $("#location-input").val());
        }
        else if (typeof (locationSearchViewModel) !== "undefined") {
            locationSearchViewModel.set("lng", place.geometry.location.lng());
            locationSearchViewModel.set("lat", place.geometry.location.lat());
            locationSearchViewModel.set("location", $("#location-input").val());
        }
    });
}
google.maps.event.addDomListener(window, 'load', initializeGeocoding);
//# sourceMappingURL=geocode.js.map