﻿//function toYesNo(value: boolean) {
//    return value !== null ? value === true ? "Ja" : "Nein" : "";
//}

class LocationSearchViewModel extends kendo.data.ObservableObject {
    state: number;
    locationTypeId: number;
    name: string;
    name2: string;
    street: string;
    street2: string;
    postalCode: string;
    postalCodeMin: string;
    postalCodeMax: string;
    city: string;
    stateProvinceId: number;
    locationGroupId: number;
    notes: string;
    centerManagementStreet: string;
    centerManagementPostalCode: string;
    centerManagementCity: string;
    phone: string;
    fax: string;
    email: string;
    website: string;
    visitorFrequency: number;
    area: number;
    sellingArea: number;
    campaignAreaPrices: string;
    audience: number;
    opening: number;
    catchmentArea: number;
    parking: number;
    outdoorArea: boolean;
    retailers: string;
    wiFiAvailable: boolean;
    conferenceRoomSize: number;
    costTransferPossible: boolean;
    roomRates: string;
    conferenceEquipmentCost: string;
    conferencePackageCost: string;
    location: string;
    lng: string;
    lat: string;
    radius: number;

    // DataSources
    locationTypeSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "MD/Location/_LocationTypes",
            }
        },
    });

    stateProvinceSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "MD/Location/_StateProvinces",
            }
        },
    });

    locationGroupSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "MD/Location/_LocationGroups",
            }
        },
    });

    radiusSource = [
        { value: null, text: "keine Umkreissuche" },
        { value: 5, text: "bis 5 KM" },
        { value: 10, text: "bis 10 KM" },
        { value: 15, text: "bis 15 KM" },
        { value: 25, text: "bis 25 KM" },
        { value: 50, text: "bis 50 KM" },
        { value: 100, text: "bis 100 KM" },
        { value: 200, text: "bis 200 KM" },
        { value: 500, text: "bis 500 KM" }
    ];

    private selected: { [index: string]: boolean } = {};
    private campaignId: number;
    preselectButtonEnabled = false;
    pc1Color = "#000";
    pc2Color = "#ccc";
    pc1Visible = true;

    constructor(private searchGridSelector: string, private searchWindowSelector: string) {
        super();
        super.init(this);
    }

    search() {
        var grid = <kendo.ui.Grid>$(this.searchGridSelector).data("kendoGrid");
        this.selected = {};
        // So auf Seite 1 gehen und damit auch alle Filter und Sortierungen löschen.
        grid.dataSource.query({ page: 1, pageSize: grid.dataSource.pageSize() });
    }

    close() {
        $(this.searchWindowSelector).data("kendoWindow").close();
    }

    pc1Click() {
        this.set("pc1Visible", true);
        this.set("pc1Color", "#000");
        this.set("pc2Color", "#ccc");
        this.set("postalCode", "");
    }

    pc2Click() {
        this.set("pc1Visible", false);
        this.set("pc1Color", "#ccc");
        this.set("pc2Color", "#000");
        this.set("postalCodeMin", "");
        this.set("postalCodeMax", "");
    }

    searchParams() {
        var params = this.toJSON();
        delete params["locationTypeSource"];
        delete params["stateProvinceSource"];
        delete params["locationGroupSource"];
        delete params["radiusSource"];
        delete params["searchWindowSelector"];
        delete params["searchGridSelector"];
        delete params["selected"];
        return params;
    }
}