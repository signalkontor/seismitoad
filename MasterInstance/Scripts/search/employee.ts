﻿interface IDataSourceItem {
    value: number;
    text: string;
}

function toYesNo(value: boolean) {
    return value !== null ? value === true ? "Ja" : "Nein" : "";
}

class SearchViewModel extends kendo.data.ObservableObject {
    // Persönliche Daten
    state: number;
    title: string;
    firstname: string;
    lastname: string;
    ageMin: number;
    ageMax: number;
    email: string;
    skype: string;
    website: string;
    mobile: string;
    phone: string;
    fax: string;
    street: string;
    postalCode: string;
    postalcodeMin: string;
    postalcodeMax: string;
    city: string;
    country: number;
    personalDataRemark: string;

    // Bodyprofil und Talente
    heightMin: number;
    heightMax: number;
    sizeMin: number;
    sizeMax: number;
    shirtSize: number;
    jeansWidth: number;
    jeansLength: number;
    shoeSize: number;
    hairColor: number;
    visibleTattoos: boolean;
    facialPiercing: boolean;
    artisticAbilities: number;
    gastronomicAbilities: number;
    sports: number;
    otherAbilities: number;
    otherAbilitiesText: string;

    // Vertrag und Gewerbe
    skeletonContract: boolean;
    copyTradeLicense: boolean;
    validTradeLicense: boolean;
    taxOfficeLocation: string;
    taxNumber: string;
    turnoverTaxDeductible: boolean;
    freelancerQuestionaire: boolean;
    healthCertificate: boolean;
    copyHealthCertificate: boolean;
    copyIdentityCard: number;
    comment: string;

    // Mobilität
    driversLicense: boolean;
    driversLicenseSince: number;
    driversLicenseClass1: string;
    driversLicenseClass2: string;
    driversLicenseRemark: string;
    hasDigitalDriversCard: boolean;
    isCarAvailable: boolean;
    isBahncardAvailable: boolean;

    // Ausbildung und Qualifikation
    education: number;
    apprenticeships: number;
    otherCompletedApprenticeships: string;
    studies: number;
    studiesDetails: string;
    workExperience: string;
    languagesGerman: number;
    languagesEnglish: number;
    languagesSpanish: number;
    languagesFrench: number;
    languagesItalian: number;
    languagesTurkish: number;
    languagesOther: string;
    softwareKnownledge: number;
    softwareKnownledgeDetails: string;
    hardwareKnownledge: number;
    hardwareKnownledgeDetails: string;
    internalTraining: string;
    externalTraining: string;
    teamleaderQualification: boolean;

    // Auftragswunsch
    preferredTasks: string;
    preferredTypes: string;
    preferredWorkSchedule: string;
    travelWillingness: boolean;
    accommodationPossible1: string;
    accommodationPossible2: string;

    // Projekte / Standort
    project: string;
    training: string;
    location: string;
    lng: string;
    lat: string;
    radius: number;

    // DataSources
    countries = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Deutschland" },
        { value: 8, text: "Großbritannien" },
        { value: 6, text: "Italien" },
        { value: 4, text: "Niederlande" },
        { value: 5, text: "Polen" },
        { value: 3, text: "Schweiz" },
        { value: 7, text: "Spanien" },
        { value: 9, text: "Türkei" },
        { value: 2, text: "Österreich" },
        { value: 1000, text: "Anderes Land" }
    ];

    shirtSizes = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "XS" },
        { value: 2, text: "S" },
        { value: 3, text: "M" },
        { value: 4, text: "L" },
        { value: 5, text: "XL" },
        { value: 6, text: "XXL" }
    ];

    jeansWidths = [];
    jeansLengths = [];
    shoeSizes = [];

    hairColors = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Blond" },
        { value: 2, text: "Braun" },
        { value: 3, text: "Grau" },
        { value: 4, text: "Rot" },
        { value: 5, text: "Schwarz" },
        { value: 6, text: "Keine" }
    ];

    artisticAbilitiesSource = [
        { value: 0, text: "Nicht benötigt" },
        { value: 6, text: "Fotografieren" },
        { value: 7, text: "Graphic-Design" },
        { value: 3, text: "Jonglieren" },
        { value: 1, text: "Malen" },
        { value: 4, text: "Schauspielern" },
        { value: 5, text: "Singen" },
        { value: 2, text: "Zaubern" },
        { value: 8, text: "Sonstiges" }
    ];

    gastronomicAbilitiesSource = [
        { value: 0, text: "Nicht benötigt" },
        { value: 3, text: "Barkeeper" },
        { value: 1, text: "Kellnern" },
        { value: 2, text: "Kochen" }
    ];

    sportsSource = [
        { value: 0, text: "Nicht benötigt" },
        { value: 14, text: "Basketball" },
        { value: 11, text: "Bodybuilding" },
        { value: 7, text: "Fitness/Aerobic" },
        { value: 2, text: "Fußball" },
        { value: 6, text: "Golf" },
        { value: 5, text: "Inline skaten" },
        { value: 16, text: "Joggen" },
        { value: 15, text: "Kampfsport" },
        { value: 13, text: "Motorsport" },
        { value: 12, text: "Radfahren" },
        { value: 8, text: "Reiten" },
        { value: 3, text: "Schwimmen" },
        { value: 10, text: "Surfen" },
        { value: 4, text: "Snowboard" },
        { value: 1, text: "Tanzen" },
        { value: 9, text: "Tennis" }
    ];

    otherAbilitiesSource = [
        { value: 0, text: "Nicht benötigt" },
        { value: 3, text: "Auto" },
        { value: 7, text: "DJ" },
        { value: 6, text: "Kinder-Animation" },
        { value: 4, text: "Kosmetik" },
        { value: 8, text: "Moderation" },
        { value: 2, text: "PC" },
        { value: 1, text: "Security" },
        { value: 5, text: "Tiere" }
    ];

    driversLicenseClass1Source = [
        { value: "", text: "Nicht benötigt" },
        { value: "B", text: "B" },
        { value: "BE", text: "BE" },
        { value: "S", text: "S" },
        { value: "A", text: "A" },
        { value: "A1", text: "A1" },
        { value: "M", text: "M" },
        { value: "C", text: "C" },
        { value: "CE", text: "CE" },
        { value: "C1", text: "C1" },
        { value: "C1E", text: "C1E" },
        { value: "D", text: "D" },
        { value: "DE", text: "DE" },
        { value: "D1", text: "D1" },
        { value: "D1E", text: "D1E" },
        { value: "T", text: "T" },
        { value: "L", text: "L" }
    ];

    driversLicenseClass2Source = [
        { value: "", text: "Nicht benötigt" },
        { value: "1", text: "1" },
        { value: "1A", text: "1A" },
        { value: "1B", text: "1B" },
        { value: "2", text: "2" },
        { value: "3", text: "3" },
        { value: "4", text: "4" },
        { value: "5", text: "5" }
    ];

    educationSource = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Hauptschulabschluss" },
        { value: 2, text: "Realschulabschluss" },
        { value: 3, text: "Fachhochschulreife" },
        { value: 4, text: "Abitur" },
        { value: 5, text: "Kein Abschluss" }
    ];

    apprenticeshipsSource = [
        { value: 0, text: "Beliebig" },
        { value: 8, text: "Keine Berufsausbildung" },
        { value: 4, text: "Gastronomisch" },
        { value: 1, text: "Kaufmännisch" },
        { value: 3, text: "Künstlerisch" },
        { value: 5, text: "Sozial" },
        { value: 6, text: "Technisch" },
        { value: 7, text: "Sonstiges" }
    ];

    languageKnowledgeSource = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Keine" },
        { value: 2, text: "Grundkentnisse" },
        { value: 3, text: "Fortgeschritten" },
        { value: 4, text: "Fließend" },
        { value: 5, text: "Muttersprache" }
    ];

    itKnowledgeSource = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Keine" },
        { value: 2, text: "Anwenderkenntnisse" },
        { value: 3, text: "Gute Kenntnisse" },
        { value: 4, text: "Sehr gute Kenntnisse" },
        { value: 5, text: "Experte" },
    ];

    preferreTasksSource = [
        { value: 0, text: "Beliebig" },
        { value: 4, text: "Abverkauf" },
        { value: 7, text: "Animation" },
        { value: 10, text: "Auf/-Abbau" },
        { value: 12, text: "Barkeeping" },
        { value: 3, text: "Fachberatung" },
        { value: 5, text: "Gäste-/ Kundenbetreuung" },
        { value: 13, text: "Koch" },
        { value: 14, text: "Künstler" },
        { value: 6, text: "Messehost/ -ess" },
        { value: 9, text: "Model" },
        { value: 8, text: "Moderation" },
        { value: 1, text: "Sampling" },
        { value: 11, text: "Servicekraft" },
        { value: 2, text: "Verkostung" }
    ];

    preferreTypesSource = [
        { value: 0, text: "Beliebig" },
        { value: 4, text: "Einzelpromotion" },
        { value: 1, text: "Messe/ Event" },
        { value: 5, text: "Mystery Shopping" },
        { value: 2, text: "Roadshow" },
        { value: 3, text: "Teampromotion" }
    ];

    preferredWorkScheduleSource = [
        { value: 0, text: "Beliebig" },
        { value: 1, text: "Werktags" },
        { value: 2, text: "Abends" },
        { value: 3, text: "Am Wochenende" },
        { value: 4, text: "Ferien-/ Semesterferien" },
        { value: 5, text: "Immer" }
    ];

    accommodationPossible1Source = [
        { value: 0, text: "Beliebig" },
        { value: 5, text: "Berlin" },
        { value: 8, text: "Frankfurt" },
        { value: 4, text: "Hamburg" },
        { value: 3, text: "Hannover" },
        { value: 2, text: "Köln" },
        { value: 9, text: "Leipzig" },
        { value: 6, text: "München" },
        { value: 1, text: "Ruhrgebiet" },
        { value: 7, text: "Stuttgart" }
    ];

    trainingSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "MD/Training/_ReadCombobox",
            }
        },
    });

    radiusSource = [
        { value: null, text: "keine Umkreissuche" },
        { value: 5, text: "bis 5 KM" },
        { value: 10, text: "bis 10 KM" },
        { value: 15, text: "bis 15 KM" },
        { value: 25, text: "bis 25 KM" },
        { value: 50, text: "bis 50 KM" },
        { value: 100, text: "bis 100 KM" },
        { value: 200, text: "bis 200 KM" },
        { value: 500, text: "bis 500 KM" }
    ];

    private selected: { [index: string]: boolean } = {};
    private campaignId: number;
    preselectButtonEnabled = false;
    searchResultLength = 0;
    pc1Color = "#000";
    pc2Color = "#ccc";
    pc1Visible = true;

    constructor(
        private searchGridSelector: string,
        private searchWindowSelector: string) {
        super();

        var i;
        this.campaignId = campaignId;

        this.jeansWidths.push({ value: 0, text: "Beliebig" });
        for (i = 25; i <= 39; i++) {
            this.jeansWidths.push({ value: i, text: "" + i });
        }
        this.jeansLengths.push({ value: 0, text: "Beliebig" });
        for (i = 28; i <= 38; i += 2) {
            this.jeansLengths.push({ value: i, text: "" + i });
        }
        this.shoeSizes.push({ value: 0, text: "Beliebig" });
        for (i = 32; i <= 54; i++) {
            this.shoeSizes.push({ value: i, text: "" + i });
        }
        super.init(this);
    }

    search() {
        var grid = <kendo.ui.Grid>$(this.searchGridSelector).data("kendoGrid");
        this.selected = {};
        // So auf Seite 1 gehen und damit auch alle Filter und Sortierungen löschen.
        grid.dataSource.query({ page: 1, pageSize: grid.dataSource.pageSize() });
    }

    preselect() {
        var parameters = this.searchParams();
        $(this.searchWindowSelector).block({ message: "Vorauswahl wird aktualisiert..." });
        $.post(appPath + "CM/CampaignEmployees/_Preselect", parameters)
            .fail(() => {
                alert("Es ist ein Fehler aufgetreten.");
            })
            .always(() => {
                $(this.searchWindowSelector).unblock();
                var state = viewModel.get("state");
                this.search();
                if (state === "Any" || state === "New") {
                    viewModel.applyFilter(null);
                }
            });
    }

    close() {
        $(this.searchWindowSelector).data("kendoWindow").close();
    }

    pc1Click() {
        this.set("pc1Visible", true);
        this.set("pc1Color", "#000");
        this.set("pc2Color", "#ccc");
        this.set("postalCode", "");
    }

    pc2Click() {
        this.set("pc1Visible", false);
        this.set("pc1Color", "#ccc");
        this.set("pc2Color", "#000");
        this.set("postalCodeMin", "");
        this.set("postalCodeMax", "");
    }

    searchParams() {
        var params = this.toJSON();
        delete params["countries"];
        delete params["shirtSizes"];
        delete params["jeansWidths"];
        delete params["jeansLengths"];
        delete params["hairColors"];
        delete params["artisticAbilitiesSource"];
        delete params["gastronomicAbilitiesSource"];
        delete params["sportsSource"];
        delete params["otherAbilitiesSource"];
        delete params["driversLicenseClass1Source"];
        delete params["driversLicenseClass2Source"];
        delete params["educationSource"];
        delete params["apprenticeshipsSource"];
        delete params["languageKnowledgeSource"];
        delete params["itKnowledgeSource"];
        delete params["preferreTasksSource"];
        delete params["preferreTypesSource"];
        delete params["preferredWorkScheduleSource"];
        delete params["accommodationPossible1Source"];
        delete params["trainingSource"];
        delete params["radiusSource"];
        delete params["searchWindowSelector"];
        delete params["searchGridSelector"];
        delete params["selected"];
        delete params["preselectButtonEnabled"];
        delete params["searchResultLength"];
        return params;
    }

    gridDataBound(e: kendo.ui.GridDataBoundEvent) {
        var grid = e.sender;
        var currentRecords = grid.dataSource.view();
        this.set("searchResultLength", grid.dataSource.total());
        for (var i = 0; i < currentRecords.length; i++) {
            var uid = currentRecords[i].uid;
            var employeeId = currentRecords[i].EmployeeId;
            var checkbox = grid.tbody.find("tr[data-uid='" + uid + "'] input[type=checkbox]");

            // Checkbox deaktivieren, falls der aktuelle Status nicht dem angezeigten Status entspricht.
            if (this.selected.hasOwnProperty(employeeId)) {
                var checked = this.selected[employeeId];
                if (checked) {
                    checkbox.prop("checked", true);
                }
            }
        }
        this.set("preselectButtonEnabled", currentRecords.length > 0);
    }

    gridColumnChanged() {
        var grid = <kendo.ui.Grid>$(this.searchGridSelector).data("kendoGrid"),
            columns = $.grep(grid.columns, e => typeof (e.hidden) === "undefined" || e.hidden === false);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: appPath + "CampaignEmployees/_SaveColumns/" + campaignId,
            data: "columns=" + $.map(columns, e => e.field).join(","),
            cache: false,
            global: false, // daduch wird der globale ajaxError-Handler nicht getriggert, wenn die o.g. Datei nicht existiert
        });
    }

    private formatMultiSelect(value: string, dataSource: IDataSourceItem[]): string {
        var splitted: number[];

        if (typeof (value) === "undefined" || value == null) {
            return "";
        }

        splitted = $.map($.grep(value.split(";"), element => element !== ""), (element: string) => parseInt(element));

        return $.map(
            $.grep(dataSource, (element: IDataSourceItem) => $.inArray(element.value, splitted) > -1),
            (element: IDataSourceItem) => element.text).join(", ");
    }

    private formatSingleSelect(value: number, dataSource: IDataSourceItem[]): string {
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].value == value) {
                return dataSource[i].text;
            }
        }
        return "";
    }

    private formatMultiSelectString(value: string, dataSource: any[]): string {
        var splitted: string[];

        if (typeof (value) === "undefined" || value == null) {
            return "";
        }

        splitted = $.grep(value.split(";"), element => element !== "");

        return $.map(
            $.grep(dataSource, (element: IDataSourceItem) => $.inArray("" + element.value, splitted) > -1),
            (element: IDataSourceItem) => element.text).join(", ");
    }

    formatCountry(value: number) {
        return this.formatSingleSelect(value, this.countries);
    }

    formatShirtSize(value: number) {
        return this.formatSingleSelect(value, this.shirtSizes);
    }

    formatHairColor(value: number) {
        return this.formatSingleSelect(value, this.hairColors);
    }

    formatArtisticAbilities(value: string) {
        return this.formatMultiSelect(value, this.artisticAbilitiesSource);
    }

    formatGastronomicAbilities(value: string) {
        return this.formatMultiSelect(value, this.gastronomicAbilitiesSource);
    }

    formatSports(value: string) {
        return this.formatMultiSelect(value, this.sportsSource);
    }

    formatOtherAbilities(value: string) {
        return this.formatMultiSelect(value, this.otherAbilitiesSource);
    }

    formatDriversLicenseClass1(value: string) {
        return this.formatMultiSelectString(value, this.driversLicenseClass1Source);
    }

    formatDriversLicenseClass2(value: string) {
        return this.formatMultiSelectString(value, this.driversLicenseClass2Source);
    }

    formatEducation(value: number) {
        return this.formatSingleSelect(value, this.educationSource);
    }

    formatApprenticeships(value: number) {
        return this.formatSingleSelect(value, this.apprenticeshipsSource);
    }

    formatLanguage(value: number) {
        return this.formatSingleSelect(value, this.languageKnowledgeSource);
    }

    formatIT(value: number) {
        return this.formatSingleSelect(value, this.itKnowledgeSource);
    }

    formatPreferredTasks(value: number) {
        return this.formatSingleSelect(value, this.preferreTasksSource);
    }

    formatPreferredTypes(value: number) {
        return this.formatSingleSelect(value, this.preferreTypesSource);
    }

    formatPreferredWorkSchedule(value: number) {
        return this.formatSingleSelect(value, this.preferredWorkScheduleSource);
    }

    formatAccommodationPossible1(value: number) {
        return this.formatSingleSelect(value, this.accommodationPossible1Source);
    }
}