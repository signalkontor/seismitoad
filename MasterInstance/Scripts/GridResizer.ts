﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="kendo.all.d.ts" />

class GridResizer {
    constructor(
        private hSplitter: JQuery,
        private gridElement: JQuery,
        private callback: (height: number) => void = (height: number) => { },
        private correction = 60) {
        window["_onResize"] = $.proxy(this.resize, this);
    }

    resize() {
        var height = this.hSplitter.height() - this.correction;
        this.gridElement.find(".k-grid-content").height(height - 65);
        this.gridElement.height(height);
        this.gridElement.data("kendoGrid").refresh();
        this.callback(height);
    }
} 