var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var searchViewModel = new SearchViewModel("#eg", "#search-window");
var EmployeeListViewModel = (function (_super) {
    __extends(EmployeeListViewModel, _super);
    function EmployeeListViewModel() {
        var _this = _super.call(this) || this;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    EmployeeListViewModel.prototype.nameOrEmailSearchButtonClick = function () {
        this.set("searchByNameOrEmail", true);
        searchViewModel.search();
    };
    EmployeeListViewModel.prototype.fullSearchButtonClick = function () {
        this.set("nameOrEmail", "");
        this.set("searchByNameOrEmail", false);
        var window = $("#search-window").data("kendoWindow");
        window.center().open();
    };
    EmployeeListViewModel.prototype.searchParams = function () {
        if (this.get("searchByNameOrEmail")) {
            return { nameOrMail: this.get("nameOrEmail") };
        }
        else {
            return searchViewModel.searchParams();
        }
    };
    EmployeeListViewModel.prototype.reset = function () {
        gridStateSaver.clearState();
        window.location.reload();
    };
    EmployeeListViewModel.prototype.onEdit = function (e) {
        if ([0, 100, 200, 300].indexOf(e.model["State"]) < 0) {
            e.sender.closeCell();
        }
    };
    EmployeeListViewModel.prototype.onError = function () {
        $("#serverErrorWindow").data("kendoWindow").center().open();
    };
    return EmployeeListViewModel;
}(kendo.data.ObservableObject));
var userKey;
var campaignId = 0;
var employeeListViewModel = new EmployeeListViewModel();
$(document).ready(function () {
    $("#eg .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
    $("#additional-toolbar-items-template").remove();
    $("#eg").on("click", ".change-password", function (e) {
        var grid = $("#eg").data("kendoGrid"), data = grid.dataItem($(e.target).closest("tr"));
        e.preventDefault();
        userKey = data["ProviderUserKey"];
        $("#user").html(data["Firstname"] + " " + data["Lastname"]);
        $("#errorMsg").hide();
        $("#pwWnd").data("kendoWindow").center().open();
    });
    $("#saveBtn").click(function () {
        var window = $("#pwWnd").data("kendoWindow");
        var pw = document.getElementById("pw").value;
        $.ajax({
            url: appPath + "MD/Employee/_ChangePassword",
            type: "POST",
            data: { password: pw, userKey: userKey },
            global: false,
        })
            .done(function () {
            alert("Passwort geändert!");
            window.close();
        })
            .fail(function () {
            $("#errorMsg").show();
        });
    });
    gridStateSaver = new GridStateSaver($("#eg").data("kendoGrid"), true, function () { kendo.bind("#search-actions", employeeListViewModel); }, function (additionalState) {
        employeeListViewModel.set("nameOrEmail", additionalState.nameOrEmail);
        employeeListViewModel.set("searchByNameOrEmail", additionalState.searchByNameOrEmail);
        for (var property in additionalState.searchViewModel) {
            if (additionalState.searchViewModel.hasOwnProperty(property)) {
                searchViewModel.set(property, additionalState.searchViewModel[property]);
            }
        }
    });
    window.setTimeout(function () {
        var gridResizer = new GridResizer($("#hSplitter"), $("#eg"), function (height) {
            var grid = $("#eg").data("kendoGrid"), total, rows;
            if (!gridStateSaver.restoredData) {
                total = $("#eg .k-grid-content").height();
                rows = Math.floor(total / 32) - 3;
                grid.dataSource.pageSize(rows);
            }
            grid.refresh();
        });
        gridResizer.resize();
        kendo.bind("#search-window", searchViewModel);
        $("#infoIcon").click(function () {
            var window = $("#infoWnd").data("kendoWindow");
            window.center().open();
        });
    }, 0);
    function saveState() {
        gridStateSaver.saveAdditionalState({
            nameOrEmail: employeeListViewModel.get("nameOrEmail"),
            searchByNameOrEmail: employeeListViewModel.get("searchByNameOrEmail"),
            searchViewModel: searchViewModel.searchParams()
        });
    }
    employeeListViewModel.bind("change", saveState);
    searchViewModel.bind("change", saveState);
    $("#quicksearch").keyup(function (e) {
        if (e.keyCode === 13) {
            employeeListViewModel.nameOrEmailSearchButtonClick();
        }
    });
});
function keyDownEmployee(e) {
    if (e.keyCode === 13) {
        e.preventDefault();
        return false;
    }
    return true;
}
function keyupEmployee(e) {
    if (e.keyCode === 13) {
        e.preventDefault();
        employeeListViewModel.nameOrEmailSearchButtonClick();
    }
    return false;
}
var importedUserDate = new Date(1900, 1, 1);
function formatLastLogin(lastLogin) {
    if (lastLogin === null) {
        return "";
    }
    return importedUserDate >= lastLogin
        ? "Datenimport"
        : kendo.format("{0:dd.MM.yyyy HH:mm}", lastLogin);
}
//# sourceMappingURL=SearchViewModel.js.map