﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
var searchViewModel = new SearchViewModel("#eg", "#search-window");

class EmployeeListViewModel extends kendo.data.ObservableObject {
    nameOrEmail: string;
    searchByNameOrEmail: boolean;

    nameOrEmailSearchButtonClick() {
        this.set("searchByNameOrEmail", true);
        searchViewModel.search(); // auf diesem Weg triggern wir ein read der Grid-DataSource
    }
      
    fullSearchButtonClick() {
        this.set("nameOrEmail", "");
        this.set("searchByNameOrEmail", false);
        var window = $("#search-window").data("kendoWindow");
        window.center().open();
    }

    searchParams(): any {
        if (this.get("searchByNameOrEmail")) {
            return { nameOrMail: this.get("nameOrEmail") };
        } else {
            return searchViewModel.searchParams();
        }
    }

    reset() {
        gridStateSaver.clearState();
        window.location.reload();
    }

    constructor() {
        super();
        super.init(this);
    }

    onEdit(e: kendo.ui.GridEditEvent) {
        if ([0, 100, 200, 300].indexOf(e.model["State"]) < 0) {
            e.sender.closeCell();
        }
    }

    onError() {
        $("#serverErrorWindow").data("kendoWindow").center().open();
    }
}

var userKey;
var campaignId = 0;

var employeeListViewModel = new EmployeeListViewModel();

declare var gridStateSaver: GridStateSaver;

$(document).ready(() => {
    $("#eg .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
    $("#additional-toolbar-items-template").remove();

    $("#eg").on("click", ".change-password", (e: JQueryEventObject) => {
        var grid = $("#eg").data("kendoGrid"),
            data = grid.dataItem($(e.target).closest("tr"));
        e.preventDefault();
        userKey = data["ProviderUserKey"];
        $("#user").html(data["Firstname"] + " " + data["Lastname"]);
        $("#errorMsg").hide();
        $("#pwWnd").data("kendoWindow").center().open();
    });

    $("#saveBtn").click(() => {
        var window = $("#pwWnd").data("kendoWindow");
        var pw = (<HTMLInputElement>document.getElementById("pw")).value;
        $.ajax({
            url: appPath + "MD/Employee/_ChangePassword",
                type: "POST",
                data: { password: pw, userKey: userKey },
                global: false, // daduch wird der globale ajaxError-Handler nicht getriggert, wenn was schief geht
            })
            .done(() => {
                alert("Passwort geändert!");
                window.close();
            })
            .fail(() => {
                $("#errorMsg").show();
            });
    });

    gridStateSaver = new GridStateSaver($("#eg").data("kendoGrid"), true,
        // wird ausgeführt nachdem der GridState wiederhergestellt wurde
        // wir benutzes es um die Toolbar zu fixen, da Kendo die nicht speichern kann
        () => { kendo.bind("#search-actions", employeeListViewModel); },
        // wird sofort ausgeführt, wenn es zusätzlichen state gibt.
        (additionalState: any) => {
            employeeListViewModel.set("nameOrEmail", additionalState.nameOrEmail);
            employeeListViewModel.set("searchByNameOrEmail", additionalState.searchByNameOrEmail);
            for (var property in additionalState.searchViewModel) {
                if (additionalState.searchViewModel.hasOwnProperty(property)) {
                    searchViewModel.set(property, additionalState.searchViewModel[property]);
                }
            }
        });

    window.setTimeout(() => {
        var gridResizer = new GridResizer($("#hSplitter"), $("#eg"), height => {
            var grid = $("#eg").data("kendoGrid"),
                total: number,
                rows: number;

            if (!gridStateSaver.restoredData) {
                total = $("#eg .k-grid-content").height();
                rows = Math.floor(total / 32) - 3;
                grid.dataSource.pageSize(rows);
            }
            grid.refresh();
        });
        gridResizer.resize();
        kendo.bind("#search-window", searchViewModel);
        $("#infoIcon").click(() => {
            var window = <kendo.ui.Window>$("#infoWnd").data("kendoWindow");
            window.center().open();
        });
    }, 0);

    function saveState() {
        gridStateSaver.saveAdditionalState({
            nameOrEmail: employeeListViewModel.get("nameOrEmail"),
            searchByNameOrEmail: employeeListViewModel.get("searchByNameOrEmail"),
            searchViewModel: searchViewModel.searchParams()
        });
    }
    
    employeeListViewModel.bind("change", saveState);
    searchViewModel.bind("change", saveState);

    $("#quicksearch").keyup(e => {
        if (e.keyCode === 13) {
            employeeListViewModel.nameOrEmailSearchButtonClick();
        }
    });
});

function keyDownEmployee(e: KeyboardEvent) {
    if (e.keyCode === 13) {
        e.preventDefault();
        return false;
    }
    return true;
}

function keyupEmployee(e: KeyboardEvent) {
    if (e.keyCode === 13) {
        e.preventDefault();
        employeeListViewModel.nameOrEmailSearchButtonClick();
    }
    return false;
}

var importedUserDate = new Date(1900, 1, 1);

function formatLastLogin(lastLogin) {
    if (lastLogin === null) {
        return "";
    }
    return importedUserDate >= lastLogin
        ? "Datenimport"
        : kendo.format("{0:dd.MM.yyyy HH:mm}", lastLogin);
}