var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var locationSearchViewModel = new LocationSearchViewModel("#locations", "#search-window");
var LocationListViewModel = (function (_super) {
    __extends(LocationListViewModel, _super);
    function LocationListViewModel() {
        var _this = _super.call(this) || this;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    LocationListViewModel.prototype.nameStreetCitySearchButtonClick = function () {
        this.set("searchByNameStreetCity", true);
        locationSearchViewModel.search();
    };
    LocationListViewModel.prototype.fullSearchButtonClick = function () {
        this.set("nameStreetCity", "");
        this.set("searchByNameStreetCity", false);
        var window = $("#search-window").data("kendoWindow");
        window.center().open();
    };
    LocationListViewModel.prototype.searchParams = function () {
        if (this.get("searchByNameStreetCity")) {
            return { nameStreetCity: this.get("nameStreetCity") };
        }
        else {
            return locationSearchViewModel.searchParams();
        }
    };
    LocationListViewModel.prototype.reset = function () {
        gridStateSaver.clearState();
        window.location.reload();
    };
    return LocationListViewModel;
}(kendo.data.ObservableObject));
var locationListViewModel = new LocationListViewModel();
$(document).ready(function () {
    $("#locations .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
    $("#additional-toolbar-items-template").remove();
    gridStateSaver = new GridStateSaver($("#locations").data("kendoGrid"), true, function () { kendo.bind("#search-actions", locationListViewModel); }, function (additionalState) {
        locationListViewModel.set("nameStreetCity", additionalState.nameStreetCity);
        locationListViewModel.set("searchByNameStreetCity", additionalState.searchByNameStreetCity);
        for (var property in additionalState.locationSearchViewModel) {
            if (additionalState.locationSearchViewModel.hasOwnProperty(property)) {
                locationSearchViewModel.set(property, additionalState.locationSearchViewModel[property]);
            }
        }
    });
    window.setTimeout(function () {
        var gridResizer = new GridResizer($("#hSplitter"), $("#locations"), function (height) {
            var grid = $("#locations").data("kendoGrid"), total, rows;
            if (!gridStateSaver.restoredData) {
                total = $("#locations .k-grid-content").height();
                rows = Math.floor(total / 32) - 3;
                grid.dataSource.pageSize(rows);
            }
            grid.refresh();
        });
        gridResizer.resize();
        kendo.bind("#search-window", locationSearchViewModel);
    }, 0);
    function saveState() {
        gridStateSaver.saveAdditionalState({
            nameStreetCity: locationListViewModel.get("nameStreetCity"),
            searchByNameStreetCity: locationListViewModel.get("searchByNameStreetCity"),
            locationSearchViewModel: locationSearchViewModel.searchParams()
        });
    }
    locationListViewModel.bind("change", saveState);
    locationSearchViewModel.bind("change", saveState);
});
function keyupLocation(e) {
    if (e.keyCode === 13) {
        locationListViewModel.nameStreetCitySearchButtonClick();
    }
    return false;
}
//# sourceMappingURL=SearchViewModel.js.map