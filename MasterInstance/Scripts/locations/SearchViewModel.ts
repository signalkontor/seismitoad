﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
var locationSearchViewModel = new LocationSearchViewModel("#locations", "#search-window");

class LocationListViewModel extends kendo.data.ObservableObject {
    nameStreetCity: string;
    searchByNameStreetCity: boolean;

    nameStreetCitySearchButtonClick() {
        this.set("searchByNameStreetCity", true);
        locationSearchViewModel.search(); // auf diesem Weg triggern wir ein read der Grid-DataSource
    }
      
    fullSearchButtonClick() {
        this.set("nameStreetCity", "");
        this.set("searchByNameStreetCity", false);
        var window = $("#search-window").data("kendoWindow");
        window.center().open();
    }

    searchParams(): any {
        if (this.get("searchByNameStreetCity")) {
            return { nameStreetCity: this.get("nameStreetCity") };
        } else {
            return locationSearchViewModel.searchParams();
        }
    }

    reset() {
        gridStateSaver.clearState();
        window.location.reload();
    }

    constructor() {
        super();
        super.init(this);
    }
}


var locationListViewModel = new LocationListViewModel();

declare var gridStateSaver: GridStateSaver;

$(document).ready(() => {
    $("#locations .k-grid-toolbar").append($("#additional-toolbar-items-template").children());
    $("#additional-toolbar-items-template").remove();

    gridStateSaver = new GridStateSaver($("#locations").data("kendoGrid"), true,
        // wird ausgeführt nachdem der GridState wiederhergestellt wurde
        // wir benutzes es um die Toolbar zu fixen, da Kendo die nicht speichern kann
        () => { kendo.bind("#search-actions", locationListViewModel); },
        // wird sofort ausgeführt, wenn es zusätzlichen state gibt.
        (additionalState: any) => {
            locationListViewModel.set("nameStreetCity", additionalState.nameStreetCity);
            locationListViewModel.set("searchByNameStreetCity", additionalState.searchByNameStreetCity);
            for (var property in additionalState.locationSearchViewModel) {
                if (additionalState.locationSearchViewModel.hasOwnProperty(property)) {
                    locationSearchViewModel.set(property, additionalState.locationSearchViewModel[property]);
                }
            }
        });

    window.setTimeout(() => {
        var gridResizer = new GridResizer($("#hSplitter"), $("#locations"), height => {
            var grid = $("#locations").data("kendoGrid"),
                total: number,
                rows: number;

            if (!gridStateSaver.restoredData) {
                total = $("#locations .k-grid-content").height();
                rows = Math.floor(total / 32) - 3;
                grid.dataSource.pageSize(rows);
            }
            grid.refresh();
        });
        gridResizer.resize();
        kendo.bind("#search-window", locationSearchViewModel);
    }, 0);

    function saveState() {
        gridStateSaver.saveAdditionalState({
            nameStreetCity: locationListViewModel.get("nameStreetCity"),
            searchByNameStreetCity: locationListViewModel.get("searchByNameStreetCity"),
            locationSearchViewModel: locationSearchViewModel.searchParams()
        });
    }
    
    locationListViewModel.bind("change", saveState);
    locationSearchViewModel.bind("change", saveState);
});

function keyupLocation(e: KeyboardEvent) {
    if (e.keyCode === 13) {
        locationListViewModel.nameStreetCitySearchButtonClick();
    }
    return false;
}