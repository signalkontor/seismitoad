﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="kendo.all.d.ts" />

class GridStateSaver {
    localStorageKey = document.location.pathname;
    restoredData = false;
    private toolbar: string;
    private currentVersion = "3";

    constructor(
        private grid: kendo.ui.Grid,
        private restoreGridState: boolean,
        private afterOptionsSet = () => {},
        private applyAdditionalState = (additionalState: any) => { },
        private ignoreToolbar = false) {
        var additionalState = localStorage.getItem(this.localStorageKey + "_extra");
        grid.one("dataBinding", $.proxy(this.dataBinding, this));
        grid.bind("dataBound", $.proxy(this.saveState, this));
        grid.bind("columnShow", $.proxy(this.saveState, this));
        grid.bind("columnHide", $.proxy(this.saveState, this));
        grid.bind("columnResize", $.proxy(this.saveState, this));
        grid.bind("columnReorder", $.proxy(this.saveStateAfterReoderEvent, this));
        if (additionalState) {
            applyAdditionalState(JSON.parse(additionalState));
        }
        this.restoredData = restoreGridState && localStorage.getItem(this.localStorageKey) !== null;
        this.toolbar = grid.element.find(".k-grid-toolbar").html();
    }

    dataBinding(e: kendo.ui.GridDataBindingEvent) {
        var optionsJson: string,
            version: string;
        if (this.restoreGridState) {
            version = localStorage.getItem(this.localStorageKey + "_version");
            if (version != this.currentVersion) {
                localStorage.removeItem(this.localStorageKey);
            }
            optionsJson = localStorage.getItem(this.localStorageKey);
            if (optionsJson) {
                var options = JSON.parse(optionsJson);
                if (!this.ignoreToolbar) {
                    options.toolbar = [
                        { template: this.toolbar }
                    ];
                }
                e.sender.setOptions(options);
            }
            this.afterOptionsSet();
        }
        e.sender.dataSource.read();
    }

    // Das kendo.ui.GridDataBoundEvent steht hier stellvertretend für alle anderen GridEvents. Daher dürfen wir nur e.sender verwenden!
    saveState(e: kendo.ui.GridDataBoundEvent) {
        var options = e.sender.getOptions();
        localStorage.setItem(this.localStorageKey, JSON.stringify(options));
        localStorage.setItem(this.localStorageKey + "_version", this.currentVersion);
    }

    saveStateAfterReoderEvent(e: kendo.ui.GridColumnReorderEvent) {
        // Da das Reoder-Event schon feuert, bevor die Grid-Options geändert sind
        // müssen wir den Abruf der Options verzögern.
        window.setTimeout(() => {
            var options = e.sender.getOptions();
            localStorage.setItem(this.localStorageKey, JSON.stringify(options));
            localStorage.setItem(this.localStorageKey + "_version", this.currentVersion);
        }, 0);
    }

    saveAdditionalState(additionalState: any) {
        localStorage.setItem(this.localStorageKey + "_extra", JSON.stringify(additionalState));
    }

    clearState() {
        localStorage.removeItem(this.localStorageKey);
        localStorage.removeItem(this.localStorageKey + "_extra");
    }
}