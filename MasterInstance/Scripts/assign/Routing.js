var router = new kendo.Router();
router.route("/:mode/:date/:filters/:locations", function (mode, date, filters, locations) {
    actionsViewModel.set("mode", mode);
    filterViewModel.set("date", new Date(parseInt(date)));
    var f = $.parseJSON(filters);
    if ($.isArray(f) && f.length > 0 && typeof (f[0]) == "number") {
        filterViewModel.set('filters', f.map(function (item) { return { Value: item, Text: "" }; }));
    }
    var l = $.parseJSON(locations);
    if ($.isArray(l) && l.length > 0 && typeof (l[0]) == "number") {
        filterViewModel.set('locations', l.map(function (item) { return { Value: item, Text: "" }; }));
    }
    filterViewModel.applyFilter();
});
//# sourceMappingURL=Routing.js.map