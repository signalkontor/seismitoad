var employeeEditorWindowElement;
var employeeSelectWindowElement;
var assignmentsGridElement;
var assignmentsGrid;
var gridResizer;
$(document).ready(function () {
    assignmentsGridElement = $("#a");
    employeeEditorWindowElement = $("#employee-editor-window");
    employeeSelectWindowElement = $("#employee-select-window");
    window.setTimeout(function () {
        assignmentsGrid = assignmentsGridElement.data("kendoGrid");
        gridResizer = new GridResizer($("#hSplitter"), assignmentsGridElement, function () { }, 90);
        gridResizer.resize();
    }, 0);
});
//# sourceMappingURL=Initialization.js.map