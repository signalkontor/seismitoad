var AssignAjaxHelper = (function () {
    function AssignAjaxHelper(campaignId) {
        this.campaignId = campaignId;
        this.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.0000000'";
        this.createAssignmentUrl = appPath + "CM/Assignment/_Create";
        this.deleteAssignmentUrl = appPath + "CM/Assignment/_Delete";
        this.updateDateUrl = appPath + "CM/Assignment/_UpdateDate";
        this.removeEmployeeUrl = appPath + "CM/Assign/_RemoveAssignedEmployee";
        this.updateEmployeeUrl = appPath + "CM/Assign/_UpdateAssignedEmployee";
        this.needsConfirmation = "needs confirmation";
    }
    AssignAjaxHelper.prototype.deleteAssignment = function (assignmentId, notify, success) {
        this.deleteAssignmentInternal(assignmentId, notify, success);
    };
    AssignAjaxHelper.prototype.updateDate = function (model) {
        this.updateDateInternal(model);
    };
    AssignAjaxHelper.prototype.removeEmployee = function (model) {
        this.removeEmployeeInternal(model);
    };
    AssignAjaxHelper.prototype.updateEmployee = function (model) {
        this.updateEmployeeInternal(model);
    };
    AssignAjaxHelper.prototype.createAssignment = function (locationId, start, end, employeeCount, success) {
        skBlockUI();
        $.post(this.createAssignmentUrl, {
            campaignId: this.campaignId,
            locationId: locationId,
            start: kendo.toString(start, this.dateFormat),
            end: kendo.toString(end, this.dateFormat),
            employeeCount: employeeCount
        }).done(success).always($.unblockUI);
    };
    AssignAjaxHelper.prototype.deleteAssignmentInternal = function (assignmentId, notify, success, confirmed) {
        var _this = this;
        if (confirmed === void 0) { confirmed = false; }
        skBlockUI();
        $.post(this.deleteAssignmentUrl, {
            assignmentId: assignmentId,
            confirmed: confirmed,
            notify: notify
        })
            .done(function (result) {
            if (result.status == _this.needsConfirmation) {
                confirmViewModel
                    .ask(result.message)
                    .onYes(function () { _this.deleteAssignmentInternal(assignmentId, notify, success, true); });
                return;
            }
            success();
        }).always($.unblockUI);
    };
    AssignAjaxHelper.prototype.updateDateInternal = function (model) {
        var _this = this;
        skBlockUI();
        $.post(this.updateDateUrl, {
            assignmentId: model.assignmentId,
            start: kendo.toString(model.start, this.dateFormat),
            end: kendo.toString(model.end, this.dateFormat),
            reason: model.reason,
            repeat: model.repeat,
            confirmed: model.confirmed,
            notify: model.notify
        }).done(function (result) {
            switch (result.status) {
                case _this.needsConfirmation:
                    confirmViewModel
                        .ask(result.message)
                        .onYes(function () {
                        model.confirmed = true;
                        _this.updateDateInternal(model);
                    });
                    return;
                case "error":
                    alert(result.message);
                    return;
                case "warning":
                    alert(result.message);
                    break;
            }
            model.success(result.assignmentId);
        })
            .always($.unblockUI);
    };
    AssignAjaxHelper.prototype.removeEmployeeInternal = function (model) {
        var _this = this;
        var params = {
            dataId: model.dataId,
            assignmentId: model.assignmentId,
            employeeId: model.employeeId,
            repeat: model.repeat,
            additionalAssignmentIds: model.additionalAssignmentIds,
            confirmed: model.confirmed,
            notify: model.notify
        };
        skBlockUI();
        $.post(this.removeEmployeeUrl, params).done(function (result) {
            if (result.status === _this.needsConfirmation) {
                confirmViewModel
                    .ask(result.message)
                    .onYes(function () {
                    model.confirmed = true;
                    _this.removeEmployeeInternal(model);
                });
                return;
            }
            model.success();
        })
            .always($.unblockUI);
    };
    AssignAjaxHelper.prototype.updateEmployeeInternal = function (model) {
        var _this = this;
        var params = {
            assignmentId: model.assignmentId,
            previousEmployeeId: model.previousEmployeeId,
            newEmployeeId: model.newEmployeeId,
            newRoleIds: model.newRoleIds,
            repeat: model.repeat,
            additionalAssignmentIds: model.additionalAssignmentIds,
            confirmed: model.confirmed,
            notify: model.notify
        };
        skBlockUI();
        $.post(this.updateEmployeeUrl, params)
            .done(function (result) {
            if (result.status === _this.needsConfirmation) {
                confirmViewModel
                    .ask(result.message)
                    .onYes(function () {
                    model.confirmed = true;
                    _this.updateEmployeeInternal(model);
                });
                return;
            }
            _this.updateResultHandler(result, model.notify);
            model.success();
        })
            .always($.unblockUI);
    };
    return AssignAjaxHelper;
}());
//# sourceMappingURL=AssignAjaxHelper.js.map