﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
/// <reference path="Initialization.ts" />

interface ISelectListItem {
    Value: string;
    Text: string;
}

class FilterViewModel extends kendo.data.ObservableObject {
    timeoutId: number;
    date = new Date();
    filters: ISelectListItem[];
    locations: ISelectListItem[];
    filterItems: ISelectListItem[];
    locationItems: kendo.data.DataSource;
    
    constructor(filterItems: ISelectListItem[], locationItemsUrl: string) {
        super();
        this.filterItems = filterItems;
        this.locationItems = new kendo.data.DataSource({
            transport: {
                read: { url: locationItemsUrl }
            }
        });
        super.init(this);
    }

    private _fixDate() {
        var date = this.get("date");
        if (date === null) {
            this.set("date", new Date());
        }        
    }

    monday() {
        this._fixDate();
        var date = this.get("date");
        var dayOfWeek = date.getDay() - 1;
        if (dayOfWeek < 0) {
            dayOfWeek = 6;
        }
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() - dayOfWeek);
    }

    applyFilter() {
        this._fixDate();
        this.timeoutId = window.setTimeout(() => {
            assignmentsGrid.dataSource.read();
        }, 1000);
    }

    refresh() {
        assignmentsGrid.dataSource.read();
    }

    nextWeek() {
        this._fixDate();
        var date = this.get("date");
        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7);
        this.set("date", newDate);
        this.applyFilter();
    }

    previousWeek() {
        this._fixDate();
        var date = this.get("date");
        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
        this.set("date", newDate);
        this.applyFilter();
    }

    weekOfYear() {
        this._fixDate();
        return this.get("date").getWeek();
    }

    private _toValueArray(array: ISelectListItem[]): any[] {
        return array && ($.isArray(array) || array instanceof kendo.data.ObservableArray)
            ? array.map(element => element.Value)
            : [];
    }

    //setFilters(filterIds: number[]) {
    //    var filters = this.filterItems.filter((item: ISelectListItem) => {
    //        return $.inArray(parseInt(item.Value), filterIds, 0) != -1;
    //    });
    //    this.set("filters", filters);
    //}

    //setLocations(locationIds: number[]) {
    //    var locations = this.locationItems.data().filter((item: ISelectListItem) => {
    //        return $.inArray(parseInt(item.Value), locationIds, 0) != -1;
    //    });
    //    this.set("locations", locations);
    //}

    gridParams() {
        return {
            start: kendo.toString(this.monday(), "yyyy-MM-dd"),
            f: this._toValueArray(this.filters),
            l: this._toValueArray(this.locations)
        };
    }

    updateHeader(e: any) {
        var monday = this.monday(),
            grid = <kendo.ui.Grid>e.sender,
            aggregates = grid.dataSource.aggregates(),
            tmpDate = new Date(monday.getTime());

        $(".k-grid-header th", assignmentsGridElement)
            .filter(index => index > 3)
            .each((index, element) => {
                var aggregate,
                    total = 0,
                    assigned = 0;
                if (typeof (aggregates) !== "undefined" && aggregates !== null) {
                    aggregate = aggregates[element.attributes["data-field"].value];
                    total = aggregate.sum;
                    assigned = aggregate.count;
                }
            $(element)
                .data("date", new Date(tmpDate.getTime()))
                .contents().last().replaceWith(kendo.format("<span>{0:ddd dd.MM.yy} <span style='float: right; padding-right: 8px'>{1}/{2}</span></span>", tmpDate, assigned, total));
                tmpDate.setDate(tmpDate.getDate() + 1);
            });
    }
}