﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
/// <reference path="Initialization.ts" />
/// <reference path="AssignAjaxHelper.ts" />

declare var readWriteUser: boolean;

class ActionsViewModel extends kendo.data.ObservableObject {
    mode: any = { value: "default", text: "Standardmodus" };
    employeeId: number = 0;
    newRoles: number[] = [];
    employeeName: string;
    rolesText: string;
    notify: string;

    constructor(
        private ajaxHelper: AssignAjaxHelper,
        private employeeEditorViewModel: EmployeeViewModel,
        private employeeSelectViewModel: EmployeeViewModel) {
        super();
        super.init(this);
    }

    isAssignMode(): boolean {
        var mode = this.get("mode");
        return mode != null && mode.value == "assign";
    }

    selectedEmployeeDisplay(): string {
        return this.employeeAndRolesSelected()
            ? this.get("employeeName") + " (" + this.get("rolesText") + ")"
            : "Kein Promoter gewählt";
    }

    selectEmployee() {
        this.employeeSelectViewModel.set("notify", "False");
        employeeSelectWindowElement.data("kendoWindow").open().center();
    }

    popupClosed() {
        this.set("newRoles", this.employeeSelectViewModel.newRoles);
        this.set("employeeId", this.employeeSelectViewModel.newId);
        var name = this.employeeSelectViewModel.newFirstname + " " + this.employeeSelectViewModel.newLastname;
        this.set("employeeName", name);
        this.set("rolesText", this.employeeSelectViewModel.newRolesText());
        this.set("notify", this.employeeSelectViewModel.notify);
    }

    employeeClick(source: HTMLElement, assignmentId: number, employeeId: number) {
        var notify = "False",
            mode = this.get("mode");

        if (!readWriteUser) {
            return;
        }

        if ($(source.parentElement).hasClass("purple")) {
            return;
        }

        if (mode == null || mode.value == "default") {
            this.showEditEmployeeWindow(assignmentId, employeeId);
            return;
        }
        switch(mode.value)
        {
            case "assign":
                if (!this.employeeAndRolesSelected()) {
                    $("#info-window").data("kendoWindow").open().center();
                    return;
                }
                this.ajaxHelper.updateEmployee({
                    assignmentId: assignmentId,
                    previousEmployeeId: employeeId,
                    newEmployeeId: this.get("employeeId"),
                    newRoleIds: (<IRole[]>this.get("newRoles")).map(role => role.id),
                    repeat: 0,
                    additionalAssignmentIds: [],
                    confirmed: false,
                    notify: this.get("notify"),
                    success: () => { assignmentsGrid.dataSource.read(); }
                });
                return;
            case "unassign":
                notify = "True";
            case "unassign2":
                if (employeeId > 0) {
                    this.ajaxHelper.removeEmployee({
                        dataId: null,
                        assignmentId: assignmentId,
                        employeeId: employeeId,
                        repeat: 0,
                        additionalAssignmentIds: [],
                        confirmed: false,
                        notify: notify,
                        success: () => assignmentsGrid.dataSource.read()
                    });
                } else {
                    // Vielleicht irgendwas machen? Zum Beispiel: this.showEditEmployeeWindow(assignmentId, employeeId);
                }
                return;
        }
    }

    showNotifications() {
        $("#notifications-window").data("kendoWindow").open().center();
        $("#notifications").data("kendoGrid").dataSource.read();
    }

    showLegend() {
        $("#legendWindow").data("kendoWindow").center().open();
    }

    deleteClick(assignmentId: number, notifiedCount: number) {
        if (notifiedCount === 0) {
            // Keine Promoter, dann ohne weitere Nachfragen löschen
            this.ajaxHelper.deleteAssignment(assignmentId, true, () => assignmentsGrid.dataSource.read());
            return;
        }
        confirmViewModel
            .ask((notifiedCount == 1 ? "Soll der Promoter" : "Sollen die Promoter") + " über die Löschung des Einsatzes sofort benachrichtigt werden?")
            .onYes(() => {
                this.ajaxHelper.deleteAssignment(assignmentId, true, () => assignmentsGrid.dataSource.read());
            }).onNo(() => {
                this.ajaxHelper.deleteAssignment(assignmentId, false, () => assignmentsGrid.dataSource.read());
            });
    }

    private showEditEmployeeWindow(assignmentId: number, employeeId: number) {
        this.employeeEditorViewModel.assignmentId = assignmentId;
        this.employeeEditorViewModel.set("currentId", employeeId);
        this.employeeEditorViewModel.set("newId", employeeId);
        this.employeeEditorViewModel.refresh();
    }

    private employeeAndRolesSelected() {
        var hasEmployee = this.get("employeeId") > 0;
        var hasRoles = this.get("newRoles").length > 0;
        return hasEmployee && hasRoles;
    }
}