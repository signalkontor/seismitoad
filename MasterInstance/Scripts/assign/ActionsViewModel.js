var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ActionsViewModel = (function (_super) {
    __extends(ActionsViewModel, _super);
    function ActionsViewModel(ajaxHelper, employeeEditorViewModel, employeeSelectViewModel) {
        var _this = _super.call(this) || this;
        _this.ajaxHelper = ajaxHelper;
        _this.employeeEditorViewModel = employeeEditorViewModel;
        _this.employeeSelectViewModel = employeeSelectViewModel;
        _this.mode = { value: "default", text: "Standardmodus" };
        _this.employeeId = 0;
        _this.newRoles = [];
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    ActionsViewModel.prototype.isAssignMode = function () {
        var mode = this.get("mode");
        return mode != null && mode.value == "assign";
    };
    ActionsViewModel.prototype.selectedEmployeeDisplay = function () {
        return this.employeeAndRolesSelected()
            ? this.get("employeeName") + " (" + this.get("rolesText") + ")"
            : "Kein Promoter gewählt";
    };
    ActionsViewModel.prototype.selectEmployee = function () {
        this.employeeSelectViewModel.set("notify", "False");
        employeeSelectWindowElement.data("kendoWindow").open().center();
    };
    ActionsViewModel.prototype.popupClosed = function () {
        this.set("newRoles", this.employeeSelectViewModel.newRoles);
        this.set("employeeId", this.employeeSelectViewModel.newId);
        var name = this.employeeSelectViewModel.newFirstname + " " + this.employeeSelectViewModel.newLastname;
        this.set("employeeName", name);
        this.set("rolesText", this.employeeSelectViewModel.newRolesText());
        this.set("notify", this.employeeSelectViewModel.notify);
    };
    ActionsViewModel.prototype.employeeClick = function (source, assignmentId, employeeId) {
        var notify = "False", mode = this.get("mode");
        if (!readWriteUser) {
            return;
        }
        if ($(source.parentElement).hasClass("purple")) {
            return;
        }
        if (mode == null || mode.value == "default") {
            this.showEditEmployeeWindow(assignmentId, employeeId);
            return;
        }
        switch (mode.value) {
            case "assign":
                if (!this.employeeAndRolesSelected()) {
                    $("#info-window").data("kendoWindow").open().center();
                    return;
                }
                this.ajaxHelper.updateEmployee({
                    assignmentId: assignmentId,
                    previousEmployeeId: employeeId,
                    newEmployeeId: this.get("employeeId"),
                    newRoleIds: this.get("newRoles").map(function (role) { return role.id; }),
                    repeat: 0,
                    additionalAssignmentIds: [],
                    confirmed: false,
                    notify: this.get("notify"),
                    success: function () { assignmentsGrid.dataSource.read(); }
                });
                return;
            case "unassign":
                notify = "True";
            case "unassign2":
                if (employeeId > 0) {
                    this.ajaxHelper.removeEmployee({
                        dataId: null,
                        assignmentId: assignmentId,
                        employeeId: employeeId,
                        repeat: 0,
                        additionalAssignmentIds: [],
                        confirmed: false,
                        notify: notify,
                        success: function () { return assignmentsGrid.dataSource.read(); }
                    });
                }
                else {
                }
                return;
        }
    };
    ActionsViewModel.prototype.showNotifications = function () {
        $("#notifications-window").data("kendoWindow").open().center();
        $("#notifications").data("kendoGrid").dataSource.read();
    };
    ActionsViewModel.prototype.showLegend = function () {
        $("#legendWindow").data("kendoWindow").center().open();
    };
    ActionsViewModel.prototype.deleteClick = function (assignmentId, notifiedCount) {
        var _this = this;
        if (notifiedCount === 0) {
            this.ajaxHelper.deleteAssignment(assignmentId, true, function () { return assignmentsGrid.dataSource.read(); });
            return;
        }
        confirmViewModel
            .ask((notifiedCount == 1 ? "Soll der Promoter" : "Sollen die Promoter") + " über die Löschung des Einsatzes sofort benachrichtigt werden?")
            .onYes(function () {
            _this.ajaxHelper.deleteAssignment(assignmentId, true, function () { return assignmentsGrid.dataSource.read(); });
        }).onNo(function () {
            _this.ajaxHelper.deleteAssignment(assignmentId, false, function () { return assignmentsGrid.dataSource.read(); });
        });
    };
    ActionsViewModel.prototype.showEditEmployeeWindow = function (assignmentId, employeeId) {
        this.employeeEditorViewModel.assignmentId = assignmentId;
        this.employeeEditorViewModel.set("currentId", employeeId);
        this.employeeEditorViewModel.set("newId", employeeId);
        this.employeeEditorViewModel.refresh();
    };
    ActionsViewModel.prototype.employeeAndRolesSelected = function () {
        var hasEmployee = this.get("employeeId") > 0;
        var hasRoles = this.get("newRoles").length > 0;
        return hasEmployee && hasRoles;
    };
    return ActionsViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=ActionsViewModel.js.map