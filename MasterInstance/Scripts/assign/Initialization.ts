﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

declare var ajaxHelper: AssignAjaxHelper;
declare var confirmViewModel: Confirm;

var employeeEditorWindowElement: JQuery;
var employeeSelectWindowElement: JQuery;
var assignmentsGridElement: JQuery;
var assignmentsGrid: kendo.ui.Grid;
var gridResizer: GridResizer;

$(document).ready(() => {
    assignmentsGridElement = $("#a");
    employeeEditorWindowElement = $("#employee-editor-window");
    employeeSelectWindowElement = $("#employee-select-window");

    //$("input[data-role=datepicker]").bind("focus click", function () {
    //    $(this).data("kendoDatePicker").open();
    //});

    // Dieser Code soll erst nach allen $(document).ready()'s laufen, damit z.B. das Kendo-Grid schon initialisiert ist.
    window.setTimeout(() => {
        assignmentsGrid = <kendo.ui.Grid>assignmentsGridElement.data("kendoGrid");
        gridResizer = new GridResizer($("#hSplitter"), assignmentsGridElement, () => { }, 90);
        gridResizer.resize();
    }, 0);
});
