var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var NotificationsViewModel = (function (_super) {
    __extends(NotificationsViewModel, _super);
    function NotificationsViewModel() {
        var _this = _super.call(this) || this;
        _this.buttonsEnabled = false;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    NotificationsViewModel.prototype.sendNotifications = function () {
        var ids = new Array();
        $("#notifications .k-grid-content :checked").each(function (index, element) {
            ids.push($(element).attr("name"));
        });
        this.postNotifications(appPath + "CM/Assign/_SendNotifications", ids);
    };
    NotificationsViewModel.prototype.deleteNotifications = function () {
        var ids = new Array();
        $("#notifications .k-grid-content :checked").each(function (index, element) {
            ids.push(parseInt($(element).attr("value")));
        });
        this.postNotifications(appPath + "CM/Assign/_DeleteNotifications", ids);
    };
    NotificationsViewModel.prototype.postNotifications = function (url, ids) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(ids),
            success: function () {
                $("#checkAllNotifications").prop("checked", false);
                $("#notifications").data("kendoGrid").dataSource.read();
                filterViewModel.refresh();
            }
        });
    };
    return NotificationsViewModel;
}(kendo.data.ObservableObject));
$(document).ready(function () {
    var viewModel = new NotificationsViewModel();
    kendo.bind("#notifications-window", viewModel);
    $("#checkAllNotifications").on("change", function (event) {
        var checkbox = event.target;
        $("#notifications .k-grid-content :checkbox").prop("checked", checkbox.checked).change();
    });
    $("#notifications .k-grid-content")
        .on("click", "tr", function (event) {
        if (event.target.tagName !== "INPUT" &&
            event.target.type !== 'checkbox') {
            $(":checkbox", this).click();
        }
    })
        .on("change", "input[type=checkbox]", function () {
        viewModel.set("buttonsEnabled", $("#notifications .k-grid-content :checked").length > 0);
    });
});
//# sourceMappingURL=NotificationsViewModel.js.map