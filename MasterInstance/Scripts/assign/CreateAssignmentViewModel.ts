﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

class CreateAssignmentViewModel extends kendo.data.ObservableObject {
    locationId: number;
    location: string;
    date: Date;
    start: Date;
    end: Date;
    employeeCount: number;

    constructor() {
        super();
        if (readWriteUser) {
            $(document).click($.proxy(this.click, this));
        }
        super.init(this);
    }

    formattedDate() {
        return kendo.toString(this.get("date"), "ddd dd.MM.yyyy");
    }

    submitEnabled() {
        var start = <Date>this.get("start"),
            end = <Date>this.get("end");

        if (!start) {
            return false;
        }
        if (!end) {
            return false;
        }

        // Copy the date objects
        start = new Date(start.valueOf());
        end = new Date(end.valueOf());

        if (start.getHours() < 5) {
            start.setDate(start.getDate() + 1);
        }
        if (end.getHours() < 5) {
            end.setDate(end.getDate() + 1);
        }
        if (end <= start) {
            return false;
        }
        if (!this.get("employeeCount")) {
            return false;
        }
        return true;
    }

    submit() {
        var date = <Date>this.get("date");
        var start = <Date>this.get("start");
        var end = <Date>this.get("end");
        start.setFullYear(date.getFullYear());
        start.setMonth(date.getMonth());
        start.setDate(date.getDate());
        end.setFullYear(date.getFullYear());
        end.setMonth(date.getMonth());
        end.setDate(date.getDate());
        ajaxHelper.createAssignment(this.get("locationId"), start, end, this.get("employeeCount"), () => {
            assignmentsGrid.dataSource.read();
            $("#create-assignment-window").data("kendoWindow").close();
        });
    }

    click(e: JQueryMouseEventObject) {
        var element = document.elementFromPoint(e.clientX, e.clientY);
        if (element != null && element.tagName == "TD") {
            // Uns interessieren nur die Zellen im Asssignment-Grid
            if ($(element).has("div.assignments").length == 0) {
                return;
            }
            e.preventDefault();

            var row = $(element.parentNode);
            var cells = row.children();

            var location = "";
            cells
                .filter(index => index < 2 || index == 3) // Auf Spalten 1, 2 und 4 reduzieren
                .each((index, c) => {
                    var cell = $(c);
                    if (index == 0) {
                        this.set("locationId", cell.find("input[name=locationId]").val());
                    }
                    location += " " + cell.text();
                });
            this.set("location", location);

            var clickedIndex = cells.index($(element));
            this.set("date", $(assignmentsGrid.thead.find("th")[clickedIndex]).data("date"));

            $("#create-assignment-window").data("kendoWindow").center().open();
        }
    }
} 