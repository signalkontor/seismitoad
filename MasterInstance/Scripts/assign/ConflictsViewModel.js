var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ConflictsViewModel = (function (_super) {
    __extends(ConflictsViewModel, _super);
    function ConflictsViewModel(ajaxHelper) {
        var _this = _super.call(this) || this;
        _this.ajaxHelper = ajaxHelper;
        _this.dataSource = [];
        ajaxHelper.updateResultHandler = $.proxy(_this.handleEmployeeUpdateResult, _this);
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    ConflictsViewModel.prototype.visible = function () {
        var conflicts = this.get("dataSource");
        var window = $("#conflicts-window").data("kendoWindow");
        if (window == null) {
            return false;
        }
        var visible = conflicts.length > 0;
        if (visible) {
            window.center();
        }
        return visible;
    };
    ConflictsViewModel.prototype.clear = function (e) {
        if (e != null && e.userTriggered) {
            e.preventDefault();
        }
        this.sendNotification();
        this.set("dataSource", []);
    };
    ConflictsViewModel.prototype.sendNotification = function () {
        $.ajax({
            type: "POST",
            url: appPath + "CM/Assign/_SendNotifications",
            contentType: "application/json",
            data: JSON.stringify([this.get("dataId")])
        }).always(function () { assignmentsGrid.dataSource.read(); });
    };
    ConflictsViewModel.prototype.removeClick = function (sender, e) {
        var _this = this;
        e.preventDefault();
        var dataItem = sender.dataItem($(e.currentTarget).closest("tr")), assignmentId = dataItem.get("assignmentId"), dataId = dataItem.get("dataId");
        this.ajaxHelper.removeEmployee({
            dataId: dataId,
            assignmentId: assignmentId,
            employeeId: this.employeeId,
            repeat: 0,
            additionalAssignmentIds: [],
            confirmed: true,
            notify: this.notify,
            success: function () {
                for (var i = 0; i < _this.dataSource.length; i++) {
                    if (_this.dataSource[i].assignmentId == assignmentId) {
                        _this.dataSource.splice(i, 1);
                        if (_this.dataSource.length === 0) {
                            _this.sendNotification();
                        }
                        else {
                            assignmentsGrid.dataSource.read();
                        }
                        break;
                    }
                }
            }
        });
    };
    ConflictsViewModel.prototype.handleEmployeeUpdateResult = function (result, notify) {
        this.employeeId = result.employeeId;
        this.notify = notify;
        this.set("dataId", result.dataId);
        this.set("location", result.location);
        this.set("employee", result.employee);
        this.set("dataSource", result.conflicts || []);
        if (result.conflicts != null && result.conflicts.length > 0) {
            employeeEditorWindowElement.data("kendoWindow").close();
        }
    };
    return ConflictsViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=ConflictsViewModel.js.map