var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CreateAssignmentViewModel = (function (_super) {
    __extends(CreateAssignmentViewModel, _super);
    function CreateAssignmentViewModel() {
        var _this = _super.call(this) || this;
        if (readWriteUser) {
            $(document).click($.proxy(_this.click, _this));
        }
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    CreateAssignmentViewModel.prototype.formattedDate = function () {
        return kendo.toString(this.get("date"), "ddd dd.MM.yyyy");
    };
    CreateAssignmentViewModel.prototype.submitEnabled = function () {
        var start = this.get("start"), end = this.get("end");
        if (!start) {
            return false;
        }
        if (!end) {
            return false;
        }
        start = new Date(start.valueOf());
        end = new Date(end.valueOf());
        if (start.getHours() < 5) {
            start.setDate(start.getDate() + 1);
        }
        if (end.getHours() < 5) {
            end.setDate(end.getDate() + 1);
        }
        if (end <= start) {
            return false;
        }
        if (!this.get("employeeCount")) {
            return false;
        }
        return true;
    };
    CreateAssignmentViewModel.prototype.submit = function () {
        var date = this.get("date");
        var start = this.get("start");
        var end = this.get("end");
        start.setFullYear(date.getFullYear());
        start.setMonth(date.getMonth());
        start.setDate(date.getDate());
        end.setFullYear(date.getFullYear());
        end.setMonth(date.getMonth());
        end.setDate(date.getDate());
        ajaxHelper.createAssignment(this.get("locationId"), start, end, this.get("employeeCount"), function () {
            assignmentsGrid.dataSource.read();
            $("#create-assignment-window").data("kendoWindow").close();
        });
    };
    CreateAssignmentViewModel.prototype.click = function (e) {
        var _this = this;
        var element = document.elementFromPoint(e.clientX, e.clientY);
        if (element != null && element.tagName == "TD") {
            if ($(element).has("div.assignments").length == 0) {
                return;
            }
            e.preventDefault();
            var row = $(element.parentNode);
            var cells = row.children();
            var location = "";
            cells
                .filter(function (index) { return index < 2 || index == 3; })
                .each(function (index, c) {
                var cell = $(c);
                if (index == 0) {
                    _this.set("locationId", cell.find("input[name=locationId]").val());
                }
                location += " " + cell.text();
            });
            this.set("location", location);
            var clickedIndex = cells.index($(element));
            this.set("date", $(assignmentsGrid.thead.find("th")[clickedIndex]).data("date"));
            $("#create-assignment-window").data("kendoWindow").center().open();
        }
    };
    return CreateAssignmentViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=CreateAssignmentViewModel.js.map