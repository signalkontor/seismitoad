﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
/// <reference path="Initialization.ts" />
/// <reference path="AssignAjaxHelper.ts" />

declare var actionsViewModel: ActionsViewModel;

interface IRole {
    id: number;
    name: string;
}

interface IAssignedEmployee {
    dateStart: string;
    dateEnd: string;
    location: string;
    firstname: string;
    lastname: string;
    roleIds: number[];
    notes: string;
    marker: number;
}

class EmployeeViewModel extends kendo.data.ObservableObject {
    originalDateStart: Date;
    originalDateEnd: Date;
    dateStart: Date;
    dateEnd: Date;
    assignmentId: number;
    newId: number;
    newFirstname: string;
    newLastname: string;
    newRoles: IRole[];
    currentId: number;
    currentFirstname: string;
    currentLastname: string;
    currentRolesText: string;
    reason: string = "no change";
    notify: string = "False";
    customSelectionFor: string;
    customSelectionText: string;
    customSelectionButtonText: string;
    notes: string;
    marked: boolean;
    checkedAssignments = {};

    parameterMap(data: any) {
        var value = "";
        if (typeof (data) == "object" && typeof (data.filter) == "object" && typeof (data.filter.filters) == "object" && data.filter.filters.length == 1) {
            value = data.filter.filters[0].value;
        }
        return {
            text: value
        };
    }

    /* DataSources */
    allSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "CM/Assign/_Employees"
            },
            parameterMap: data => { return this.parameterMap(data); }
        },
        serverFiltering: true
    });

    nearbySource = new kendo.data.DataSource({
        transport: {
            read: { url: appPath + "CM/Assign/_NearbyEmployees" }
        }
    });

    campaignSource: kendo.data.DataSource;

    assignmentsSource = new kendo.data.DataSource({
        type: 'aspnetmvc-ajax',
        transport: {
            read: {
                url: appPath + "CM/Assignment/_FollowingAssignments",
                data: () => {
                    return {
                        refAssignmentId: this.get("assignmentId"),
                        currentEmployeeId: this.get("currentId"),
                        newEmployeeId: this.get("newId"),
                        for: this.get("customSelectionFor"),
                    };
                }
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "Id",
                fields: {
                    Id: { editable: false, type: "number" },
                    DateStart: { editable: false, type: "date" },
                    DateEnd: { editable: false,type: "date" },
                    Team: { editable: false, type: "string" },
                    Conflicts: { editable: false, type: "string" },
                    Assigned: { type: "boolean" }
                }
            }
        }
    });

    rolesSource = new kendo.data.DataSource({
        data: [
            { id: 1, name: "Fachberater/in*" },
            { id: 2, name: "Teamleiter/in*" },
            { id: 3, name: "Fachberater/in" },
            { id: 9, name: "Teamleiter/in" },
            { id: 4, name: "Host/Hostess" },
            { id: 5, name: "Moderator/in" },
            { id: 6, name: "Techniker/in" },
            { id: 7, name: "Fahrer/in" },
            { id: 8, name: "Provisionseinsatz" },
            { id: 10, name: "CM" },
            { id: 11, name: "FM" },
            { id: 12, name: "VKU" },
        ]
    });

    /* Dieses Dummy-Properties haben wir, damit wir beim öffnen des Fensters die Dropdowns/Comboboxen löschen können */
    nearbyValue: any;
    campaignValue: any;
    allValue: any;


    constructor(private ajaxHelper: AssignAjaxHelper, private campaignId: number) {
        super();

        this.assignmentId = 0;

        this.newId = 0;
        this.newFirstname = "";
        this.newLastname = "";
        this.newRoles = [];

        this.currentId = this.newId;
        this.currentFirstname = this.newFirstname;
        this.currentLastname = this.newLastname;

        this.campaignSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/Assign/_CampaignEmployees?campaignId=" + campaignId
                },
            },
            serverFiltering: false
        });
        this.nearbySource.options.transport.read.data = { assignmentId: -1 };
        super.init(this);
    }

    private updateCustomSelection(checkbox: HTMLInputElement) {
        var row = $(checkbox).closest("tr"),
            grid = $("#followingAssignments .k-grid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
        this.checkedAssignments[dataItem["Id"]] = checkbox.checked;
    }

    private getCheckedAssignmentsArray(): any[] {
        var result = [];
        for (var property in this.checkedAssignments) {
            if (this.checkedAssignments.hasOwnProperty(property) &&
                typeof (this.checkedAssignments[property]) === "boolean" && this.checkedAssignments[property]) {
                result.push(property);
            }
        }
        return result;
    }

    windowClose() {
        actionsViewModel.popupClosed();
    }

    private setCurrentAndNewValues(firstname: string, lastname: string, roleIds: number[]) {
        this.set("currentFirstname", firstname);
        this.set("currentLastname", lastname);
        this.set("newFirstname", firstname);
        this.set("newLastname", lastname);

        var roles = roleIds.map(roleId => {
            return this.rolesSource.data().find((item:any) => item.id == roleId);
        });
        this.set("newRoles", roles);
        this.set("currentRolesText", this.newRolesText());
    }

    refresh() {
        var self = this;
        var employeeId = this.get("currentId");
        this.nearbySource.options.transport.read.data = { assignmentId: this.assignmentId };
        $.getJSON(appPath + "CM/Assign/_AssignedEmployee", { employeeId: employeeId, assignmentId: this.assignmentId })
            .done((data: IAssignedEmployee) => {
                var dateStart = kendo.parseDate(data.dateStart);
                var dateEnd = kendo.parseDate(data.dateEnd);
                self.setCurrentAndNewValues(data.firstname, data.lastname, data.roleIds);
                self.set("notify", "False");
                self.set("originalDateStart", dateStart);
                self.set("dateStart", dateStart);
                self.set("originalDateEnd", dateEnd);
                self.set("dateEnd", dateEnd);
                self.set("allValue", null);
                self.set("nearbyValue", null);
                self.set("campaignValue", null);
                self.set("notes", data.notes);
                self.set("marked", data.marker > 0);
                this.checkedAssignments = {};
                self.set("reason", "no change");
                self.reasonChange();
                employeeEditorWindowElement.prev().children(".k-window-title")
                    .html('<span style="float:left; font-weight: bold">' + data.location + '</span><span style ="padding-right: 20px; float: right; font-weight: bold">' + kendo.format("{0:D}", dateStart) + '</span >');
                employeeEditorWindowElement.data("kendoWindow").open().center();
            });
    }

    isAssigned(): boolean {
        return this.get("currentId") > 0;
    }

    submitParams(repeat: number): IUpdateEmployeeModel {
        return {
            assignmentId: this.assignmentId,
            previousEmployeeId: this.get("currentId"),
            newEmployeeId: this.get("newId"),
            newRoleIds: (<IRole[]>this.get("newRoles")).map(role => role.id),
            repeat: repeat,
            additionalAssignmentIds: this.getCheckedAssignmentsArray(),
            confirmed: false,
            notify: this.get("notify"),
            success: () => {
                assignmentsGrid.dataSource.read();
                this.set("currentId", this.get("newId"));
                this.set("currentFirstname", this.get("newFirstname"));
                this.set("currentLastname", this.get("newLastname"));
                this.set("currentRolesText", this.get("newRolesText"));
                if (repeat === 3) {
                    $("#followingAssignments").data("kendoWindow").close();
                }
            }
        };
    }

    submit() {
        this.ajaxHelper.updateEmployee(this.submitParams(0));
    }

    submitWeekday() {
        this.ajaxHelper.updateEmployee(this.submitParams(1));
    }

    submitAll() {
        this.ajaxHelper.updateEmployee(this.submitParams(2));
    }

    submitCustom() {
        var promoterName = this.get("newFirstname") + " " + this.get("newLastname"),
            message = promoterName + " den gewählten Folgeterminen zuweisen";

        if (this.isAssigned() && this.get("currentId") != this.get("newId")) {
            message += " und dabei " + this.get("currentFirstname") + " " + this.get("currentLastname") + " ersetzen.";
        }
        $("#checkAll").prop("checked", false);
        this.set("customSelectionFor", "assign");
        this.set("customSelectionText", message);
        this.set("customSelectionButtonText", "Promoter dem aktuellen und den gewählten Folgeterminen zuweisen.");
        this.assignmentsSource.read();
        $("#followingAssignments").data("kendoWindow").center().open();
    }

    submitEnabled(): boolean {
        var r = this.get("newRoles");
        return this.get("newId") > 0 && r != null && r.length > 0;
    }

    submitText(): string {
        return this.get("currentId") > 0 ? "Ersetzen" : "Zuweisen<br />(unbes. Tage)";
    }

    removeParams(repeat: number): IRemoveEmployeeModel {
        return {
            dataId: null,
            assignmentId: this.assignmentId,
            employeeId: this.get("currentId"),
            repeat: repeat,
            additionalAssignmentIds: this.getCheckedAssignmentsArray(),
            confirmed: false,
            notify: this.get("notify"),
            success: () => {
                this.set("currentId", 0);
                assignmentsGrid.dataSource.read();
                if (repeat === 3) {
                    $("#followingAssignments").data("kendoWindow").close();
                }
            }
        }
    }

    remove() {
        this.ajaxHelper.removeEmployee(this.removeParams(0));
    }

    removeWeekday() {
        this.ajaxHelper.removeEmployee(this.removeParams(1));
    }

    removeAll() {
        this.ajaxHelper.removeEmployee(this.removeParams(2));
    }

    removeCustom() {
        var promoterName = this.get("currentFirstname") + " " + this.get("currentLastname"),
            message = promoterName + " von den gewählten Folgeterminen entfernen";
        $("#checkAll").prop("checked", false);
        this.set("customSelectionFor", "remove");
        this.set("customSelectionText", message);
        this.set("customSelectionButtonText", "Den Promoter vom aktuellen und den gewählten Folgeterminen entfernen");
        this.assignmentsSource.read();
        employeeEditorWindowElement.data("kendoWindow").close();
        $("#followingAssignments").data("kendoWindow").center().open();
    }

    customSelectionButtonClick() {
        var params;
        if (this.get("customSelectionFor") === "remove") {
            this.ajaxHelper.removeEmployee(this.removeParams(3));
        } else {
            this.ajaxHelper.updateEmployee(this.submitParams(3));
        }
    }

    reasonChange() {
        switch (this.get("reason")) {
            case "no change":
                $("#assign-area").unblock();
                this.set("dateStart", this.get("originalDateStart"));
                this.set("dateEnd", this.get("originalDateEnd"));
                break;
            case "change time":
                this.set("dateStart", this.get("originalDateStart"));
                this.set("dateEnd", this.get("originalDateEnd"));
                // Fall-Through ist erwünscht. Auch bei change time soll geblockt werden.
            default:
                $("#assign-area").block({ message: "Nicht verfügbar, während der Aktionstag verschoben wird." });
                break;
        }
    }

    dateChangeDisabled() {
        var reason = <string>this.get("reason");
        return (reason === "no change") || (reason === "change time");
    }

    timeOrDateChange() {
        var reason = <string>this.get("reason");
        return reason !== "no change";
    }

    private dateDifferenceInDays(first: Date, second: Date) {
        if (typeof (first) === "undefined" || typeof (second) === "undefined") {
            return 0;
        }
        var utc1 = Date.UTC(first.getFullYear(), first.getMonth(), first.getDate());
        var utc2 = Date.UTC(second.getFullYear(), second.getMonth(), second.getDate());
        return Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
    }

    differenceInDaysText() {
        var newStart = this.get("dateStart"),
            diff = this.dateDifferenceInDays(this.originalDateStart, newStart);
        return kendo.format("{0} {1}", diff, Math.abs(diff) === 1 ? "Tag" : "Tage");
    }

    newTimesText() {
        var newStart = this.get("dateStart"),
            newEnd = this.get("dateEnd");
        return kendo.format("{0:HH:mm} bis {1:HH:mm}", newStart, newEnd);
    }

    dateChange() {
        var start = <Date>this.get("dateStart");
        if (start == null) {
            start = this.originalDateStart;
            this.set("dateStart", start);
        }
        var end = <Date>this.get("dateEnd");
        var newEnd = new Date(start.getFullYear(), start.getMonth(), start.getDate(), end.getHours(), end.getMinutes());
        this.set("dateEnd", newEnd);
    }

    timeChange() {
        var start = <Date>this.get("dateStart");
        if (start == null) {
            start = this.originalDateStart;
            this.set("dateStart", start);
        }

        var end = <Date>this.get("dateEnd");
        if (end == null) {
            end = this.originalDateEnd;
            this.set("dateEnd", end);
        }

        // Copy the date objects
        start = new Date(start.valueOf());
        end = new Date(start.getFullYear(), start.getMonth(), start.getDate(), end.getHours(), end.getMinutes(), end.getSeconds());

        if (end.getHours() < 5) {
            end.setDate(end.getDate() + 1);
        }

        var timePicker = <kendo.ui.TimePicker>$("#endTime").data("kendoTimePicker");
        timePicker.min(start);
        if (start > end) {
            end = new Date(start.valueOf());
            this.set("dateEnd", end);
        }
    }

    timeDateChangeParams(repeat: number): IUpdateDateOrTimeModel {
        var dateStart = this.get("dateStart"),
            dateEnd = this.get("dateEnd");
        return {
            assignmentId: this.assignmentId,
            start: dateStart,
            end: dateEnd,
            reason: this.get("reason"),
            repeat: repeat,
            confirmed: false,
            notify: this.get("notify"),
            success: (assignmentId: number) => {
                this.set("originalDateStart", dateStart);
                this.set("originalDateEnd", dateEnd);
                this.set("reason", "no change");
                this.assignmentId = assignmentId;
                this.reasonChange();
                assignmentsGrid.dataSource.read();
            }
        };
    }

    changeTimeOrDate() {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(0));
    }

    changeTimeOrDateWeekday() {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(1));
    }

    changeTimeOrDateAll() {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(2));
    }

    markerChanged() {
        $.post(appPath + "CM/Assign/_Marker",
            { assignmentId: this.assignmentId, marker: this.get("marked") ? 1 : 0 },
            (data, textStatus, jqXHR) => {
                assignmentsGrid.dataSource.read();
            });
    }

    notesChanged() {
        $.post(appPath + "CM/Assign/_Notes",
            { assignmentId: this.assignmentId, notes: this.get("notes") },
            (data, textStatus, jqXHR) => {
                assignmentsGrid.dataSource.read();
            });
    }

    update(e: kendo.ui.DropDownListSelectEvent) {
        var employee = e.sender.dataItem(e.item.index());
        this.set("newId", employee.Id);
        this.set("newFirstname", employee.Firstname);
        this.set("newLastname", employee.Lastname);
    }

    updateRoles(e: kendo.ui.MultiSelectChangeEvent) {
        var dataItems = e.sender.dataItems();
        var roles = "";
        for (var i = 0; i < dataItems.length; i++) {
            if (i > 0) {
                roles += ", ";
            }
            roles += dataItems[i].Text;
        }
        this.set("newRolesText", roles);
    }

    newRolesText(): string {
        var roles = "";
        var newRoles = this.get("newRoles");
        for (var i = 0; i < newRoles.length; i++) {
            if (i > 0) {
                roles += ", ";
            }
            roles += newRoles[i].name;
        }
        return roles;
    }

    weekDayButtonText(): string {
        return kendo.format("Jeden {0:dddd} ab {0:d}", this.get("originalDateStart"));
    }

    allButtonText(): string {
        return kendo.format("Alle Einsätze ab {0:d}", this.get("originalDateStart"));
    }

    openCurrentProfile() {
        var win = window.open(appPath + "MD/PromoterProfile/" + this.get("currentId"), "_blank");
        win.focus();
    }

    openNewProfile() {
        var win = window.open(appPath + "MD/PromoterProfile/" + this.get("newId"), "_blank");
        win.focus();
    }
}