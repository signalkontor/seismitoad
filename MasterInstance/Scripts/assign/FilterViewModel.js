var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var FilterViewModel = (function (_super) {
    __extends(FilterViewModel, _super);
    function FilterViewModel(filterItems, locationItemsUrl) {
        var _this = _super.call(this) || this;
        _this.date = new Date();
        _this.filterItems = filterItems;
        _this.locationItems = new kendo.data.DataSource({
            transport: {
                read: { url: locationItemsUrl }
            }
        });
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    FilterViewModel.prototype._fixDate = function () {
        var date = this.get("date");
        if (date === null) {
            this.set("date", new Date());
        }
    };
    FilterViewModel.prototype.monday = function () {
        this._fixDate();
        var date = this.get("date");
        var dayOfWeek = date.getDay() - 1;
        if (dayOfWeek < 0) {
            dayOfWeek = 6;
        }
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() - dayOfWeek);
    };
    FilterViewModel.prototype.applyFilter = function () {
        this._fixDate();
        this.timeoutId = window.setTimeout(function () {
            assignmentsGrid.dataSource.read();
        }, 1000);
    };
    FilterViewModel.prototype.refresh = function () {
        assignmentsGrid.dataSource.read();
    };
    FilterViewModel.prototype.nextWeek = function () {
        this._fixDate();
        var date = this.get("date");
        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7);
        this.set("date", newDate);
        this.applyFilter();
    };
    FilterViewModel.prototype.previousWeek = function () {
        this._fixDate();
        var date = this.get("date");
        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
        this.set("date", newDate);
        this.applyFilter();
    };
    FilterViewModel.prototype.weekOfYear = function () {
        this._fixDate();
        return this.get("date").getWeek();
    };
    FilterViewModel.prototype._toValueArray = function (array) {
        return array && ($.isArray(array) || array instanceof kendo.data.ObservableArray)
            ? array.map(function (element) { return element.Value; })
            : [];
    };
    FilterViewModel.prototype.gridParams = function () {
        return {
            start: kendo.toString(this.monday(), "yyyy-MM-dd"),
            f: this._toValueArray(this.filters),
            l: this._toValueArray(this.locations)
        };
    };
    FilterViewModel.prototype.updateHeader = function (e) {
        var monday = this.monday(), grid = e.sender, aggregates = grid.dataSource.aggregates(), tmpDate = new Date(monday.getTime());
        $(".k-grid-header th", assignmentsGridElement)
            .filter(function (index) { return index > 3; })
            .each(function (index, element) {
            var aggregate, total = 0, assigned = 0;
            if (typeof (aggregates) !== "undefined" && aggregates !== null) {
                aggregate = aggregates[element.attributes["data-field"].value];
                total = aggregate.sum;
                assigned = aggregate.count;
            }
            $(element)
                .data("date", new Date(tmpDate.getTime()))
                .contents().last().replaceWith(kendo.format("<span>{0:ddd dd.MM.yy} <span style='float: right; padding-right: 8px'>{1}/{2}</span></span>", tmpDate, assigned, total));
            tmpDate.setDate(tmpDate.getDate() + 1);
        });
    };
    return FilterViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=FilterViewModel.js.map