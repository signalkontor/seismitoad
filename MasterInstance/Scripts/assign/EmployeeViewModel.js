var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var EmployeeViewModel = (function (_super) {
    __extends(EmployeeViewModel, _super);
    function EmployeeViewModel(ajaxHelper, campaignId) {
        var _this = _super.call(this) || this;
        _this.ajaxHelper = ajaxHelper;
        _this.campaignId = campaignId;
        _this.reason = "no change";
        _this.notify = "False";
        _this.checkedAssignments = {};
        _this.allSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/Assign/_Employees"
                },
                parameterMap: function (data) { return _this.parameterMap(data); }
            },
            serverFiltering: true
        });
        _this.nearbySource = new kendo.data.DataSource({
            transport: {
                read: { url: appPath + "CM/Assign/_NearbyEmployees" }
            }
        });
        _this.assignmentsSource = new kendo.data.DataSource({
            type: 'aspnetmvc-ajax',
            transport: {
                read: {
                    url: appPath + "CM/Assignment/_FollowingAssignments",
                    data: function () {
                        return {
                            refAssignmentId: _this.get("assignmentId"),
                            currentEmployeeId: _this.get("currentId"),
                            newEmployeeId: _this.get("newId"),
                            for: _this.get("customSelectionFor"),
                        };
                    }
                }
            },
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        DateStart: { editable: false, type: "date" },
                        DateEnd: { editable: false, type: "date" },
                        Team: { editable: false, type: "string" },
                        Conflicts: { editable: false, type: "string" },
                        Assigned: { type: "boolean" }
                    }
                }
            }
        });
        _this.rolesSource = new kendo.data.DataSource({
            data: [
                { id: 1, name: "Fachberater/in*" },
                { id: 2, name: "Teamleiter/in*" },
                { id: 3, name: "Fachberater/in" },
                { id: 9, name: "Teamleiter/in" },
                { id: 4, name: "Host/Hostess" },
                { id: 5, name: "Moderator/in" },
                { id: 6, name: "Techniker/in" },
                { id: 7, name: "Fahrer/in" },
                { id: 8, name: "Provisionseinsatz" },
                { id: 10, name: "CM" },
                { id: 11, name: "FM" },
                { id: 12, name: "VKU" },
            ]
        });
        _this.assignmentId = 0;
        _this.newId = 0;
        _this.newFirstname = "";
        _this.newLastname = "";
        _this.newRoles = [];
        _this.currentId = _this.newId;
        _this.currentFirstname = _this.newFirstname;
        _this.currentLastname = _this.newLastname;
        _this.campaignSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/Assign/_CampaignEmployees?campaignId=" + campaignId
                },
            },
            serverFiltering: false
        });
        _this.nearbySource.options.transport.read.data = { assignmentId: -1 };
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    EmployeeViewModel.prototype.parameterMap = function (data) {
        var value = "";
        if (typeof (data) == "object" && typeof (data.filter) == "object" && typeof (data.filter.filters) == "object" && data.filter.filters.length == 1) {
            value = data.filter.filters[0].value;
        }
        return {
            text: value
        };
    };
    EmployeeViewModel.prototype.updateCustomSelection = function (checkbox) {
        var row = $(checkbox).closest("tr"), grid = $("#followingAssignments .k-grid").data("kendoGrid"), dataItem = grid.dataItem(row);
        this.checkedAssignments[dataItem["Id"]] = checkbox.checked;
    };
    EmployeeViewModel.prototype.getCheckedAssignmentsArray = function () {
        var result = [];
        for (var property in this.checkedAssignments) {
            if (this.checkedAssignments.hasOwnProperty(property) &&
                typeof (this.checkedAssignments[property]) === "boolean" && this.checkedAssignments[property]) {
                result.push(property);
            }
        }
        return result;
    };
    EmployeeViewModel.prototype.windowClose = function () {
        actionsViewModel.popupClosed();
    };
    EmployeeViewModel.prototype.setCurrentAndNewValues = function (firstname, lastname, roleIds) {
        var _this = this;
        this.set("currentFirstname", firstname);
        this.set("currentLastname", lastname);
        this.set("newFirstname", firstname);
        this.set("newLastname", lastname);
        var roles = roleIds.map(function (roleId) {
            return _this.rolesSource.data().find(function (item) { return item.id == roleId; });
        });
        this.set("newRoles", roles);
        this.set("currentRolesText", this.newRolesText());
    };
    EmployeeViewModel.prototype.refresh = function () {
        var _this = this;
        var self = this;
        var employeeId = this.get("currentId");
        this.nearbySource.options.transport.read.data = { assignmentId: this.assignmentId };
        $.getJSON(appPath + "CM/Assign/_AssignedEmployee", { employeeId: employeeId, assignmentId: this.assignmentId })
            .done(function (data) {
            var dateStart = kendo.parseDate(data.dateStart);
            var dateEnd = kendo.parseDate(data.dateEnd);
            self.setCurrentAndNewValues(data.firstname, data.lastname, data.roleIds);
            self.set("notify", "False");
            self.set("originalDateStart", dateStart);
            self.set("dateStart", dateStart);
            self.set("originalDateEnd", dateEnd);
            self.set("dateEnd", dateEnd);
            self.set("allValue", null);
            self.set("nearbyValue", null);
            self.set("campaignValue", null);
            self.set("notes", data.notes);
            self.set("marked", data.marker > 0);
            _this.checkedAssignments = {};
            self.set("reason", "no change");
            self.reasonChange();
            employeeEditorWindowElement.prev().children(".k-window-title")
                .html('<span style="float:left; font-weight: bold">' + data.location + '</span><span style ="padding-right: 20px; float: right; font-weight: bold">' + kendo.format("{0:D}", dateStart) + '</span >');
            employeeEditorWindowElement.data("kendoWindow").open().center();
        });
    };
    EmployeeViewModel.prototype.isAssigned = function () {
        return this.get("currentId") > 0;
    };
    EmployeeViewModel.prototype.submitParams = function (repeat) {
        var _this = this;
        return {
            assignmentId: this.assignmentId,
            previousEmployeeId: this.get("currentId"),
            newEmployeeId: this.get("newId"),
            newRoleIds: this.get("newRoles").map(function (role) { return role.id; }),
            repeat: repeat,
            additionalAssignmentIds: this.getCheckedAssignmentsArray(),
            confirmed: false,
            notify: this.get("notify"),
            success: function () {
                assignmentsGrid.dataSource.read();
                _this.set("currentId", _this.get("newId"));
                _this.set("currentFirstname", _this.get("newFirstname"));
                _this.set("currentLastname", _this.get("newLastname"));
                _this.set("currentRolesText", _this.get("newRolesText"));
                if (repeat === 3) {
                    $("#followingAssignments").data("kendoWindow").close();
                }
            }
        };
    };
    EmployeeViewModel.prototype.submit = function () {
        this.ajaxHelper.updateEmployee(this.submitParams(0));
    };
    EmployeeViewModel.prototype.submitWeekday = function () {
        this.ajaxHelper.updateEmployee(this.submitParams(1));
    };
    EmployeeViewModel.prototype.submitAll = function () {
        this.ajaxHelper.updateEmployee(this.submitParams(2));
    };
    EmployeeViewModel.prototype.submitCustom = function () {
        var promoterName = this.get("newFirstname") + " " + this.get("newLastname"), message = promoterName + " den gewählten Folgeterminen zuweisen";
        if (this.isAssigned() && this.get("currentId") != this.get("newId")) {
            message += " und dabei " + this.get("currentFirstname") + " " + this.get("currentLastname") + " ersetzen.";
        }
        $("#checkAll").prop("checked", false);
        this.set("customSelectionFor", "assign");
        this.set("customSelectionText", message);
        this.set("customSelectionButtonText", "Promoter dem aktuellen und den gewählten Folgeterminen zuweisen.");
        this.assignmentsSource.read();
        $("#followingAssignments").data("kendoWindow").center().open();
    };
    EmployeeViewModel.prototype.submitEnabled = function () {
        var r = this.get("newRoles");
        return this.get("newId") > 0 && r != null && r.length > 0;
    };
    EmployeeViewModel.prototype.submitText = function () {
        return this.get("currentId") > 0 ? "Ersetzen" : "Zuweisen<br />(unbes. Tage)";
    };
    EmployeeViewModel.prototype.removeParams = function (repeat) {
        var _this = this;
        return {
            dataId: null,
            assignmentId: this.assignmentId,
            employeeId: this.get("currentId"),
            repeat: repeat,
            additionalAssignmentIds: this.getCheckedAssignmentsArray(),
            confirmed: false,
            notify: this.get("notify"),
            success: function () {
                _this.set("currentId", 0);
                assignmentsGrid.dataSource.read();
                if (repeat === 3) {
                    $("#followingAssignments").data("kendoWindow").close();
                }
            }
        };
    };
    EmployeeViewModel.prototype.remove = function () {
        this.ajaxHelper.removeEmployee(this.removeParams(0));
    };
    EmployeeViewModel.prototype.removeWeekday = function () {
        this.ajaxHelper.removeEmployee(this.removeParams(1));
    };
    EmployeeViewModel.prototype.removeAll = function () {
        this.ajaxHelper.removeEmployee(this.removeParams(2));
    };
    EmployeeViewModel.prototype.removeCustom = function () {
        var promoterName = this.get("currentFirstname") + " " + this.get("currentLastname"), message = promoterName + " von den gewählten Folgeterminen entfernen";
        $("#checkAll").prop("checked", false);
        this.set("customSelectionFor", "remove");
        this.set("customSelectionText", message);
        this.set("customSelectionButtonText", "Den Promoter vom aktuellen und den gewählten Folgeterminen entfernen");
        this.assignmentsSource.read();
        employeeEditorWindowElement.data("kendoWindow").close();
        $("#followingAssignments").data("kendoWindow").center().open();
    };
    EmployeeViewModel.prototype.customSelectionButtonClick = function () {
        var params;
        if (this.get("customSelectionFor") === "remove") {
            this.ajaxHelper.removeEmployee(this.removeParams(3));
        }
        else {
            this.ajaxHelper.updateEmployee(this.submitParams(3));
        }
    };
    EmployeeViewModel.prototype.reasonChange = function () {
        switch (this.get("reason")) {
            case "no change":
                $("#assign-area").unblock();
                this.set("dateStart", this.get("originalDateStart"));
                this.set("dateEnd", this.get("originalDateEnd"));
                break;
            case "change time":
                this.set("dateStart", this.get("originalDateStart"));
                this.set("dateEnd", this.get("originalDateEnd"));
            default:
                $("#assign-area").block({ message: "Nicht verfügbar, während der Aktionstag verschoben wird." });
                break;
        }
    };
    EmployeeViewModel.prototype.dateChangeDisabled = function () {
        var reason = this.get("reason");
        return (reason === "no change") || (reason === "change time");
    };
    EmployeeViewModel.prototype.timeOrDateChange = function () {
        var reason = this.get("reason");
        return reason !== "no change";
    };
    EmployeeViewModel.prototype.dateDifferenceInDays = function (first, second) {
        if (typeof (first) === "undefined" || typeof (second) === "undefined") {
            return 0;
        }
        var utc1 = Date.UTC(first.getFullYear(), first.getMonth(), first.getDate());
        var utc2 = Date.UTC(second.getFullYear(), second.getMonth(), second.getDate());
        return Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
    };
    EmployeeViewModel.prototype.differenceInDaysText = function () {
        var newStart = this.get("dateStart"), diff = this.dateDifferenceInDays(this.originalDateStart, newStart);
        return kendo.format("{0} {1}", diff, Math.abs(diff) === 1 ? "Tag" : "Tage");
    };
    EmployeeViewModel.prototype.newTimesText = function () {
        var newStart = this.get("dateStart"), newEnd = this.get("dateEnd");
        return kendo.format("{0:HH:mm} bis {1:HH:mm}", newStart, newEnd);
    };
    EmployeeViewModel.prototype.dateChange = function () {
        var start = this.get("dateStart");
        if (start == null) {
            start = this.originalDateStart;
            this.set("dateStart", start);
        }
        var end = this.get("dateEnd");
        var newEnd = new Date(start.getFullYear(), start.getMonth(), start.getDate(), end.getHours(), end.getMinutes());
        this.set("dateEnd", newEnd);
    };
    EmployeeViewModel.prototype.timeChange = function () {
        var start = this.get("dateStart");
        if (start == null) {
            start = this.originalDateStart;
            this.set("dateStart", start);
        }
        var end = this.get("dateEnd");
        if (end == null) {
            end = this.originalDateEnd;
            this.set("dateEnd", end);
        }
        start = new Date(start.valueOf());
        end = new Date(start.getFullYear(), start.getMonth(), start.getDate(), end.getHours(), end.getMinutes(), end.getSeconds());
        if (end.getHours() < 5) {
            end.setDate(end.getDate() + 1);
        }
        var timePicker = $("#endTime").data("kendoTimePicker");
        timePicker.min(start);
        if (start > end) {
            end = new Date(start.valueOf());
            this.set("dateEnd", end);
        }
    };
    EmployeeViewModel.prototype.timeDateChangeParams = function (repeat) {
        var _this = this;
        var dateStart = this.get("dateStart"), dateEnd = this.get("dateEnd");
        return {
            assignmentId: this.assignmentId,
            start: dateStart,
            end: dateEnd,
            reason: this.get("reason"),
            repeat: repeat,
            confirmed: false,
            notify: this.get("notify"),
            success: function (assignmentId) {
                _this.set("originalDateStart", dateStart);
                _this.set("originalDateEnd", dateEnd);
                _this.set("reason", "no change");
                _this.assignmentId = assignmentId;
                _this.reasonChange();
                assignmentsGrid.dataSource.read();
            }
        };
    };
    EmployeeViewModel.prototype.changeTimeOrDate = function () {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(0));
    };
    EmployeeViewModel.prototype.changeTimeOrDateWeekday = function () {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(1));
    };
    EmployeeViewModel.prototype.changeTimeOrDateAll = function () {
        this.ajaxHelper.updateDate(this.timeDateChangeParams(2));
    };
    EmployeeViewModel.prototype.markerChanged = function () {
        $.post(appPath + "CM/Assign/_Marker", { assignmentId: this.assignmentId, marker: this.get("marked") ? 1 : 0 }, function (data, textStatus, jqXHR) {
            assignmentsGrid.dataSource.read();
        });
    };
    EmployeeViewModel.prototype.notesChanged = function () {
        $.post(appPath + "CM/Assign/_Notes", { assignmentId: this.assignmentId, notes: this.get("notes") }, function (data, textStatus, jqXHR) {
            assignmentsGrid.dataSource.read();
        });
    };
    EmployeeViewModel.prototype.update = function (e) {
        var employee = e.sender.dataItem(e.item.index());
        this.set("newId", employee.Id);
        this.set("newFirstname", employee.Firstname);
        this.set("newLastname", employee.Lastname);
    };
    EmployeeViewModel.prototype.updateRoles = function (e) {
        var dataItems = e.sender.dataItems();
        var roles = "";
        for (var i = 0; i < dataItems.length; i++) {
            if (i > 0) {
                roles += ", ";
            }
            roles += dataItems[i].Text;
        }
        this.set("newRolesText", roles);
    };
    EmployeeViewModel.prototype.newRolesText = function () {
        var roles = "";
        var newRoles = this.get("newRoles");
        for (var i = 0; i < newRoles.length; i++) {
            if (i > 0) {
                roles += ", ";
            }
            roles += newRoles[i].name;
        }
        return roles;
    };
    EmployeeViewModel.prototype.weekDayButtonText = function () {
        return kendo.format("Jeden {0:dddd} ab {0:d}", this.get("originalDateStart"));
    };
    EmployeeViewModel.prototype.allButtonText = function () {
        return kendo.format("Alle Einsätze ab {0:d}", this.get("originalDateStart"));
    };
    EmployeeViewModel.prototype.openCurrentProfile = function () {
        var win = window.open(appPath + "MD/PromoterProfile/" + this.get("currentId"), "_blank");
        win.focus();
    };
    EmployeeViewModel.prototype.openNewProfile = function () {
        var win = window.open(appPath + "MD/PromoterProfile/" + this.get("newId"), "_blank");
        win.focus();
    };
    return EmployeeViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=EmployeeViewModel.js.map