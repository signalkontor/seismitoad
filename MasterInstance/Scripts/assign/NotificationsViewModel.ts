﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />
/// <reference path="Initialization.ts" />

class NotificationsViewModel extends kendo.data.ObservableObject {
    buttonsEnabled: boolean = false;

    constructor() {
        super();
        super.init(this);
    }
    
    sendNotifications() {
        var ids = new Array<string>();
        $("#notifications .k-grid-content :checked").each((index, element) => {
            ids.push($(element).attr("name"));
        });
        this.postNotifications(appPath + "CM/Assign/_SendNotifications", ids);
    }

    deleteNotifications() {
        var ids = new Array<number>();
        $("#notifications .k-grid-content :checked").each((index, element) => {
            ids.push(parseInt($(element).attr("value")));
        });
        this.postNotifications(appPath + "CM/Assign/_DeleteNotifications", ids);
    }

    private postNotifications(url: string, ids: any[]) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(ids),
            success: () => {
                $("#checkAllNotifications").prop("checked", false);
                $("#notifications").data("kendoGrid").dataSource.read();
                filterViewModel.refresh();
            }
        });
    }
}

$(document).ready(() => {
    var viewModel = new NotificationsViewModel();
    kendo.bind("#notifications-window", viewModel);

    $("#checkAllNotifications").on("change", event => {
        var checkbox = <HTMLInputElement>event.target;
        $("#notifications .k-grid-content :checkbox").prop("checked", checkbox.checked).change();
    });

    // The Event-Handler needs to be a function and not a lamdba expression.
    // This is because TypeScript will change this to _this in lambdas
    $("#notifications .k-grid-content")
        .on("click", "tr", function(event) {
            if ((<HTMLElement>event.target).tagName !== "INPUT" &&
            (<HTMLInputElement>event.target).type !== 'checkbox') {
                $(":checkbox", this).click();
            }
        })
        .on("change", "input[type=checkbox]", () => {
            viewModel.set("buttonsEnabled", $("#notifications .k-grid-content :checked").length > 0);
        });
})