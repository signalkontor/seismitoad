﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.blockUI/jquery.blockUI.d.ts" />
/// <reference path="../kendo.all.d.ts" />
/// <reference path="Initialization.ts" />

// Sollte immer zusammen mit der .NET-Klasse AssignmentConflict geändert werden
interface IConflict {
    dataId: string;
    assignmentId: number;
    date: string;
    campaigns: string;
    cities: string;
}

// Sollte immer zusammen mit der .NET-Klasse AjaxActionResultModel geändert werden
interface IAjaxActionResultModel {
    status: string;
    message: string;
    employeeId: number;
    assignmentId?: number;
    employee: string;
    location: string;
    dataId: string;
    conflicts: IConflict[];
}

interface IRemoveEmployeeModel {
    dataId: string;
    assignmentId: number;
    employeeId: number;
    repeat: number;
    additionalAssignmentIds: number[];
    confirmed: boolean;
    notify: string;
    success: () => void;
}

interface IUpdateEmployeeModel {
    assignmentId: number;
    previousEmployeeId: number;
    newEmployeeId: number;
    newRoleIds: number[];
    repeat: number;
    additionalAssignmentIds: number[];
    confirmed: boolean;
    notify: string;
    success: () => void;
}

interface IUpdateDateOrTimeModel {
    assignmentId: number;
    start: Date;
    end: Date;
    reason: string;
    repeat: number;
    confirmed: boolean;
    notify: string;
    success: (assignmentId: number) => void;
}

//declare function skBlockUI() : void;

class AssignAjaxHelper {
    // 2014-04-01T12:00:00.0000000
    private dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.0000000'";
    private createAssignmentUrl = appPath + "CM/Assignment/_Create";
    private deleteAssignmentUrl = appPath + "CM/Assignment/_Delete";
    private updateDateUrl = appPath + "CM/Assignment/_UpdateDate";
    private removeEmployeeUrl = appPath + "CM/Assign/_RemoveAssignedEmployee";
    private updateEmployeeUrl = appPath + "CM/Assign/_UpdateAssignedEmployee";
    private needsConfirmation = "needs confirmation";

    updateResultHandler: (result: IAjaxActionResultModel, notify: string) => void;

    constructor(private campaignId: number) {
    }

    deleteAssignment(assignmentId: number, notify: boolean, success: () => void) {
        this.deleteAssignmentInternal(assignmentId, notify, success);
    }

    updateDate(model: IUpdateDateOrTimeModel) {
        this.updateDateInternal(model);
    }

    removeEmployee(model: IRemoveEmployeeModel) {
        this.removeEmployeeInternal(model);
    }

    updateEmployee(model: IUpdateEmployeeModel) {
        this.updateEmployeeInternal(model);
    }

    createAssignment(locationId: number, start: Date, end: Date, employeeCount: number, success: () => void) {
        skBlockUI();
        $.post(this.createAssignmentUrl, {
            campaignId: this.campaignId,
            locationId: locationId,
            start: kendo.toString(start, this.dateFormat),
            end: kendo.toString(end, this.dateFormat),
            employeeCount: employeeCount
        }).done(success).always($.unblockUI);
    }

    private deleteAssignmentInternal(assignmentId: number, notify: boolean, success: () => void, confirmed = false) {
        skBlockUI();
        $.post(this.deleteAssignmentUrl, {
                assignmentId: assignmentId,
                confirmed: confirmed,
                notify: notify
            })
            .done((result: IAjaxActionResultModel) => {
                if (result.status == this.needsConfirmation) {
                    confirmViewModel
                        .ask(result.message)
                        .onYes(() => { this.deleteAssignmentInternal(assignmentId, notify, success, true); });
                    return;
                }
                success();
            }).always($.unblockUI);
    }

    private updateDateInternal(model: IUpdateDateOrTimeModel) {
        skBlockUI();
        $.post(this.updateDateUrl, {
            assignmentId: model.assignmentId,
            start: kendo.toString(model.start, this.dateFormat),
            end: kendo.toString(model.end, this.dateFormat),
            reason: model.reason,
            repeat: model.repeat,
            confirmed: model.confirmed,
            notify: model.notify
        }).done((result: IAjaxActionResultModel) => {
            switch(result.status) {
                case this.needsConfirmation:
                    confirmViewModel
                        .ask(result.message)
                        .onYes(() => {
                            model.confirmed = true;
                            this.updateDateInternal(model);
                        });
                    return;
                case "error":
                    alert(result.message);
                    return;
                case "warning":
                    alert(result.message);
                    break;
            }
            model.success(result.assignmentId);
        })
        .always($.unblockUI);
    }

    private removeEmployeeInternal(model: IRemoveEmployeeModel) {
        var params = {
            dataId: model.dataId,
            assignmentId: model.assignmentId,
            employeeId: model.employeeId,
            repeat: model.repeat,
            additionalAssignmentIds: model.additionalAssignmentIds,
            confirmed: model.confirmed,
            notify: model.notify
        };
        skBlockUI();
        $.post(this.removeEmployeeUrl, params).done((result: IAjaxActionResultModel) => {
            if (result.status === this.needsConfirmation) {
                confirmViewModel
                    .ask(result.message)
                    .onYes(() => {
                        model.confirmed = true;
                        this.removeEmployeeInternal(model);
                    });
                return;
            }
            model.success();
        })
        .always($.unblockUI);
    }

    private updateEmployeeInternal(model: IUpdateEmployeeModel) {
        var params = {
            assignmentId: model.assignmentId,
            previousEmployeeId: model.previousEmployeeId,
            newEmployeeId: model.newEmployeeId,
            newRoleIds: model.newRoleIds,
            repeat: model.repeat,
            additionalAssignmentIds: model.additionalAssignmentIds,
            confirmed: model.confirmed,
            notify: model.notify
        };
        skBlockUI();
        $.post(this.updateEmployeeUrl, params)
            .done((result: IAjaxActionResultModel) => {
                if (result.status === this.needsConfirmation) {
                    confirmViewModel
                        .ask(result.message)
                        .onYes(() => {
                            model.confirmed = true;
                            this.updateEmployeeInternal(model);
                        });
                    return;
                }
                this.updateResultHandler(result, model.notify);
                model.success();
            })
        .always($.unblockUI);
    }
}