﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

class ConflictsViewModel extends kendo.data.ObservableObject
{
    dataId: string;
    location: string;
    employee: string;
    employeeId: number;
    dataSource: IConflict[] = [];
    notify: string;

    constructor(private ajaxHelper: AssignAjaxHelper) {
        super();
        ajaxHelper.updateResultHandler = $.proxy(this.handleEmployeeUpdateResult, this);
        super.init(this);
    }

    visible(): boolean {
        var conflicts = <IConflict[]>this.get("dataSource");

        var window = (<kendo.ui.Window>$("#conflicts-window").data("kendoWindow"));
        if (window == null) {
            return false;
        }
        var visible = conflicts.length > 0;
        if (visible) {
            window.center();
        }
        return visible;
    }

    clear(e?: kendo.ui.WindowCloseEvent) {
        // Wir wollen nicht, dass der Benutzer das Fenster per Escape-Taste schließt.
        // Daher stoppen wir das entsprechende Ereignis und setzen stattdessen 
        // dataSource auf ein leeres Array.
        if (e != null && e.userTriggered) {
            e.preventDefault();
        }
        this.sendNotification();
        this.set("dataSource", []);
    }

    private sendNotification() {
        $.ajax({
            type: "POST",
            url: appPath + "CM/Assign/_SendNotifications",
            contentType: "application/json",
            data: JSON.stringify([this.get("dataId")])
        }).always(() => { assignmentsGrid.dataSource.read(); });
    }

    removeClick(sender: kendo.ui.Grid, e: JQueryEventObject) {
        e.preventDefault();
        var dataItem = sender.dataItem($(e.currentTarget).closest("tr")),
            assignmentId = dataItem.get("assignmentId"),
            dataId = dataItem.get("dataId");

        this.ajaxHelper.removeEmployee({
            dataId: dataId,
            assignmentId: assignmentId,
            employeeId: this.employeeId,
            repeat: 0,
            additionalAssignmentIds: [],
            confirmed: true,
            notify: this.notify,
            success: () => {
                for (var i = 0; i < this.dataSource.length; i++) {
                    if (this.dataSource[i].assignmentId == assignmentId) {
                        this.dataSource.splice(i, 1);
                        if (this.dataSource.length === 0) {
                            // Wenn keien Konflikte mehr da sind können wir benachrichtigen
                            this.sendNotification();
                        } else {
                            assignmentsGrid.dataSource.read();
                        }
                        break;
                    }
                }
            }
        });
    }

    private handleEmployeeUpdateResult(result: IAjaxActionResultModel, notify: string) {
        this.employeeId = result.employeeId;
        this.notify = notify;
        this.set("dataId", result.dataId);
        this.set("location", result.location);
        this.set("employee", result.employee);
        this.set("dataSource", result.conflicts || []);
        if (result.conflicts != null && result.conflicts.length > 0) {
            employeeEditorWindowElement.data("kendoWindow").close();
        }
    }
}