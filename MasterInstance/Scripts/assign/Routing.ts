﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

declare var actionsViewModel: ActionsViewModel;
declare var filterViewModel: FilterViewModel;
//declare var filtersMultiSelect: KendoWidgets.CheckboxMultiSelect;
//declare var locationsMultiSelect: KendoWidgets.CheckboxMultiSelect;

var router = new kendo.Router();

router.route("/:mode/:date/:filters/:locations", (mode: string, date: string, filters: string, locations: string) => {
    actionsViewModel.set("mode", mode);
    filterViewModel.set("date", new Date(parseInt(date)));

    var f = <number[]>$.parseJSON(filters);
    if ($.isArray(f) && f.length > 0 && typeof (f[0]) == "number") {
        filterViewModel.set('filters', f.map(item=> { return { Value: item, Text: "" }; }));
    }
    var l = <number[]>$.parseJSON(locations);
    if ($.isArray(l) && l.length > 0 && typeof (l[0]) == "number") {
        filterViewModel.set('locations', l.map(item=> { return { Value: item, Text: "" }; }));
    }
    filterViewModel.applyFilter();
});