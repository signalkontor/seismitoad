﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.blockUI/jquery.blockUI.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

declare var campaignId: number;
declare var contractName: string;

class ContractViewModel extends kendo.data.ObservableObject {
    constructor() {
        super();

        function firstCharToLower(str: string): string {
            return str.charAt(0).toLowerCase() + str.slice(1);
        }

        $.ajax({
            url: appPath + "CM/Contract/_Data?campaignId=" + campaignId + "&name=" + contractName,
            cache: false,
            dataType: "json",
            success: (data) => {
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        this.set(firstCharToLower(property), data[property] || "");
                    }
                }
            }
        });
        super.init(this);
    }

    aktionstitel = "";
    projektleitung = "";
    projektnummer = "";
    aktionszeitraum = "";
    aktionszeiten = "";
    pausenzeiten = "";
    aktionsinhalt = "";
    beschreibungZuErbringendeLeistung = "";
    voraussetzungen = "";
    honorarMontagBisSamstag = "";
    honorarSonntagsUndFeiertags = "";
    honorarBezahlteSpringer = "";
    bonus = "";
    honorarBeiLängererAktionszeit = "";
    durchhaltebonus = "";
    teamleiterbonus = "";
    fahrerbonus = "";
    sonstigeVereinbarung = "";
    fahrtkosten = "";
    provision = "";
    schulungsvergütung = "";
    bemerkungAdvantage = "";

    beschreibungZuErbringendeLeistungVisible: boolean;
    honorarBezahlteSpringerVisible: boolean;
    bonusVisible: boolean;
    honorarBeiLängererAktionszeitVisible: boolean;
    durchhaltebonusVisible: boolean;
    teamleiterbonusVisible: boolean;
    fahrerbonusVisible: boolean;
    sonstigeVereinbarungVisible: boolean;
    fahrtkostenVisible: boolean;
    provisionVisible: boolean;
    schulungVisible: boolean;
    sonstigesVisible: boolean;

    save(reload = false) {
        (<JQueryStatic>window.top["jQuery"]).blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "CM/Contract/_Update?campaignId=" + campaignId + "&name=" + contractName,
            type: "POST",
            data: this.toJSON(),
            error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                alert("Beim Speichern ist ein Fehler aufgetreten.\n\nStatus: " + textStatus + "\n\nInternere Fehlerbeschreibung: " + errorThrown);
            },
        }).always(() => {
            if (reload) {
                window.top.location.href = appPath + "CM/Contract/Edit/" + campaignId + "?name=" + contractName;
            } else {
                (<JQueryStatic>window.top["jQuery"]).unblockUI();
            }
        });
    }

    saveAs() {
        window.top["jQuery"]("#saveAsWindow").data("kendoWindow").center().open();
    }

    honorarSonntagsUndFeiertagsVisible() {
        var text = this.get("honorarSonntagsUndFeiertags");
        return text != null && text.length > 0;
    }
}

var contractViewModel = new ContractViewModel();

$(document).ready(() => {
    kendo.bind("body", contractViewModel);
    if (window.top.document) {
        kendo.bind($(window.top.document).find(".editor"), contractViewModel);
    }
});