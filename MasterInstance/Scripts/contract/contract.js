var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ContractViewModel = (function (_super) {
    __extends(ContractViewModel, _super);
    function ContractViewModel() {
        var _this = _super.call(this) || this;
        _this.aktionstitel = "";
        _this.projektleitung = "";
        _this.projektnummer = "";
        _this.aktionszeitraum = "";
        _this.aktionszeiten = "";
        _this.pausenzeiten = "";
        _this.aktionsinhalt = "";
        _this.beschreibungZuErbringendeLeistung = "";
        _this.voraussetzungen = "";
        _this.honorarMontagBisSamstag = "";
        _this.honorarSonntagsUndFeiertags = "";
        _this.honorarBezahlteSpringer = "";
        _this.bonus = "";
        _this.honorarBeiLängererAktionszeit = "";
        _this.durchhaltebonus = "";
        _this.teamleiterbonus = "";
        _this.fahrerbonus = "";
        _this.sonstigeVereinbarung = "";
        _this.fahrtkosten = "";
        _this.provision = "";
        _this.schulungsvergütung = "";
        _this.bemerkungAdvantage = "";
        function firstCharToLower(str) {
            return str.charAt(0).toLowerCase() + str.slice(1);
        }
        $.ajax({
            url: appPath + "CM/Contract/_Data?campaignId=" + campaignId + "&name=" + contractName,
            cache: false,
            dataType: "json",
            success: function (data) {
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        _this.set(firstCharToLower(property), data[property] || "");
                    }
                }
            }
        });
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    ContractViewModel.prototype.save = function (reload) {
        if (reload === void 0) { reload = false; }
        window.top["jQuery"].blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "CM/Contract/_Update?campaignId=" + campaignId + "&name=" + contractName,
            type: "POST",
            data: this.toJSON(),
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Beim Speichern ist ein Fehler aufgetreten.\n\nStatus: " + textStatus + "\n\nInternere Fehlerbeschreibung: " + errorThrown);
            },
        }).always(function () {
            if (reload) {
                window.top.location.href = appPath + "CM/Contract/Edit/" + campaignId + "?name=" + contractName;
            }
            else {
                window.top["jQuery"].unblockUI();
            }
        });
    };
    ContractViewModel.prototype.saveAs = function () {
        window.top["jQuery"]("#saveAsWindow").data("kendoWindow").center().open();
    };
    ContractViewModel.prototype.honorarSonntagsUndFeiertagsVisible = function () {
        var text = this.get("honorarSonntagsUndFeiertags");
        return text != null && text.length > 0;
    };
    return ContractViewModel;
}(kendo.data.ObservableObject));
var contractViewModel = new ContractViewModel();
$(document).ready(function () {
    kendo.bind("body", contractViewModel);
    if (window.top.document) {
        kendo.bind($(window.top.document).find(".editor"), contractViewModel);
    }
});
//# sourceMappingURL=contract.js.map