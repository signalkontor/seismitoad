var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var KendoWidgets;
(function (KendoWidgets) {
    var CheckboxMultiSelect = (function (_super) {
        __extends(CheckboxMultiSelect, _super);
        function CheckboxMultiSelect(element, options) {
            var _this;
            options.template = options.template || "<label style='display: block'><input type='checkbox' value='#= " + options.dataValueField + " #' />#= " + options.dataTextField + " #</label>";
            options.headerTemplate = options.headerTemplate ||
                "<div><a href='#' rel='all'><span class='k-icon k-i-tick' />" + (options.selectAllText || "Alle") + "</a>" +
                    "<a href='#' rel='none'><span class='k-icon k-i-close' />" + (options.selectNoneText || "Keins") + "</a></div>";
            _this = _super.call(this, element, options) || this;
            _this._inputName = _this.element.attr("name");
            _this.element.removeAttr("name");
            _this._selectedValues = {};
            _this._updateHeaderText();
            _this.input.val(_this._headerText);
            _this["ns"] = ".kendoCheckBoxMultiSelect";
            var self = _this;
            _this["popup"].bind("close", function (event) {
                self.input.val(self._headerText);
            });
            _this.input
                .focus(function (event) { self.input.val(""); })
                .blur(function (event) {
                if (self["popup"].visible() == false) {
                    self.input.val(self._headerText);
                }
            });
            _this["header"].find("a").click(function (event) {
                event.preventDefault();
                var action = $(event.target).attr("rel");
                switch (action) {
                    case "all":
                        self._checkAll();
                        break;
                    case "none":
                        self._uncheckAll();
                        break;
                }
            });
            return _this;
        }
        CheckboxMultiSelect.prototype._updateHeaderText = function () {
            var selectedCount = Object.keys(this._selectedValues).length;
            var options = this.options;
            if (selectedCount == 0) {
                this._headerText = options.noneSelectedText || "Nichts gewählt";
            }
            else {
                var totalCount = this.dataSource.data().length;
                this._headerText = totalCount == 0
                    ? kendo.format(options.singleSelectedText || "{0} gewählt", selectedCount)
                    : kendo.format(options.selectedText || "{0} von {1} gewählt", selectedCount, totalCount);
            }
        };
        CheckboxMultiSelect.prototype._getDataItem = function (value) {
            var items = this.dataSource.view();
            for (var index = 0; index < items.length; index++) {
                if (items[index][this.options.dataValueField] == value) {
                    return items[index];
                }
            }
        };
        CheckboxMultiSelect.prototype._checkAll = function () {
            var self = this;
            this.ul.find("input[type=checkbox]").each(function (index, element) {
                var checkbox = element;
                var dataItem = self._getDataItem(checkbox.value);
                checkbox.checked = true;
                self._selectedValues[checkbox.value] = dataItem;
            });
            this._change();
        };
        CheckboxMultiSelect.prototype._uncheckAll = function () {
            var self = this;
            this.ul.find(":checked").each(function (index, element) {
                var checkbox = element;
                checkbox.checked = false;
                delete self._selectedValues[checkbox.value];
            });
            this._change();
        };
        CheckboxMultiSelect.prototype._updateHiddenInputs = function () {
            if (!this._inputName)
                return;
            var container = this.input.parent();
            container.remove("input[type=hidden]");
            for (var selectedValue in this._selectedValues) {
                if (this._selectedValues.hasOwnProperty(selectedValue)) {
                    container.append("<input type='hidden' name='" + this._inputName + "' value='" + selectedValue + "' />");
                }
            }
        };
        CheckboxMultiSelect.prototype._change = function () {
            this._updateHiddenInputs();
            this._updateHeaderText();
            this.trigger("change");
        };
        CheckboxMultiSelect.prototype._select = function (li) { };
        CheckboxMultiSelect.prototype._blur = function () { };
        CheckboxMultiSelect.prototype.open = function () {
            var self = this;
            _super.prototype.open.call(this);
            this.ul.find("input[type=checkbox]")
                .change(function (event) {
                var checkbox = event.target;
                if (checkbox.checked) {
                    var dataItem = self._getDataItem(checkbox.value);
                    self._selectedValues[checkbox.value] = dataItem;
                }
                else {
                    delete self._selectedValues[checkbox.value];
                }
                self._change();
            })
                .each(function (index, element) {
                var checkbox = element;
                checkbox.checked = checkbox.value in self._selectedValues;
            });
        };
        CheckboxMultiSelect.prototype.dataItems = function () {
            var result = [];
            for (var selectedValue in this._selectedValues) {
                if (this._selectedValues.hasOwnProperty(selectedValue)) {
                    result.push(this._selectedValues[selectedValue]);
                }
            }
            return result;
        };
        CheckboxMultiSelect.prototype.value = function (value) {
            if (value === undefined) {
                return this.dataItems();
            }
            if (value !== null && ($.isArray(value) || value instanceof kendo.data.ObservableArray)) {
                this._selectedValues = [];
                for (var i = 0; i < value.length; i++) {
                    var v = value[i].get ? value[i].get(this.options.dataValueField) : value[i];
                    this._selectedValues[v] = value[i];
                }
                var self = this;
                this.ul.find("input[type=checkbox]").each(function (index, element) {
                    var checkbox = element;
                    checkbox.checked = checkbox.value in self._selectedValues;
                });
                this._updateHeaderText();
                this.input.val(this._headerText);
            }
        };
        return CheckboxMultiSelect;
    }(kendo.ui.ComboBox));
    KendoWidgets.CheckboxMultiSelect = CheckboxMultiSelect;
    CheckboxMultiSelect.fn = CheckboxMultiSelect.prototype;
    CheckboxMultiSelect.fn.options = $.extend(true, {}, kendo.ui.ComboBox.fn.options);
    CheckboxMultiSelect.fn.options.name = "CheckBoxMultiSelect";
    kendo.ui.plugin(CheckboxMultiSelect);
})(KendoWidgets || (KendoWidgets = {}));
kendo.data.binders.widget.checkboxmultiselect = kendo.data.binders.widget.multiselect;
//# sourceMappingURL=checkboxmultiselect.js.map