﻿function selectNewestVersion() {
    var dropdown = <kendo.ui.DropDownList>$("#dropdown").data("kendoDropDownList");
    dropdown.select(dropdown.dataSource.data().length - 1);
    dropdown.trigger("change");
} 

function versionChanged() {
    var url = $("#detailsUrl").val();
    $("#staffrequirement").load(url, { version: this.value() });
}

$(document).ready(() => {
    window.setTimeout(() => {
        selectNewestVersion();
    }, 50);
});