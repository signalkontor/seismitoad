﻿/** 
* Get the ISO week date week number 
*/
declare var appPath: string;

Date.prototype["getWeek"] = function () {
    // Create a copy of this date object  
    var target = new Date(this.valueOf());

    // ISO week date weeks start on monday  
    // so correct the day number  
    var dayNr = (this.getDay() + 6) % 7;

    // ISO 8601 states that week 1 is the week  
    // with the first thursday of that year.  
    // Set the target date to the thursday in the target week  
    target.setDate(target.getDate() - dayNr + 3);

    // Store the millisecond value of the target date  
    var firstThursday = target.valueOf();

    // Set the target to the first thursday of the year  
    // First set the target to january first  
    target.setMonth(0, 1);
    // Not a thursday? Correct the date to the next thursday  
    if (target.getDay() != 4) {
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
    }

    // The weeknumber is the number of weeks between the   
    // first thursday of the year and the thursday in the target week  
    return 1 + Math.ceil((firstThursday - target.valueOf()) / 604800000); // 604800000 = 7 * 24 * 3600 * 1000  
}

// ReSharper disable once InconsistentNaming
function skBlockUI() {
    if ($.blockUI) {
        $.blockUI({ message: '<div style="padding:15px;"><img src="' + appPath + 'Content/images/ajax-loader.gif" alt="loading..."/><br/><strong>Bitte warten...</strong></div>', baseZ: 20000 });
    }
}

// ReSharper disable once InconsistentNaming
function _onResize() {
    $("iframe").height($("#hSplitter").height() - 100);
}
$(window).bind('resize', _onResize);

if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [],
            k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys;
    };
}

function logMessage(msg: string) {
    // send error message
    $.ajax({
        type: 'POST',
        url: '/Log',
        data: { message: msg }
    });
}

$(document).ajaxError(function (event, xhr, ajaxSetting, thrownError) {
    // prevent recursion, if logging fails
    if (ajaxSetting.url.indexOf("/Log") === -1 && xhr.getAllResponseHeaders()) {
        logMessage(ajaxSetting.type + " " + ajaxSetting.url + "[" + ajaxSetting.data + "]: " + xhr.statusText);
        alert("Es ist ein Fehler bei der Kommunikation mit dem Server aufgetreten.");
    }
});

function saveMenuState() {
    window.setTimeout(() => {
        var expanded = $("#Menu > li.save-state").map((index: number, domElement: Element) => {
            var el = $(domElement);
            return el.hasClass("t-state-active") || el.hasClass("k-state-active");
        }).get();
        try {
            localStorage.setItem("menuState", JSON.stringify(expanded));
        } catch (ignored) {
        }
    }, 100);
}

function applyMenuState() {
    try {
        var menuElement = $("#Menu"),
            menuWidget = menuElement.data("kendoPanelBar") || menuElement.data("tPanelBar"),
            menuState = <boolean[]>JSON.parse(localStorage.getItem("menuState")),
            item: JQuery;

        for (var i = 0; i < menuState.length; i++) {
            item = $("#Menu > li.save-state:eq(" + i + ")");
            if (menuState[i]) {
                menuWidget.expand(item);
            } else {
                menuWidget.collapse(item);
            }
        }
    } catch (ignored) {
    }
}

$(document).ready(() => {
    window.setTimeout(applyMenuState, 200);
})