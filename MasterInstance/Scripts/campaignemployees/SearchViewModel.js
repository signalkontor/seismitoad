var searchViewModel = new SearchViewModel("#s", "#search-window");
var svm = searchViewModel;
$(document).ready(function () {
    kendo.bind("#search-window", searchViewModel);
    searchViewModel.bind("change", function (e) {
        if (e.field !== "preselectButtonEnabled") {
            searchViewModel.set("preselectButtonEnabled", false);
        }
        if (e.field !== "search" && e.field !== "preselectButtonEnabled" && e.field !== "searchResultLength") {
            $("#s").data("kendoGrid").dataSource.data([]);
        }
    });
    viewModel.set("searchButtonVisible", true);
    function firstCharToLower(str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    }
    $.ajax({
        dataType: "json",
        url: appPath + "CampaignAssets/" + campaignId + "/AcquisitionSearch.txt",
        cache: false,
        global: false,
        success: function (data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    searchViewModel.set(firstCharToLower(key), data[key]);
                }
            }
            searchViewModel.search();
        }
    });
});
//# sourceMappingURL=SearchViewModel.js.map