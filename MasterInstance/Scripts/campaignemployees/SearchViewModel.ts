﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

declare var viewModel: CampaignEmployeesViewModel;

var searchViewModel = new SearchViewModel("#s", "#search-window");
var svm = searchViewModel;
$(document).ready(() => {
    kendo.bind("#search-window", searchViewModel);
    searchViewModel.bind("change", (e) => {
        if (e.field !== "preselectButtonEnabled") {
            searchViewModel.set("preselectButtonEnabled", false);
        }
        if (e.field !== "search" && e.field !== "preselectButtonEnabled" && e.field !== "searchResultLength") {
            $("#s").data("kendoGrid").dataSource.data([]);
        }
    });
    viewModel.set("searchButtonVisible", true);

    function firstCharToLower(str: string): string {
        return str.charAt(0).toLowerCase() + str.slice(1);
    }

    $.ajax({
        dataType: "json",
        url: appPath + "CampaignAssets/" + campaignId + "/AcquisitionSearch.txt",
        cache: false,
        global: false, // daduch wird der globale ajaxError-Handler nicht getriggert, wenn die o.g. Datei nicht existiert
        success: data => {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    searchViewModel.set(firstCharToLower(key), data[key]);
                }
            }
            searchViewModel.search();
        }
    });
});
