﻿var states = [
    { value: "Any", text: "Jeder Status (Gesamtliste)" },
    { value: "New", text: "In Vorauswahl" },
    { value: "Invited", text: "Job-Hinweis erhalten" },
    { value: "NotInterested", text: "Kein Interesse" },
    { value: "InformationRequested", text: "Job-Info angefordert" },
    { value: "InformationSent", text: "Job-Info erhalten" },
    { value: "InformationRejected", text: "Job-Info-Anforderung abgelehnt" },
    { value: "Applied", text: "Beworben" },
    { value: "Rejected", text: "Abgelehnt" },
    { value: "Deferred", text: "Zurückgestellt" },
    { value: "Accepted", text: "Im Team" }
];