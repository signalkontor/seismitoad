﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

function toArray(object: { [index: string]: boolean }): string[] {
    var result = [];
    for (var prop in object) {
        if (object.hasOwnProperty(prop) && object[prop] === true) {
            result.push(prop);
        }
    }
    return result;
}

declare var states: any[];
declare var campaignId: number;
declare var acquisitionList: boolean;

class CampaignEmployeesViewModel extends kendo.data.ObservableObject {
    private localStorageVersion = "8";
    private selected: { [index: string]: boolean } = {};
    private campaignId: number;
    private acquisitionList: boolean;
    private localStorageKey: string;
    private firstGridUpdate = true;
    private scrollTop: number;
    private scrollLeft: number;
    state = "Any";
    action: string;
    amount: number;
    actionButtonEnabled = false;
    searchButtonVisible = false;
    result: string = "";
    states = states;
    contract: string;
    nameOrMail: string;

    employeeId: number;
    trainingDay: any;
    trainingDays: kendo.data.DataSource;
    trainingTitle = "Keine Schulung gewählt";
    trainingDate = "Keine Schulung gewählt";
    trainingLocation = "Keine Schulung gewählt";
    trainingRemarks = "Keine Schulung gewählt";
    trainingMaxParticipants = "Keine Schulung gewählt";
    trainingParticipantsCount = "Keine Schulung gewählt";

    appliedForLocationsText: string;
    appliedForLocationListVisible: boolean = false;

    trainingChanged() {
        var trainingDay = this.get("trainingDay");
        if (typeof (trainingDay) === "undefined" || trainingDay === null) {
            this.set("trainingTitle", "Keine Schulung gewählt");
            this.set("trainingRemarks", "Keine Schulung gewählt");
            this.set("trainingLocation", "Keine Schulung gewählt");
            this.set("trainingMaxParticipants", "Keine Schulung gewählt");
            this.set("trainingParticipantsCount", "Keine Schulung gewählt");
            this.set("trainingDate", "Keine Schulung gewählt");
        } else {
            this.set("trainingTitle", trainingDay.Title);
            this.set("trainingRemarks", trainingDay.Remarks);
            this.set("trainingLocation", trainingDay.Location);
            this.set("trainingMaxParticipants", trainingDay.MaxParticipants);
            this.set("trainingParticipantsCount", trainingDay.ParticipantsCount);
            this.set("trainingDate", kendo.format("{0:dd.MM.yyyy HH:mm} bis {1:dd.MM.yyyy HH:mm}",
                kendo.parseDate(trainingDay.DateFrom),
                kendo.parseDate(trainingDay.DateTil)));
        }
    }

    appliedLocations = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "CM/CampaignEmployees/_AppliedLocations",
                data: () => {
                    return {
                        campaignId: this.campaignId,
                        employeeId: this.employeeId
                    };
                }
            }
        },
        sort: [{ field: "PostalCode", dir: "asc" }]
    });

    contracts = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "CM/Contract/_DropdDownList?campaignId=" + campaignId
            }
        }
    });

    showLocations(element: HTMLElement) {
        var row = $(element).closest("tr"),
            grid = <kendo.ui.Grid>$(this.grid).data("kendoGrid"),
            item = grid.dataItem(row),
            appliedBySelection = item.get("AppliedForLocationsBySelection"),
            locationsText = item.get("AppliedForLocations"),
            index: number;

        this.employeeId = item.get("EmployeeId");
        if (appliedBySelection) {
            this.appliedLocations.read();
            this.set("appliedForLocationListVisible", true);
            // Auch wenn der Promoter direkt Locations ausgewählt hat, kann es sein, dass
            // er was im Freitextfeld eingegeben hat. In diesem Fall enhält locationsText
            // zuerst die Auswahl in der Form "Stadt A, Stadt B, ...". Nach dem letzten
            // Stadtnamen folgt ein Punkt und ein Leerzeichen. Dies nehmen wir als Trenner
            // um die Freitexteingabe aus dem Gesamtstring zu extrahieren. Es gibt allerdings
            // ein paar Locations die in Städten liegen die einen Punkt enthalten.
            // Zum Beispiel: "St. Augustin". In diesem Fall klappt das natürlich nicht richtig,
            // Aber wir berücksichtigen das hier nicht, da es viel zu Aufwändig ist.
            index = locationsText.indexOf(". ");
            if (index > -1) {
                locationsText = locationsText.substring(index + 2);
            } else {
                locationsText = "";
            }
        } else {
            this.set("appliedForLocationListVisible", false);
        }
        this.set("appliedForLocationsText", locationsText);
        $(this.appliedLocationsWindow).data("kendoWindow").center().open();
    }

    showAllTrainings: boolean = false;

    constructor(private grid: string, private progressWindow: string, private trainingWindow: string, private appliedLocationsWindow: string) {
        super();
        this.campaignId = campaignId;
        this.acquisitionList = acquisitionList;

        this.trainingDays = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/CampaignEmployees/_Trainings",
                    data: () => {
                        return {
                            campaignId: this.showAllTrainings ? null : campaignId,
                            employeeId: this.employeeId
                        };
                    },
                    type: "POST"
                }
            }
        });

        this.localStorageKey = (acquisitionList ? "acquisition" + campaignId : "promoter") + "-grid-options-" + campaignId;
        super.init(this);
        this.bind("change", e => {
            if (e.field === "showAllTrainings") {
                this.trainingDays.read();
            }
        });
    }

    openSearch() {
        $("#search-window").data("kendoWindow").center().open();
    }

    actionButton1Text() {
        switch (this.get("state")) {
            case "New":
                return "Gewählten Promotern Job-Hinweis senden";
            case "Invited":
            case "NotInterested":
                return "Gewählten Promotern Job-Hinweis erneut senden";
            case "InformationRequested":
                return "Gewählten Promotern Job-Info senden";
            case "InformationSent":
                return "Gewählten Promotern Job-Info erneut senden";
            case "InformationRejected":
                return "Gewählten Promotern Job-Info doch senden";
            case "Applied":
            case "Deferred":
                return "Gewählte Promoter akzeptieren";
            case "Accepted":
                return acquisitionList
                    ? null
                    : "Gewählten Promotern Projektauftrag senden";
            case "ContractSent":
                return "Gewählten Promotern Projektauftrag erneut senden";
            case "ContractCancelled":
                return null;
            case "ContractAccepted":
                return "Projektauftrag stornieren";
        }
        return null;
    }

    actionButton2Text() {
        switch (this.get("state")) {
            case "InformationRequested":
            case "Applied":
            case "Deferred":
                return "Gewählte Promoter ablehnen";
        }
        return null;
    }

    actionButton3Text() {
        switch (this.get("state")) {
            case "Applied":
                return "Gewählte Promoter zurückstellen";
        }
        return null;
    }

    contractSelectVisible() {
        if (acquisitionList)
            return false;

        switch (this.get("state")) {
            case "Accepted":
            case "ContractSent":
                return true;
            default:
                return false;
        }
    }

    actionButton1Visible() {
        return this.actionButton1Text() !== null;
    }

    actionButton1Enabled() {
        return this.contractSelectVisible() === false || this.get("contract") !== null;
    }

    actionButton2Visible() {
        return this.actionButton2Text() !== null;
    }

    actionButton3Visible() {
        return this.actionButton3Text() !== null;
    }

    actionButton1Click() {
        this.handleClick(1);
    }

    actionButton2Click() {
        this.handleClick(2);
    }

    actionButton3Click() {
        this.handleClick(3);
    }

    progressVisible() {
        var result = this.get("result");
        return result === null || result === "";
    }

    closeWindow() {
        var progressWindow = <kendo.ui.Window>$(this.progressWindow).data("kendoWindow");
        progressWindow.close();
    }

    closeTrainingWindow() {
        var trainingWindow = <kendo.ui.Window>$(this.trainingWindow).data("kendoWindow");
        trainingWindow.close();
    }

    assignTrainingButtonEnabled() {
        var trainingDay = this.get("trainingDay");
        return typeof (trainingDay) !== "undefined" && trainingDay !== null;
    }

    resetColumns() {
        localStorage.removeItem(this.localStorageKey);
        window.location.reload();
    }

    copyEmailAddresses() {
        var data = $(this.grid).data("kendoGrid").dataSource.data(),
            emails = data
                .filter((element: any) => element.Email != null && element.Email.length > 0)
                .sort((element: any) => element.EmployeeState === 100 ? 1 : element.EmployeeState === 200 ? 2 : 0)
                .map((element: any) => kendo.format(
                    "{0}{1} {2} <{3}>",
                    element.EmployeeState === 100 ? "INAKTIV! " : element.EmployeeState === 200 ? "GESPERRT! " : "",
                    element.Firstname,
                    element.Lastname,
                    element.Email)),
            textarea = $("#email-addresses");

        textarea.val("Lade E-Mail Adressen...");
        $("#addresses-window").data("kendoWindow").center().open();
        window.setTimeout(() => {
            textarea.val(emails.join("\n")).select();
        }, 500);
    }

    assignTraining() {
        $.post(appPath + "CM/CampaignEmployees/_AddToTraining", {
                campaignId: this.campaignId,
                employeeId: this.employeeId,
                trainingDayId: this.get("trainingDay").Id
            })
            .fail(() => {
                alert("Es ist ein Fehler aufgetreten. Die Aktion wurde wahrscheinlich nicht durchgeführt.");
            })
            .done((e) => {
                if (e === "reload") {
                    window.setTimeout(() => {
                        window.location.reload(true);
                    }, 10);
                } else {
                    this.applyFilter(null);
                }
            })
            .always(() => {
                $(this.trainingWindow).data("kendoWindow").close();
            });
    }

    private handleClick(button: number) {
        var action = this.get("actionButton" + button + "Text"),
            progressWindow = <kendo.ui.Window>$(this.progressWindow).data("kendoWindow"),
            contract = this.get("contract"),
            parameters = {
                campaignId: this.campaignId,
                employeeIds: toArray(this.selected),
                contract: contract
            };

        if (this.contractSelectVisible() && (typeof (contract) === "undefined" || contract === null)) {
            alert("Bitte einen Projektauftrag wählen.");
            return;
        }

        this.set("result", "");
        this.set("action", action);
        this.set("amount", parameters.employeeIds.length);
        progressWindow.setOptions({ actions: [] });
        progressWindow.center().open();

        $.post(appPath + "CM/CampaignEmployees/_" + this.get("state") + "Button" + button, parameters)
            .fail(() => {
                this.applyFilter(null);
                this.set("result", "Es ist ein Fehler aufgetreten. Die Aktion wurde wahrscheinlich nicht durchgeführt.");
            })
            .done((data: string) => {
                this.applyFilter(null);
                this.set("result", data);
            })
            .always(() => {
                progressWindow.setOptions({ actions: ["Close"] });
            });
    }

    applyFilter(e: JQueryEventObject) {
        var grid = <kendo.ui.Grid>$(this.grid).data("kendoGrid"),
            state = <string>this.get("state");
        this.selected = {};
        window["cwl"]("filter set: " + state, "k-info-colored");
        $("#checkAll").prop("checked", false);
        this.set("actionButtonEnabled", false);
        window["cwl"]("grid clear request");
        grid.dataSource.data([]);
        if (state === "Any") {
            grid.hideColumn("EmployeeId");
        } else {
            if (state === "Accepted" && this.acquisitionList) {
                grid.hideColumn("EmployeeId");
            } else {
                grid.showColumn("EmployeeId");
            }
        }
        grid.dataSource.read();
    }

    updateSelected(checkbox: HTMLInputElement) {
        var employeeId = checkbox.value,
            state = checkbox.checked;
        this.selected[employeeId] = state;
        this.set("actionButtonEnabled", false);
        for (var property in this.selected) {
            if (this.selected[property] === true) {
                this.set("actionButtonEnabled", true);
                return;
            }
        }
    }

    nameOrEmailSearchButtonClick() {
        this.applyFilter(null);
    }

    gridParams() {
        return { "state": this.get("state"), "nameOrMail": this.get("nameOrMail") };
    }

    gridDataBound(e: kendo.ui.GridDataBoundEvent) {
        var grid = e.sender,
            contentElement = grid.element.find(".k-virtual-scrollable-wrap"),
            currentRecords = grid.dataSource.view();

        contentElement.scrollLeft(this.scrollLeft);
        contentElement.scrollTop(this.scrollTop);

        $("#rowCount").text(kendo.format("{0:#,###}", grid.dataSource.total()));

        for (var i = 0; i < currentRecords.length; i++) {
            var currentRecord = currentRecords[i],
                uid = currentRecord.uid,
                employeeId = currentRecord.EmployeeId;

            currentRecord.RolesEdit = currentRecord.Roles !== null
                ? currentRecord.Roles.split(", ")
                : [];

            if (currentRecord.State !== currentRecord.RealState) {
                grid.tbody.find("tr[data-uid='" + uid + "']").addClass("worker-list");
            }

            switch(currentRecord.EmployeeState) {
                case 100:
                    grid.tbody.find("tr[data-uid='" + uid + "']").addClass("inactive-promoter");
                    grid.lockedTable.find("tr[data-uid='" + uid + "']").addClass("inactive-promoter");
                    break;
                case 200:
                    grid.tbody.find("tr[data-uid='" + uid + "']").addClass("blocked-promoter");
                    grid.lockedTable.find("tr[data-uid='" + uid + "']").addClass("blocked-promoter");
                    break;
            }

            var checkbox = grid.tbody.find("tr[data-uid='" + uid + "'] input[type=checkbox]");
            if (!checkbox) {
                continue;
            }

            if (this.selected.hasOwnProperty(employeeId)) {
                var checked = this.selected[employeeId];
                if (checked) {
                    checkbox.prop("checked", true);
                }
            }
        }
    }

    gridDataBinding(e: kendo.ui.GridDataBindingEvent) {
        var optionsJson: string,
            currentOptions: any,
            options: any,
            index: number,
            columnFound: boolean,
            gridContentElement = $(this.grid).find(".k-virtual-scrollable-wrap");

        this.scrollLeft = gridContentElement.scrollLeft();
        this.scrollTop = gridContentElement.scrollTop();

        if (this.firstGridUpdate) {
            this.firstGridUpdate = false;
            if (localStorage.getItem(this.localStorageKey + "_version") != this.localStorageVersion) {
                return;
            }
            optionsJson = localStorage.getItem(this.localStorageKey);
            if (optionsJson) {
                try {
                    options = JSON.parse(optionsJson);
                } catch (ex) {
                    return;
                }
                currentOptions = e.sender.getOptions();
                for (index = 0; index < options.columns.length; index++) {
                    if (options.columns[index].field === "EmployeeId") {
                        options.columns[index].hidden = true;
                        break;
                    }
                }
                for (index = 0; index < currentOptions.columns.length; index++) {
                    var column = currentOptions.columns[index];
                    if (typeof(column.field) !== "undefined" && column.field != null && column.field.indexOf("TrainingDay") > -1) {
                        columnFound = false;
                        for (var i = 0; i < options.columns.length; i++) {
                            if (options.columns[i].field === column.field) {
                                columnFound = true;
                                break;
                            }
                        }
                        if (columnFound) {
                            continue;
                        }
                        options.columns.push(column);
                    }
                }
                e.sender.setOptions(options);
            }
        }
        // Die Spalte mit der Checkbox muss schön gemacht werden. Wir wollen kein Column-Menu.
        // Das erreichen wir durch entfernen der Klasse k-with-icon und durch entfernen des
        // Elements mit der Klasse k-header-column-menu. Da einige Browser der Meinung sind,
        // dass die Spalte nicht breit genug für die Checkbox (im Header) ist zeigen sie eine
        // ellipsis an. Durch "text-overflow: clip" treiben wir ihnen das aus..
        $("#a [data-field=EmployeeId]")
            .removeClass("k-with-icon")
            .css("text-overflow", "clip")
            .find(".k-header-column-menu")
            .remove();
    }

    saveColumnState(e) {
        var options = e.sender.getOptions();
        localStorage.setItem(this.localStorageKey, kendo.stringify(options));
        localStorage.setItem(this.localStorageKey + "_version", this.localStorageVersion);
    }

    saveColumnStateAfterReoderEvent(e) {
        // Da das Reoder-Event schon feuert, bevor die Grid-Options geändert sind
        // müssen wir den Abruf der Options verzögern.
        window.setTimeout(() => {
            var options = { columns: e.sender.getOptions().columns };
            localStorage.setItem(this.localStorageKey, kendo.stringify(options));
            localStorage.setItem(this.localStorageKey + "_version", this.localStorageVersion);
        }, 0);
    }
}

var viewModel = new CampaignEmployeesViewModel("#a", "#progress-window", "#training-window", "#applied-locations-window");
var currentlyEditing = false;

$(document).ready(() => {
    kendo.bind("#data-bound", viewModel);
    window.setTimeout(() => {
        var gridElement = $("#a"),
            detailsWindow = $("#details-window").data("kendoWindow");
            gridResizer = new GridResizer($("#hSplitter"), gridElement, height => { }, 110);

        gridResizer.resize();

        $("#checkAll").on("change", event => {
            var checkbox = <HTMLInputElement>event.target;
            $(".k-grid-content-locked :checkbox").prop("checked", checkbox.checked).change();
        });

        gridElement.on("click", ".details-link, .attention", event => {
            var dataItem = (<kendo.ui.Grid>gridElement.data("kendoGrid")).dataItem($(event.currentTarget).closest("tr")),
                url = appPath + "Employee/Card/" + dataItem.get("EmployeeId") +
                    "?campaignId=" + campaignId +
                    "&view=" + (acquisitionList ? "Acquisition" : "Promoter") +
                    "&_" + new Date().getTime();

            event.preventDefault();
            event.stopPropagation();
            detailsWindow.element.load(url, null, () => {
                detailsWindow.center().open();
            });
        });

        // The Event-Handlers needs to be a function and not a lamdba expression.
        // This is because TypeScript will change this to _this in lambdas
        gridElement.on("click", ".k-grid-content tr, .k-grid-content-locked tr", function (event) {
            // Wenn wir im Bearbeiten-Modus sind, deaktivieren wir die Funktion, dass beim klicken der Zeile die Checkbox angehakt wird.
            if (currentlyEditing) {
                // In der Regel bedeutet ein Klick im Beabeiten-Modus, dass die Bearbeitung beendet wird. Die einzige Ausnahme ist,
                // dass der User einfach im Input-Feld rumklickt. In dem Fall lassen wir currentlyEditing auf true
                if ((<HTMLElement>event.target).tagName !== "INPUT") {
                    currentlyEditing = false;
                }
                return;
            }

            // Im Status "Any" gibt es keine (sichtbaren) Checkboxen, daher in diesem Fall nichts machen.
            if (viewModel.get("state") === "Any") {
                return;
            }
            if ((<HTMLElement>event.target).tagName !== "INPUT" &&
                (<HTMLInputElement>event.target).type !== 'checkbox') {
                var uid = $(event.currentTarget).closest("tr").attr("data-uid");
                $("#a .k-grid-content-locked tr[data-uid=" + uid + "] :checkbox").click();
            }
        });
        gridElement.on("click", "a.k-grid-Training", event => {
            var dataItem = (<kendo.ui.Grid>gridElement.data("kendoGrid")).dataItem($(event.currentTarget).closest("tr"));
            event.preventDefault();
            viewModel.employeeId = dataItem.get("EmployeeId");
            viewModel.trainingDays.read();
            $("#training-window").data("kendoWindow").center().open();
        });
        gridElement.on("change", ".k-grid-content-locked input[type=checkbox]", event => {
            viewModel.updateSelected(<HTMLInputElement>event.target);
        });
    }, 0);
});

function onEdit(e) {
    // Das Click-Event, dass für dieses Edit-Event verantwortlich ist, hat bereits die Checkbox getoggled
    // da da nicht erwünscht ist, "klicken" wir hier einfach noch mal, damit sich für den User nichts
    // ändert.
    e.container.closest("tr").find(":checkbox").click();
    // Wir merken uns, dass wir im Edit-Modus sind, damit wir den nächsten Klick bezüglich der checkboxen
    // ignorieren können
    currentlyEditing = true;

    if (e.container[0].innerHTML.indexOf("Remark") > -1) {
        // Bemerkung darf immer editiert werden
        return;
    }

    if (e.container[0].innerHTML.indexOf("RolesEdit") > -1) {
        // Rollen dürfen immer editiert werden
        // Wir speichern das ViewModel der Zeile in eine globale Variable, da wir diese brauchen um ein
        // Problem von Kendo zu umgehen.
        window["currentRowViewModel"] = e.model;
        return;
    }

    if (e.model.RealState !== e.model.State) {
        this.closeCell();
    }
}

function onSave(e) {
    var grid = $("#a").data("kendoGrid"),
        state = <string>viewModel.get("state");

    currentlyEditing = false;
    if (e.values.hasOwnProperty("Remark") || e.values.hasOwnProperty("RolesEdit") || e.values.hasOwnProperty("Deployable")) {
        window.setTimeout(() => {
            grid.dataSource.sync();
        }, 50);
        return;
    }

    confirmViewModel
        .ask("Der Promoter wird ohne Mailbenachrichtigung in den gewählten Status gesetzt.\n\nStatus des Promoters ändern?")
        .onNo(() => {
            grid.cancelChanges();
        })
        .onYes(() => {
            if (state !== "Any") {
                grid.dataSource.one("sync", () => {
                    viewModel.applyFilter(null);
                });
            }
            grid.dataSource.sync();
        });
}

function keyupCampaignEmployee(e: KeyboardEvent) {
    if (e.keyCode === 13) {
        viewModel.nameOrEmailSearchButtonClick();
    }
    return false;
}