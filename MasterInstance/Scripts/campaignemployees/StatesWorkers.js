﻿var states = [
    { value: "Any", text: "Jeder Status (Gesamtliste)" },
    { value: "Accepted", text: "Im Team" },
    { value: "ContractSent", text: "Projektauftrag versendet" },
    { value: "ContractCancelled", text: "Projektauftrag storniert" },
    { value: "ContractRejected", text: "Projektauftrag abgelehnt" },
    { value: "ContractAccepted", text: "Projektauftrag bestätigt" }
];