var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function toArray(object) {
    var result = [];
    for (var prop in object) {
        if (object.hasOwnProperty(prop) && object[prop] === true) {
            result.push(prop);
        }
    }
    return result;
}
var CampaignEmployeesViewModel = (function (_super) {
    __extends(CampaignEmployeesViewModel, _super);
    function CampaignEmployeesViewModel(grid, progressWindow, trainingWindow, appliedLocationsWindow) {
        var _this = _super.call(this) || this;
        _this.grid = grid;
        _this.progressWindow = progressWindow;
        _this.trainingWindow = trainingWindow;
        _this.appliedLocationsWindow = appliedLocationsWindow;
        _this.localStorageVersion = "8";
        _this.selected = {};
        _this.firstGridUpdate = true;
        _this.state = "Any";
        _this.actionButtonEnabled = false;
        _this.searchButtonVisible = false;
        _this.result = "";
        _this.states = states;
        _this.trainingTitle = "Keine Schulung gewählt";
        _this.trainingDate = "Keine Schulung gewählt";
        _this.trainingLocation = "Keine Schulung gewählt";
        _this.trainingRemarks = "Keine Schulung gewählt";
        _this.trainingMaxParticipants = "Keine Schulung gewählt";
        _this.trainingParticipantsCount = "Keine Schulung gewählt";
        _this.appliedForLocationListVisible = false;
        _this.appliedLocations = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/CampaignEmployees/_AppliedLocations",
                    data: function () {
                        return {
                            campaignId: _this.campaignId,
                            employeeId: _this.employeeId
                        };
                    }
                }
            },
            sort: [{ field: "PostalCode", dir: "asc" }]
        });
        _this.contracts = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/Contract/_DropdDownList?campaignId=" + campaignId
                }
            }
        });
        _this.showAllTrainings = false;
        _this.campaignId = campaignId;
        _this.acquisitionList = acquisitionList;
        _this.trainingDays = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/CampaignEmployees/_Trainings",
                    data: function () {
                        return {
                            campaignId: _this.showAllTrainings ? null : campaignId,
                            employeeId: _this.employeeId
                        };
                    },
                    type: "POST"
                }
            }
        });
        _this.localStorageKey = (acquisitionList ? "acquisition" + campaignId : "promoter") + "-grid-options-" + campaignId;
        _super.prototype.init.call(_this, _this);
        _this.bind("change", function (e) {
            if (e.field === "showAllTrainings") {
                _this.trainingDays.read();
            }
        });
        return _this;
    }
    CampaignEmployeesViewModel.prototype.trainingChanged = function () {
        var trainingDay = this.get("trainingDay");
        if (typeof (trainingDay) === "undefined" || trainingDay === null) {
            this.set("trainingTitle", "Keine Schulung gewählt");
            this.set("trainingRemarks", "Keine Schulung gewählt");
            this.set("trainingLocation", "Keine Schulung gewählt");
            this.set("trainingMaxParticipants", "Keine Schulung gewählt");
            this.set("trainingParticipantsCount", "Keine Schulung gewählt");
            this.set("trainingDate", "Keine Schulung gewählt");
        }
        else {
            this.set("trainingTitle", trainingDay.Title);
            this.set("trainingRemarks", trainingDay.Remarks);
            this.set("trainingLocation", trainingDay.Location);
            this.set("trainingMaxParticipants", trainingDay.MaxParticipants);
            this.set("trainingParticipantsCount", trainingDay.ParticipantsCount);
            this.set("trainingDate", kendo.format("{0:dd.MM.yyyy HH:mm} bis {1:dd.MM.yyyy HH:mm}", kendo.parseDate(trainingDay.DateFrom), kendo.parseDate(trainingDay.DateTil)));
        }
    };
    CampaignEmployeesViewModel.prototype.showLocations = function (element) {
        var row = $(element).closest("tr"), grid = $(this.grid).data("kendoGrid"), item = grid.dataItem(row), appliedBySelection = item.get("AppliedForLocationsBySelection"), locationsText = item.get("AppliedForLocations"), index;
        this.employeeId = item.get("EmployeeId");
        if (appliedBySelection) {
            this.appliedLocations.read();
            this.set("appliedForLocationListVisible", true);
            index = locationsText.indexOf(". ");
            if (index > -1) {
                locationsText = locationsText.substring(index + 2);
            }
            else {
                locationsText = "";
            }
        }
        else {
            this.set("appliedForLocationListVisible", false);
        }
        this.set("appliedForLocationsText", locationsText);
        $(this.appliedLocationsWindow).data("kendoWindow").center().open();
    };
    CampaignEmployeesViewModel.prototype.openSearch = function () {
        $("#search-window").data("kendoWindow").center().open();
    };
    CampaignEmployeesViewModel.prototype.actionButton1Text = function () {
        switch (this.get("state")) {
            case "New":
                return "Gewählten Promotern Job-Hinweis senden";
            case "Invited":
            case "NotInterested":
                return "Gewählten Promotern Job-Hinweis erneut senden";
            case "InformationRequested":
                return "Gewählten Promotern Job-Info senden";
            case "InformationSent":
                return "Gewählten Promotern Job-Info erneut senden";
            case "InformationRejected":
                return "Gewählten Promotern Job-Info doch senden";
            case "Applied":
            case "Deferred":
                return "Gewählte Promoter akzeptieren";
            case "Accepted":
                return acquisitionList
                    ? null
                    : "Gewählten Promotern Projektauftrag senden";
            case "ContractSent":
                return "Gewählten Promotern Projektauftrag erneut senden";
            case "ContractCancelled":
                return null;
            case "ContractAccepted":
                return "Projektauftrag stornieren";
        }
        return null;
    };
    CampaignEmployeesViewModel.prototype.actionButton2Text = function () {
        switch (this.get("state")) {
            case "InformationRequested":
            case "Applied":
            case "Deferred":
                return "Gewählte Promoter ablehnen";
        }
        return null;
    };
    CampaignEmployeesViewModel.prototype.actionButton3Text = function () {
        switch (this.get("state")) {
            case "Applied":
                return "Gewählte Promoter zurückstellen";
        }
        return null;
    };
    CampaignEmployeesViewModel.prototype.contractSelectVisible = function () {
        if (acquisitionList)
            return false;
        switch (this.get("state")) {
            case "Accepted":
            case "ContractSent":
                return true;
            default:
                return false;
        }
    };
    CampaignEmployeesViewModel.prototype.actionButton1Visible = function () {
        return this.actionButton1Text() !== null;
    };
    CampaignEmployeesViewModel.prototype.actionButton1Enabled = function () {
        return this.contractSelectVisible() === false || this.get("contract") !== null;
    };
    CampaignEmployeesViewModel.prototype.actionButton2Visible = function () {
        return this.actionButton2Text() !== null;
    };
    CampaignEmployeesViewModel.prototype.actionButton3Visible = function () {
        return this.actionButton3Text() !== null;
    };
    CampaignEmployeesViewModel.prototype.actionButton1Click = function () {
        this.handleClick(1);
    };
    CampaignEmployeesViewModel.prototype.actionButton2Click = function () {
        this.handleClick(2);
    };
    CampaignEmployeesViewModel.prototype.actionButton3Click = function () {
        this.handleClick(3);
    };
    CampaignEmployeesViewModel.prototype.progressVisible = function () {
        var result = this.get("result");
        return result === null || result === "";
    };
    CampaignEmployeesViewModel.prototype.closeWindow = function () {
        var progressWindow = $(this.progressWindow).data("kendoWindow");
        progressWindow.close();
    };
    CampaignEmployeesViewModel.prototype.closeTrainingWindow = function () {
        var trainingWindow = $(this.trainingWindow).data("kendoWindow");
        trainingWindow.close();
    };
    CampaignEmployeesViewModel.prototype.assignTrainingButtonEnabled = function () {
        var trainingDay = this.get("trainingDay");
        return typeof (trainingDay) !== "undefined" && trainingDay !== null;
    };
    CampaignEmployeesViewModel.prototype.resetColumns = function () {
        localStorage.removeItem(this.localStorageKey);
        window.location.reload();
    };
    CampaignEmployeesViewModel.prototype.copyEmailAddresses = function () {
        var data = $(this.grid).data("kendoGrid").dataSource.data(), emails = data
            .filter(function (element) { return element.Email != null && element.Email.length > 0; })
            .sort(function (element) { return element.EmployeeState === 100 ? 1 : element.EmployeeState === 200 ? 2 : 0; })
            .map(function (element) { return kendo.format("{0}{1} {2} <{3}>", element.EmployeeState === 100 ? "INAKTIV! " : element.EmployeeState === 200 ? "GESPERRT! " : "", element.Firstname, element.Lastname, element.Email); }), textarea = $("#email-addresses");
        textarea.val("Lade E-Mail Adressen...");
        $("#addresses-window").data("kendoWindow").center().open();
        window.setTimeout(function () {
            textarea.val(emails.join("\n")).select();
        }, 500);
    };
    CampaignEmployeesViewModel.prototype.assignTraining = function () {
        var _this = this;
        $.post(appPath + "CM/CampaignEmployees/_AddToTraining", {
            campaignId: this.campaignId,
            employeeId: this.employeeId,
            trainingDayId: this.get("trainingDay").Id
        })
            .fail(function () {
            alert("Es ist ein Fehler aufgetreten. Die Aktion wurde wahrscheinlich nicht durchgeführt.");
        })
            .done(function (e) {
            if (e === "reload") {
                window.setTimeout(function () {
                    window.location.reload(true);
                }, 10);
            }
            else {
                _this.applyFilter(null);
            }
        })
            .always(function () {
            $(_this.trainingWindow).data("kendoWindow").close();
        });
    };
    CampaignEmployeesViewModel.prototype.handleClick = function (button) {
        var _this = this;
        var action = this.get("actionButton" + button + "Text"), progressWindow = $(this.progressWindow).data("kendoWindow"), contract = this.get("contract"), parameters = {
            campaignId: this.campaignId,
            employeeIds: toArray(this.selected),
            contract: contract
        };
        if (this.contractSelectVisible() && (typeof (contract) === "undefined" || contract === null)) {
            alert("Bitte einen Projektauftrag wählen.");
            return;
        }
        this.set("result", "");
        this.set("action", action);
        this.set("amount", parameters.employeeIds.length);
        progressWindow.setOptions({ actions: [] });
        progressWindow.center().open();
        $.post(appPath + "CM/CampaignEmployees/_" + this.get("state") + "Button" + button, parameters)
            .fail(function () {
            _this.applyFilter(null);
            _this.set("result", "Es ist ein Fehler aufgetreten. Die Aktion wurde wahrscheinlich nicht durchgeführt.");
        })
            .done(function (data) {
            _this.applyFilter(null);
            _this.set("result", data);
        })
            .always(function () {
            progressWindow.setOptions({ actions: ["Close"] });
        });
    };
    CampaignEmployeesViewModel.prototype.applyFilter = function (e) {
        var grid = $(this.grid).data("kendoGrid"), state = this.get("state");
        this.selected = {};
        window["cwl"]("filter set: " + state, "k-info-colored");
        $("#checkAll").prop("checked", false);
        this.set("actionButtonEnabled", false);
        window["cwl"]("grid clear request");
        grid.dataSource.data([]);
        if (state === "Any") {
            grid.hideColumn("EmployeeId");
        }
        else {
            if (state === "Accepted" && this.acquisitionList) {
                grid.hideColumn("EmployeeId");
            }
            else {
                grid.showColumn("EmployeeId");
            }
        }
        grid.dataSource.read();
    };
    CampaignEmployeesViewModel.prototype.updateSelected = function (checkbox) {
        var employeeId = checkbox.value, state = checkbox.checked;
        this.selected[employeeId] = state;
        this.set("actionButtonEnabled", false);
        for (var property in this.selected) {
            if (this.selected[property] === true) {
                this.set("actionButtonEnabled", true);
                return;
            }
        }
    };
    CampaignEmployeesViewModel.prototype.nameOrEmailSearchButtonClick = function () {
        this.applyFilter(null);
    };
    CampaignEmployeesViewModel.prototype.gridParams = function () {
        return { "state": this.get("state"), "nameOrMail": this.get("nameOrMail") };
    };
    CampaignEmployeesViewModel.prototype.gridDataBound = function (e) {
        var grid = e.sender, contentElement = grid.element.find(".k-virtual-scrollable-wrap"), currentRecords = grid.dataSource.view();
        contentElement.scrollLeft(this.scrollLeft);
        contentElement.scrollTop(this.scrollTop);
        $("#rowCount").text(kendo.format("{0:#,###}", grid.dataSource.total()));
        for (var i = 0; i < currentRecords.length; i++) {
            var currentRecord = currentRecords[i], uid = currentRecord.uid, employeeId = currentRecord.EmployeeId;
            currentRecord.RolesEdit = currentRecord.Roles !== null
                ? currentRecord.Roles.split(", ")
                : [];
            if (currentRecord.State !== currentRecord.RealState) {
                grid.tbody.find("tr[data-uid='" + uid + "']").addClass("worker-list");
            }
            switch (currentRecord.EmployeeState) {
                case 100:
                    grid.tbody.find("tr[data-uid='" + uid + "']").addClass("inactive-promoter");
                    grid.lockedTable.find("tr[data-uid='" + uid + "']").addClass("inactive-promoter");
                    break;
                case 200:
                    grid.tbody.find("tr[data-uid='" + uid + "']").addClass("blocked-promoter");
                    grid.lockedTable.find("tr[data-uid='" + uid + "']").addClass("blocked-promoter");
                    break;
            }
            var checkbox = grid.tbody.find("tr[data-uid='" + uid + "'] input[type=checkbox]");
            if (!checkbox) {
                continue;
            }
            if (this.selected.hasOwnProperty(employeeId)) {
                var checked = this.selected[employeeId];
                if (checked) {
                    checkbox.prop("checked", true);
                }
            }
        }
    };
    CampaignEmployeesViewModel.prototype.gridDataBinding = function (e) {
        var optionsJson, currentOptions, options, index, columnFound, gridContentElement = $(this.grid).find(".k-virtual-scrollable-wrap");
        this.scrollLeft = gridContentElement.scrollLeft();
        this.scrollTop = gridContentElement.scrollTop();
        if (this.firstGridUpdate) {
            this.firstGridUpdate = false;
            if (localStorage.getItem(this.localStorageKey + "_version") != this.localStorageVersion) {
                return;
            }
            optionsJson = localStorage.getItem(this.localStorageKey);
            if (optionsJson) {
                try {
                    options = JSON.parse(optionsJson);
                }
                catch (ex) {
                    return;
                }
                currentOptions = e.sender.getOptions();
                for (index = 0; index < options.columns.length; index++) {
                    if (options.columns[index].field === "EmployeeId") {
                        options.columns[index].hidden = true;
                        break;
                    }
                }
                for (index = 0; index < currentOptions.columns.length; index++) {
                    var column = currentOptions.columns[index];
                    if (typeof (column.field) !== "undefined" && column.field != null && column.field.indexOf("TrainingDay") > -1) {
                        columnFound = false;
                        for (var i = 0; i < options.columns.length; i++) {
                            if (options.columns[i].field === column.field) {
                                columnFound = true;
                                break;
                            }
                        }
                        if (columnFound) {
                            continue;
                        }
                        options.columns.push(column);
                    }
                }
                e.sender.setOptions(options);
            }
        }
        $("#a [data-field=EmployeeId]")
            .removeClass("k-with-icon")
            .css("text-overflow", "clip")
            .find(".k-header-column-menu")
            .remove();
    };
    CampaignEmployeesViewModel.prototype.saveColumnState = function (e) {
        var options = e.sender.getOptions();
        localStorage.setItem(this.localStorageKey, kendo.stringify(options));
        localStorage.setItem(this.localStorageKey + "_version", this.localStorageVersion);
    };
    CampaignEmployeesViewModel.prototype.saveColumnStateAfterReoderEvent = function (e) {
        var _this = this;
        window.setTimeout(function () {
            var options = { columns: e.sender.getOptions().columns };
            localStorage.setItem(_this.localStorageKey, kendo.stringify(options));
            localStorage.setItem(_this.localStorageKey + "_version", _this.localStorageVersion);
        }, 0);
    };
    return CampaignEmployeesViewModel;
}(kendo.data.ObservableObject));
var viewModel = new CampaignEmployeesViewModel("#a", "#progress-window", "#training-window", "#applied-locations-window");
var currentlyEditing = false;
$(document).ready(function () {
    kendo.bind("#data-bound", viewModel);
    window.setTimeout(function () {
        var gridElement = $("#a"), detailsWindow = $("#details-window").data("kendoWindow");
        gridResizer = new GridResizer($("#hSplitter"), gridElement, function (height) { }, 110);
        gridResizer.resize();
        $("#checkAll").on("change", function (event) {
            var checkbox = event.target;
            $(".k-grid-content-locked :checkbox").prop("checked", checkbox.checked).change();
        });
        gridElement.on("click", ".details-link, .attention", function (event) {
            var dataItem = gridElement.data("kendoGrid").dataItem($(event.currentTarget).closest("tr")), url = appPath + "Employee/Card/" + dataItem.get("EmployeeId") +
                "?campaignId=" + campaignId +
                "&view=" + (acquisitionList ? "Acquisition" : "Promoter") +
                "&_" + new Date().getTime();
            event.preventDefault();
            event.stopPropagation();
            detailsWindow.element.load(url, null, function () {
                detailsWindow.center().open();
            });
        });
        gridElement.on("click", ".k-grid-content tr, .k-grid-content-locked tr", function (event) {
            if (currentlyEditing) {
                if (event.target.tagName !== "INPUT") {
                    currentlyEditing = false;
                }
                return;
            }
            if (viewModel.get("state") === "Any") {
                return;
            }
            if (event.target.tagName !== "INPUT" &&
                event.target.type !== 'checkbox') {
                var uid = $(event.currentTarget).closest("tr").attr("data-uid");
                $("#a .k-grid-content-locked tr[data-uid=" + uid + "] :checkbox").click();
            }
        });
        gridElement.on("click", "a.k-grid-Training", function (event) {
            var dataItem = gridElement.data("kendoGrid").dataItem($(event.currentTarget).closest("tr"));
            event.preventDefault();
            viewModel.employeeId = dataItem.get("EmployeeId");
            viewModel.trainingDays.read();
            $("#training-window").data("kendoWindow").center().open();
        });
        gridElement.on("change", ".k-grid-content-locked input[type=checkbox]", function (event) {
            viewModel.updateSelected(event.target);
        });
    }, 0);
});
function onEdit(e) {
    e.container.closest("tr").find(":checkbox").click();
    currentlyEditing = true;
    if (e.container[0].innerHTML.indexOf("Remark") > -1) {
        return;
    }
    if (e.container[0].innerHTML.indexOf("RolesEdit") > -1) {
        window["currentRowViewModel"] = e.model;
        return;
    }
    if (e.model.RealState !== e.model.State) {
        this.closeCell();
    }
}
function onSave(e) {
    var grid = $("#a").data("kendoGrid"), state = viewModel.get("state");
    currentlyEditing = false;
    if (e.values.hasOwnProperty("Remark") || e.values.hasOwnProperty("RolesEdit") || e.values.hasOwnProperty("Deployable")) {
        window.setTimeout(function () {
            grid.dataSource.sync();
        }, 50);
        return;
    }
    confirmViewModel
        .ask("Der Promoter wird ohne Mailbenachrichtigung in den gewählten Status gesetzt.\n\nStatus des Promoters ändern?")
        .onNo(function () {
        grid.cancelChanges();
    })
        .onYes(function () {
        if (state !== "Any") {
            grid.dataSource.one("sync", function () {
                viewModel.applyFilter(null);
            });
        }
        grid.dataSource.sync();
    });
}
function keyupCampaignEmployee(e) {
    if (e.keyCode === 13) {
        viewModel.nameOrEmailSearchButtonClick();
    }
    return false;
}
//# sourceMappingURL=CampaignEmployeesViewModel.js.map