﻿var cookieOptions = { path: appPath };

function onResize() {
    if (window["_onResize"])
        window["_onResize"]();
}

function onCollapse(e) {
    $.cookie(e.pane.id, "collapsed", cookieOptions);
}

function onExpand(e) {
    $.cookie(e.pane.id, "expanded", cookieOptions);
}