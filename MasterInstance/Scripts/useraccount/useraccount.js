var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PasswordChangeViewModel = (function (_super) {
    __extends(PasswordChangeViewModel, _super);
    function PasswordChangeViewModel() {
        var _this = _super.call(this) || this;
        _this.errorMessageVisable = false;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    PasswordChangeViewModel.prototype.setNewPassword = function () {
        var _this = this;
        $.ajax({
            url: appPath + "MD/Employee/_ChangePassword",
            type: "POST",
            data: {
                userKey: this.get("userId"),
                password: this.get("password")
            },
            global: false,
            error: function (jqxhr) {
                switch (jqxhr.status) {
                    case 500:
                        alert("Es ist ein Fehler aufgetreten. Das Passwort würde wahrscheinlich nicht geändert.");
                        break;
                    default:
                        _this.set("errorMessageVisible", true);
                }
            },
            success: function () {
                _this.set("errorMessageVisible", false);
                $("#password-window").data("kendoWindow").close();
            }
        });
    };
    return PasswordChangeViewModel;
}(kendo.data.ObservableObject));
var passwordChangeViewModel = new PasswordChangeViewModel();
function changePasswordClick(e) {
    var data = this.dataItem($(e.target).closest("tr"));
    e.preventDefault();
    passwordChangeViewModel.set("userId", data.ProviderUserkey);
    passwordChangeViewModel.set("firstname", data.Firstname);
    passwordChangeViewModel.set("lastname", data.Lastname);
    passwordChangeViewModel.set("password", "");
    passwordChangeViewModel.set("errorMessageVisible", false);
    $("#password-window").data("kendoWindow").center().open();
}
$(document).ready(function () {
    kendo.bind("#password-window", passwordChangeViewModel);
    window.setTimeout(function () {
        new GridResizer($("#hSplitter"), $("#accounts")).resize();
    }, 50);
});
var validationMessageTmpl = kendo.template($("#message").html());
function error(args) {
    if (args.errors) {
        var grid = $("#accounts").data("kendoGrid");
        grid.one("dataBinding", function (e) {
            e.preventDefault();
            for (var error in args.errors) {
                showMessage(grid["editable"].element, error, args.errors[error].errors);
            }
        });
    }
}
function showMessage(container, name, errors) {
    container.find("[data-valmsg-for=" + name + "],[data-val-msg-for=" + name + "]")
        .replaceWith(validationMessageTmpl({ field: name, message: errors[0] }));
}
function onUserGridEdit(e) {
    if (e.model.AccountType !== null && e.model.AccountType !== "") {
        $("#AccountType").closest("div").hide().prev().hide();
        $("#Password").closest("div").hide().prev().hide();
        if (e.model.AccountType !== "Kunde") {
            $("#CustomerId").closest("div").hide().prev().hide();
        }
    }
}
//# sourceMappingURL=useraccount.js.map