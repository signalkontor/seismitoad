﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" />

class PasswordChangeViewModel extends kendo.data.ObservableObject {
    firstname: string;
    lastname: string;
    password: string;
    userId: string;
    errorMessageVisable = false;

    constructor() {
        super();
        super.init(this);
    }

    setNewPassword() {
        $.ajax({
            url: appPath + "MD/Employee/_ChangePassword",
            type: "POST",
            data: {
                userKey: this.get("userId"),
                password: this.get("password")
            },
            global: false,
            error: (jqxhr: JQueryXHR) => {
                switch(jqxhr.status) {
                    case 500:
                        alert("Es ist ein Fehler aufgetreten. Das Passwort würde wahrscheinlich nicht geändert.")
                        break;
                    default:
                        this.set("errorMessageVisible", true);
                }
            },
            success: () => {
                this.set("errorMessageVisible", false);
                $("#password-window").data("kendoWindow").close();
            }
        });
    }
}

var passwordChangeViewModel = new PasswordChangeViewModel();

function changePasswordClick(e: JQueryEventObject) {
    var data = this.dataItem($(e.target).closest("tr"));
    e.preventDefault();

    passwordChangeViewModel.set("userId", data.ProviderUserkey);
    passwordChangeViewModel.set("firstname", data.Firstname);
    passwordChangeViewModel.set("lastname", data.Lastname);
    passwordChangeViewModel.set("password", "");
    passwordChangeViewModel.set("errorMessageVisible", false);
    $("#password-window").data("kendoWindow").center().open();
}

$(document).ready(() => {
    kendo.bind("#password-window", passwordChangeViewModel);
    window.setTimeout(() => {
        new GridResizer($("#hSplitter"), $("#accounts")).resize();
    }, 50);
});

var validationMessageTmpl = kendo.template($("#message").html());

function error(args) {
    if (args.errors) {
        var grid = $("#accounts").data("kendoGrid");
        grid.one("dataBinding", e => {
            e.preventDefault();   // cancel grid rebind if error occurs
            for (var error in args.errors) {
                showMessage(grid["editable"].element, error, args.errors[error].errors);
            }
        });
    }
}

function showMessage(container, name, errors) {
    //add the validation message to the form
    container.find("[data-valmsg-for=" + name + "],[data-val-msg-for=" + name + "]")
        .replaceWith(validationMessageTmpl({ field: name, message: errors[0] }));
}

function onUserGridEdit(e) {
    if (e.model.AccountType !== null && e.model.AccountType !== "") {
        $("#AccountType").closest("div").hide().prev().hide();
        $("#Password").closest("div").hide().prev().hide();
        if (e.model.AccountType !== "Kunde") {
            $("#CustomerId").closest("div").hide().prev().hide();
        }
    }
}