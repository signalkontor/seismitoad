﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="kendo.all.d.ts" />

class Question {
    constructor(private confirm: Confirm) {
        confirm.onYes = () => {};
        confirm.onNo = () => {};
    }

    onYes(callback: () => void) {
        this.confirm.onYes = callback;
        return this;
    }

    onNo(callback: () => void) {
        this.confirm.onNo = callback;
        return this;
    }
}

class Confirm extends kendo.data.ObservableObject {
    question: string;
    onYes = () => {}
    onNo = () => {}
    visible = false;

    ask(question: string) {
        this.set("question", question);
        this.set("visible", true);
        return new Question(this);
    }

    yes(e) {
        e.stopImmediatePropagation();
        this.set("visible", false);
        this.onYes();
    }

    no(e) {
        e.stopImmediatePropagation();
        this.set("visible", false);
        this.onNo();
    }
}