﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

interface IAttendanceFilterViewModel {
    CampaignIds: number[];
    DateFrom: string;
    DateTil: string;
}

interface ISelectedItem {
    Text: string;
    Value: number;
}

class AttendanceSelectionViewModel extends kendo.data.ObservableObject {

    selectVisible: boolean;
    campaignIds: ISelectedItem[];

    dateFrom: Date = new Date();
    dateTil: Date = new Date();

    // Properties und Methoden für die Export Funktion
    employee: any;

    exportCampaignClick() {
        this.postForm({
            campaignIds: $.map(<ISelectedItem[]>this.get("campaignIds"), e => e.Value),
            from: kendo.toString(this.get("dateFrom"), "dd.MM.yyyy"),
            til: kendo.toString(this.get("dateTil"), "dd.MM.yyyy"),
        });
    }

    exportEmployeeClick() {
        if (this.exportEmployeeActive()) {
            this.postForm({
                employeeId: this.get("employee").Id,
                from: kendo.toString(this.get("dateFrom"), "dd.MM.yyyy"),
                til: kendo.toString(this.get("dateTil"), "dd.MM.yyyy"),
            });
        }
    }

    private postForm(paramsObj: any) {
        var form = $("form"),
            params = $.param(paramsObj);

        form.attr("action", appPath + "CM/Export/Attendance?" + params);
        form.submit();
    }

    private exportEmployeeActive() {
        var employee = this.get("employee"); 
        return typeof(employee) == "object" && typeof(employee.Id) == "number";
    }

    employeeSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: appPath + "CM/Assign/_Employees",
            },
            parameterMap: (data, type) => {
                var value = "";
                if (typeof (data) == "object" && typeof (data.filter) == "object" && typeof (data.filter.filters) == "object") {
                    value = data.filter.filters[0].value;
                }
                return {
                    text: value
                };
            }
        },
        serverFiltering: true
    });
    // Properties und Methoden für die Export Funktion

    campaignSource = new kendo.data.DataSource({
        transport: {
            read: { url: appPath + "CM/Attendance/_ReadCampaigns" }
        }
    });

    dateFilter: ISelectedItem = { Text: "Heute", Value: 0 };
    dateSource = new kendo.data.DataSource({
        data: [
            { Value: 1, Text: "Alle" },
            { Value: 2, Text: "Zeitraum" },
            { Value: 0, Text: "Heute" },
            { Value: -1, Text: "Gestern" },
            { Value: -2, Text: "Vorgestern" },
            { Value: -3, Text: "Vor 3 Tagen" }
        ]
    });

    customTimeSpanVisible() {
        return (<ISelectedItem>this.get('dateFilter')).Value === 2;
    }

    constructor(campaignId?: number) {
        super();

        if (campaignId != null) {
            this.campaignIds = [{Value: campaignId, Text: ""}];
            this.selectVisible = false;
        } else {
            this.campaignIds = [];
            this.selectVisible = true;
        }

        super.init(this);

        this.bind("change", e => {
            var date: Date,
                dateFilterValue;
            if (e.field === "dateFilter") {
                dateFilterValue = this.get("dateFilter").Value;
                if (dateFilterValue == 1) {
                    date = null;
                } else if (dateFilterValue == 2) {
                    return; // Bei Zeitraum behalten wir die aktuellen Werte bei
                } else {
                    date = new Date();
                    date.setDate(date.getDate() + dateFilterValue);
                }
                this.set("dateFrom", date);
                this.set("dateTil", date);
            }
        });
    }

    gridParams(): IAttendanceFilterViewModel {
        var campaignIds = <ISelectedItem[]>this.get("campaignIds");
        var dateFrom = this.get("dateFrom");
        var dateTil = this.get("dateTil");

        return {
            CampaignIds: campaignIds.map(e => e.Value),
            DateFrom: dateFrom != null ? kendo.toString(dateFrom, "yyyy-MM-dd") : null,
            DateTil: dateTil != null ? kendo.toString(dateTil, "yyyy-MM-dd") : null
        };
    }

    onItemsChange() {
        var grid = <kendo.ui.Grid>$("#attendance").data("kendoGrid");
        grid.dataSource.read();
    }
}

function colorRow(e) {
    var grid = <kendo.ui.Grid>this;

    grid.tbody.find("tr").each((i, e) => {
        var current = grid.dataSource.view()[i];
        $(e).addClass(`attendance-${current.Attendance}`);
    });
}

class NotesViewModel extends kendo.data.ObservableObject {
    assignmentId: number;
    notes: string;
    marker: boolean;

    constructor(private windowSelector: string, private gridSelector: string) {
        super();        
        super.init(this);
    }

    cancel() {
        $(this.windowSelector).data("kendoWindow").close();
    }

    save() {
        var gridDataSource = (<kendo.ui.Grid>$(this.gridSelector).data("kendoGrid")).dataSource;
        if (gridDataSource.hasChanges()) {
            if (!confirm("Die Anwesenheitsliste enthält nicht gespeicherte Änderungen.\nDurch speichern der Notiz gehen diese verloren.\n\n\Trotzdem fortfahren?")) {
                return;
            }
        }
        $.post(appPath + "CM/Attendance/_Notes", {
            assignmentId: this.get("assignmentId "),
            notes: this.get("notes"),
            marker: this.get("marker")
        }, () => {
            gridDataSource.read();
            $(this.windowSelector).data("kendoWindow").close();
        });
    }
}