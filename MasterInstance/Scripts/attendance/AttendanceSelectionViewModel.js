var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AttendanceSelectionViewModel = (function (_super) {
    __extends(AttendanceSelectionViewModel, _super);
    function AttendanceSelectionViewModel(campaignId) {
        var _this = _super.call(this) || this;
        _this.dateFrom = new Date();
        _this.dateTil = new Date();
        _this.employeeSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: appPath + "CM/Assign/_Employees",
                },
                parameterMap: function (data, type) {
                    var value = "";
                    if (typeof (data) == "object" && typeof (data.filter) == "object" && typeof (data.filter.filters) == "object") {
                        value = data.filter.filters[0].value;
                    }
                    return {
                        text: value
                    };
                }
            },
            serverFiltering: true
        });
        _this.campaignSource = new kendo.data.DataSource({
            transport: {
                read: { url: appPath + "CM/Attendance/_ReadCampaigns" }
            }
        });
        _this.dateFilter = { Text: "Heute", Value: 0 };
        _this.dateSource = new kendo.data.DataSource({
            data: [
                { Value: 1, Text: "Alle" },
                { Value: 2, Text: "Zeitraum" },
                { Value: 0, Text: "Heute" },
                { Value: -1, Text: "Gestern" },
                { Value: -2, Text: "Vorgestern" },
                { Value: -3, Text: "Vor 3 Tagen" }
            ]
        });
        if (campaignId != null) {
            _this.campaignIds = [{ Value: campaignId, Text: "" }];
            _this.selectVisible = false;
        }
        else {
            _this.campaignIds = [];
            _this.selectVisible = true;
        }
        _super.prototype.init.call(_this, _this);
        _this.bind("change", function (e) {
            var date, dateFilterValue;
            if (e.field === "dateFilter") {
                dateFilterValue = _this.get("dateFilter").Value;
                if (dateFilterValue == 1) {
                    date = null;
                }
                else if (dateFilterValue == 2) {
                    return;
                }
                else {
                    date = new Date();
                    date.setDate(date.getDate() + dateFilterValue);
                }
                _this.set("dateFrom", date);
                _this.set("dateTil", date);
            }
        });
        return _this;
    }
    AttendanceSelectionViewModel.prototype.exportCampaignClick = function () {
        this.postForm({
            campaignIds: $.map(this.get("campaignIds"), function (e) { return e.Value; }),
            from: kendo.toString(this.get("dateFrom"), "dd.MM.yyyy"),
            til: kendo.toString(this.get("dateTil"), "dd.MM.yyyy"),
        });
    };
    AttendanceSelectionViewModel.prototype.exportEmployeeClick = function () {
        if (this.exportEmployeeActive()) {
            this.postForm({
                employeeId: this.get("employee").Id,
                from: kendo.toString(this.get("dateFrom"), "dd.MM.yyyy"),
                til: kendo.toString(this.get("dateTil"), "dd.MM.yyyy"),
            });
        }
    };
    AttendanceSelectionViewModel.prototype.postForm = function (paramsObj) {
        var form = $("form"), params = $.param(paramsObj);
        form.attr("action", appPath + "CM/Export/Attendance?" + params);
        form.submit();
    };
    AttendanceSelectionViewModel.prototype.exportEmployeeActive = function () {
        var employee = this.get("employee");
        return typeof (employee) == "object" && typeof (employee.Id) == "number";
    };
    AttendanceSelectionViewModel.prototype.customTimeSpanVisible = function () {
        return this.get('dateFilter').Value === 2;
    };
    AttendanceSelectionViewModel.prototype.gridParams = function () {
        var campaignIds = this.get("campaignIds");
        var dateFrom = this.get("dateFrom");
        var dateTil = this.get("dateTil");
        return {
            CampaignIds: campaignIds.map(function (e) { return e.Value; }),
            DateFrom: dateFrom != null ? kendo.toString(dateFrom, "yyyy-MM-dd") : null,
            DateTil: dateTil != null ? kendo.toString(dateTil, "yyyy-MM-dd") : null
        };
    };
    AttendanceSelectionViewModel.prototype.onItemsChange = function () {
        var grid = $("#attendance").data("kendoGrid");
        grid.dataSource.read();
    };
    return AttendanceSelectionViewModel;
}(kendo.data.ObservableObject));
function colorRow(e) {
    var grid = this;
    grid.tbody.find("tr").each(function (i, e) {
        var current = grid.dataSource.view()[i];
        $(e).addClass("attendance-" + current.Attendance);
    });
}
var NotesViewModel = (function (_super) {
    __extends(NotesViewModel, _super);
    function NotesViewModel(windowSelector, gridSelector) {
        var _this = _super.call(this) || this;
        _this.windowSelector = windowSelector;
        _this.gridSelector = gridSelector;
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    NotesViewModel.prototype.cancel = function () {
        $(this.windowSelector).data("kendoWindow").close();
    };
    NotesViewModel.prototype.save = function () {
        var _this = this;
        var gridDataSource = $(this.gridSelector).data("kendoGrid").dataSource;
        if (gridDataSource.hasChanges()) {
            if (!confirm("Die Anwesenheitsliste enthält nicht gespeicherte Änderungen.\nDurch speichern der Notiz gehen diese verloren.\n\n\Trotzdem fortfahren?")) {
                return;
            }
        }
        $.post(appPath + "CM/Attendance/_Notes", {
            assignmentId: this.get("assignmentId "),
            notes: this.get("notes"),
            marker: this.get("marker")
        }, function () {
            gridDataSource.read();
            $(_this.windowSelector).data("kendoWindow").close();
        });
    };
    return NotesViewModel;
}(kendo.data.ObservableObject));
//# sourceMappingURL=AttendanceSelectionViewModel.js.map