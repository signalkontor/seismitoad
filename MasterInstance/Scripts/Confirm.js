var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Question = (function () {
    function Question(confirm) {
        this.confirm = confirm;
        confirm.onYes = function () { };
        confirm.onNo = function () { };
    }
    Question.prototype.onYes = function (callback) {
        this.confirm.onYes = callback;
        return this;
    };
    Question.prototype.onNo = function (callback) {
        this.confirm.onNo = callback;
        return this;
    };
    return Question;
}());
var Confirm = (function (_super) {
    __extends(Confirm, _super);
    function Confirm() {
        var _this = _super.apply(this, arguments) || this;
        _this.onYes = function () { };
        _this.onNo = function () { };
        _this.visible = false;
        return _this;
    }
    Confirm.prototype.ask = function (question) {
        this.set("question", question);
        this.set("visible", true);
        return new Question(this);
    };
    Confirm.prototype.yes = function (e) {
        e.stopImmediatePropagation();
        this.set("visible", false);
        this.onYes();
    };
    Confirm.prototype.no = function (e) {
        e.stopImmediatePropagation();
        this.set("visible", false);
        this.onNo();
    };
    return Confirm;
}(kendo.data.ObservableObject));
//# sourceMappingURL=Confirm.js.map