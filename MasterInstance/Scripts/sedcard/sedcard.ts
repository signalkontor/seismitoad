﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.blockUI/jquery.blockUI.d.ts" />
/// <reference path="../kendo.all.d.ts" /> 

declare var employeeId: number;
declare var type: string;
declare var portrait: string;
declare var fullBody: string;
declare var other: string;

class SedcardViewModel extends kendo.data.ObservableObject {
    constructor() {
        super();
        function firstCharToLower(str: string): string {
            return str.charAt(0).toLowerCase() + str.slice(1);
        }

        $.ajax({
            url: appPath + "MD/Sedcard/_Data?employeeId=" + employeeId + "&type=" + type,
            cache: false,
            dataType: "json",
            success: (data) => {
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        this.set(firstCharToLower(property), data[property] || "");
                    }
                }
            }
        });
        super.init(this);
    }

    // Persönliche Daten
    vorname: string;
    nachname: string;
    funktion: string;
    einsatzort: string;
    alter: string;
    groesse: string;
    konfektionsgroesse: string;
    shirtGroesse: string;
    fuehrerschein: string;
    ausbildung: string;
    studium: string;
    berufserfahrung: string;
    sprachkenntnisse: string;
    softwarekenntnisse: string;
    hardwarekenntnisse: string;
    sonstigeKenntnisse: string;
    projekte: string;
    sonstiges: string;
    extra: string;
    portraitPhotoVisible: boolean = true;
    fullBodyPhotoVisible: boolean;
    otherPhotoVisible: boolean;
    einsatzortVisible: boolean;
    funktionVisible: boolean;
    alterVisible: boolean;
    groesseVisible: boolean;
    konfektionsgroesseVisible: boolean;
    shirtGroesseVisible: boolean;
    fuehrerscheinVisible: boolean;
    ausbildungVisible: boolean;
    studiumVisible: boolean;
    berufserfahrungVisible: boolean;
    sprachkenntnisseVisible: boolean;
    softwarekenntnisseVisible: boolean;
    hardwarekenntnisseVisible: boolean;
    sonstigeKenntnisseVisible: boolean;
    projekteVisible: boolean;
    sonstigesVisible: boolean;
    extraVisible: boolean;

    save() {
        (<JQueryStatic>window.top["jQuery"]).blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: ()=> {
                alert("Es ist ein Fehler aufgetreten.");
            }
        }).always(()=> {
            (<JQueryStatic>window.top["jQuery"]).unblockUI();
        });
    }

    pdf() {
        (<JQueryStatic>window.top["jQuery"]).blockUI({ message: "Erstelle PDF..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: () => {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: (data) => {
                window.top["downloadPDF"](data);
            }
        }).always(() => {
            (<JQueryStatic>window.top["jQuery"]).unblockUI();
        });
    }

    docx() {
        (<JQueryStatic>window.top["jQuery"]).blockUI({ message: "Erstelle DOCX..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: () => {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: (data) => {
                window.top["downloadDOCX"](data);
            }
        }).always(() => {
            (<JQueryStatic>window.top["jQuery"]).unblockUI();
        });
    }

    private reset(field: string) {
        var self = this;
        (<JQueryStatic>window.top["jQuery"]).blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Reset?employeeId=" + employeeId + "&field=" + field,
            type: "POST",
            error: () => {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: (data) => {
                self.set(field, data);
            }
        }).always(()=> {
            (<JQueryStatic>window.top["jQuery"]).unblockUI();
        });
    }

    private photoSize(): number {
        var noOfPhotos = 0;
        if (this.get("portraitPhotoVisible")) {
            noOfPhotos++;
        }
        if (this.get("fullBodyPhotoVisible")) {
            noOfPhotos++;
        }
        if (this.get("otherPhotoVisible")) {
            noOfPhotos++;
        }
        return Math.floor(450 / noOfPhotos);
    }

    portraitPhotoSource() {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return portrait + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    }

    fullBodyPhotoSource() {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return fullBody + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    }

    otherPhotoSource() {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return other + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    }

    resetAusbildung() {
        this.reset("ausbildung");
    }

    resetStudium() {
        this.reset("studium");
    }

    resetBerufserfahrung() {
        this.reset("berufserfahrung");
    }

    resetSoftwarekenntnisse() {
        this.reset("softwarekenntnisse");
    }

    resetHardwarekenntnisse() {
        this.reset("hardwarekenntnisse");
    }

    resetReferenzen() {
        this.reset("referenzen");
    }

    paste(e: kendo.ui.EditorPasteEvent) {
        var div = document.createElement("div");
        div.innerHTML = e.html;
        e.html = div.textContent || div.innerText || "";
    }
}

var sedcardViewModel = new SedcardViewModel();

$(document).ready(() => {
    kendo.bind("body", sedcardViewModel );
    if (window.top.document) {
        kendo.bind($(window.top.document).find(".editor"), sedcardViewModel );
    }
});