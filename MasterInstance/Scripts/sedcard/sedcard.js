var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SedcardViewModel = (function (_super) {
    __extends(SedcardViewModel, _super);
    function SedcardViewModel() {
        var _this = _super.call(this) || this;
        _this.portraitPhotoVisible = true;
        function firstCharToLower(str) {
            return str.charAt(0).toLowerCase() + str.slice(1);
        }
        $.ajax({
            url: appPath + "MD/Sedcard/_Data?employeeId=" + employeeId + "&type=" + type,
            cache: false,
            dataType: "json",
            success: function (data) {
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        _this.set(firstCharToLower(property), data[property] || "");
                    }
                }
            }
        });
        _super.prototype.init.call(_this, _this);
        return _this;
    }
    SedcardViewModel.prototype.save = function () {
        window.top["jQuery"].blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: function () {
                alert("Es ist ein Fehler aufgetreten.");
            }
        }).always(function () {
            window.top["jQuery"].unblockUI();
        });
    };
    SedcardViewModel.prototype.pdf = function () {
        window.top["jQuery"].blockUI({ message: "Erstelle PDF..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: function () {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: function (data) {
                window.top["downloadPDF"](data);
            }
        }).always(function () {
            window.top["jQuery"].unblockUI();
        });
    };
    SedcardViewModel.prototype.docx = function () {
        window.top["jQuery"].blockUI({ message: "Erstelle DOCX..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Save?employeeId=" + employeeId + "&type=" + type,
            type: "POST",
            data: this.toJSON(),
            error: function () {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: function (data) {
                window.top["downloadDOCX"](data);
            }
        }).always(function () {
            window.top["jQuery"].unblockUI();
        });
    };
    SedcardViewModel.prototype.reset = function (field) {
        var self = this;
        window.top["jQuery"].blockUI({ message: "Speichere..." });
        $.ajax({
            url: appPath + "MD/Sedcard/_Reset?employeeId=" + employeeId + "&field=" + field,
            type: "POST",
            error: function () {
                alert("Es ist ein Fehler aufgetreten.");
            },
            success: function (data) {
                self.set(field, data);
            }
        }).always(function () {
            window.top["jQuery"].unblockUI();
        });
    };
    SedcardViewModel.prototype.photoSize = function () {
        var noOfPhotos = 0;
        if (this.get("portraitPhotoVisible")) {
            noOfPhotos++;
        }
        if (this.get("fullBodyPhotoVisible")) {
            noOfPhotos++;
        }
        if (this.get("otherPhotoVisible")) {
            noOfPhotos++;
        }
        return Math.floor(450 / noOfPhotos);
    };
    SedcardViewModel.prototype.portraitPhotoSource = function () {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return portrait + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    };
    SedcardViewModel.prototype.fullBodyPhotoSource = function () {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return fullBody + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    };
    SedcardViewModel.prototype.otherPhotoSource = function () {
        var width = this.photoSize();
        var height = Math.floor(width * 1.2);
        return other + "?width=" + width + "&height=" + height + "&404=default&scale=both&mode=max";
    };
    SedcardViewModel.prototype.resetAusbildung = function () {
        this.reset("ausbildung");
    };
    SedcardViewModel.prototype.resetStudium = function () {
        this.reset("studium");
    };
    SedcardViewModel.prototype.resetBerufserfahrung = function () {
        this.reset("berufserfahrung");
    };
    SedcardViewModel.prototype.resetSoftwarekenntnisse = function () {
        this.reset("softwarekenntnisse");
    };
    SedcardViewModel.prototype.resetHardwarekenntnisse = function () {
        this.reset("hardwarekenntnisse");
    };
    SedcardViewModel.prototype.resetReferenzen = function () {
        this.reset("referenzen");
    };
    SedcardViewModel.prototype.paste = function (e) {
        var div = document.createElement("div");
        div.innerHTML = e.html;
        e.html = div.textContent || div.innerText || "";
    };
    return SedcardViewModel;
}(kendo.data.ObservableObject));
var sedcardViewModel = new SedcardViewModel();
$(document).ready(function () {
    kendo.bind("body", sedcardViewModel);
    if (window.top.document) {
        kendo.bind($(window.top.document).find(".editor"), sedcardViewModel);
    }
});
//# sourceMappingURL=sedcard.js.map