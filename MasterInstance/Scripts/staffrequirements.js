function selectNewestVersion() {
    var dropdown = $("#dropdown").data("kendoDropDownList");
    dropdown.select(dropdown.dataSource.data().length - 1);
    dropdown.trigger("change");
}
function versionChanged() {
    var url = $("#detailsUrl").val();
    $("#staffrequirement").load(url, { version: this.value() });
}
$(document).ready(function () {
    window.setTimeout(function () {
        selectNewestVersion();
    }, 50);
});
//# sourceMappingURL=staffrequirements.js.map