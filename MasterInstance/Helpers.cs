﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Hosting;
using Elmah;
using MasterInstance.Models.ViewModels;
using MasterInstance.Services;
using Postal;
using SeismitoadModel.DatabaseContext;
using System.Data.SqlClient;

namespace MasterInstance
{
    public static class Helpers
    {
        private const string query = "SELECT Campaigns.Name + ' (' + Customers.Number + Campaigns.Number +  ')' FROM Campaigns INNER JOIN Customers ON Campaigns.Customer_Id = Customers.Id WHERE Campaigns.Id = @CampaignId";

        public static string PageTitle(int campaignId, string title)
        {
            if (campaignId == -1)
            {
                return title + " - Neues Projekt";
            }
            using(var dbContext = new SeismitoadDbContext())
            {
                var campaign = dbContext.Database.SqlQuery<string>(query, new SqlParameter("@CampaignId", campaignId)).Single();
                return title + " - " + campaign;
            }
        }

        public static string GetImageUrl(string baseUrl, string baseName, int width, int height)
        {
            var queryString = string.Format("?width={0}&height={1}&mode=max&404=default", width, height);

            var basePath = HostingEnvironment.MapPath(baseUrl);
            if (basePath == null || !Directory.Exists(basePath))
                return baseUrl + "missing.jpg" + queryString;

            var filename = Path.GetFileName(Directory.GetFiles(basePath, baseName + ".*").FirstOrDefault());
            return baseUrl + (filename ?? "missing.jpg") + queryString;
        }

        public static EmailService GetEmailService()
        {
            // Prepare Postal classes to work outside of ASP.NET request
            var viewsPath = HostingEnvironment.MapPath(@"~/Views/Emails");
            var engines = new MyViewEngineCollection { new FileSystemRazorViewEngine(viewsPath) };
            return new EmailService(engines);
        }
    }

    public static class FindTypes
    {
        public static AssemblyNavigator InAssembly(Assembly assembly)
        {
            return new AssemblyNavigator(assembly);
        }
    }

    public class AssemblyNavigator
    {
        private readonly Assembly _assembly;

        public AssemblyNavigator(Assembly assembly)
        {
            _assembly = assembly;
        }

        public IEnumerable<T> Implementing<T>() where T : class
        {
            return
                from type in _assembly.GetTypes()
                where typeof(T).IsAssignableFrom(type) && type.IsClass
                select Activator.CreateInstance(type) as T;
        }
    }
}