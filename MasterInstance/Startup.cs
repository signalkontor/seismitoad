﻿using Hangfire;
using Hangfire.Dashboard;
using MasterInstance;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace MasterInstance
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HangfireBootstrapper.Instance.Start();

            var options = new DashboardOptions
            {
                AuthorizationFilters = new[] {new HangfireAuthorizationFilter()}
            };
            app.UseHangfireDashboard("/hangfire", options);
        }
    }
}
