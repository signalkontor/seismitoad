﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MasterInstance.App_Start;
using MasterInstance.Models;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Constants;
using SeismitoadShared.App_Start;
using SeismitoadShared.ModelBinders;
using SeismitoadCommon.ModelBinders;

namespace MasterInstance
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static readonly bool IsStaging = ConfigurationManager.AppSettings.AllKeys.Contains("StagingEnvironment");

        // ReSharper disable InconsistentNaming
        protected virtual void Application_BeginRequest()
        // ReSharper restore InconsistentNaming
        {
            HttpContext.Current.Items[HttpContextKeys.DbContextKey] = new SeismitoadDbContext();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("de-DE");
        }

        // ReSharper disable InconsistentNaming
        protected virtual void Application_EndRequest()
        // ReSharper restore InconsistentNaming
        {
            var dbContext = HttpContext.Current.Items[HttpContextKeys.DbContextKey] as SeismitoadDbContext;
            if (dbContext != null)
                dbContext.Dispose();
        }

        // ReSharper disable InconsistentNaming
        protected void Application_Start()
        // ReSharper restore InconsistentNaming
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            DefaultRouteConfig.RegisterRoutes(RouteTable.Routes);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ChangeLoggerConfig.Configure();
            ObjectTemplateConfig.Configure();
            AutoFacConfig.Configure();
            AutoMapperConfig.RegisterMappings();
            ModelBinders.Binders.DefaultBinder = new DefaultModelBinderWithHtmlValidation();
            ModelBinders.Binders.Add(typeof(DateTime), new DEDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DEDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(string), new TrimModelBinder());
            DbInterception.Add(new OptionRecompileHintDbCommandInterceptor());
            HangfireBootstrapper.Instance.Start();
            StartBackgroundServices();
        }

        private static void StartBackgroundServices()
        {
            AssignmentSyncService.Start();
            GeocodingService.Start();
            FaxReceptionService.Start();
            FaxAttachmentArchiving.Start();
            ProfileUploadsService.Start();
            PingCustomerInstances.Start();
            DeferredMails.Start();
        }

        protected void Application_End()
        {
            HangfireBootstrapper.Instance.Stop();
        }
    }

    public static class DB
    {
        public static SeismitoadDbContext Context
        {
            get { return HttpContext.Current.Items[HttpContextKeys.DbContextKey] as SeismitoadDbContext; }
        }

        public static void DisposeAndRecreateContext()
        {
            Context.Dispose();
            HttpContext.Current.Items[HttpContextKeys.DbContextKey] = new SeismitoadDbContext();
        }
    }
}