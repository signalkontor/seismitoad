﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MasterInstance.Models;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Controllers
{
    public partial class EmployeeController : Controller
    {
        private const string ProfileImagesBaseUrl = "~/PromoterUploads/Employee/{0}/";

        private readonly SeismitoadDbContext _dbContext;

        public EmployeeController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Profile(int id)
        {
            return RedirectToAction(MVC.MD.EmployeeProfile.Index(id));
        }

        public virtual ActionResult FailedIndex()
        {
            var context = new AdvantageModel.Models.seismitoad_advantageContext();

            return View(context.doa_login.Where(d => d.employeeId == null && d.user_typ == "do_promoter"));
        }

        public virtual ActionResult AdvantageProfile(int id, int status)
        {
            ViewBag.doa_User = new[] { id, status };
            return View();
        }

        public virtual ActionResult Card(int id, int? campaignId = null, string view = null)
        {
            var model = new EmployeeCardViewModel
            {
                RelativeImageUrl = Helpers.GetImageUrl(string.Format(ProfileImagesBaseUrl, id), "UploadPortraitPhoto", 120, 120)
            };
            var employee = _dbContext.Employees.Single(e => e.Id == id);
            Mapper.Map(employee, model);
            Mapper.Map(employee.Profile, model);
            if(campaignId.HasValue && view == "Promoter")
            {
                model.AddressChanged = _dbContext.CampaignEmployees
                    .Where(e => e.CampaignId == campaignId && e.EmployeeId == id)
                    .Select(e => e.AddressChanged)
                    .Single();
                model.AddressChangeAckUrl = Url.Action(MVC.Employee.AcknowledgeAddressChange(campaignId.Value, id));
            }
            return View(model);
        }

        public virtual ActionResult AcknowledgeAddressChange(int campaignId, int employeeId)
        {
            var campaignEmployee = _dbContext.CampaignEmployees
                .Single(e => e.CampaignId == campaignId && e.EmployeeId == employeeId);

            campaignEmployee.AddressChanged = false;
            _dbContext.SaveChanges();
            return Content("OK");
        }
    }
}
