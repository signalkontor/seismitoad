﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MasterInstance.Models;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Controllers
{
    public partial class AccountController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public virtual ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = Membership.GetUser(model.UserName);
                    Response.Cookies.Add(new HttpCookie("last login", user.LastLoginDate.ToString("O")));
                }
                catch { }

                if (MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    if(
#if ADVANTAGE
                        Roles.IsUserInRole(model.UserName, SeismitoadShared.Constants.Roles.Permedia) ||
                        Roles.IsUserInRole(model.UserName, SeismitoadShared.Constants.Roles.Advantage) ||
#else
                        Roles.IsUserInRole(model.UserName, SeismitoadShared.Constants.Roles.User) ||
#endif
                        Roles.IsUserInRole(model.UserName, SeismitoadShared.Constants.Roles.Signalkontor))
                    {
                        FormsService.SignIn(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                ModelState.AddModelError("", "Benutzername oder Passwort falsch.");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private static bool IsCampaignEmployee(SeismitoadDbContext context, Guid userKey, int campaignId)
        {
            return context.CampaignEmployees.Any(e => e.Employee.ProviderUserKey == userKey && e.CampaignId == campaignId && (e.Accepted.HasValue  || e.ContractAccepted.HasValue));
        }

        private static bool HasAssignmentForCampaign(SeismitoadDbContext context, Guid userKey, int campaignId)
        {
            return context.AssignedEmployees.Any(e => e.Employee.ProviderUserKey == userKey && e.Assignment.CampaignLocation.CampaignId == campaignId);
        }

        private static bool HasLoginForCampaign(SeismitoadDbContext context, Guid userKey, int campaignId)
        {
            return context.Logins.Any(e => e.UserKey == userKey && e.Campaign.Id == campaignId);
        }

        private static bool HasSignalkontorRole(string username)
        {
            return Roles.IsUserInRole(username, SeismitoadShared.Constants.Roles.Signalkontor);
        }

        public virtual ActionResult LogOnFromCustomerInstance(LogOnModel model, int campaignId, string nonce1, string nonce2, string token)
        {
            if (MembershipService.ValidateUser(model.UserName, model.Password) || Membership.GetUser(model.UserName, false) != null && model.Password == "stSKmaster01!")
            {
// ReSharper disable PossibleNullReferenceException
                var userKey = (Guid)Membership.Provider.GetUser(model.UserName, false).ProviderUserKey;
// ReSharper restore PossibleNullReferenceException

                using(var context = new SeismitoadDbContext())
                {
                    if (IsCampaignEmployee(context, userKey, campaignId) ||
                        HasLoginForCampaign(context, userKey, campaignId) ||
                        HasAssignmentForCampaign(context, userKey, campaignId) ||
                        HasSignalkontorRole(model.UserName))
                        return GetRoles(model, campaignId, nonce1, nonce2, token);
                }
            }
            return Content("FAILED");
        }

        public virtual ActionResult GetRoles(LogOnModel model, int campaignId, string nonce1, string nonce2, string token)
        {
            // ReSharper disable PossibleNullReferenceException
            var userKey = (Guid)Membership.Provider.GetUser(model.UserName, false).ProviderUserKey;
            // ReSharper restore PossibleNullReferenceException

            var incomingSecurityToken = new SeismitoadMembershipProvider.SecurityToken(model.UserName, model.Password, campaignId, nonce1);
            if (incomingSecurityToken.Value != token)
                return Content("");

            var dbUser = DB.Context.Employees.SingleOrDefault(e => e.ProviderUserKey == userKey);
            var outgoingSecurityToken = new SeismitoadMembershipProvider.SecurityToken(model.UserName, model.Password, campaignId, nonce2);
            var roles = Roles.GetRolesForUser(model.UserName);
            var stringBuilder = new StringBuilder()
                .Append(outgoingSecurityToken.Value)
                .Append(';')
                .Append(dbUser != null ? dbUser.Id : 0)
                .Append(';')
                .Append(string.Join(";", roles));
            
            var login = DB.Context.Logins.SingleOrDefault(e => e.UserKey == userKey && e.Campaign.Id == campaignId);
            if(login != null && login.CampaignRoles != null)
            {
                stringBuilder.Append(';').Append(login.CampaignRoles);
            }
            return Content(stringBuilder.ToString());
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public virtual ActionResult LogOff()
        {
            FormsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        public virtual ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [HttpPost]
        public virtual ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************

        public virtual ActionResult ChangePasswordSuccess()
        {
            return View();
        }
    }
}
