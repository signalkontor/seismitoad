﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;

namespace MasterInstance.Controllers
{
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Campaigns([DataSourceRequest] DataSourceRequest request)
        {
            var model = User.Identity.GetCampaigns().Project().To<HomeCampaignViewModel>();
            return Json(model.ToDataSourceResult(request));
        }
    }
}
