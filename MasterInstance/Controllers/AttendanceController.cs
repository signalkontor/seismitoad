﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using SeismitoadModel.DatabaseContext;
using ObjectTemplate.Extensions;
using OfficeOpenXml;
using SeismitoadMembershipProvider;
using SeismitoadModel;
using SeismitoadShared;
using SeismitoadShared.Controllers;
using SeismitoadShared.Models;

namespace MasterInstance.Controllers
{
    public partial class AttendanceController : Controller
    {
        public virtual ActionResult Grid(int id, DateTime? dateFrom, DateTime? dateTil, int[] locationIds)
        {
            // TODO check authorization?
            var model = DB.Context.AssignedEmployees
                .Where(e => e.Assignment.CampaignLocation.CampaignId == id);
            if(dateFrom.HasValue)
            {
                model = model.Where(e => dateFrom < e.Assignment.DateStart);
            }
            if (dateTil.HasValue)
            {
                var date = dateTil.Value.AddDays(1);
                model = model.Where(e => date > e.Assignment.DateEnd);
            }
            if(locationIds != null)
            {
                model = model.Where(e => locationIds.Contains(e.Assignment.CampaignLocation.LocationId));
            }
            Response.AddHeader("Access-Control-Allow-Origin", "*");
            return View(model);
        }
    }
}
