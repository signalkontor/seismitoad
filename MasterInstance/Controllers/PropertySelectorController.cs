﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MasterInstance.Models.ViewModels;
using SeismitoadShared.Extensions;

namespace MasterInstance.Controllers
{
    public abstract partial class PropertySelectorController : Controller
    {
        private readonly string _visiblePropertiesKey;
        protected readonly string[] DefaultProperties;

        protected PropertySelectorController(string[] defaultProperties, string controllerName)
        {
            DefaultProperties = defaultProperties;
            _visiblePropertiesKey = controllerName + "_VisibleProperties";
        }

        protected string[] VisibleProperties
        {
            get { return Session[_visiblePropertiesKey] as string[] ?? DefaultProperties; }
            set { Session[_visiblePropertiesKey] = value; }
        }

        [HttpPost]
        public abstract ActionResult SetVisibleProperties(VisiblePropertiesModel model);
    }
}
