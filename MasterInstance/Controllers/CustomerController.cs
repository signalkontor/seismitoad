﻿using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Models;
using SeismitoadModel;
using System.Linq;

namespace MasterInstance.Controllers
{
    [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin)]
    public partial class CustomerController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        // Wird vom Popup "Neuer Kunde benutzt"
        #region Vielleicht kann man hierauf verzichen
        public virtual ActionResult New()
        {
            return View();
        }

        // Wird vom Popup "Neuer Kunde benutzt"
        [HttpPost]
        public virtual ActionResult New(Customer model)
        {
            if (DB.Context.Customers.Any(e => e.Number == model.Number))
            {
                ModelState.AddModelError("Number", "Nummer wird bereits von einem anderen Kunden verwendet.");
            }
            if (ModelState.IsValid)
            {
                DB.Context.Customers.Add(model);
                DB.Context.SaveChanges();
                return View(MVC.Shared.Views.RefreshParentFrame, model);
            }
            return View();
        }
        #endregion

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = DB.Context.Customers.Project().To<CustomerViewModel>();
            return Json(model.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var customer = DB.Context.Customers.Single(e => e.Id == model.Id);
                UpdateModel(customer);
                DB.Context.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var customer = new Customer();
                UpdateModel(customer);
                DB.Context.Customers.Add(customer);
                DB.Context.SaveChanges();
                model.Id = customer.Id;
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

    }
}
