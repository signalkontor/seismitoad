﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.Extensions;

namespace MasterInstance.Controllers
{
    [MenuArea(MenuArea.Project)]
    public partial class AssignmentController : PropertySelectorController
    {
        #region Properties and constants
        /// <summary>
        /// Never show these properties (including sub-properties) in grid / multiselect
        /// No need to add Id-properies here, they are filtered in any case.
        /// </summary>
        private static readonly string[] HiddenProperties = new[]
                                                                {
                                                                    "Contact", "Campaign",
                                                                    "FriendlyName", "FriendlyType",
                                                                    "WiFiAvailable", "NetworkConnectionAvailable",
                                                                    "GeographicCoordinates", "State"
                                                                };
        private static readonly Dictionary<string, KeyValuePair<string, string>> Properties;
        #endregion

        #region c'tors
        public AssignmentController()
            : base(new[] { "Location.Name", "Location.Street", "Location.City" }, MVC.Assignment.Name)
        {
            
        }

        static AssignmentController()
        {
            Properties = new Dictionary<string, KeyValuePair<string, string>>();

            var propertyInfos =
                typeof (CampaignLocation).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(e => !e.Name.EndsWith("Id") && !HiddenProperties.Contains(e.Name) && e.SetMethod != null && e.SetMethod.IsPublic);

            foreach (var propertyInfo in propertyInfos)
            {
                var displayName = SeismitoadShared.Helpers.DisplayNameHelper.GetDisplayName(propertyInfo);
                if (propertyInfo.PropertyType.IsClass && propertyInfo.PropertyType != typeof (string))
                {
                    var subPropertyInfos = propertyInfo.PropertyType.GetProperties(BindingFlags.Instance |
                                                                                   BindingFlags.Public)
                        .Where(e =>
                               !HiddenProperties.Contains(e.Name) &&
                               e.SetMethod != null && e.SetMethod.IsPublic &&
                               !e.Name.EndsWith("Id") && !e.Name.EndsWith("Start") && !e.Name.EndsWith("End") &&
                               (e.PropertyType.IsValueType || e.PropertyType == typeof (string)));

                    foreach (var subPropertyInfo in subPropertyInfos)
                    {
                        var name = SeismitoadShared.Helpers.DisplayNameHelper.GetDisplayName(subPropertyInfo);
                        Properties.Add($"{propertyInfo.Name}.{subPropertyInfo.Name}", new KeyValuePair<string, string>(displayName, name));
                    }
                }
                else
                {
                    Properties.Add(propertyInfo.Name, new KeyValuePair<string, string>("Ort", displayName));
                }
            }
            Properties.Add("NetworkConnectionBestValue", new KeyValuePair<string, string>("Ort", "W-LAN / Internet vorhanden"));
            Properties.Add("Location.LocationGroup", new KeyValuePair<string, string>("Ort", "Channel"));
            Properties.Add("Location.StateProvince", new KeyValuePair<string, string>("Ort", "Bundesland"));
            Properties.Add("State", new KeyValuePair<string, string>("CampaignLocation", "Status"));
        }
        #endregion

        #region Edit assignments by location
        // ReSharper disable InconsistentNaming
        public virtual ActionResult _Location(int campaignId, int locationId)
// ReSharper restore InconsistentNaming
        {
            return PartialView(new { CampaignId = campaignId, LocationId = locationId });
        }

        [GridAction]
        public virtual ActionResult SelectAjax(int campaignId, int locationId)
        {
            var assignments = ToSerializableAssignments(
                DB.Context.ActiveAssignments.Where(e => e.CampaignLocation.CampaignId == campaignId && e.CampaignLocation.LocationId == locationId));
            return View(new GridModel(assignments));
        }

        [HttpPost]
        [GridAction]
        public virtual ActionResult UpdateAjax(int id)
        {
            var assignment = DB.Context.Assignments.Single(e => e.Id == id);
            TryUpdateModel(assignment);
            assignment.MarkForSync();
            DB.Context.SaveChanges();

            var campaignId = assignment.CampaignLocation.CampaignId;
            var locationId = assignment.CampaignLocation.LocationId;

            var assignments = ToSerializableAssignments(
                DB.Context.ActiveAssignments.Where(e => e.CampaignLocation.CampaignId == campaignId && e.CampaignLocation.LocationId == locationId));

            return View(new GridModel(assignments));
        }

        [HttpPost]
        [GridAction]
        public virtual ActionResult DeleteAjax(int id)
        {
            var assignment = DB.Context.Assignments.Single(e => e.Id == id);
            assignment.State = AssignmentState.Deleted;
            assignment.MarkForSync();
            DB.Context.SaveChanges();
            var assignments = DB.Context.ActiveAssignments
                .Where(e =>
                    e.CampaignLocation.CampaignId == assignment.CampaignLocation.CampaignId &&
                    e.CampaignLocation.LocationId == assignment.CampaignLocation.LocationId);
            var model = ToSerializableAssignments(assignments);

            return View(new GridModel(model));
        }
        #endregion

        #region SetVisibleProperties
        public override ActionResult SetVisibleProperties(VisiblePropertiesModel model)
        {
            VisibleProperties = model.Reset ? DefaultProperties : model.P;
            return RedirectToAction(MVC.Assignment.Index().AddRouteValues(this.CleanRouteValues()));
        }
        #endregion

        #region ToSerializableAssignments
        private static IEnumerable<object> ToSerializableAssignments(IEnumerable<Assignment> assignments, bool localtime = false)
        {
            return assignments.Select(assignment => new
            {
                assignment.Id,
                DateStart = localtime ? assignment.DateStart.ToLocalTime() : assignment.DateStart,
                DateEnd = localtime ? assignment.DateEnd.ToLocalTime() : assignment.DateEnd,
                assignment.State,
                assignment.EmployeeCount,
                Team = assignment.TeamShort,
            });
        }
        #endregion
    }
}
