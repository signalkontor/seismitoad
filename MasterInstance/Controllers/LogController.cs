﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elmah;

namespace MasterInstance.Controllers
{
    public class JavaScriptException : Exception
    {
        public JavaScriptException(string message)
            : base(message)
        {
        }
    }

    public partial class LogController : Controller
    {
        public virtual ActionResult Index(string message)
        {
            ErrorSignal.FromCurrentContext().Raise(new JavaScriptException(message));
            return new EmptyResult();
        }
    }
}