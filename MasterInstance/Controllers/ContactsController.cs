﻿using System.Linq;
using System.Web.Mvc;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Controllers;

namespace MasterInstance.Controllers
{
    public partial class ContactsController : CreateUpdateDeleteController<Contact>
    {
        public ContactsController() : base(DB.Context, DB.Context.Contacts)
        {
        }

        public override ActionResult Index(int? locationId)
        {
            var location = DB.Context.Locations.Single(e => e.Id == locationId);
            ViewBag.Location = location;
            return View(location.Contacts);
        }
    }
}
