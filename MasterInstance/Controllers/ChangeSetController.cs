﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MasterInstance.Controllers
{
    public partial class ChangeSetController : Controller
    {
        //
        // GET: /ChangeSet/

        public virtual ActionResult Index()
        {
            return View(DB.Context.ChangeSets);
        }

        public virtual ActionResult Show(string entityType, string id)
        {
            ViewBag.IsSingleEntity = true;
            return View(Views.Index, DB.Context.ChangeSets.Where(e => e.TableName == entityType && e.EntityId == id));
        }
    }
}
