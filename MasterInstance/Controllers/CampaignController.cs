﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using MasterInstance.Extensions;
using MasterInstance.Models;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadShared.Controllers;
using System.Linq;
using System.Net;
using Telerik.Web.Mvc;
using AutoMapper;
using Hangfire;

namespace MasterInstance.Controllers
{
    public partial class CampaignController : CreateUpdateDeleteController<Campaign>
    {
        public override ActionResult Index(int? id)
        {
            ViewBag.CampaignId = id;
            return base.Index(id);
        }

        public CampaignController() : base(DB.Context, DB.Context.Campaigns)
        {
        }

        #region Overrides of CreateUpdateDeleteController-Methods
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin)]
        public override ActionResult Save(int id)
        {
            var customerId = Convert.ToInt32(Request["CustomerId"]);
            var number = Request["Number"];
            if (DB.Context.Campaigns.Any(e => e.Id != id && e.CustomerId == customerId && e.Number == number))
            {
                ModelState.AddModelError("Number", "Projektnummer ist bereits vergeben");
            }
            return base.Save(id);
        }
        #endregion

        public static void NotifyAdvantageUsers(string title, string username)
        {
            var emailService = Helpers.GetEmailService();
            var email = new NewCampaignEmail("NewCampaign", username, title);
#if ADVANTAGE
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            var admins = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Admin);
            var userObj = Membership.GetUser(username, false);
            if (userObj != null)
            {
                email.AddRecipient(userObj.Email);
            }
            foreach (var user in users.Concat(admins).Distinct())
            {
                userObj = Membership.GetUser(user, false);
                if (userObj != null && userObj.UserName != username && userObj.IsApproved)
                {
                    email.AddRecipient(userObj.Email);
                }
            }
            emailService.Send(email);
        }

        #region Ajax-Actions: CRUD for Logins
        [GridAction]
        public virtual ActionResult SelectAjax(int id)
        {
            var model = DB.Context.Logins
                .Where(e =>
                        e.CampaignId == id &&
                        !e.CampaignRoles.Contains(SeismitoadShared.Constants.Roles.SelfbookingPromoter) &&
                        !e.CampaignRoles.Contains(SeismitoadShared.Constants.Roles.RegionalManager))
                .Join(DB.Context.Accounts, login => login.UserKey, account => account.ProviderUserKey, (login, account) => new { Login = login, Account = account })
                .ToList()
                .Select(e =>
                {
                    var user = Membership.GetUser(e.Login.UserKey);
                    var isApproved = user?.IsApproved;
                    return new FriendlyLoginModel(e.Login.CampaignRoles)
                    {
                        Id = e.Login.Id,
                        UserKey = e.Login.UserKey,
                        FriendlyName = e.Account.Firstname + " " + e.Account.Lastname,
                        IsApproved = isApproved.GetValueOrDefault()
                    };
                });
            return View(new GridModel(model));
        }

        private bool UserAllowed(FriendlyLoginModel model, int id)
        {
            if (DB.Context.Logins.Any(e => e.UserKey == model.UserKey && e.CampaignId == id))
            {
                var username = Membership.GetUser(model.UserKey).UserName;
                ModelState.AddModelError("UserKey", string.Format("Zugang für '{0}' bereits vorhanden.", username));
                return false;
            }
            return true;
        }

        [HttpPost]
        [GridAction]
        public virtual ActionResult InsertAjax(int id, FriendlyLoginModel model)
        {
            if(UserAllowed(model, id))
            {
                var login = new Login
                                {
                                    CampaignId = id,
                                    UserKey = model.UserKey,
                                    CampaignRoles = model.CampaignRoles
                                };
                DB.Context.Logins.Add(login);
                DB.Context.SaveChanges();
            }
            return SelectAjax(id);
        }

        [HttpPost]
        [GridAction]
        public virtual ActionResult UpdateAjax(int id, int loginId, FriendlyLoginModel model)
        {
            var login = DB.Context.Logins.Single(e => e.Id == loginId);
            if (login.UserKey == model.UserKey || UserAllowed(model, id))
            {
                login.UserKey = model.UserKey;
                login.CampaignRoles = model.CampaignRoles;
                DB.Context.SaveChanges();
            }
            return SelectAjax(id);
        }

        [HttpPost]
        [GridAction]
        public virtual ActionResult DeleteAjax(int id, int loginId)
        {
            var login = DB.Context.Logins.Single(e => e.Id == loginId);
            DB.Context.Logins.Remove(login);
            DB.Context.SaveChanges();

            return SelectAjax(id);
        }
        #endregion

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Link(int id)
        {
            var campaign = DB.Context.Campaigns.Single(e => e.Id == id);
            return View(campaign);
        }

        public virtual ActionResult Ended()
        {
            return View();
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Edit(int id)
        {
            ViewBag.CampaignId = id;
            var dbCampaign = id != -1
                ? DB.Context.Campaigns.Single(e => e.Id == id)
                : null;

            var viewModel = dbCampaign == null
                ? new CampaignViewModel()
                : Mapper.Map<Campaign, CampaignViewModel>(dbCampaign);

            if (Request.HttpMethod == "POST")
            {
                TryUpdateModel(viewModel);
                ModelState.Remove("InstanceUrl");
                if (ModelState.IsValid)
                {
                    if (DB.Context.Campaigns.Any(e => e.Id != id && e.CustomerId == viewModel.CustomerId && e.Number == viewModel.Number))
                    {
                        ModelState.AddModelError("Number", "Projektnummer ist bereits vergeben");
                    }
                    else
                    {
                        var isNew = false;
                        if (dbCampaign == null)
                        {
                            // New
                            isNew = true;
                            dbCampaign = Mapper.Map<CampaignViewModel, Campaign>(viewModel);
                            dbCampaign.UsesSqlReplication = true;
                            dbCampaign.Logins = new List<Login>();
                            dbCampaign.Logins.Add(new Login
                            {
                                Campaign = dbCampaign,
                                UserKey = viewModel.ManagerId,
                                CampaignRoles = 
                                    SeismitoadShared.Constants.Roles.Admin + ";" +
                                    SeismitoadShared.Constants.Roles.Analyst + ";" +
                                    SeismitoadShared.Constants.Roles.DownloadsEditor + ";" +
                                    SeismitoadShared.Constants.Roles.DownloadsViewer+ ";" +
                                    SeismitoadShared.Constants.Roles.NewsEditor+ ";" +
                                    SeismitoadShared.Constants.Roles.NewsReader + ";" +
                                    SeismitoadShared.Constants.Roles.ReportEditor + ";"
                            });
                            DB.Context.Campaigns.Add(dbCampaign);
                        }
                        else
                        {
                            // Update
                            var previousInstanceUrl = dbCampaign.InstanceUrl;

                            Mapper.Map(viewModel, dbCampaign);

                            if (viewModel.ExternalReportTool == false)
                            {
                                viewModel.ExternalReportToolUrl = null;
                                dbCampaign.InstanceUrl = previousInstanceUrl == null || !previousInstanceUrl.StartsWith("_ext_") ? previousInstanceUrl : null;
                            }
                        }
                        DB.Context.SaveChanges();
                        if(isNew)
                        {
                            BackgroundJob.Enqueue(() => NotifyAdvantageUsers(dbCampaign.Name, User.Identity.Name));
                        }
                        return View(MVC.Shared.Views.RedirectParentFrame, model: Url.Action(MVC.CampaignWizard.Step(dbCampaign.Id, 1)));
                    }
                }
            }
            viewModel.CreateCustomer = User.IsInRole(SeismitoadShared.Constants.Roles.Admin) 
                ? Url.Action(MVC.Customer.New())
                : Url.Action(MVC.Campaign.NotAllowed());
            return View(viewModel);
        }

        public virtual ActionResult NotAllowed()
        {
            return View();
        }

        public virtual JsonResult _Customers()
        {
            var model = DB.Context.Customers
                .OrderBy(e => e.Name)
                .Select(e => new
                {
                    e.Id,
                    e.Name
                });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult Upload(int id, string name)
        {
            var path = Server.MapPath("~/CampaignAssets/" + id + "/" + name);

            var uploadedFileExtension = Path.GetExtension(Request.Files[0].FileName).ToLower();
            if (uploadedFileExtension == ".jpeg") uploadedFileExtension = ".jpg";

            string desiredExtension;

            switch (name)
            {
                case "CampaignDetailInfo":
                    desiredExtension = ".pdf";
                    break;
                case "logo":
                    desiredExtension = ".png";
                    break;
                case "image":
                    desiredExtension = ".jpg";
                    break;
                default:
                    throw new ArgumentException("Invalid asset name.", nameof(name));
            }
            if (uploadedFileExtension != desiredExtension)
                return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable, $"Falsches Dateiformat. Bitte eine '{desiredExtension}'-Datei hochladen.");

            path += desiredExtension;

            Directory.CreateDirectory(Path.GetDirectoryName(path));
            Request.Files[0].SaveAs(path);

            // Gucken, ob es ein Bild ist, denn Bilder wollen wir skalieren.
            if (uploadedFileExtension != "pdf")
            {
                try
                {
                    var processStart = new ProcessStartInfo(Server.MapPath("~/App_Data/mogrify.exe"), $"-resize \"3200x3200>\" \"{path}\"")
                    {
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        LoadUserProfile = false,
                        RedirectStandardOutput = true
                    };
                    var process = Process.Start(processStart);
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit(20 * 1000);
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                }
            }

            return Content("");
        }

        [MenuArea(MenuArea.Project)]
#if ADVANTAGE
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin + "," + SeismitoadShared.Constants.Roles.Permedia)]
#else
        [Authorize(Roles = SeismitoadShared.Constants.Roles.User)]
#endif
        public virtual ActionResult Authorization(int id)
        {
            var isAdmin = User.IsInRole(SeismitoadShared.Constants.Roles.Admin);
            if (!isAdmin)
            {
                // Kein allgemeiner Admin, aber vielleicht Projekt-Admin
                var userkey = (Guid)Membership.GetUser(User.Identity.Name).ProviderUserKey;
                isAdmin = DB.Context.Logins.Any(e =>
                    e.CampaignId == id &&
                    e.UserKey == userkey &&
                    e.CampaignRoles.Contains(SeismitoadShared.Constants.Roles.Admin));
            }

            if (!isAdmin)
                return new HttpUnauthorizedResult();

#if ADVANTAGE
            var permedia = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Permedia);
            var advantage = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
            var users = permedia.Concat(advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            var customers = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Customer);
            var allUsers = users.Concat(customers).Select(Membership.GetUser);

            var items = (from user in allUsers
                where user.IsApproved
                select (Guid) user.ProviderUserKey
                into userKey
                let account = DB.Context.Accounts.SingleOrDefault(e => e.ProviderUserKey == userKey)
                where account != null
                let friendlyName = account.Firstname + " " + account.Lastname
                orderby friendlyName
                select new SelectListItem
                {
                    Text = friendlyName,
                    Value = userKey.ToString(),
                });

            ViewData["FriendlyNames"] = items;

            return View();
        }
    }
}
