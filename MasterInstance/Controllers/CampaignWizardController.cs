﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;

namespace MasterInstance.Controllers
{
    public partial class CampaignWizardController : Controller
    {
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Step(int campaignId, int step)
        {
            ActionResult actionResult;
            switch(step)
            {
                case 1:
                    actionResult = MVC.Campaign.Edit(campaignId);
                    break;
                case 2:
                    actionResult = MVC.Campaign.Authorization(campaignId);
                    break;
                case 3:
                    actionResult = MVC.CM.CampaignLocation.Index(campaignId);
                    break;
                case 4:
                    actionResult = MVC.Assignment.Index(campaignId, null);
                    break;
                case 5:
                    return RedirectToAction(MVC.CM.Assign.Index(campaignId));
                default:
                    actionResult = null;
                    break;
            }

            int maxStep;
            if (DB.Context.Assignments.Any(e => e.CampaignLocation.CampaignId == campaignId))
                maxStep = 5;
            else if (DB.Context.CampaignsLocations.Any(e => e.CampaignId == campaignId))
                maxStep = 4;
            else if(campaignId != -1)
                maxStep = 3;
            else 
                maxStep = 1;

            var model = new CampaignWizardModel
                            {
                                ActionResult = actionResult,
                                CampaignId = campaignId,
                                Step = step,
                                MaxStep = maxStep
                            };
            return View(model);
        }
    }
}
