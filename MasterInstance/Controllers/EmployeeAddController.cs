﻿using System;
using System.Data.Linq.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;

namespace MasterInstance.Controllers
{
    public partial class EmployeeAddController : Controller
    {
        //
        // GET: /EmployeeAdd/

        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Index(EmployeeAddModel model)
        {
            ModelState.Remove("Step");
            switch (model.Step)
            {
                case 0:
                    if (ModelState.IsValid)
                    {
                        var temp = DB.Context.Database.SqlQuery<Employee>(
                            @"SELECT * FROM Employees WHERE
    dbo.Similarity(Firstname, {0}, 0, 0.85, 0) >= 0.5 AND
	dbo.Similarity(Lastname, {1}, 0, 0.85, 0) >= 0.5", new[] {model.Firstname, model.Lastname});

                        //var temp = DB.Context.Employees.Where(e =>
                        //        SqlFunctions.Difference(model.Firstname, e.Firstname) >= 3 &&
                        //        SqlFunctions.Difference(model.Lastname, e.Lastname) >= 3);

                        var usersByEmail = Membership.FindUsersByEmail(model.Email).Cast<MembershipUser>().Select(e => (Guid)e.ProviderUserKey);
                        var moreEmployees = DB.Context.Employees.Where(e => usersByEmail.Contains(e.ProviderUserKey));

                        model.Employees = temp.Union(moreEmployees);

                        model.Step++;

                        if (Membership.GetUserNameByEmail(model.Email) != null)
                        {
                            ModelState.AddModelError("Email", "Es existiert bereits ein User mit dieser E-Mail-Adresse");
                            return View(model);
                        }

                        return View(model);
                    }
                    break;

                case 1:
                    if (Membership.GetUserNameByEmail(model.Email) != null)
                    {
                        ModelState.AddModelError("Email", "Es existiert bereits ein User mit dieser E-Mail-Adresse");
                        model.Step--;
                        return View();
                    }
                    model.Step++;
                    return View(model);

                case 2:
                        if (model.AddAllModel.Username != null && Membership.GetUser(model.AddAllModel.Username) != null)
                        {
                            ModelState.AddModelError("AddAllModel.Username", "Benutzername schon vergeben.");
                        }
                        if (!String.IsNullOrWhiteSpace(Membership.PasswordStrengthRegularExpression) && !Regex.IsMatch(model.AddAllModel.Password, Membership.PasswordStrengthRegularExpression))
                        {
                            ModelState.AddModelError("AddAllModel.Password", String.Format("Passwort ungültig."));
                                //Folgende Eigenschaften sind erforderlich: {0}", Membership.PasswordStrengthRegularExpression));
                        }
                        if (Membership.MinRequiredNonAlphanumericCharacters > 0)
                        {
                            if (Regex.Matches(model.AddAllModel.Password, "[^a-zA-Z0-9]").Count > Membership.MinRequiredNonAlphanumericCharacters)
                                ModelState.AddModelError("AddAllModel.Password",
                                                         String.Format(
                                                             "Zu wenig Sonderzeichen. Mindestens {0} Sonderzeichen.",
                                                             Membership.MinRequiredNonAlphanumericCharacters));
                        }
                        if (model.AddAllModel.Password != null && model.AddAllModel.Password.Length < Membership.MinRequiredPasswordLength)
                        {
                            ModelState.AddModelError("AddAllModel.Password",
                                                     String.Format("Passwort zu kurz. Mindestens {0} Zeichen.",
                                                                   Membership.MinRequiredPasswordLength));
                        }
                        if (ModelState.IsValid && CreateNewUser(model))
                        {
                            ModelState.Clear();
                            return View();
                        }

                    return View(model);
            }

            return View();
        }


        private bool CreateNewUser(EmployeeAddModel model)
        {
            MembershipCreateStatus createStatus;
            Membership.CreateUser(model.AddAllModel.Username, model.AddAllModel.Password, model.Email, null, null, true, out createStatus);

            if (createStatus == MembershipCreateStatus.Success)
            {
                var finalUsername = model.AddAllModel.Username;

                Roles.AddUserToRoles(finalUsername, new[]
                                                        {
                                                            SeismitoadShared.Constants.Roles.Reporter,
                                                            SeismitoadShared.Constants.Roles.NewsReader,
                                                            SeismitoadShared.Constants.Roles.DownloadsViewer
                                                        });

                var employee = new Employee { Firstname = model.Firstname, Lastname = model.Lastname, Title = model.EmployeeTitle, State = model.AddAllModel.State };

                employee.ProviderUserKey = (Guid)Membership.GetUser(finalUsername).ProviderUserKey;
                DB.Context.Employees.Add(employee);
                DB.Context.SaveChanges();
                TempData["CreatedUser"] = new EmployeeCreatedModel
                {
                    MembershipUser = Membership.GetUser(finalUsername),
                    Password = model.AddAllModel.Password,
                    Employee = employee
                };

                return true;
            }

            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateEmail:
                    ModelState.AddModelError("Email", "Es existiert bereits ein User mit dieser E-Mail-Adresse (Beachten Sie die Tabelle auf der vorherigen Seite!)");
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    ModelState.AddModelError("AddAllModel.Username", "Es existiert bereits ein User mit diesem Namen (Beachten Sie die Tabelle auf der vorherigen Seite!)");
                    break;
                case MembershipCreateStatus.InvalidEmail:
                    ModelState.AddModelError("Email", "Ungültige E-Mail-Adresse");
                    break;
                case MembershipCreateStatus.InvalidPassword:
                    ModelState.AddModelError("AddAllModel.Password", "Ungültiges Passwort");
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    ModelState.AddModelError("AddAllModel.Username", "Ungültiger Benutzername");
                    break;
                default:
                    ModelState.AddModelError("", createStatus.ToString());
                    break;
            }

            return false;
        }
    }
}
