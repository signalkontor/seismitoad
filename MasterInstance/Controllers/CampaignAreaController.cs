﻿using System.Linq;
using System.Web.Mvc;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Controllers;

namespace MasterInstance.Controllers
{
    public partial class CampaignAreaController : CreateUpdateDeleteController<CampaignArea>
    {
        public CampaignAreaController() : base(DB.Context, DB.Context.CampaignAreas)
        {
        }

        public override ActionResult Index(int? locationId)
        {
            var location = DB.Context.Locations.Single(e => e.Id == locationId);
            ViewBag.Location = location;
            return View(location.CampaignAreas);
        }
    }
}
