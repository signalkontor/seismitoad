﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadShared.Constants;
using SeismitoadShared.Controllers;
using ObjectTemplate.Extensions;

namespace MasterInstance.Controllers
{
    public partial class LocationController : Controller
    {
        //********* vCard Bereitstellungs-Funktion ******************************************************//
        //public virtual ActionResult GetVCard(int id)
        //{
        //    var location = DB.Context.Locations.Single(l => l.Id == id);
        //    Response.AddHeader("Content-Disposition", "attachment; filename=\""+location.Name+".vcf\"");
        //    return Content(location.ToVCardString(),"text/vcard",Encoding.GetEncoding("windows-1257"));
        //}

        [Authorize(Roles = Roles.LocationEditor + "," + Roles.Admin)]
        public virtual ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = Roles.LocationEditor + "," + Roles.Admin)]
        public virtual ActionResult Create(Location model, bool? ignoreSimilarLocations)
        {
            if (ignoreSimilarLocations != true && !string.IsNullOrWhiteSpace(model.Name) && !string.IsNullOrWhiteSpace(model.PostalCode))
            {
                // Leider können wir nicht direkt SqlQuery<Location>() verwenden, da wir in der Location-Tabelle
                // die Spalten LocationGroup_Id und StateProvince_Id haben, die zugehörigen Eigenschaften der
                // Location-Klasse heißen aber LocationGroupId und StateProvinceId (ohne Underscore).
                // Leider ignoriert SqlQuery das Mapping (egal ob per Attribut oder Fluent-API).
                var similarLocationIds = DB.Context.Database.SqlQuery<int>(@"SELECT Id FROM Locations WHERE
       dbo.Similarity(Name, {0}, 0, 0.85, 0) >= 0.5 AND
       dbo.Similarity(PostalCode, {1}, 0, 0.85, 0) >= 0.6", new [] { model.Name, model.PostalCode } );
                if (similarLocationIds.Any())
                {
                    if (ModelState.IsValid)
                        ViewBag.ShowSimilarLocationsCheckBox = true; // Nur wenn es sonst keine Fehler gibt die Checkbox anzeigen

                    ModelState.AddModelError("Name", "Ähnliche Location(s) vorhanden.");
                    ModelState.AddModelError("PostalCode", "Ähnliche Location(s) vorhanden.");
                    var similarLocations = DB.Context.Locations.Join(similarLocationIds, location => location.Id, i => i, (location, i) => location);
                    ViewBag.SimiliarLocations = similarLocations;
                }
            }

            if (ModelState.IsValid)
            {
                model.UpdateCoordinates();
                DB.Context.Locations.Add(model);
                DB.Context.SaveChanges();
                return RedirectToAction(MVC.MD.Location.Index());
            }
            return View(model);
        }

        [Authorize(Roles = Roles.Signalkontor)]
        public virtual ActionResult Import(HttpPostedFileBase file)
        {
            if(Request.RequestType == "POST")
            {
                var name = Path.GetTempFileName();
                file.SaveAs(name);
                var locationImportService = new LocationImportService(name);
                const string encoding = "windows-1250";
                var importLines = locationImportService.Import(encoding);
                var dataGuid = Guid.NewGuid();
                Session[dataGuid.ToString()] = importLines;
                System.IO.File.Delete(name);
                return RedirectToAction(MVC.Location.ImportPreview(dataGuid));
            }
            return View();
        }

        [Authorize(Roles = Roles.Signalkontor)]
        public virtual ActionResult ImportPreview(Guid id)
        {
            var importLines = Session[id.ToString()] as IEnumerable<ImportLine<Location>>;
            return View(importLines);
        }

        [Authorize(Roles = Roles.Signalkontor)]
        public virtual ActionResult FinishImport(Guid id, string action)
        {
            switch(action)
            {
                case "revert":
                    Session.Remove(id.ToString());
                    return RedirectToAction(MVC.Location.Import());
                case "commit":
                    LocationImportService.Commit(Session[id.ToString()] as IEnumerable<ImportLine<Location>>);
                    Session.Remove(id.ToString());
                    return RedirectToAction(MVC.Home.Index());
                default:
                    throw new ArgumentException("Invalid value for parameter 'action'", "action");
            }
        }

        public virtual ActionResult CardPreview(int id)
        {
            return View(id);
        }

        public virtual ActionResult Card(int id)
        {
#if ADVANTAGE
            ViewBag.Hidden = !System.Web.HttpContext.Current.User.HasAnyRole(Roles.Permedia, Roles.Signalkontor);
#else
            ViewBag.Hidden = !System.Web.HttpContext.Current.User.HasAnyRole(Roles.User, Roles.Signalkontor);
#endif
            return View(DB.Context.Locations.Single(e => e.Id == id));
        }

        // Lesend darf jeder auf Location/Edit zugreifen, nur der POST s.u. ist den Admin vorbehalten.
        public virtual ActionResult Edit(int id)
        {
            return View(DB.Context.Locations.Single(e => e.Id == id));
        }

        [HttpPost]
        [Authorize(Roles = Roles.LocationEditor + "," + Roles.Admin)]
        public virtual ActionResult Edit(int id, string dummy)
        {
            var location = DB.Context.Locations.Single(e => e.Id == id);
            var street = location.Street;
            var postalCode = location.PostalCode;
            var city = location.City;
            if (TryUpdateModel(location))
            {
                if (street != location.Street || postalCode != location.PostalCode || city != location.City)
                {
                    // Geokoordinaten resetten, damit sie vom Job neu berechnet werden.
                    location.GeographicCoordinates = null;
                    location.GeocodingFailed = false;
                }
                DB.Context.SaveChanges();
                return RedirectToAction(MVC.MD.Location.Index());
            }
            return View(location);
        }
    }
}
