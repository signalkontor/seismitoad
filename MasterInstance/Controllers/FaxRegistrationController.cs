﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rebex.Net;
using Rebex.Mail;

namespace MasterInstance.Controllers
{
    public class FaxRegistrationController : Controller
    {
        //
        // GET: /FaxRegistration/
        /// <summary>
        /// Wird periodisch über einen Timer Event aufgerufen und prüft neu eingegangene Faxe.
        /// Kann die Anmeldung zugeordnet werden, wird der Promoter eingebucht. Andernfalls wird
        /// das Fax in eine Liste für abzuarbeitende Anmeldugen eingetragen
        /// Hauptmerkmal bei der Zuordnung ist hierbei die Absendenummer.
        /// Faxe laufen als Email mit Anhang ein.
        /// </summary>
        /// <returns>Statuscode für das Loggin von Events</returns>
        public ActionResult Index()
        {
            var messages = new List<string>();
            var config = SeismitoadShared.Models.SeismitoadFaxConfigurationSection.Current;
            try
            {
                Pop3 client = new Pop3();
                client.Settings.SslAcceptAllCertificates = true;
                client.Connect(config.Server, config.Port, SslMode.None);
                client.Login(config.User, config.Password);
                var list = client.GetMessageList();

                foreach (var msg in list)
                {
                    try
                    {
                        if (!ProcessHeader(msg.Subject, msg.Sender.Address))
                        {
                            // Diese Nachricht muss manuel bearbeitet werden.
                            // Wir erzeugen eine Warnung

                            // Und speichern das Fax (den Anhang)
                            var message = client.GetMailMessage(msg.SequenceNumber);
                            // Wir suchen unseren Anhang heraus:
                            foreach (var attachemend in message.Attachments)
                            {
                                if (attachemend.FileName.ToLower().EndsWith(".pdf"))
                                {
                                    // Gefunden:
                                    /// ToDo: richtigen Pfad setzten
                                    string filename = msg.SequenceNumber.ToString() + "_" + attachemend.FileName;
                                    attachemend.Save(filename);
                                    // Datenbankeintrag schreiben:
                                    var unprocessed = new SeismitoadModel.UnprocessedFax
                                        {
                                            Attachemend = filename,
                                            Processed = false,
                                            Sender = msg.Sender.Address,
                                            Subject = msg.Subject
                                        };
                                    DB.Context.UnprocessedFaxes.Add(unprocessed);
                                    DB.Context.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messages.Add(ex.Message);
                        var e = ex.InnerException;
                        while (e != null)
                        {
                            messages.Add(e.Message);
                            e = e.InnerException;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add(ex.Message);
                var e = ex.InnerException;
                while (e != null)
                {
                    messages.Add(e.Message);
                    e = e.InnerException;
                }
            }

            var str = messages.Aggregate((a, b) => a + "\r\n" + b);

            return Content(str);
        }

        private bool ProcessHeader(string header, string sender)
        {
            // ToDo: Parse Header, get Sender Fax Number and search for location
            // Fax Nummer der Location könnte in verschiedenen Formaten vorliegen.S
            var senderNoFormat1 = "+49";
            var senderNoFormat2 = "040...";

            var location =
                DB.Context.Locations.SingleOrDefault(l => l.Fax == senderNoFormat1 || l.Fax == senderNoFormat2);
            {
                if (location == null) return false;
                // Can we find an Assignment for this day:
                var assignment =
                    DB.Context.Assignments.SingleOrDefault(
                        a => a.CampaignLocation.Location.Id == location.Id && a.DateStart.Date == DateTime.Now.Date);

                if (assignment == null) return false; // Nicht gefunden, also false zurück geben und manuelle Bearbeitung

                assignment.State = SeismitoadModel.AssignmentState.PositionActive;

                DB.Context.SaveChanges();

            }

        }

    }
}
