﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;
using Telerik.Web.Mvc.Extensions;

namespace MasterInstance.Controllers
{
    public partial class AssignmentController
    {
        public const int PageSize = 20;
        public const string GridName = "c";

        #region Index (Create/Update/Delete Assignments)
        public virtual ActionResult Index(int campaignId, AssigmentsCreateModel model)
        {
            int page, size;
            string orderBy, groupBy, filter;
            this.TelerikGridRequestParameters(GridName, PageSize, out page, out size, out orderBy, out groupBy, out filter);
            var campaignLocations = DB.Context.CampaignsLocations.Where(e => e.CampaignId == campaignId);
            if (!string.IsNullOrWhiteSpace(filter))
            {
                var matches = Other.LargestCampaignAreaRegex.Match(filter);
                if (matches.Success)
                {
                    filter = filter.Replace(matches.Value, ""); // remove from filter so that ToFilterModel does not complain.
                    var operation = matches.Groups[2].Value;
                    var value = Convert.ToInt32(matches.Groups[3].Value);
                    switch (operation)
                    {
                        case "eq":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) == value);
                            break;
                        case "ge":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) >= value);
                            break;
                        case "gt":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) > value);
                            break;
                        case "le":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) <= value);
                            break;
                        case "lt":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) < value);
                            break;
                        case "ne":
                            campaignLocations = campaignLocations.Where(e => e.Location.CampaignAreas.Max(i => i.Size) != value);
                            break;
                    }
                }
            }

            model = new AssigmentsCreateModel
            {
                CampaignId = campaignId,
                CampaignClassification = DB.Context.Campaigns.Where(e => e.Id == campaignId).Select(e => e.Classification).Single(),
                Properties = Properties.Select(e => new SelectListItem { Value = e.Key, Text = e.Value.Value }).ToArray(),
                VisibleProperties = VisibleProperties,
                GridModel = campaignLocations.ToGridModel(page, size, orderBy, groupBy, filter),
                AssignmentDateGeneratorModel = model.AssignmentDateGeneratorModel ?? new AssignmentDateGeneratorModel()
            };

            model.AssignmentDateGeneratorModel.CalculateDates();

            foreach (var selectListItem in model.Properties)
            {
                selectListItem.Selected = model.VisibleProperties.Contains(selectListItem.Value);
                if (selectListItem.Selected && selectListItem.Value == "SalesRegion")
                {
                    ViewData["SalesRegionItems"] = DB.Context.CampaignsLocations
                        .Where(e => e.CampaignId == campaignId)
                        .Select(e => new {e.SalesRegion})
                        .Distinct()
                        .Select(e => new SelectListItem {Text = e.SalesRegion, Value = e.SalesRegion});
                }
            }

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(int campaignId, int? locationId, AssignmentEditAction action, AssigmentsCreateModel model)
        {
            model.AssignmentDateGeneratorModel?.CalculateDates();
            switch (action)
            {
                case AssignmentEditAction.InsertForAll:
                    AddAssignmentsForAllFilteredLocations(campaignId, model.AssignmentDateGeneratorModel?.Dates ?? new Day[0]);
                    break;
                case AssignmentEditAction.Insert:
                    AddAssignmentsForSingleLocation(campaignId, locationId, model.AssignmentDateGeneratorModel?.Dates ?? new Day[0]);
                    break;
                case AssignmentEditAction.Delete:
                    var assignmentsToDelete = DB.Context.Assignments
                        .Where(e => e.CampaignLocation.CampaignId == campaignId && e.CampaignLocation.LocationId == locationId && !e.AssignedEmployees.Any());
                    foreach (var assignment in assignmentsToDelete.ToList())
                    {
                        assignment.State = AssignmentState.Deleted;
                        assignment.MarkForSync();
                    }
                    DB.Context.SaveChanges();
                    break;
            }
            return RedirectToAction(MVC.Assignment.Index().AddRouteValues(this.CleanRouteValues()));
        }

        private static void AddAssignments(IEnumerable<Assignment> assignments)
        {
            foreach (var assignment in assignments)
            {
                assignment.MarkForSync();
                DB.Context.Assignments.Add(assignment);
            }
            DB.Context.SaveChanges();
        }

        private static void AddAssignmentsForSingleLocation(int campaignId, int? locationId, Day[] dates)
        {
            var campaignLocation = DB.Context.CampaignsLocations.Single(e => e.CampaignId == campaignId && e.LocationId == locationId);
            AddAssignments(dates.Select(day =>
            {
                var start = day.Start.GetValueOrDefault();
                var end = day.End.GetValueOrDefault();
                // KEINE automatische Anpassung der Aktionszeiten mehr (http://redmine.advantage-promotion.de/redmine/issues/467)
                //campaignLocation.Location.OpeningHours.BestOpeningHours(ref start, ref end); 
                return new Assignment
                {
                    State = AssignmentState.Planned,
                    EmployeeCount = day.EmployeeCount ?? 0,
                    DateStart = start,
                    DateEnd = end,
                    CampaignLocation = campaignLocation
                };
            }));
        }

        private void AddAssignmentsForAllFilteredLocations(int campaignId, Day[] dates)
        {
            int page, size;
            string orderBy, groupBy, filter;
            this.TelerikGridRequestParameters(GridName, PageSize, out page, out size, out orderBy, out groupBy, out filter);
            var allCampaignLocations = DB.Context.CampaignsLocations.Where(e => e.CampaignId == campaignId);
            var gridModel = allCampaignLocations.ToGridModel(0, allCampaignLocations.Count(), orderBy, groupBy, filter);
            var campaignLocations = gridModel.Data as IEnumerable<CampaignLocation>;
            if (campaignLocations != null)
            {
                AddAssignments(dates.Join(campaignLocations,
                                          day => 1,
                                          campaignLocation => 1,
                                          (day, campaignLocation) =>
                                          {
                                              var start = day.Start.GetValueOrDefault();
                                              var end = day.End.GetValueOrDefault();
                                              campaignLocation.Location.OpeningHours.BestOpeningHours(ref start, ref end);
                                              return new Assignment
                                              {
                                                  State = AssignmentState.Planned,
                                                  EmployeeCount = day.EmployeeCount ?? 0,
                                                  DateStart = day.Start.GetValueOrDefault(),
                                                  DateEnd = day.End.GetValueOrDefault(),
                                                  CampaignLocation = campaignLocation,
                                              };
                                          }));
            }
        }

        #endregion
      
    }
}