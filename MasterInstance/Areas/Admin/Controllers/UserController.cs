﻿using System;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Glimpse.AspNet;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.Admin.Models;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Extensions;
using MasterInstance.Services;

namespace MasterInstance.Areas.Admin.Controllers
{
    [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin)]
    public partial class UserController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public UserController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = _dbContext.Accounts.Project().To<UserAccountViewModel>().ToList();
            foreach (var userAccountViewModel in model)
            {
                var membershipUser = Membership.GetUser(userAccountViewModel.ProviderUserkey, false);
                if (membershipUser != null)
                {
                    Mapper.Map(membershipUser, userAccountViewModel);
#if ADVANTAGE
                    if (userAccountViewModel.AccountType != "Kunde")
                    {
                        userAccountViewModel.AccountType = Roles.IsUserInRole(membershipUser.UserName,
                            SeismitoadShared.Constants.Roles.Permedia)
                            ? "PER MEDIA"
                            : "advantage";
                    }
#endif
                }
                else
                {
                    userAccountViewModel.UserName = "Gelöschter Benutzer";
                }
            }
            return Json(model.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult Edit(UserViewModel model)
        {
#region Update des Account
            if (ModelState.IsValid)
            {
                var account = DB.Context.Accounts.Single(e => e.ProviderUserKey == model.ProviderUserkey);
                account.Title = model.Title;
                account.Firstname = model.Firstname;
                account.Lastname = model.Lastname;
                if (account is CustomerAccount)
                {
                    (account as CustomerAccount).CustomerId = model.CustomerId.Value;
                }
                DB.Context.SaveChanges();
            }
#endregion

#region Update des Membership Users
            var user = Membership.GetUser(model.ProviderUserkey);
            user.Email = model.Email;
            user.IsApproved = model.IsActive;

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                user.UnlockUser();
                var resetPassword = user.ResetPassword();
                user.ChangePassword(resetPassword, model.Password);                
            }

            try
            {
                Membership.UpdateUser(user);
            }
            catch (ProviderException)
            {
                ModelState.AddModelError("Email", "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
            }

            // Usernamen Änderung als letztes vornehmen, da wir dies am Provider vorbei direkt in der Datenbank machen
            if (!model.UserName.Equals(user.UserName, StringComparison.OrdinalIgnoreCase))
            {
                if (!MembershipService.ChangeUsername(model.ProviderUserkey, model.UserName))
                {
                    ModelState.AddModelError("UserName", "Benutzername ist bereits vergeben.");
                }
            }
#endregion

            return ModelState.IsValid ? (ActionResult) RedirectToAction("Index", this.GridRouteValues()) : Index();
        }

        [HttpPost]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, UserAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
#region Update des Account
                var account = _dbContext.Accounts.Single(e => e.ProviderUserKey == model.ProviderUserkey);
                Mapper.Map(model, account);
                if (account is CustomerAccount && model.CustomerId.HasValue)
                {
                    (account as CustomerAccount).CustomerId = model.CustomerId.Value;
                }
                _dbContext.SaveChanges();
#endregion

#region Update des Membership Users
                var user = Membership.GetUser(model.ProviderUserkey);
                user.Email = model.Email;
                user.IsApproved = "True".Equals(model.Active, StringComparison.OrdinalIgnoreCase);
                try
                {
                    Membership.UpdateUser(user);
                }
                catch (ProviderException)
                {
                    ModelState.AddModelError("Email",
                        "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
                }

                if (ModelState.IsValid)
                {
                    // Usernamen Änderung als letztes vornehmen, da wir dies am Provider vorbei direkt in der Datenbank machen
                    if (!model.UserName.Equals(user.UserName, StringComparison.OrdinalIgnoreCase))
                    {
                        if (!MembershipService.ChangeUsername(model.ProviderUserkey, model.UserName))
                        {
                            ModelState.AddModelError("UserName", "Benutzername ist bereits vergeben.");
                        }
                    }
                }
            }
#endregion

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, UserAccountViewModel model)
        {
            ModelState.Remove("ProviderUserkey");
            if (ModelState.IsValid)
            {
                MembershipCreateStatus membershipCreateStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null,
                    "True".Equals(model.Active, StringComparison.OrdinalIgnoreCase), out membershipCreateStatus);

                if (membershipCreateStatus == MembershipCreateStatus.Success)
                {
                    var newAccount = model.AccountType == "Kunde"
                        ? (Account) new CustomerAccount()
                        : (Account) new AdminAccount();
                    Mapper.Map(model, newAccount);
                    var membershipUser = Membership.GetUser(model.UserName);
                    newAccount.ProviderUserKey = (Guid) membershipUser.ProviderUserKey;
                    // Jetzt mappen wir noch die Datem vom neuen MembershipUser auf unser ViewModel 
                    // Das machen wir eigentlich nur, damit LastActivityDate und LastLoginDate im ViewModel gesetzt sind.
                    Mapper.Map(membershipUser, model);
                    if (newAccount is CustomerAccount && model.CustomerId.HasValue)
                    {
                        Roles.AddUserToRole(model.UserName, SeismitoadShared.Constants.Roles.Customer);
                        (newAccount as CustomerAccount).CustomerId = model.CustomerId.Value;
                    }
                    else
                    {
#if ADVANTAGE
                        switch (model.AccountType)
                        {
                            case "PER MEDIA":
                                Roles.AddUserToRole(model.UserName, SeismitoadShared.Constants.Roles.Permedia);
                                break;
                            case "advantage":
                                Roles.AddUserToRole(model.UserName, SeismitoadShared.Constants.Roles.Advantage);
                                break;
                        }
#else
                        Roles.AddUserToRole(model.UserName, SeismitoadShared.Constants.Roles.User);
#endif
                    }
                    _dbContext.Accounts.Add(newAccount);
                    try
                    {
                        _dbContext.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        Membership.DeleteUser(model.UserName, true);
                    }
                }
                else
                {
                    switch (membershipCreateStatus)
                    {
                        case MembershipCreateStatus.DuplicateEmail:
                            ModelState.AddModelError("Email", "Es existiert bereits ein User mit dieser E-Mail-Adresse");
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            ModelState.AddModelError("UserName", "Es existiert bereits ein User mit diesem Namen");
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            ModelState.AddModelError("Email", "Ungültige E-Mail-Adresse");
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            ModelState.AddModelError("Password", "Ungültiges Passwort");
                            break;
                        case MembershipCreateStatus.InvalidUserName:
                            ModelState.AddModelError("UserName", "Ungültiger Benutzername");
                            break;
                        default:
                            ModelState.AddModelError("", membershipCreateStatus.ToString());
                            break;
                    }
                }
            }
            return Json(new[] {model}.ToDataSourceResult(request, ModelState));
        }
    }
}
