﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.Admin.Controllers
{
    public partial class ConfirmationsController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public ConfirmationsController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: Admin/Confirmations
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = _dbContext.ConfirmationEmails;
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult Download(int employeeId, string filename)
        {
            return File(Server.MapPath($"~/ConfirmationMails/{employeeId}/{filename}"), "message/rfc822", filename);
        }
    }
}