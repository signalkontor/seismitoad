﻿using System.Net.Mail;
using System.Reflection;
using System.Web.Mvc;
using System.Linq;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadShared.Extensions;
using SeismitoadShared.Controllers;

namespace MasterInstance.Areas.Admin.Controllers
{
    public partial class SignalkontorController : SignalkontorControllerBase
    {
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Signalkontor)]
        public virtual ActionResult Index()
        {
            ViewBag.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Signalkontor)]
        public virtual ActionResult CreateLoginsForAllCampaigns()
        {
            var providerUserKey = User.GetProviderUserKey();
            var campaignsToAdd = DB.Context.Campaigns.Where(e => e.Logins.All(i => i.UserKey != providerUserKey));
            foreach (var campaign in campaignsToAdd)
            {
                campaign.Logins.Add(new Login { Campaign = campaign, UserKey = providerUserKey });
            }
            DB.Context.SaveChanges();
            return RedirectToAction(ActionNames.Index);
        }

        public virtual ActionResult SendTestMail()
        {
            var mail = new MailMessage
            {
                Subject = "seismitoad Testmail",
                Body = "Testmail from seismitoad"
            };
            mail.To.Add("mikko.jania@signalkontor.com");
            new SmtpClient().Send(mail);
            return Content("Mail gesendet");
        }

        public virtual ActionResult CollectGarbage()
        {
            System.GC.Collect();
            return RedirectToAction(ActionNames.Index);
        }

        public virtual ActionResult FaxTest()
        {
            FaxReceptionService.ReceiveFaxes();
            return RedirectToAction(ActionNames.Index);
        }
    }
}
