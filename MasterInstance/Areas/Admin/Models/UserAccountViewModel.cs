﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Security;
using Autofac.Features.OpenGenerics;
using AutoMapper;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using SeismitoadShared.Attributes;

namespace MasterInstance.Areas.Admin.Models
{
    public class UserAccountViewModel
    {
        [Visibility(ShowForDisplay = false, ShowForEdit = false)]
        public Guid ProviderUserkey { get; set; }

        [Required]
        [Display(Name = "Anrede")]
        [UIHint("SingleSelectKendo")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Vorname")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Nachname")]
        public string Lastname { get; set; }

        [Required]
        [Display(Name = "Benutzername")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Zugangsart")]
        [UIHint("SingleSelectKendo")]
        public string AccountType { get; set; }

        [Display(Name = "Kunde")]
        [UIHint("SingleSelectKendo")]
        [EnumerationName("CustomerId")]
        [CustomValidation(typeof(UserAccountViewModel), "CustomerIdValidation")]
        public int? CustomerId { get; set; }

        public static ValidationResult CustomerIdValidation(int? value, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance as UserAccountViewModel;
            return value.HasValue || model.AccountType != "Kunde"
                ? ValidationResult.Success
                : new ValidationResult("Bitte einen Kunden auswählen.");
        }

        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public DateTime? LastLoginDate { get; set; }

        [Visibility(ShowForDisplay = true, ShowForEdit = false)]
        public DateTime? LastActivityDate { get; set; }

        [Required]
        [Display(Name = "Zugang aktiv")]
        [EnumerationName("YesNo")]
        [UIHint("SingleSelectKendo")]
        public string Active { get; set; }
    }

    public class UserAccountViewModelAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Account, UserAccountViewModel>()
#if ADVANTAGE
                .ForMember(e => e.AccountType, opt => opt.MapFrom(e => e is CustomerAccount ? "Kunde" : "PM oder AD"))
#else
                .ForMember(e => e.AccountType, opt => opt.MapFrom(e => e is CustomerAccount ? "Kunde" : "Benutzer"))
#endif
                .ForMember(e => e.CustomerId, opt => opt.MapFrom(e => (e as CustomerAccount).CustomerId));
            Mapper.CreateMap<MembershipUser, UserAccountViewModel>()
                .ForMember(e => e.Active, opt => opt.MapFrom(e => e.IsApproved.ToString()))
                .ForMember(e => e.Password, opt => opt.Ignore());
            Mapper.CreateMap<UserAccountViewModel, Account>();
        }
    }
}