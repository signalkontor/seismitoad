﻿using System.ComponentModel.DataAnnotations;
using AdvantageModel.Models;
using AutoMapper;

namespace MasterInstance.Areas.Migration.Models
{
    public class doaLoginViewModel
    {
        public const string Contact = "Kontaktdaten";
        public const string Login = "Login";
        public const string Qualification = "Qualifikation";
        public const string Profile = "Profil";

        public int id { get; set; }
        public string ds_status { get; set; }
        public System.DateTime changed_time { get; set; }

        #region Login
        [Display(Name = "Benutzername", Description = Login)]
        public string login_name { get; set; }

        [Display(Name = "Passwort", Description = Login)]
        public string password { get; set; }

        [Display(Name = "Kontofreigabe", Description = Login)]
        [UIHint("Bool")]
        public string loginfreigabe { get; set; }

        [Display(Name = "Rolle", Description = Login)]
        public string rolle { get; set; }

        [Display(Name = "Benutzertyp", Description = Login)]
        public string user_typ { get; set; }

        [Display(Name = "Erstes Einlogdatum", Description = Login)]
        [UIHint("Date")]
        public System.DateTime first_login { get; set; }

        [Display(Name = "Letztes Einlogdatum", Description = Login)]
        [UIHint("Date")]
        public System.DateTime last_login { get; set; }

        [Display(Name = "Fragebogen überspringen", Description = Login)]
        [UIHint("Bool")]
        public int skip_fragebogen { get; set; }

        [Display(Name = "Promoter ID", Description = Login)]
        public int? employeeId { get; set; }
        #endregion

        #region Kontaktdaten
        [Display(Name = "Nachname", Description = Contact)]
        public string nachname { get; set; }

        [Display(Name = "Vorname", Description = Contact)]
        public string vorname { get; set; }

        [Display(Name = "Geburtsdatum", Description = Contact)]
        [UIHint("Date")]
        public System.DateTime geburtsdatum { get; set; }

        [Display(Name = "Geschlecht", Description = Contact)]
        [UIHint("Gender")]
        public string geschlecht { get; set; } /* m=männlich, w=weiblich */

        /* Statuscodes
         * 1 Aktiv
         * 2 Neu
         * 3 Inaktiv
         * 4 Fremdpersonal
         * 5 Gesperrt
         */
        [Display(Name = "Benutzerstatus", Description = Contact)]
        [UIHint("UserStatus")]
        public string userstatus { get; set; }

        [Display(Name = "Bemerkung", Description = Contact)]
        public string userstatus_bemerkung { get; set; }

        [Display(Name = "Straße", Description = Contact)]
        public string strasse { get; set; }

        [Display(Name = "Postleitzahl", Description = Contact)]
        public string plz { get; set; }

        [Display(Name = "Ort", Description = Contact)]
        public string ort { get; set; }

        [Display(Name = "Land", Description = Contact)]
        public string land { get; set; }

        [Display(Name = "Vorwahl 1", Description = Contact)]
        public string tel_vorwahl { get; set; }

        [Display(Name = "Rufnummer 1", Description = Contact)]
        public string tel_nummer { get; set; }

        [Display(Name = "Fax-Vorwahl", Description = Contact)]
        public string fax_vorwahl { get; set; }

        [Display(Name = "Fax-Rufnummer", Description = Contact)]
        public string fax_nummer { get; set; }

        [Display(Name = "Mobil-Vorwahl", Description = Contact)]
        public string mobil_vorwahl { get; set; }

        [Display(Name = "Mobil-Rufnummer", Description = Contact)]
        public string mobil_nummer { get; set; }

        [Display(Name = "E-Mail 1", Description = Contact)]
        public string email1 { get; set; }

        [Display(Name = "E-Mail 2", Description = Contact)]
        public string email2 { get; set; }

        [Display(Name = "Webseite", Description = Contact)]
        public string homepage { get; set; }

        [Display(Name = "Gesundheitszeugnis", Description = Contact)]
        [UIHint("Bool")]
        public string gesundheitszeugnis { get; set; }

        [Display(Name = "Kopie vorhanden", Description = Contact)]
        [UIHint("Bool")]
        public string gesundheitszeugnis_kopie { get; set; }

        [Display(Name = "Gesundheitszeugnis Dateiname", Description = Contact)]
        public string gesundheitszeugnis_dateiname { get; set; }

        [Display(Name = "Gewerbeschein", Description = Contact)]
        [UIHint("Bool")]
        public string gewerbeschein { get; set; }

        [Display(Name = "Kopie vorhanden", Description = Contact)]
        [UIHint("Bool")]
        public string gewerbeschein_kopie { get; set; }

        [Display(Name = "Gewerbeschein Dateiname", Description = Contact)]
        public string gewerbeschein_dateiname { get; set; }

        [Display(Name = "Führerscheinklasse", Description = Contact)]
        public string fs_klasse { get; set; }

        [Display(Name = "Führerschein seit", Description = Contact)]
        [UIHint("Date")]
        public System.DateTime fs_seit_date { get; set; }

        [Display(Name = "Bemerkungen", Description = Contact)]
        public string fs_bemerkung { get; set; }

        [Display(Name = "Eigener PKW", Description = Contact)]
        [UIHint("Bool")]
        public string eigener_pkw { get; set; }

        [Display(Name = "Rahmenvertrag (alt)", Description = Contact)]
        [UIHint("Bool")]
        public string rahmenvertrag { get; set; }

        [Display(Name = "Rahmenvertrag (neu)", Description = Contact)]
        [UIHint("Bool")]
        public string rahmenvertrag_neu { get; set; }

        [Display(Name = "Selbstauskunft", Description = Contact)]
        [UIHint("Bool")]
        public short? selbstauskunft { get; set; }
        #endregion

        #region Qualifikation
        [Display(Name = "Englisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_en { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Französisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_fr { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Spanisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_sp { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Italienisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_it { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Türkisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_tr { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Polnisch", Description = Qualification)]
        [UIHint("Knowledge")]
        public string sprache_pl { get; set; } /* 1=fliessend, 2=Fortgeschritten, 3=Grundkenntnisse */

        [Display(Name = "Sonstige Sprachen", Description = Qualification)]
        public string sprache_sonstige { get; set; } /* Freitext */

        [Display(Name = "Sonstige Bemerkungen", Description = Qualification)]
        public string sprache_bemerkung { get; set; } /* Wird anscheinend nicht angezeigt */ // MIGRATION Keine Übernahme

        [Display(Name = "interne Schulungen", Description = Qualification)]
        public string schulung_intern { get; set; } /* Ungenutzt? In der DB immer leer */// MIGRATION Keine Übernahme

        [Display(Name = "externe Schulungen", Description = Qualification)]
        public string schulung_extern { get; set; } /* Freitext */

        [Display(Name = "Projekte", Description = Qualification)]
        public string projekte { get; set; } /* Ungenutzt? In der DB immer leer */

        [Display(Name = "besondere Fähigkeiten", Description = Qualification)]
        public string besondere_faehigkeiten { get; set; } /* Freitext */

        [Display(Name = "Ausbildung/Beruf", Description = Qualification)]
        public string ausbildung { get; set; } /* Freitext */

        [Display(Name = "Bevorzugte Arbeitszeiten", Description = Qualification)]
        public string bevorzugte_arbeitszeiten { get; set; } /* XXXXX X=0|1=Ja|Nein Reihenfolge: Werktags, Abends, Am Wochenende, Immer, Ferien-/Semesterferien */

        [Display(Name = "Sonstige Bemerkungen", Description = Qualification)]
        public string sonstige_bemerkungen { get; set; }

        [Display(Name = "Sonstige (Promotionerfahrung)", Description = Qualification)]
        public string sonstige { get; set; } /* Freitext */

        [Display(Name = "Computerkenntnisse", Description = Qualification)]
        public string pc_kenntnisse { get; set; } /* Freitext */

        [Display(Name = "Übernachtung möglich in", Description = Qualification)]
        public string uebernachtung { get; set; } /* Freitext */

        [Display(Name = "Bemerkungen", Description = Qualification)]
        public string bemerkung { get; set; } /* Freitext */
        #endregion

        #region Profil

        [Display(Name = "Körpergröße(cm)", Description = Profile)]
        public string groesse { get; set; }

        [Display(Name = "Konfektionsgröße", Description = Profile)]
        public string konf_groesse { get; set; }

        [Display(Name = "Bundweite", Description = Profile)]
        public string jeansgroesse_w { get; set; }

        [Display(Name = "Beinlänge", Description = Profile)]
        public string jeansgroesse_l { get; set; }

        #region Haarfarbe-Codes
        /*
         1 Blond
         2 Mittelblond
         3 Dunkelblond
         4 Braun
         5 Dunkelbraun
         6 Rot
         7 Schwarz
         8 Andere
        */
        #endregion
        [Display(Name = "Haarfarbe", Description = Profile)]
        [UIHint("HairColor")]
        public string haarfarbe { get; set; }

        [Display(Name = "Sonstige Bewertung", Description = Profile)]
        public string bemerkungProfil { get; set; }

        [Display(Name = "Bild 1", Description = Profile)]
        public string name_bild1 { get; set; }

        [Display(Name = "Bild 2", Description = Profile)]
        public string name_bild2 { get; set; }
        #endregion
    }

    public class MappingConfigurationsProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<doa_login, doaLoginViewModel>()
                #region Kontaktdaten
                .ForMember(e => e.nachname, a => a.MapFrom(e => e.doa_kontaktdaten.nachname))
                .ForMember(e => e.vorname, a => a.MapFrom(e => e.doa_kontaktdaten.vorname))
                .ForMember(e => e.email1, a => a.MapFrom(e => e.doa_kontaktdaten.email1))
                .ForMember(e => e.email2, a => a.MapFrom(e => e.doa_kontaktdaten.email2))
                .ForMember(e => e.tel_nummer, a => a.MapFrom(e => e.doa_kontaktdaten.tel1_nummer))
                .ForMember(e => e.tel_vorwahl, a => a.MapFrom(e => e.doa_kontaktdaten.tel1_vorwahl))
                .ForMember(e => e.mobil_nummer, a => a.MapFrom(e => e.doa_kontaktdaten.mobil_nummer))
                .ForMember(e => e.mobil_vorwahl, a => a.MapFrom(e => e.doa_kontaktdaten.mobil_vorwahl))
                .ForMember(e => e.geburtsdatum, a => a.MapFrom(e => e.doa_kontaktdaten.geburtsdatum))
                .ForMember(e => e.geschlecht, a => a.MapFrom(e => e.doa_kontaktdaten.geschlecht))
                .ForMember(e => e.userstatus, a => a.MapFrom(e => e.doa_kontaktdaten.userstatus))
                .ForMember(e => e.userstatus_bemerkung, a => a.MapFrom(e => e.doa_kontaktdaten.userstatus_bemerkung))
                .ForMember(e => e.strasse, a => a.MapFrom(e => e.doa_kontaktdaten.strasse))
                .ForMember(e => e.plz, a => a.MapFrom(e => e.doa_kontaktdaten.plz))
                .ForMember(e => e.ort, a => a.MapFrom(e => e.doa_kontaktdaten.ort))
                .ForMember(e => e.land, a => a.MapFrom(e => e.doa_kontaktdaten.land))
                .ForMember(e => e.fax_vorwahl, a => a.MapFrom(e => e.doa_kontaktdaten.fax_vorwahl))
                .ForMember(e => e.fax_nummer, a => a.MapFrom(e => e.doa_kontaktdaten.fax_nummer))
                .ForMember(e => e.homepage, a => a.MapFrom(e => e.doa_kontaktdaten.homepage))
                .ForMember(e => e.gesundheitszeugnis, a => a.MapFrom(e => e.doa_kontaktdaten.gesundheitszeugnis))
                .ForMember(e => e.gesundheitszeugnis_kopie,a => a.MapFrom(e => e.doa_kontaktdaten.gesundheitszeugnis_kopie))
                .ForMember(e => e.gesundheitszeugnis_dateiname,a => a.MapFrom(e => e.doa_kontaktdaten.gesundheitszeugnis_dateiname))
                .ForMember(e => e.gewerbeschein, a => a.MapFrom(e => e.doa_kontaktdaten.gewerbeschein))
                .ForMember(e => e.gewerbeschein_kopie, a => a.MapFrom(e => e.doa_kontaktdaten.gewerbeschein_kopie))
                .ForMember(e => e.gewerbeschein_dateiname,a => a.MapFrom(e => e.doa_kontaktdaten.gewerbeschein_dateiname))
                .ForMember(e => e.fs_klasse, a => a.MapFrom(e => e.doa_kontaktdaten.fs_klasse))
                .ForMember(e => e.fs_seit_date, a => a.MapFrom(e => e.doa_kontaktdaten.fs_seit_date))
                .ForMember(e => e.fs_bemerkung, a => a.MapFrom(e => e.doa_kontaktdaten.fs_bemerkung))
                .ForMember(e => e.eigener_pkw, a => a.MapFrom(e => e.doa_kontaktdaten.eigener_pkw))
                .ForMember(e => e.rahmenvertrag, a => a.MapFrom(e => e.doa_kontaktdaten.rahmenvertrag))
                .ForMember(e => e.rahmenvertrag_neu, a => a.MapFrom(e => e.doa_kontaktdaten.rahmenvertrag_neu))
                .ForMember(e => e.selbstauskunft, a => a.MapFrom(e => e.doa_kontaktdaten.selbstauskunft))
                #endregion
                #region Qualifikation

                .ForMember(e => e.sprache_en, a => a.MapFrom(e => e.doa_qualifikation.sprache_en))
                .ForMember(e => e.sprache_fr, a => a.MapFrom(e => e.doa_qualifikation.sprache_fr))
                .ForMember(e => e.sprache_it, a => a.MapFrom(e => e.doa_qualifikation.sprache_it))
                .ForMember(e => e.sprache_sp, a => a.MapFrom(e => e.doa_qualifikation.sprache_sp))
                .ForMember(e => e.sprache_pl, a => a.MapFrom(e => e.doa_qualifikation.sprache_pl))
                .ForMember(e => e.sprache_tr, a => a.MapFrom(e => e.doa_qualifikation.sprache_tr))
                .ForMember(e => e.sprache_sonstige, a => a.MapFrom(e => e.doa_qualifikation.sprache_sonstige))
                .ForMember(e => e.sprache_bemerkung, a => a.MapFrom(e => e.doa_qualifikation.sprache_bemerkung))
                .ForMember(e => e.schulung_intern, a => a.MapFrom(e => e.doa_qualifikation.schulung_intern))
                .ForMember(e => e.schulung_extern, a => a.MapFrom(e => e.doa_qualifikation.schulung_extern))
                .ForMember(e => e.projekte, a => a.MapFrom(e => e.doa_qualifikation.projekte))
                .ForMember(e => e.besondere_faehigkeiten,
                    a => a.MapFrom(e => e.doa_qualifikation.besondere_faehigkeiten))
                .ForMember(e => e.ausbildung, a => a.MapFrom(e => e.doa_qualifikation.ausbildung))
                .ForMember(e => e.bevorzugte_arbeitszeiten,
                    a => a.MapFrom(e => e.doa_qualifikation.bevorzugte_arbeitszeiten))
                .ForMember(e => e.sonstige_bemerkungen, a => a.MapFrom(e => e.doa_qualifikation.sonstige_bemerkungen))
                .ForMember(e => e.sonstige, a => a.MapFrom(e => e.doa_qualifikation.sonstige))
                .ForMember(e => e.pc_kenntnisse, a => a.MapFrom(e => e.doa_qualifikation.pc_kenntnisse))
                .ForMember(e => e.uebernachtung, a => a.MapFrom(e => e.doa_qualifikation.uebernachtung))
                .ForMember(e => e.bemerkung, a => a.MapFrom(e => e.doa_qualifikation.bemerkung))
                #endregion
                #region Profil
                .ForMember(e => e.groesse, a => a.MapFrom(e => e.doa_profil.groesse))
                .ForMember(e => e.konf_groesse, a => a.MapFrom(e => e.doa_profil.konf_groesse))
                .ForMember(e => e.jeansgroesse_w, a => a.MapFrom(e => e.doa_profil.jeansgroesse_w))
                .ForMember(e => e.jeansgroesse_l, a => a.MapFrom(e => e.doa_profil.jeansgroesse_l))
                .ForMember(e => e.haarfarbe, a => a.MapFrom(e => e.doa_profil.haarfarbe))
                .ForMember(e => e.name_bild1, a => a.MapFrom(e => e.doa_profil.name_bild1))
                .ForMember(e => e.name_bild2, a => a.MapFrom(e => e.doa_profil.name_bild2))
                .ForMember(e => e.bemerkungProfil, a => a.MapFrom(e => e.doa_profil.bemerkung));
            #endregion

            Mapper.CreateMap<doaLoginViewModel, doa_login>();
        }
    }
}