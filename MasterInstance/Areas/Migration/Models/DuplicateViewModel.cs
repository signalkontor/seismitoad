﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SeismitoadModel;

namespace MasterInstance.Areas.Migration.Models
{
    public class DuplicateViewModel
    {
        public int Id { get; set; }
        public ImportMatchType ImportMatchType { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Postalcode { get; set; }
        public string Email { get; set; }
        public bool HasAssignments { get; set; }
        public DateTime? FirstLogin { get; set; }
        public DateTime? ChangedTime { get; set; }

        public int Id2 { get; set; }
        public ImportMatchType ImportMatchType2 { get; set; }
        public string Title2 { get; set; }
        public string Firstname2 { get; set; }
        public string Lastname2 { get; set; }
        public string Street2 { get; set; }
        public string City2 { get; set; }
        public string Postalcode2 { get; set; }
        public string Email2 { get; set; }
        public bool HasAssignments2 { get; set; }
        public DateTime? FirstLogin2 { get; set; }
        public DateTime? ChangedTime2 { get; set; }
    }
}