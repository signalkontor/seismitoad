﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.Migration.Models;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.Migration.Controllers
{
    public partial class DuplicatesController : Controller
    {
        private readonly SeismitoadDbContext  _dbContext;

        public DuplicatesController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: Migration/Duplicates
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            _dbContext.Database.CommandTimeout = 5*60;
            var model = _dbContext.Database.SqlQuery<DuplicateViewModel>(
                @"
SELECT     e1.Id, e1.ImportMatchType, e1.Title, e1.Firstname, e1.Lastname, p1.Street, p1.PostalCode, p1.City, e1.Email,
		   CAST(CASE WHEN EXISTS(SELECT * FROM AssignedEmployees AE where AE.Employee_Id = e1.Id) THEN 1 ELSE 0 END AS BIT) AS HasAssignments,
		   e2.Id as Id2, e2.Title as Title2, e2.Firstname as Firstname2, e2.Lastname as Lastname2, p2.Street as Street2, p2.PostalCode as PostalCode2, p2.City as City2, e2.Email as Email2, e2.ImportMatchType as ImportMatchType2,
		   CAST(CASE WHEN EXISTS(SELECT * FROM AssignedEmployees AE2 where AE2.Employee_Id = e2.Id) THEN 1 ELSE 0 END AS BIT) AS HasAssignments2,
		   l1.first_login as FirstLogin, l1.changed_time as ChangedTime, l2.first_login as FirstLogin2, l2.changed_time as ChangedTime2
FROM         dbo.Employees e1 INNER JOIN
             dbo.EmployeeProfiles p1 ON e1.Id = p1.Id INNER JOIN
			 dbo.Employees e2 ON e1.Id < e2.Id AND e1.Firstname = e2.Firstname AND e1.Lastname = e2.Lastname INNER JOIN
             dbo.EmployeeProfiles p2 ON e2.Id = p2.Id LEFT OUTER JOIN
			 seismitoad_advantage.dbo.doa_login l1 ON l1.employeeId = e1.Id LEFT OUTER JOIN
			 seismitoad_advantage.dbo.doa_login l2 ON l2.employeeId = e2.Id 
WHERE p1.Birthday = p2.Birthday 
").ToList();
            return Json(model.ToDataSourceResult(request));
        }
    }
}