﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration.Provider;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AdvantageModel.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.Migration.Models;
using MasterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Extensions;
#if false
namespace MasterInstance.Areas.Migration.Controllers
{
    public partial class FailedController : Controller
    {
        private readonly seismitoad_advantageContext  _dbContext;

        public FailedController(seismitoad_advantageContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        // GET: Migration/Failed
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = _dbContext.doa_login.Where(d => d.employeeId == null && d.user_typ == "do_promoter");
            var viewModel = model.Project().To<doaLoginViewModel>().ToDataSourceResult(request);
            return Json(viewModel);
        }

        [HttpPost]
        public virtual ActionResult _Delete(int id, string status)
        {
            var logins = _dbContext.doa_login.Where(e => e.id == id && e.ds_status == status);
            foreach (var login in logins)
            {
                login.user_typ = "do_promoter_delete";
            }
            _dbContext.SaveChanges();
            return Json("Benutzer " + logins.First(f => f.login_name != null).login_name + " wurde entfernt");
        }

        public virtual ActionResult Details(int id, string status)
        {
            var logins = _dbContext.doa_login.Where(e => e.id == id && e.ds_status == status);
            var loginViewModel = logins.Project().To<doaLoginViewModel>().ToList();

            return Request.IsAjaxRequest() ? (ActionResult)PartialView(loginViewModel) : View(loginViewModel);
        }

        #region Import

        private static readonly DateTime ValidDateCutOff = new DateTime(1900, 1, 1);
        private const int PersonalityTestQuestionsLength = 52;
        private const string InputDir = @"C:\AdvantageAssets\In";
        private const string OutputBaseDir = @"C:\AdvantageAssets\Out";

        public virtual ActionResult Import(int id, string status)
        {
            const string regexEmailStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                                        + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                                        + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                                        + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                                        + @"[a-zA-Z]{2,}))$";

            var logins = _dbContext.doa_login.Where(e => e.id == id && e.ds_status == status);
            var employeeViewModel = new ImportEmployeeProfileViewModel();
            employeeViewModel.ImageBaseUrl = string.Format("/PromoterUploads/Employee/{0}/", logins.First().employeeId);
            foreach (var login in logins)
            {
                var email1Valid = Regex.IsMatch(login.doa_kontaktdaten.email1, regexEmailStrict);
                var email2Valid = Regex.IsMatch(login.doa_kontaktdaten.email2, regexEmailStrict);
                var seismitoadUsername = (email1Valid
                                                      ? Membership.GetUserNameByEmail(login.doa_kontaktdaten.email1)
                                                      : null) ??
                                                 (email2Valid
                                                      ? Membership.GetUserNameByEmail(login.doa_kontaktdaten.email2)
                                                      : null);
                var matchStatus = MatchUser(login.login_name, seismitoadUsername);
                switch (matchStatus)
                {
                    case ImportMatchType.Matching:
                        UpdateUser(employeeViewModel, _dbContext, login, login.login_name, matchStatus);
                        break;
                    case ImportMatchType.MatchingEmail:
                        UpdateUser(employeeViewModel, _dbContext, login, seismitoadUsername, matchStatus);
                        break;
                    case ImportMatchType.Conflict:
                        UpdateUser(employeeViewModel, _dbContext, login, seismitoadUsername, matchStatus);
                        break;
                    case ImportMatchType.MatchingUsername:
                        CreateUser(employeeViewModel, _dbContext, login, null,
                                   email1Valid ? login.doa_kontaktdaten.email1 : login.doa_kontaktdaten.email2,
                                   matchStatus);
                        break;
                    case ImportMatchType.NoMatch:
                        CreateUser(employeeViewModel, _dbContext, login, login.login_name,
                                   email1Valid ? login.doa_kontaktdaten.email1 : login.doa_kontaktdaten.email2,
                                   matchStatus);
                        break;
                }
            }
            Mapper.Map(logins.First(), employeeViewModel);
            var keepPasswordAndAddAsApproved = logins.First().password.Trim().Length >= 6;
            if (!keepPasswordAndAddAsApproved)
            {
                employeeViewModel.Password = null;
            }
            return Request.IsAjaxRequest() ? (ActionResult)PartialView(employeeViewModel) : View(employeeViewModel);
        }

        [HttpPost]
        public virtual ActionResult Import(int id, string status,bool getNext)
        {
            var importModel = new ImportEmployeeProfileViewModel();
            if (TryUpdateModel(importModel) && Membership.GetUser(importModel.Username) == null)
            {
                var employee = new Employee();
                if (employee.Profile == null)
                    employee.Profile = new EmployeeProfile();

                Mapper.Map(importModel, employee);
                Mapper.Map(importModel, employee.Profile);
                try
                {
                    Membership.CreateUser(importModel.Username, importModel.Password, importModel.Email1);
                    DB.Context.SaveChanges();
                    var logins = _dbContext.doa_login.Where(e => e.id == id && e.ds_status == status);
                    foreach (var login in logins)
                    {
                        login.employeeId = employee.Id;
                    }
                    _dbContext.SaveChanges();
                }
                catch (ProviderException)
                {
                    ModelState.AddModelError("Email1", "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e);
                }
                if (getNext)
                {
                    var model = _dbContext.doa_login.Where(d => d.employeeId == null && d.user_typ == "do_promoter");
                    if (model.Any())
                    {
                        var next = model.First();
                        return RedirectToAction(MVC.Migration.Failed.Import(next.id,next.ds_status));
                    }
                }
                return RedirectToAction(MVC.Migration.Failed.Index());
            }
            if (Membership.GetUser(importModel.Username) != null)
            {
                ModelState.AddModelError("Username","Benutzername bereits vergeben.");
            }
            importModel.ImageBaseUrl = string.Format("/PromoterUploads/Employee/{0}/", importModel.EmployeeId);
            return View(importModel);
        }

        public virtual ActionResult DeleteImport(int id, string status, bool getNext)
        {
            var logins = _dbContext.doa_login.Where(e => e.id == id && e.ds_status == status);
            foreach (var login in logins)
            {
                login.user_typ = "do_promoter_delete";
            }
            _dbContext.SaveChanges();
            if (getNext)
            {
                var model = _dbContext.doa_login.Where(d => d.employeeId == null && d.user_typ == "do_promoter");
                if (model.Any())
                {
                    return RedirectToAction(MVC.Migration.Failed.Import(model.First().id, model.First().ds_status));
                }
            }
            return RedirectToAction(MVC.Migration.Failed.Index());
        }

        private static ImportMatchType MatchUser(string advantageUsername, string seismitoadUsername)
        {
            if (advantageUsername == seismitoadUsername)
                return ImportMatchType.Matching;

            var userByName = Membership.GetUser(advantageUsername);
            var userByEmail1 = seismitoadUsername != null ? Membership.GetUser(seismitoadUsername) : null;

            if (userByName != null)
            {
                if (userByEmail1 == null)
                    return ImportMatchType.MatchingUsername;

                return ImportMatchType.Conflict;
            }

            if (userByEmail1 != null)
                return ImportMatchType.MatchingEmail;

            return ImportMatchType.NoMatch;
        }

        public class MatchingEmployee
        {
            public int Id { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
        }

        public virtual ActionResult ShowMatchingUsers()
        {
            var users = DB.Context.Employees.Select(s => new MatchingEmployee{Id = s.Id, Firstname = s.Firstname, Lastname = s.Lastname});
            var matchingUsers = new Dictionary<MatchingEmployee, List<MatchingEmployee>>();
            foreach (var user in users)
            {
                var similarUserIds = DB.Context.Database.SqlQuery<MatchingEmployee>(@"SELECT Id, Firstname, Lastname FROM Employees WHERE Id != {2} AND
               dbo.Similarity(Firstname, {0}, 2, 0, 0) >= 0.9 AND
               dbo.Similarity(Lastname, {1}, 2, 0, 0) >= 0.9", new[] { user.Firstname, user.Lastname, user.Id.ToString() });
                if (similarUserIds != null && similarUserIds.Any())
                {
                    matchingUsers.Add(user, similarUserIds.ToList());
                }
            }
            return View(matchingUsers);
        }

        private static void CreateUser(ImportEmployeeProfileViewModel model, seismitoad_advantageContext advantageContext, doa_login login, string username, string email, ImportMatchType status)
        {
            if (username == null)
            {
                username = email;
                while (Membership.GetUser(username) != null)
                {
                    username += "1";
                }
            }

            Console.WriteLine("new user '{0}'", username);
            MembershipCreateStatus membershipCreateStatus;
            var keepPasswordAndAddAsApproved = login.password.Trim().Length >= 6;
            
            var employee = new Employee
            {
                Profile = new EmployeeProfile(),
                ImportMatchType = status
            };

            UpdateEmployee(employee, login);
            UpdateProfile(employee.Profile, login);
            try
            {
                Mapper.Map(employee, model);
                Mapper.Map(employee.Profile, model);
                Console.WriteLine("added employee: {0} {1} ({2})", employee.Firstname, employee.Lastname, employee.Profile.City);
            }
            catch (Exception e)
            {
                for (var exception = e; exception != null; exception = exception.InnerException)
                {
                    Console.WriteLine(e.Message);
                    if (exception is DbEntityValidationException)
                    {
                        var validationException = exception as DbEntityValidationException;
                        var errors =
                            validationException.EntityValidationErrors
                                               .SelectMany(o => o.ValidationErrors)
                                               .Select(o => o.PropertyName + ": " + o.ErrorMessage);
                        var msg = string.Join("\n", errors);
                        Console.WriteLine(msg);
                    }
                }
                // Huch, hier ist was schief gegangen. Wir löschen den MembershipUser besser wieder
                Membership.DeleteUser(username, true);
                return; // Der User konnte nicht angelegt werden, daher brechen wir hier ab
            }

            // Dieser Code wird nur ausgeführt, wenn der Employee korrekt gespeichert wurde.
            UpdatePersonalityTest(advantageContext, employee, login, employee.Profile);
            HandleFiles(login, employee.Id);
        }

        private static void UpdateUser(ImportEmployeeProfileViewModel model, seismitoad_advantageContext advantageContext, doa_login login, string username, ImportMatchType status)
        {
            var membershipUser = Membership.GetUser(username);
            var providerUserKey = (Guid)membershipUser.ProviderUserKey;

            // Es kann auch sein, dass wir zwar einen MemberShip-User haben, dieser aber kein Promoter ist
            var employee = DB.Context.Employees.SingleOrDefault(e => e.ProviderUserKey == providerUserKey);
            if (employee == null)
            {
                employee = new Employee { ProviderUserKey = providerUserKey };
            }
            Console.WriteLine("update employee: {0} {1} ({2})", employee.Firstname, employee.Lastname, employee.Profile != null ? employee.Profile.City : "unbekannt");

            employee.ImportMatchType = status;

            UpdateEmployee(employee, login);

            if (employee.Profile == null)
                employee.Profile = new EmployeeProfile();

            UpdateProfile(employee.Profile, login);
            UpdatePersonalityTest(advantageContext, employee, login, employee.Profile);
            Mapper.Map(employee, model);
            Mapper.Map(employee.Profile, model);
            HandleFiles(login, employee.Id);
        }

        private static void HandleFiles(doa_login login, int employeeId)
        {
            var outputDir = Path.Combine(OutputBaseDir, employeeId.ToString(CultureInfo.InvariantCulture));
            FileCopy(login.doa_kontaktdaten.gesundheitszeugnis_dateiname, outputDir, "UploadHealthCertificate");
            FileCopy(login.doa_kontaktdaten.gewerbeschein_dateiname, outputDir, "UploadTradeLicense");
            FileCopy(login.doa_kontaktdaten.selbstauskunft_dateiname, outputDir, "UploadFreelancerQuestionaire");
            FileCopy(login.doa_kontaktdaten.fs_dateiname, outputDir, "UploadDriversLicense");
            FileCopy(login.doa_profil.name_bild1, outputDir, "UploadPortraitPhoto");
            FileCopy(login.doa_profil.name_bild2, outputDir, "UploadFullBodyPhoto");
        }

        private static void FileCopy(string filename, string outputDir, string baseName)
        {
            if (filename == null) return;
            var extension = Path.GetExtension(filename);
            try
            {
                var sourceFileName = Path.Combine(InputDir, filename);
                if (!System.IO.File.Exists(sourceFileName)) return;
                Directory.CreateDirectory(outputDir);
                System.IO.File.Copy(sourceFileName, Path.Combine(outputDir, baseName + extension), true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void UpdatePersonalityTest(seismitoad_advantageContext advantageContext, Employee employee, doa_login login, EmployeeProfile profile)
        {
            doa_formular1 test;
            doa_bewertung bewertung;
            try
            {
                test = advantageContext.doa_formular1.SingleOrDefault(e => e.dataId == login.id);
                if (test == null)
                {
                    Console.WriteLine("Kein Persönlichkeitstest für diesen User gefunden!");
                    return;
                }
                bewertung = advantageContext.doa_bewertung
                                            .SingleOrDefault(
                                                e => e.formular_id == test.id && e.formular_name == "formular1");
                if (bewertung == null)
                {
                    Console.WriteLine("Kein Persönlichkeitstest für diesen User gefunden!");
                    return;
                }
            }
            catch
            {
                Console.WriteLine("Mehr als ein Persönlichkeitstest für diesen User gefunden!");
                return;
            }
            var stringBuilder = new StringBuilder(PersonalityTestQuestionsLength);
            for (var i = 1; i <= PersonalityTestQuestionsLength; i++)
            {
                var property = typeof(doa_formular1).GetProperty("frage" + i);
                stringBuilder.Append(property.GetValue(test) as string);
            }
            profile.PersonalityTestAnswers = stringBuilder.ToString();

            if (employee.Ratings == null)
            {
                employee.Ratings = new Collection<Rating>();
            }
            var rating = employee.Ratings.SingleOrDefault(s => s.RatingType == 1);
            if (rating == null)
            {
                rating = new Rating();
                employee.Ratings.Add(rating);
            }
            rating.CampaignId = null;
            rating.RatingType = 1;
            rating.Comment = "Selftest";
            rating.Employee = profile.Employee;
            rating.Appearance = null; // wird beim selbsttest nicht bewertet
            rating.Customerdialog = null; // wird beim selbsttest nicht bewertet
            rating.Engagement = bewertung.engagement;
            rating.Reliability = bewertung.zuverlaessigkeit;
            rating.Teamwork = bewertung.teamfaehigkeit;
            rating.Toughness = bewertung.belastbarkeit;
        }

        private static void UpdateEmployee(Employee employee, doa_login login)
        {
            var kontaktdaten = login.doa_kontaktdaten;

            var title = "Unbekannt";
            if (kontaktdaten.geschlecht == "m") title = "Herr";
            if (kontaktdaten.geschlecht == "w") title = "Frau";

            employee.Title = title;
            employee.Firstname = kontaktdaten.vorname ?? "";
            employee.Lastname = kontaktdaten.nachname ?? "";

            EmployeeState state;
            switch (kontaktdaten.userstatus)
            {
                case "1": // Aktiv
                    state = EmployeeState.Active;
                    break;
                case "2": // Neu TODO Soll es in seismitoad auch diesen Status geben?
                    state = EmployeeState.Active;
                    break;
                case "3": // Inaktiv
                    state = EmployeeState.Inactive;
                    break;
                case "4": // Fremdpersonal TODO Soll es in seismitoad auch diesen Status geben?
                    state = EmployeeState.Inactive;
                    break;
                case "5": // Gesperrt
                    state = EmployeeState.Blocked;
                    break;
                default:
                    // Sollte eigentlich nicht vorkommen. Falls doch, ist "inaktiv" eine sichere Wahl
                    state = EmployeeState.Inactive;
                    break;
            }
            employee.State = state;
            employee.StateComment = kontaktdaten.userstatus_bemerkung;
        }

        private static void UpdateProfile(EmployeeProfile profile, doa_login login)
        {
            // TODO Neues Property das angibt, ob das Profil seit dem Import verändert wurde. 
            // Hierdurch ist es möglich einen Parallelbetrieb der beiden Datenbanken durchzuführen. Die User die
            // bereits mit dem neuen System arbeiten werden nicht mehr angefasst. Bei den anderen wird bei jedem Import
            // das Profil mit den aktuellen Werten überschrieben.
            if (ChangedSince(profile, new DateTime(2010, 1, 1)))
                return;

            // Kommentare zurücksetzen.
            profile.Comment = "";
            profile.PersonalDataComment = "";

            var kontaktdaten = login.doa_kontaktdaten;
            var qualifikation = login.doa_qualifikation;
            var profil = login.doa_profil;

            #region Kontaktdaten (Done)
            profile.Birthday = kontaktdaten.geburtsdatum;
            profile.Street = kontaktdaten.strasse ?? "";
            profile.PostalCode = kontaktdaten.plz ?? "";
            profile.City = kontaktdaten.ort ?? "";
            profile.Country = GetCountry(kontaktdaten.land);
            profile.PhoneNo = FormatPhoneNumber(kontaktdaten.tel1_vorwahl, kontaktdaten.tel1_nummer);
            profile.FaxNo = FormatPhoneNumber(kontaktdaten.fax_vorwahl, kontaktdaten.fax_nummer);
            profile.PhoneMobileNo = FormatPhoneNumber(kontaktdaten.mobil_vorwahl, kontaktdaten.mobil_nummer);
            profile.Email2 = kontaktdaten.email2;
            profile.Website = kontaktdaten.homepage;
            profile.HealthCertificate = kontaktdaten.gesundheitszeugnis.AsNullableBool();
            profile.CopyHealthCertificate = kontaktdaten.gesundheitszeugnis_kopie.AsNullableBool();
            profile.ValidTradeLicense = kontaktdaten.gewerbeschein.AsNullableBool();
            // „Kopie Gewerbeschein vorhanden“ – Ausnahme wenn ein Upload zum Gewerbeschein vorliegt soll hier „ja“ angegeben werden (SB 17.9.13) 
            profile.CopyTradeLicense =
                !string.IsNullOrWhiteSpace(kontaktdaten.gewerbeschein_dateiname) &&
                System.IO.File.Exists(Path.Combine(InputDir, kontaktdaten.gewerbeschein_dateiname));

            var validDriversLicenseDate = kontaktdaten.fs_seit_date > ValidDateCutOff;

            if (kontaktdaten.fs_klasse.Length == 25)
            {
                profile.DriversLicenseClass1 = MapNewDriversLicenseClasses(kontaktdaten.fs_klasse);
                profile.DriversLicenseClass2 = MapOldDriversLicenseClasses(kontaktdaten.fs_klasse);
            }

            profile.DriversLicense = validDriversLicenseDate ? true : (bool?)null;
            profile.DriversLicenseSince = validDriversLicenseDate ? kontaktdaten.fs_seit_date : (DateTime?)null;
            profile.Comment = AppendComment(profile.Comment, "Bemerkung Führerschein", kontaktdaten.fs_bemerkung);

            // Soll nicht übernommen werden (SB 17.9.13)
            //profile.SkeletonContract = kontaktdaten.rahmenvertrag_neu.AsNullableBool();
            // Soll nicht übernommen werden (SB 17.9.13)
            //profile.FreelancerQuestionaire = kontaktdaten.selbstauskunft == 1;
            profile.IsCarAvailable = kontaktdaten.eigener_pkw.AsNullableBool();
            #endregion

            #region Qualifikation (Done)
            profile.LanguagesEnglish = MapLanguageSkillLevel(qualifikation.sprache_en);
            profile.LanguagesFrench = MapLanguageSkillLevel(qualifikation.sprache_fr);
            profile.LanguagesItalian = MapLanguageSkillLevel(qualifikation.sprache_it);
            profile.LanguagesSpanish = MapLanguageSkillLevel(qualifikation.sprache_sp);
            profile.LanguagesTurkish = MapLanguageSkillLevel(qualifikation.sprache_tr);
            profile.LanguagesOther = qualifikation.sprache_sonstige;
            profile.ExternalTraining = qualifikation.schulung_extern;
            profile.OtherAbilitiesText = qualifikation.besondere_faehigkeiten;
            profile.OtherCompletedApprenticeships = qualifikation.ausbildung;
            profile.PreferredWorkSchedule = MapPreferredWorkSchedule(qualifikation.bevorzugte_arbeitszeiten).ToArray().SafeJoin(";");
            profile.Comment = AppendComment(profile.Comment, "Bemerkung sonstige Qualifikation", qualifikation.sonstige_bemerkungen);
            profile.HardwareKnownledgeDetails = qualifikation.pc_kenntnisse;
            profile.AccommodationPossible2 = qualifikation.uebernachtung;
            profile.Comment = AppendComment(profile.Comment, "Bemerkung Qualifikation", qualifikation.bemerkung);
            #endregion

            #region Profil (Done)
            profile.Height = profil.groesse.AsNullableInt();
            profile.Size = profil.konf_groesse.AsNullableInt();
            profile.JeansWidth = profil.jeansgroesse_w.AsNullableInt();
            profile.JeansLength = profil.jeansgroesse_l.AsNullableInt();
            profile.HairColor = MapHairColor(profil.haarfarbe);
            profile.ShirtSize = profil.tshirt_groesse.AsNullableInt();
            profile.Comment = AppendComment(profile.Comment, "Bemerkungen", profil.bemerkung);
            #endregion
        }

        private static int? MapHairColor(string color)
        {
            switch (color)
            {
                case "1":
                    return 1;
                case "2":
                    return 1;
                case "3":
                    return 1;
                case "4":
                    return 2;
                case "5":
                    return 2;
                case "6":
                    return 3;
                case "7":
                    return 4;
                default:
                    return null;
            }
        }

        private static IEnumerable<string> MapPreferredWorkSchedule(string preferredSchedule)
        {
            for (var i = 0; i < preferredSchedule.Length; i++)
            {
                if (preferredSchedule[i] == 0) continue;

                switch (i)
                {
                    case '0': // Werktags
                        yield return "1";
                        break;
                    case '1': // Abends
                        yield return "2";
                        break;
                    case '2': // Am Wochenende
                        yield return "3";
                        break;
                    case '3': // Immer
                        yield return "5";
                        break;
                    case '4': // Ferien-/Semesterferien
                        yield return "4";
                        break;
                }
            }
        }

        private static int? MapLanguageSkillLevel(string level)
        {
            switch (level)
            {
                case "1":
                    return 4;
                case "2":
                    return 3;
                case "3":
                    return 2;
                default:
                    return null;
            }
        }

        private static string AppendComment(string existingComments, string type, string comment)
        {
            if (string.IsNullOrWhiteSpace(comment))
                return existingComments;

            return existingComments + type + ":\n" + comment + "\n\n";
        }

        private static readonly Dictionary<int, string> OldDriversLicenseMapping = new Dictionary<int, string>()
            {
                {0, "1"},
                {1, "1a"},
                {2, "1b"},
                {3, "2"},
                {4, "3"},
                {5, "4"},
                {6, "5"},
            };

        private static readonly Dictionary<int, string> NewDriversLicenseMapping = new Dictionary<int, string>()
            {
                {7, "B"},
                {8, "BE"},
                //{9, "Mofa"}, //  gibt's bei uns nicht
                {10, "M"},
                {11, "A1"},
                {12, "A"},
                {13, "A"},
                {14, "S"},
                {15, "C1"},
                {16, "C1E"},
                {17, "C"},
                {18, "CE"},
                {19, "D1"},
                {20, "D1E"},
                {21, "D"},
                {22, "DE"},
                {23, "L"},
                {24, "T"},
            };

        private static string MapOldDriversLicenseClasses(string fsKlasse)
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < 7; i++)
            {
                if (fsKlasse[i] != '1') continue;
                stringBuilder.Append(OldDriversLicenseMapping[i]);
                stringBuilder.Append(";");
            }
            return stringBuilder.ToString();
        }

        private static string MapNewDriversLicenseClasses(string fsKlasse)
        {
            var stringBuilder = new StringBuilder();
            for (var i = 7; i < 25; i++)
            {
                if (fsKlasse[i] != '1' || !NewDriversLicenseMapping.ContainsKey(i)) continue;
                stringBuilder.Append(NewDriversLicenseMapping[i]);
                stringBuilder.Append(";");
            }
            return stringBuilder.ToString();
        }

        private static string FormatPhoneNumber(string vorwahl, string nummer)
        {
            var finalNumber = new StringBuilder();

            vorwahl = vorwahl.Replace(" ", string.Empty);

            if (vorwahl.StartsWith("0"))
            {
                finalNumber.Append("+49 ");
                finalNumber.Append(vorwahl.Substring(1));
            }
            else if (vorwahl.StartsWith("+49"))
            {
                finalNumber.Append("+49 ");
                finalNumber.Append(vorwahl.Substring(3).Replace("(0)", string.Empty));
            }
            else
            {
                finalNumber.Append(vorwahl);
            }

            finalNumber.Append('-');
            finalNumber.Append(nummer.Replace(" ", string.Empty));

            return finalNumber.ToString();
        }

        private static int GetCountry(string land)
        {
            switch (land)
            {
                case "at":
                    return 2; // Österreich
                case "ch":
                    return 3; // Schweiz
                // ReSharper disable RedundantCaseLabel
                case "de":
                case "pl":
                case "tr":
                // ReSharper restore RedundantCaseLabel
                default:
                    return 1; // Deutschland (auch für pl und tr und alle weiteren da die Promoter hier Fehlangaben gemacht haben.

            }
        }

        private static bool ChangedSince(EmployeeProfile profile, DateTime dateTime)
        {
            using (var context = new SeismitoadDbContext())
            {
                var idAsString = profile.Id.ToString(CultureInfo.InvariantCulture);
                var tableName = context.GetTableName<EmployeeProfile>();
                return context.ChangeSets.Any(e =>
                                              e.Date > dateTime &&
                                              e.TableName == tableName &&
                                              e.EntityId == idAsString);
            }
        }
        #endregion
    }
}
#endif