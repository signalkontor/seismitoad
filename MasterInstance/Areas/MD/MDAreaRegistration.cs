﻿using System.Web.Mvc;

namespace MasterInstance.Areas.MD
{
    public class MDAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MD"; // MD = Master Data = Stammdaten
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MD_EmployeeProfile",
                "MD/PromoterProfile/{id}",
                new { controller = "EmployeeProfile", action = "Index", id = UrlParameter.Optional });

            context.MapRoute(
                "MD_default",
                "MD/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}