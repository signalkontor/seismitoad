﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class LocationViewModel
    {
        public int Id { get; set; }
        public LocationState? State { get; set; }
        public int? LocationTypeId { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodeMin { get; set; }
        public string PostalCodeMax { get; set; }
        public string City { get; set; }
        public int? StateProvinceId { get; set; }
        public int? LocationGroupId { get; set; }
        public string Notes { get; set; }
        public string CenterManagementStreet { get; set; }
        public string CenterManagementPostalCode { get; set; }
        public string CenterManagementCity { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int? VisitorFrequency { get; set; }
        public int? Area { get; set; }
        public int? SellingArea { get; set; }
        public string CampaignAreaPrices { get; set; }
        public int? Audience { get; set; }
        public int? Opening { get; set; }
        public int? CatchmentArea { get; set; }
        public int? Parking { get; set; }
        public bool? OutdoorArea { get; set; }
        public string Retailers { get; set; }
        public bool? WiFiAvailable { get; set; }
        public string ConferenceRoomSize { get; set; }
        public bool? CostTransferPossible { get; set; }
        public string RoomRates { get; set; }
        public string ConferenceEquipmentCost { get; set; }
        public string ConferencePackageCost { get; set; }
        public string Contacts { get; set; }
        public bool Deleteable { get; set; }
    }

    public class LocationViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Location, LocationViewModel>()
                .ForMember(e => e.Deleteable, opt => opt.MapFrom(e => !e.CampaignLocations.Any()))
                .ForMember(e => e.Contacts, opt => opt.Ignore());
            Mapper.CreateMap<LocationViewModel, Location>();
        }
    }
}