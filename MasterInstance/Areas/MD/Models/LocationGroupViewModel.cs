﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class LocationGroupViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int LocationCount { get; set; }
    }

    public class LocationGroupViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LocationGroup, LocationGroupViewModel>()
                .ForMember(e => e.Id, opt => opt.MapFrom(i => i.Id))
                .ForMember(e => e.Name, opt => opt.MapFrom(i => i.Name))
                .ForMember(e => e.LocationCount, opt => opt.MapFrom(i => i.Locations.Count));

            Mapper.CreateMap<LocationGroupViewModel, LocationGroup>()
                .ForSourceMember(e => e.LocationCount, opt => opt.Ignore())
                .ForMember(e => e.Id, opt => opt.MapFrom(i => i.Id))
                .ForMember(e => e.Name, opt => opt.MapFrom(i => i.Name));
        }
    }
}