using System;

namespace MasterInstance.Areas.MD.Models
{
    public class EditTrainingParticipantsViewModel
    {
        public int TrainingId { get; set; }
        public int TrainingDayId { get; set; }
        public int DateId { get; set; }
        public string Title { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTil { get; set; }
    }
}