﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using ObjectTemplate.Extensions;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class TrainingParticipantViewModel
    {
        public int TrainingDayId { get; set; }
        public int EmployeeId { get; set; }

        public string State { get; set; }
        public string Travel { get; set; }
        public decimal? TravelCost { get; set; }
        public string Hotel { get; set; }
        public string Remark { get; set; }

        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string CampaignEmployeeState { get; set; }
        public string Project { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTil { get; set; }

        public int RowNumber { get; set; }
    }

    public class TrainingParticipantViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TrainingParticipantViewModel, TrainingParticipant>()
                ;

            Mapper.CreateMap<TrainingParticipant, TrainingParticipantViewModel>()
                .ForMember(e => e.RowNumber, opt => opt.MapFrom(e => 1))
                .ForMember(e => e.EmployeeId, src => src.MapFrom(e => e.CampaignEmployee.EmployeeId))
                .ForMember(e => e.DateFrom, mem => mem.MapFrom(e => e.TrainingDay.DateFrom))
                .ForMember(e => e.DateTil, mem => mem.MapFrom(e => e.TrainingDay.DateTil))
                .ForMember(e => e.Lastname, mem => mem.MapFrom(e => e.CampaignEmployee.Employee.Lastname))
                .ForMember(e => e.Firstname, mem => mem.MapFrom(e => e.CampaignEmployee.Employee.Firstname))
                .ForMember(e => e.PostalCode, mem => mem.MapFrom(e => e.CampaignEmployee.Employee.Profile.PostalCode))
                .ForMember(e => e.Email, mem => mem.MapFrom(e => e.CampaignEmployee.Employee.Email))
                .ForMember(e => e.CampaignEmployeeState, mem => mem.MapFrom(e => e.CampaignEmployee.State))
                .ForMember(e => e.Project, mem => mem.MapFrom(e => e.CampaignEmployee.Campaign.Name))
                ;
        }
    }
}