﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class TrainingEmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public int TrainingDayId { get; set; }
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }
        public string TrainingTitle { get; set; }

        public DateTime TrainingDateFrom { get; set; }
        public DateTime TrainingDateTil { get; set; }

        public string TrainingState { get; set; }
    }

    public class TrainingEmployeeViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TrainingEmployeeViewModel, TrainingParticipant>();

            Mapper.CreateMap<TrainingParticipant, TrainingEmployeeViewModel>()
                .ForMember(dest => dest.EmployeeId, src => src.MapFrom(e => e.CampaignEmployee.EmployeeId))
                .ForMember(dest => dest.TrainingDayId, src => src.MapFrom(e => e.TrainingDayId))
                .ForMember(dest => dest.ProjectNumber, src => src.MapFrom(e =>
                    e.CampaignEmployee.Campaign.Customer.Number + e.CampaignEmployee.Campaign.Number))
                .ForMember(dest => dest.CustomerName, src => src.MapFrom(e => e.CampaignEmployee.Campaign.Customer.Name))
                .ForMember(dest => dest.TrainingTitle, src => src.MapFrom(e => e.TrainingDay.Training.Title))
                .ForMember(dest => dest.TrainingDateFrom, src => src.MapFrom(e => e.TrainingDay.DateFrom))
                .ForMember(dest => dest.TrainingDateTil, src => src.MapFrom(e => e.TrainingDay.DateTil))
                .ForMember(dest => dest.TrainingState, src => src.MapFrom(e => e.State));

        }
    }
}