﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class TrainingDayViewModel
    {
        public int Id { get; set; }
        [UIHint("KendoDateTime")]
        public DateTime DateFrom { get; set; }
        [UIHint("KendoDateTime")]
        public DateTime DateTil { get; set; }
        public string Remarks { get; set; }
        public int ParticipantCount { get; set; }

        public string Location { get; set; }

        public int TrainingId { get; set; }
        public int MaxParticipants { get; set; }
    }

    public class TrainingDayViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TrainingDayViewModel, TrainingDay>()
                ;

            Mapper.CreateMap<TrainingDay, TrainingDayViewModel>()
                .ForMember(e => e.ParticipantCount, member => member.MapFrom(e => e.Participants.Count))
                ;
        }
    }
}