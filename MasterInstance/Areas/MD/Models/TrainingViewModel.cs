﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Models.ViewModels;
using ObjectTemplate.Attributes;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    [Bind(Exclude = "Locations,Dates")]
    public class TrainingViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string ProjectNumber { get; set; }
        public int? ParticipantCount { get; set; }
        public int? MaxParticipants { get; set; }
        public int DateCount { get; set; }

        public string CustomerName { get; set; }

        public int CampaignId { get; set; }

        public bool HiddenCampaign { get; set; }

        public IEnumerable<string> Locations { get; set; }
        public IEnumerable<DateTime> Dates { get; set; }
    }

    public class TrainingsViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TrainingViewModel, Training>()
                ;

            Mapper.CreateMap<Training, TrainingViewModel>()
                .ForMember(e => e.HiddenCampaign, mem => mem.MapFrom(e => e.Campaign.State == CampaignState.Archived || e.Campaign.State == CampaignState.Deleted))
                .ForMember(e => e.DateCount, member => member.MapFrom(e => e.TrainingDays.Count))
                .ForMember(e => e.ProjectNumber, mem => mem.MapFrom(e => e.Campaign.Customer.Number + e.Campaign.Number))
                .ForMember(e => e.MaxParticipants, mem => mem.MapFrom(e => e.TrainingDays.Count > 0 ? e.TrainingDays.Sum(i => i.MaxParticipants) : 0))
                .ForMember(e => e.ParticipantCount, mem => mem.MapFrom(e => e.TrainingDays.Count > 0 ? e.TrainingDays.Sum(i => i.Participants.Count) : 0))
                .ForMember(e => e.CustomerName, mem => mem.MapFrom(e => e.Campaign.Customer.Name))
                .ForMember(e => e.Locations, mem => mem.MapFrom(e => e.TrainingDays.OrderBy(i => i.DateFrom).Select(i => i.Location)))
                .ForMember(e => e.Dates, mem => mem.MapFrom(e => e.TrainingDays.OrderBy(i => i.DateFrom).Select(i => i.DateFrom)))
                ;
        }
    }
}