﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.MD.Models
{
    public class LocationTypeViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int LocationCount { get; set; }
    }

    public class LocationTypeViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LocationType, LocationTypeViewModel>()
                .ForMember(e => e.Id, opt => opt.MapFrom(i => i.Id))
                .ForMember(e => e.Name, opt => opt.MapFrom(i => i.Name))
                .ForMember(e => e.LocationCount, opt => opt.MapFrom(i => i.Locations.Count));

            Mapper.CreateMap<LocationTypeViewModel, LocationType>()
                .ForSourceMember(e => e.LocationCount, opt => opt.Ignore())
                .ForMember(e => e.Id, opt => opt.MapFrom(i => i.Id))
                .ForMember(e => e.Name, opt => opt.MapFrom(i => i.Name));
        }
    }
}