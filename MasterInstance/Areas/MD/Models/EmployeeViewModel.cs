﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using ObjectTemplate.Attributes;
using SeismitoadModel;
using System.Web.Security;
using SeismitoadShared;

namespace MasterInstance.Areas.MD.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public Guid ProviderUserKey { get; set; }

        [EnumerationType(typeof(EmployeeState))]
        public EmployeeState State { get; set; }

        [EnumerationType(typeof(ImportMatchType))]
        public ImportMatchType ImportMatchType { get; set; }

        //[UIHint("SingleSelectKendo")]
        public string Title { get; set; }

        //[UIHint("SingleSelectKendo")]
        public string Login { get; set; }

        public DateTime? LastLogin { get; set; }

        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string StateComment { get; set; }

        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class EmployeeViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Employee, EmployeeViewModel>()
                .ForMember(e => e.State, opt => opt.MapFrom(e => e.Profile.ExternalUntil > LocalDateTime.Now ? EmployeeState.External : e.State))
                .ForMember(e => e.Password, opt => opt.Ignore());
            Mapper.CreateMap<EmployeeViewModel, Employee>();
        }
    }
}