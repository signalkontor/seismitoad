﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using AdvantageModel.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using MasterInstance.Models;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class EmployeeProfileController : Controller
    {
        private const string ProfileImagesBaseUrl = "~/PromoterUploads/Employee/{0}/";
        private readonly SeismitoadDbContext _dbContext;

        public EmployeeProfileController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        //
        // GET: /MD/Employee/
        public virtual ActionResult Index(int id)
        {
            var employee = _dbContext.Employees
                .Where(e => e.Id == id)
                .Include(e => e.Profile)
                .Single();
            var model = GetEmployeeProfileViewModel(employee);
            SetDisplayOnlyProperties(employee, ref model);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(int id, EmployeeProfileViewModel model)
        {
            if (model.IsExternal == false && model.State == EmployeeState.External)
            {
                ModelState.AddModelError("State", "Bitte ein Datum bei 'Fremdpersonal bis' eingeben.");
            }
            if (!ModelState.IsValid)
            {
                return Index(id);
            }

            var employee = _dbContext.Employees.Single(e => e.Id == id);
            if (employee.Profile == null)
                employee.Profile = new EmployeeProfile();

            // AddressChanged muss vor dem Mapping aufgerufen werden, da danach die Adresse ja immer gleich ist.
            var addressChanged = AddressChanged(model, employee.Profile);
            Mapper.Map(model, employee);
            Mapper.Map(model, employee.Profile);

            if (addressChanged)
            {
                // Geokoordinaten resetten, damit sie vom Job neu berechnet werden.
                _dbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[CampaignEmployees] SET [AddressChanged] = 1 WHERE [EmployeeId] = @EmployeeId", new SqlParameter("@EmployeeId", id));
                employee.Profile.GeographicCoordinates = null;
                employee.Profile.GeocodingFailed = false;
            }
            try
            {
                var user = Membership.GetUser(employee.ProviderUserKey);
                user.Email = model.Email1;
                Membership.UpdateUser(user);
                _dbContext.SaveChanges();
            }
            catch (ProviderException)
            {
                ModelState.AddModelError("Email1", "Ungültige oder bereits von einem anderem Benutzer verwendete E-Mail-Adresse.");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("X", "Es ist ein Fehler beim Speichern aufgetreten");
            }
            return Index(id);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            var employee = _dbContext.Employees.Single(e => e.Id == id);
            employee.State = EmployeeState.Deleted;
            //employee.Firstname = "Gelöschter";
            //employee.Lastname = "Benutzer (" + id + ")";
            //employee.Title = "";

            var originalProfile = employee.Profile;
            // Wir setzen ganz bewusst, das Gerbutsdatum auf den 2. Januar 1800. So markieren wir, dass dieser Datensatz
            // kürzlich gelöscht wurde. Diese kürzlich gelöschen Datensätze müssen dann noch mal vom Lösch-Job durchlaufen
            // werden, der dann die vom Promoter hochgeladenen Dateien löscht.
            employee.Profile = new EmployeeProfile
            {
                Id = employee.Id,
                Employee = employee,
                Birthday = new DateTime(1800, 01, 02),
            };
            if (originalProfile != null)
            {
                employee.Profile.PersonalDataComment = originalProfile.PersonalDataComment;
                employee.Profile.Comment = originalProfile.Comment;
                employee.Profile.DriversLicenseRemark = originalProfile.DriversLicenseRemark;
            }
            _dbContext.Configuration.ValidateOnSaveEnabled = false;
            _dbContext.SaveChanges();
            var user = Membership.GetUser(employee.ProviderUserKey);
            Membership.DeleteUser(user.UserName, true);
            return RedirectToAction(MVC.MD.EmployeeProfile.Index(id));
        }

        #region Profile helpers
        private EmployeeProfileViewModel GetEmployeeProfileViewModel(Employee employee)
        {
            var model = new EmployeeProfileViewModel();
            Mapper.Map(employee, model);
            Mapper.Map(employee.Profile, model);
            HandleExternalState(model);
            var zeugnisDir = Server.MapPath(string.Format("~/PromoterUploads/Employee/{0}/Zeugnisse", employee.Id));
            if (Directory.Exists(zeugnisDir))
            {
                model.Zeugnisse = Directory.GetFiles(zeugnisDir).Select(Path.GetFileName).ToArray();
            }
            return model;
        }

        private static void HandleExternalState(EmployeeProfileViewModel model)
        {
            if (model.IsExternal)
            {
                model.State = EmployeeState.External;
            }
        }

        private void SetDisplayOnlyProperties(Employee employee, ref EmployeeProfileViewModel model)
        {
            if (User.IsInRole(SeismitoadShared.Constants.Roles.Admin))
            {
                ViewBag.Dupes = _dbContext.Employees
                    .Where(e => e.Id != employee.Id && e.Firstname == employee.Firstname && e.Lastname == employee.Lastname)
                    .Select(e => e.Id)
                    .ToList();
            }
            Mapper.Map(Membership.GetUser(employee.ProviderUserKey), model);
            model.ImageBaseUrl = string.Format(ProfileImagesBaseUrl, employee.Id);
            using (var advantageContext = new seismitoad_advantageContext())
            {
                var advantageLogin = advantageContext.doa_login.FirstOrDefault(e => e.employeeId == employee.Id);
                Mapper.Map(advantageLogin, model);
            }
            ViewData["CampaignItems"] = _dbContext.Campaigns.Select(e => new SelectListItem { Value = "" + e.Id, Text = e.Name}).ToList();
        }
        private static bool AddressChanged(EmployeeProfileViewModel viewModel, EmployeeProfile profile)
        {
            return viewModel.Street != profile.Street || viewModel.PostalCode != profile.PostalCode ||
                   viewModel.City != profile.City;
        }
        #endregion

        public virtual ActionResult _Experiences([DataSourceRequest] DataSourceRequest request, int employeeId)
        {
            var model = _dbContext.Experiences.Where(e => e.Employee.Id == employeeId)
                .Project().To<ExperienceViewModel>();
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult _Projects([DataSourceRequest] DataSourceRequest request, int employeeId)
        {
            // Neben den Einträgen die den Status Projektauftrag bestätigt haben, nehmen wir auch die bei denen der Status
            // Storniert (durch pm/adv) ist, da ja auch hier der Promoter Teil des Projekts war, selbst wenn er früher aus dem
            // Projekt rausgenommen wurden.
            var seismitoadProjects = _dbContext.CampaignEmployees
                                       .Where(e => e.EmployeeId == employeeId && (e.ContractAccepted.HasValue || e.ContractCancelled.HasValue))
                                       .Select(e => new ProjectListModel { Customer = e.Campaign.Customer.Name, Name = e.Campaign.Name })
                                       .ToList();

            var projectsOldDbSearch = _dbContext.EmployeeProfiles.Where(e => e.Id == employeeId).Select(e => e.ProjectsOldDbSearch).SingleOrDefault();
            IEnumerable<ProjectListModel> advantageProjects;
            if (!string.IsNullOrWhiteSpace(projectsOldDbSearch))
            {
                var projects = projectsOldDbSearch.Split(new[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
                advantageProjects = projects.Select(e =>
                {
                    var splitted = e.Split(new[] { "~~~" }, StringSplitOptions.None);
                    return new ProjectListModel
                    {
                        Customer = splitted[0],
                        Name = splitted[1]
                    };
                });
            }
            else
            {
                advantageProjects = new ProjectListModel[0];
            }

            var model = seismitoadProjects.Concat(advantageProjects);
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult _Trainings([DataSourceRequest] DataSourceRequest request, int employeeId)
        {
            var trainingDays = _dbContext.TrainingParticipants.Where(e => e.CampaignEmployee.EmployeeId == employeeId);
            var trainingsViewModel = trainingDays.Project().To<TrainingEmployeeViewModel>();
            return Json(trainingsViewModel.ToDataSourceResult(request));
        }
    }
}