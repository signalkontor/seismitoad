﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class RatingController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public RatingController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult _Details(int ratingId)
        {
            var details = _dbContext.Database
                .SqlQuery<RatingDetailsViewModel>(DetailsQuery, new SqlParameter("@RatingId", ratingId))
                .SingleOrDefault();
            ViewBag.OtherRemark =
                _dbContext.Ratings.Where(e => e.Id == ratingId).Select(e => e.Comment).SingleOrDefault();

            return details != null ? (ActionResult)PartialView(details) : Content("Für diese Bewertung liegen keine Details vor.");
        }

        public virtual ActionResult _Create(int employeeId)
        {
            if (TempData["CloseWindow"] != null)
                ViewBag.CloseWindow = true;

            UpdateViewData();
            return View();
        }

        [HttpPost]
        public virtual ActionResult _Create(int? ratingType, int employeeId, int? campaign, RatingDetailsViewModel model, string otherRemark)
        {
            if (!campaign.HasValue)
                ModelState.AddModelError("campaign", "Bitte auswählen.");

            if (!ratingType.HasValue)
                ModelState.AddModelError("ratingType", "Bitte auswählen.");


            if (ModelState.IsValid)
            {
                var rating = new Rating
                {
                    RatingType = ratingType.Value,
                    CampaignId = campaign.Value,
                    EmployeeId = employeeId,
                    Appearance = Average(model.Appearance01, model.Appearance02, model.Appearance03, model.Appearance04, model.Appearance05, model.Appearance06, model.Appearance07, model.Appearance08, model.Appearance09),
                    Customerdialog = Average(model.CustomerDialog01, model.CustomerDialog02, model.CustomerDialog03, model.CustomerDialog04, model.CustomerDialog05, model.CustomerDialog06, model.CustomerDialog07, model.CustomerDialog08, model.CustomerDialog09, model.CustomerDialog10),
                    KnowledgeAndSales = Average(model.KnowledgeAndSales01, model.KnowledgeAndSales02, model.KnowledgeAndSales03, model.KnowledgeAndSales04, model.KnowledgeAndSales05, model.KnowledgeAndSales06, model.KnowledgeAndSales07, model.KnowledgeAndSales08, model.KnowledgeAndSales09, model.KnowledgeAndSales10),
                    Engagement = Average(model.Engagement01, model.Engagement02, model.Engagement03, model.Engagement04, model.Engagement05, model.Engagement06, model.Engagement07),
                    Reliability = Average(model.Reliability01, model.Reliability02, model.Reliability03, model.Reliability04, model.Reliability05, model.Reliability06, model.Reliability07, model.Reliability08, model.Reliability09, model.Reliability10, model.Reliability11+ model.Reliability12),
                    Teamwork = Average(model.Teamwork01, model.Teamwork02, model.Teamwork03, model.Teamwork04, model.Teamwork05, model.Teamwork06, model.Teamwork07, model.Teamwork08, model.Teamwork09),
                    Toughness = Average(model.Toughness01, model.Toughness02, model.Toughness03),
                    Comment = otherRemark
                };
                _dbContext.Ratings.Add(rating);
                _dbContext.SaveChanges();
                InsertDetailsIntoDatabase(model, rating.Id);
                TempData["CloseWindow"] = true;
                return RedirectToAction(MVC.MD.Rating._Create(employeeId));
            }
            UpdateViewData();
            return View();
        }

        public virtual ActionResult _Edit(int? ratingId)
        {
            if (ratingId == null)
                return Content("Bitte warten...");

            UpdateViewData();
            var details = _dbContext.Database
                .SqlQuery<RatingDetailsViewModel>(DetailsQuery, new SqlParameter("@RatingId", ratingId))
                .SingleOrDefault();

            var tmp = _dbContext.Ratings.Where(e => e.Id == ratingId).Select(e => new { e.RatingType, e.CampaignId, e.Comment }).SingleOrDefault();

            ViewBag.RatingType = tmp?.RatingType;
            ViewBag.Campaign = tmp?.CampaignId;
            ViewBag.OtherRemark = tmp?.Comment;

            return View("_Create", details);
        }

        [HttpPost]
        public virtual ActionResult _Edit(int ratingId, int? ratingType, int? campaign, RatingDetailsViewModel model, string otherRemark)
        {
            if (!campaign.HasValue)
                ModelState.AddModelError("campaign", "Bitte auswählen.");

            if (!ratingType.HasValue)
                ModelState.AddModelError("ratingType", "Bitte auswählen.");

            if (ModelState.IsValid)
            {
                var rating = _dbContext.Ratings.Single(e => e.Id == ratingId);
                rating.RatingType = ratingType.Value;
                rating.CampaignId = campaign.Value;
                rating.Appearance = Average(model.Appearance01, model.Appearance02, model.Appearance03, model.Appearance04, model.Appearance05, model.Appearance06, model.Appearance07, model.Appearance08, model.Appearance09);
                rating.Customerdialog = Average(model.CustomerDialog01, model.CustomerDialog02, model.CustomerDialog03, model.CustomerDialog04, model.CustomerDialog05, model.CustomerDialog06, model.CustomerDialog07, model.CustomerDialog08, model.CustomerDialog09, model.CustomerDialog10);
                rating.KnowledgeAndSales = Average(model.KnowledgeAndSales01, model.KnowledgeAndSales02, model.KnowledgeAndSales03, model.KnowledgeAndSales04, model.KnowledgeAndSales05, model.KnowledgeAndSales06, model.KnowledgeAndSales07, model.KnowledgeAndSales08, model.KnowledgeAndSales09, model.KnowledgeAndSales10);
                rating.Engagement = Average(model.Engagement01, model.Engagement02, model.Engagement03, model.Engagement04, model.Engagement05, model.Engagement06, model.Engagement07);
                rating.Reliability = Average(model.Reliability01, model.Reliability02, model.Reliability03, model.Reliability04, model.Reliability05, model.Reliability06, model.Reliability07, model.Reliability08, model.Reliability09, model.Reliability10, model.Reliability11 + model.Reliability12);
                rating.Teamwork = Average(model.Teamwork01, model.Teamwork02, model.Teamwork03, model.Teamwork04, model.Teamwork05, model.Teamwork06, model.Teamwork07, model.Teamwork08, model.Teamwork09);
                rating.Toughness = Average(model.Toughness01, model.Toughness02, model.Toughness03);
                rating.Comment = otherRemark;
                _dbContext.SaveChanges();
                UpdateDetailsInDatabase(model, rating.Id);
                ViewBag.CloseWindow = true;
            }
            UpdateViewData();
            return View("_Create");
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int employeeId)
        {
            var ratings = _dbContext.Ratings.Where(e => e.EmployeeId == employeeId).Project().To<RatingViewModel>();
            return Json(ratings.ToDataSourceResult(request));
        }

        private const string DetailsQuery = @"
SELECT [Id]
      ,[Appearance01]
      ,[Appearance02]
      ,[Appearance03]
      ,[Appearance04]
      ,[Appearance05]
      ,[Appearance06]
      ,[Appearance07]
      ,[Appearance08]
      ,[Appearance09]
      ,[CustomerDialog01]
      ,[CustomerDialog02]
      ,[CustomerDialog03]
      ,[CustomerDialog04]
      ,[CustomerDialog05]
      ,[CustomerDialog06]
      ,[CustomerDialog07]
      ,[CustomerDialog08]
      ,[CustomerDialog09]
      ,[CustomerDialog10]
      ,[KnowledgeAndSales01]
      ,[KnowledgeAndSales02]
      ,[KnowledgeAndSales03]
      ,[KnowledgeAndSales04]
      ,[KnowledgeAndSales05]
      ,[KnowledgeAndSales06]
      ,[KnowledgeAndSales07]
      ,[KnowledgeAndSales08]
      ,[KnowledgeAndSales09]
      ,[KnowledgeAndSales10]
      ,[Engagement01]
      ,[Engagement02]
      ,[Engagement03]
      ,[Engagement04]
      ,[Engagement05]
      ,[Engagement06]
      ,[Engagement07]
      ,[Reliability01]
      ,[Reliability02]
      ,[Reliability03]
      ,[Reliability04]
      ,[Reliability05]
      ,[Reliability06]
      ,[Reliability07]
      ,[Reliability08]
      ,[Reliability09]
      ,[Reliability10]
      ,[Reliability11]
      ,[Reliability12]
      ,[Teamwork01]
      ,[Teamwork02]
      ,[Teamwork03]
      ,[Teamwork04]
      ,[Teamwork05]
      ,[Teamwork06]
      ,[Teamwork07]
      ,[Teamwork08]
      ,[Teamwork09]
      ,[Teamleader01]
      ,[Teamleader02]
      ,[Teamleader03]
      ,[Teamleader04]
      ,[Toughness01]
      ,[Toughness02]
      ,[Toughness03]
      ,[AppearenceRemark01]
      ,[AppearenceRemark02]
      ,[CustomerDialogRemark01]
      ,[CustomerDialogRemark02]
      ,[KnowledgeAndSalesRemark01]
      ,[KnowledgeAndSalesRemark02]
      ,[EngagementRemark01]
      ,[EngagementRemark02]
      ,[ReliabilityRemark01]
      ,[ReliabilityRemark02]
      ,[ReliabilityRemark03]
      ,[TeamworkRemark01]
      ,[TeamworkRemark02]
      ,[TeamleaderRemark01]
      ,[ToughnessRemark01]
      ,[PositiveAttributes]
      ,[PossibleImprovements]
FROM [dbo].[RatingDetails]
WHERE [Id] = @RatingId
";

        private void UpdateViewData()
        {
            ViewData["CampaignItems"] =
                _dbContext.Campaigns.Where(e => e.State < CampaignState.Archived)
                    .Select(e => new SelectListItem { Value = "" + e.Id, Text = e.Name })
                    .ToList();
        }

        private static decimal? Average(params decimal?[] args)
        {
            return args.Where(e => e.HasValue).Average();
        }

        private void InsertUpdateHelper(bool isInsert, RatingDetailsViewModel model, int id)
        {
            #region INSERT Statement
            const string insertStatement = @"INSERT INTO [dbo].[RatingDetails]
           ([Id]
           ,[Appearance01]
           ,[Appearance02]
           ,[Appearance03]
           ,[Appearance04]
           ,[Appearance05]
           ,[Appearance06]
           ,[Appearance07]
           ,[Appearance08]
           ,[Appearance09]
           ,[CustomerDialog01]
           ,[CustomerDialog02]
           ,[CustomerDialog03]
           ,[CustomerDialog04]
           ,[CustomerDialog05]
           ,[CustomerDialog06]
           ,[CustomerDialog07]
           ,[CustomerDialog08]
           ,[CustomerDialog09]
           ,[CustomerDialog10]
           ,[KnowledgeAndSales01]
           ,[KnowledgeAndSales02]
           ,[KnowledgeAndSales03]
           ,[KnowledgeAndSales04]
           ,[KnowledgeAndSales05]
           ,[KnowledgeAndSales06]
           ,[KnowledgeAndSales07]
           ,[KnowledgeAndSales08]
           ,[KnowledgeAndSales09]
           ,[KnowledgeAndSales10]
           ,[Engagement01]
           ,[Engagement02]
           ,[Engagement03]
           ,[Engagement04]
           ,[Engagement05]
           ,[Engagement06]
           ,[Engagement07]
           ,[Reliability01]
           ,[Reliability02]
           ,[Reliability03]
           ,[Reliability04]
           ,[Reliability05]
           ,[Reliability06]
           ,[Reliability07]
           ,[Reliability08]
           ,[Reliability09]
           ,[Reliability10]
           ,[Reliability11]
           ,[Reliability12]
           ,[Teamwork01]
           ,[Teamwork02]
           ,[Teamwork03]
           ,[Teamwork04]
           ,[Teamwork05]
           ,[Teamwork06]
           ,[Teamwork07]
           ,[Teamwork08]
           ,[Teamwork09]
           ,[Teamleader01]
           ,[Teamleader02]
           ,[Teamleader03]
           ,[Teamleader04]
           ,[Toughness01]
           ,[Toughness02]
           ,[Toughness03]
           ,[AppearenceRemark01]
           ,[AppearenceRemark02]
           ,[CustomerDialogRemark01]
           ,[CustomerDialogRemark02]
           ,[KnowledgeAndSalesRemark01]
           ,[KnowledgeAndSalesRemark02]
           ,[EngagementRemark01]
           ,[EngagementRemark02]
           ,[ReliabilityRemark01]
           ,[ReliabilityRemark02]
           ,[ReliabilityRemark03]
           ,[TeamworkRemark01]
           ,[TeamworkRemark02]
           ,[TeamleaderRemark01]
           ,[ToughnessRemark01]
           ,[PositiveAttributes]
           ,[PossibleImprovements])
     VALUES
           (@Id
           ,@Appearance01
           ,@Appearance02
           ,@Appearance03
           ,@Appearance04
           ,@Appearance05
           ,@Appearance06
           ,@Appearance07
           ,@Appearance08
           ,@Appearance09
           ,@CustomerDialog01
           ,@CustomerDialog02
           ,@CustomerDialog03
           ,@CustomerDialog04
           ,@CustomerDialog05
           ,@CustomerDialog06
           ,@CustomerDialog07
           ,@CustomerDialog08
           ,@CustomerDialog09
           ,@CustomerDialog10
           ,@KnowledgeAndSales01
           ,@KnowledgeAndSales02
           ,@KnowledgeAndSales03
           ,@KnowledgeAndSales04
           ,@KnowledgeAndSales05
           ,@KnowledgeAndSales06
           ,@KnowledgeAndSales07
           ,@KnowledgeAndSales08
           ,@KnowledgeAndSales09
           ,@KnowledgeAndSales10
           ,@Engagement01
           ,@Engagement02
           ,@Engagement03
           ,@Engagement04
           ,@Engagement05
           ,@Engagement06
           ,@Engagement07
           ,@Reliability01
           ,@Reliability02
           ,@Reliability03
           ,@Reliability04
           ,@Reliability05
           ,@Reliability06
           ,@Reliability07
           ,@Reliability08
           ,@Reliability09
           ,@Reliability10
           ,@Reliability11
           ,@Reliability12
           ,@Teamwork01
           ,@Teamwork02
           ,@Teamwork03
           ,@Teamwork04
           ,@Teamwork05
           ,@Teamwork06
           ,@Teamwork07
           ,@Teamwork08
           ,@Teamwork09
           ,@Teamleader01
           ,@Teamleader02
           ,@Teamleader03
           ,@Teamleader04
           ,@Toughness01
           ,@Toughness02
           ,@Toughness03
           ,@AppearenceRemark01
           ,@AppearenceRemark02
           ,@CustomerDialogRemark01
           ,@CustomerDialogRemark02
           ,@KnowledgeAndSalesRemark01
           ,@KnowledgeAndSalesRemark02
           ,@EngagementRemark01
           ,@EngagementRemark02
           ,@ReliabilityRemark01
           ,@ReliabilityRemark02
           ,@ReliabilityRemark03
           ,@TeamworkRemark01
           ,@TeamworkRemark02
           ,@TeamleaderRemark01
           ,@ToughnessRemark01
           ,@PositiveAttributes
           ,@PossibleImprovements)";
            #endregion
            #region UPDATE Statement
            const string updateStatement = @"UPDATE [dbo].[RatingDetails]
           SET
            [Appearance01] = @Appearance01
           ,[Appearance02] = @Appearance02
           ,[Appearance03] = @Appearance03
           ,[Appearance04] = @Appearance04
           ,[Appearance05] = @Appearance05
           ,[Appearance06] = @Appearance06
           ,[Appearance07] = @Appearance07
           ,[Appearance08] = @Appearance08
           ,[Appearance09] = @Appearance09
           ,[CustomerDialog01] = @CustomerDialog01
           ,[CustomerDialog02] = @CustomerDialog02
           ,[CustomerDialog03] = @CustomerDialog03
           ,[CustomerDialog04] = @CustomerDialog04
           ,[CustomerDialog05] = @CustomerDialog05
           ,[CustomerDialog06] = @CustomerDialog06
           ,[CustomerDialog07] = @CustomerDialog07
           ,[CustomerDialog08] = @CustomerDialog08
           ,[CustomerDialog09] = @CustomerDialog09
           ,[CustomerDialog10] = @CustomerDialog10
           ,[KnowledgeAndSales01] = @KnowledgeAndSales01
           ,[KnowledgeAndSales02] = @KnowledgeAndSales02
           ,[KnowledgeAndSales03] = @KnowledgeAndSales03
           ,[KnowledgeAndSales04] = @KnowledgeAndSales04
           ,[KnowledgeAndSales05] = @KnowledgeAndSales05
           ,[KnowledgeAndSales06] = @KnowledgeAndSales06
           ,[KnowledgeAndSales07] = @KnowledgeAndSales07
           ,[KnowledgeAndSales08] = @KnowledgeAndSales08
           ,[KnowledgeAndSales09] = @KnowledgeAndSales09
           ,[KnowledgeAndSales10] = @KnowledgeAndSales10
           ,[Engagement01] = @Engagement01
           ,[Engagement02] = @Engagement02
           ,[Engagement03] = @Engagement03
           ,[Engagement04] = @Engagement04
           ,[Engagement05] = @Engagement05
           ,[Engagement06] = @Engagement06
           ,[Engagement07] = @Engagement07
           ,[Reliability01] = @Reliability01
           ,[Reliability02] = @Reliability02
           ,[Reliability03] = @Reliability03
           ,[Reliability04] = @Reliability04
           ,[Reliability05] = @Reliability05
           ,[Reliability06] = @Reliability06
           ,[Reliability07] = @Reliability07
           ,[Reliability08] = @Reliability08
           ,[Reliability09] = @Reliability09
           ,[Reliability10] = @Reliability10
           ,[Reliability11] = @Reliability11
           ,[Reliability12] = @Reliability12
           ,[Teamwork01] = @Teamwork01
           ,[Teamwork02] = @Teamwork02
           ,[Teamwork03] = @Teamwork03
           ,[Teamwork04] = @Teamwork04
           ,[Teamwork05] = @Teamwork05
           ,[Teamwork06] = @Teamwork06
           ,[Teamwork07] = @Teamwork07
           ,[Teamwork08] = @Teamwork08
           ,[Teamwork09] = @Teamwork09
           ,[Teamleader01] = @Teamleader01
           ,[Teamleader02] = @Teamleader02
           ,[Teamleader03] = @Teamleader03
           ,[Teamleader04] = @Teamleader04
           ,[Toughness01] = @Toughness01
           ,[Toughness02] = @Toughness02
           ,[Toughness03] = @Toughness03
           ,[AppearenceRemark01] = @AppearenceRemark01
           ,[AppearenceRemark02] = @AppearenceRemark02
           ,[CustomerDialogRemark01] = @CustomerDialogRemark01
           ,[CustomerDialogRemark02] = @CustomerDialogRemark02
           ,[KnowledgeAndSalesRemark01] = @KnowledgeAndSalesRemark01
           ,[KnowledgeAndSalesRemark02] = @KnowledgeAndSalesRemark02
           ,[EngagementRemark01] = @EngagementRemark01
           ,[EngagementRemark02] = @EngagementRemark02
           ,[ReliabilityRemark01] = @ReliabilityRemark01
           ,[ReliabilityRemark02] = @ReliabilityRemark02
           ,[ReliabilityRemark03] = @ReliabilityRemark03
           ,[TeamworkRemark01] = @TeamworkRemark01
           ,[TeamworkRemark02] = @TeamworkRemark02
           ,[TeamleaderRemark01] = @TeamleaderRemark01
           ,[ToughnessRemark01] = @ToughnessRemark01
           ,[PositiveAttributes] = @PositiveAttributes
           ,[PossibleImprovements] = @PossibleImprovements
     WHERE ID = @Id";
            #endregion

            _dbContext.Database.ExecuteSqlCommand(isInsert ? insertStatement : updateStatement,
                new SqlParameter("@Id", id),
                new SqlParameter("@Appearance01", model.Appearance01 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance02", model.Appearance02 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance03", model.Appearance03 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance04", model.Appearance04 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance05", model.Appearance05 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance06", model.Appearance06 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance07", model.Appearance07 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance08", model.Appearance08 ?? (object)DBNull.Value),
                new SqlParameter("@Appearance09", model.Appearance09 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog01", model.CustomerDialog01 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog02", model.CustomerDialog02 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog03", model.CustomerDialog03 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog04", model.CustomerDialog04 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog05", model.CustomerDialog05 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog06", model.CustomerDialog06 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog07", model.CustomerDialog07 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog08", model.CustomerDialog08 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog09", model.CustomerDialog09 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialog10", model.CustomerDialog10 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales01", model.KnowledgeAndSales01 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales02", model.KnowledgeAndSales02 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales03", model.KnowledgeAndSales03 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales04", model.KnowledgeAndSales04 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales05", model.KnowledgeAndSales05 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales06", model.KnowledgeAndSales06 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales07", model.KnowledgeAndSales07 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales08", model.KnowledgeAndSales08 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales09", model.KnowledgeAndSales09 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSales10", model.KnowledgeAndSales10 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement01", model.Engagement01 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement02", model.Engagement02 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement03", model.Engagement03 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement04", model.Engagement04 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement05", model.Engagement05 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement06", model.Engagement06 ?? (object)DBNull.Value),
                new SqlParameter("@Engagement07", model.Engagement07 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability01", model.Reliability01 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability02", model.Reliability02 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability03", model.Reliability03 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability04", model.Reliability04 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability05", model.Reliability05 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability06", model.Reliability06 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability07", model.Reliability07 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability08", model.Reliability08 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability09", model.Reliability09 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability10", model.Reliability10 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability11", model.Reliability11 ?? (object)DBNull.Value),
                new SqlParameter("@Reliability12", model.Reliability12 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork01", model.Teamwork01 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork02", model.Teamwork02 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork03", model.Teamwork03 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork04", model.Teamwork04 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork05", model.Teamwork05 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork06", model.Teamwork06 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork07", model.Teamwork07 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork08", model.Teamwork08 ?? (object)DBNull.Value),
                new SqlParameter("@Teamwork09", model.Teamwork09 ?? (object)DBNull.Value),
                new SqlParameter("@Toughness01", model.Toughness01 ?? (object)DBNull.Value),
                new SqlParameter("@Toughness02", model.Toughness02 ?? (object)DBNull.Value),
                new SqlParameter("@Toughness03", model.Toughness03 ?? (object)DBNull.Value),
                new SqlParameter("@Teamleader01", model.Teamleader01 ?? (object)DBNull.Value),
                new SqlParameter("@Teamleader02", model.Teamleader02 ?? (object)DBNull.Value),
                new SqlParameter("@Teamleader03", model.Teamleader03 ?? (object)DBNull.Value),
                new SqlParameter("@Teamleader04", model.Teamleader04 ?? (object)DBNull.Value),
                new SqlParameter("@AppearenceRemark01", model.AppearenceRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@AppearenceRemark02", model.AppearenceRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialogRemark01", model.CustomerDialogRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@CustomerDialogRemark02", model.CustomerDialogRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSalesRemark01", model.KnowledgeAndSalesRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@KnowledgeAndSalesRemark02", model.KnowledgeAndSalesRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@EngagementRemark01", model.EngagementRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@EngagementRemark02", model.EngagementRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@ReliabilityRemark01", model.ReliabilityRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@ReliabilityRemark02", model.ReliabilityRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@ReliabilityRemark03", model.ReliabilityRemark03 ?? (object)DBNull.Value),
                new SqlParameter("@TeamworkRemark01", model.TeamworkRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@TeamworkRemark02", model.TeamworkRemark02 ?? (object)DBNull.Value),
                new SqlParameter("@TeamleaderRemark01", model.TeamleaderRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@ToughnessRemark01", model.ToughnessRemark01 ?? (object)DBNull.Value),
                new SqlParameter("@PositiveAttributes", model.PositiveAttributes ?? (object)DBNull.Value),
                new SqlParameter("@PossibleImprovements", model.PossibleImprovements ?? (object)DBNull.Value)
                );
        }

        private void InsertDetailsIntoDatabase(RatingDetailsViewModel model, int id)
        {
            InsertUpdateHelper(true, model, id);
        }

        private void UpdateDetailsInDatabase(RatingDetailsViewModel model, int id)
        {
            InsertUpdateHelper(false, model, id);
        }
    }
}
