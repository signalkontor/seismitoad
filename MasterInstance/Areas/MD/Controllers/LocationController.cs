﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Services;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Constants;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class LocationController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public LocationController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, LocationSearchModel model, string nameStreetCity)
        {
            _dbContext.Configuration.LazyLoadingEnabled = false;

            var locations = (string.IsNullOrEmpty(nameStreetCity)
                ? new SearchService().Locations(_dbContext.Locations, model)
                : _dbContext.Locations.Where(e =>
                    e.Name.Contains(nameStreetCity) ||
                    e.Street.Contains(nameStreetCity) ||
                    e.City.Contains(nameStreetCity)));

            var viewModel = locations.Project().To<LocationViewModel>();
            return Json(viewModel.ToDataSourceResult(request));
        }

        public virtual ActionResult _LocationTypes()
        {
            return Json(_dbContext.LocationTypes
                .OrderBy(e => e.Name)
                .Select(e => new { Value = e.Id, Text = e.Name}), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult _LocationGroups()
        {
            return Json(_dbContext.LocationGroups
                .OrderBy(e => e.Name)
                .Select(e => new { Value = e.Id, Text = e.Name }), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult _StateProvinces()
        {
            return Json(_dbContext.StateProvinces
                .OrderBy(e => e.Name)
                .Select(e => new { Value = e.Id, Text = e.Name }), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Roles.LocationEditor + "," + Roles.Admin)]
        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var objectToDDelete = _dbContext.Locations.Single(e => e.Id == model.Id);
                _dbContext.Locations.Remove(objectToDDelete);
            }
            _dbContext.SaveChanges();
            return Json(new [] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}