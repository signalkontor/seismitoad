﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using Microsoft.Ajax.Utilities;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class LocationGroupController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public LocationGroupController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: MD/LocationType
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = _dbContext.LocationGroups.Project().To<LocationGroupViewModel>();
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IList<LocationGroupViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var type = _dbContext.LocationGroups.Single(e => e.Id == model.Id);
                    type.Name = model.Name;
                }

                _dbContext.SaveChanges();
            }

            return Json(models.ToDataSourceResult(request));
        }

        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<LocationGroupViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var objectToDDelete = _dbContext.LocationGroups.Single(e => e.Id == model.Id);
                    _dbContext.LocationGroups.Remove(objectToDDelete);
                }
            }
            _dbContext.SaveChanges();

            return Json(models.ToDataSourceResult(request,ModelState));
        }

        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<LocationGroupViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var newLocationGroup = new LocationGroup() { Name = model.Name };
                    _dbContext.LocationGroups.Add(newLocationGroup);
                    _dbContext.SaveChanges();
                    model.Id = newLocationGroup.Id;
                }
            }
            return Json(models.ToDataSourceResult(request));
        }
    }
}