﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using DocumentFormat.OpenXml;
using Newtonsoft.Json;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using NotesFor.HtmlToOpenXml;
using Paragraph = DocumentFormat.OpenXml.Wordprocessing.Paragraph;
using Path = System.IO.Path;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using Formatting = Newtonsoft.Json.Formatting;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class SedcardController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public SedcardController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index(int id, string type)
        {
            ViewBag.EmployeeId = id;
            ViewBag.Type = type;
            return View();
        }

        public virtual ActionResult Display(int employeeId, string type)
        {
            if (Request.IsLocal || Request.IsAuthenticated)
            {
                ViewBag.EmployeeId = employeeId;
                ViewBag.Type = type;
                return View();
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private static string RemoveParamers(string url)
        {
            var paramsStart = url.IndexOf("?", StringComparison.Ordinal);
            return paramsStart < 0
                ? url
                : url.Substring(0, paramsStart);
        }

        private Point AddImagePart(WordprocessingDocument document, string url, string id)
        {
            
            ImagePartType imagePartType;
            var path = Server.MapPath(RemoveParamers(url));
            switch (Path.GetExtension(path).ToLower())
            {
                case ".jpg":
                    imagePartType = ImagePartType.Jpeg;
                    break;
                case ".png":
                    imagePartType = ImagePartType.Png;
                    break;
                default:
                    throw new InvalidOperationException("Nicht erlaubter Dateityp: " + Path.GetExtension(url));
            }
            var imagePart = document.MainDocumentPart.AddImagePart(imagePartType, id);
            using (var stream = new FileStream(path, FileMode.Open))
            {
                imagePart.FeedData(stream);
            }
            using (var img = new Bitmap(path))
            {
                return new Point(img.Width, img.Height);
            }
        }

        private uint _anchorIdCounter = 0;

        private void AddImage(OpenXmlElement parent, ImageInfo imageInfo, int noOfPictures, int currentPicture)
        {
            var ratio = (double) imageInfo.Size.Y/imageInfo.Size.X;
            var width = 3500000L / noOfPictures;
            var height = (long)(width*ratio);
            // Define the reference of the image.

            var element =
                 new Drawing(
                     new DW.Anchor(
                         new DW.SimplePosition() { X = 0, Y = 0 },
                         new DW.HorizontalPosition(new DW.PositionOffset((-3850000L + currentPicture * width).ToString())) {  RelativeFrom = DW.HorizontalRelativePositionValues.Column},
                         new DW.VerticalPosition(new DW.PositionOffset("0")) { RelativeFrom = DW.VerticalRelativePositionValues.Paragraph },
                         new DW.Extent() { Cx = width, Cy = height },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         //new DW.WrapSquare() { WrapText = DW.WrapTextValues.BothSides },
                         new DW.WrapNone(),
                         new DW.DocProperties()
                         {
                             Id = _anchorIdCounter++,
                             Name = imageInfo.Name
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = _anchorIdCounter++,
                                             Name = imageInfo.Name
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri =
                                                        "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = imageInfo.Id,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = width, Cy = height }),
                                         new A.PresetGeometry(
                                             new A.AdjustValueList()
                                         )
                                         { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         AllowOverlap = true,
                         LayoutInCell = true,
                         Locked = false,
                         BehindDoc = false,
                         SimplePos = false,
                         RelativeHeight = 1,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            parent.AppendChild(new Run(element));
        }

        public virtual ActionResult DocX(int employeeId, string type)
        {
            var sedcard = GetSedcardViewModel(employeeId, type);
            var templateFileName = Server.MapPath("~/App_Data/Sedcard_Layout.docx");
            var template = System.IO.File.ReadAllBytes(templateFileName);
            using (var stream = new MemoryStream { Capacity = 2 * template.Length})
            {
                stream.Write(template, 0, template.Length);
                using (var document = WordprocessingDocument.Open(stream, true))
                {
                    var doc = document.MainDocumentPart.Document;
                    var headerParagraph = document.MainDocumentPart.HeaderParts.First().Header.Descendants<Paragraph>().Single();
                    headerParagraph.AppendChild(new Run(new Text($"{sedcard.Vorname} {sedcard.Nachname}")));

                    // Um einen sauberen Zustand zu haben, entfernen wir erstmal alle Absätze im Template.
                    // RemoveAllChildren() können wir übrigens nicht benutzen, da dies auch "Metadaten" wie die
                    // Definition der Kopf- und Fußzeile entferen würde.
                    var paragraphs = doc.Body.ChildElements.OfType<Paragraph>();
                    foreach (var paragraph in paragraphs)
                    {
                        doc.Body.RemoveChild(paragraph);
                    }

                    var table = new Table(
                        new TableProperties(
                            new TableWidth() {Width = "7371", Type = TableWidthUnitValues.Dxa},
                            new TableJustification() { Val = TableRowAlignmentValues.Right }),
                        new TableGrid(
                            new GridColumn(),
                            new GridColumn())
                    );

                    if (sedcard.FunktionVisible)
                    {
                        AddTableRow(table, "Funktion:", sedcard.Funktion);
                    }
                    if (sedcard.EinsatzortVisible)
                    {
                        AddTableRow(table, "Einsatzort:", sedcard.Einsatzort);
                    }
                    if (sedcard.AlterVisible)
                    {
                        AddTableRow(table, "Alter:", sedcard.Alter);
                    }
                    if (sedcard.GroesseVisible)
                    {
                        AddTableRow(table, "Größe:", sedcard.Groesse);
                    }
                    if (sedcard.ShirtGroesseVisible)
                    {
                        AddTableRow(table, "Konfektionsgröße:", sedcard.Konfektionsgroesse);
                    }
                    if (sedcard.KonfektionsgroesseVisible)
                    {
                        AddTableRow(table, "T-Shirt-Größe:", sedcard.ShirtGroesse);
                    }
                    if (sedcard.FuehrerscheinVisible)
                    {
                        AddTableRow(table, "Führerschein:", sedcard.Fuehrerschein);
                    }
                    if (sedcard.AusbildungVisible)
                    {
                        AddTableRow(table, "Ausbildung:", sedcard.Ausbildung);
                    }
                    if (sedcard.StudiumVisible)
                    {
                        AddTableRow(table, "Studium:", sedcard.Studium);
                    }
                    if (sedcard.BerufserfahrungVisible)
                    {
                        AddTableRow(table, "Berufserfahrung:", sedcard.Berufserfahrung);
                    }
                    if (sedcard.SprachkenntnisseVisible)
                    {
                        AddTableRow(table, "Sprachkenntnisse:", sedcard.Sprachkenntnisse);
                    }
                    if (sedcard.SoftwarekenntnisseVisible)
                    {
                        AddTableRow(table, "Softwarekenntnisse:", sedcard.Softwarekenntnisse);
                    }
                    if (sedcard.HardwarekenntnisseVisible)
                    {
                        AddTableRow(table, "Hardwarekenntnisse:", sedcard.Hardwarekenntnisse);
                    }
                    if (sedcard.SonstigeKenntnisseVisible)
                    {
                        AddTableRow(table, "Sonstige Kenntnisse:", sedcard.SonstigeKenntnisse);
                    }
                    if (sedcard.ReferenzenVisible)
                    {
                        AddTableRow(table, "Auszug Referenzen:", sedcard.Referenzen);
                    }
                    if (sedcard.SonstigesVisible)
                    {
                        AddTableRow(table, "Sonstiges:", sedcard.Sonstiges);
                    }
                    if (sedcard.ExtraVisible && !string.IsNullOrWhiteSpace(sedcard.Extra))
                    {
                        var converter = new HtmlConverter(document.MainDocumentPart);
                        var p = converter.Parse(sedcard.Extra);
                        table.AppendChild(new TableRow(
                            new TableCell(new Paragraph(new Run(new Text("")))),
                            new TableCell(p)));
                    }

                    var baseUrl = $"~/PromoterUploads/Employee/{employeeId}/";
                    var imageInfos = new List<ImageInfo>();
                    if (sedcard.PortraitPhotoVisible)
                    {
                        var portrait = Helpers.GetImageUrl(baseUrl, "UploadPortraitPhoto", 120, 120);
                        imageInfos.Add(new ImageInfo { Id = "portraitImage", Name = "Portraitfoto", Size = AddImagePart(document, portrait, "portraitImage") });
                    }
                    if (sedcard.FullBodyPhotoVisible)
                    {
                        var fullBody = Helpers.GetImageUrl(baseUrl, "UploadFullBodyPhoto", 120, 120);
                        imageInfos.Add(new ImageInfo { Id = "fullBodyImage", Name = "Ganzkörperfoto", Size = AddImagePart(document, fullBody, "fullBodyImage") });
                    }
                    if (sedcard.OtherPhotoVisible)
                    {
                        var other = Helpers.GetImageUrl(baseUrl, "UploadOtherPhoto", 120, 120);
                        imageInfos.Add(new ImageInfo { Id = "otherImage", Name = "Weiteres Foto", Size = AddImagePart(document, other, "otherImage") });
                    }
                    var noOfImages = imageInfos.Count;
                    var currentPicture = 0;
                    var pictureRow = table.ChildElements.OfType<TableRow>().FirstOrDefault();
                    if (pictureRow == null)
                    {
                        pictureRow = new TableRow();
                        table.AppendChild(pictureRow);
                    }
                    var pictureCell = pictureRow.ChildElements.OfType<TableCell>().FirstOrDefault();
                    if (pictureCell == null)
                    {
                        pictureCell = new TableCell();
                        pictureRow.AppendChild(pictureCell);
                    }
                    var pictureParagraph = pictureCell.ChildElements.OfType<Paragraph>().FirstOrDefault();
                    if (pictureParagraph == null)
                    {
                        pictureParagraph = new Paragraph();
                        pictureCell.AppendChild(pictureParagraph);
                    }

                    foreach (var imageInfo in imageInfos)
                    {
                        AddImage(pictureParagraph, imageInfo, noOfImages, currentPicture++);
                    }

                    // Wir fügen ganz vorne ein, weil es (aus dem Template) noch ein <sectPr> Element gibt, das aber ganz am Ende bleiben soll.
                    // Wenn wir einfach Append(Child)() Aufrufen würde unsere Table hinter dem besagten Element landen, was nicht valide ist.
                    doc.Body.InsertAt(table, 0);
                   
                    var validator = new OpenXmlValidator();
                    var errors = validator.Validate(document);
                    var validationErrorInfos = errors as ValidationErrorInfo[] ?? errors.ToArray();
                    if (validationErrorInfos.Any())
                        return Content(string.Join("\n\n", validationErrorInfos.Select(e => e.Description)));
                }
                var filename = MakeValidFileName($"{sedcard.Vorname} {sedcard.Nachname}.docx");
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }

        private static char[] _invalids;

        /// <summary>Replaces characters in <c>text</c> that are not allowed in 
        /// file names with the specified replacement character.</summary>
        /// <param name="text">Text to make into a valid filename. The same string is returned if it is valid already.</param>
        /// <param name="replacement">Replacement character, or null to simply remove bad characters.</param>
        /// <param name="fancy">Whether to replace quotes and slashes with the non-ASCII characters ” and ⁄.</param>
        /// <returns>A string that can be used as a filename. If the output string would otherwise be empty, returns "_".</returns>
        private static string MakeValidFileName(string text, char? replacement = '_', bool fancy = true)
        {
            StringBuilder sb = new StringBuilder(text.Length);
            var invalids = _invalids ?? (_invalids = Path.GetInvalidFileNameChars());
            bool changed = false;
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                if (invalids.Contains(c))
                {
                    changed = true;
                    var repl = replacement ?? '\0';
                    if (fancy)
                    {
                        if (c == '"') repl = '”'; // U+201D right double quotation mark
                        else if (c == '\'') repl = '’'; // U+2019 right single quotation mark
                        else if (c == '/') repl = '⁄'; // U+2044 fraction slash
                    }
                    if (repl != '\0')
                        sb.Append(repl);
                }
                else
                    sb.Append(c);
            }
            if (sb.Length == 0)
                return "_";
            return changed ? sb.ToString() : text;
        }

        private static void AddTableRow(Table table, string header, string value)
        {
            var row = new TableRow();
            var headerCell = new TableCell(new Paragraph(new Run(new Text(header))));
            var splitted = value?.Replace("\r\n", "\n").Split(new[] {'\r', '\n'}, StringSplitOptions.None);
            var run = new Run();
            for (var i = 0; splitted != null && i < splitted.Length; i++)
            {
                run.AppendChild(new Text(splitted[i]));
                if (i < splitted.Length - 1) run.AppendChild(new Break());
            }
            var valueCell = new TableCell(new Paragraph(run));
            row.Append(headerCell, valueCell);
            table.AppendChild(row);
        }

        public string Referenzen(Employee employee)
        {
            var projectList = employee.Experiences
                .Select(e => e.Title)
                .ToList();

            // Neben den Einträgen die den Status Projektauftrag bestätigt haben, nehmen wir auch die bei denen der Status
            // Storniert (durch pm/adv) ist, da ja auch hier der Promoter Teil des Projekts war, selbst wenn er früher aus dem
            // Projekt rausgenommen wurden.
            projectList.AddRange(_dbContext.CampaignEmployees
                .Where(e => e.EmployeeId == employee.Id && (e.ContractAccepted.HasValue || e.ContractCancelled.HasValue))
                .Select(e => e.Campaign.Name));

            if (!string.IsNullOrWhiteSpace(employee.Profile.ProjectsOldDbSearch))
            {
                var tmp = employee.Profile.ProjectsOldDbSearch.Split(new[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
                projectList.AddRange(tmp.Select(e => e.Replace("~~~", " - ")));
            }
            return string.Join("\n", projectList.OrderBy(e => e));
        }

        public virtual ActionResult _Data(int employeeId, string type)
        {
            var viewModel = GetSedcardViewModel(employeeId, type);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        private SedcardViewModel GetSedcardViewModel(int employeeId, string type)
        {
            var filename = GetSedcardFilename(employeeId, type);
            var employee = _dbContext.Employees
                .Where(e => e.Id == employeeId)
                .Include(e => e.Profile)
                .Single();

            if (!System.IO.File.Exists(filename))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filename));
                var sedcard = new SedcardViewModel();
                sedcard.Funktion = "";
                sedcard.Einsatzort = "";
                sedcard.Sonstiges = "";
                if (employee.Profile != null)
                {
                    var itknowledge = SelectListRepository.Retrieve("ITKnowledge", ViewData, null);
                    var p = employee.Profile;
                    sedcard.Ausbildung = p.OtherCompletedApprenticeships;
                    sedcard.Studium = p.StudiesDetails;
                    sedcard.Berufserfahrung = p.WorkExperience;
                    if (p.SoftwareKnownledge.HasValue)
                    {
                        var tmp = itknowledge.SingleOrDefault(e => e.Value == "" + p.SoftwareKnownledge);
                        sedcard.Softwarekenntnisse = tmp != null ? tmp.Text : "";
                    }
                    if (!string.IsNullOrWhiteSpace(p.SoftwareKnownledgeDetails))
                    {
                        sedcard.Softwarekenntnisse += (sedcard.Softwarekenntnisse.Length > 0 ? "\n\n" : "") +
                                                      p.SoftwareKnownledgeDetails;
                    }
                    if (p.HardwareKnownledge.HasValue)
                    {
                        var tmp = itknowledge.SingleOrDefault(e => e.Value == "" + p.HardwareKnownledge);
                        sedcard.Hardwarekenntnisse = tmp != null ? tmp.Text : "";
                    }
                    else
                    {
                        sedcard.Hardwarekenntnisse = "";
                    }
                    if (!string.IsNullOrWhiteSpace(p.HardwareKnownledgeDetails))
                    {
                        sedcard.Hardwarekenntnisse += (sedcard.Hardwarekenntnisse.Length > 0 ? "\n\n" : "") +
                                                      p.HardwareKnownledgeDetails;
                    }
                    sedcard.Referenzen = Referenzen(employee);
                }
                else
                {
                    sedcard.Ausbildung = "";
                    sedcard.Studium = "";
                    sedcard.Berufserfahrung = "";
                    sedcard.Softwarekenntnisse = "";
                    sedcard.Hardwarekenntnisse = "";
                    sedcard.Referenzen = "";
                }
                _Save(sedcard, employeeId, type);
            }

            var viewModel = JsonConvert.DeserializeObject<SedcardViewModel>(System.IO.File.ReadAllText(filename));
            viewModel.Vorname = employee.Firstname;
            viewModel.Nachname = employee.Lastname;
            if (employee.Profile != null)
            {
                var p = employee.Profile;
                var now = DateTime.UtcNow;
                var age = now.Year - p.Birthday.Year;
                if (now.Month < p.Birthday.Month || (now.Month == p.Birthday.Month && now.Day < p.Birthday.Day))
                {
                    age--;
                }
                viewModel.Alter = "" + age;
                viewModel.Groesse = p.Height + " cm";
                viewModel.Konfektionsgroesse = "" + p.Size;
                var sl1 = SelectListRepository.Retrieve("ShirtSize", ViewData).ToDictionary(e => e.Value, e => e.Text);
                var key = "" + p.ShirtSize;
                viewModel.ShirtGroesse = p.ShirtSize.HasValue && sl1.ContainsKey(key) ? sl1[key] : "";
                viewModel.Fuehrerschein = FormatDriversLicense(p.DriversLicenseClass1);
                var sb = new StringBuilder();
                var langKnowledge = SelectListRepository.Retrieve("LanguageKnowledge", ViewData)
                    .ToDictionary(e => e.Value, e => e.Text);
                if (p.LanguagesGerman.HasValue && p.LanguagesGerman > 1)
                {
                    sb.AppendFormat("Deutsch ({0})\n", langKnowledge["" + p.LanguagesGerman]);
                }
                if (p.LanguagesEnglish.HasValue && p.LanguagesEnglish > 1)
                {
                    sb.AppendFormat("Englisch ({0})\n", langKnowledge["" + p.LanguagesEnglish]);
                }
                if (p.LanguagesFrench.HasValue && p.LanguagesFrench > 1)
                {
                    sb.AppendFormat("Französisch ({0})\n", langKnowledge["" + p.LanguagesFrench]);
                }
                if (p.LanguagesSpanish.HasValue && p.LanguagesSpanish > 1)
                {
                    sb.AppendFormat("Spanisch ({0})\n", langKnowledge["" + p.LanguagesSpanish]);
                }
                if (p.LanguagesItalian.HasValue && p.LanguagesItalian > 1)
                {
                    sb.AppendFormat("Italienisch ({0})\n", langKnowledge["" + p.LanguagesItalian]);
                }
                if (p.LanguagesTurkish.HasValue && p.LanguagesTurkish > 1)
                {
                    sb.AppendFormat("Türkisch ({0})\n", langKnowledge["" + p.LanguagesTurkish]);
                }
                if (!string.IsNullOrWhiteSpace(p.LanguagesOther))
                {
                    sb.Append(p.LanguagesOther + "\n");
                }
                viewModel.Sprachkenntnisse = sb.ToString();
            }
            return viewModel;
        }

        private static string FormatDriversLicense(string driversLicense)
        {
            if (string.IsNullOrWhiteSpace(driversLicense))
                return string.Empty;

            driversLicense = driversLicense.Replace(';', ',');
            return driversLicense.Trim(' ', ',');
        }

        private string GetSedcardFilename(int employeeId, string type)
        {
            return Server.MapPath(string.Format("~/Sedcards/{0}/{1}.json", employeeId, type));
        }

        [HttpPost]
        public virtual ActionResult _Save(SedcardViewModel model, int employeeId, string type)
        {
            var json = JsonConvert.SerializeObject(model, Formatting.Indented);
            var filename = GetSedcardFilename(employeeId, type);
            System.IO.File.WriteAllText(filename, json);
            return Content("ok");
        }

        [HttpPost]
        public virtual ActionResult _Reset(int employeeId, string field)
        {
            var employee = _dbContext.Employees
                .Where(e => e.Id == employeeId)
                .Include(e => e.Profile)
                .Single();

            var itknowledge = SelectListRepository.Retrieve("ITKnowledge", ViewData, null);

            var p = employee.Profile;
            switch (field)
            {
                case "ausbildung":
                    return Content(p.OtherCompletedApprenticeships);
                case "studium":
                    return Content(p.StudiesDetails);
                case "berufserfahrung ":
                    return Content(p.WorkExperience);
                case "softwarekenntnisse":
                    var s = itknowledge.SingleOrDefault(e => e.Value == "" + p.SoftwareKnownledge);
                    return Content((s != null ? s.Text + "\n\n" : "") + p.SoftwareKnownledgeDetails);
                case "hardwarekenntnisse":
                    var h = itknowledge.SingleOrDefault(e => e.Value == "" + p.HardwareKnownledge);
                    return Content((h != null ? h.Text + "\n\n" : "") + p.HardwareKnownledgeDetails);
                case "referenzen":
                    return Content(Referenzen(employee));
                default:
                    throw new ArgumentException("Invalid value " + field + " for parameter field", field);
            }
        }

        [HttpPost]
        public virtual ActionResult Pdf(int id, string type)
        {
            var tempFile = Path.GetTempFileName();
            GeneratePdf(Server, Url, tempFile, id, type);
            var bytes = System.IO.File.ReadAllBytes(tempFile);
            System.IO.File.Delete(tempFile);
            return File(bytes, "application/pdf", "Sedcard.pdf");
        }

        public static void GeneratePdf(HttpServerUtilityBase server, UrlHelper urlHelper, string path, int employeeId, string type)
        {
            var tempFile = Path.GetTempFileName();
            var wkhtmlPath = server.MapPath("~/App_Data/wkhtmltopdf/wkhtmltopdf.exe");
            var url = urlHelper.ActionAbsolute(MVC.MD.Sedcard.Display(employeeId, type));
            var process = Process.Start(wkhtmlPath,
                $"-O landscape --margin-left 25mm --margin-right 25mm --margin-top 20mm --margin-bottom 20mm --print-media-type \"{url}\" \"{tempFile}\"");
            process.WaitForExit(20 * 1000); // Maximal 20 Sekunden warten
            var backgroundPdfPath = server.MapPath("~/App_Data/Sedcard_Layout.pdf");
            var pdfTkPath = server.MapPath("~/App_Data/wkhtmltopdf/pdftk.exe");
            process = Process.Start(pdfTkPath,
                $"\"{tempFile}\" multibackground \"{backgroundPdfPath}\" output \"{path}\"");
            process.WaitForExit(20 * 1000);
            System.IO.File.Delete(tempFile);
        }

    }

    public class ImageInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Point Size { get; set; }
    }

    public class SedcardViewModel
    {
        [JsonIgnore]
        public string Vorname { get; set; }
        [JsonIgnore]
        public string Nachname { get; set; }
        public string Funktion { get; set; }
        public string Einsatzort { get; set; }
        [JsonIgnore]
        public string Alter { get; set; }
        [JsonIgnore]
        public string Groesse { get; set; }
        [JsonIgnore]
        public string Konfektionsgroesse { get; set; }
        [JsonIgnore]
        public string ShirtGroesse { get; set; }
        [JsonIgnore]
        public string Fuehrerschein { get; set; }
        [AllowHtml]
        public string Ausbildung { get; set; }
        [AllowHtml]
        public string Studium { get; set; }
        [AllowHtml]
        public string Berufserfahrung { get; set; }
        [AllowHtml]
        public string Sprachkenntnisse { get; set; }
        [AllowHtml]
        public string Softwarekenntnisse { get; set; }
        [AllowHtml]
        public string Hardwarekenntnisse { get; set; }
        [AllowHtml]
        public string SonstigeKenntnisse { get; set; }
        [AllowHtml]
        public string Referenzen { get; set; }
        [AllowHtml]
        public string Sonstiges { get; set; }
        [AllowHtml]
        public string Extra { get; set; }

        public bool PortraitPhotoVisible { get; set; }
        public bool FullBodyPhotoVisible { get; set; }
        public bool OtherPhotoVisible { get; set; }
        public bool EinsatzortVisible { get; set; }
        public bool FunktionVisible { get; set; }
        public bool AlterVisible { get; set; }
        public bool GroesseVisible { get; set; }
        public bool KonfektionsgroesseVisible { get; set; }
        public bool ShirtGroesseVisible { get; set; }
        public bool FuehrerscheinVisible { get; set; }
        public bool AusbildungVisible { get; set; }
        public bool StudiumVisible { get; set; }
        public bool BerufserfahrungVisible { get; set; }
        public bool SprachkenntnisseVisible { get; set; }
        public bool SoftwarekenntnisseVisible { get; set; }
        public bool HardwarekenntnisseVisible { get; set; }
        public bool SonstigeKenntnisseVisible { get; set; }
        public bool ReferenzenVisible { get; set; }
        public bool SonstigesVisible { get; set; }
        public bool ExtraVisible { get; set; }
    }

}