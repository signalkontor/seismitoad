﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using Microsoft.Ajax.Utilities;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class LocationTypeController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public LocationTypeController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: MD/LocationType
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = _dbContext.LocationTypes.Project().To<LocationTypeViewModel>();
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IList<LocationTypeViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var type = _dbContext.LocationTypes.Single(e => e.Id == model.Id);
                    type.Name = model.Name;
                }

                _dbContext.SaveChanges();
            }

            return Json(models.ToDataSourceResult(request));
        }

        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<LocationTypeViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var objectToDDelete = _dbContext.LocationTypes.Single(e => e.Id == model.Id);
                    _dbContext.LocationTypes.Remove(objectToDDelete);
                }
            }
            _dbContext.SaveChanges();

            return Json(models.ToDataSourceResult(request,ModelState));
        }

        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<LocationTypeViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var newType = new LocationType() {Name = model.Name};
                    _dbContext.LocationTypes.Add(newType);
                    _dbContext.SaveChanges();
                    model.Id = newType.Id;
                }
            }
            return Json(models.ToDataSourceResult(request));
        }
    }
}