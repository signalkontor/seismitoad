﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using MasterInstance.Services;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class EmployeeController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public EmployeeController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        //
        // GET: /MD/Employee/
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest]DataSourceRequest request, EmployeeSearchModel model, string nameOrMail)
        {
            IQueryable<SeismitoadModel.Employee> employees;
            if (string.IsNullOrEmpty(nameOrMail))
            {
                employees = new SearchService().Employees(_dbContext.Employees, model);
            }
            else
            {
                var parts = nameOrMail.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                employees = _dbContext.Employees;
                foreach(var part in parts)
                {
                    employees = employees.Where(e => e.Firstname.Contains(part) || e.Lastname.Contains(part) || e.Email.Contains(part));
                }
            }
            employees = employees.Include(e => e.Profile);

            var viewModel = employees.Project().To<EmployeeViewModel>().ToDataSourceResult(request);
            foreach (EmployeeViewModel data in viewModel.Data)
            {
                var user = Membership.GetUser(data.ProviderUserKey);
                if (user == null) continue;
                data.Username = user.UserName;
                data.Login = user.IsLockedOut ? "Gesperrt" : "Aktiv";
            }

            return Json(viewModel);
        }


        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IList<EmployeeViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var employee = _dbContext.Employees.Single(e => e.Id == model.Id);
                    Mapper.Map(model, employee);
                    if(string.IsNullOrWhiteSpace(employee.Title))
                        employee.Title = "";

                    var user = employee.GetMemberShipUser();
                    user.Email = model.Email;

                    Membership.UpdateUser(user); 
                }
                _dbContext.Configuration.ValidateOnSaveEnabled = false;
                _dbContext.SaveChanges();
            }

            return Json(models);
        }

        [HttpPost]
        public virtual ActionResult _ChangePassword(string password, Guid userKey)
        {
            var user = Membership.GetUser(userKey, false);
            user.UnlockUser();

            var tmpPw = user.ResetPassword();
            try
            {
                if (user.ChangePassword(tmpPw, password))
                {
                    Membership.UpdateUser(user);
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
            }
            catch
            {
                
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }
    }
}