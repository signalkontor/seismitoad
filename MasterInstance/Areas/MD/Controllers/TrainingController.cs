﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using Microsoft.Ajax.Utilities;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using Telerik.Web.Mvc;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class TrainingController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public TrainingController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        #region Trainings

        public virtual ActionResult Index()
        {
            ViewBag.Title = "Schulungen";
            return View();
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Trainings(int campaignId)
        {
            ViewBag.Title = Helpers.PageTitle(campaignId, "Schulungen");
            ViewBag.CampaignId = campaignId;
            return View("Index");
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            var allTrainings = _dbContext.Trainings.Where(e => true);

            if (campaignId > 0)
                allTrainings = allTrainings.Where(e => e.CampaignId == campaignId);

            return Json(allTrainings.Project().To<TrainingViewModel>().ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var projectNumber = model.ProjectNumber.Substring(2);
                    var customerNumber = model.ProjectNumber.Substring(0, 2);
                    var campaign = _dbContext.Campaigns.Single(e => e.Number == projectNumber && e.Customer.Number == customerNumber);

                    var newTraining = new Training
                    {
                        CampaignId = campaign.Id,
                        Campaign = campaign,
                        TrainingDays = new List<TrainingDay>(),
                        Description = model.Description,
                        Title = model.Title
                    };

                    _dbContext.Trainings.Add(newTraining);
                    _dbContext.SaveChanges();

                    model.Id = newTraining.Id;
                }
                
            }
            return Json(models.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var training = _dbContext.Trainings.Single(e => e.Id == model.Id);

                    training.CampaignId = model.CampaignId;
                    training.Description = model.Description;
                    training.Title = model.Title;
                }

                _dbContext.SaveChanges();
            }
            return Json(models.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var training = _dbContext.Trainings.Single(e => e.Id == model.Id);
                    _dbContext.Trainings.Remove(training);
                }

                _dbContext.SaveChanges();
            }
            return Json(models.ToDataSourceResult(request));
        }

        public virtual ActionResult _ReadCombobox()
        {
            var trainings = _dbContext.Trainings.Select(e => new { Value = e.Id, Text = e.Title });
            return Json(trainings, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TrainingsDay
        public virtual ActionResult Dates(int trainingId)
        {
            return View(_dbContext.Trainings.Single(e => e.Id == trainingId));
        }

        public virtual ActionResult _ReadDate([DataSourceRequest] DataSourceRequest request, int trainingId)
        {
            var trainings = _dbContext.TrainingDays.Where(e => e.Training.Id == trainingId).Project().To<TrainingDayViewModel>().ToDataSourceResult(request);
            return Json(trainings);
        }

        [HttpPost]
        public virtual ActionResult _UpdateDate([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingDayViewModel> models)
        {
            foreach (var model in models)
            {
                var thisDay = _dbContext.TrainingDays.Single(e => e.Id == model.Id);

                thisDay.MaxParticipants = model.MaxParticipants;
                thisDay.Remarks = model.Remarks;
                thisDay.Location = model.Location;
                thisDay.DateFrom = model.DateFrom;
                thisDay.DateTil = model.DateTil;
            }
            _dbContext.SaveChanges();

            return Json(models.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _CreateDate([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingDayViewModel> models, int trainingId)
        {
            foreach (var model in models)
            {
                var thisDay = new TrainingDay
                {
                    MaxParticipants = model.MaxParticipants,
                    DateFrom = model.DateFrom,
                    DateTil = model.DateTil,
                    Remarks = model.Remarks
                };

                _dbContext.Trainings.Single(e => e.Id == trainingId).TrainingDays.Add(thisDay);
                _dbContext.SaveChanges();

                model.Id = thisDay.Id;
            }
            
            return Json(models.ToDataSourceResult(request));
        }

        public virtual ActionResult _DestroyDate([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingDayViewModel> models)
        {
                foreach (var model in models)
                {
                    var thisDay = _dbContext.TrainingDays.Single(e => e.Id == model.Id);
                    _dbContext.TrainingDays.Remove(thisDay);
                }
                _dbContext.SaveChanges();
            
            return Json(models.ToDataSourceResult(request));
        }

        #endregion

    }
}