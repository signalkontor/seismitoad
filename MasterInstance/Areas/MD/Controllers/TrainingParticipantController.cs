﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.MD.Controllers
{
    public partial class TrainingParticipantController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;
        public TrainingParticipantController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: MD/TrainingParticipant
        public virtual ActionResult Index(int trainingDayId)
        {
            var day = _dbContext.TrainingDays.Include("Training").Single(e => e.Id == trainingDayId);

            return View(new EditTrainingParticipantsViewModel
            {
                DateId = trainingDayId,
                TrainingDayId = day.Id,
                TrainingId = day.TrainingId,
                Title = day.Training.Title,
                DateFrom = day.DateFrom,
                DateTil = day.DateTil
            });
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int dateId)
        {
            var participants = _dbContext.TrainingParticipants.Where(e => e.TrainingDayId == dateId);
            var result = participants.Project().To<TrainingParticipantViewModel>().ToDataSourceResult(request);
            var rowNumber = 1 + (request.Page - 1) * request.PageSize;
            foreach (TrainingParticipantViewModel viewModel in result.Data)
            {
                viewModel.RowNumber = rowNumber++;
            }
            return Json(result);
        }

        [HttpPost]
        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingParticipantViewModel> models)
        {
            foreach (var model in models)
            {
                var participant = _dbContext.TrainingParticipants.Single(e => 
                    e.CampaignEmployee.EmployeeId == model.EmployeeId && e.TrainingDayId == model.TrainingDayId);
                
                _dbContext.TrainingParticipants.Remove(participant);
                _dbContext.SaveChanges();
            }
            return Json(models.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IList<TrainingParticipantViewModel> models)
        {
            foreach (var model in models)
            {
                var participant = _dbContext.TrainingParticipants.Single(e => 
                    e.CampaignEmployee.EmployeeId == model.EmployeeId && e.TrainingDayId == model.TrainingDayId);

                participant.Travel = model.Travel;
                participant.TravelCost = model.TravelCost;
                participant.Hotel = model.Hotel;
                participant.Remark = model.Remark;
                participant.State = model.State;

                _dbContext.SaveChanges();
            }
            return Json(models.ToDataSourceResult(request));
        }
    }
}