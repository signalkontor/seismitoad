﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Models.ViewModels;
using MasterInstance.Services;
using Newtonsoft.Json;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Elmah;
using Hangfire;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class CampaignEmployeesController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;
        private const int SecondsBetweenMails = 6;

        public CampaignEmployeesController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private const string Query = @"
SELECT
	1 AS RowNumber,
	[CampaignEmployees].[EmployeeId],
	[CampaignEmployees].[Roles],
	'' AS RolesEdit,
	CAST(CASE WHEN Invited IS NULL AND InformationRequested IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS Highlighted,
	[Employees].[Firstname],
	[Employees].[Lastname],
	[EmployeeProfiles].[Street],
	[EmployeeProfiles].[PostalCode],
	[EmployeeProfiles].[City],
	Email,
 	[EmployeeProfiles].[PhoneMobileNo] AS Mobile,
	CASE
        -- Zuerst der Sonderfall, dass die Aquiseliste nur bis Accepted läuft
		WHEN @State = 'Any' AND @AcquisitionList = 1 AND Accepted IS NOT NULL THEN Accepted
		WHEN @State = 'Any' THEN StateSetDate
        WHEN @State = 'New' THEN StateSetDate
        WHEN @State = 'Invited' THEN Invited
        WHEN @State = 'NotInterested' THEN NotInterested
        WHEN @State = 'InformationRequested' THEN InformationRequested
        WHEN @State = 'InformationRejected' THEN InformationRejected
        WHEN @State = 'InformationSent' THEN InformationSent
        WHEN @State = 'Applied' THEN Applied
        WHEN @State = 'Accepted' THEN Accepted
        WHEN @State = 'Rejected' THEN Rejected
        WHEN @State = 'Deferred' THEN Deferred
        WHEN @State = 'ContractSent' THEN ContractSent
        WHEN @State = 'ContractAccepted' THEN ContractAccepted
        WHEN @State = 'ContractRejected' THEN ContractRejected
        WHEN @State = 'ContractCancelled' THEN ContractCancelled
        ELSE NULL
        END
    AS Date,
	[Employees].[State] AS EmployeeState,
    CASE WHEN @AcquisitionList = 1 AND Accepted IS NOT NULL THEN 'Accepted' ELSE[CampaignEmployees].[State] END as State,
	[CampaignEmployees].[State] as RealState,
	[CampaignEmployees].[Remark],
	[CampaignEmployees].[AddressChanged],
	ISNULL([CampaignEmployees].[Deployable], 'Keine Angabe') AS Deployable,
	[CampaignEmployees].[LocationsText] AS AppliedForLocations,
    CAST (
        CASE WHEN EXISTS
            (SELECT * FROM[CampaignEmployeeLocations] CL WHERE CL.[CampaignEmployee_CampaignId] = @CampaignId AND CL.[CampaignEmployee_EmployeeId] = [Employees].[Id])
        THEN 1 ELSE 0 END
    AS BIT) AS AppliedForLocationsBySelection,
    [EmployeeProfiles].[SkeletonContract],
	[EmployeeProfiles].[CopyIdentityCard],
	[EmployeeProfiles].[FreelancerQuestionaire],
	[EmployeeProfiles].[CopyTradeLicense] as ValidTradeLicense,
	[EmployeeProfiles].[ShirtSize],
	[EmployeeProfiles].[Height],
	[EmployeeProfiles].[DriversLicenseClass1] + ';' + [EmployeeProfiles].[DriversLicenseClass2] AS DriversLicense,
	[EmployeeProfiles].[Skype],
	[EmployeeProfiles].[PersonalDataComment] as ProfileRemark,
	[CampaignEmployees].[ContractAccepted]
FROM
    [CampaignEmployees] INNER JOIN
    [Employees] ON [Employees].[Id] = [CampaignEmployees].[EmployeeId] LEFT OUTER JOIN
    [EmployeeProfiles] ON [EmployeeProfiles].[Id] = [Employees].[Id]
WHERE
    [CampaignEmployees].[CampaignId]  = @CampaignId AND
	((@State = 'Any' AND @AcquisitionList = 1) OR (@State = 'Any' AND [CampaignEmployees].[Accepted] IS NOT NULL) OR [CampaignEmployees].[State] = @State OR (@State = 'Accepted' AND @AcquisitionList = 1 AND [CampaignEmployees].[Accepted] IS NOT NULL))
";

        // Das obige WHERE kann man so lesen:
        // 1. Die CampaignId muss stimmen (logisch!)
        // 2. Wenn es die Akquiseliste ist und der Status Any ist, dann nehmen wir alle Einträge
        // 3. Alternativ ist der Status entweder nicht Any oder es ist die Persoliste daraus ergeben sich
        // => 3a. Der Status muss genau übereinstimmen
        // => 3b oder der Status ist Any, dann nehmen wir alle Einträge ab Status Accepted ("Im Team").
        // Dann gibt es noch den Sonderfall 4 (oder 3c). Wenn der Status Accepted ist und es die Akquiseliste istm dann soll alles ab Accepted angezeigt werden
        
        internal const string TrainingMappingCookiePrefix = "TrainingsForCampaign_";

        private static bool Contains(string str, string containedStr)
        {
            return str.IndexOf(containedStr, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }

        public virtual ActionResult _Read([DataSourceRequest]DataSourceRequest request, int campaignId, string state, string view, string nameOrMail)
        {
            var promoterList = view == "Promoter";

            _dbContext.Database.CommandTimeout = 180; // 3 minutes
            var data = _dbContext.Database.SqlQuery<AcquisitionRowViewModel>(Query,
                new SqlParameter("@CampaignId", campaignId),
                new SqlParameter("@State", state),
                new SqlParameter("@AcquisitionList", !promoterList)).ToList();

            #region Filtern anhand des Textfelds
            if(!string.IsNullOrWhiteSpace(nameOrMail))
            {
                var nameOrEmailParts = nameOrMail.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                data = data
                    .Where(row => nameOrEmailParts.All(p => Contains(row.Firstname, p) || Contains(row.Lastname, p) || Contains(row.Email ?? "", p)))
                    .ToList();
            }
            #endregion

            // Im Falle der Promoterliste müssen wir noch die Schulungen holen
            if (promoterList)
            {
                var cookie = Request.Cookies.Get(TrainingMappingCookiePrefix + campaignId);
                var trainingIds = cookie?.Value != null
                    ? JsonConvert.DeserializeAnonymousType(cookie.Value, new int[0])
                    : new int[0];

                var trainings = _dbContext.TrainingParticipants
                    .Where(e => trainingIds.Contains(e.TrainingDay.TrainingId))
                    .Select(e => new
                    {
                        e.TrainingDay.TrainingId,
                        e.CampaignEmployee.EmployeeId,
                        e.TrainingDay.DateFrom,
                        e.State
                    })
                    .ToLookup(e => e.EmployeeId);

                foreach (var row in data)
                {
                    var employeeTrainings = trainings[row.EmployeeId];
                    foreach(var training in employeeTrainings)
                    {
                        var pos = Array.IndexOf(trainingIds, training.TrainingId);
                        #region Setzen des entsprechenden Properties
                        switch(pos)
                        {
                            case 0:
                                row.Training0 = training.State;
                                row.TrainingDate0 = training.DateFrom;
                                break;
                            case 1:
                                row.Training1 = training.State;
                                row.TrainingDate1 = training.DateFrom;
                                break;
                            case 2:
                                row.Training2 = training.State;
                                row.TrainingDate2 = training.DateFrom;
                                break;
                            case 3:
                                row.Training3 = training.State;
                                row.TrainingDate3 = training.DateFrom;
                                break;
                            case 4:
                                row.Training4 = training.State;
                                row.TrainingDate4 = training.DateFrom;
                                break;
                            case 5:
                                row.Training5 = training.State;
                                row.TrainingDate5 = training.DateFrom;
                                break;
                            case 6:
                                row.Training6 = training.State;
                                row.TrainingDate6 = training.DateFrom;
                                break;
                            case 7:
                                row.Training7 = training.State;
                                row.TrainingDate7 = training.DateFrom;
                                break;
                            case 8:
                                row.Training8 = training.State;
                                row.TrainingDate8 = training.DateFrom;
                                break;
                            case 9:
                                row.Training9 = training.State;
                                row.TrainingDate9 = training.DateFrom;
                                break;
                            case 10:
                                row.Training10 = training.State;
                                row.TrainingDate10 = training.DateFrom;
                                break;
                            case 11:
                                row.Training11 = training.State;
                                row.TrainingDate11 = training.DateFrom;
                                break;
                            case 12:
                                row.Training12 = training.State;
                                row.TrainingDate12 = training.DateFrom;
                                break;
                            case 13:
                                row.Training13 = training.State;
                                row.TrainingDate13 = training.DateFrom;
                                break;
                            case 14:
                                row.Training14 = training.State;
                                row.TrainingDate14 = training.DateFrom;
                                break;
                        }
                        #endregion
                    }
                }
            }

            var result = data.ToDataSourceResult(request);
            var rowNumber = 1 + (request.Page - 1) * request.PageSize;

            foreach (AcquisitionRowViewModel row in result.Data)
            {
                row.RowNumber = rowNumber++;
            }
            return Json(result);
        }

        // Bis zu 10 MB JSON erlauben. Das reicht (geschätzt) für mehr als 6000 Einträge in der Liste
        public virtual new JsonResult Json(object data, JsonRequestBehavior jsonRequestBehavior = JsonRequestBehavior.DenyGet)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8,
                MaxJsonLength = 10 * 1024 * 1024,
                JsonRequestBehavior = jsonRequestBehavior
            };
        }

        private void SetAllNull(CampaignEmployee campaignEmployee)
        { 
            campaignEmployee.Invited = null;
            campaignEmployee.NotInterested = null;
            campaignEmployee.InformationRequested = null;
            campaignEmployee.InformationSent = null;
            campaignEmployee.InformationRejected = null;
            campaignEmployee.Applied = null;
            campaignEmployee.Rejected = null;
            campaignEmployee.Deferred = null;
            campaignEmployee.Accepted = null;
            campaignEmployee.ContractSent = null;
            campaignEmployee.ContractCancelled = null;
            campaignEmployee.ContractRejected = null;
            campaignEmployee.ContractAccepted = null;
        }

        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, int campaignid, AcquisitionRowViewModel model)
        {
            model.RealState = model.State;
            var campaignEmployee =
                _dbContext.CampaignEmployees.Single(e => e.EmployeeId == model.EmployeeId && e.CampaignId == campaignid);

            var now = LocalDateTime.Now;

            // Nur wenn sich der Status geändert hat was machen. Es kann nämlich sein, dass die Bemerkung geändert wurde und dann
            // soll sich am State auch nichts ändern.
            if (campaignEmployee.State != model.State)
            {
                // Da NotInterested alles "sticht" müssen wir es bei jeder manuellen Änderung auf null setzen
                // da der State sonst immer NotInterested bliebe
                campaignEmployee.NotInterested = null;
                switch (model.State)
                {
                    case "New":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.StateSetDate = now;
                        break;
                    case "Invited":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Invited = now;
                        break;
                    case "NotInterested":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.NotInterested = now;
                        break;
                    case "InformationRequested":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.InformationRequested = now;
                        break;
                    case "InformationSent":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.InformationSent = now;
                        break;
                    case "InformationRejected":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.InformationRejected = now;
                        break;
                    case "Applied":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Applied = now;
                        break;
                    case "Rejected":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Rejected = now;
                        break;
                    case "Deferred":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Deferred = now;
                        break;
                    case "Accepted":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Accepted = now;
                        break;
                    case "ContractSent":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Accepted = now;
                        campaignEmployee.ContractSent = now;
                        break;
                    case "ContractCancelled":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Accepted = now;
                        campaignEmployee.ContractCancelled = now;
                        break;
                    case "ContractRejected":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Accepted = now;
                        campaignEmployee.ContractRejected = now;
                        break;
                    case "ContractAccepted":
                        SetAllNull(campaignEmployee);
                        campaignEmployee.Accepted = now;
                        campaignEmployee.ContractAccepted = now;
                        break;
                    default:
                        throw new InvalidOperationException("Unknown state: " + model.State);
                }
            }
            campaignEmployee.Deployable = model.Deployable;
            campaignEmployee.Remark = model.Remark;
            campaignEmployee.Roles = model.RolesEdit != null ? string.Join(", ", model.RolesEdit) : "";
            model.Roles = campaignEmployee.Roles;   

            _dbContext.Configuration.ValidateOnSaveEnabled = false;
            _dbContext.SaveChanges();
            return Json(new[] {model}.ToDataSourceResult(request));
        }

        public virtual ActionResult _Search([DataSourceRequest]DataSourceRequest request, EmployeeSearchModel model)
        {
            var savedSearchPath = GetCampaignAssetPath(model.CampaignId);
            Directory.CreateDirectory(savedSearchPath);
            savedSearchPath = Path.Combine(savedSearchPath, "AcquisitionSearch.txt");
            System.IO.File.WriteAllText(savedSearchPath, JsonConvert.SerializeObject(model, Formatting.Indented));

            var employees = new SearchService()
                .Employees(_dbContext.AvailableEmployees, model)
                .Include(e => e.Profile)
                // Ignore Employees that are already CampaignEmployees for this campaign
                .Where(e => e.CampaignEmployees.All(i => i.CampaignId != model.CampaignId));

            var result = employees.Project().To<AcquisitionSearchResultRowViewModel>();
            return Json(result.ToDataSourceResult(request));
        }

        public virtual ActionResult _SaveColumns(int id, string columns)
        {
            var dir = GetCampaignAssetPath(id);
            System.IO.File.WriteAllText(Path.Combine(dir, "AcquisitionColumns.txt"), columns);
            return Content("OK");
        }

        private string GetCampaignAssetPath(int campaignId)
        {
            return Server.MapPath("~/CampaignAssets/" + campaignId);
        }

        #region Preselect
        [HttpPost]
        public virtual ActionResult _Preselect(EmployeeSearchModel model)
        {
            _dbContext.Configuration.ValidateOnSaveEnabled = false;
            _dbContext.Configuration.AutoDetectChangesEnabled = false;
            var employeeIds = new SearchService()
                .Employees(_dbContext.AvailableEmployees, model)
                // Ignore Employees that are already CampaignEmployees for this campaign
                .Where(e => e.CampaignEmployees.All(i => i.CampaignId != model.CampaignId))
                .Select(e => e.Id)
                .ToArray();

            const int blockSize = 50;
            var startIndex = 0;
            var now = LocalDateTime.Now;
            while (startIndex < employeeIds.Length)
            {
                var block = employeeIds.Skip(startIndex).Take(blockSize).Select(employeeId => string.Format("{0}, {1}, @StateSetDate", model.CampaignId, employeeId));
                var query = string.Format("INSERT INTO CampaignEmployees (CampaignId, EmployeeId, StateSetDate) VALUES ({0})", string.Join("), (", block));
                _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@StateSetDate", now));
                startIndex += blockSize;
            }
            return Content("OK");
        }
        #endregion

        private static void SaveJobTracking(SeismitoadDbContext dbContext, string jobId, string mailType, CampaignEmployee campaignEmployee)
        {
            var campaignId = campaignEmployee.CampaignId;
            var employeeId = campaignEmployee.EmployeeId;
            var firstname = campaignEmployee.Employee.Firstname;
            var lastname = campaignEmployee.Employee.Lastname;

            dbContext.Database.ExecuteSqlCommand(
                "INSERT INTO CampaignEmployeeMails (JobId, MailType, CampaignId, EmployeeId, Firstname, Lastname, Done, QueuedAt) VALUES (@JobId, @MailType, @CampaignId, @EmployeeId, @Firstname, @Lastname, 0, GETDATE())",
                new SqlParameter("@JobId", jobId), new SqlParameter("@MailType", mailType),
                new SqlParameter("@CampaignId", campaignId), new SqlParameter("@EmployeeId", employeeId),
                new SqlParameter("@Firstname", firstname), new SqlParameter("@Lastname", lastname));
        }

        public static void SendMail(string viewName, int campaignId, int employeeId, string senderUsername)
        {
            var emailService = Helpers.GetEmailService();
            var recipient = "unknown recepient";
            var subject = $"Sending {viewName} for campaign {campaignId} to promoter {employeeId}";
            try
            {
                using (var dbContext = new SeismitoadDbContext())
                {
                    var campaignEmployee =
                        dbContext.CampaignEmployees.Single(e => e.CampaignId == campaignId && e.EmployeeId == employeeId);

                    var mail = new AcquisitionEmail(campaignEmployee, viewName, senderUsername);
                    recipient = mail.To;
                    if (mail.Error == null)
                    {
                        emailService.Send(mail);
                        dbContext.Database.ExecuteSqlCommand(
                            "UPDATE CampaignEmployeeMails SET Done = 1 WHERE JobId = @JobId",
                            new SqlParameter("@JobId", JobContext.JobId));
                    }
                    else
                    {
                        dbContext.Database.ExecuteSqlCommand(
                            "UPDATE CampaignEmployeeMails SET Done = 1, Error = @Error WHERE JobId = @JobId",
                            new SqlParameter("@Error", mail.Error), new SqlParameter("@JobId", JobContext.JobId));
                    }
                }
            }
            catch (Exception exception)
            {
                var loggedException = new Exception($"Failed sending e-mail with subject '{subject}' to {recipient}", exception);
                ErrorLog.GetDefault(null).Log(new Error(loggedException));
                throw;
            }
        }

        public class BackgroundParameters
        {
            public int Counter { get; set; }
            public string Username { get; set; }
            public DateTime Now { get; set; }
            public string State { get; set; }
            public string ContractPath { get; set; }
        }

        private void UpdateState(AcquisitionActionParameters model, string column)
        {
            const int blockSize = 50;
            var startIndex = 0;
            var now = LocalDateTime.Now;
            while (startIndex < model.EmployeeIds.Length)
            {
                var block = model.EmployeeIds.Skip(startIndex).Take(blockSize);
                var query = column == "Invited"
                    ? $"UPDATE CampaignEmployees SET Invited = @CurrentDateTime, NotInterested = null WHERE CampaignId = @CampaignId AND EmployeeId in ({string.Join(", ", block)})"
                    : $"UPDATE CampaignEmployees SET {column} = @CurrentDateTime WHERE CampaignId = @CampaignId AND EmployeeId in ({string.Join(", ", block)})";
                _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@CampaignId", model.CampaignId), new SqlParameter("@CurrentDateTime", now));
                startIndex += blockSize;
            }
        }

        private ActionResult Esitmate(AcquisitionActionParameters model, string text)
        {
            var eta = LocalDateTime.Now.AddSeconds(20 + model.EmployeeIds.Length * SecondsBetweenMails);
            return PartialView("_UpdateResult", new[]
{
                new AcquisitionUpdateResultViewModel
                {
                    Firstname = "Alle gewählten Promoter",
                    Result = $"{text}. Vorraussichtlich abgeschlossen um {eta:HH:mm}. Dieses Fenster kann geschlossen werden."
                }
            });

        }

        private void SendMailInBackground(AcquisitionActionParameters model, string state)
        {
            var contractPath = (state == "ContractSent")
                ? Server.MapPath("~/CampaignAssets/" + model.CampaignId + "/GeneratedContracts")
                : null;

            var wkhtmlPath = Server.MapPath("~/App_Data/wkhtmltopdf/wkhtmltopdf.exe");
            var pdfTkPath = Server.MapPath("~/App_Data/wkhtmltopdf/pdftk.exe");
            var backgroundPdfPath = Server.MapPath("~/App_Data/Projektauftrag_blanko.pdf");
            var url = Url.ActionAbsolute(MVC.CM.Contract.Display(model.CampaignId, model.Contract));

            var task = new Task((object arg) =>
            {
                var data = arg as BackgroundParameters;
                UpdateCampaignEmployeesBackground(model, (dbContext, campaignEmployee) =>
                {
                    if (data.ContractPath != null)
                    {
                        var employeeId = campaignEmployee.EmployeeId;
                        var filename = Path.Combine(data.ContractPath, employeeId + ".pdf");
                        Directory.CreateDirectory(Path.GetDirectoryName(filename));
                        ContractController.GeneratePdf(filename, wkhtmlPath, pdfTkPath, backgroundPdfPath, url + "&employeeId=" + employeeId);

                    }
                    var jobId = BackgroundJob.Schedule(
                        () => SendMail(data.State, campaignEmployee.CampaignId, campaignEmployee.EmployeeId, data.Username),
                        TimeSpan.FromSeconds(10 + data.Counter * SecondsBetweenMails));

                    SaveJobTracking(dbContext, jobId, data.State, campaignEmployee);
                    data.Counter++;
                });
            }, new BackgroundParameters { Now = LocalDateTime.Now, Counter = 0, Username = User.Identity.Name, State = state, ContractPath = contractPath });
            task.Start();
        }

        #region Reused Actions
        private ActionResult Invite(AcquisitionActionParameters model)
        {
            UpdateState(model, "Invited");
            SendMailInBackground(model, "JobOffering");
            return Esitmate(model, "Job-Hinweis wird gesendet");
        }

        private ActionResult SendJobInfo(AcquisitionActionParameters model)
        {
            UpdateState(model, "InformationSent");
            SendMailInBackground(model, "JobInfo");
            return Esitmate(model, "Job-Info wird gesendet");
        }

        private ActionResult InformationRejected(AcquisitionActionParameters model)
        {
            UpdateState(model, "InformationRejected");
            SendMailInBackground(model, "InformationRejected");
            return Esitmate(model, "Job-Info abgelehnt-Mail wird gesendet");
        }

        private ActionResult Rejected(AcquisitionActionParameters model)
        {
            UpdateState(model, "Rejected");
            SendMailInBackground(model, "Rejected");
            return Esitmate(model, "Bewerbung abgelehnt-Mail wird gesendet");
        }

        private ActionResult Accepted(AcquisitionActionParameters model)
        {
            UpdateState(model, "Accepted");
            SendMailInBackground(model, "Accepted");
            return Esitmate(model, "Bewerbung akzeptiert-Mail wird gesendet");
        }

        private ActionResult Deferred(AcquisitionActionParameters model)
        {
            UpdateState(model, "Deferred");
            SendMailInBackground(model, "Deferred");
            return Esitmate(model, "Bewerbung zurückgestellt-Mail wird gesendet");
        }

        private ActionResult ContractSent(AcquisitionActionParameters model)
        {
            UpdateState(model, "ContractSent");
            SendMailInBackground(model, "ContractSent");
            return Esitmate(model, "Projektauftrag bereitgestellt-Mail wird gesendet");
        }

        private ActionResult ContractCancelled(AcquisitionActionParameters model)
        {
            UpdateState(model, "ContractCancelled");
            // Storno, hier manuelle Mail
            var result = _dbContext.Employees
                .Where(e => model.EmployeeIds.Contains(e.Id))
                .Select(e => new AcquisitionUpdateResultViewModel
                {
                    Firstname = e.Firstname,
                    Lastname = e.Lastname,
                    Result = "Projektauftrag storniert. Hinweis: Storno-Mail manuell schreiben!"
                });
            return PartialView("_UpdateResult", result);
        }

        // Das ist glaube ich eine Hilfs-Action, wenn man mal manuell Verträge neu erstellen muss.
        public virtual ActionResult GenerateContract(int campaignId, int employeeId, string contract)
        {
            var path = Server.MapPath("~/CampaignAssets/" + campaignId + "/GeneratedContracts");
            var filename = Path.Combine(path, employeeId + ".pdf");
            ContractController.GeneratePdf(Server, Url, filename, campaignId, contract, employeeId);
            return Content("Done");
        }

        #endregion

        #region New
        [HttpPost]
        public virtual ActionResult _NewButton1(AcquisitionActionParameters model)
        {
            return Invite(model);
        }
        #endregion

        #region Invited
        [HttpPost]
        public virtual ActionResult _InvitedButton1(AcquisitionActionParameters model)
        {
            return Invite(model);
        }
        #endregion

        #region NotInterested
        [HttpPost]
        public virtual ActionResult _NotInterestedButton1(AcquisitionActionParameters model)
        {
            return Invite(model);
        }
        #endregion

        #region InformationRequested
        [HttpPost]
        public virtual ActionResult _InformationRequestedButton1(AcquisitionActionParameters model)
        {
            return SendJobInfo(model);
        }

        [HttpPost]
        public virtual ActionResult _InformationRequestedButton2(AcquisitionActionParameters model)
        {
            return InformationRejected(model);
        }

        [HttpPost]
        public virtual ActionResult _InformationRejectedButton1(AcquisitionActionParameters model)
        {
            return SendJobInfo(model);
        }
        #endregion

        #region InformationSent
        [HttpPost]
        public virtual ActionResult _InformationSentButton1(AcquisitionActionParameters model)
        {
            return SendJobInfo(model);
        }
        #endregion

        #region Applied
        [HttpPost]
        public virtual ActionResult _AppliedButton1(AcquisitionActionParameters model)
        {
            return Accepted(model);
        }

        [HttpPost]
        public virtual ActionResult _AppliedButton2(AcquisitionActionParameters model)
        {
            return Rejected(model);
        }

        [HttpPost]
        public virtual ActionResult _AppliedButton3(AcquisitionActionParameters model)
        {
            return Deferred(model);
        }
        #endregion

        #region Accepted
        [HttpPost]
        public virtual ActionResult _AcceptedButton1(AcquisitionActionParameters model)
        {
            return ContractSent(model);
        }
        #endregion

        #region Rejected
        [HttpPost]
        public virtual ActionResult _RejectedButton1(AcquisitionActionParameters model)
        {
            return Accepted(model);
        }

        [HttpPost]
        public virtual ActionResult _RejectedButton2(AcquisitionActionParameters model)
        {
            return Deferred(model);
        }
        #endregion

        #region Deferred
        [HttpPost]
        public virtual ActionResult _DeferredButton1(AcquisitionActionParameters model)
        {
            return Accepted(model);
        }

        [HttpPost]
        public virtual ActionResult _DeferredButton2(AcquisitionActionParameters model)
        {
            return Rejected(model);
        }
        #endregion

        #region ContractSent
        [HttpPost]
        public virtual ActionResult _ContractSentButton1(AcquisitionActionParameters model)
        {
            return ContractSent(model);
        }
        #endregion

        #region ContractRejected
        // In diesem Status ist nur eine manuelle Statusänderung möglich
        #endregion

        #region ContractCancelled
        [HttpPost]
        public virtual ActionResult _ContractCancelledButton1(AcquisitionActionParameters model)
        {
            return Accepted(model);
        }
        #endregion

        #region ContractAccepted
        [HttpPost]
        public virtual ActionResult _ContractAcceptedButton1(AcquisitionActionParameters model)
        {
            // Storno, hier manuelle Mail
            return ContractCancelled(model);
        }
        #endregion

        private static void UpdateCampaignEmployeesBackground(AcquisitionActionParameters model, Action<SeismitoadDbContext, CampaignEmployee> action)
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                foreach (var employeeId in model.EmployeeIds)
                {
                    var id = employeeId;
                    var campaignEmployee = dbContext.CampaignEmployees
                        .Where(e => e.CampaignId == model.CampaignId && e.EmployeeId == id)
                        .Include(e => e.Employee)
                        .Include(e => e.Campaign.Customer)
                        .Single();

                    try
                    {
                        action(dbContext, campaignEmployee);
                    }
                    catch (Exception e)
                    {
                        ErrorLog.GetDefault(null).Log(new Error(e));
                    }
                }
            }
        }

        public virtual ActionResult _Trainings(int? campaignId, int employeeId)
        {
            var specificCampaign = campaignId.HasValue;
            var trainignDays = _dbContext.TrainingDays
                .Where(e => e.Participants.All(i => i.CampaignEmployee.EmployeeId != employeeId));

            if (specificCampaign) trainignDays = trainignDays.Where(e => e.Training.CampaignId == campaignId);

            var model = trainignDays.Select(e => new
                {
                    e.Id,
                    e.Training.Title,
                    e.DateFrom,
                    e.DateTil,
                    e.Location,
                    e.Remarks,
                    e.MaxParticipants,
                    ParticipantsCount = e.Participants.Count
                });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult _AddToTraining(int campaignId, int employeeId, int trainingDayId)
        {
            var trainingDay = _dbContext.TrainingDays.Single(e => e.Id == trainingDayId);
            var trainingId = trainingDay.TrainingId;

            // Dieser Code muss ausgeführt werden bevor wir den Teilnehmer hinzufügen, denn sonst würden wir den gerade zugewiesen Teilnehmer
            // finden und kämen zum Schluss, dass dies nicht der erste Teilnehmer ist.
            var firstEmployeeForThisTraining =
                // Erster ist man, wenn es noch keinen Teilnehmer für diese TrainingId gibt oder wenn die Teilnehmer aus einem anderen Projekt stammen
                _dbContext.TrainingParticipants.All(e => e.TrainingDay.TrainingId != trainingId || e.CampaignEmployee.CampaignId != campaignId);

            var previousParticipation = _dbContext.TrainingParticipants
                .SingleOrDefault(e => e.CampaignEmployee.EmployeeId == employeeId && e.TrainingDay.TrainingId == trainingId);

            // Wenn der Promoter für dieses Training schon für einen anderen Tag eingetragen ist dies erst löschen
            if (previousParticipation != null)
            {
                _dbContext.TrainingParticipants.Remove(previousParticipation);
                _dbContext.SaveChanges();
            }

            var trainingParticipant = new TrainingParticipant
            {
                TrainingDayId = trainingDayId,
                CampaignEmployee = _dbContext.CampaignEmployees.Single(e => e.CampaignId == campaignId && e.EmployeeId == employeeId),
                State = "zugewiesen"
            };

            _dbContext.TrainingParticipants.Add(trainingParticipant);
            _dbContext.SaveChanges();

            var traininDayIds = _dbContext.Trainings
                .Where(e => e.TrainingDays.SelectMany(i => i.Participants).Any(i => i.CampaignEmployee.CampaignId == campaignId))
                .OrderBy(e => e.Title)
                .Select(e => e.Id).ToList();
            SetTrainingCookie(Response, campaignId, traininDayIds);

            return Content(firstEmployeeForThisTraining ? "reload" : "ok");
        }

        internal static void SetTrainingCookie(HttpResponseBase response, int campaignId, IEnumerable<int> trainingDayIds)
        {
            var cookie = new HttpCookie(TrainingMappingCookiePrefix + campaignId,
                JsonConvert.SerializeObject(trainingDayIds))
            {
                Expires = DateTime.Now.AddMonths(1),
            };
            response.SetCookie(cookie);
        }
        
        public virtual ActionResult _AppliedLocations(int campaignId, int employeeId)
        {
            var trainignDays = _dbContext.CampaignEmployees
                .Where(e => e.CampaignId == campaignId && e.EmployeeId == employeeId)
                .SelectMany(e => e.Locations)
                .Select(e => new
                {
                    e.Name,
                    e.Street,
                    e.PostalCode,
                    e.City,
                    Channel = e.LocationGroup.Name
                });

            return Json(trainignDays, JsonRequestBehavior.AllowGet);
        }
    }

    public static class GridExtensions
    {
        public static GridBoundColumnBuilder<TModel> BoundEx<TModel, TValue>(
            this GridColumnFactory<TModel> column,
            Expression<Func<TModel, TValue>> expression,
            ISet<string> visibleColumns) where TModel : class
        {
            var property = expression.Body.ToString();
            var start = property.IndexOf(".");
            property = property.Substring(start + 1);
            return column.Bound(expression).Hidden(!visibleColumns.Contains(property));
        }
    }
}