﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper.QueryableExtensions;
using Hangfire;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using System.Data.SqlClient;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class AssignController : Controller
    {
        #region Warn- und Fehlermeldungen
        private const string DeleteMessageSingle = "Dieser Einsatz liegt in der Vergangenheit. " +
                                                   "Falls der Promoter bereits ein Reporting für diesen Einsatz eingegeben hat wird es gelöscht. " +
                                                   "Soll dieser Promoter wirklich von diesem Einsatz entfernt werden?";

        private const string DeleteMessageMultipleFormat = "Dieser Einsatz sowie {0} liegen in der Vergangenheit. " +
                                                           "Falls der Promoter bereits Reportings für diese Einsätze eingegeben hat werden diese gelöscht. " +
                                                           "Soll dieser Promoter wirklich von diesen Einsätzen entfernt werden?";

        private const string UpdateMessageSingle = "Dieser Einsatz liegt in der Vergangenheit. " +
                                                   "Falls der bisherige Promoter bereits ein Reporting für diesen Einsatz eingegeben hat wird es gelöscht. " +
                                                   "Soll der bisherige Promoter wirklich ersetzt werden?";

        private const string UpdateMessageMultipleFormat = "Dieser Einsatz sowie {0} liegen in der Vergangenheit. " +
                                                           "Falls der bisherige Promoter bereits Reportings für diese Einsätze eingegeben hat werden diese gelöscht. " +
                                                           "Soll der bisherige Promoter wirklich ersetzt werden?";
        #endregion

        private readonly SeismitoadDbContext _dbContext;

        public AssignController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [MenuArea(MenuArea.Project)]
        //[OutputCache(Duration = 240, Location = OutputCacheLocation.Any, VaryByParam = "*")]
        public virtual ActionResult Index(int campaignId)
        {
            return View(campaignId);
        }

        [OutputCache(NoStore = true, Duration = 0, Location = OutputCacheLocation.None)]
        public virtual ActionResult _AssignedEmployee(int assignmentId, int employeeId)
        {
            var assignment = _dbContext.Assignments.Single(e => e.Id == assignmentId);
            var assignedEmployee = assignment.AssignedEmployees.SingleOrDefault(e => e.EmployeeId == employeeId);

            var firstname = "";
            var lastname = "";
            var roleIds = new [] {1};
            if (assignedEmployee != null)
            {
                firstname = assignedEmployee.Employee.Firstname;
                lastname = assignedEmployee.Employee.Lastname;
                roleIds = assignedEmployee.AssignmentRoles.Select(e => e.Id).ToArray();
            }

            return Json(new
            {
                firstname,
                lastname,
                roleIds,
                dateStart = assignment.DateStart.ToUniversalTime().ToString("O"),
                dateEnd = assignment.DateEnd.ToUniversalTime().ToString("O"),
                location = assignment.CampaignLocation.Location.FriendlyName,
                notes = assignment.Notes,
                marker = assignment.Marker
            }, JsonRequestBehavior.AllowGet);
        }

        private static void FilterWeekdays(ref IQueryable<IntermediateLocationRowViewModelNew> rows, ICollection<IFilterDescriptor> filters)
        {
            var filtersToRemove = new List<IFilterDescriptor>();
            foreach (var filter in filters)
            {
                var descriptor = filter as FilterDescriptor;
                if (descriptor != null)
                {
                    if(!(descriptor.Value is bool))
                        continue;

                    var desired = (bool) descriptor.Value;

                    switch (descriptor.Member)
                    {
                        case "Monday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 1) == desired);
                            break;
                        case "Tuesday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 2) == desired);
                            break;
                        case "Wednesday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 3) == desired);
                            break;
                        case "Thursday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 4) == desired);
                            break;
                        case "Friday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 5) == desired);
                            break;
                        case "Saturday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 6) == desired);
                            break;
                        case "Sunday":
                            rows = rows.Where(row => row.Assignments.Any(e => SqlFunctions.DatePart("weekday", e.DateStart) == 7) == desired);
                            break;
                        default:
                            descriptor = null;
                            break;
                    }
                    if(descriptor != null)
                        filtersToRemove.Add(descriptor);
                }
                else if (filter is CompositeFilterDescriptor)
                {
                    FilterWeekdays(ref rows, ((CompositeFilterDescriptor) filter).FilterDescriptors);
                }
            }
            foreach (var filterDescriptor in filtersToRemove)
            {
                filters.Remove(filterDescriptor);
            }
        }

        public class FakeAggregate
        {
            public int Value { get; set; }
            public string Member { get; set; }
            public int FormattedValue { get; set; }
            public int ItemCount { get; set; }
            public string Caption { get; set; }
            public string FunctionName { get; set; }
            public string AggregateMethodName { get; set; }
        }

        public virtual async System.Threading.Tasks.Task<ActionResult> _Assignments([DataSourceRequest]DataSourceRequest request, AssignFilterModel filterModel, bool alternateEnumeration = false)
        {
            _dbContext.Configuration.AutoDetectChangesEnabled = false;
            _dbContext.Configuration.LazyLoadingEnabled = false;
            _dbContext.Database.CommandTimeout = 180; // 3 minutes
            _dbContext.RecompileAllQueries = true;

            var start = filterModel.Start;
            Debug.Assert(start.DayOfWeek == DayOfWeek.Monday);

            var end = start.AddDays(7);

            var dataFromView = _dbContext.ViewAssignments
                .Where(e =>
                    e.CampaignId == filterModel.CampaignId &&
                    e.DateStart >= start && e.DateStart < end &&
                    e.State != AssignmentState.Deleted
                );

            if (filterModel.L != null && filterModel.L.Length > 0)
            {
                dataFromView = dataFromView.Where(e => filterModel.L.Contains(e.LocationId));
            }

            if (filterModel.F != null && filterModel.F.Length > 0)
            {
                var assigned = filterModel.F.Contains(AssignmentFilter.Assigned);
                var notFullyAssigned = filterModel.F.Contains(AssignmentFilter.Open);
                if (assigned != notFullyAssigned)
                {
                    // Wir müssen nur etwas filtern, wenn nur einer der beiden Filter gewählt ist. Sind beide gewählt heben
                    // sie sich auf, sind beide nicht gewählt auch.

                    dataFromView = assigned
                        ? dataFromView.Where(e => e.AssignedEmployeeCount >= e.EmployeeCount)
                        : dataFromView.Where(e => e.AssignedEmployeeCount < e.EmployeeCount && e.State == AssignmentState.Planned);
                }

                var confirmed = filterModel.F.Contains(AssignmentFilter.Confimed);
                var unconfirmed = filterModel.F.Contains(AssignmentFilter.Unconfimed);
                var rejected = filterModel.F.Contains(AssignmentFilter.Rejected);

                var allTrue = confirmed && unconfirmed && rejected;
                var noneTrue = !confirmed && !unconfirmed && !rejected;

                if (!allTrue && !noneTrue)
                {
                    // Wir müssen nur etwas filtern, solange maximal 2 der Filter gewählt sind.
                    // Sind alle oder keiner gewählt heben sie sich auf.
                    var allowedColors = new List<string>(3);
                    if (confirmed) allowedColors.Add("green");
                    if (unconfirmed) allowedColors.AddRange(new[] { "blue", "pink" });
                    if (rejected) allowedColors.Add("red");
                    dataFromView = dataFromView.Where(e => allowedColors.Contains(e.Color));
                }

                if (filterModel.F.Contains(AssignmentFilter.Marked))
                {
                    dataFromView = dataFromView.Where(e => e.Marker > 0);
                }
            }

            var campaignLocations = _dbContext.CampaignsLocations
                .Where(e => e.CampaignId == filterModel.CampaignId);

            if (filterModel.L != null && filterModel.L.Length > 0)
            {
                campaignLocations = campaignLocations.Where(e => filterModel.L.Contains(e.LocationId));
            }

            var data = from campaignLocation in campaignLocations
                       join assignment in dataFromView
                           on campaignLocation.LocationId equals assignment.LocationId
                           into tmp
                       from assignment in tmp.DefaultIfEmpty()
                       select new { CampaignLocation = campaignLocation, Assignment = assignment };

            var gridModel = data
                .GroupBy(e => e.CampaignLocation, e => e.Assignment)
                .Select(g => new
                IntermediateLocationRowViewModelNew
                {
                    LocationId = g.Key.LocationId,
                    Name = g.Key.Location.Name,
                    Street = g.Key.Location.Street,
                    PostalCode = g.Key.Location.PostalCode,
                    City = g.Key.Location.City,
                    Assignments = g
                });

            // Die Kendo-Methode ToDataSourceResult() führt jetz auf SQL-Ebene die Filterung, Sortierung
            // und Paginierung durch. Dabei werden die Daten die auch angezeigt werden aus der Datenbank
            // in den Speicher gelesen. Jetzt können wir auch Operationen auf den Daten durchführen die
            // vorher nicht möglich waren.

            // Allerdings müssen wir die Filterung nach Tagen manuell machen, weil wir in unserem Zwischenmodell
            // Keine Properties für die einzelnen Wochentage haben (wollen).
            FilterWeekdays(ref gridModel, request.Filters);

            // Wir verwenden die Aggregate um die Anzahl der Einsätze pro Wochentag zu übermitteln. Damit die
            // Tage die wir ins JSON packen auch in der DataSource ankommen müssen für die Aggregate in denen
            // wir sie transportieren auch wirklich vom Grid gesetzt sein. Daher sendet das Grid auch Aggregates
            // mit. Da diese Aggregates aber mit dem normalen Mechanismus nicht berechnet werden können und sollen
            // setzten wir Aggegrates hier einfach auf null.
            request.Aggregates = null;

            var dataSourceResult = alternateEnumeration
                ? (await gridModel.ToListAsync()).ToDataSourceResult(request)
                : gridModel.ToDataSourceResult(request);

            var gridModelInMemory = dataSourceResult.Data as IEnumerable<IntermediateLocationRowViewModelNew>;
            Debug.Assert(gridModelInMemory != null, "gridModelInMemory != null");
            dataSourceResult.Data = gridModelInMemory
                .Select(row => new LocationRowViewModel
                {
                    LocationId = row.LocationId,
                    Name = row.Name,
                    Street = row.Street,
                    PostalCode = row.PostalCode,
                    City = row.City,
                    MondayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Monday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    TuesdayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Tuesday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    WednesdayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Wednesday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    ThursdayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Thursday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    FridayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Friday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    SaturdayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Saturday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                    SundayItems = row.Assignments
                        .Where(e => e != null && e.DateStart.DayOfWeek == DayOfWeek.Sunday)
                        .GroupBy(e => e.Id)
                        .Select(e => new AssignmentViewModel(e))
                        .ToList(),
                });

            var dayCountsList = _dbContext.Assignments
                .Where(e =>
                    start <= e.DateStart && e.DateStart < end &&
                    e.State == AssignmentState.Planned &&
                    e.CampaignLocation.CampaignId == filterModel.CampaignId)
                .GroupBy(assignment => SqlFunctions.DatePart("weekday", assignment.DateStart))
                .Select(grouping => new
                {
                    Day = grouping.Key,
                    Total = grouping.Count(),
                    Assigned = grouping.Count(e => e.EmployeeCount <= e.AssignedEmployees.Count())
                });

            var dayCounts = new[]
            {
                new FakeAggregate { Member = "Monday", FunctionName = "MondayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Monday", FunctionName = "MondayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Tuesday", FunctionName = "TuesdayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Tuesday", FunctionName = "TuesdayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Wednesday", FunctionName = "WednesdayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Wednesday", FunctionName = "WednesdayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Thursday", FunctionName = "ThursdayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Thursday", FunctionName = "ThursdayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Friday", FunctionName = "FridayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Friday", FunctionName = "FridayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Saturday", FunctionName = "SaturdayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Saturday", FunctionName = "SaturdayAssigned", AggregateMethodName = "Count"},
                new FakeAggregate { Member = "Sunday", FunctionName = "SundayTotal", AggregateMethodName = "Sum"},
                new FakeAggregate { Member = "Sunday", FunctionName = "SundayAssigned", AggregateMethodName = "Count"},
            };

            foreach (var dayCount in dayCountsList)
            {
                var index = (dayCount.Day.Value - 1) * 2;
                dayCounts[index].Value = dayCount.Total;
                dayCounts[index++].FormattedValue = dayCount.Total;
                dayCounts[index].Value = dayCount.Assigned;
                dayCounts[index].FormattedValue = dayCount.Assigned;
            }

            var model = new
            {
                AggregateResults = dayCounts,
                dataSourceResult.Data,
                dataSourceResult.Errors,
                dataSourceResult.Total,
            };

            return Json(model);
        }

        #region AJAX Actions für Promoterlisten
        private ActionResult EmployeeResult(IEnumerable<Employee> employees)
        {
            return Json(employees.Select(e => new { e.Id, FullName = e.Firstname + " " + e.Lastname, e.Firstname, e.Lastname }), JsonRequestBehavior.AllowGet);
        }

        // Diese Action wird von Assign, Attendance und Training aufgerufen.
        public virtual ActionResult _Employees(string text)
        {
            if (string.IsNullOrEmpty(text))
                return EmployeeResult(new Employee[0]);

            var employees = _dbContext.ActiveEmployees
                .Where(e => text != null && (e.Firstname + " " + e.Lastname).Contains(text));

            return EmployeeResult(employees);
        }

        public virtual ActionResult _NearbyEmployees(int assignmentId)
        {
            if (assignmentId == -1)
                return EmployeeResult(new Employee[0]);

            var assignment = _dbContext.Assignments.Single(e => e.Id == assignmentId);
            var coordinates = assignment.CampaignLocation.Location.GeographicCoordinates;
            var employees = _dbContext.ActiveEmployees.Where(e => e.Profile.GeographicCoordinates.Distance(coordinates) < 50000);
            return EmployeeResult(employees);
        }

        public virtual ActionResult _CampaignEmployees(int campaignId)
        {
            var employees = _dbContext.CampaignEmployees
                .Where(e => e.CampaignId == campaignId && e.ContractAccepted.HasValue);

            return Json(employees.Select(e => new
            {
                Id = e.EmployeeId, // Wichtig das hier Id zu nennen, da das JS diesen Namen erwartet
                FullName = e.Employee.Firstname + " " + e.Employee.Lastname + " (" + e.Roles + ")",
                e.Employee.Firstname,
                e.Employee.Lastname
            }), JsonRequestBehavior.AllowGet);

        }
        #endregion

        public virtual ActionResult _Locations(int campaignId)
        {
            var locations = _dbContext.CampaignsLocations.Where(e => e.CampaignId == campaignId).Select(e => e.Location);
            return Json(locations.Select(e => new { Value = e.Id, Text = e.Name + " " + e.Street + " " + e.City }), JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<AssignedEmployee> AssignEmployee(AssignUpdateEmployeeModel model, out Guid? dataId)
        {
            var assignments = MatchingAssignments(model).Where(e => e.AssignedEmployees.Count < e.EmployeeCount);
            var newAssignedEmployees = new List<AssignedEmployee>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            // ReSharper disable once PossibleNullReferenceException
            foreach (var assignment in assignments)
            {
                newAssignedEmployees.Add(new AssignedEmployee
                {
                    Assignment = assignment,
                    AssignmentId = assignment.Id,
                    EmployeeId = model.NewEmployeeId,
                    Employee = _dbContext.ActiveEmployees.Single(e => e.Id == model.NewEmployeeId),
                    AssignmentRoles = new Collection<AssignmentRole>()
                });
                assignment.MarkForSync();
            }
            dataId = new PromoterNotification()
                .ForNewAssignments(newAssignedEmployees)
                .ForImmediateDelivery(model.Notify ? DeliveryTime.AfterConflictsWindowClosed : DeliveryTime.Later);

            _dbContext.AssignedEmployees.AddRange(newAssignedEmployees);
            return newAssignedEmployees;
        }

        private IEnumerable<AssignedEmployee> ReplaceEmployee(AssignUpdateEmployeeModel model, ref AjaxActionResultModel result, out Guid? dataId)
        {
            var assignedEmployees = MatchingAssignedEmployees(model)
                .Where(e => e.EmployeeId == model.PreviousEmployeeId)
                .ToList();

            var newAssignedEmployees = new List<AssignedEmployee>();

            int count;
            if (!model.Confirmed && (count = assignedEmployees.Count(e => e.Assignment.DateStart < LocalDateTime.Now)) > 0)
            {
                SetAssignmentsWithReportsResult(count, result, UpdateMessageSingle, UpdateMessageMultipleFormat);
                dataId = null;
                return null;
            }

            var newEmployee = _dbContext.Employees.Single(e => e.Id == model.NewEmployeeId);

            foreach (var assignedEmployee in assignedEmployees)
            {
                var newAssignedEmployee = new AssignedEmployee
                {
                    Assignment = assignedEmployee.Assignment,
                    AssignmentId = assignedEmployee.AssignmentId,
                    EmployeeId = model.NewEmployeeId,
                    Employee = newEmployee,
                    AssignmentRoles = assignedEmployee.AssignmentRoles.ToList()
                };
                newAssignedEmployee.Assignment.MarkForSync();
                newAssignedEmployees.Add(newAssignedEmployee);
            }

            dataId = new PromoterNotification()
                .ForCancelledAssignments(assignedEmployees)
                .ForNewAssignments(newAssignedEmployees)
                .ForImmediateDelivery(model.Notify ? DeliveryTime.AfterConflictsWindowClosed: DeliveryTime.Later);

            _dbContext.AssignedEmployees.AddRange(newAssignedEmployees);
            _dbContext.AssignedEmployees.RemoveRange(assignedEmployees);
            return newAssignedEmployees;
        }

        private void UpdateRoles(AssignUpdateEmployeeModel model, IEnumerable<AssignedEmployee> assignedEmployees)
        {
            var newRoles = _dbContext.AssignmentRoles.Where(e => model.NewRoleIds.Contains(e.Id));

            foreach (var assignedEmployee in assignedEmployees)
            {
                if(assignedEmployee.AssignmentRoles.Select(e => e.Id).SequenceEqual(model.NewRoleIds))
                    continue;

                if(assignedEmployee.AssignmentRoles == null)
                    assignedEmployee.AssignmentRoles = new Collection<AssignmentRole>();

                assignedEmployee.AssignmentRoles.ReplaceWith(newRoles);
                assignedEmployee.Assignment.MarkForSync();
            }
        }

        public virtual ActionResult _UpdateAssignedEmployee(AssignUpdateEmployeeModel model)
        {
            IQueryable<AssignedEmployee> assignedEmployees;
            _dbContext.Database.ExecuteSqlCommand("SET DATEFIRST 7;");

            var result = new AjaxActionResultModel();
            Guid? dataId;
            if (model.IsNewAssignment)
            {
                assignedEmployees = AssignEmployee(model, out dataId).AsQueryable();
            }
            else if (model.IsReplacement)
            {
                var tmp = ReplaceEmployee(model, ref result, out dataId);
                if (tmp == null)
                    return Json(result);
                assignedEmployees = tmp.AsQueryable();
            }
            else
            {
                // Wenn wir hier sind, haben sich nur die Rollen geändert
                dataId = null;

                // Falls auch Folgeeinsätze geändert werden sollen, nehmen wir nur die bei denen die Rollen exakt passen.
                // Sonst würden z.B. bei der Änderung "Fachberater* => Teamleiter" auch Provisionseinsätze zu Teamleiter geändert werden.
                var oldRoles = _dbContext.AssignedEmployees
                    .Single(e => e.AssignmentId == model.AssignmentId && e.EmployeeId == model.EmployeeId)
                    .AssignmentRoles.Select(e => e.Id).ToArray();
                var length = oldRoles.Length;

                assignedEmployees = MatchingAssignedEmployees(model)
                    .Where(e => e.AssignmentRoles.Count == length && !e.AssignmentRoles.Select(i => i.Id).Except(oldRoles).Any());
            }

            UpdateRoles(model, assignedEmployees);

            _dbContext.SaveChanges();

            if (model.IsNewAssignment || model.IsReplacement)
            {
                // Konflikte finden
                var conflicts = new List<AssignmentConflict>();
                foreach (var assignedEmployee in assignedEmployees)
                {
                    var conflictingAssignments = ConflictingAssignments(assignedEmployee)
                        .Select(e => new { e.Assignment.CampaignLocation.Campaign.Name, e.Assignment.CampaignLocation.Location.City });

                    if (conflictingAssignments.Any())
                    {
                        result.location = assignedEmployee.Assignment.CampaignLocation.Location.FriendlyName;
                        result.employee = assignedEmployee.Employee.Firstname + " " + assignedEmployee.Employee.Lastname;

                        conflicts.Add(new AssignmentConflict
                        {
                            dataId = dataId,
                            assignmentId = assignedEmployee.AssignmentId,
                            date = assignedEmployee.Assignment.DateStart.ToString("d"),
                            campaigns= string.Join(", ", conflictingAssignments.Select(e => e.Name)),
                            cities = string.Join(", ", conflictingAssignments.Select(e => e.City))
                        });
                    }
                }
                result.dataId = dataId;
                result.employeeId = model.NewEmployeeId;
                result.conflicts = conflicts;
                if (!conflicts.Any() && model.Notify)
                {
                    // Keine Konflikte, dann können wir direkt Benachrichtigen, falls dies gewünscht war
                    Debug.Assert(dataId.HasValue, "Need the dataId");
                    SendNotifications(new[] {dataId.Value});
                }
            }

            result.status = "ok";
            return Json(result);
        }

        private IQueryable<AssignedEmployee> ConflictingAssignments(AssignedEmployee assignedEmployee)
        {
            var date = assignedEmployee.Assignment.DateStart.Date;
            var date2 = date.AddDays(1);
            var employeeId = assignedEmployee.EmployeeId;
            var assignmentId = assignedEmployee.AssignmentId;
            return _dbContext.AssignedEmployees.Where(e =>
                    e.EmployeeId == employeeId &&
                    e.AssignmentId != assignmentId &&
                    e.Assignment.State == AssignmentState.Planned &&
                    date <= e.Assignment.DateStart && e.Assignment.DateStart < date2);
        }

        public virtual ActionResult _RemoveAssignedEmployee(AssignRemoveEmployeeModel model)
        {
            // Falls auch Folgeeinsätze gelöscht werden sollen, nehmen wir nur die bei denen die Rollen exakt passen.
            // Sonst würden z.B. auch Provisionseinsätze gelöscht, wenn bei einem normalem Einsatz alle folgenden Löschen gewählt wird.
            var oldRoles = _dbContext.AssignedEmployees
                .Single(e => e.AssignmentId == model.AssignmentId && e.EmployeeId == model.EmployeeId)
                .AssignmentRoles.Select(e => e.Id).ToArray();
            var length = oldRoles.Length;

            var assignedEmployees = MatchingAssignedEmployees(model)
                .Where(e => e.AssignmentRoles.Count == length && !e.AssignmentRoles.Select(i => i.Id).Except(oldRoles).Any())
                .ToList();

            var result = new AjaxActionResultModel();

            int count;
            if (!model.Confirmed && (count = assignedEmployees.Count(e => e.Assignment.DateStart < LocalDateTime.Now)) > 0)
            {
                SetAssignmentsWithReportsResult(count, result, DeleteMessageSingle, DeleteMessageMultipleFormat);
            }
            else
            {
                result.status = "ok";
                foreach (var assignedEmployee in assignedEmployees)
                {
                    assignedEmployee.Assignment.MarkForSync();
                }

                if (model.DataId == null)
                {
                    // Nur benachrichtigen wenn wir KEINE dataId haben. Keine dataId heißt normales entfernen der Zuweisung.
                    // Mit dataId heißt, entfernen eines Konflikts. Da für Konflikte noch keine Benachrichtigung raus gegangen
                    // ist, muss auch bei der Storinierung auch keine Mail rausgehen.
                    new PromoterNotification()
                        .ForCancelledAssignments(assignedEmployees)
                        .ForImmediateDelivery(model.Notify ? DeliveryTime.Now : DeliveryTime.Later);
                }
                else
                {
                    // Wenn wir eine dataId haben, heißt es, dass diese Zuweisung ein Konflikt ist, der nun entfernt wird.
                    // Diesen Konflikt müssen wir nun auch aus unserer gespeicherten Benachrichtigung löschen.
                    // Im Konfliktfall ist es auch immer nur ein Assignment zur Zeit, daher können wir hier einfach model.AssignmentId
                    // verwenden und müssen nicht die MatchingAssignments beachten.
                    PromoterNotification.RemoveAssignmentFromSavedData(model.DataId.Value, model.AssignmentId);
                }

                _dbContext.AssignedEmployees.RemoveRange(assignedEmployees);
                _dbContext.SaveChanges();
            }

            return Json(result);
        }

        public static void SetAssignmentsWithReportsResult(int count, AjaxActionResultModel result, string singleMessage, string multipleMessageFormat)
        {
            result.status = "needs confirmation";
            result.message = count == 1
                ? singleMessage
                : string.Format(multipleMessageFormat,
                    count == 2 ? "ein Folgetermin" : (count - 1) + " Folgetermine");
        }

        private IQueryable<Assignment> MatchingAssignments(AssignAjaxPostBase model)
        {
            return MatchingAssignments(_dbContext, model.AssignmentId, model.Repeat, model.AdditionalAssignmentIds);
        }

        public static IQueryable<Assignment> MatchingAssignments(SeismitoadDbContext dbContext, int assignmentId, AssignmentRepeatType repeat, int[] additionalAssignmentIds = null)
        {
            var assignment = dbContext.Assignments.Single(e => e.Id == assignmentId);

            if (repeat == AssignmentRepeatType.NoRepeat)
            {
                return new[] { assignment }.AsQueryable();
            }

            var campaignId = assignment.CampaignLocation.CampaignId;
            var locationId = assignment.CampaignLocation.LocationId;
            var date = assignment.DateStart;

            var assignments = dbContext.ActiveAssignments
                .Where(e =>
                    e.CampaignLocation.CampaignId == campaignId &&
                    e.CampaignLocation.LocationId == locationId &&
                    e.DateStart >= date);

            switch (repeat)
            {
                case AssignmentRepeatType.Everyday:
                    // Nichts weiter zu filtern
                    break;

                case AssignmentRepeatType.SameWeekday:
                    // Nur die Einsätze mit selben Wochentag
                    var dayOfWeek = (int)date.DayOfWeek;
                    // SqlFunctions.DatePart("weekday") runs from 1 to 7 (Monday to Sunday, if Logins Language is German), DateTime.DayOfWeek from 0 to 6 (Sunday to Saturday)
                    // Thus Monday to Saturday have the correct value, but Sunday has to changed from 0 to 7
                    if (dayOfWeek == 0) dayOfWeek = 7;
                    assignments = assignments
                        .Where(e => SqlFunctions.DatePart("weekday", e.DateStart) == dayOfWeek);
                    break;
                case AssignmentRepeatType.Custom:
                    var assignmentIds = new List<int>(additionalAssignmentIds ?? new int[0]);
                    assignmentIds.Add(assignmentId);
                    assignments = assignments.Where(e => assignmentIds.Contains(e.Id));
                    break;
                case AssignmentRepeatType.NoRepeat:
                    // Hier sollten wir nie ankommen, da bei NoRepeat oben schon rausgesprungen wird
                    throw new InvalidOperationException();
                default:
                    throw new InvalidOperationException("Invalid value for Repeat.");
            }
            return assignments.OrderBy(e => e.DateStart);
        }

        private IQueryable<AssignedEmployee> MatchingAssignedEmployees(AssignAjaxPostBase model)
        {
            var assignedEmployee = _dbContext.AssignedEmployees
                .Single(e => e.AssignmentId == model.AssignmentId && e.EmployeeId == model.EmployeeId);

            if (model.Repeat == AssignmentRepeatType.NoRepeat)
            {
                return new [] { assignedEmployee }.AsQueryable();
            }

            var campaignId = assignedEmployee.Assignment.CampaignLocation.CampaignId;
            var locationId = assignedEmployee.Assignment.CampaignLocation.LocationId;
            var date = assignedEmployee.Assignment.DateStart;

            var assignedEmployees = _dbContext.AssignedEmployees
                .Where(e =>
                    e.Assignment.CampaignLocation.CampaignId == campaignId &&
                    e.Assignment.CampaignLocation.LocationId == locationId &&
                    e.EmployeeId == model.EmployeeId &&
                    e.Assignment.DateStart >= date
                );

            switch (model.Repeat)
            {
                case AssignmentRepeatType.Everyday:
                    // Nichts weiter zu filtern
                    break;

                case AssignmentRepeatType.SameWeekday:
                    // Nur die Einsätze mit selben Wochentag
                    var dayOfWeek = (int) date.DayOfWeek;
                    if (dayOfWeek == 0) dayOfWeek = 7;
                    assignedEmployees = assignedEmployees
                        .Where(e => SqlFunctions.DatePart("weekday", e.Assignment.DateStart) == dayOfWeek);
                    break;
                case AssignmentRepeatType.Custom:
                    var assignmentIds = new List<int>(model.AdditionalAssignmentIds ?? new int[0]);
                    assignmentIds.Add(model.AssignmentId);
                    assignedEmployees = assignedEmployees.Where(e => assignmentIds.Contains(e.AssignmentId));
                    break;
                case AssignmentRepeatType.NoRepeat:
                    // Hier sollten wir nie ankommen, da bei NoRepeat oben schon rausgesprungen wird
                    throw new InvalidOperationException();
                default:
                    throw new InvalidOperationException("Invalid value for Repeat.");
            }
            return assignedEmployees.OrderBy(e => e.Assignment.DateStart);
        }

        public virtual ActionResult _Notifications([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            var outstandingNotifications = _dbContext.OutstandingNotifications
                .Where(e => e.CampaignId == campaignId)
                .Include(e => e.Employee)
                .OrderBy(e => e.Id)
                .Project().To<OutstandingNotificationViewModel>();
            return Json(outstandingNotifications.ToDataSourceResult(request));
        }

        public virtual ActionResult _SendNotifications(Guid[] ids)
        {
            SendNotifications(ids);
            return Content("OK");
        }

        private void SendNotifications(Guid[] ids)
        {
            foreach (var guid in ids)
            {
                var guid1 = guid;
                PromoterNotification.SetNotified(guid1);
                BackgroundJob.Enqueue(() => PromoterNotification.SendNotification(guid1));
            }
            _dbContext.Database.ExecuteSqlCommand(string.Format("DELETE FROM OutstandingNotifications WHERE DataId in ('{0}')", string.Join("','", ids)));
        }

        public virtual ActionResult _DeleteNotifications(int[] ids)
        {
            _dbContext.Database.ExecuteSqlCommand(string.Format("DELETE FROM OutstandingNotifications WHERE Id in ({0})", string.Join(",", ids)));
            return Content("OK");
        }

        public virtual ActionResult _Marker(int assignmentId, int marker)
        {
            _dbContext.Database.ExecuteSqlCommand(
                "UPDATE Assignments SET Marker = @Marker WHERE Id = @AssignmentId",
                new SqlParameter("@Marker", marker), new SqlParameter("@AssignmentId", assignmentId));
            return Content("OK");
        }

        public virtual ActionResult _Notes(int assignmentId, string notes)
        {
            var notesParameter = string.IsNullOrWhiteSpace(notes)
                ? new SqlParameter("@Notes", DBNull.Value)
                : new SqlParameter("@Notes", notes);

            _dbContext.Database.ExecuteSqlCommand(
                "UPDATE Assignments SET Notes = @Notes WHERE Id = @AssignmentId",
                notesParameter, new SqlParameter("@AssignmentId", assignmentId));
            return Content("OK");
        }
    }
}
