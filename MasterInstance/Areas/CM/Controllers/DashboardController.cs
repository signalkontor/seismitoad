﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ImageResizer.Plugins.SourceDiskCache;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.Infrastructure.Implementation;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
#if ADVANTAGE
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin + "," + SeismitoadShared.Constants.Roles.Advantage)]
#else
    [Authorize(Roles = SeismitoadShared.Constants.Roles.User)]
#endif
    public partial class DashboardController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public DashboardController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: CM/Dashboard
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int campaignId)
        {
            var model = new AdvantageDashboardViewModel();
            model.CampaignId = campaignId;

            #region Location stats

            var campaignLocations = _dbContext.CampaignsLocations.Where(e => e.CampaignId == campaignId);
            model.CampaignLocationCount = campaignLocations.Count();
            
            var campaignLocationAssignments = _dbContext.Assignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId && e.State == AssignmentState.Planned)
                .GroupBy(e => e.CampaignLocation);

            model.UnassignedCampaignLocationCount =
                // Anzahl der Locations bei denen alle AT unterbesetzt sind
                campaignLocationAssignments.Count(e => e.All(i => i.AssignedEmployees.Count < i.EmployeeCount)) +
                // plus Anzahl der Locations die gar keine Aktionstage haben
                campaignLocations.Count(e => e.Assignments.All(i => i.State != AssignmentState.Planned));

            // Anzahl der Locations die mindestens einen AT haben der nicht vollständig besetzt ist und
            // mindestens einen AT haben der voll besetzt ist.
            model.PartiallyAssignedCampaignLocationCount = campaignLocationAssignments
                .Count(e => e.Any(i => i.AssignedEmployees.Count < i.EmployeeCount) && e.Any(i => i.AssignedEmployees.Count >= i.EmployeeCount));

            // Anzahl der Locations bei denen allte AT voll (oder über) besetzt sind.
            model.FullyAssignedCampaignLocationCount = campaignLocationAssignments
                .Count(e => e.All(i => i.AssignedEmployees.Count >= i.EmployeeCount));
            #endregion

            #region Team stats
            var campaignEmployees = _dbContext.CampaignEmployees.Where(e => e.CampaignId == campaignId);
            var cookie = Request.Cookies["last login"];
            DateTime date;
            if (cookie != null && DateTime.TryParse(cookie.Value, out date))
            {
                model.NewCandidatesSince = date;
            }
            else
            {
                model.NewCandidatesSince = Membership.GetUser().LastLoginDate;
            }
            model.NewCandidates = campaignEmployees.Count(e => e.Applied >= model.NewCandidatesSince);
            model.Candidates = campaignEmployees.Count(e => e.Applied.HasValue);
            model.TeamSize = campaignEmployees.Count(e => e.Accepted.HasValue);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var campaignEmployeeIds = campaignEmployees
                .Where(e => e.Accepted.HasValue)
                .Select(e => e.EmployeeId);
            var assignedEmployeeIds = _dbContext.AssignedEmployees
                .Where(e => e.Assignment.CampaignLocation.CampaignId == campaignId)
                .Select(e => e.EmployeeId)
                .Distinct();
            model.EmployeesWithoutAssignments = campaignEmployeeIds.Except(assignedEmployeeIds).Count();
            stopwatch.Stop();
            ViewBag.TimeInfo = string.Format("Dauer: {0}", stopwatch.Elapsed);

            var path = Server.MapPath("~/CampaignAssets/" + campaignId + "/StaffRequirements");
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path, "*.json");
                if (files.Length > 0)
                {
                    Array.Sort(files);
                    model.StaffRequirementLastChanged = Directory.GetCreationTime(files[files.Length - 1]);
                }
            }
            #endregion

            #region Aufträge und Einsatzplanung

            model.UnsentContracts = campaignEmployees.Count(e => e.Accepted.HasValue && !e.ContractSent.HasValue);
            model.UnconfirmedContracts = campaignEmployees.Count(e => e.ContractSent.HasValue && !e.ContractAccepted.HasValue);
            model.UnsentAssignments = _dbContext.Database.SqlQuery<int>(@"SELECT     COUNT(*)
FROM         Assignments INNER JOIN
                      AssignedEmployees ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
                      AssignedEmployeeExtraData ON
						AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND
						AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id 
				WHERE AssignedEmployeeExtraData.Notified IS NULL AND Assignments.CampaignLocation_CampaignId = {0}
", campaignId).First();
            model.UnconfirmedAssignments = _dbContext.Database.SqlQuery<int>(@"SELECT     COUNT(*)
FROM         Assignments INNER JOIN
                      AssignedEmployees ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
                      AssignedEmployeeExtraData ON
						AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND
						AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id 
				WHERE AssignedEmployeeExtraData.Notified IS NOT NULL AND AssignedEmployeeExtraData.Accepted IS NULL AND Assignments.CampaignLocation_CampaignId = {0}
", campaignId).First();
            model.RejectedAssignments = _dbContext.Database.SqlQuery<int>(@"SELECT     COUNT(*)
FROM         Assignments INNER JOIN
                      AssignedEmployees ON Assignments.Id = AssignedEmployees.Assignment_Id INNER JOIN
                      AssignedEmployeeExtraData ON
						AssignedEmployeeExtraData.AssignmentId = AssignedEmployees.Assignment_Id AND
						AssignedEmployeeExtraData.EmployeeId = AssignedEmployees.Employee_Id 
				WHERE AssignedEmployeeExtraData.Rejected IS NOT NULL AND Assignments.CampaignLocation_CampaignId = {0}
", campaignId).First();
            #endregion
            return View(model);
        }

        public virtual ActionResult _AssignmentsByMonth(int campaignId, [DataSourceRequest] DataSourceRequest request)
        {
            var data = _dbContext.Assignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId && e.State == AssignmentState.Planned)
                .GroupBy(e => new { e.DateStart.Month, e.DateStart.Year })
                .Select(e => new AssignmentsByMonth
                {
                    Year = e.Key.Year,
                    Month = e.Key.Month,
                    Total = e.Count(),
                    Unassigned = e.Count(i => i.AssignedEmployees.Count < i.EmployeeCount)
                });
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}