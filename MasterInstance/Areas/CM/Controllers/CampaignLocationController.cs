using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Security;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Controllers;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Constants;
using SeismitoadShared.Extensions;
using Telerik.Web.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AutoMapper.QueryableExtensions;
using System.Data.SqlClient;
using System.Data;

namespace MasterInstance.Areas.CM.Controllers
{
    [MenuArea(MenuArea.Project)]
    public partial class CampaignLocationController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public CampaignLocationController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public const int PageSize = 10;
        public const string GridName = "c";

        public class LocationJoinedWithCampaignLocation
        {
            public int LocationId { get; set; }
            public Location Location { get; set; }
            public CampaignLocation CampaignLocation { get; set; }
        }

        public virtual ActionResult Index(int id)
        {
            var customerId = _dbContext.Campaigns
                .Where(e => e.Id == id)
                .Select(e => e.CustomerId)
                .Single();

            AddSalesRegionsToViewData(customerId);
            var model = new CampaignLocationSelectionModel
            {
                LocationGroups = new SelectList(_dbContext.LocationGroups.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
                LocationTypes = new SelectList(_dbContext.LocationTypes.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
                StateProvinces = new SelectList(_dbContext.StateProvinces.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
            };
            return View(model);
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int campaignId, bool onlyCampaignLocations, string addOrRemove, CampaignLocation template)
        {
            var campaignLocations = _dbContext.CampaignsLocations.Where(e => e.CampaignId == campaignId);
            var locations = from location in _dbContext.Locations
                            join campaignLocation in campaignLocations on location.Id equals campaignLocation.LocationId into outerJoin
                            from campaignLocation in outerJoin.DefaultIfEmpty()
                            where location.State == LocationState.Active || campaignLocation != null
                            select new CampaignLocationHelper { Location = location, CampaignLocation = campaignLocation };

            if (onlyCampaignLocations)
            {
                locations = locations.Where(e => e.CampaignLocation != null);
            }

            var model = locations.Project().To<CampaignLocationViewModel>();

            if (addOrRemove != null)
                ApplyAction(addOrRemove, request, model, campaignId, template);

            return Json(model.ToDataSourceResult(request));
        }

        private void ApplyAction(string action, DataSourceRequest request, IQueryable<CampaignLocationViewModel> locations, int campaignId, CampaignLocation template)
        {
            var originalPage = request.Page;
            var originalPageSize = request.PageSize;
            request.Page = 1;
            request.PageSize = int.MaxValue;
            var remove = action == "remove";
            if (remove)
            {
                // Wir entfernen nur die CampaignLocations die l�schbar sind, das hei�t keine Aktionstage haben
                locations = locations.Where(e => e.Deleteable);
                var locationIds = (locations.ToDataSourceResult(request).Data as IEnumerable<CampaignLocationViewModel>).Select(e => e.LocationId).ToArray();
                for(var i = 0; i < locationIds.Length; i += 100)
                {
                    var segment = new ArraySegment<int>(locationIds, i, Math.Min(100, locationIds.Length - i));
                    var query = string.Format("DELETE FROM CampaignLocations WHERE Campaign_Id = @CampaignId AND Location_Id IN ({0})", string.Join(",", segment));
                    _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@CampaignId", campaignId));
                }
            }
            else
            {
                // Wir f�gen nur die Locations hinzu die noch keine CampaignLocations sind
                locations = locations.Where(e => !e.IsCampaignLocation);
                var locationIds = (locations.ToDataSourceResult(request).Data as IEnumerable<CampaignLocationViewModel>).Select(e => e.LocationId).ToArray();
                for (var i = 0; i < locationIds.Length; i += 100)
                {
                    var segment = new ArraySegment<int>(locationIds, i, Math.Min(100, locationIds.Length - i));
                    var query = string.Format(@"
INSERT INTO [dbo].[CampaignLocations]
           ([Campaign_Id]
           ,[Location_Id]
           ,[NetworkConnectionAvailable]
           ,[State]
           ,[Classification]
           ,[SalesRegion]
           ,[Notes])
SELECT @CampaignId,
    Locations.Id,
    @NetworkConnectionAvailable,
    @State,
    @Classification,
    @SalesRegion,
    @Notes
FROM Locations WHERE Locations.Id IN ({0})
", string.Join(",", locationIds));
                    _dbContext.Database.ExecuteSqlCommand(query,
                        new SqlParameter("@CampaignId", campaignId),
                        new SqlParameter("@NetworkConnectionAvailable", (object)template.NetworkConnectionAvailable ?? DBNull.Value),
                        new SqlParameter("@State", template.State),
                        new SqlParameter("@Classification", (object)template.Classification ?? DBNull.Value),
                        new SqlParameter("@SalesRegion", (object)template.SalesRegion ?? DBNull.Value),
                        new SqlParameter("@Notes", (object)template.Notes ?? DBNull.Value));
                }
            }
            request.Page = originalPage;
            request.PageSize = originalPageSize;
        }

        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, int campaignId, CampaignLocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var campaignLocation = _dbContext.CampaignsLocations.Single(e => e.CampaignId == campaignId && e.LocationId == model.LocationId);
                campaignLocation.NetworkConnectionAvailable = model.WiFiAvailableCampaign;
                campaignLocation.State = model.State.Value;
                campaignLocation.Classification = model.Classification;
                campaignLocation.SalesRegion = model.SalesRegion;
                campaignLocation.Notes = model.RemarkCampaign;
                campaignLocation.ContactId = model.ContactId;
                if (model.WiFiAvailable == null)
                {
                    model.WiFiAvailable = model.WiFiAvailableCampaign;
                }
                _dbContext.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public virtual ActionResult _Contacts(int locationId)
        {
            var contacts = _dbContext.Contacts
                .Where(e => e.LocationId == locationId)
                .Select(e => new { e.Id, Name = e.Title + " " + e.Firstname + " " + e.Lastname })
                .OrderBy(e => e.Name);
                ///.ToList();

            //contacts.Insert(0, new { Id = (int?)null, Name = "Keiner" });

            return Json(contacts, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult _SalesRegions(int campaignId)
        {
            var customerId = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Select(e => e.CustomerId)
                .Single();
            var salesRegions = GetSalesRegionSelectListItems(customerId);
            return Json(salesRegions, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public virtual ActionResult _AddContact(Contact model)
        {
            _dbContext.Contacts.Add(model);
            _dbContext.SaveChanges();
            return Content("OK");
        }

        private void AddSalesRegionsToViewData(int? customerId)
        {
            ViewData["SalesRegionItems"] = GetSalesRegionSelectListItems(customerId);
        }

        private IEnumerable<SelectListItem> GetSalesRegionSelectListItems(int? customerId)
        {
            return _dbContext.CustomerAccounts
                .Where(e => e.CustomerId == customerId)
                .Select(e => new {e.ProviderUserKey, Text = e.Firstname + " " + e.Lastname})
                .OrderBy(e => e.Text)
                .ToList()
                .Select(e =>
                {
                    var user = Membership.GetUser(e.ProviderUserKey, false);
                    return new SelectListItem
                    {
                        Value = user != null ? user.UserName : "Gel�schter Account",
                        Text = e.Text
                    };
                });
        }

        [HttpPost]
        public virtual ActionResult _Toggle(int locationId, int campaignId, [Bind(Prefix = "Template")]CampaignLocation template = null)
        {
            _dbContext.Database.CommandTimeout = 300;
            var campaignLocation = _dbContext.CampaignsLocations.SingleOrDefault(e => e.Campaign.Id == campaignId && e.Location.Id == locationId);
            if (campaignLocation != null && campaignLocation.Assignments.All(e => e.State == AssignmentState.Deleted))
            {
                _dbContext.Assignments.RemoveRange(campaignLocation.Assignments);
                _dbContext.CampaignsLocations.Remove(campaignLocation);
                _dbContext.SaveChanges();
                return Content("Removed");
            }

            campaignLocation = new CampaignLocation
            {
                CampaignId = campaignId,
                Location = _dbContext.Locations.Single(e => e.Id == locationId),
                NetworkConnectionAvailable = template.NetworkConnectionAvailable,
                Notes = template.Notes,
                SalesRegion = template.SalesRegion,
                Classification = template.Classification,
                State = template.State
            };

            _dbContext.CampaignsLocations.Add(campaignLocation);
            _dbContext.SaveChanges();

            return Content("Inserted");
        }
    }
}
