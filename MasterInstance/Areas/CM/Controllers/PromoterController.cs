﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Web.UI;
using MasterInstance.Extensions;
using MasterInstance.Areas.CM.Models;
using Newtonsoft.Json;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class PromoterController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public PromoterController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: CM/Promtoter
        [MenuArea(MenuArea.Project)]
        [OutputCache(NoStore = true, Duration = 0, Location = OutputCacheLocation.None)]
        public virtual ActionResult Index(int id)
        {
            var trainingDays = _dbContext.Trainings
                .Where(e => e.TrainingDays.SelectMany(i => i.Participants).Any(i => i.CampaignEmployee.CampaignId == id))
                .OrderBy(e => e.Title)
                .Select(e => new 
                {
                    e.Id,
                    e.Title,
                }).ToList();

            CampaignEmployeesController.SetTrainingCookie(Response, id, trainingDays.Select(e => e.Id));
            
            var model = new CampaignEmployeeViewModel
            {
                View = "Promoter",
                TrainingColumnTitles = trainingDays.Select(e => e.Title)
            };
            return View(MVC.CM.CampaignEmployees.Views.Index, model);
        }

    }
}