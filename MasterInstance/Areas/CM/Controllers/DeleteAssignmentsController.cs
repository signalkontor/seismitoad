﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
    [MenuArea(MenuArea.Project)]
#if ADVANTAGE
    [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin + "," + SeismitoadShared.Constants.Roles.Advantage)]
#else
    [Authorize(Roles = SeismitoadShared.Constants.Roles.User)]
#endif
    public partial class DeleteAssignmentsController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public DeleteAssignmentsController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult Index(int campaignId)
        {
            return View(_dbContext.Campaigns.Single(e => e.Id == campaignId));
        }

        public virtual ActionResult _Read([DataSourceRequest]DataSourceRequest request, int campaignId)
        {
            Session["DeleteAssignmentsDataSourceRequest"] = request;
            var model = _dbContext.ActiveAssignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId)
                .Project()
                .To<DeleteAssignmentViewModel>();
            return Json(model.ToDataSourceResult(request));
        }

        [HttpPost]
        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, DeleteAssignmentViewModel model)
        {
            var assignment = _dbContext.Assignments.Single(e => e.Id == model.Id);
            assignment.State = AssignmentState.Deleted;
            _dbContext.SaveChanges();
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public virtual ActionResult _Unassign([DataSourceRequest] DataSourceRequest request, int id)
        {
            if (ModelState.IsValid)
            {
                var assignment = _dbContext.Assignments.Single(e => e.Id == id);
                assignment.AssignedEmployees.Clear();
                _dbContext.SaveChanges();
            }
            return Content("ok");
        }

        [HttpPost]
        public virtual ActionResult _DestroyAll(int campaignId)
        {
            return Content(DeleteHelper(
                campaignId,
                "Es können maximal 25 Aktionstage auf einmal gelöscht werden.",
                assignment =>
                {
                    assignment.State = AssignmentState.Deleted;
                }));
        }

        [HttpPost]
        public virtual ActionResult _UnassignAll(int campaignId)
        {
            return Content(DeleteHelper(
                campaignId,
                "Es können von maximal 25 Aktionstagen auf einmal die Zuweisungen entfernt werden.",
                assignment => assignment.AssignedEmployees.Clear()));
        }

        private string DeleteHelper(int campaignId, string errorMessage, Action<Assignment> action)
        {
            var request = Session["DeleteAssignmentsDataSourceRequest"] as DataSourceRequest;

            if (request == null)
                return errorMessage;

            var model = _dbContext.ActiveAssignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId)
                .Project()
                .To<DeleteAssignmentViewModel>()
                .ToDataSourceResult(request);

            if (model.Total > 25)
                return errorMessage;

            request.Page = 1;
            request.PageSize = int.MaxValue;

            model = _dbContext.ActiveAssignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId)
                .Project()
                .To<DeleteAssignmentViewModel>()
                .ToDataSourceResult(request);

            foreach (DeleteAssignmentViewModel deleteAssignment in model.Data)
            {
                var assignment = _dbContext.Assignments.Single(e => e.Id == deleteAssignment.Id);
                assignment.MarkForSync();
                action(assignment);
            }
            _dbContext.SaveChanges();

            return "ok";
        }
    }
}