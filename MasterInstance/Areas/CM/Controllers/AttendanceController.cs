﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Extensions;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class AttendanceController : Controller
    {
        private const int ShowAllDaysValue = 1;
        private readonly SeismitoadDbContext _dbContext;

        public AttendanceController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        //
        // GET: /CM/Attendance/
        public virtual ActionResult Index()
        {
            ViewBag.Title = "Promoteranwesenheit";
            return View();
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Campaign(int id)
        {
            ViewBag.Title = Helpers.PageTitle(id, "Promoteranwesenheit");
            return View(Views.ViewNames.Index, id);
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request,
                                          AttendanceFilterViewModel attendanceFilterViewModel)
        {
            var model = _dbContext.AssignedEmployees.Where(e =>
                e.AssignmentRoles.All(r => r.OnlySalesReport == false) &&
                e.Assignment.State == AssignmentState.Planned &&
                e.Assignment.CampaignLocation.Campaign.State <= CampaignState.Running);

            if(attendanceFilterViewModel.DateFrom.HasValue)
            {
                model = model.Where(e => attendanceFilterViewModel.DateFrom <= e.Assignment.DateStart);
            }

            if (attendanceFilterViewModel.DateTil.HasValue)
            {
                // Wir wollen einschließlich DateTil alles haben, daher addieren wir hier einen Tag
                attendanceFilterViewModel.DateTil = attendanceFilterViewModel.DateTil.Value.AddDays(1);
                model = model.Where(e => attendanceFilterViewModel.DateTil > e.Assignment.DateStart);
            }

            if (attendanceFilterViewModel.CampaignIds != null)
            {
                model = model
                    .Where(e => attendanceFilterViewModel.CampaignIds.Contains(e.Assignment.CampaignLocation.CampaignId));
            }

            model = model.OrderBy(e => e.AssignmentId).ThenBy(e => e.EmployeeId);

            var viewModel = model.Project().To<AttendanceViewModel>().ToDataSourceResult(request);

            foreach (AttendanceViewModel element in viewModel.Data)
            {
                element.Username = (element.AttendanceSetByUser != null
                    ? Membership.GetUser(element.AttendanceSetByUser)?.UserName
                    : null) ?? "n.A.";

//                element.StateColor = _dbContext.Database.SqlQuery<string>(@"
//SELECT Color
//FROM AssignedEmployeeExtraData
//WHERE AssignmentId = @AssignmentId AND EmployeeId = @EmployeeId",
//                    new SqlParameter("@AssignmentId", element.AssingmentId),
//                    new SqlParameter("@EmployeeId", element.EmployeeId))
//                    .SingleOrDefault();
            }

            return Json(viewModel);
        }

        [HttpPost]
        public virtual ActionResult _Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IList<AttendanceViewModel> models)
        {
            if (ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var assignedEmployee = _dbContext.AssignedEmployees
                        .Single(e => e.AssignmentId == model.AssingmentId && e.EmployeeId == model.EmployeeId);
                    Mapper.Map(model, assignedEmployee);
                    assignedEmployee.AttendanceSetDate = LocalDateTime.Now;
                    assignedEmployee.SetAttendanceSetBy(User.GetProviderUserKey());
                    assignedEmployee.Assignment.MarkForSync();
                    model.Type = assignedEmployee.AttendanceType;

                    model.Username = User.Identity.Name;
                }

                _dbContext.SaveChanges();
            }

            return Json(models.ToDataSourceResult(request, ModelState));
        }

        public virtual ActionResult _ReadCampaigns()
        {
            var model = _dbContext.Campaigns.Where(i => i.State < CampaignState.Archived).Select(e => new { Value = e.Id, Text = e.Name});
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult _Notes(int assignmentId, string notes, bool marker)
        {
            var assigment = _dbContext.Assignments.Single(e => e.Id == assignmentId);
            assigment.Notes = notes;
            assigment.Marker = marker ? 1 : 0;
            _dbContext.SaveChanges();
            return Content("ok");
        }
    }
}