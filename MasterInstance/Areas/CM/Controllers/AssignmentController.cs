﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Models.ViewModels;
using MasterInstance.Services;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class AssignmentController : Controller
    {
        private const string ChangeDateMessageSingle = "Dieser Einsatz liegt in der Vergangenheit. " +
                                           "Falls bereits ein Reporting für diesen Einsatz eingegeben wurde, wird es gelöscht. " +
                                           "Soll der Einsatz wirklich verschoben werden?";

        private const string ChangeDateMessageMultipleFormat = "Dieser Einsatz sowie {0} liegen in der Vergangenheit. " +
                                                           "Falls bereits Reportings für diese Einsätze eingegeben wurden, werden diese gelöscht. " +
                                                           "Soll die Einsätze wirklich verschoben werden?";


        private readonly SeismitoadDbContext _dbContext;

        public AssignmentController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult _FollowingAssignments([DataSourceRequest] DataSourceRequest request, int refAssignmentId, int currentEmployeeId, int newEmployeeId, string @for)
        {
            var referenceAssignment = _dbContext.Assignments
                .Where(e => e.Id == refAssignmentId)
                .Select(e => new
                {
                    e.DateStart,
                    e.CampaignLocation.CampaignId,
                    e.CampaignLocation.LocationId
                })
                .Single();

            var date = referenceAssignment.DateStart;
            var campaignId = referenceAssignment.CampaignId;
            var locationId = referenceAssignment.LocationId;
            
            var employeeAssignments = _dbContext.AssignedEmployees
                .Where(e => e.EmployeeId == newEmployeeId && e.Assignment.State == AssignmentState.Planned)
                .Select(e => e.Assignment);

            var forRemove = @for == "remove";

            var tmp = forRemove
                ? _dbContext.ActiveAssignments.Where(e => e.AssignedEmployees.Any(i => i.EmployeeId == currentEmployeeId))
                : currentEmployeeId != -1 // Ersetzung?
                    ? _dbContext.ActiveAssignments.Where(e => e.AssignedEmployees.Any(i => i.EmployeeId == currentEmployeeId) && e.AssignedEmployees.All(i => i.EmployeeId != newEmployeeId))
                    : _dbContext.ActiveAssignments.Where(e => e.AssignedEmployees.All(i => i.EmployeeId != newEmployeeId));

            var assignments = tmp.Where(e =>
                e.Id != refAssignmentId &&
                e.DateStart > date &&
                e.CampaignLocation.CampaignId == campaignId &&
                e.CampaignLocation.LocationId == locationId)
                .Select(e => new
                {
                    e.Id,
                    e.DateStart,
                    e.DateEnd,
                    e.Team,
                    Conflicts = employeeAssignments
                        .Where(i =>
                            i.Id != e.Id &&
                            i.DateStart.Day == e.DateStart.Day &&
                            i.DateStart.Month == e.DateStart.Month &&
                            i.DateStart.Year == e.DateStart.Year)
                        .Select(i => i.CampaignLocation.Campaign.Name + " (" + i.CampaignLocation.Location.City + ")")
                })
                .OrderBy(e => e.DateStart)
                .ToList() // damit wir gleich string.Join() verwenden können
                .Select(e => new
                {
                    e.Id,
                    e.DateStart,
                    e.DateEnd,
                    e.Team,
                    Conflicts = string.Join(", ", e.Conflicts)
                });
            var model = assignments.ToDataSourceResult(request);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult _Create(int campaignId, int locationId, DateTime start, DateTime end, int employeeCount)
        {
            var campaignLocation =
                _dbContext.CampaignsLocations.Single(e => e.CampaignId == campaignId && e.LocationId == locationId);

            if(start.Hour < 5)
            {
                start = start.AddDays(1);
            }

            if(end.Hour < 5)
            {
                end = end.AddDays(1);
            }

            var assignment = new Assignment
            {
                CampaignLocation = campaignLocation,
                EmployeeCount = employeeCount,
                DateStart = start,
                DateEnd = end,
                State = AssignmentState.Planned,
            };

            assignment.MarkForSync();
            _dbContext.Assignments.Add(assignment);
            _dbContext.SaveChanges();

            return Content("ok");
        }

        public virtual ActionResult _Delete(int assignmentId, bool confirmed, bool notify)
        {
            var result = new AjaxActionResultModel();
            var data = _dbContext.Assignments
                .Where(e => e.Id == assignmentId)
                .Select(e => new { Assignment = e, HasEmployees = e.AssignedEmployees.Any() })
                .Single();

            #region Wenn der Aktionstag in der Vergangenheit liegt muss der User bestätigen, dass eventuell Reports gelöscht werden
            if (!confirmed && data.Assignment.DateStart < LocalDateTime.Now && data.HasEmployees)
            {
                return Json(new AjaxActionResultModel
                {
                    status = "needs confirmation",
                    message = "Dieser Einsatz liegt in der Vergangenheit. " +
                    "Falls die zugewiesenen Promoter bereits Reportings für diesen Einsatz eingegeben haben, werden diese gelöscht. " +
                    "Soll der Aktionstag wirklich gelöscht werden?"
                });
            }
            #endregion
            data.Assignment.State = AssignmentState.Deleted;
            data.Assignment.MarkForSync();
            
            new PromoterNotification()
                .ForCancelledAssignemnt(data.Assignment)
                .ForImmediateDelivery(notify ? DeliveryTime.Now : DeliveryTime.Later);

            _dbContext.SaveChanges();

            return Json(result);
        }

        public class NotifiedAssignedEmployee
        {
            public int AssignmentId { get; set; }
            public int EmployeeId { get; set; }
        }


        public virtual ActionResult _UpdateDate(int assignmentId, DateTime start, DateTime end, string reason, AssignmentRepeatType repeat, bool confirmed, bool notify)
        {
            if(start.Hour < 5 && reason == "change time")
            {
                return Json(new AjaxActionResultModel
                {
                    status = "error",
                    message = "Die Startzeit darf nicht auf den nächsten Tag gelegt werden. Bitte stattdessen den Einsatz verschieben."
                });
            }
            var assignments = AssignController.MatchingAssignments(_dbContext, assignmentId, repeat)
                .Where(e => e.AssignedEmployees.All(i => !i.AssignmentRoles.Any(j => j.OnlySalesReport)))
                .ToList();

            var referenceAssignment = assignments.Single(e => e.Id == assignmentId);

            #region Erst prüfen, ob nur die Uhrzeit geändert wurde
            // Indem wir das Datum des Referenz-Assignments mit dem neuen Datum vergleichen stellen wir fest, ob sich auch
            // das Datum geändert hat oder nur die Uhrzeit
            if (referenceAssignment.DateStart.Date == start.Date)
            {
                var endIsNextDay = end.Hour < 5;
                foreach (var assignment in assignments)
                {
                    var day = assignment.DateStart;
                    assignment.DateStart = new DateTime(day.Year, day.Month, day.Day, start.Hour, start.Minute, start.Second);
                    assignment.DateEnd = new DateTime(day.Year, day.Month, day.Day, end.Hour, end.Minute, end.Second);
                    if (endIsNextDay) assignment.DateEnd = assignment.DateEnd.AddDays(1);
                    assignment.MarkForSync();
                    foreach (var assignedEmployee in assignment.AssignedEmployees)
                    {
                        _dbContext.Database.ExecuteSqlCommand(string.Format("UPDATE AssignedEmployeeExtraData SET Notified = NULL, Accepted = NULL, Rejected = NULL WHERE AssignmentId = {0} AND EmployeeId = {1}",
                                assignedEmployee.AssignmentId, assignedEmployee.EmployeeId));
                    }
                    _dbContext.SaveChanges();
                }
                new PromoterNotification()
                    .ForChangedTimeOfAssignments(assignments)
                    .ForImmediateDelivery(notify ? DeliveryTime.Now : DeliveryTime.Later);
                return Json(new AjaxActionResultModel { status = "ok", assignmentId = assignmentId });
            }
            #endregion

            AssignmentState postponedState;
            switch (reason)
            {
                case "postponed location":
                    postponedState = AssignmentState.PostponedLocation;
                    break;
                case "postponed promoter":
                    postponedState = AssignmentState.PostponedPromoter;
                    break;
                default:
                    throw new ArgumentException("Invalid reason: " + reason);
            }

            #region Wenn der/die Aktionstag(e) in der Vergangenheit liegt/liegen muss der User bestätigen, dass eventuell Reports gelöscht werden
            int count;
            if (!confirmed && (count = assignments.Count(e => e.DateStart < LocalDateTime.Now && e.AssignedEmployees.Any())) > 0)
            {
                var tmp = new AjaxActionResultModel();
                AssignController.SetAssignmentsWithReportsResult(count, tmp, ChangeDateMessageSingle, ChangeDateMessageMultipleFormat);
                return Json(tmp);
            }
            #endregion

            var dayDifference = start.Subtract(referenceAssignment.DateStart).TotalDays;

            #region Prüfen ob der/die Promoter am neuen Datum bereits einen anderen Einsatz haben
            var employeesWithConflicts = new List<string>();
            foreach (var assignment in assignments)
            {
                employeesWithConflicts.AddRange(
                    assignment.AssignedEmployees
                        .Where(e => _dbContext.GetConflictingAssignments(
                            e.Assignment.DateStart.AddDays(dayDifference),
                            e.Assignment.DateEnd.AddDays(dayDifference),
                            e.EmployeeId, e.AssignmentId).Any())
                        .Select(e => string.Format("{0} {1} ({2:dd.MM.yyyy})", e.Employee.Firstname, e.Employee.Lastname, e.Assignment.DateStart.AddDays(dayDifference)))
                    );
            }
            if (employeesWithConflicts.Any())
            {
                return Json(new AjaxActionResultModel
                {
                    status = "error",
                    message = (assignments.Count > 1 ? "Aktionstage wurden" : "Aktionstag wurde") + " nicht verschoben, da folgende Promoter an folgenden Tagen bereits einen Einsatz haben: " +
                        string.Join(", ", employeesWithConflicts)
                });
            }
            #endregion

            #region Prüfen ob es für diese Location bereits einen anderen Einsatz am neuen Datum gibt
            var locationsWithConflicts = new List<string>();
            foreach (var assignment in assignments)
            {
                var dateStart = assignment.DateStart.AddDays(dayDifference);
                if (_dbContext
                    .GetConflictingAssignmentsForCampaignLocation(
                        dateStart,
                        assignment.DateEnd.AddDays(dayDifference),
                        assignment.CampaignLocation.CampaignId,
                        assignment.CampaignLocation.LocationId).Any())
                {
                    locationsWithConflicts.Add(string.Format(
                        "Der Aktionstag wurde auf den {0:dd.MM.yyyy} verschoben. Allerdings gibt es an diesem Datum bereits einen sich überschneidenen Aktionstag.",
                        dateStart));
                }
            }
            var result = new AjaxActionResultModel { status = "ok" };
            if (locationsWithConflicts.Any())
            {
                result.status = "warning";
                result.message = string.Join("; ", locationsWithConflicts);
            }
            #endregion

            var notification = new PromoterNotification();

            // Muss gemacht werden bevor die AssignedEmployees gelöscht werden, da dieses löschen ein
            // Löschen von AssignedEmployeeExtradData triggert.
            var alreadyNotifiedAssignedEmployees = GetNotifiedAssignedEmployees(assignments.Select(e => e.Id));

            foreach (var assignment in assignments)
            {
                #region Neuen Aktionstag anlegen und Promoter übernehmen
                var newAssignment = new Assignment
                {
                    DateStart = assignment.DateStart.AddDays(dayDifference),
                    DateEnd = assignment.DateEnd.AddDays(dayDifference),
                    State = AssignmentState.Planned,
                    EmployeeCount = assignment.EmployeeCount,
                    CampaignLocation = assignment.CampaignLocation,
                    Predecessor = assignment,
                    AssignedEmployees = new Collection<AssignedEmployee>(),
                };
                newAssignment.MarkForSync();
                _dbContext.Assignments.Add(newAssignment);
                _dbContext.SaveChanges(); // Wir müssen hier leider schon mal zwischenspeichern, da wir newAssigment.Id brauchen falls wir UpdateMovement aufrufen

                foreach (var assignedEmployee in assignment.AssignedEmployees)
                {
                    var roleIds = assignedEmployee.AssignmentRoles.Select(e => e.Id);
                    var newAssignedEmployee = new AssignedEmployee
                    {
                        Assignment = newAssignment,
                        Employee = assignedEmployee.Employee,
                        AssignmentRoles = _dbContext.AssignmentRoles.Where(e => roleIds.Contains(e.Id)).ToList(),
                    };
                    newAssignment.AssignedEmployees.Add(newAssignedEmployee);
                    // Wenn der Promoter schon benachrichtigt wurde, ist es für ihn eine Verschiebung
                    if (alreadyNotifiedAssignedEmployees.Any(e => e.AssignmentId == assignedEmployee.AssignmentId && e.EmployeeId == assignedEmployee.EmployeeId))
                    {
                        notification.ForMovedAssignment(assignedEmployee, newAssignedEmployee);
                    }
                    else
                    {
                        var searchId = "," + assignedEmployee.AssignmentId + "_" + assignedEmployee.EmployeeId;

                        var previousMovementNotification = _dbContext.OutstandingNotifications
                            .SingleOrDefault(e => e.AssignmentIds.Contains(searchId) && (e.Type == "Verschobener Aktionstag" || e.Type == "Verschobene Aktionstage"));

                        // Wenn es bereits eine Verschiebungs-Benachrichtigung gibt, dann müssen wir diese Updaten
                        if(previousMovementNotification != null)
                        {
                            PromoterNotification.UpdateMovement(previousMovementNotification, assignment, newAssignment);
                        }
                        // Wenn wir hier sind heißt es, dass die vorherige Benachrichtigung zu diesem Einsatz entweder nicht vorhanden 
                        // ist (was eigentlich nicht sein kann, es sei denn der User hat sie gelöscht) oder sie ist vom Typ "Neuer Einsatz".
                        // Da der Promoter von dem neuen Einsatz nichts weiß ist es für ihne keine Verschiebung und wir generieren daher hier
                        // Stornierungen (zum löschen der vorherigen Zuweisungs-Benachrich.) sowie eine neue Zuweisungs-Benachrichtigung. 
                        else
                        {
                            notification.ForCancelledAssignments(new[] { assignedEmployee });
                            notification.ForNewAssignments(new[] { newAssignedEmployee });
                        }
                    }

                }
                #endregion

                #region Status (Verschoben) für alten Aktionstag setzen
                assignment.State = postponedState;
                #endregion

                #region Restliche Daten des alten Aktionstages aktualisieren
                _dbContext.AssignedEmployees.RemoveRange(assignment.AssignedEmployees);
                //assignment.AssignedEmployees.Clear();
                assignment.Successor = newAssignment;
                assignment.MarkForSync();
                #endregion

                // Erst speichern, damit das Assignment eine Id bekommt, da wir die gleich
                // für die Benachrichtigung brauchen
                _dbContext.SaveChanges();

                result.assignmentId = newAssignment.Id;
            }
            notification.ForImmediateDelivery(notify ? DeliveryTime.Now : DeliveryTime.Later);

            return Json(result);
        }

        private IList<NotifiedAssignedEmployee> GetNotifiedAssignedEmployees(IEnumerable<int> assignmentIds)
        {
            const string queryFmt = @"
SELECT AssignmentId, EmployeeId
FROM AssignedEmployeeExtraData
WHERE AssignmentId IN ({0}) AND Notified IS NOT NULL AND Rejected IS NULL
";
            return _dbContext.Database
                .SqlQuery<NotifiedAssignedEmployee>(string.Format(queryFmt, string.Join(",", assignmentIds)))
                .ToList();
        }
    }
}