﻿using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using MasterInstance.Extensions;
using MasterInstance.Areas.CM.Models;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class AcquisitionController : Controller
    {
        // GET: CM/Acqusition
        [MenuArea(MenuArea.Project)]
#if ADVANTAGE
        [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin + "," + SeismitoadShared.Constants.Roles.Advantage)]
#else
        [Authorize(Roles = SeismitoadShared.Constants.Roles.User)]
#endif
        [OutputCache(NoStore = true, Duration = 0, Location = OutputCacheLocation.None)]
        public virtual ActionResult Index(int id)
        {
            var path = Path.Combine(Server.MapPath("~/CampaignAssets/" + id), "AcquisitionColumns.txt");
            if (System.IO.File.Exists(path))
            {
                ViewBag.Columns = System.IO.File.ReadAllText(path).Split(',');
            }
            var model = new CampaignEmployeeViewModel
            {
                View = "Acquisition",
            };
            return View(MVC.CM.CampaignEmployees.Views.Index, model);
        }
    }
}