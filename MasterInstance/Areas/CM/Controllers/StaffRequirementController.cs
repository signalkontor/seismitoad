﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hangfire;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using MasterInstance.Models.ViewModels;
using Newtonsoft.Json;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using System.Globalization;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class StaffRequirementController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public StaffRequirementController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Show(int campaignId)
        {
            var path = GetRequirementsPath(campaignId);
            var model = new StaffRequirementsDisplay
            {
                CampaignId = campaignId,
                CampaignName = _dbContext.Campaigns.Single(e => e.Id == campaignId).Name,
                StaffRequirements = Directory
                    .GetFiles(path, "*.json")
                    .Select(Path.GetFileNameWithoutExtension)
                    .ToArray()
            };
            Array.Sort(model.StaffRequirements);
            return View(model);
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Print(int campaignId)
        {
            var path = GetRequirementsPath(campaignId);
            var requirements = Directory
                    .GetFiles(path, "*.json")
                    .ToArray();

            Array.Sort(requirements);

            var currentRequirement =
                JsonConvert.DeserializeObject<StaffRequirement>(System.IO.File.ReadAllText(requirements.Last()));

            var staffRequirementViewModel = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Project()
                .To<StaffRequirementViewModel>()
                .Single();

            var dateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(requirements.Last()), "yyyy-MM-ddTHH.mm.ss.fff", CultureInfo.InvariantCulture);
            ViewBag.Title = string.Format("Personalanforderung für „{0}“ - Stand {1:dd.MM.yyyy}", staffRequirementViewModel.Aktionstitel, dateTime);

            Mapper.Map(currentRequirement, staffRequirementViewModel);
            return View(staffRequirementViewModel);
        }

        public virtual ActionResult _Show(int campaignId, string version)
        {
            var staffRequirementViewModel = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Project()
                .To<StaffRequirementViewModel>()
                .Single();

            var requirements = GetRequirements(campaignId);
            var index = Array.FindIndex(requirements, path => path.Contains(version));
            var currentRequirement =
                JsonConvert.DeserializeObject<StaffRequirement>(System.IO.File.ReadAllText(requirements[index]));

            Mapper.Map(currentRequirement, staffRequirementViewModel);

            ViewBag.PreviousRequirement = index > 0
                ? JsonConvert.DeserializeObject<StaffRequirement>(System.IO.File.ReadAllText(requirements[index-1]))
                : null;

            return PartialView(staffRequirementViewModel);
        }

        // GET: CM/StaffRequirement
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int campaignId)
        {
            var staffRequirementViewModel = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Project()
                .To<StaffRequirementViewModel>()
                .Single();

            var staffRequirement = GetNewestRequirement(campaignId);
            if (staffRequirement != null)
            {
                Mapper.Map(staffRequirement, staffRequirementViewModel);
                staffRequirementViewModel.IsUpdate = true;
            }
            return View(staffRequirementViewModel);
        }

        public static void NotifyAdvantageUsers(string title, string username, bool isUpdate)
        {
            var emailService = Helpers.GetEmailService();
            var email = new StaffRequirementEmail("StaffRequirement", username, title, isUpdate);
#if ADVANTAGE
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
            var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
            var admins = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Admin);
            var userObj = Membership.GetUser(username, false);
            if (userObj != null)
            {
                email.AddRecipient(userObj.Email);
            }
            foreach (var user in users.Concat(admins).Distinct())
            {
                userObj = Membership.GetUser(user, false);
                if (userObj != null && userObj.UserName != username && userObj.IsApproved)
                {
                    email.AddRecipient(userObj.Email);
                }
            }
            emailService.Send(email);
        }

        [HttpPost, MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int campaignId, bool isUpdate, StaffRequirement staffRequirement)
        {
            var staffRequirementViewModel = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Project()
                .To<StaffRequirementViewModel>()
                .Single();
            staffRequirementViewModel.IsUpdate = isUpdate;

            if (ModelState.IsValid)
            {
                var previousRequirementPath = GetNewestRequirementPath(campaignId);
                var previousRequirementJson = previousRequirementPath != null ? System.IO.File.ReadAllText(previousRequirementPath) : "";
                var json = JsonConvert.SerializeObject(staffRequirement, Formatting.Indented);

                if (previousRequirementJson != json)
                {
                    var path = GetRequirementsPath(campaignId);
                    System.IO.File.WriteAllText(Path.Combine(path, string.Format("{0:yyyy-MM-dd}T{0:HH.mm.ss}.{0:fff}.json", LocalDateTime.Now)), json);
                }
                BackgroundJob.Enqueue(() => NotifyAdvantageUsers(staffRequirementViewModel.Aktionstitel, User.Identity.Name, !string.IsNullOrWhiteSpace(previousRequirementJson)));
                staffRequirementViewModel.IsUpdate = true;

            }
            Mapper.Map(staffRequirement, staffRequirementViewModel);
            return View(staffRequirementViewModel);
        }

        public virtual ActionResult _Upload(int campaignId)
        {
            var path = GetRequirementsPath(campaignId);
            foreach (string name in Request.Files)
            {
                var file = Request.Files[name];
                var fileName = Path.GetFileName(file.FileName);
                var fullPath = Path.Combine(path, fileName);
                if (Path.GetExtension(fileName).Equals(".pdf", StringComparison.OrdinalIgnoreCase))
                {
                    file.SaveAs(fullPath);
                    return Content("");
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public virtual ActionResult _RemoveUpload(int campaignId, string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            var path = GetRequirementsPath(campaignId);
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(path, fileName);
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        private static string GetRequirementsPath(int campaignId)
        {
            return HostingEnvironment.MapPath("~/CampaignAssets/" + campaignId + "/StaffRequirements");
        }

        public static StaffRequirement GetNewestRequirement(int campaignId)
        {
            var path = GetNewestRequirementPath(campaignId);
            return path != null
                ? new JavaScriptSerializer().Deserialize<StaffRequirement>(System.IO.File.ReadAllText(path))
                : null;
        }

        private static string[] GetRequirements(int campaignId)
        {
            var requirementsPath = GetRequirementsPath(campaignId);
            Directory.CreateDirectory(requirementsPath);
            var requirements = Directory.GetFiles(requirementsPath, "*.json");
            Array.Sort(requirements);
            return requirements;
        }

        private static string GetNewestRequirementPath(int campaignId)
        {
            var requirements = GetRequirements(campaignId);
            return requirements.Length > 0 ? requirements[requirements.Length - 1] : null;
        }
    }
}