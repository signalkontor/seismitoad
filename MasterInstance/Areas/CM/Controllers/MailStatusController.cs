﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using AdvantageModel.Models;
using AutoMapper.QueryableExtensions;
using Hangfire;
using Hangfire.States;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using MasterInstance.Models;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
    [MenuArea(MenuArea.Project)]
    public partial class MailStatusController : Controller
    {

        private readonly SeismitoadDbContext _dbContext;

        public MailStatusController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: CM/MailStatus
        public virtual ActionResult Index(int id)
        {
            return View(id);
        }

        public virtual ActionResult Failed(int id)
        {
            return View(id);
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            var model =
                _dbContext.Database.SqlQuery<CampaignEmployeeMail>(
                    @"SELECT JobId, MailType, CampaignId, EmployeeId, Firstname, Lastname, Done, QueuedAt, Error
FROM CampaignEmployeeMails
WHERE Done = 0 AND CampaignId = @CampaignId
", new SqlParameter("@CampaignId", campaignId)).ToList();
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult _ReadFailed([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            var model =
                _dbContext.Database.SqlQuery<CampaignEmployeeMail>(
                    @"SELECT JobId, MailType, CampaignId, EmployeeId, Firstname, Lastname, Done, QueuedAt, Error
FROM CampaignEmployeeMails
WHERE Error IS NOT NULL AND CampaignId = @CampaignId
", new SqlParameter("@CampaignId", campaignId)).ToList();
            return Json(model.ToDataSourceResult(request));
        }

        public virtual ActionResult Details(string id)
        {
            ViewBag.MailInfo = _dbContext.Database.SqlQuery<CampaignEmployeeMail>(
                @"SELECT JobId, MailType, CampaignId, EmployeeId, Firstname, Lastname, Done, QueuedAt, Error
FROM CampaignEmployeeMails
WHERE JobId = @JobId
", new SqlParameter("@JobId", id)).Single();

            var api = JobStorage.Current.GetMonitoringApi();
            var jobDetails = api.JobDetails(id);
            return View(jobDetails);
        }

        public virtual ActionResult Retry(string id)
        {
            var client = new BackgroundJobClient(JobStorage.Current);
            client.Requeue(id, FailedState.StateName);
            Thread.Sleep(1000);
            return RedirectToAction(MVC.CM.MailStatus.Details(id));
        }
    }
}