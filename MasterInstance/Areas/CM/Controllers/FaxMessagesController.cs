﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Models;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class FaxMessagesController : Controller
    {
        private const int ShowAllDaysValue = 1;
        private SeismitoadDbContext _dbContext;

        public FaxMessagesController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        //
        // GET: /CM/FaxMessages/
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult _Read([DataSourceRequest]DataSourceRequest request, FaxMessagesFilterViewModel faxMessagesFilterViewModel)
        {
            IQueryable<FaxMessage> model = _dbContext.FaxMessages;

            if (faxMessagesFilterViewModel.DateFilter != ShowAllDaysValue)
            {
                var dateStart = DateTime.Today.AddDays(faxMessagesFilterViewModel.DateFilter);
                var dateEnd = dateStart.AddDays(1);
                model = model.Where(e => dateStart <= e.Received && e.Received < dateEnd);
            }
            return Json(model.Project().To<FaxMessageViewModel>().ToDataSourceResult(request));
        }
	}
}