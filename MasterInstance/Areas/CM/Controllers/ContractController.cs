﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Elmah;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using Newtonsoft.Json;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
#if ADVANTAGE
    [Authorize(Roles = SeismitoadShared.Constants.Roles.Admin + "," + SeismitoadShared.Constants.Roles.Advantage)]
#else
    [Authorize(Roles = SeismitoadShared.Constants.Roles.User)]
#endif
    public partial class ContractController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;

        public ContractController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        private string GetContractPath(int campaignId)
        {
            return Server.MapPath("~/CampaignAssets/" + campaignId + "/Contracts");
        }

        private string[] GetContracts(int campaignId)
        {
            var contractPath = GetContractPath(campaignId);
            Directory.CreateDirectory(contractPath);
            var contracts = Directory.GetFiles(contractPath, "*.json");
            Array.Sort(contracts);
            return contracts;
        }

        // GET: CM/Contract
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int id)
        {
            return View(id);
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int campaignId)
        {
            var contracts = GetContracts(campaignId)
                .Select(e => new ContractListItem
                {
                    LastChange = System.IO.File.GetLastWriteTime(e),
                    Name = Path.GetFileNameWithoutExtension(e)
                });
            return Json(contracts.ToDataSourceResult(request));
        }
        
        public virtual ActionResult _DropdDownList(int campaignId)
        {
            var contracts = GetContracts(campaignId).Select(e => new {Name = Path.GetFileNameWithoutExtension(e)});
            return Json(contracts, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult _Create([DataSourceRequest] DataSourceRequest request, int campaignId, ContractListItem model)
        {
            var contracts = GetContracts(campaignId).Select(Path.GetFileNameWithoutExtension);
            if (contracts.Contains(model.Name))
            {
                ModelState.AddModelError("Name", "Dieser Name wird bereits verwendet.");
            }
            if (ModelState.IsValid)
            {
                var campaign = _dbContext.Campaigns.Single(e => e.Id == campaignId);
                var path = GetContractPath(campaignId);
                var filename = Path.Combine(path, model.Name + ".json");
                var contract = new Contract();
                Mapper.Map(campaign, contract);
                var requirement = StaffRequirementController.GetNewestRequirement(campaignId);
                if (requirement != null)
                {
                    Mapper.Map(requirement, contract);
                }
                System.IO.File.WriteAllText(filename, JsonConvert.SerializeObject(contract, Formatting.Indented));
            }
            return Json(new[] {model}.ToDataSourceResult(request, ModelState));
        }

        public virtual ActionResult _Destroy([DataSourceRequest] DataSourceRequest request, int campaignId, ContractListItem model)
        {
            var path = GetContractPath(campaignId);
            var filename = Path.Combine(path, model.Name + ".json");
            try
            {
                System.IO.File.Delete(filename);
            }
            catch { }
            return Json(new[] {model}.ToDataSourceResult(request));
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Edit(int id, string name)
        {
            ViewBag.CampaignId = id;
            ViewBag.ContractName = name;
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Display(int campaignId, string name, int? employeeId = null)
        {
            if (Request.IsLocal || Request.IsAuthenticated)
            {
                var employee = employeeId.HasValue
                    ? _dbContext.Employees
                        .Where(e => e.Id == employeeId)
                        .Select(e => new ContractDisplayModel
                        {
                            Title = e.Title,
                            Firstname = e.Firstname,
                            Lastname = e.Lastname,
                            Street = e.Profile.Street,
                            PostalCode = e.Profile.PostalCode,
                            City = e.Profile.City,
                        }).Single()
                    : new ContractDisplayModel
                    {
                         Title = "Anrede",
                         Firstname = "Vorname",
                         Lastname = "Nachname",
                         Street = "Straße",
                         PostalCode = "PLZ",
                         City = "Ort"
                    };
                return View(employee);
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [AllowAnonymous]
        public virtual ActionResult _Data(int campaignId, string name)
        {
            if (Request.IsLocal || Request.IsAuthenticated)
            {
                var path = GetContractPath(campaignId);
                var filename = Path.Combine(path, name + ".json");
                return File(filename, "application/json");
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        // Diese Update Methode gehört nicht zum Grid, sondern zu Edit()
        [HttpPost]
        public virtual ActionResult _Update(int campaignId, string name, Contract model)
        {
            var json = JsonConvert.SerializeObject(model, Formatting.Indented);
            var path = GetContractPath(campaignId);
            var filename = Path.Combine(path, name + ".json");
            System.IO.File.WriteAllText(filename, json);
            return Content("ok");
        }

        public virtual ActionResult Preview(int campaignId, string name, int? employeeId = null)
        {
            var tempFile = Path.GetTempFileName();
            GeneratePdf(Server, Url, tempFile, campaignId, name, employeeId);
            var bytes = System.IO.File.ReadAllBytes(tempFile);
            System.IO.File.Delete(tempFile);
            return File(bytes, "application/pdf", "Vorschau.pdf");
        }

        public static void GeneratePdf(string path, string wkhtmlPath, string pdfTkPath, string backgroundPdfPath, string url)
        {
            try
            {
                var tempFile = Path.GetTempFileName();
                var process = Process.Start(wkhtmlPath,
                    string.Format(
                        "--margin-left 25mm --margin-right 25mm --margin-top 50mm --margin-bottom 41mm --print-media-type \"{0}\" \"{1}\"",
                        url, tempFile));
                process.WaitForExit(20 * 1000); // Maximal 20 Sekunden warten

                process = Process.Start(pdfTkPath,
                    string.Format("\"{0}\" multibackground \"{1}\" output \"{2}\"", tempFile, backgroundPdfPath, path));
                process.WaitForExit(20 * 1000);
                System.IO.File.Delete(tempFile);
            }
            catch (Exception e)
            {
                ErrorLog.GetDefault(null).Log(new Error(e));
                throw;
            }
        }

        public static void GeneratePdf(HttpServerUtilityBase server, UrlHelper urlHelper, string path, int campaignId, string name, int? employeeId = null)
        {
            var wkhtmlPath = server.MapPath("~/App_Data/wkhtmltopdf/wkhtmltopdf.exe");
            var pdfTkPath = server.MapPath("~/App_Data/wkhtmltopdf/pdftk.exe");
            var backgroundPdfPath = server.MapPath("~/App_Data/Projektauftrag_blanko.pdf");
            var url = urlHelper.ActionAbsolute(MVC.CM.Contract.Display(campaignId, name, employeeId));
            GeneratePdf(path, wkhtmlPath, pdfTkPath, backgroundPdfPath, url);
        }
    }
}