﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using LinqKit;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Areas.MD.Models;
using MasterInstance.Extensions;
using ObjectTemplate.Extensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Controllers;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class ExportController : Controller
    {
        private static readonly Expression<Func<AssignedEmployee, bool>> NotSalesReport =
            e => e.AssignmentRoles.All(i => i.OnlySalesReport == false);

        private readonly SeismitoadDbContext _dbContext;

        public ExportController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Assignments(int id)
        {
            var model = new ExportAssignmentModel
            {
                CampaignId = id
            };
            return View(model);
        }

        private static readonly Dictionary<string, string> ColorStyleNameMapping = new Dictionary<string, string>()
        {
            { "red", "abgelehnt" },
            { "red-deleted", "gelöscht" },
            { "yellow", "nicht vollständig besetzt" },
            { "green", "bestätigt" },
            { "purple", "verschoben" },
            { "pink", "zugewiesen und benachrichtigt" },
            { "blue", "zugewiesen aber noch nicht benachrichtigt" },
        };

        [HttpPost]
        public virtual ActionResult Assignments(ExportAssignmentModel model, int id)
        {
            _dbContext.Database.CommandTimeout = 5 * 60;
            _dbContext.Configuration.LazyLoadingEnabled = false;
            _dbContext.Configuration.AutoDetectChangesEnabled = false;
            _dbContext.Configuration.ProxyCreationEnabled = false;

            // Grundsätzlich fangen wir mit allen Einsätzen an
            var assignments = _dbContext.ViewAssignments.Where(e => e.CampaignId == id);

            // Wir filtern nur dann, wenn wir einen Filter haben und dort irgendwas gewählt wurde und in der Auswahl der Filter für "Alle" nicht dabei war
            if (model.AssignmentFilterExport != null && model.AssignmentFilterExport.Length > 0 && model.AssignmentFilterExport.All(e => e != AssignmentFilterExport.All))
            {
                assignments = assignments.AsExpandable();

                var confirmed = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Confimed);
                var unconfirmed = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Unconfimed);
                var rejected = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Rejected);
                var notFullyAssigned = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Open);
                var assigned = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Assigned);
                var deleted = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Deleted);
                var moved = model.AssignmentFilterExport.Contains(AssignmentFilterExport.Moved);

                var allTrue = assigned && notFullyAssigned && deleted && moved && confirmed && unconfirmed && rejected;
                var noneTrue = !assigned && !notFullyAssigned && !deleted && !moved && !confirmed && !unconfirmed && !rejected;

                if (!allTrue && !noneTrue)
                {
                    // Wir müssen nur etwas filtern, wenn mindestens einer der Filter, nicht aber alle gewählt sind. Kein Filter und
                    // alle Filter heben sich nämlich auf.
                    var predicate = PredicateBuilder.False<ViewAssignment>();

                    if (notFullyAssigned) // yellow
                        predicate = predicate.Or(e => e.AssignedEmployeeCount < e.EmployeeCount && e.State == AssignmentState.Planned);
                    if (deleted) // dark red
                        predicate = predicate.Or(e => e.State == AssignmentState.Deleted);
                    if (moved) // purple
                        predicate = predicate.Or(e => e.State == AssignmentState.PostponedLocation || e.State == AssignmentState.PostponedPromoter);

                    var allowedColors = new List<string>(5);
                    if (assigned) allowedColors.Add("blue");
                    if (confirmed) allowedColors.Add("green");
                    if (unconfirmed) allowedColors.Add("pink");
                    if (rejected) allowedColors.Add("red");
                    if (allowedColors.Count > 0)
                    {
                        predicate = predicate.Or(e => allowedColors.Contains(e.Color));
                    }
                    assignments = assignments.Where(predicate);
                }

                if(model.AssignmentFilterExport.Contains(AssignmentFilterExport.Marked))
                {
                    assignments = assignments.Where(e => e.Marker > 0);
                }
            }

            assignments = assignments.OrderBy(e => e.DateStart);

            if (model.Start != null)
                assignments = assignments.Where(e => e.DateStart >= model.Start);
            if (model.End != null)
                assignments = assignments.Where(e => e.DateStart < model.End);

            if (!assignments.Any())
            {
                var p = new ExcelPackage();
                var w = p.Workbook.Worksheets.Add("Filterung ergab keine Daten");
                w.Cells[1, 1].Value = "Filterung ergab keine Daten";
                return new Excel2007FileResult("Aktionstage.xlsx", p);
            }

            var minDate = assignments.Min(e => e.DateStart).Date;
            var maxDate = assignments.Max(e => e.DateEnd).Date;

            // EPPlus
            var package = new ExcelPackage();

            //Create the worksheet
            var worksheet = package.Workbook.Worksheets.Add("Aktionstage");

            var purpleStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["purple"]);
            purpleStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            purpleStyle.Style.Fill.BackgroundColor.SetColor(Color.Purple);
            purpleStyle.Style.Font.Color.SetColor(Color.White);

            var redStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["red"]);
            redStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            redStyle.Style.Fill.BackgroundColor.SetColor(Color.Red);

            var redDeletedStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["red-deleted"]);
            redDeletedStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            redDeletedStyle.Style.Fill.BackgroundColor.SetColor(Color.Firebrick);

            var yellowStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["yellow"]);
            yellowStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            yellowStyle.Style.Fill.BackgroundColor.SetColor(Color.Yellow);

            var greenStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["green"]);
            greenStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            greenStyle.Style.Fill.BackgroundColor.SetColor(Color.LightGreen);

            var pinkStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["pink"]);
            pinkStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            pinkStyle.Style.Fill.BackgroundColor.SetColor(Color.DeepPink);

            var blueStyle = package.Workbook.Styles.CreateNamedStyle(ColorStyleNameMapping["blue"]);
            blueStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
            blueStyle.Style.Fill.BackgroundColor.SetColor(Color.DeepSkyBlue);

            //Format the header
            using (var range = worksheet.Cells["1:1"])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(126, 177, 238));
                range.Style.Font.Color.SetColor(Color.White);
            }

            var colCount = 1;
            worksheet.Cells[1, colCount++].Value = "Markt";
            worksheet.Cells[1, colCount++].Value = "Straße";
            worksheet.Cells[1, colCount++].Value = "PLZ";
            worksheet.Cells[1, colCount++].Value = "Ort";
            for (var date = minDate; date <= maxDate; date = date.AddDays(1))
            {
                worksheet.Cells[1, colCount++].Value = date.ToString("ddd dd.MM.yyyy");
            }

            var rownum = 2;
            foreach (var grouping in assignments.GroupBy(e => e.LocationId))
            {
                var location = _dbContext.Locations.Where(e => e.Id == grouping.Key)
                    .Select(e => new { e.Name, e.Street, e.PostalCode, e.City })
                    .Single();

                var cellnum = 1;
                var maxInnerRowCount = 1;
                var row = worksheet.Row(rownum);
                worksheet.Cells[rownum, cellnum++].Value = location.Name;
                worksheet.Cells[rownum, cellnum++].Value = location.Street;
                worksheet.Cells[rownum, cellnum++].Value = location.PostalCode;
                worksheet.Cells[rownum, cellnum++].Value = location.City;
                for (var d = minDate; d <= maxDate; d = d.AddDays(1))
                {
                    var date = d;
                    var assignmentsForDay = grouping.Where(e => date <= e.DateStart && e.DateStart < date.AddDays(1));
                    var innerRowCount = 0;

                    foreach (var assignment in assignmentsForDay)
                    {
                        if (innerRowCount > 0)
                        {
                            row = worksheet.Row(rownum + innerRowCount);
                        }
                        var cell = worksheet.Cells[row.Row, cellnum];
                        cell.Value = string.Format("{4}{0:HH:mm}-{1:HH:mm}: {2} {3}", assignment.DateStart, assignment.DateEnd, assignment.EmployeeFirstname, assignment.EmployeeLastname, assignment.Marker == 1 ? "! " : "");

                        if (!string.IsNullOrWhiteSpace(assignment.Notes))
                        {
                            cell.AddComment(assignment.Notes, "Aktionstagnotizen");
                        }
                        if (assignment.PredecessorDateStart.HasValue)
                        {
                            cell.Value += string.Format(" (Ersatztermin für {0:dd.MM.yyyy})", assignment.PredecessorDateStart.Value);
                        }
                        if (assignment.State == AssignmentState.Planned)
                        {
                            cell.StyleName = ColorStyleNameMapping[assignment.Color ?? "yellow"];
                        }
                        else
                        {
                            if (assignment.State == AssignmentState.PostponedLocation && assignment.SuccessorDateStart.HasValue)
                            {
                                cell.Value += string.Format(" (Verschoben (Marktwunsch) auf {0:dd.MM.yyyy})", assignment.SuccessorDateStart.Value);
                            }
                            if(assignment.State == AssignmentState.PostponedPromoter && assignment.SuccessorDateStart.HasValue)
                            {
                                cell.Value += string.Format(" (Verschoben (Promoterausfall) auf {0:dd.MM.yyyy})", assignment.SuccessorDateStart.Value);
                            }
                            cell.StyleName = ColorStyleNameMapping[assignment.State == AssignmentState.Deleted
                                ? "red-deleted"
                                : "purple"];
                        }
                        innerRowCount++;
                    }

                    // reset to first inner row
                    row = worksheet.Row(rownum);

                    maxInnerRowCount = Math.Max(maxInnerRowCount, innerRowCount);
                    cellnum++;
                }
                rownum += maxInnerRowCount;
            }

            // Freeze first column and first row
            worksheet.View.FreezePanes(2, 5);
            worksheet.Cells.AutoFitColumns();

            //Write it back to the client
            return new Excel2007FileResult("Aktionstage.xlsx", package);
        }

        public virtual ActionResult Attendance(int[] campaignIds, int? employeeId, DateTime? from, DateTime? til)
        {
            campaignIds = campaignIds ?? new int[0];

            string filename = null;
            Expression<Func<AssignedEmployee, bool>> filterPredicate = null;
            if (employeeId.HasValue)
            {
                var employee = _dbContext.ActiveEmployees.Single(e => e.Id == employeeId);
                filename = $"Export_{employee.Firstname}_{employee.Lastname}.xlsx";
                filterPredicate = e => e.EmployeeId == employeeId;
            }
            else
            {
                filename = "Anwesenheit.xlsx";
                if (campaignIds.Length > 0)
                    filterPredicate = e => campaignIds.Contains(e.Assignment.CampaignLocation.CampaignId);
                else
                    filterPredicate = e => true;
            }

            var assignedEmployees = _dbContext.AssignedEmployees
                .Where(e => e.AssignmentRoles.All(r => r.OnlySalesReport == false) &&
                            e.Assignment.State == AssignmentState.Planned &&
                            e.Assignment.CampaignLocation.Campaign.State <= CampaignState.Running);

            if (from.HasValue)
                assignedEmployees = assignedEmployees.Where(e => e.Assignment.DateStart >= from);
            if (til.HasValue)
            {
                til = til.Value.AddDays(1);
                assignedEmployees = assignedEmployees.Where(e => e.Assignment.DateStart < til);
            }

            return Attendance(assignedEmployees.Where(filterPredicate), filename);
        }

        private static ActionResult Attendance(IQueryable<AssignedEmployee> assignedEmployees, string filename)
        {
            var row = 1;
            var excelPackage = new ExcelPackage();
            var excelWorksheet = excelPackage.Workbook.Worksheets.Add("Anwesenheiten");
            excelWorksheet.Cells[row, 1].Value = "Start";
            excelWorksheet.Cells[row, 2].Value = "Ende";
            excelWorksheet.Cells[row, 3].Value = "Aktion";
            excelWorksheet.Cells[row, 4].Value = "Name";
            excelWorksheet.Cells[row, 5].Value = "Straße";
            excelWorksheet.Cells[row, 6].Value = "PLZ";
            excelWorksheet.Cells[row, 7].Value = "Ort";
            excelWorksheet.Cells[row, 8].Value = "Promoter";
            excelWorksheet.Cells[row, 9].Value = "Anwesenheit";
            excelWorksheet.Cells[row, 10].Value = "Bemerkung";
            excelWorksheet.View.FreezePanes(2, 1);
            foreach (var assignedEmployee in assignedEmployees
                .Where(e => e.Assignment.State == AssignmentState.Planned && e.AssignmentRoles.All(r => r.OnlySalesReport == false))
                .OrderBy(e => e.Assignment.DateStart))
            {
                row++;
                excelWorksheet.Cells[row, 1].Value = assignedEmployee.Assignment.DateStart.ToString(SeismitoadShared.Constants.Other.GermanCultureInfo);
                excelWorksheet.Cells[row, 2].Value = assignedEmployee.Assignment.DateEnd.ToString(SeismitoadShared.Constants.Other.GermanCultureInfo);
                excelWorksheet.Cells[row, 3].Value = assignedEmployee.Assignment.CampaignLocation.Campaign.Name;
                excelWorksheet.Cells[row, 4].Value = assignedEmployee.Assignment.CampaignLocation.Location.Name;
                excelWorksheet.Cells[row, 5].Value = assignedEmployee.Assignment.CampaignLocation.Location.Street;
                excelWorksheet.Cells[row, 6].Value = assignedEmployee.Assignment.CampaignLocation.Location.PostalCode;
                excelWorksheet.Cells[row, 7].Value = assignedEmployee.Assignment.CampaignLocation.Location.City;
                excelWorksheet.Cells[row, 8].Value = assignedEmployee.Employee.Firstname + " " + assignedEmployee.Employee.Lastname;
                excelWorksheet.Cells[row, 9].Value = assignedEmployee.Attendance.GetDisplayName();
                excelWorksheet.Cells[row, 10].Value = assignedEmployee.AttendanceComment;
            }

            excelWorksheet.Cells.AutoFitColumns();
            return new Excel2007FileResult(filename, excelPackage);
        }

        public virtual ActionResult TrainingParticipant(int trainingDayId)
        {
            var participants = _dbContext.TrainingParticipants
                .Where(e => e.TrainingDayId == trainingDayId).Project().To<TrainingParticipantViewModel>();

            var row = 1;
            var excelPackage = new ExcelPackage();
            var excelWorksheet = excelPackage.Workbook.Worksheets.Add("Anwesenheiten");
            excelWorksheet.Cells[row, 1].Value = "Promoter Id";
            excelWorksheet.Cells[row, 2].Value = "Nachname";
            excelWorksheet.Cells[row, 3].Value = "Vorname";
            excelWorksheet.Cells[row, 4].Value = "PLZ";
            excelWorksheet.Cells[row, 5].Value = "EMail";
            excelWorksheet.Cells[row, 6].Value = "Telefon";
            excelWorksheet.Cells[row, 7].Value = "Mobil";
            excelWorksheet.Cells[row, 8].Value = "Projektstatus";
            excelWorksheet.Cells[row, 9].Value = "Schulungsstatus";
            excelWorksheet.Cells[row, 10].Value = "Anreise";
            excelWorksheet.Cells[row, 11].Value = "Anreisekosten";
            excelWorksheet.Cells[row, 12].Value = "Hotel";
            excelWorksheet.Cells[row, 13].Value = "Bemerkung";
            excelWorksheet.View.FreezePanes(2, 1);

            foreach (var participant in participants)
            {
                row++;
                excelWorksheet.Cells[row, 1].Value = participant.EmployeeId;
                excelWorksheet.Cells[row, 2].Value = participant.Lastname;
                excelWorksheet.Cells[row, 3].Value = participant.Firstname;
                excelWorksheet.Cells[row, 4].Value = participant.PostalCode;
                excelWorksheet.Cells[row, 5].Value = participant.Email;
                var employeeProfile = _dbContext.EmployeeProfiles.SingleOrDefault(e => e.Id == participant.EmployeeId);
                if(employeeProfile != null)
                {
                    excelWorksheet.Cells[row, 6].Value = employeeProfile.PhoneNo;
                    excelWorksheet.Cells[row, 7].Value = employeeProfile.PhoneMobileNo;
                }
                else
                {
                    excelWorksheet.Cells[row, 6].Value = "";
                    excelWorksheet.Cells[row, 7].Value = "";
                }
                excelWorksheet.Cells[row, 8].Value = participant.CampaignEmployeeState;
                excelWorksheet.Cells[row, 9].Value = participant.State;
                excelWorksheet.Cells[row, 10].Value = participant.Travel;
                excelWorksheet.Cells[row, 11].Value = participant.TravelCost;
                excelWorksheet.Cells[row, 12].Value = participant.Hotel;
                excelWorksheet.Cells[row, 13].Value = participant.Remark;
            }

            excelWorksheet.Cells.AutoFitColumns();

            var trainingTitle = _dbContext.TrainingDays.Single(e => e.Id == trainingDayId).Training.Title;
            while (trainingTitle.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                var index = trainingTitle.IndexOfAny(Path.GetInvalidFileNameChars());
                trainingTitle = trainingTitle.Replace(trainingTitle[index], '-');
            }
            return new Excel2007FileResult(string.Format("Export_Schulungsteilnehmer_{0}.xlsx", trainingTitle), excelPackage);
        }

        public virtual ActionResult CampaignLocations(int campaignId)
        {
            var campaignLocations = _dbContext.CampaignsLocations
                .Where(e => e.CampaignId == campaignId)
                .Include(e => e.Contact)
                .Include(e => e.Location.LocationGroup)
                .Include(e => e.Location.LocationType)
                .Include(e => e.Location.StateProvince)
                .Include(e => e.Location.CampaignAreas);

            var row = 1;
            var col = 1;
            var excelPackage = new ExcelPackage();
            var excelWorksheet = excelPackage.Workbook.Worksheets.Add("Aktionsorte");
            excelWorksheet.Cells[row, col++].Value = "Channel";
            excelWorksheet.Cells[row, col++].Value = "Name";
            excelWorksheet.Cells[row, col++].Value = "Zusatz";
            excelWorksheet.Cells[row, col++].Value = "Straße";
            excelWorksheet.Cells[row, col++].Value = "Straße Zusatz";
            excelWorksheet.Cells[row, col++].Value = "PLZ";
            excelWorksheet.Cells[row, col++].Value = "Ort";
            excelWorksheet.Cells[row, col++].Value = "Vertriebsregion";
            excelWorksheet.Cells[row, col++].Value = "Klassifizierung";
            excelWorksheet.Cells[row, col++].Value = "Bemerkung";
            excelWorksheet.Cells[row, col++].Value = "Centerverwaltung - Straße";
            excelWorksheet.Cells[row, col++].Value = "Centerverwaltung - PLZ";
            excelWorksheet.Cells[row, col++].Value = "Centerverwaltung - Ort";
            excelWorksheet.Cells[row, col++].Value = "Telefon";
            excelWorksheet.Cells[row, col++].Value = "Fax";
            excelWorksheet.Cells[row, col++].Value = "E-Mail";
            excelWorksheet.Cells[row, col++].Value = "Website";
            excelWorksheet.Cells[row, col++].Value = "Besucher / Tag";
            excelWorksheet.Cells[row, col++].Value = "Gesamtfläche (in qm)";
            excelWorksheet.Cells[row, col++].Value = "Verkaufsfläche (in qm)";
            excelWorksheet.Cells[row, col++].Value = "Preise Aktionsfläche";
            excelWorksheet.Cells[row, col++].Value = "Publikumsniveau";
            excelWorksheet.Cells[row, col++].Value = "Eröffnungsjahr";
            excelWorksheet.Cells[row, col++].Value = "Einzugsgebiet";
            excelWorksheet.Cells[row, col++].Value = "Parkplätze";
            excelWorksheet.Cells[row, col++].Value = "Standfläche Außenbereich";
            excelWorksheet.Cells[row, col++].Value = "Vorhandene Retailer";
            excelWorksheet.Cells[row, col++].Value = "WLAN vorhanden (Aktion / Allgemein)";
            excelWorksheet.Cells[row, col++].Value = "Größe Konferenzräume";
            excelWorksheet.Cells[row, col++].Value = "Kostenübernahme möglich";
            excelWorksheet.Cells[row, col++].Value = "Zimmerpreise";
            excelWorksheet.Cells[row, col++].Value = "Kosten Konferenztechnik";
            excelWorksheet.Cells[row, col++].Value = "Kosten Tagungspaket";
            excelWorksheet.Cells[row, col++].Value = "Bundesland";
            excelWorksheet.Cells[row, col++].Value = "Typ";
            excelWorksheet.Cells[row, col++].Value = "ASP Anrede";
            excelWorksheet.Cells[row, col++].Value = "ASP Vorname";
            excelWorksheet.Cells[row, col++].Value = "ASP Nachname";
            excelWorksheet.Cells[row, col++].Value = "ASP Email";
            excelWorksheet.Cells[row, col++].Value = "ASP Telefon";
            excelWorksheet.Cells[row, col++].Value = "ASP Funktion";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Mo";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Di";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Mi";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Do";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Fr";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten Sa";
            excelWorksheet.Cells[row, col++].Value = "Öffnungszeiten So";
            excelWorksheet.Cells[row, col++].Value = "Aktionsflächen";
            excelWorksheet.View.FreezePanes(2, 1);

            var audience = SelectListRepository.Retrieve("Audience", ViewData).ToDictionary(e => e.Value, e => e.Text);

            foreach (var campaignLocation in campaignLocations)
            {
                col = 1;
                row++;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.LocationGroup != null ? campaignLocation.Location.LocationGroup.Name : "";
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Name;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Name2;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Street;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Street2;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.PostalCode;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.City;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.SalesRegion;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Classification;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Notes;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CenterManagementStreet;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CenterManagementPostalCode;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CenterManagementCity;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Phone;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Fax;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Email;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Website;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.VisitorFrequency;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Area;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.SellingArea;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CampaignAreaPrices;
                var audienceStr = campaignLocation.Location.Audience.HasValue
                    ? audience[campaignLocation.Location.Audience.ToString()]
                    : "-";
                excelWorksheet.Cells[row, col++].Value = audienceStr;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Opening;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CatchmentArea;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Parking;
                excelWorksheet.Cells[row, col++].Value = NullableBoolToString(campaignLocation.Location.OutdoorArea);
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.Retailers;
                excelWorksheet.Cells[row, col++].Value = NullableBoolToString(campaignLocation.NetworkConnectionAvailable) + " / " + NullableBoolToString(campaignLocation.Location.WiFiAvailable);
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.ConferenceRoomSize;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.CostTransferPossible;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.RoomRates;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.ConferenceEquipmentCost;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.ConferencePackageCost;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.StateProvince != null ? campaignLocation.Location.StateProvince.Name : "";
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.LocationType.Name;
                if (campaignLocation.Contact != null)
                {
                    // Werden hier mehr oder weniger Zeilen ausgegeben, muss auch der else-Teil angepasst werden!
                    var c = campaignLocation.Contact;
                    excelWorksheet.Cells[row, col++].Value = c.Title;
                    excelWorksheet.Cells[row, col++].Value = c.Firstname;
                    excelWorksheet.Cells[row, col++].Value = c.Lastname;
                    excelWorksheet.Cells[row, col++].Value = c.Email;
                    excelWorksheet.Cells[row, col++].Value = c.Phone;
                    excelWorksheet.Cells[row, col++].Value = c.Type;
                }
                else
                {
                    // Wird die Anzahl der Zeilen im if-Teil geänder muss auch dieser Wert angepasst werden.
                    col += 6;
                }
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.MondayStartTime + "-" + campaignLocation.Location.OpeningHours.MondayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.TuesdayStartTime + "-" + campaignLocation.Location.OpeningHours.TuesdayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.WednesdayStartTime + "-" + campaignLocation.Location.OpeningHours.WednesdayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.ThursdayStartTime + "-" + campaignLocation.Location.OpeningHours.ThursdayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.FridayStartTime + "-" + campaignLocation.Location.OpeningHours.FridayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.SaturdayStartTime + "-" + campaignLocation.Location.OpeningHours.SaturdayEndTime;
                excelWorksheet.Cells[row, col++].Value = campaignLocation.Location.OpeningHours.SundayStartTime + "-" + campaignLocation.Location.OpeningHours.SundayEndTime;
                excelWorksheet.Cells[row, col++].Value = string.Join(", ", campaignLocation.Location.CampaignAreas);
            }

            excelWorksheet.Cells.AutoFitColumns();
            return new Excel2007FileResult("Aktionsorte.xlsx", excelPackage);
        }

        private static string NullableBoolToString(bool? theBool)
        {
            if (theBool.HasValue)
            {
                return theBool.Value ? "Ja" : "Nein";
            }
            return "-";
        }

        public virtual ActionResult PromoterList(int campaignId)
        {
            var campaignEmployees = _dbContext.CampaignEmployees
                .Where(e => e.CampaignId == campaignId && e.Accepted.HasValue)
                .Include(e => e.TrainingParticipants.Select(i => i.TrainingDay.Training))
                .Include(e => e.Employee.Profile);

            var trainingDays = _dbContext.TrainingDays
                .Where(e => e.Participants.Any(i => i.CampaignEmployee.CampaignId == campaignId))
                .Select(e => new {e.Id, e.DateFrom, e.DateTil, e.Training.Title})
                .OrderBy(e => e.Title).ThenBy(e => e.DateFrom)
                .ToArray();

            var row = 1;
            var col = 1;
            var excelPackage = new ExcelPackage();
            var excelWorksheet = excelPackage.Workbook.Worksheets.Add("Personalliste");

            var campaignEmployeeStates = SelectListRepository.Retrieve("CampaignEmployeeState", ViewData).ToDictionary(e => e.Value, e => e.Text);
            var trainingStates = SelectListRepository.Retrieve("TrainingState", ViewData).ToDictionary(e => e.Value, e => e.Text);
            var shirtSizes = SelectListRepository.Retrieve("ShirtSize", ViewData).ToDictionary(e => e.Value, e => e.Text);

            excelWorksheet.Cells[row, col++].Value = "Anrede";
            excelWorksheet.Cells[row, col++].Value = "Vorname";
            excelWorksheet.Cells[row, col++].Value = "Nachname";
            excelWorksheet.Cells[row, col++].Value = "Straße";
            excelWorksheet.Cells[row, col++].Value = "PLZ";
            excelWorksheet.Cells[row, col++].Value = "Ort";
            excelWorksheet.Cells[row, col++].Value = "Mobilnummer";
            excelWorksheet.Cells[row, col++].Value = "E-Mail";
            excelWorksheet.Cells[row, col++].Value = "Status";
            excelWorksheet.Cells[row, col++].Value = "Initiativ beworben";
            excelWorksheet.Cells[row, col++].Value = "Aktueller Status";
            excelWorksheet.Cells[row, col++].Value = "Im Status seit";
            excelWorksheet.Cells[row, col++].Value = "Projektauftrag angenommen am";
            excelWorksheet.Cells[row, col++].Value = "Bemerkung";
            excelWorksheet.Cells[row, col++].Value = "Beworben für";
            excelWorksheet.Cells[row, col++].Value = "Rahmenvertrag";
            excelWorksheet.Cells[row, col++].Value = "Fragebogen zur Selbstständigkeit vorhanden";
            excelWorksheet.Cells[row, col++].Value = "Gewerbeschein";
            excelWorksheet.Cells[row, col++].Value = "T-Shirt-Größe";
            excelWorksheet.Cells[row, col++].Value = "Körpergröße";
            excelWorksheet.Cells[row, col++].Value = "Führerscheinklassen";
            excelWorksheet.Cells[row, col++].Value = "Skype";
            excelWorksheet.Cells[row, col++].Value = "Bemerkung (aus Profil)";

            foreach (var t in trainingDays)
            {
                excelWorksheet.Cells[row, col++].Value = $"{t.Title} ({t.DateFrom:dd.MM.yyyy} - {t.DateTil:dd.MM.yyyy})";
            }

            excelWorksheet.View.FreezePanes(2, 1);

            foreach (var campaignEmployee in campaignEmployees)
            {
                col = 1;
                row++;
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Title;
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Firstname;
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Lastname;
                if (campaignEmployee.Employee.Profile != null)
                {
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.Street;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.PostalCode;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.City;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.PhoneMobileNo;
                }
                else
                {
                    col += 4;
                }
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Email;
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.State.GetDisplayName();
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Invited == null && campaignEmployee.InformationRequested != null ? "Ja" : "Nein";
                excelWorksheet.Cells[row, col++].Value = campaignEmployeeStates[campaignEmployee.State];
                excelWorksheet.Cells[row, col++].Value = $"{campaignEmployee.StateSetDate:dd.MM.yyyy}";
                excelWorksheet.Cells[row, col++].Value = $"{campaignEmployee.ContractAccepted:dd.MM.yyyy}";
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.Remark;
                excelWorksheet.Cells[row, col++].Value = campaignEmployee.LocationsText;
                if (campaignEmployee.Employee.Profile != null)
                {
                    excelWorksheet.Cells[row, col++].Value = NullableBoolToString(campaignEmployee.Employee.Profile.SkeletonContract);
                    excelWorksheet.Cells[row, col++].Value = NullableBoolToString(campaignEmployee.Employee.Profile.FreelancerQuestionaire);
                    excelWorksheet.Cells[row, col++].Value = NullableBoolToString(campaignEmployee.Employee.Profile.CopyTradeLicense);
                    var shirtSizesStr = campaignEmployee.Employee.Profile.ShirtSize.HasValue
                        ? shirtSizes[campaignEmployee.Employee.Profile.ShirtSize.ToString()]
                        : "-";
                    excelWorksheet.Cells[row, col++].Value = shirtSizesStr;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.Height;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.DriversLicenseClass1 +
                                                             ";" +
                                                             campaignEmployee.Employee.Profile.DriversLicenseClass2;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.Skype;
                    excelWorksheet.Cells[row, col++].Value = campaignEmployee.Employee.Profile.Comment;
                }
                else
                {
                    col += 8;
                }

                foreach (var t in trainingDays)
                {
                    var trainingParticipant = _dbContext.TrainingParticipants.SingleOrDefault(e => e.TrainingDay.Id == t.Id && e.CampaignEmployee.EmployeeId == campaignEmployee.EmployeeId);
                    if (trainingParticipant != null)
                    {
                        excelWorksheet.Cells[row, col].Value = trainingStates[trainingParticipant.State];
                    }
                    col++;
                }
            }

            excelWorksheet.Cells.AutoFitColumns();
            return new Excel2007FileResult("Personalliste.xlsx", excelPackage);
        }


    }
}
