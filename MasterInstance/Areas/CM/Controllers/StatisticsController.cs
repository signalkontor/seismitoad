﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Glimpse.AspNet;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using Microsoft.Ajax.Utilities;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using SeismitoadShared.Models;
using Telerik.Web.Mvc.Extensions;

namespace MasterInstance.Areas.CM.Controllers
{
    public partial class StatisticsController : Controller
    {
        readonly private SeismitoadDbContext _dbContext;

        // GET: CM/Statistics
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int campaignId)
        {
            var model = new AdvantageDashboardViewModel();
            model.CampaignId = campaignId;

            var campaignLocations = _dbContext.CampaignsLocations.Where(e => e.CampaignId == campaignId);
            model.CampaignLocationCount = campaignLocations.Count();

            var campaignLocationAssignments = _dbContext.Assignments
                .Where(e => e.CampaignLocation.CampaignId == campaignId && e.State == AssignmentState.Planned)
                .GroupBy(e => e.CampaignLocation);

            model.UnassignedCampaignLocationCount =
                // Anzahl der Locations bei denen alle AT unterbesetzt sind
                campaignLocationAssignments.Count(e => e.All(i => i.AssignedEmployees.Count < i.EmployeeCount)) +
                // plus Anzahl der Locations die gar keine Aktionstage haben
                campaignLocations.Count(e => e.Assignments.All(i => i.State != AssignmentState.Planned));

            // Anzahl der Locations die mindestens einen AT haben der nicht vollständig besetzt ist und
            // mindestens einen AT haben der voll besetzt ist.
            model.PartiallyAssignedCampaignLocationCount = campaignLocationAssignments
                .Count(e => e.Any(i => i.AssignedEmployees.Count < i.EmployeeCount) && e.Any(i => i.AssignedEmployees.Count >= i.EmployeeCount));

            // Anzahl der Locations bei denen allte AT voll (oder über) besetzt sind.
            model.FullyAssignedCampaignLocationCount = campaignLocationAssignments
                .Count(e => e.All(i => i.AssignedEmployees.Count >= i.EmployeeCount));

            return View(model);
        }

        public StatisticsController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ActionResult _Read([DataSourceRequest] DataSourceRequest request, int campaignId, DateTime? from, DateTime? til)
        {
            var now = LocalDateTime.Now;
            if (til.HasValue)
                til = til.Value.AddDays(1);

            Expression<Func<Assignment, bool>> predicate = e => true;
            if (from.HasValue && til.HasValue)
            {
                predicate = e => from <= e.DateStart && e.DateStart < til;
            }
            else if (from.HasValue)
            {
                predicate = e => from <= e.DateStart;
            }
            else if (til.HasValue)
            {
                predicate = e => e.DateStart < til;
            }

            var campaignLocations = _dbContext.CampaignsLocations.Where(e => campaignId == e.CampaignId)
                .Select(cl => new StatisticsViewModel
                {
                    LocationName = cl.Location.Name,
                    LocationCity = cl.Location.City,
                    LocationStreet = cl.Location.Street,
                    LocationPostalCode = cl.Location.PostalCode,

                    ETdurchgeführt = cl.Assignments
                        .AsQueryable().Where(predicate)
                        .Count(e => e.DateStart <= now && e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance != Attendance.DidNotAttend)),
                    
                    ETgeplant = cl.Assignments
                        .AsQueryable().Where(predicate)
                        .Count(e => e.DateStart > now && e.State == AssignmentState.Planned),

                    ETausgefallen = cl.Assignments
                        .AsQueryable().Where(predicate)
                        .Count(e => e.State ==  AssignmentState.Cancelled || (e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance == Attendance.DidNotAttend))),

                    ETverschoben = cl.Assignments
                        .AsQueryable().Where(predicate)
                        .Count(e => e.State == AssignmentState.PostponedPromoter || e.State == AssignmentState.PostponedLocation),

                    ETgesamt= cl.Assignments
                            .AsQueryable().Where(predicate)
                            .Count(e => e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance != Attendance.DidNotAttend)),

                    Ausfallquote = (float)
                    cl.Assignments.AsQueryable().Where(predicate).Count(e => e.State == AssignmentState.Cancelled || (e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance == Attendance.DidNotAttend)))
                    /
                    (cl.Assignments.AsQueryable().Where(predicate).Count(e => e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance != Attendance.DidNotAttend)) != 0 ?
                    cl.Assignments.AsQueryable().Where(predicate).Count(e => e.State == AssignmentState.Planned && e.AssignedEmployees.Any(i => i.Attendance != Attendance.DidNotAttend)) : 1)
                });

            return Json(campaignLocations.ToDataSourceResult(request));
        }
    }
}