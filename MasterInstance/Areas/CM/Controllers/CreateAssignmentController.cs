﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MasterInstance.Areas.CM.Models;
using MasterInstance.Extensions;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Areas.CM.Controllers
{
    /*
     * Hier haben wir mal angefangen das Aktionstage Anlegen mit Kendo neu zu bauen. Wird aber so noch nicht benutzt. War aber auch zu schade zum wegschme
     */

    public partial class CreateAssignmentController : Controller
    {
        private readonly SeismitoadDbContext _dbContext;
        public CreateAssignmentController(SeismitoadDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: CM/CreateAssignment
        [MenuArea(MenuArea.Project)]
        public virtual ActionResult Index(int campaignId)
        {
            var classification = _dbContext.Campaigns
                .Where(e => e.Id == campaignId)
                .Select(e => e.Classification)
                .Single()
                ?.Replace("\n", " / ") ?? "Nicht hinterlegt.";

            var model = new CreateAssignmentModel
            {
                CampaignId = campaignId,
                Classification = classification,
                LocationGroups = new SelectList(_dbContext.LocationGroups.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
                LocationTypes = new SelectList(_dbContext.LocationTypes.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
                StateProvinces = new SelectList(_dbContext.StateProvinces.Select(e => new { e.Id, e.Name }).OrderBy(e => e.Name), "Id", "Name"),
            };

            return View(model);
        }

        public virtual ActionResult _Locations([DataSourceRequest] DataSourceRequest request, int campaignId,
            string addOrRemove, DateTime[] start, DateTime[] end, int[] amount, int? locationId)
        {
            var locations = _dbContext.CampaignsLocations
                .Where(e => e.CampaignId == campaignId)
                .Select(e => new CampaignLocationHelper { Location = e.Location, CampaignLocation = e });

            var model = locations.Project().To<CampaignLocationViewModel>();

            //if (addOrRemove != null)
            //    ApplyAction(addOrRemove, request, model, start, end, amount, locationId);

            return Json(model.ToDataSourceResult(request));
        }

        private void ApplyAction(string action, DataSourceRequest request, IQueryable<CampaignLocationViewModel> locations, int campaignId, DateTime[] start, DateTime[] end, int[] amount, int? locationId)
        {
            var originalPage = request.Page;
            var originalPageSize = request.PageSize;
            request.Page = 1;
            request.PageSize = int.MaxValue;

            var locationIds = locationId.HasValue
                ? new[] { locationId.Value }
                : (locations.ToDataSourceResult(request).Data as IEnumerable<CampaignLocationViewModel>).Select(e => e.LocationId).ToArray();

            request.Page = originalPage;
            request.PageSize = originalPageSize;

            var days = start.Select((o, i) => new
            {
                Start = o,
                End = end[i],
                Amount = amount[i]
            });

            var remove = action == "remove";
            if (remove)
            {

            }
            else
            {
            }
        }

    }
}