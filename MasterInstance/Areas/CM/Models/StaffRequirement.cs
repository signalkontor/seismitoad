﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using AutoMapper;
using Newtonsoft.Json;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class StaffRequirement
    {
        [Required] 
        public string Projektleitung { get; set; }
        public Guid AuftraggeberId { get; set; }
        public string Pausenzeiten { get; set; }
        public string Aktionsinhalt { get; set; }
        public string InternerKommentarZurAktion { get; set; }
        public string Marktliste { get; set; }

        [Required] public string AnforderungsprofilPersonal { get; set; }
        public string BeschreibungZuErbringendeLeistung { get; set; }
        public string Casting { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? PromoterAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? HostessAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? MerchandiserAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? TrainerAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? TeamleiterAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        public int? BezahlteSpringerAnzahl { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidateSonstigerPersonalbedarf")]
        public string SonstigerPersonalbedarf { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidatePersonalbedarf")]
        [CustomValidation(typeof(StaffRequirement), "ValidateSonstigerPersonalbedarf")]
        public int? SonstigerPersonalbedarfAnzahl { get; set; }
        [Required] public string Outfits { get; set; }
        public bool GewerbescheinSteuernummer { get; set; }
        public bool Infektionsschutzbelehrung { get; set; }
        public bool Führerschein { get; set; }
        public string[] FührerscheinKlassen { get; set; }
        public bool EigenerPKW { get; set; }
        public string AndereVorraussetzung { get; set; }

        public decimal HonorarMontagBisSamstag { get; set; }
        public decimal HonorarSonntagsUndFeiertags { get; set; }
        public decimal? HonorarBezahlteSpringer { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? HonorarBeiLängererAktionszeit { get; set; }
        public string Provision { get; set; }
        public string Provisionsliste { get; set; }
        public string Durchhaltebonus { get; set; }
        public string Teamleiterbonus { get; set; }
        public string Fahrerbonus { get; set; }
        public string SonstigeVereinbarung { get; set; }

        public bool Fahrtkosten { get; set; }
        public string FahrtkostenSonstiges { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidateSchulung")]
        public bool SchulungFaceToFace { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidateSchulung")]
        public bool SchulungOnline { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidateSchulung")]
        public bool SchulungKeine { get; set; }
        public string SchulungsDatumOrt { get; set; }
        public string SchulungsKommentar { get; set; }
        public string Schulungshotel { get; set; }
        [CustomValidation(typeof(StaffRequirement), "ValidateSchulungshotelAb")]
        public int? SchulungshotelAb { get; set; }
        public decimal? SchulungVergütungFaceToFace { get; set; }
        public decimal? SchulungVergütungOnline { get; set; }
        public string BemerkungAdvantage { get; set; }

        public static ValidationResult ValidatePersonalbedarf(int? dummy, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance as StaffRequirement;
            Debug.Assert(model != null, "model != null");
            if (model.PromoterAnzahl.HasValue ||
                model.HostessAnzahl.HasValue ||
                model.MerchandiserAnzahl.HasValue ||
                model.TrainerAnzahl.HasValue ||
                model.TeamleiterAnzahl.HasValue ||
                model.BezahlteSpringerAnzahl.HasValue ||
                model.SonstigerPersonalbedarfAnzahl.HasValue)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("Bitte mindestens ein Feld ausfüllen.");
        }

        public static ValidationResult ValidateSonstigerPersonalbedarf(object dummy, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance as StaffRequirement;
            Debug.Assert(model != null, "model != null");
            if (!string.IsNullOrWhiteSpace(model.SonstigerPersonalbedarf) && !model.SonstigerPersonalbedarfAnzahl.HasValue)
                return new ValidationResult("Bitte eine Anzahl eingeben.");
            if (string.IsNullOrWhiteSpace(model.SonstigerPersonalbedarf) && model.SonstigerPersonalbedarfAnzahl.HasValue)
                return new ValidationResult("Bitte Art des Personalbedarfs eingeben.");
            return ValidationResult.Success;
        }

        public static ValidationResult ValidateSchulung(bool dummy, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance as StaffRequirement;
            Debug.Assert(model != null, "model != null");
            if (model.SchulungFaceToFace|| model.SchulungOnline || model.SchulungKeine)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("Bitte mindestens ein Feld ausfüllen.");
        }

        public static ValidationResult ValidateSchulungshotelAb(int? dummy, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance as StaffRequirement;
            Debug.Assert(model != null, "model != null");
            if (model.Schulungshotel == "ja, ab dem" && !model.SchulungshotelAb.HasValue)
                return new ValidationResult("Bitte Entfernung eingeben.");
            if (model.Schulungshotel != "ja, ab dem" && model.SchulungshotelAb.HasValue)
                return new ValidationResult("Eingabe der Entfernung ist nur erlaubt, wennn 'ja, ab dem' gewählt wurde");
            return ValidationResult.Success;
        }
    }

    public class StaffRequirementViewModel : StaffRequirement
    {
        public bool IsUpdate { get; set; }
        public int CampaignId { get; set; }
        public string Aktionstitel { get; set; }
        public string Projektnummer { get; set; }
        public string Aktionszeitraum { get; set; }
        public string Aktionszeiten { get; set; }
        public string Aktionstage { get; set; }
        public int AktionsorteAnzahl { get; set; }
        public int Teamgröße { get; set; }
    }

    public class StaffRequirementAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Campaign, StaffRequirementViewModel>()
                .ForMember(e => e.CampaignId, opt => opt.MapFrom(e => e.Id))
                .ForMember(e => e.AuftraggeberId, opt => opt.MapFrom(e => e.ManagerId))
                .ForMember(e => e.Aktionstitel, opt => opt.MapFrom(e => e.Name))
                .ForMember(e => e.Projektnummer, opt => opt.MapFrom(e => e.Customer.Number + e.Number))
                .ForMember(e => e.Aktionszeitraum, opt => opt.MapFrom(e => e.Duration))
                .ForMember(e => e.Aktionszeiten, opt => opt.MapFrom(e => e.Times))
                .ForMember(e => e.Aktionstage, opt => opt.MapFrom(e => e.Days))
                .ForMember(e => e.AktionsorteAnzahl, opt => opt.MapFrom(e => e.CampaignLocations.Count))
                .ForMember(e => e.Teamgröße, opt => opt.MapFrom(e => e.EmployeesPerAssignment))
                .ForMember(e => e.Aktionsinhalt, opt => opt.MapFrom(e => e.Description))
                .ForMember(e => e.AnforderungsprofilPersonal, opt => opt.MapFrom(e => e.Requirements))
                ;

            Mapper.CreateMap<StaffRequirement, StaffRequirementViewModel>();
        }
    }
}
