﻿using AutoMapper;
using SeismitoadModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class CampaignLocationViewModel
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Remark { get; set; }
        public int? LocationTypeId { get; set; }
        public int? StateProvinceId { get; set; }
        public int? LocationGroupId { get; set; }
        public string CenterManagementStreet { get; set; }
        public string CenterManagementPostalCode { get; set; }
        public string CenterManagementCity { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int? VisitorFrequency { get; set; }
        public int? Area { get; set; }
        public int? SellingArea { get; set; }
        public string CampaignAreaPrices { get; set; }
        public int? Audience { get; set; }
        public int? Opening { get; set; }
        public int? CatchmentArea { get; set; }
        public int? Parking { get; set; }
        public bool? OutdoorArea { get; set; }
        public string Retailers { get; set; }
        public string ConferenceRoomSize { get; set; }
        public bool? CostTransferPossible { get; set; }
        public string RoomRates { get; set; }
        public string ConferenceEquipmentCost { get; set; }
        public string ConferencePackageCost { get; set; }
        public CampaignLocationState? State { get; set; }
        public int? ContactId { get; set; }
        public string SalesRegion { get; set; }
        public string Classification { get; set; }
        public string RemarkCampaign { get; set; }
        public bool? WiFiAvailable { get; set; }
        public bool? WiFiAvailableLocation { get; set; }
        public bool? WiFiAvailableCampaign { get; set; }
        public bool IsCampaignLocation { get; set; }
        public bool Deleteable { get; set; }
    }

    public class CampaignLocationHelper
    {
        public Location Location { get; set; }
        public CampaignLocation CampaignLocation { get; set; }
    }

    public class CampaignLocationViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignLocationHelper, CampaignLocationViewModel>()
                .ForMember(e => e.LocationId, opt => opt.MapFrom(e => e.Location.Id))
                .ForMember(e => e.Name, opt => opt.MapFrom(e => e.Location.Name))
                .ForMember(e => e.Name2, opt => opt.MapFrom(e => e.Location.Name2))
                .ForMember(e => e.Street, opt => opt.MapFrom(e => e.Location.Street))
                .ForMember(e => e.Street2, opt => opt.MapFrom(e => e.Location.Street2))
                .ForMember(e => e.PostalCode, opt => opt.MapFrom(e => e.Location.PostalCode))
                .ForMember(e => e.City, opt => opt.MapFrom(e => e.Location.City))
                .ForMember(e => e.Remark, opt => opt.MapFrom(e => e.Location.Notes))
                .ForMember(e => e.LocationTypeId, opt => opt.MapFrom(e => e.Location.LocationTypeId))
                .ForMember(e => e.LocationGroupId, opt => opt.MapFrom(e => e.Location.LocationGroupId))
                .ForMember(e => e.StateProvinceId, opt => opt.MapFrom(e => e.Location.StateProvinceId))
                .ForMember(e => e.CenterManagementStreet, opt => opt.MapFrom(e => e.Location.CenterManagementStreet))
                .ForMember(e => e.CenterManagementPostalCode, opt => opt.MapFrom(e => e.Location.CenterManagementPostalCode))
                .ForMember(e => e.CenterManagementCity, opt => opt.MapFrom(e => e.Location.CenterManagementCity))
                .ForMember(e => e.Phone, opt => opt.MapFrom(e => e.Location.Phone))
                .ForMember(e => e.Fax, opt => opt.MapFrom(e => e.Location.Fax))
                .ForMember(e => e.Email, opt => opt.MapFrom(e => e.Location.Email))
                .ForMember(e => e.Website, opt => opt.MapFrom(e => e.Location.Website))
                .ForMember(e => e.VisitorFrequency, opt => opt.MapFrom(e => e.Location.VisitorFrequency))
                .ForMember(e => e.Area, opt => opt.MapFrom(e => e.Location.Area))
                .ForMember(e => e.SellingArea, opt => opt.MapFrom(e => e.Location.SellingArea))
                .ForMember(e => e.CampaignAreaPrices, opt => opt.MapFrom(e => e.Location.CampaignAreaPrices))
                .ForMember(e => e.Audience, opt => opt.MapFrom(e => e.Location.Audience))
                .ForMember(e => e.Opening, opt => opt.MapFrom(e => e.Location.Opening))
                .ForMember(e => e.CatchmentArea, opt => opt.MapFrom(e => e.Location.CatchmentArea))
                .ForMember(e => e.Parking, opt => opt.MapFrom(e => e.Location.Parking))
                .ForMember(e => e.OutdoorArea, opt => opt.MapFrom(e => e.Location.OutdoorArea))
                .ForMember(e => e.Retailers, opt => opt.MapFrom(e => e.Location.Retailers))
                .ForMember(e => e.ConferenceRoomSize, opt => opt.MapFrom(e => e.Location.ConferenceRoomSize))
                .ForMember(e => e.CostTransferPossible, opt => opt.MapFrom(e => e.Location.CostTransferPossible))
                .ForMember(e => e.RoomRates, opt => opt.MapFrom(e => e.Location.RoomRates))
                .ForMember(e => e.ConferenceEquipmentCost, opt => opt.MapFrom(e => e.Location.ConferenceEquipmentCost))
                .ForMember(e => e.ConferencePackageCost, opt => opt.MapFrom(e => e.Location.ConferencePackageCost))
                .ForMember(e => e.State, opt => opt.MapFrom(e => e.CampaignLocation.State))
                .ForMember(e => e.ContactId, opt => opt.MapFrom(e => e.CampaignLocation.ContactId))
                .ForMember(e => e.SalesRegion, opt => opt.MapFrom(e => e.CampaignLocation.SalesRegion))
                .ForMember(e => e.Classification, opt => opt.MapFrom(e => e.CampaignLocation.Classification))
                .ForMember(e => e.RemarkCampaign, opt => opt.MapFrom(e => e.CampaignLocation.Notes))
                .ForMember(e => e.WiFiAvailable, opt => opt.MapFrom(e => e.CampaignLocation.NetworkConnectionAvailable ?? e.Location.WiFiAvailable))
                .ForMember(e => e.WiFiAvailableLocation, opt => opt.MapFrom(e => e.Location.WiFiAvailable))
                .ForMember(e => e.WiFiAvailableCampaign, opt => opt.MapFrom(e => e.CampaignLocation.NetworkConnectionAvailable))
                .ForMember(e => e.IsCampaignLocation, opt => opt.MapFrom(e => e.CampaignLocation != null))
                .ForMember(e => e.Deleteable, opt => opt.MapFrom(e => e.CampaignLocation.Assignments.All(i => i.State == AssignmentState.Deleted)))
            ;
        }
    }
}