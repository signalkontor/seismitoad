namespace MasterInstance.Areas.CM.Models
{
    public class AssignmentsByMonth
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Total { get; set; }
        public int Unassigned { get; set; }
    }
}