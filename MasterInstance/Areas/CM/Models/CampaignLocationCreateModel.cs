﻿using System.Web.Mvc;
using System.Web.Routing;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using Telerik.Web.Mvc;

namespace MasterInstance.Areas.CM.Models
{
    public class CampaignLocationCreateModel : CreateModelWithTemplate<CampaignLocation>, IPropertySelectorModel
    {
        public Campaign Campaign { get; set; }
        public GridModel GridModel { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
        public SelectListItem[] Properties { get; set; }
        public string[] VisibleProperties { get; set; }

        public string ControllerName
        {
            get { return MVC.CM.CampaignLocation.Name; }
        }
    }
}