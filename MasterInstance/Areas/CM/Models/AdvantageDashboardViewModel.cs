﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class AdvantageDashboardViewModel
    {
        public int CampaignId { get; set; }
        public int CampaignLocationCount { get; set; }
        public int UnassignedCampaignLocationCount { get; set; }
        public int PartiallyAssignedCampaignLocationCount { get; set; }
        public int FullyAssignedCampaignLocationCount { get; set; }

        public DateTime? StaffRequirementLastChanged { get; set; }
        public int Candidates { get; set; }
        public int NewCandidates { get; set; }
        public DateTime NewCandidatesSince { get; set; }
        public int TeamSize { get; set; }
        public int EmployeesWithoutAssignments { get; set; }

        public int UnsentContracts { get; set; }
        public int UnconfirmedContracts { get; set; }
        public int UnsentAssignments { get; set; }
        public int UnconfirmedAssignments { get; set; }
        public int RejectedAssignments { get; set; }
    }
}