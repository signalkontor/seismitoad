﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AutoMapper;
using Glimpse.Core.ClientScript;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class AcquisitionRowViewModel
    {
        public int RowNumber { get; set; }
        public int EmployeeId { get; set; }
        public string Roles { get; set; }
        public string[] RolesEdit { get; set; }
        public bool Highlighted { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime? Date { get; set; }
        public EmployeeState EmployeeState { get; set; }
        public string State { get; set; }
        public string RealState { get; set; }
        public string Remark { get; set; }
        public bool AddressChanged { get; set; }
        public string Deployable { get; set; }
        public string AppliedForLocations { get; set; }
        public bool AppliedForLocationsBySelection { get; set; }
        public bool? SkeletonContract { get; set; }
        public int? CopyIdentityCard { get; set; }
        public bool? FreelancerQuestionaire { get; set; }
        // Auch wenn das Feld hier ValidTradeLicense heißt, es soll hier das Feld CopyTradeLicense ausgegeben werden.
        // (http://redmine.advantage-promotion.de/redmine/issues/394)
        public bool? ValidTradeLicense { get; set; }
        public int? ShirtSize { get; set; }
        public int? Height { get; set; }
        public string DriversLicense { get; set; }
        public string Skype { get; set; }
        public string ProfileRemark { get; set; }
        public DateTime? ContractAccepted { get; set; }
        public string Training0 { get; set; }
        public string Training1 { get; set; }
        public string Training2 { get; set; }
        public string Training3 { get; set; }
        public string Training4 { get; set; }
        public string Training5 { get; set; }
        public string Training6 { get; set; }
        public string Training7 { get; set; }
        public string Training8 { get; set; }
        public string Training9 { get; set; }
        public string Training10 { get; set; }
        public string Training11 { get; set; }
        public string Training12 { get; set; }
        public string Training13 { get; set; }
        public string Training14 { get; set; }
        public DateTime? TrainingDate0 { get; set; }
        public DateTime? TrainingDate1 { get; set; }
        public DateTime? TrainingDate2 { get; set; }
        public DateTime? TrainingDate3 { get; set; }
        public DateTime? TrainingDate4 { get; set; }
        public DateTime? TrainingDate5 { get; set; }
        public DateTime? TrainingDate6 { get; set; }
        public DateTime? TrainingDate7 { get; set; }
        public DateTime? TrainingDate8 { get; set; }
        public DateTime? TrainingDate9 { get; set; }
        public DateTime? TrainingDate10 { get; set; }
        public DateTime? TrainingDate11 { get; set; }
        public DateTime? TrainingDate12 { get; set; }
        public DateTime? TrainingDate13 { get; set; }
        public DateTime? TrainingDate14 { get; set; }
    }
}