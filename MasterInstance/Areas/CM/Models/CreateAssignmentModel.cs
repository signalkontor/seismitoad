﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MasterInstance.Areas.CM.Models
{
    public class CreateAssignmentModel
    {
        public int CampaignId { get; set; }
        public string Classification { get; set; }
        public SelectList LocationTypes { get; set; }
        public SelectList LocationGroups { get; set; }
        public SelectList StateProvinces { get; set; }
    }
}