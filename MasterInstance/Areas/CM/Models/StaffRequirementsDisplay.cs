﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class StaffRequirementsDisplay
    {
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string[] StaffRequirements { get; set; }
    }
}