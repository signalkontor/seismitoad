﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class AssignedEmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public string Label { get; set; }
        public string Classes { get; set; }
    }

    public class AssignedEmployeeViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ViewAssignment, AssignedEmployeeViewModel>()
                .ForMember(e => e.Classes, opt => opt.ResolveUsing(e => e.Attendance.HasValue ? string.Format("attendance-{0} {1}", (int)e.Attendance, e.Color) : ""))
                .ForMember(e => e.Label, opt => opt.ResolveUsing(e => string.Format("{0} {1} ({2})", e.EmployeeFirstname, e.EmployeeLastname, e.RoleShortName)));

        }
    }

    public class AssignmentViewModel
    {       
        public AssignmentViewModel(IGrouping<int, ViewAssignment> viewAssignments)
        {
            var first = true;
            string assignmentColor = null;
            string teamClass = null;
            string fullyAssignedClass = null;
            foreach (var assignment in viewAssignments)
            {
                if (first)
                {
                    first = false;
                    AssignmentId = assignment.Id;
                    Notes = assignment.Notes;
                    Marker = assignment.Marker;
                    Title = string.Format("{0:t}-{1:t}", assignment.DateStart, assignment.DateEnd);
                    if (assignment.PredecessorDateStart.HasValue)
                    {
                        Title += string.Format(" | Ersatztermin für {0:dd.MM.yyyy}", assignment.PredecessorDateStart);
                    }
                    if (assignment.SuccessorDateStart.HasValue)
                    {
                        Title += string.Format(" | Verschoben ({1}) auf {0:dd.MM.yyyy}", assignment.SuccessorDateStart, assignment.State == AssignmentState.PostponedLocation ? "Marktwusch" : "Promoterausfall");
                    }

                    /* http://redmine.advantage-promotion.de/redmine/issues/328
                    Bei den Farben in der Ansicht Aktionstage zuweisen ist noch etwas durcheinander.
                    folgende Regeln gelten für die Farbwahl:


                    Aktionstag mit einem Promoter:

                    Die Schriftfarbe (Promotername) ist immer schwarz.
                    Die Rahmenfarbe ist abhängig vom Status (zugewiesen - blau, benachrichtigt - pink, bestätigt - grün, abgesagt - rot)
                    Könnt ihr für den Fall, dass der Promoter einen Einsatztag ablehnt den Promoternamen in Fett darstellen?


                    Aktionstag mit zwei oder mehr Promotern

                    Die Schriftfarbe (Promotername) gibt den Status an (zugewiesen - blau, benachrichtigt - pink, bestätigt - grün, abgesagt - rot)
                    Die Rahmenfarbe wird immer so gesetzt wie der kleinste Status. (z.B. ein Promoter hat bestätigt und der andere noch nicht: Rahmenfarbe ist pink; wenn beide bestätigt haben wird der Rahmen grün).
                    Als Ausnahme gilt hier, wenn ein Promoter einen Tag ablehnt. Dann würde der Rahmen rot und der Promotername fett.

                    Ergänzung: http://redmine.advantage-promotion.de/redmine/issues/438

                    Wir haben nun in einem Projekt den Fall, dass es an einem Tag nur einen Promoter gibt und an den darauffolgenden ein Team aus 6 Leuten... Jetzt sieht es etwas komisch aus, dass der einzelne Name "schwarz" ist und beim Team alle "grün" (siehe Bild).
                    Wie aufwendig wäre es, dass wenn alle aus dem Team bestätigt haben, die Name "schwarz" angezeigt werden würden und nur der Rahmen grün..?
                    
                    Der Code hierzu ist && (viewAssignments.Any(e => e.Color != "green") || fullyAssignedClass == "")
                    Erklärung: Wenn einer der Promoter nicht grün ist, dann müssen wir die Farben pro Promoter anzeigen. Es kann auch sein, dass alle zugesagten Promoter grün sind, aber der Einsatz nicht voll besetzt ist. Dann greift der rechte Teil vom oder.
                    */

                    fullyAssignedClass = assignment.EmployeeCount <= assignment.AssignedEmployeeCount ? " fully-assigned " : "";
                    teamClass = assignment.EmployeeCount > 1 && (viewAssignments.Any(e => e.Color != "green") || fullyAssignedClass == "") ? "team " : ""; // Hilfsklasse für Teams, da wir nur so vernünftige CSS Selektoren bauen können

                    if (assignment.State == AssignmentState.PostponedLocation ||
                        assignment.State == AssignmentState.PostponedPromoter)
                    {
                        teamClass = ""; // bei verschobenen tagen ist der teamstatus irrelevant
                        assignmentColor = "purple";
                        break; // Hier müssen wir nicht weitermachen
                    }

                    if (assignment.State == AssignmentState.Deleted)
                    {
                        assignmentColor = "red";
                        break; // Hier müssen wir nicht weitermachen
                    }

                    if (assignment.AssignedEmployeeCount < assignment.EmployeeCount)
                    {
                        assignmentColor = "yellow";
                    }
                }
                UpdateColor(ref assignmentColor, assignment.Color);
            }
            Employees = viewAssignments
                .Where(e => e.State == AssignmentState.Planned && e.EmployeeId != -1) // Ist die Id nicht -1 ist es ein unbesetzter Tag
                .Select(Mapper.Map<ViewAssignment, AssignedEmployeeViewModel>)
                .ToArray();
            TrafficLight = teamClass + fullyAssignedClass + assignmentColor;
            NotifiedCount = Employees.Count(e => e.Classes.Contains("green") || e.Classes.Contains("pink"));
        }

        private static void UpdateColor(ref string color, string newColor)
        {
            switch (color)
            {
                case null:
                    color = newColor;
                    return;
                case "red":
                    // Wenn einer der Promoter abgesagt hat, dannn das gesamte Assigment rot machen
                    return;
                case "yellow":
                    // Gelb heißt immer nicht voll besetzt und wird daher auch nie ersetzt
                    return;
                case "green":
                    // Grün bleibt nur bestehen solange alle Promoter grün sind
                    if (newColor != "green")
                        color = newColor;
                    return;
                case "pink":
                    // Pink kann nur durch rot ersetzt werden
                    if (newColor == "red")
                        color = newColor;
                    return;
                case "blue":
                    // Blau kann durch rot und pink ersetzt werden
                    if (newColor == "red" || newColor == "pink")
                        color = newColor;
                return;
            }
        }

        public int AssignmentId { get; set; }
        public AssignedEmployeeViewModel[] Employees { get; set; }
        public string TrafficLight { get; set; }
        public string Title { get; set; }
        public int NotifiedCount { get; set; }
        public string Notes { get; set; }
        public int Marker { get; set; }
    }

    public class IntermediateLocationRowViewModel
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public IEnumerable<Assignment> Monday { get; set; }
        public IEnumerable<Assignment> Tuesday { get; set; }
        public IEnumerable<Assignment> Wednesday { get; set; }
        public IEnumerable<Assignment> Thursday { get; set; }
        public IEnumerable<Assignment> Friday { get; set; }
        public IEnumerable<Assignment> Saturday { get; set; }
        public IEnumerable<Assignment> Sunday { get; set; }
    }

    public class IntermediateLocationRowViewModelNew
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public IEnumerable<ViewAssignment> Assignments { get; set; }
    }


    public class LocationRowViewModel
    {
        public int LocationId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Straße")]
        public string Street { get; set; }

        [Display(Name = "PLZ")]
        public string PostalCode { get; set; }

        [Display(Name = "Ort")]
        public string City { get; set; }

        [Display(Name = "Montag")]
        public bool Monday { get { return true; } }
        public IEnumerable<AssignmentViewModel> MondayItems { get; set; }

        [Display(Name = "Dienstag")]
        public bool Tuesday { get { return true; } }
        public IEnumerable<AssignmentViewModel> TuesdayItems { get; set; }

        [Display(Name = "Mittwoch")]
        public bool Wednesday { get { return true; } }
        public IEnumerable<AssignmentViewModel> WednesdayItems { get; set; }

        [Display(Name = "Donnerstag")]
        public bool Thursday { get { return true; } }
        public IEnumerable<AssignmentViewModel> ThursdayItems { get; set; }

        [Display(Name = "Freitag")]
        public bool Friday { get { return true; } }
        public IEnumerable<AssignmentViewModel> FridayItems { get; set; }

        [Display(Name = "Samstag")]
        public bool Saturday { get { return true; } }
        public IEnumerable<AssignmentViewModel> SaturdayItems { get; set; }

        [Display(Name = "Sonntag")]
        public bool Sunday { get { return true; } }
        public IEnumerable<AssignmentViewModel> SundayItems { get; set; }
    }
}