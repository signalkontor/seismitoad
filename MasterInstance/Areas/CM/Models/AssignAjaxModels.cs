﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MasterInstance.Models.ViewModels;

namespace MasterInstance.Areas.CM.Models
{
    public abstract class AssignAjaxPostBase
    {
        public int AssignmentId { get; set; }
        public int EmployeeId { get; set; }
        public bool Confirmed { get; set; }
        public bool Notify { get; set; }
        public AssignmentRepeatType Repeat { get; set; }
        public int[] AdditionalAssignmentIds { get; set; }
    }

    public class AssignRemoveEmployeeModel : AssignAjaxPostBase
    {
        public Guid? DataId { get; set; }
    }

    public class AssignUpdateEmployeeModel : AssignAjaxPostBase
    {
        // Beim Update wird die bisherige Id als previousEmployeeId gesendet.
        // Aus diversen Gründen wollen wir hier aber EmployeeId verwenden.
        public int PreviousEmployeeId { get { return EmployeeId; } set { EmployeeId = value; } }
        public int NewEmployeeId { get; set; }
        public int[] NewRoleIds { get; set; }

        public bool IsNewAssignment { get { return PreviousEmployeeId <= 0; } }
        public bool IsReplacement { get { return !IsNewAssignment && PreviousEmployeeId != NewEmployeeId; } }
    }

    // Sollte immer zusammen mit dem TypeScript-Interface IAjaxActionResultModel geändert werden
    public class AjaxActionResultModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public int employeeId { get; set; }
        public int? assignmentId { get; set; }
        public string employee { get; set; }
        public string location { get; set; }
        public Guid? dataId { get; set; }
        public List<AssignmentConflict> conflicts { get; set; }
    }

    public class AssignmentConflict
    {
        public Guid? dataId { get; set; }
        public int assignmentId { get; set; }
        public string date { get; set; }
        public string campaigns { get; set; }
        public string cities { get; set; }
    }
}