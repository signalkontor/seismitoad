﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasterInstance.Areas.CM.Models
{
    public class ExportAssignmentModel : IValidatableObject
    {
        public int CampaignId { get; set; }

        [Display(Name = "Ab Datum (optional)")]
        public DateTime? Start { get; set; }
        
        [Display(Name = "Bis Datum (optional)")]
        public DateTime? End { get; set; }

        [Display(Name = "Auswahl Tage")]
        public AssignmentFilterExport[] AssignmentFilterExport { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (End < Start)
            {
                yield return new ValidationResult("Startdatum muss vor dem Enddatum liegen!",new []{"Start","End"});
            }
        }
    }

    public enum AssignmentFilterExport
    {
        [Display(Name = "Alle")]
        All,
        [Display(Name = "Nur markierte")]
        Marked,
        [Display(Name = "Alle bestätigten (grün)")]
        Confimed,
        [Display(Name = "Alle unbestätigten (pink)")]
        Unconfimed,
        [Display(Name = "Alle abgelehnten (rot)")]
        Rejected,
        [Display(Name = "Alle offenen (gelb)")]
        Open,
        [Display(Name = "Alle nur zugewiesenen (blau)")]
        Assigned,
        [Display(Name = "Alle gelöschten (dunkelrot)")]
        Deleted,
        [Display(Name = "Alle verschobenen (lila)")]
        Moved,
    }
}