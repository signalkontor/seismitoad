﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class CampaignEmployeeViewModel
    {
        public string View { get; set; }
        public IEnumerable<string> TrainingColumnTitles { get; set; }
    }
}