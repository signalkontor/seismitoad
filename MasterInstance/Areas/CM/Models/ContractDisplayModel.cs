﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class ContractDisplayModel
    {
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}