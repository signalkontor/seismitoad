﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class TrainingFilterModel
    {
        public int[] CampaignIds { get; set; }
    }

    public class AttendanceFilterViewModel
    {
        public int[] CampaignIds { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTil { get; set; }
    }

    public class FaxMessagesFilterViewModel
    {
        public int DateFilter { get; set; }
    }

    public class AssignFilterModel
    {
        public int CampaignId { get; set; }
        public DateTime Start { get; set; }
        public AssignmentFilter[] F { get; set; }
        public int[] L { get; set; }
    }

    public enum AssignmentFilter
    {
        [Display(Name = "Nur markierte anzeigen")]
        Marked,
        [Display(Name = "Alle bestätigten")]
        Confimed,
        [Display(Name = "Alle unbestätigten")]
        Unconfimed,
        [Display(Name = "Alle abgelehnten")]
        Rejected,
        [Display(Name = "Alle offenen")]
        Open,
        [Display(Name = "Nur zugewiesen")]
        Assigned,
    }
}