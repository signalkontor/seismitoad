﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class Contract
    {
        public string Aktionstitel { get; set; }
        [AllowHtml]
        public string Projektleitung { get; set; }
        [AllowHtml]
        public string Projektnummer { get; set; }
        [AllowHtml]
        public string Aktionszeitraum { get; set; }
        [AllowHtml]
        public string Aktionszeiten { get; set; }
        [AllowHtml]
        public string Pausenzeiten { get; set; }
        [AllowHtml]
        public string Aktionsinhalt { get; set; }
        public string BeschreibungZuErbringendeLeistung { get; set; }
        [AllowHtml]
        public string Voraussetzungen { get; set; }
        [AllowHtml]
        public string HonorarMontagBisSamstag { get; set; }
        [AllowHtml]
        public string HonorarSonntagsUndFeiertags { get; set; }
        [AllowHtml]
        public string HonorarBezahlteSpringer { get; set; }
        [AllowHtml]
        public string Bonus { get; set; }
        [AllowHtml]
        public string HonorarBeiLängererAktionszeit { get; set; }
        [AllowHtml]
        public string Durchhaltebonus { get; set; }
        [AllowHtml]
        public string Teamleiterbonus { get; set; }
        [AllowHtml]
        public string Fahrerbonus { get; set; }
        [AllowHtml]
        public string SonstigeVereinbarung { get; set; }
        
        [AllowHtml]
        public string Fahrtkosten { get; set; }
        [AllowHtml]
        public string Provision { get; set; }
        [AllowHtml]
        public string Schulungsvergütung { get; set; }
        [AllowHtml]
        public string BemerkungAdvantage { get; set; }

        #region Toggles for optionally shown fields
        public bool HonorarBezahlteSpringerVisible { get; set; }
        public bool BonusVisible { get; set; }
        public bool HonorarBeiLängererAktionszeitVisible { get; set; }
        public bool BeschreibungZuErbringendeLeistungVisible { get; set; }
        public bool DurchhaltebonusVisible { get; set; }
        public bool TeamleiterbonusVisible { get; set; }
        public bool FahrerbonusVisible { get; set; }
        public bool SonstigeVereinbarungVisible { get; set; }
        public bool FahrtkostenVisible { get; set; }
        public bool ProvisionVisible { get; set; }
        public bool SchulungVisible { get; set; }
        public bool SonstigesVisible { get; set; }
        #endregion
    }

    public class ContractAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<StaffRequirement, Contract>()
                .ForMember(e => e.Voraussetzungen, opt => opt.ResolveUsing(e =>
                {
                    #region Voraussetzungen
                    var sb = new StringBuilder();
                    if (e.GewerbescheinSteuernummer)
                    {
                        sb.AppendLine("- Gewerbeschein / Steuernummer");
                    }
                    if (e.Infektionsschutzbelehrung)
                    {
                        sb.AppendLine("- Infektionsschutzbelehrung");
                    }
                    if (e.Führerschein)
                    {
                        sb.AppendLine("- Führerschein");   
                    }
                    if (e.FührerscheinKlassen != null && e.FührerscheinKlassen.Length > 0)
                    {
                        sb.AppendFormat("- Klasse{0} {1}\n", e.FührerscheinKlassen.Length > 1 ? "n" : "", string.Join(", ", e.FührerscheinKlassen));
                    }
                    if (e.EigenerPKW)
                    {
                        sb.AppendLine("- eigener PKW");   
                    }
                    if (!string.IsNullOrWhiteSpace(e.AndereVorraussetzung))
                    {
                        sb.AppendLine("- "+ e.AndereVorraussetzung);
                    }
                    return sb.ToString();
                    #endregion
                }))
                .ForMember(e => e.HonorarMontagBisSamstag, opt => opt.ResolveUsing(e => string.Format("{0:#,##0.00} € / Aktionstag", e.HonorarMontagBisSamstag)))
                .ForMember(e => e.HonorarSonntagsUndFeiertags, opt => opt.ResolveUsing(e => string.Format("{0:#,##0.00} € / Aktionstag", e.HonorarSonntagsUndFeiertags)))
                .ForMember(e => e.HonorarBezahlteSpringer, opt => opt.ResolveUsing(e => string.Format("{0:#,##0.00} € / Aktionstag", e.HonorarBezahlteSpringer)))
                .ForMember(e => e.Bonus, opt => opt.ResolveUsing(e => string.Format("{0:#,##0.00} € / Aktionstag", e.Bonus)))
                .ForMember(e => e.HonorarBeiLängererAktionszeit, opt => opt.ResolveUsing(e => string.Format("{0:#,##0.00} € / Stunde", e.HonorarBeiLängererAktionszeit)))
                .ForMember(e => e.Fahrtkosten, opt => opt.MapFrom(e => (e.Fahrtkosten ? "0,30 €/km ab dem 26. Kilometer pro Strecke (Hin- und Rückweg) " : " ") + e.FahrtkostenSonstiges))
                .ForMember(e => e.Schulungsvergütung, opt => opt.ResolveUsing(e =>
                {
                    #region Schulungsvergütung
                    var sb = new StringBuilder("Schulungsvergütung:\n");
                    if (e.SchulungVergütungFaceToFace.HasValue)
                    {
                        sb.AppendFormat("- Face-to-face Schulung: {0:#,##0.00} € / Tag\n", e.SchulungVergütungFaceToFace);
                    }
                    if (e.SchulungVergütungFaceToFace.HasValue)
                    {
                        sb.AppendFormat("- Online Schulung: {0:#,##0.00} € / Tag\n", e.SchulungVergütungOnline);
                    }
                    return sb.ToString();
                    #endregion
                }))
                ;

            Mapper.CreateMap<Campaign, Contract>()
                .ForMember(e => e.Aktionstitel, opt => opt.MapFrom(e => e.Name))
                .ForMember(e => e.Projektnummer, opt => opt.MapFrom(e => e.Customer.Number + e.Number))
                .ForMember(e => e.Aktionszeitraum, opt => opt.MapFrom(e => e.Duration))
                .ForMember(e => e.Aktionszeiten, opt => opt.MapFrom(e => e.Times));
        }
    }

    public class ContractListItem
    {
        public DateTime LastChange { get; set; }
        [Required, RegularExpression("[a-zA-Z0-9_\\-, \\.äÄöÖüÜß]*",
            ErrorMessage = "Der Name darf nur aus Buchstaben, Zahlen, Leerzeichen, Punkten, Kommas, Binde- und Unterstrichen bestehen.")]
        public string Name { get; set; }
    }
}