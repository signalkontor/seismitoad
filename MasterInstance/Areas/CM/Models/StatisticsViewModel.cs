using System;
using System.Collections.Generic;
using AutoMapper;
using MasterInstance.Areas.CM.Models;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class StatisticsViewModel : ITrackChanges
    {
        public string LocationName { get; set; }
        public string LocationCity { get; set; }
        public string LocationStreet { get; set; }
        public string LocationPostalCode { get; set; }

        public int ETausgefallen { get; set; }
        public int ETverschoben { get; set; }
        public int ETdurchgeführt { get; set; }
        public int ETgeplant { get; set; }
        public int ETgesamt { get; set; }
        public float Ausfallquote { get; set; }
    }
}