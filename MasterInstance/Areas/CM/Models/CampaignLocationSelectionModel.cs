﻿using System.Web.Mvc;
using System.Web.Routing;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using Telerik.Web.Mvc;

namespace MasterInstance.Areas.CM.Models
{
    public class CampaignLocationSelectionModel
    {
        public SelectList LocationTypes { get; set; }
        public SelectList LocationGroups { get; set; }
        public SelectList StateProvinces { get; set; }
    }
}