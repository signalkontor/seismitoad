﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;
using SeismitoadShared.Models;

namespace MasterInstance.Areas.CM.Models
{
    public class AttendanceViewModel
    {
        public int AssingmentId { get; set; }
        public int EmployeeId { get; set; }

        public string Campaign { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime? MobileCheckInDate { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        [UIHint("SingleSelectKendo")]
        public Attendance Attendance { get; set; }
        public int Marker { get; set; }
        public string Notes { get; set; }
        public string Type { get; set; }
        public string Remark { get; set; }
        public string Username { get; set; }
        public Guid? AttendanceSetByUser { get; set; }
        public string LocationContact { get; set; }
        public string LocationContactPhone { get; set; }
        public string StateColor { get; set; }
    }

    public class AttendanceViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AssignedEmployee, AttendanceViewModel>()
                .ForMember(member => member.Campaign, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Campaign.Name))
                .ForMember(member => member.FirstName, a => a.MapFrom(assignment => assignment.Employee.Firstname))
                .ForMember(member => member.Name, a => a.MapFrom(assignment => assignment.Employee.Lastname))
                .ForMember(member => member.DateStart, a => a.MapFrom(assignment => assignment.Assignment.DateStart))
                .ForMember(member => member.DateEnd, a => a.MapFrom(assignment => assignment.Assignment.DateEnd))
                .ForMember(member => member.LocationId, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.LocationId))
                .ForMember(member => member.LocationName, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Location.Name))
                .ForMember(member => member.Street, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Location.Street))
                .ForMember(member => member.Zip, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Location.PostalCode))
                .ForMember(member => member.City, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Location.City))
                .ForMember(member => member.LocationContact, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Contact.Title + " "+ assignment.Assignment.CampaignLocation.Contact.Firstname + " " + assignment.Assignment.CampaignLocation.Contact.Lastname))
                .ForMember(member => member.LocationContactPhone, a => a.MapFrom(assignment => assignment.Assignment.CampaignLocation.Contact.Phone))
                .ForMember(member => member.Type, a => a.MapFrom(assignment => assignment.AttendanceType))
                .ForMember(member => member.MobileCheckInDate, a => a.MapFrom(assignment => assignment.AttendanceMobileCheckInDate))
                .ForMember(member => member.Marker, a => a.MapFrom(assignment => assignment.Assignment.Marker == 0 && assignment.Assignment.Notes != null ? -1 : assignment.Assignment.Marker))
                .ForMember(member => member.Notes, a => a.MapFrom(assignment => assignment.Assignment.Notes))
                .ForMember(member => member.Remark, a => a.MapFrom(assignment => assignment.AttendanceComment))
                .ForMember(member => member.EmployeeId, a => a.MapFrom(assignment => assignment.EmployeeId))
                .ForMember(member => member.AssingmentId, a => a.MapFrom(assignment => assignment.AssignmentId))
                .ForMember(member => member.Attendance, a => a.MapFrom(assignment => assignment.Attendance))
                ;

            var mapping = Mapper.CreateMap<AttendanceViewModel, AssignedEmployee>();
            mapping.ForAllMembers(opt => opt.Ignore());
            mapping
                .ForMember(member => member.Attendance, a => a.MapFrom(attendance => attendance.Attendance))
                .ForMember(member => member.AttendanceComment, a => a.MapFrom(attendance => attendance.Remark));
        }
    }
}