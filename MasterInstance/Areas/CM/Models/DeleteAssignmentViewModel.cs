﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class DeleteAssignmentViewModel
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string LocationName { get; set; }
        public string LocationStreet { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCity { get; set; }
        public string Team { get; set; }
    }

    public class DeleteAssignmentViewModelAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Assignment, DeleteAssignmentViewModel>()
                .ForMember(e => e.Start, opt => opt.MapFrom(e => e.DateStart))
                .ForMember(e => e.End, opt => opt.MapFrom(e => e.DateEnd))
                .ForMember(e => e.LocationName, opt => opt.MapFrom(e => e.CampaignLocation.Location.Name))
                .ForMember(e => e.LocationStreet, opt => opt.MapFrom(e => e.CampaignLocation.Location.Street))
                .ForMember(e => e.LocationPostalCode, opt => opt.MapFrom(e => e.CampaignLocation.Location.PostalCode))
                .ForMember(e => e.LocationCity, opt => opt.MapFrom(e => e.CampaignLocation.Location.City));
        }
    }
}