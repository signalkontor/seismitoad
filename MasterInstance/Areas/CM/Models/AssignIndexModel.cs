﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ObjectTemplate.Extensions;

namespace MasterInstance.Areas.CM.Models
{
    public class AssignIndexModel
    {
        public int CampaignId { get; set; }
        public static readonly MvcHtmlString TypeFilter = new MvcHtmlString(
            string.Join(",", new AssignmentFilter().ToSelectList().Select(e => string.Format("{{ Value: '{0}', Text: '{1}' }}", e.Value, e.Text))));
    }
}