﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Glimpse.Core.Configuration;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class AcquisitionSearchResultRowViewModel
    {
        public int EmployeeId { get; set; }

        #region A. Persönliche Daten
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime? ProfileBirthday { get; set; }
        public string ProfileEmail2 { get; set; }
        public string ProfileSkype { get; set; }
        public string ProfileWebsite { get; set; }
        public string ProfilePhoneMobileNo { get; set; }
        public string ProfilePhoneNo { get; set; }
        public string ProfileFaxNo { get; set; }
        public string ProfileStreet { get; set; }
        public string ProfilePostalCode { get; set; }
        public string ProfileCity { get; set; }
        public int? ProfileCountry { get; set; }
        public string ProfilePersonalDataRemark { get; set; }
        #endregion

        #region B. Body Profil und Talente
        public int? ProfileHeight { get; set; }
        public int? ProfileSize { get; set; }
        public int? ProfileShirtSize { get; set; }
        public int? ProfileJeansWidth { get; set; }
        public int? ProfileJeansLength { get; set; }
        public int? ProfileShoeSize { get; set; }
        public int? ProfileHairColor { get; set; }
        public bool? ProfileVisibleTattoos { get; set; }
        public bool? ProfileFacialPiercing { get; set; }
        public string ProfileArtisticAbilities { get; set; }
        public string ProfileGastronomicAbilities { get; set; }
        public string ProfileSports { get; set; }
        public string ProfileOtherAbilities { get; set; }
        public string ProfileOtherAbilitiesText { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        public bool? ProfileSkeletonContract { get; set; }
        public bool? ProfileCopyTradeLicense { get; set; }
        public bool? ProfileValidTradeLicense { get; set; }
        public string ProfileTaxOfficeLocation { get; set; }
        public string ProfileTaxNumber { get; set; }
        public bool? ProfileTurnoverTaxDeductible { get; set; }
        public bool? ProfileFreelancerQuestionaire { get; set; }
        public bool? ProfileHealthCertificate { get; set; }
        public bool? ProfileCopyHealthCertificate { get; set; }
        public int? ProfileCopyIdentityCard { get; set; }
        public string ProfileComment { get; set; }
        #endregion

        #region D. Mobilität
        [Display(Name = "Führerschein vorhanden")]
        public bool? ProfileDriversLicense { get; set; }

        [Display(Name = "Führerschein seit")]
        public DateTime? ProfileDriversLicenseSince { get; set; }

        [Display(Name = "Führerscheinklasse (neue)")]
        public string ProfileDriversLicenseClass1 { get; set; }

        [Display(Name = "Führerscheinklasse (alte)")]
        public string ProfileDriversLicenseClass2 { get; set; }
        
        public string ProfileDriversLicenseRemark { get; set; }

        [Display(Name = "Digitale Fahrerkarte vorhanden")]
        public bool? ProfileHasDigitalDriversCard { get; set; }

        [Display(Name = "PKW vorhanden")]
        public bool? ProfileIsCarAvailable { get; set; }

        [Display(Name = "Bahncard vorhanden")]
        public bool? ProfileIsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        [Display(Name = "Schulbildung")]
        public int? ProfileEducation { get; set; }

        [Display(Name = "Berufsausbildung")]
        public string ProfileApprenticeships { get; set; }

        [Display(Name = "Sonst. Ausbildung")]
        public string ProfileOtherCompletedApprenticeships { get; set; }

        [Display(Name = "Studium")]
        public bool? ProfileStudies { get; set; }

        [Display(Name = "Details zum Studium")]
        public string ProfileStudiesDetails { get; set; }

        [Display(Name = "Berufserfahrung")]
        public string ProfileWorkExperience { get; set; }

        [Display(Name = "Deutsch")]
        public int? ProfileLanguagesGerman { get; set; }

        [Display(Name = "Englisch")]
        public int? ProfileLanguagesEnglish { get; set; }

        [Display(Name = "Spanisch")]
        public int? ProfileLanguagesSpanish { get; set; }

        [Display(Name = "Französisch")]
        public int? ProfileLanguagesFrench { get; set; }

        [Display(Name = "Italienisch")]
        public int? ProfileLanguagesItalian { get; set; }

        [Display(Name = "Türkisch")]
        public int? ProfileLanguagesTurkish { get; set; }

        [Display(Name = "Sonstige")]
        public string ProfileLanguagesOther { get; set; }

        [Display(Name = "Software-Kentnisse")]
        public int? ProfileSoftwareKnownledge { get; set; }

        [Display(Name = "Welche SW-Kentnisse?")]
        public string ProfileSoftwareKnownledgeDetails { get; set; }

        [Display(Name = "Hardware-Kentnisse")]
        public int? ProfileHardwareKnownledge { get; set; }

        [Display(Name = "Welche HW-Kentnisse")]
        public string ProfileHardwareKnownledgeDetails { get; set; }

        [Display(Name = "Schulungen intern")]
        public string ProfileInternalTraining { get; set; }

        [Display(Name = "Schulungen extern")]
        public string ProfileExternalTraining { get; set; }
        #endregion

        #region G. Auftragswunsch
        [Display(Name = "Bevorzugte Promotion Aktivitäten")]
        public string ProfilePreferredTasks { get; set; }

        [Display(Name = "Bevorzugte Promotionarten")]
        public string ProfilePreferredTypes { get; set; }

        [Display(Name = "Bevorzugte Arbeitszeiten")]
        public string ProfilePreferredWorkSchedule { get; set; }

        [Display(Name = "Reisebereitschaft")]
        public bool? ProfileTravelWillingness { get; set; }

        [Display(Name = "Übernachtung im Großraum")]
        public string ProfileAccommodationPossible1 { get; set; }

        [Display(Name = "Übernachtung in folgenden Städten")]
        public string ProfileAccommodationPossible2 { get; set; }
        #endregion
    }

    public class AcquisitionSearchResultRowViewModelAutomapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Employee, AcquisitionSearchResultRowViewModel>()
                .ForMember(e => e.EmployeeId, opt => opt.MapFrom(e => e.Id));
        }
    }

}