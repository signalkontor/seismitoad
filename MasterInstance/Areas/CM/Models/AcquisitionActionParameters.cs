﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class AcquisitionActionParameters
    {
        public int CampaignId { get; set; }
        public int[] EmployeeIds { get; set; }
        public string Contract { get; set; }
    }
}