﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Areas.CM.Models
{
    public class AcquisitionUpdateResultViewModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Result { get; set; }
    }

    public class AcquisitionUpdateException : Exception
    {
        public AcquisitionUpdateException(string message) : base(message) { }
    }
}