﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SeismitoadModel;

namespace MasterInstance.Areas.CM.Models
{
    public class OutstandingNotificationViewModel
    {
        public int Id { get; set; }
        public Guid DataId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
    }

    public class OutstandingNotificationViewModelAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<OutstandingNotification, OutstandingNotificationViewModel>()
                .ForMember(e => e.Firstname, opt => opt.MapFrom(e => e.Employee.Firstname))
                .ForMember(e => e.Lastname, opt => opt.MapFrom(e => e.Employee.Lastname))
                ;
        }
    }
}