﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Links;
using ObjectTemplate;
using SeismitoadShared.Constants;

namespace MasterInstance.App_Start
{
    public class ObjectTemplateConfig
    {
        public static void Configure()
        {
            Configurator.Configure(config =>
            {
                config.SelectListRepository = new SelectListRepository();
                config.InfoIconUrl = Content.icons._16x16.actions.info_png;
                config.ShowValidationSummary = true;
                config.AddAllowedComplexTypes("Hours", "Ratings", "Experiences", "CampaignLocations", "DbGeography", "Zeugnisse");
            });

            HtmlAttributes.Initialize();
        }
    }
}