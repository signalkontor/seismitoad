﻿using AutoMapper;

namespace MasterInstance
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                var autoMapperProfiles = FindTypes
                    .InAssembly(typeof(AutoMapperConfig).Assembly)
                    .Implementing<Profile>();

                foreach (var autoMapperProfile in autoMapperProfiles)
                {
                    cfg.AddProfile(autoMapperProfile);
                }
            });
        }
    }
}