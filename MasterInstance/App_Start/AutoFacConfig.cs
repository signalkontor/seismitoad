using System.Web.Mvc;
using AdvantageModel.Models;
using Autofac;
using Autofac.Integration.Mvc;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance
{
    public class AutoFacConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.Register((_) => new SeismitoadDbContext()).InstancePerHttpRequest();
            builder.Register((_) => new seismitoad_advantageContext()).InstancePerHttpRequest();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}

