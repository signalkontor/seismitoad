﻿using System;
using System.Web.Hosting;
using Hangfire;
using Hangfire.Server;

namespace  MasterInstance
{
    public class ApplicationPreload : System.Web.Hosting.IProcessHostPreloadClient
    {
        public void Preload(string[] parameters)
        {
            HangfireBootstrapper.Instance.Start();
        }
    }

    public class JobContext : IServerFilter
    {
        [ThreadStatic]
        private static string _jobId;

        public static string JobId { get { return _jobId; } set { _jobId = value; } }

        public void OnPerforming(PerformingContext context)
        {
            JobId = context.BackgroundJob.Id;
        }

        public void OnPerformed(PerformedContext filterContext)
        {
            // Nothing to do
        }
    }

    public class HangfireBootstrapper : IRegisteredObject
    {
        public static readonly HangfireBootstrapper Instance = new HangfireBootstrapper();
        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;

        private HangfireBootstrapper()
        {
        }

        public void Start()
        {
            lock (_lockObject)
            {
                if (_started) return;
                _started = true;

                HostingEnvironment.RegisterObject(this);
                GlobalConfiguration.Configuration.UseSqlServerStorage("seismitoad_hangfire");
                GlobalConfiguration.Configuration.UseFilter(new JobContext());
                // Specify other options here
                _backgroundJobServer = new BackgroundJobServer();
            }
        }

        public void Stop()
        {
            lock (_lockObject)
            {
                if (_backgroundJobServer != null)
                {
                    _backgroundJobServer.Dispose();
                }
                HostingEnvironment.UnregisterObject(this);
            }
        }

        void IRegisteredObject.Stop(bool immediate)
        {
            Stop();
        }
    }
    
}

