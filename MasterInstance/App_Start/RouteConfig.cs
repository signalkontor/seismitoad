﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MasterInstance.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "CampaignWizard",
                "Campaign/{campaignId}/Steps",
                new { controller = "CampaignWizard", action = "Index" }
                );

            routes.MapRoute(
                "CampaignWizardStep",
                "Campaign/{campaignId}/Step/{step}",
                new { controller = "CampaignWizard", action = "Step" }
                );   
            
            routes.MapRoute(
                "ChangeSetViewer",
                "History/{entityType}/{id}",
                new { controller = "ChangeSet", action = "Show" }
                );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new [] { "MasterInstance.Controllers" }// Parameter defaults
                );
        }
    }
}