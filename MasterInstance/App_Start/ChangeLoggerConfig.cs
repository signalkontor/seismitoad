﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Extensions;
using System.Web.Security;

namespace MasterInstance.App_Start
{
    public class ChangeLoggerConfig
    {
        public static void Configure()
        {
            ChangeLogger.UserKey = () =>
            {
                try
                {
                    if (HttpContext.Current == null || HttpContext.Current.User == null)
                        return null;

                    var user = HttpContext.Current.User;
                    var membershipUser = Membership.GetUser(user.Identity.Name);
                    var userId = (Guid)membershipUser.ProviderUserKey;
                    return userId;
                }
                catch(Exception e)
                {
                    return null;
                }
            };

            ChangeLogger.DbContext = () => new SeismitoadDbContext();
        }
    }
}