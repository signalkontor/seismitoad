﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace MasterInstance.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                "~/Scripts/jquery-{version}.js",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Scripts/jquery-ui-{version}.custom.js",
                "~/Scripts/jquery.blockUI.js",
                "~/Scripts/jquery.cookie.js",
                "~/Scripts/jquery.hotkeys.js",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Scripts/jquery.multiselect.js",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Scripts/jquery.multiselect.filter.js",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Scripts/selectToUISlider.jQuery.js",
                "~/Scripts/fixes.js",
                "~/Scripts/sk-fixes.js",
                "~/Scripts/sk-helpers.js",
                "~/Scripts/splitters.js",
                "~/Scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Content/jquery.multiselect.css",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Content/jquery.multiselect.filter.css",
                "~/Content/base.css",
                "~/Content/dategenerator.css",
                "~/Content/dropdownlist.css",
                "~/Content/filterbetween.css",
                "~/Content/permedia.css",
                "~/Content/propertyselector.css",
                // Falls irgendwann überall Kendo verwendet wird kann dies entfernt werden
                "~/Content/ui.slider.extras.css"));

            bundles.Add(new StyleBundle("~/Content/jqueryui/css").Include("~/Content/jqueryui/jquery-ui-1.8.21.custom.css"));
        }
    }
}