﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Elmah;
using Hangfire;

namespace MasterInstance.Services
{
    public class FaxAttachmentArchiving
    {
        public static void Start()
        {
            if (MvcApplication.IsStaging)
                return;

            RecurringJob.AddOrUpdate("archive-fax-pdfs", () => MoveOldAttachments(), "37 1 * * SUN");
        }

        public static void MoveOldAttachments()
        {
            var dateThreshold = DateTime.Today.AddMonths(-6);
            var firstDirectoryToDelete = string.Format("{0:yyyyMMdd}", dateThreshold);
            var sourcePath = HostingEnvironment.MapPath("~/FaxMessages");
            var destinationPath = ConfigurationManager.AppSettings["FaxPdfArchivePath"];

            if (!Directory.Exists(destinationPath))
            {
                ErrorLog.GetDefault(null).Log(new Error(new DirectoryNotFoundException(destinationPath)));
                return;
            }

// ReSharper disable once AssignNullToNotNullAttribute
            var directories = new DirectoryInfo(sourcePath).GetDirectories().Where(directory =>
                String.Compare(directory.Name, firstDirectoryToDelete,
                    StringComparison.Ordinal) <= 0);

            foreach (var directory in directories)
            {
                Directory.Move(directory.FullName, Path.Combine(destinationPath, directory.Name));
            }
        }
    }
}