﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Elmah;
using Hangfire;
using SeismitoadModel.DatabaseContext;
using System.Net.Http;
using System.Threading.Tasks;

namespace MasterInstance.Services
{
    public class PingCustomerInstances
    {
        public static void Start()
        {
            if (MvcApplication.IsStaging)
                return;

            RecurringJob.AddOrUpdate("ping-customerinstances", () => Ping(), "*/3 * * * *");
        }

        public static void Ping()
        {
            using(var dbContext = new SeismitoadDbContext())
            {
                var urls = dbContext.Campaigns
                    .Where(e => e.State <= SeismitoadModel.CampaignState.Archived && e.InstanceUrl != null && !e.InstanceUrl.StartsWith("_ext_"))
                    .Select(e => e.InstanceUrl)
                    .ToList();

                var tasks = new List<Task>();
                using (var client = new HttpClient())
                {
                    urls.ForEach(url => tasks.Add(client.GetStringAsync(url + "Report/Missing?id=46&campaign=DummyRequest")));
                    try
                    {
                        Task.WaitAll(tasks.ToArray());
                    }
                    catch(AggregateException e)
                    {
                        // ignore
                    }
                }
            }
        }
    }
}