﻿using System;

namespace MasterInstance.Services.EmailReceiver
{
    public class EmailMessage
    {
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachmendPath { get; set; }
        public DateTime ReceiveDate { get; set; }
    }
}