﻿using System;

namespace MasterInstance.Services.EmailReceiver
{
    public class DummyEMailReceiver : IEmailreceiver
    {
        public void GetNewMessages(Func<EmailMessage, bool> handleMessage)
        {
            handleMessage(new EmailMessage
            {
                AttachmendPath = null,
                Body = @"Status: OK
 Sender ISDN-Nummer: '004930263997189'
 Empfänger ISDN-Nummer: '00498912504105015'
 Verbindungsstart: '2014-06-11-11.31.47'
 Verbindungsdauer: 33 Sekunden
 Sender-Kennung: '+49 30 263997189'
 Anzahl der Seiten: 1
 Auflösung: L
 Übertragungsrate: 14400 bps",
                ReceiveDate = new DateTime(2014, 06, 16, 10, 10, 0),
                Sender = "sender",
                Subject = "Fax von '+49 221-925960-40' für '8839'"
            });

            handleMessage(new EmailMessage
            {
                AttachmendPath = null,
                Body = @"Status: OK
 Sender ISDN-Nummer: '00496934008205'
 Empfänger ISDN-Nummer: '00498912504105015'
 Verbindungsstart: '2014-06-11-11.09.50'
 Verbindungsdauer: 34 Sekunden
 Sender-Kennung: '06934008205    ____'
 Anzahl der Seiten: 1
 Auflösung: L
 Übertragungsrate: 14400 bps",
                ReceiveDate = new DateTime(2014, 06, 16, 10, 20, 0),
                Sender = "sender",
                Subject = "Fax von '+49 3322/249050' für '8839'"
            });

        }
    }
}
