﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Elmah;
using Rebex.Net;

namespace MasterInstance.Services.EmailReceiver
{
    public class RebexEmailReceiver : IEmailreceiver
    {
        public static DateTime? GetReceivedDate(string body)
        {
            var lines = body.Split(new [] { "\r\n", "\n" }, StringSplitOptions.None);
            var theLine = lines.SingleOrDefault(e => e.StartsWith("Verbindungsstart: '"));
            if (theLine == null)
                return null;
            theLine = theLine.Replace("Verbindungsstart: '", "");
            if(theLine.Length > 0)
                theLine = theLine.Substring(0, theLine.Length - 1);
            DateTime date;
            return DateTime.TryParseExact(theLine, "yyyy-MM-dd-HH.mm.ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out date)
                ? date
                : (DateTime?) null;
        }

        public void GetNewMessages(Func<EmailMessage, bool> handleMessage)
        {
            var client = new Pop3();
            try
            {
                var server = ConfigurationManager.AppSettings["FaxMessagesPop3Server"];
                var port = Convert.ToInt32(ConfigurationManager.AppSettings["FaxMessagesPop3ServerPort"]);
                var sslMode = ConfigurationManager.AppSettings["FaxMessagesPop3ServerSslMode"];
                switch (sslMode.ToLower())
                {
                    case "none":
                        client.Connect(server, port, SslMode.None);
                        break;
                    case "implicit":
                        client.Connect(server, port, SslMode.Implicit);
                        break;
                    case "explicit":
                        client.Connect(server, port, SslMode.Explicit);
                        break;
                    default:
                        // ReSharper disable once NotResolvedInText
                        throw new ArgumentException("Invalid SslMode: " + sslMode, "FaxMessagesPop3ServerSslMode");
                }

                client.Login(ConfigurationManager.AppSettings["FaxMessagesPop3ServerUser"], ConfigurationManager.AppSettings["FaxMessagesPop3ServerPassword"]);
                var list = client.GetMessageList();
                
                foreach (var pop3Message in list)
                {
                    var message = client.GetMailMessage(pop3Message.SequenceNumber);

                    var email = new EmailMessage
                        {
                            Sender = message.From.Count > 0 ? message.From[0].DisplayName : "",
                            Subject = message.Subject,
                            Body = message.BodyText,
                            ReceiveDate = GetReceivedDate(message.BodyText).GetValueOrDefault(new DateTime(1900, 1, 1))
                        };

                    // Auch wenn der Filename ja eigentlich nur gebraucht wird, wenn es attachments gibt
                    // die wir ja erst nachher speichern, müssen wir hier den Namen erstellen, da bereits im handleMessage(email)
                    // alles in die DB gespeichert wird.
                    var virtualPath = string.Format("~/FaxMessages/{0:yyyyMMdd}/{1}.pdf", email.ReceiveDate, Guid.NewGuid());
                    var filename = HostingEnvironment.MapPath(virtualPath);
                    email.AttachmendPath = Path.GetFileName(filename);

                    var result = handleMessage(email);
                    if(result == false)
                        continue;
                    
                    // Eigentlich wäre es logische diesen Code in handleMessage einzufügen, da wäre aber jetzt zu auswändig
                    // umzubauen.

                    // Wir müssen noch das Fax herunterladen, damit es später 
                    // angezeigt werden kann.
                    var attachment = message.Attachments
                        .FirstOrDefault(e => e.FileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase));
                    
                    if (attachment != null)
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(filename));
                        attachment.Save(filename);
                    }
                    client.Delete(pop3Message.SequenceNumber);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.GetDefault(null).Log(new Error(ex));
            }
            finally
            {
                client.Disconnect();
            }
        }
     }
}
