﻿namespace MasterInstance.Services.EmailReceiver
{
    public class FaxData
    {
        public int CampaignId { get; set; }
        public string FaxNumber { get; set; }
    }
}