﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterInstance.Services.EmailReceiver
{
    public interface IEmailreceiver
    {
        void GetNewMessages(Func<EmailMessage, bool> handleMessage);
    }
}