﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Web.Hosting;
using Elmah;
using Hangfire;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;
using System.Data.SqlClient;

namespace MasterInstance.Services
{
    public class PromoterNotification
    {
        private const string TypeNewAssignments = "NewAssignments";
        private const string TypeCancelledAssignments = "CancelledAssignments";
        public const string TypeMovedAssignment = "MovedAssignment";
        public const string TypeTimeChangedAssignments = "TimeChangedAssignments";

        // In dieser Klasse brauchen wir einiges an Intelligenz.
        // Beispielsweise wäre es wünschenswert, dass eine Zuweisung ohne Benachrichtung mit anschließender
        // Stornierung keine Benachrichtigungen auslöst.
        private static readonly string Directory = HostingEnvironment.MapPath("~/Notifications/");

        private readonly List<AssignedEmployee> _newAssignments = new List<AssignedEmployee>();
        private readonly List<AssignedEmployee> _cancelledAssignments = new List<AssignedEmployee>();
        private readonly Dictionary<int, IList<Movement>> _movedAssignments = new Dictionary<int, IList<Movement>>();
        private readonly List<Assignment> _timeChangedAssignments = new List<Assignment>();

        public PromoterNotification ForNewAssignments(IList<AssignedEmployee> assignedEmployees)
        {
            //// Wenn der Promoter erst von diesem Einsatz entfernt wurde und darüber noch nicht benachrichtigt wurde
            //// und nun dem Einsatz erneut hinzugefügt wurde können wir die Stornobenachrichtigung löschen
            //using (var dbContext = new SeismitoadDbContext())
            //{
            //    foreach (var assignedEmployee in assignedEmployees)
            //    {
            //        var assignmentId = "," + assignedEmployee.AssignmentId + "_" + assignedEmployee.EmployeeId;
            //        var outstandingNotifications = dbContext.OutstandingNotifications
            //            .Where(e => e.EmployeeId == assignedEmployee.EmployeeId && e.AssignmentIds.Contains(assignmentId));

            //        var hadOutstandingNotifications = false;
            //        foreach (var outstandingNotification in outstandingNotifications)
            //        {
            //            RemoveAssignmentFromSavedData(outstandingNotification.DataId, assignedEmployee.AssignmentId);
            //            hadOutstandingNotifications = true;
            //        }
            //        if(!hadOutstandingNotifications)
            //        {
            //            _newAssignments.Add(assignedEmployee);
            //        } else
            //        {

            //        }
            //    }
            //}
            _newAssignments.AddRange(assignedEmployees);
            return this;
        }

        public PromoterNotification ForCancelledAssignments(IList<AssignedEmployee> assignedEmployees)
        {
            // Wenn der Promoter über den Einsatz der hier entfernt wird noch nicht benachrichtigt wurde
            // bruachen wir ihn auch nicht über das entfernen benachrichtigen. Die eventuell noch ausstehende
            // Benachrichtigung für die - non obsolete - Zuweisung muss auch gelöscht werden.
            using (var dbContext = new SeismitoadDbContext())
            {
                foreach (var assignedEmployee in assignedEmployees)
                {
                    var notifiedAndHasNotRejected = dbContext.Database
                        .SqlQuery<int>(@"SELECT 1 FROM AssignedEmployeeExtraData WHERE AssignmentId = @AssignmentId AND EmployeeId = @EmployeeId AND Notified IS NOT NULL AND Rejected IS NULL",
                        new SqlParameter("@AssignmentId", assignedEmployee.AssignmentId), new SqlParameter("@EmployeeId", assignedEmployee.EmployeeId) );

                    if (notifiedAndHasNotRejected.Any())
                    {
                        // Wurde schon benachrichtigt, dann müssen wir die Stornierungsmail senden
                        _cancelledAssignments.Add(assignedEmployee);
                    }
                    else
                    {
                        // Der Promoter weiß noch von nichts oder hat selbst storniert. In dem Fall unterdrücken wir die Stornomail.
                        // Außerdem können wir eventuell ausstehende Benachrichtigungen zu diesem Einsatz löschen
                        var assignmentId = "," + assignedEmployee.AssignmentId + "_" + assignedEmployee.EmployeeId;
                        var outstandingNotifications = dbContext.OutstandingNotifications
                            .Where(e => e.EmployeeId == assignedEmployee.EmployeeId && e.AssignmentIds.Contains(assignmentId));

                        foreach(var outstandingNotification in outstandingNotifications) 
                        {
                            RemoveAssignmentFromSavedData(outstandingNotification.DataId, assignedEmployee.AssignmentId);
                        }
                    }
                }
            }
            return this;
        }

        public PromoterNotification ForCancelledAssignemnt(Assignment assignment)
        {
            ForCancelledAssignments(assignment.AssignedEmployees.ToList());
            return this;
        }

        public PromoterNotification ForMovedAssignment(AssignedEmployee originalAssignment, AssignedEmployee newAssigment)
        {
            IList<Movement> movements;
            if (!_movedAssignments.TryGetValue(originalAssignment.EmployeeId, out movements))
            {
                movements = new List<Movement>();
                _movedAssignments.Add(originalAssignment.EmployeeId, movements);
            }
            movements.Add(new Movement { originalAssigmment = originalAssignment.Assignment, newAssignment = newAssigment });
            return this;
        }

        public PromoterNotification ForChangedTimeOfAssignments(IList<Assignment> assignments)
        {
            _timeChangedAssignments.AddRange(assignments);
            return this;
        }

        public Guid? ForImmediateDelivery(DeliveryTime time)
        {
            System.IO.Directory.CreateDirectory(Directory);
            var username = HttpContext.Current.User.Identity.Name;
            var dataId = HandleInternal(TypeNewAssignments, username, _newAssignments, time);
            // Beim Ersetzen von Promotern kann es zu konflikten kommen. Dem Promoter der ersetzt wird, können wir aber sofort benachrichtigen.
            HandleInternal(TypeCancelledAssignments, username, _cancelledAssignments, time == DeliveryTime.AfterConflictsWindowClosed ? DeliveryTime.Now : time);
            if (_movedAssignments.Any())
            {
                HandleMovedInternal(username, time);
            }
            if (_timeChangedAssignments.Any())
            {
                HandleTimeChangeInternal(username, time);
            }
            return dataId;
        }

        private static Guid? HandleInternal(string type, string username, List<AssignedEmployee> assignedEmployees, DeliveryTime time)
        {
            if (assignedEmployees.Any())
            {
                var data = new PromoterNotificationData(type, username, assignedEmployees);
                var dataId = Guid.NewGuid();
                SaveToDisk(data, dataId);
                switch (time)
                {
                    case DeliveryTime.Now:
                        SetNotified(data);
                        BackgroundJob.Enqueue(() => SendNotification(dataId));
                        break;
                    case DeliveryTime.AfterConflictsWindowClosed:
                    case DeliveryTime.Later:
                        var campaignLocation = assignedEmployees.First().Assignment.CampaignLocation;
                        SaveToDatabase(data, campaignLocation.Location, campaignLocation.CampaignId, dataId);
                        break;
                }
                if (time == DeliveryTime.AfterConflictsWindowClosed)
                {
                    HeldBackNotifications.Add(dataId);
                    return dataId;
                }
            }
            return null;
        }

        public static IList<Guid> HeldBackNotifications
        {
            get
            {
                var list = HttpContext.Current.Session["HeldBackNotifications"] as IList<Guid>;
                if (list == null)
                {
                    list = new List<Guid>();
                    HttpContext.Current.Session["HeldBackNotifications"] = list;
                }
                return list;
            }
        }

        public class Movement
        {
            public Assignment originalAssigmment;
            public AssignedEmployee newAssignment;
        }

        private void HandleMovedInternal(string username, DeliveryTime time)
        {
            foreach (var movements in _movedAssignments)
            {
                var data = new PromoterNotificationData(username, movements.Key, movements.Value);
                var dataId = Guid.NewGuid();
                SaveToDisk(data, dataId);
                switch (time)
                {
                    case DeliveryTime.Now:
                        SetNotified(data);
                        BackgroundJob.Enqueue(() => SendNotification(dataId));
                        break;
                    case DeliveryTime.Later:
                        var campaignLocation = movements.Value.First().originalAssigmment.CampaignLocation;
                        SaveToDatabase(data, campaignLocation.Location, campaignLocation.CampaignId, dataId);
                        break;
                    case DeliveryTime.AfterConflictsWindowClosed:
                        throw new InvalidOperationException();
                }
            }
        }

        private void HandleTimeChangeInternal(string username, DeliveryTime time)
        {
            // Hier müssen für jeden Promoter getrennt alle für ihn passenden Einträge zusammengestellt werden
            var movementsForEmployee = new Dictionary<int, IList<AssignedEmployee>>();
            foreach (var assignment in _timeChangedAssignments)
            {
                foreach (var assignedEmployee in assignment.AssignedEmployees)
                {
                    IList<AssignedEmployee> assignedEmployees;
                    if (!movementsForEmployee.TryGetValue(assignedEmployee.EmployeeId, out assignedEmployees))
                    {
                        assignedEmployees = new List<AssignedEmployee>();
                        movementsForEmployee.Add(assignedEmployee.EmployeeId, assignedEmployees);
                    }
                    assignedEmployees.Add(assignedEmployee);
                }
            }
            foreach (var movements in movementsForEmployee)
            {
                var data = new PromoterNotificationData(username, movements.Key, movements.Value);
                var dataId = Guid.NewGuid();
                SaveToDisk(data, dataId);
                switch (time)
                {
                    case DeliveryTime.Now:
                        SetNotified(data);
                        BackgroundJob.Enqueue(() => SendNotification(dataId));
                        break;
                    case DeliveryTime.Later:
                        var campaignLocation = _timeChangedAssignments.First().CampaignLocation;
                        SaveToDatabase(data, campaignLocation.Location, campaignLocation.CampaignId, dataId);
                        break;
                    case DeliveryTime.AfterConflictsWindowClosed:
                        throw new InvalidOperationException();
                }
            }

        }

        private static PromoterNotificationData LoadPromoterNotification(string filename)
        {
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                var formatter = new BinaryFormatter();
                return formatter.Deserialize(stream) as PromoterNotificationData;
            }
        }
        
        public static void SendNotification(Guid id)
        {
            var emailService = Helpers.GetEmailService();
            var recipient = "unknown recepient";
            var subject = "unknown subject";
            try
            {
                var filename = Path.Combine(Directory, id + ".bin");

                // Es ist möglich, dass durch durch das entfernen von Konflikten eine Notification nicht mehr existiert.
                // Das fangen wir hier ab.
                if (!File.Exists(filename))
                    return;

                var data = LoadPromoterNotification(filename);
                recipient = "EmployeeId = " + data.EmployeeId;
                subject = "Notificationtype = " + data.Type;
                var mail = new AssignmentNotificationEmail(data);
                recipient = mail.To;
                emailService.Send(mail);
                // Laut Ticket http://redmine.advantage-promotion.de/redmine/issues/377 soll der Einsatz sofort auf benachrichtigt
                // gesetzt werden und nicht erst wenn die Mail gesendet wird.
                //SetNotified(data);
                File.Delete(filename);
            }
            catch (Exception exception)
            {
                var loggedException = new Exception(string.Format("Failed sending notification e-mail with subject '{0}' to {1}", subject, recipient), exception);
                ErrorLog.GetDefault(null).Log(new Error(loggedException));
                throw;
            }
        }

        public static void SetNotified(Guid dataId)
        {
            var filename = Path.Combine(Directory, dataId + ".bin");

            // Es ist möglich, dass durch durch das entfernen von Konflikten eine Notification nicht mehr existiert.
            // Das fangen wir hier ab.
            if (!File.Exists(filename))
                return;

            SetNotified(LoadPromoterNotification(filename));
        }

        private static void SetNotified(PromoterNotificationData data)
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                // Jetzt müssen wir noch sicherstellen, dass der Promoter die Einsätze bestätigt
                // Bestätigen muss er a) neue Einsätze und b) Einsätze bei denen sich die Uhrzeit geändert hat
                // und c) Einsätze die auf ein neues Datum verschoben wurden.

                // Wenn es eine Absage gab, muss das der Promoter nicht bestätigen. Allerdings muss der vorhanden
                // Eintrag in AssignedEmployeeExtraData gelöscht werden, weil es ja sein könnte, das der Promoter
                // nach der Absage doch noch mal zugewiesen wird und dann darf natürlich keine vorherige Besätigung
                // mehr gespeichert sein. Allerdings müssen wir uns hierum nicht kümmern, weil wir einnen Datenbank-
                // Trigger ([UpdateAssignedEmployeeExtraData]) haben der das macht.

                // Auch beim ändern des Datums wird ja die alte Zuweisung gelöscht und eine neue angelegt. Auch hier
                // müssen wir uns um die alte Zuweisung nicht kümmern, weil das der Trigger macht. Für die neue Zuweisung
                // müssen wir aber das Notified-Flag setzen. Das macht folgendes UPDATE-Statement.
                dbContext.Database.ExecuteSqlCommand(
                    string.Format("UPDATE AssignedEmployeeExtraData SET Notified = GETDATE(), Accepted = NULL, Rejected = NULL WHERE AssignmentId in ({0}) AND EmployeeId = {1}",
                    string.Join(",", data.Assignments.Select(e => e.AssignmentId)), data.EmployeeId));

                // Mal überlegen, ob dieses Statement richtig ist.

                // Fall a) Die Einsätze sind neu. In data.Assignments stecken alle Daten dazu.
                // Insbesondere data.Assignments.Select(e => e.AssignmentId) enthält alle Ids der neu zugewiesenen Assignments.
                // Bei Inserts in AssignedEmployees hat der o.g. Trigger einen quasi leeren Eintrag (nur AssignmentId und EmployeeId)
                // in AssignedEmployeeExtraData angelegt. Daher müssen wir hier nun Notified auf das aktuelle Datum setzzen (check!).
                // Das wir "Accepted" und "Rejected" auf NULL setzen ist wurscht, weil sie es vorher auch schon sind.
                //
                // Fall b) Die Uhrzeit hat sich geändert. In diesem Fall sind data.Assignments.Select(e => e.AssignmentId) wieder
                // die Ids der betroffenen Assignments. Wie bei a) müssen wir Notified setzen, aber hier wird das NULL setzen von
                // Accepted wichtig. Es könnte ja sein, dass der Promoter den ursprünglichen Termin bestätigt hat, wenn sich die
                // Zeit ändert, muss er das aber noch mal bestätigen. Daher setzen wir es hier auf NULL. Wobei das Accepted = NULL
                // hier auch nur eine "Vorsichtsmaßnahme" ist. Denn bereits vorher in AssignmentController._UpdateDate() setzen wir
                // bei geänderten Zeiten alle drei Flags auf NULL.
                //
                // Fall c) Das Datum wurde geändert. Hier sollte man zuerst einen kurzen Blick auf den Contructor
                // public PromoterNotificationData(string userNameOfSender, int employeeId, IList<PromoterNotification.Movement> movements)
                // werfen. In dem sieht man, dass data.Assignments eine Liste in der der an den gerade Indizez (0, 2, 4, ...) jeweils das
                // Original-Assignemnt steht und an den ungeraden Indizes (1, 3, 5, ...) jeweils der Ersatztermin.
                // Da für die Originaltermine die Zuweisungen schon gelöscht wurden (siehe AssignController) und somit wieder der o.g.
                // Trigger aktiv wurde, gibt es für die AssignmentIds an den geraden Indizes keine Einträge in AssignedEmployeeExtraData.
                // Somit wird auch nichts umgedatet (was ja okay ist). Für die Ids an den ungeraden Indizes gilt dann aber wieder Fall a)
                // womit dann eigentlich gezeigt wurde, dass das Statement (unter den beschrieben Rahmenbedigungen z.B. des Triggers)
                // funktioniert.
            }
        }

        internal static void UpdateMovement(OutstandingNotification outstandingNotification, Assignment originalAssignment, Assignment newAssignment)
        {
            // Hier müssen wie die Datei öffnen. Dann müssen wir mittels originalAssignment das richtige Assignment in
            // der Datei finden und dieses durch newAssignment ersetzen. Anschließend müssen wir die Datei speichern und
            // Den OutstandingNotification Eintrag in der DB Updaten
            var filename = Path.Combine(Directory, outstandingNotification.DataId + ".bin");
            var ids = outstandingNotification.AssignmentIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            // Nur die ungeraden indizes ansehen
            for(var i = 1; i < ids.Length; i += 2)
            {
                if(ids[i].StartsWith(originalAssignment.Id + "_"))
                {
                    ids[i] = newAssignment.Id + "_" + outstandingNotification.EmployeeId;
                    break; // Wir können aufhören sobald wird den richtigen Eintrag gefunden haben
                }
            }
            outstandingNotification.AssignmentIds = "," + string.Join(",", ids);
            outstandingNotification.Details = outstandingNotification.Details.Replace(string.Format("⇒ {0:dd.MM.yyyy}", originalAssignment.DateStart), string.Format("⇒ {0:dd.MM.yyyy}", newAssignment.DateStart));

            if (!File.Exists(filename))
            {
                // Sollte eigentlich nicht vorkommen. Daher loggen wir das mal in ELmah
                ErrorLog.GetDefault(null).Log(new Error(new FileNotFoundException("PromoterNotfication file ist missing.", filename)));
                return;
            }
            var promoterNotification = LoadPromoterNotification(filename);
            foreach(var assignment in promoterNotification.Assignments)
            {
                if(assignment.AssignmentId == originalAssignment.Id)
                {
                    assignment.AssignmentId = newAssignment.Id;
                    assignment.DateStart = newAssignment.DateStart;
                    assignment.DateEnd = newAssignment.DateEnd;
                    break; // Wir können aufhören sobald wird den richtigen Eintrag gefunden haben
                }
            }
            SaveToDisk(promoterNotification, outstandingNotification.DataId);
        }

        private static void SaveToDisk(PromoterNotificationData data, Guid id)
        {
            var filename = Path.Combine(Directory, id + ".bin");
            using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, data);
            }
        }

        private static void SaveToDatabase(PromoterNotificationData data, Location location, int campaignId, Guid id)
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                var outstandingNotification = new OutstandingNotification
                {
                    DataId = id,
                    CreatedBy = HttpContext.Current.User.Identity.Name,
                    CreatedDate = LocalDateTime.Now,
                    EmployeeId = data.EmployeeId,
                    CampaignId = campaignId,
                    AssignmentIds = "," + string.Join(",", data.Assignments.Select(e => e.AssignmentId + "_" + data.EmployeeId))
                };

                switch (data.Type)
                {
                    case TypeNewAssignments:
                        outstandingNotification.Type = data.Assignments.Length > 1
                            ? "Neue Aktionstage"
                            : "Neuer Aktionstag";
                        break;
                    case TypeCancelledAssignments:
                        outstandingNotification.Type = data.Assignments.Length > 1
                            ? "Stornierte Aktionstage"
                            : "Stornierter Aktionstag";
                        break;
                    case TypeMovedAssignment:
                        // Größer 2 ist hier richtig, da es immer mindestens 2 Zeilen sind. Die ungeraden Zeilen enthalten
                        // den original Tag, die geraden den neuen Tag.
                        outstandingNotification.Type = data.Assignments.Length > 2 
                            ? "Verschobene Aktionstage"
                            : "Verschobener Aktionstag";
                        break;
                    case TypeTimeChangedAssignments:
                        outstandingNotification.Type = "Geänderte Zeiten";
                        break;
                }

                var stringBuilder = new StringBuilder()
                    .Append(location.Name)
                    .Append(" ")
                    .Append(location.Street)
                    .Append(" ")
                    .Append(location.City)
                    .Append(": ");

                if (data.Type == TypeMovedAssignment)
                {
                    for (var i = 0; i < data.Assignments.Length; i += 2)
                    {
                        stringBuilder.AppendFormat("{0:dd.MM.yyyy} ⇒ {1:dd.MM.yyyy}, ", data.Assignments[i].DateStart,
                            data.Assignments[i+1].DateStart);
                    }
                    stringBuilder.Length -= 2; // Das überflüssige ", " am Ende entfernen
                }
                else
                {
                    stringBuilder.Append(string.Join(", ", data.Assignments.Select(e => e.DateStart.ToString("dd.MM.yyyy"))));
                }
                outstandingNotification.Details = stringBuilder.ToString();
                dbContext.OutstandingNotifications.Add(outstandingNotification);
                dbContext.SaveChanges();
            }
        }

        public static void RemoveAssignmentFromSavedData(Guid dataId, int assignmentId)
        {
            try
            {
                var filename = Path.Combine(Directory, dataId + ".bin");
                var data = LoadPromoterNotification(filename);
                data.Assignments = data.Assignments.Where(e => e.AssignmentId != assignmentId).ToArray();
                if (data.Assignments.Length == 0)
                {
                    // Wenn keine Assignments mehr drin sind, können wir diese Notification auch löschen.
                    using (var dbContext = new SeismitoadDbContext())
                    {
                        var notification = dbContext.OutstandingNotifications.Single(e => e.DataId == dataId);
                        dbContext.OutstandingNotifications.Remove(notification);
                        dbContext.SaveChanges();
                    }
                    File.Delete(filename);
                }
                else
                {
                    using (var dbContext = new SeismitoadDbContext())
                    {
                        var notification = dbContext.OutstandingNotifications.Single(e => e.DataId == dataId);
                        notification.AssignmentIds = "," + string.Join(",", data.Assignments.Select(e => e.AssignmentId + "_" + data.EmployeeId));
                        var newDetails = notification.Details.Substring(0, notification.Details.LastIndexOf(": ")+2);
                        newDetails += string.Join(", ", data.Assignments.Select(e => e.DateStart.ToString("dd.MM.yyyy")));
                        notification.Details = newDetails;
                        dbContext.SaveChanges();
                    }

                    SaveToDisk(data, dataId);
                }
            }
            catch
            {
                
            }
        }
    }

    [Serializable]
    public class PromoterNotificationData
    {
        public int EmployeeId { get; set; }
        public string Type { get; set; }
        public string UserNameOfSender { get; set; }
        public AssignmentData[] Assignments { get; set; }

        public PromoterNotificationData(string type, string userNameOfSender, ICollection<AssignedEmployee> assignedEmployees)
        {
            Type = type;
            UserNameOfSender = userNameOfSender;
            Assignments = new AssignmentData[assignedEmployees.Count];
            var index = 0;
            foreach (var assignedEmployee in assignedEmployees)
            {
                EmployeeId = assignedEmployee.EmployeeId;
                Assignments[index++] = new AssignmentData
                {
                    AssignmentId = assignedEmployee.AssignmentId,
                    DateStart = assignedEmployee.Assignment.DateStart,
                    DateEnd = assignedEmployee.Assignment.DateEnd,
                };
            }
        }

        public PromoterNotificationData(string userNameOfSender, int employeeId, IList<PromoterNotification.Movement> movements)
        {
            Type = PromoterNotification.TypeMovedAssignment;
            UserNameOfSender = userNameOfSender;
            EmployeeId = employeeId;
            Assignments = new AssignmentData[movements.Count*2];
            var index = 0;
            foreach (var movement in movements.OrderBy(e => e.originalAssigmment.DateStart))
            {
                var o = movement.originalAssigmment;
                var n = movement.newAssignment.Assignment;
                Assignments[index++] = new AssignmentData { AssignmentId = o.Id, DateStart = o.DateStart, DateEnd = o.DateEnd};
                Assignments[index++] = new AssignmentData { AssignmentId = n.Id, DateStart = n.DateStart, DateEnd = n.DateEnd};
            }
        }

        public PromoterNotificationData(string userNameOfSender, int employeeId, IEnumerable<AssignedEmployee> movements)
        {
            Type = PromoterNotification.TypeTimeChangedAssignments;
            UserNameOfSender = userNameOfSender;
            EmployeeId = employeeId;
            Assignments = movements.OrderBy(e => e.Assignment.DateStart).Select(e => new AssignmentData
            {
                AssignmentId = e.AssignmentId,
                DateStart = e.Assignment.DateStart,
                DateEnd = e.Assignment.DateEnd
            }).ToArray();
        }

    }

    [Serializable]
    public class AssignmentData
    {
        public int AssignmentId { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }

    public enum DeliveryTime
    {
        Now,
        Later,
        AfterConflictsWindowClosed
    }

}