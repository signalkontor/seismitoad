﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using Elmah;
using Hangfire;
using SeismitoadMembershipProvider;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace MasterInstance.Services
{
    public class AssignmentSyncService
    {
        private static readonly JavaScriptSerializer Serializer = new JavaScriptSerializer();

        private static object _locker = new object();

        public static void Start()
        {
            if (MvcApplication.IsStaging == false)
            {
                // Alle 5 Minuten
                RecurringJob.AddOrUpdate("sync-service", () => Synchronize(), "*/5 * * * *");
            }
        }

        public static void Synchronize()
        {
            // Nur einen Thread zur Zeit zulassen
            if (Monitor.TryEnter(_locker) == false)
                return;

            string lastSyncInfo = "";
            try
            {

                Directory.CreateDirectory(@"C:\Logs");
                try
                {
                    if (new FileInfo(@"C:\Logs\SyncLog.txt").Length > 1024 * 1024 * 5)
                    {
                        File.Move(@"C:\Logs\SyncLog.txt",
                            string.Format(@"C:\Logs\SyncLog-{0:yyyy-MM-dd-HH-mm}.txt", LocalDateTime.Now));
                    }
                }
                catch
                {
                }
                using (var writer = new StreamWriter(@"C:\Logs\SyncLog.txt", true, Encoding.UTF8))
                {
                    Log(writer, "Started");

                    using (var context = new SeismitoadDbContext())
                    {
                        var assignmentsPerCampaign = context.Assignments
                            .Where(e => e.Synchronized != true &&
                                        e.CampaignLocation.Campaign.InstanceUrl != null &&
                                        e.CampaignLocation.Campaign.State != CampaignState.Archived &&
                                        e.CampaignLocation.Campaign.State != CampaignState.Deleted)
                            .GroupBy(e => e.CampaignLocation.Campaign);


                        var hadAssignments = false;
                        foreach (var grouping in assignmentsPerCampaign.ToList())
                        {
                            lastSyncInfo = string.Format("Campaign {0} '{1}' ({2})", grouping.Key.Id, grouping.Key.Name, grouping.Key.InstanceUrl);
                            hadAssignments = true;
                            Log(writer, "Before SynchronizeInternal() CampaignId" + grouping.Key.Id);
                            SynchronizeInternal(grouping, writer);
                            Log(writer, "After SynchronizeInternal()");
                            Log(writer, "Before SaveChanges()");
                            context.SaveChanges();
                            Log(writer, "After SaveChanges()");
                        }
                        if (!hadAssignments)
                        {
                            Log(writer, "Nothing to do this time.");
                        }
                        Log(writer, "Finished");
                    }
                }
            }
            catch (Exception e)
            {
               ErrorLog.GetDefault(null).Log(new Error(e));
                ErrorLog.GetDefault(null).Log(new Error(new Exception(lastSyncInfo)));
            }
            finally
            {
                Monitor.Exit(_locker);
            }
        }

        private static void SynchronizeInternal(IGrouping<Campaign, Assignment> grouping, TextWriter writer)
        {
            var baseUrl = grouping.Key.InstanceUrl;
            if(baseUrl.StartsWith("_ext_"))
            {
                baseUrl = baseUrl.Substring(4);
            }

            var skip = 0;
            var updates = grouping.Where(e => e.State == AssignmentState.Planned).Skip(skip).Take(20);
            while (updates.Any())
            {
                var serializedAssignments = Serializer.Serialize(updates.ToSerializableAssignments(true));

                var bytes = Encoding.UTF8.GetBytes(serializedAssignments);
                var securityToken = SecurityToken.ForServiceController(bytes.ToSHA1(), grouping.Key.Id).Value;
                var url =
                    string.Format(
                        "{0}Service/SetAssignments?securityToken={1}&synchronizingAll={2}&allOrNothing={3}&aspnetuser={4}",
                        baseUrl, securityToken, false, false, "service");
                var request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = bytes.Length;

                try
                {
                    var stream = request.GetRequestStream();
                    stream.Write(bytes, 0, bytes.Length);
                    var response = request.GetResponse();
                    // ReSharper disable once AssignNullToNotNullAttribute
                    var responseStream = new StreamReader(response.GetResponseStream());
                    var result = responseStream.ReadToEnd();
                    Log(writer, "Response was: " + result);

                    foreach (var assignment in updates)
                    {
                        assignment.Synchronized = true;
                    }
                }
                catch (WebException webException)
                {
                    Log(writer, webException.Message);
                    Console.WriteLine(webException.Message);
                }
                skip = skip + 20;
                updates = grouping.Where(e => e.State == AssignmentState.Planned).Skip(skip).Take(20);
            }
            Log(writer, "Doing Deletes");
            var deletes = grouping
                .Where(e => e.State != AssignmentState.Planned)
                .ToList();

            foreach (var delete in deletes)
            {
                var securityToken = SecurityToken.ForServiceController(delete.Id.ToString(CultureInfo.InvariantCulture), delete.CampaignLocation.Campaign.Id).Value;
                var request = WebRequest.Create(string.Format("{0}Service/DeleteAssignment?securityToken={1}&assignmentId={2}&aspnetuser={3}", baseUrl, securityToken, delete.Id, "User"));
                request.Method = "DELETE";
                try
                {
                    Log(writer, request.RequestUri.AbsoluteUri);
                    var response = request.GetResponse() as HttpWebResponse;
                    var responseStream = new StreamReader(response.GetResponseStream());
                    var result = responseStream.ReadToEnd();
                    delete.Synchronized = true;
                }
                catch (WebException webException)
                {
                    Log(writer, webException.Message);
                    Console.WriteLine(webException.Message);
                }
            }
        }

        private static void Log(TextWriter writer, string message)
        {
            writer.WriteLine("{0:O}: {1}", LocalDateTime.Now, message);
        }
    }

    static class Serialization
    {
        public static IEnumerable<object> ToSerializableAssignments(this IEnumerable<Assignment> assignments, bool localtime = false)
        {
            foreach (var assignment in assignments.Where(e => e.State == AssignmentState.Planned))
            {
                IEnumerable<AssignedEmployee> assignedEmployees;
                if (assignment.AssignedEmployees != null)
                {
                    var reportingEmployees = assignment.AssignedEmployees.Where(e => e.AssignmentRoles.Any(i => i.DoesReport)).ToList();
                    assignedEmployees = reportingEmployees.Any() ? (IEnumerable<AssignedEmployee>)reportingEmployees : new Collection<AssignedEmployee> { null };
                }
                else
                {
                    assignedEmployees = new Collection<AssignedEmployee> { null };
                }
                foreach (var assignedEmployee in assignedEmployees)
                {
                    yield return new
                    {
                        AssignmentId = assignment.Id,
                        DateStart = localtime ? assignment.DateStart.ToLocalTime() : assignment.DateStart,
                        DateEnd = localtime ? assignment.DateEnd.ToLocalTime() : assignment.DateEnd,
                        State = (int)assignment.State,
                        OnlySalesReport = assignedEmployee == null || assignedEmployee.AssignmentRoles == null
                           ? (bool?)null
                           : assignedEmployee.AssignmentRoles.Any(e => e.OnlySalesReport),
                        Location = new
                        {
                            assignment.CampaignLocation.Location.Id,
                            assignment.CampaignLocation.Location.Name,
                            assignment.CampaignLocation.Location.Street,
                            assignment.CampaignLocation.Location.PostalCode,
                            assignment.CampaignLocation.Location.City,
                            LocationGroup = assignment.CampaignLocation.Location.LocationGroup != null ? assignment.CampaignLocation.Location.LocationGroup.Name : null,
                            assignment.CampaignLocation.SalesRegion,
                        },
                        Employee = assignedEmployee == null ? null : new
                        {
                            assignedEmployee.Employee.Id,
                            assignedEmployee.Employee.Firstname,
                            assignedEmployee.Employee.Lastname,
                            Attendance = (int)assignedEmployee.Attendance // IntAttendence Property funktioniert hier nicht
                        }
                    };
                }
            }
        }
    }

}