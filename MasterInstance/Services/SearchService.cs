﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Services
{
    public class SearchService
    {
        public IQueryable<Employee> Employees(IQueryable<Employee> employees, EmployeeSearchModel model)
        {
            var today = DateTime.Today;

            if (model.State.HasValue)
                employees = employees.Where(e => e.State == model.State);

            if (model.Title != null)
                employees = employees.Where(e => e.Title == model.Title);

            if (model.Firstname != null)
                employees = employees.Where(e => e.Firstname.Contains(model.Firstname));

            if (model.Lastname != null)
                employees = employees.Where(e => e.Lastname.Contains(model.Lastname));

            if (model.AgeMin.HasValue)
                employees = employees.Where(e => SqlFunctions.DateDiff("year", e.Profile.Birthday, today) >= model.AgeMin);

            if (model.AgeMax.HasValue)
                employees = employees.Where(e => SqlFunctions.DateDiff("year", e.Profile.Birthday, today) <= model.AgeMax);

            if (model.Email != null)
                employees = employees.Where(e => e.Email.Contains(model.Email) || e.Profile.Email2.Contains(model.Email));

            if (model.Skype != null)
                employees = employees.Where(e => e.Profile.Skype.Contains(model.Skype));

            if (model.Website != null)
                employees = employees.Where(e => e.Profile.Website.Contains(model.Website));

            if (model.Mobile != null)
                employees = employees.Where(e => e.Profile.PhoneMobileNo.Contains(model.Mobile));

            if (model.Phone != null)
                employees = employees.Where(e => e.Profile.PhoneNo.Contains(model.Phone));

            if (model.Fax != null)
                employees = employees.Where(e => e.Profile.FaxNo.Contains(model.Fax));

            if (model.Street != null)
                employees = employees.Where(e => e.Profile.Street.Contains(model.Street));

            if (model.PostalCode != null)
                employees = employees.Where(e => e.Profile.PostalCode.StartsWith(model.PostalCode));

            if(model.PostalCodeMin != null)
                employees = employees.Where(e => string.Compare(model.PostalCodeMin, e.Profile.PostalCode) <= 0);

            if(model.PostalCodeMax != null)
                employees = employees.Where(e => string.Compare(model.PostalCodeMax, e.Profile.PostalCode) >= 0);

            if (model.City != null)
                employees = employees.Where(e => e.Profile.City.Contains(model.City));

            if (model.Country != 0)
                employees = employees.Where(e => e.Profile.Country == model.Country);

            if (model.PersonalDataRemark != null)
                employees = employees.Where(e => e.Profile.PersonalDataComment.Contains(model.PersonalDataRemark));

            if (model.HeightMin.HasValue)
                employees = employees.Where(e => e.Profile.Height >= model.HeightMin);

            if (model.HeightMax.HasValue)
                employees = employees.Where(e => e.Profile.Height <= model.HeightMax);

            if (model.SizeMin.HasValue)
                employees = employees.Where(e => e.Profile.Size >= model.SizeMin);

            if (model.SizeMax.HasValue)
                employees = employees.Where(e => e.Profile.Size <= model.SizeMax);

            if (model.ShirtSize != 0)
                employees = employees.Where(e => e.Profile.ShirtSize == model.ShirtSize);

            if (model.JeansWidth != 0)
                employees = employees.Where(e => e.Profile.JeansWidth == model.JeansWidth);

            if (model.JeansLength != 0)
                employees = employees.Where(e => e.Profile.JeansLength == model.JeansLength);

            if (model.ShoeSize != 0)
                employees = employees.Where(e => e.Profile.ShoeSize == model.ShoeSize);

            if (model.HairColor != 0)
                employees = employees.Where(e => e.Profile.HairColor == model.HairColor);

            if (model.VisibleTattoos.HasValue)
                employees = employees.Where(e => e.Profile.VisibleTattoos == model.VisibleTattoos);

            if (model.FacialPiercing.HasValue)
                employees = employees.Where(e => e.Profile.FacialPiercing == model.FacialPiercing);

            if (model.ArtisticAbilities != 0)
                employees = employees.Where(e => e.Profile.ArtisticAbilities.Contains(";" + model.ArtisticAbilities + ";"));

            if (model.GastronomicAbilities != 0)
                employees = employees.Where(e => e.Profile.GastronomicAbilities.Contains(";" + model.GastronomicAbilities + ";"));

            if (model.Sports != 0)
                employees = employees.Where(e => e.Profile.Sports.Contains(";" + model.Sports + ";"));

            if (model.OtherAbilities != 0)
                employees = employees.Where(e => e.Profile.OtherAbilities.Contains(";" + model.OtherAbilities + ";"));

            if (model.OtherAbilitiesText != null)
                employees = employees.Where(e => e.Profile.OtherAbilitiesText.Contains(model.OtherAbilitiesText));

            if (model.SkeletonContract.HasValue)
                employees = employees.Where(e => e.Profile.SkeletonContract == model.SkeletonContract);

            if (model.CopyTradeLicense.HasValue)
                employees = employees.Where(e => e.Profile.CopyTradeLicense == model.CopyTradeLicense);

            if (model.ValidTradeLicense.HasValue)
                employees = employees.Where(e => e.Profile.ValidTradeLicense == model.ValidTradeLicense);

            if (model.TaxOfficeLocation != null)
                employees = employees.Where(e => e.Profile.TaxOfficeLocation.Contains(model.TaxOfficeLocation));

            if (model.TaxNumber != null)
                employees = employees.Where(e => e.Profile.TaxNumber.Contains(model.TaxNumber));

            if (model.TurnoverTaxDeductible.HasValue)
                employees = employees.Where(e => e.Profile.TurnoverTaxDeductible == model.TurnoverTaxDeductible);

            if (model.FreelancerQuestionaire.HasValue)
                employees = employees.Where(e => e.Profile.FreelancerQuestionaire == model.FreelancerQuestionaire);

            if (model.HealthCertificate.HasValue)
                employees = employees.Where(e => e.Profile.HealthCertificate == model.HealthCertificate);

            if (model.CopyHealthCertificate.HasValue)
                employees = employees.Where(e => e.Profile.CopyHealthCertificate == model.CopyHealthCertificate);

            if (model.CopyIdentityCard.HasValue)
                employees = employees.Where(e => e.Profile.CopyIdentityCard == model.CopyIdentityCard);

            if (model.Comment != null)
                employees = employees.Where(e => e.Profile.Comment.Contains(model.Comment));

            if (model.DriversLicense.HasValue)
                employees = employees.Where(e => e.Profile.DriversLicense == model.DriversLicense);

            if (model.DriversLicenseSince.HasValue)
                employees = employees.Where(e => SqlFunctions.DatePart("year", e.Profile.DriversLicenseSince) >= model.DriversLicenseSince);

            if (model.DriversLicenseClass1 != null)
                employees = employees.Where(e => e.Profile.DriversLicenseClass1.Contains(";" + model.DriversLicenseClass1 + ";"));

            if (model.DriversLicenseClass2 != null)
                employees = employees.Where(e => e.Profile.DriversLicenseClass2.Contains(";" + model.DriversLicenseClass2 + ";"));

            if (model.DriversLicenseRemark != null)
                employees = employees.Where(e => e.Profile.DriversLicenseRemark.Contains(model.DriversLicenseRemark));

            if (model.HasDigitalDriversCard.HasValue)
                employees = employees.Where(e => e.Profile.HasDigitalDriversCard == model.HasDigitalDriversCard);

            if (model.IsCarAvailable.HasValue)
                employees = employees.Where(e => e.Profile.IsCarAvailable == model.IsCarAvailable);

            if (model.IsBahncardAvailable.HasValue)
                employees = employees.Where(e => e.Profile.IsBahncardAvailable == model.IsBahncardAvailable);

            if (model.Education != 0 && model.Education != 5)
                employees = employees.Where(e => e.Profile.Education >= model.Education);

            if (model.Education == 5)
                employees = employees.Where(e => e.Profile.Education == 5);

            if (model.Apprenticeships != null)
                employees = employees.Where(e => e.Profile.Apprenticeships.Contains(";" + model.Apprenticeships + ";"));

            if (model.OtherCompletedApprenticeships != null)
                employees = employees.Where(e => e.Profile.OtherCompletedApprenticeships.Contains(model.OtherCompletedApprenticeships));

            if (model.Studies.HasValue)
                employees = employees.Where(e => e.Profile.Studies == model.Studies);

            if (model.StudiesDetails != null)
                employees = employees.Where(e => e.Profile.StudiesDetails.Contains(model.StudiesDetails));

            if (model.WorkExperience != null)
                employees = employees.Where(e => e.Profile.WorkExperience.Contains(model.WorkExperience));

            if (model.LanguagesGerman > 0)
                employees = employees.Where(e => e.Profile.LanguagesGerman >= model.LanguagesGerman);

            if (model.LanguagesEnglish > 0)
                employees = employees.Where(e => e.Profile.LanguagesEnglish >= model.LanguagesEnglish);

            if (model.LanguagesSpanish > 0)
                employees = employees.Where(e => e.Profile.LanguagesSpanish >= model.LanguagesSpanish);

            if (model.LanguagesFrench > 0)
                employees = employees.Where(e => e.Profile.LanguagesFrench >= model.LanguagesFrench);

            if (model.LanguagesItalian > 0)
                employees = employees.Where(e => e.Profile.LanguagesItalian >= model.LanguagesItalian);

            if (model.LanguagesTurkish > 0)
                employees = employees.Where(e => e.Profile.LanguagesTurkish >= model.LanguagesTurkish);

            if (model.SoftwareKnownledge > 0)
                employees = employees.Where(e => e.Profile.SoftwareKnownledge >= model.SoftwareKnownledge);

            if (model.SoftwareKnownledgeDetails != null)
                employees = employees.Where(e => e.Profile.SoftwareKnownledgeDetails.Contains(model.SoftwareKnownledgeDetails));

            if (model.HardwareKnownledge > 0)
                employees = employees.Where(e => e.Profile.HardwareKnownledge >= model.HardwareKnownledge);

            if (model.HardwareKnownledgeDetails != null)
                employees = employees.Where(e => e.Profile.HardwareKnownledgeDetails.Contains(model.HardwareKnownledgeDetails));

            if (model.InternalTraining != null)
                employees = employees.Where(e => e.Profile.InternalTraining.Contains(model.InternalTraining));

            if (model.ExternalTraining != null)
                employees = employees.Where(e => e.Profile.ExternalTraining.Contains(model.ExternalTraining));

            if (model.TeamleaderQualification.HasValue)
                employees = employees.Where(e => e.Profile.TeamleaderQualification == model.TeamleaderQualification);

            if (model.PreferredTasks != null)
                employees = employees.Where(e => e.Profile.PreferredTasks.Contains(";" + model.PreferredTasks + ";"));

            if (model.PreferredTypes != null)
                employees = employees.Where(e => e.Profile.PreferredTypes.Contains(";" + model.PreferredTypes + ";"));

            if (model.PreferredWorkSchedule != null)
                employees = employees.Where(e => e.Profile.PreferredWorkSchedule.Contains(";" + model.PreferredWorkSchedule + ";"));

            if (model.TravelWillingness.HasValue)
                employees = employees.Where(e => e.Profile.TravelWillingness == model.TravelWillingness);

            if (model.AccommodationPossible1 != null)
                employees = employees.Where(e => e.Profile.AccommodationPossible1.Contains(";" + model.AccommodationPossible1 + ";"));

            if (model.AccommodationPossible2 != null)
                employees = employees.Where(e => e.Profile.AccommodationPossible2.Contains(model.AccommodationPossible2));

            if (model.Project != null)
                employees = employees.Where(e =>
                    e.Profile.ProjectsOldDbSearch.Contains(model.Project) ||
                    // Wir nehmen alle Promoter die beim gesuchten Projekt den Status "im Team" haben.
                    e.CampaignEmployees.Any(i => i.Accepted.HasValue && (i.Campaign.Name.Contains(model.Project) || i.Campaign.Customer.Name.Contains(model.Project))));

            if (model.Training.HasValue)
                employees = employees.Where(e => e.CampaignEmployees.SelectMany(i => i.TrainingParticipants.Select(j => j.TrainingDay.TrainingId)).Any(i => i == model.Training));

            if (model.Radius.HasValue && model.Location != null && model.Lat != null && model.Lng != null)
            {
                var coordinates = DbGeography.FromText(string.Format("POINT({0} {1})", model.Lng.Replace(',', '.'), model.Lat.Replace(',', '.')), 4326);
                var radius = model.Radius*1000;
                employees = employees.Where(e => e.Profile.GeographicCoordinates.Distance(coordinates) <= radius);
            }

            return employees;
        }

        public IQueryable<Location> Locations(IQueryable<Location> locations, LocationSearchModel model)
        {
            if (model.State.HasValue)
                locations = locations.Where(e => e.State == model.State);

            if (model.LocationTypeId.HasValue)
                locations = locations.Where(e => e.LocationTypeId == model.LocationTypeId);

            if (model.Name != null)
                locations = locations.Where(e => e.Name.Contains(model.Name));

            if (model.Name2 != null)
                locations = locations.Where(e => e.Name2.Contains(model.Name2));

            if (model.Street != null)
                locations = locations.Where(e => e.Street.Contains(model.Street));

            if (model.Street2 != null)
                locations = locations.Where(e => e.Name2.Contains(model.Street2));

            if (model.PostalCode != null)
                locations = locations.Where(e => e.PostalCode.StartsWith(model.PostalCode));

            if(model.PostalCodeMin != null)
                locations = locations.Where(e => string.Compare(model.PostalCodeMin, e.PostalCode) <= 0);

            if(model.PostalCodeMax != null)
                locations = locations.Where(e => string.Compare(model.PostalCodeMax, e.PostalCode) >= 0);

            if (model.City != null)
                locations = locations.Where(e => e.City.StartsWith(model.City));

            if(model.StateProvinceId.HasValue)
                locations = locations.Where(e => e.StateProvinceId == model.StateProvinceId);

            if(model.LocationGroupId.HasValue)
                locations = locations.Where(e => e.LocationGroupId == model.LocationGroupId);

            if (model.Notes!= null)
                locations = locations.Where(e => e.Notes.StartsWith(model.Notes));

            if (model.CenterManagementStreet != null)
                locations = locations.Where(e => e.CenterManagementStreet.StartsWith(model.CenterManagementStreet));

            if (model.CenterManagementPostalCode != null)
                locations = locations.Where(e => e.CenterManagementPostalCode.StartsWith(model.CenterManagementPostalCode));

            if (model.CenterManagementCity != null)
                locations = locations.Where(e => e.CenterManagementCity.StartsWith(model.CenterManagementCity));

            if (model.Phone != null)
                locations = locations.Where(e => e.Phone.StartsWith(model.Phone));

            if (model.Fax != null)
                locations = locations.Where(e => e.Fax.StartsWith(model.Fax));

            if (model.Email != null)
                locations = locations.Where(e => e.Email.StartsWith(model.Email));

            if (model.Website != null)
                locations = locations.Where(e => e.Website.StartsWith(model.Website));

            if(model.VisitorFrequency.HasValue)
                locations = locations.Where(e => e.VisitorFrequency == model.VisitorFrequency);

            if(model.Area.HasValue)
                locations = locations.Where(e => e.Area == model.Area);

            if(model.SellingArea.HasValue)
                locations = locations.Where(e => e.SellingArea == model.SellingArea);

            if (model.CampaignAreaPrices != null)
                locations = locations.Where(e => e.CampaignAreaPrices.StartsWith(model.CampaignAreaPrices));

            if(model.Audience.HasValue)
                locations = locations.Where(e => e.Audience == model.Audience);

            if(model.Opening.HasValue)
                locations = locations.Where(e => e.Opening == model.Opening);

            if(model.CatchmentArea.HasValue)
                locations = locations.Where(e => e.CatchmentArea == model.CatchmentArea);

            if(model.Parking.HasValue)
                locations = locations.Where(e => e.Parking == model.Parking);

            if(model.OutdoorArea.HasValue)
                locations = locations.Where(e => e.OutdoorArea == model.OutdoorArea);

            if (model.Retailers != null)
                locations = locations.Where(e => e.Retailers.StartsWith(model.Retailers));
            
            if(model.WiFiAvailable.HasValue)
                locations = locations.Where(e => e.WiFiAvailable == model.WiFiAvailable);

            if (model.ConferenceRoomSize != null)
                locations = locations.Where(e => e.ConferenceRoomSize.StartsWith(model.ConferenceRoomSize));

            if(model.CostTransferPossible.HasValue)
                locations = locations.Where(e => e.CostTransferPossible == model.CostTransferPossible);

            if (model.RoomRates != null)
                locations = locations.Where(e => e.RoomRates.StartsWith(model.RoomRates));

            if (model.ConferenceEquipmentCost != null)
                locations = locations.Where(e => e.ConferenceEquipmentCost.StartsWith(model.ConferenceEquipmentCost));

            if (model.ConferencePackageCost != null)
                locations = locations.Where(e => e.ConferencePackageCost.StartsWith(model.ConferencePackageCost));

            if (model.Radius.HasValue && model.Location != null && model.Lat != null && model.Lng != null)
            {
                var coordinates = DbGeography.FromText(string.Format("POINT({0} {1})", model.Lng.Replace(',', '.'), model.Lat.Replace(',', '.')), 4326);
                var radius = model.Radius*1000;
                locations = locations.Where(e => e.GeographicCoordinates.Distance(coordinates) <= radius);
            }

            return locations;
        }
    }
}