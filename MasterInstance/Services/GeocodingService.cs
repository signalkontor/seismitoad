﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Services
{
    public class GeocodingService
    {
        public static void Start()
        {
            if (MvcApplication.IsStaging == false)
            {
                RecurringJob.AddOrUpdate("employee-coordinates-service", () => UpdateEmployeeCoordinates(), "00,15,30,45 * * * *");
                RecurringJob.AddOrUpdate("location-coordinates-service", () => UpdateLocationCoordinates(), "10,40 * * * *");
            }
        }

        public static void UpdateEmployeeCoordinates()
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                dbContext.Configuration.ValidateOnSaveEnabled = false;
                var employeeProfiles = dbContext.EmployeeProfiles
                    .Where(e => e.GeographicCoordinates == null && !e.GeocodingFailed)
                    .Take(10)
                    .ToList();
                foreach (var employeeProfile in employeeProfiles)
                {
                    employeeProfile.UpdateCoordinates();
                }
                dbContext.SaveChanges();
            }
        }

        public static void UpdateLocationCoordinates()
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                dbContext.Configuration.ValidateOnSaveEnabled = false;
                var locations = dbContext.Locations
                    .Where(e => e.GeographicCoordinates == null && !e.GeocodingFailed)
                    .Take(10)
                    .ToList();
                foreach (var location in locations)
                {
                    location.UpdateCoordinates();
                }
                dbContext.SaveChanges();
            }
        }

    }
}