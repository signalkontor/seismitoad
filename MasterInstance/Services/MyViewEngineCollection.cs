﻿// Type: System.Web.Mvc.ViewEngineCollection
// Assembly: System.Web.Mvc, Version=5.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// MVID: DD2244B2-962E-4A2B-958D-8AA9EC717FF2
// Assembly location: C:\Projekte\pm\seismitoad\packages\Microsoft.AspNet.Mvc.5.1.2\lib\net45\System.Web.Mvc.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Properties;

namespace MasterInstance.Services
{
    /// <summary>
    /// Represents a collection of view engines that are available to the application.
    /// </summary>
    public class MyViewEngineCollection : ViewEngineCollection 
    {
        private IViewEngine[] _combinedItems;
        private IDependencyResolver _dependencyResolver;

        internal IViewEngine[] CombinedItems
        {
            get
            {
                IViewEngine[] viewEngineArray = this._combinedItems;
                if (viewEngineArray == null)
                {
                    viewEngineArray = this.Items.ToArray();
                    this._combinedItems = viewEngineArray;
                }
                return viewEngineArray;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MasterInstance.Services.ViewEngineCollection"/> class.
        /// </summary>
        public MyViewEngineCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MasterInstance.Services.ViewEngineCollection"/> class using the specified list of view engines.
        /// </summary>
        /// <param name="list">The list that is wrapped by the new collection.</param><exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
        public MyViewEngineCollection(IList<IViewEngine> list)
            : base(list)
        {
        }

        public MyViewEngineCollection(IList<IViewEngine> list, IDependencyResolver dependencyResolver)
            : base(list)
        {
            this._dependencyResolver = dependencyResolver;
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1"/>.
        /// </summary>
        protected override void ClearItems()
        {
            this._combinedItems = (IViewEngine[])null;
            base.ClearItems();
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param><param name="item">The object to insert.</param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="index"/> is less than zero.-or-<paramref name="index"/> is greater than the number of items in the collection.</exception><exception cref="T:System.ArgumentNullException">The <paramref name="item"/> parameter is null.</exception>
        protected override void InsertItem(int index, IViewEngine item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            this._combinedItems = (IViewEngine[])null;
            base.InsertItem(index, item);
        }

        /// <summary>
        /// Removes the element at the specified index of the <see cref="T:System.Collections.ObjectModel.Collection`1"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="index"/> is less than zero.-or-<paramref name="index"/> is equal to or greater than <see cref="T:System.Collections.ObjectModel.Collection`1.Count"/></exception>
        protected override void RemoveItem(int index)
        {
            this._combinedItems = (IViewEngine[])null;
            base.RemoveItem(index);
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param><param name="item">The new value for the element at the specified index.</param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="index"/> is less than zero.-or-<paramref name="index"/> is greater than the number of items in the collection.</exception><exception cref="T:System.ArgumentNullException">The <paramref name="item"/> parameter is null.</exception>
        protected override void SetItem(int index, IViewEngine item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            this._combinedItems = (IViewEngine[])null;
            base.SetItem(index, item);
        }

        private ViewEngineResult Find(Func<IViewEngine, ViewEngineResult> cacheLocator, Func<IViewEngine, ViewEngineResult> locator)
        {
            return this.Find(cacheLocator, false) ?? this.Find(locator, true);
        }

        private ViewEngineResult Find(Func<IViewEngine, ViewEngineResult> lookup, bool trackSearchedPaths)
        {
            List<string> list = (List<string>)null;
            if (trackSearchedPaths)
                list = new List<string>();
            foreach (IViewEngine viewEngine in this.CombinedItems)
            {
                if (viewEngine != null)
                {
                    ViewEngineResult viewEngineResult = lookup(viewEngine);
                    if (viewEngineResult.View != null)
                        return viewEngineResult;
                    if (trackSearchedPaths)
                        list.AddRange(viewEngineResult.SearchedLocations);
                }
            }
            if (trackSearchedPaths)
                return new ViewEngineResult((IEnumerable<string>)Enumerable.ToList<string>(Enumerable.Distinct<string>((IEnumerable<string>)list)));
            else
                return (ViewEngineResult)null;
        }

        /// <summary>
        /// Finds the specified partial view by using the specified controller context.
        /// </summary>
        /// 
        /// <returns>
        /// The partial view.
        /// </returns>
        /// <param name="controllerContext">The controller context.</param><param name="partialViewName">The name of the partial view.</param><exception cref="T:System.ArgumentNullException">The <paramref name="controllerContext"/> parameter is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="partialViewName"/> parameter is null or empty.</exception>
        public virtual ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentException("MvcResources.Common_NullOrEmpty", "partialViewName");
            else
                return this.Find((Func<IViewEngine, ViewEngineResult>)(e => e.FindPartialView(controllerContext, partialViewName, true)), (Func<IViewEngine, ViewEngineResult>)(e => e.FindPartialView(controllerContext, partialViewName, false)));
        }

        /// <summary>
        /// Finds the specified view by using the specified controller context and master view.
        /// </summary>
        /// 
        /// <returns>
        /// The view.
        /// </returns>
        /// <param name="controllerContext">The controller context.</param><param name="viewName">The name of the view.</param><param name="masterName">The name of the master view.</param><exception cref="T:System.ArgumentNullException">The <paramref name="controllerContext"/> parameter is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="partialViewName"/> parameter is null or empty.</exception>
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (string.IsNullOrEmpty(viewName))
                throw new ArgumentException("MvcResources.Common_NullOrEmpty", "viewName");
            else
                return this.Find((Func<IViewEngine, ViewEngineResult>)(e => e.FindView(controllerContext, viewName, masterName, true)), (Func<IViewEngine, ViewEngineResult>)(e => e.FindView(controllerContext, viewName, masterName, false)));
        }
    }
}
