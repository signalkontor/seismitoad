﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Elmah;
using Hangfire;
using MasterInstance.Services.EmailReceiver;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Models;

namespace MasterInstance.Services
{
    public class FaxReceptionService
    {
        private static readonly Regex ExtractProjectNumber = new Regex("'[0-9]{4}'");

        public static void Start()
        {
            if (MvcApplication.IsStaging)
                return; // Stage die Mails nicht abrufen abrufen

            RecurringJob.AddOrUpdate("fax-receiver", () => ReceiveFaxes(), "*/10 * * * *");
        }

        public static void ReceiveFaxes()
        {
            var emailreceiver = new RebexEmailReceiver();
            emailreceiver.GetNewMessages(emailMessage =>
            {
                // HINWEIS: Der Rückgabewert dieser Funktion gibt an, ob wir mit dieser E-Mail erfolgreich abarbeiten konnten.
                // Eigentlich sollte der Rückgabewert immer true sein, da die E-Mail sonst nicht auf dem Server gelöscht wird
                // und man dann beim nächsten mal ja wieder das gleiche Problem hat. Das heißt nur bei temporäreren Problemen
                // DB nicht erreichbar oder so, macht es sinn false zurückzugeben.
                var faxMessage = new FaxMessage
                {
                    Body = emailMessage.Body,
                    Subject = emailMessage.Subject,
                    Sender = emailMessage.Sender,
                    AttachementFile = emailMessage.AttachmendPath,
                    Received = emailMessage.ReceiveDate,
                    State = FaxMessageState.ErrorAssignmentNotFound
                };

                using (var dbContext = new SeismitoadDbContext())
                {
                    dbContext.FaxMessages.Add(faxMessage);
                    var match = ExtractProjectNumber.Match(emailMessage.Subject);
                    if (!match.Success)
                    {
                        dbContext.SaveChanges();
                        return true;
                    }

                    var faxData = ParseEmailSubject(emailMessage.Subject, dbContext);
                    if (faxData == null)
                    {
                        faxMessage.State = FaxMessageState.ErrorMessageFormat;
                        dbContext.SaveChanges();
                        return true;
                    }

                    var receiveDate = emailMessage.ReceiveDate;
                    var assignmentsForDate = dbContext.Assignments
                        .Where(a =>
                            a.CampaignLocation.CampaignId == faxData.CampaignId &&
                            a.DateStart.Day == receiveDate.Day &&
                            a.DateStart.Month == receiveDate.Month &&
                            a.DateStart.Year == receiveDate.Year &&
                            a.State == AssignmentState.Planned &&
                            a.AssignedEmployees.Any())
                        .Include(e => e.CampaignLocation.Location)
                        .Include(e => e.AssignedEmployees);

                    if (string.IsNullOrWhiteSpace(faxData.FaxNumber))
                        return true;

                    var exactMatches = assignmentsForDate.Where(
                        e => e.CampaignLocation.Location.Fax.Replace(" ", "").Replace("-", "") == faxData.FaxNumber)
                        .ToList();

                    var exactMatchesCount = exactMatches.Count();
                    if (exactMatchesCount > 1)
                    {
                        // Es gibt mehr als einen Treffer => da können wir nichts sinnvolles mehr machen
                        dbContext.SaveChanges();
                        return true;
                    }

                    var assignment = exactMatches.SingleOrDefault();

                    // Falls wir keinen exakten Treffer haben, lassen wir bis zu 3 Stellen der Telefonnummer (vom Ende) weg,
                    // bis wir einen Treffer haben. Haben wir dann immer noch keinen Treffer beenden wir die Suche.
                    // Sobald wir mehr als einen Treffer haben, beenden wir die Suche auch, da wir dann keine
                    // eindeutige Zuordnung mehr vornehmen können.
                    for (var i = 1; assignment == null && i <= 3 ; i++)
                    {
                        if (faxData.FaxNumber.Length - i <= 6)
                            break; // Wir wollen mindestens 6 Stellen zum vergleichen

                        var faxNumber = faxData.FaxNumber.Substring(0, faxData.FaxNumber.Length - i);

                        var assignments =
                            assignmentsForDate.Where(a => a.CampaignLocation.Location.Fax
                                .Replace(" ", "")
                                .Replace("-", "").StartsWith(faxNumber));

                        var noOfMatches = assignments.Count();
                        if (noOfMatches > 1)
                            break; // Mehr als ein Treffer. Keine eindeutige Zuordnung möglich => Abbruch

                        assignment = assignments.FirstOrDefault();
                    }

                    if (assignment != null && assignment.AssignedEmployees != null)
                    {
                        var attendance = assignment.DateStart.AddMinutes(15) < receiveDate
                            ? Attendance.Late
                            : Attendance.InTime;

                        foreach (var assignedEmployee in assignment.AssignedEmployees.ToList())
                        {
                            assignedEmployee.AttendanceSetDate = receiveDate;
                            assignedEmployee.Attendance = attendance;
                            assignedEmployee.SetAttendanceSetBy("Fax");
                        }

                        faxMessage.Assignment = assignment;
                        faxMessage.State = FaxMessageState.Processed;
                        faxMessage.Received = emailMessage.ReceiveDate;
                    }
                    dbContext.SaveChanges();
                    return true;
                }
            });
        }

        private static FaxData ParseEmailSubject(string subject, SeismitoadDbContext dbContext)
        {
            // Subject sieht aus wie:
            // Fax von '+4921161854129' für '4711'
            try
            {
                var startFax = subject.IndexOf('\'');
                var endFax = subject.IndexOf('\'', startFax + 1);
                var faxNumber = subject.Substring(startFax + 1, endFax - startFax - 1);

                // Wir wandeln alles auf +49 ... um
                if (faxNumber.StartsWith("00"))
                    faxNumber = "+" + faxNumber.Substring(2);

                if (faxNumber.StartsWith("0"))
                    faxNumber = "+49" + faxNumber.Substring(1);

                // wir entsorgen noch alle sonderzeichen, soweit bekannt:
                faxNumber = faxNumber.Replace(" ", "").Replace("-", "").Replace("/", "").Replace(".", "");

                // Nun parsen wir den Empfänger:
                // Das sollte in Zukunft die Projektnummer sein (nicht mehr nur ein Kundennamen)
                var startProject = subject.IndexOf('\'', endFax + 1);
                var endProject = subject.IndexOf('\'', startProject + 1);
                var projectNumber = subject.Substring(startProject + 1, endProject - startProject - 1);

                if (projectNumber.Length != 4) return null;

                var customerNumber = projectNumber.Substring(0, 2);
                var campaignNumber = projectNumber.Substring(2, 2);

                var campaignId = dbContext.Campaigns
                    .Where(c => c.Number == campaignNumber && c.Customer.Number == customerNumber)
                    .Select(e => (int?)e.Id)
                    .SingleOrDefault();

                return campaignId.HasValue ? new FaxData { FaxNumber = faxNumber, CampaignId = campaignId.Value } : null;
            }
            catch (Exception ex)
            {
                ErrorLog.GetDefault(null).Log(new Error(new Exception(subject)));
                ErrorLog.GetDefault(null).Log(new Error(ex));
            }
            return null;
        }
    }
}