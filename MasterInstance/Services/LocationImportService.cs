﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SeismitoadModel;
using ImportFramework;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared.Extensions;

namespace MasterInstance.Services
{
    public class LocationImportService
    {
        private readonly string _filename;

        public LocationImportService(string filename)
        {
            _filename = filename;
        }

        public IEnumerable<ImportLine<Location>> Import(string encoding)
        {
            if (ImportFramework.Import.From(_filename, ";", encoding).Skip(1).Take(1).Any(e => e.Field[0] != "Id" || e.Field[1] != "ExternalId" || e.Field[2] != "Type" || e.Field[3] != "Name" || e.Field[14] != "Website"))
                throw new ArgumentException("Invalid file: " + Path.GetFileName(_filename));

            var lineNumber = 3;
            using (var context = new SeismitoadDbContext())
            {
                return ImportFramework.Import
                    .From(_filename, ";", encoding)
                    .Skip(lineNumber)
                    .Where(r => !string.IsNullOrWhiteSpace(r.Field[3]))
                    .Select(r => ToImportLine(r, ++lineNumber, context))
                    .ToList();
            }
        }

        private static Contact ImportContact(Record r, ref int col)
        {
            var contact = new Contact();
            try
            {
                contact.Firstname = r.Field[col++];
                contact.Lastname = r.Field[col++];
                contact.Phone = r.Field[col++];
                contact.Fax = r.Field[col++];
                contact.Email = r.Field[col++];
                contact.Type = r.Field[col++];
                contact.Title = r.Field[col++];
            }
            catch(KeyNotFoundException)
            {
            }
            return (string.IsNullOrWhiteSpace(contact.Lastname) && string.IsNullOrWhiteSpace(contact.Email)) ? null : contact;
        }

        private static ImportLine<Location> ToImportLine(Record r, int lineNumber, SeismitoadDbContext context)
        {
            var line = new ImportLine<Location> { LineNumber = lineNumber };

            try
            {
                var locationGroupName = r.Field[9];
                var locationGroup = context.LocationGroups.SingleOrDefault(e => e.Name == locationGroupName);
                var externalId = r.Field[1].AsNullableInt();
                if(externalId.HasValue)
                {
                    line.ImportAction = context.Locations.Any(e => e.ExternalId == externalId)
                                            ? ImportAction.Update
                                            : ImportAction.Create;
                }
                else
                {
                    line.Message = "Keine oder ungültige externe Id!";
                }
                line.Entity = new Location
                {
                    ExternalId = externalId,
                    LocationTypeId = null,
                    Name = r.Field[3],
                    Name2 = r.Field[4],
                    Street = r.Field[5],
                    PostalCode = r.Field[6],
                    City = r.Field[7],
                    StateProvinceId = 17,
                    LocationGroupId = locationGroup != null ? locationGroup.Id : (int?)null,
                    Notes = r.Field[10],
                    Phone = r.Field[11],
                    Fax = r.Field[12],
                    Email = r.Field[13],
                    Website = r.Field[14],
                    CampaignAreaPrices = r.Field[15],
                    OutdoorArea = r.Field[16].AsValueOrDefault<bool?>(),
                    Retailers = r.Field[17],
                    WiFiAvailable = r.Field[18].AsValueOrDefault<bool?>(),
                    ConferenceRoomSize = r.Field[19],
                    CostTransferPossible = r.Field[20].AsValueOrDefault<bool?>(),
                    RoomRates = r.Field[21],
                    ConferenceEquipmentCost = r.Field[22],
                    ConferencePackageCost = r.Field[23],
                    Contacts = new Collection<Contact>(),
                    Area = r.Field[52].AsValueOrDefault<int>(),
                    Opening = r.Field[53].AsValueOrDefault<int>(),
                    CatchmentArea = r.Field[54].AsValueOrDefault<int>(),
                    Parking = r.Field[55].AsValueOrDefault<int>(),
                    CenterManagementStreet = r.Field[56],
                    CenterManagementPostalCode = r.Field[57],
                    CenterManagementCity = r.Field[58],
                    VisitorFrequency = r.Field[59].AsValueOrDefault<int>(),
                    SellingArea = r.Field[60].AsValueOrDefault<int>(),
                    // TODO (?) Das ist falsch, da Audience auch NULL sein kann.
                    // As ValueOrDefault mappt NULL aber nicht auf NULL, sondern auf 0 und das ist kein erlaubter Wert für den Enum Audience!
                    //Audience = r.Field[61].AsValueOrDefault<int>(),
                };


                var col = 62;
                while(r.Field.ContainsKey(col))
                {
                    var contact = ImportContact(r, ref col);
                    if(contact != null) line.Entity.Contacts.Add(contact);
                }
            }
            catch (Exception e)
            {
                line.Message = line.Message + " ### Ausnahme: " + e.Message;
            }
            return line;
        }

        public static void Commit(IEnumerable<ImportLine<Location>> importLines)
        {
            using(var context = new SeismitoadDbContext())
            {
                foreach (var importLine in importLines)
                {
                    if (importLine.ImportAction == ImportAction.Create)
                    {
                        context.Locations.Add(importLine.Entity);
                    }
                    else
                    {
                        var s = importLine.Entity;
                        var d = context.Locations.Single(e => e.ExternalId == importLine.Entity.ExternalId);
                        d.Area = s.Area;
                        d.Audience = s.Audience;
                        d.CampaignAreaPrices = s.CampaignAreaPrices;
                        d.CatchmentArea = s.CatchmentArea;
                        d.CenterManagementCity = s.CenterManagementCity;
                        d.CenterManagementPostalCode = s.CenterManagementPostalCode;
                        d.CenterManagementStreet = s.CenterManagementStreet;
                        d.City = s.City;
                        d.ConferenceEquipmentCost = s.ConferenceEquipmentCost;
                        d.ConferencePackageCost = s.ConferencePackageCost;
                        d.ConferenceRoomSize = s.ConferenceRoomSize;
                        d.CostTransferPossible = s.CostTransferPossible;
                        d.Email = s.Email;
                        d.ExternalId = s.ExternalId;
                        d.Fax = s.Fax;
                        d.LocationGroupId = s.LocationGroupId;
                        d.Name = s.Name;
                        d.Name2 = s.Name2;
                        d.Notes = s.Notes;
                        d.Opening = s.Opening;

                        d.OpeningHours.MondayStart = s.OpeningHours.MondayStart;
                        d.OpeningHours.MondayEnd = s.OpeningHours.MondayEnd;
                        d.OpeningHours.TuesdayStart = s.OpeningHours.TuesdayStart;
                        d.OpeningHours.TuesdayEnd = s.OpeningHours.TuesdayEnd;
                        d.OpeningHours.WednesdayStart = s.OpeningHours.WednesdayStart;
                        d.OpeningHours.WednesdayEnd = s.OpeningHours.WednesdayEnd;
                        d.OpeningHours.ThursdayStart = s.OpeningHours.ThursdayStart;
                        d.OpeningHours.ThursdayEnd= s.OpeningHours.ThursdayEnd;
                        d.OpeningHours.FridayStart = s.OpeningHours.FridayStart;
                        d.OpeningHours.FridayEnd = s.OpeningHours.FridayEnd;
                        d.OpeningHours.SaturdayStart = s.OpeningHours.SaturdayStart;
                        d.OpeningHours.SaturdayEnd = s.OpeningHours.SaturdayEnd;
                        d.OpeningHours.SundayStart = s.OpeningHours.SundayStart;
                        d.OpeningHours.SundayEnd = s.OpeningHours.SundayEnd;

                        d.OpeningHoursIncomingGoodsDepartment.MondayStart = s.OpeningHoursIncomingGoodsDepartment.MondayStart;
                        d.OpeningHoursIncomingGoodsDepartment.MondayEnd = s.OpeningHoursIncomingGoodsDepartment.MondayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.TuesdayStart = s.OpeningHoursIncomingGoodsDepartment.TuesdayStart;
                        d.OpeningHoursIncomingGoodsDepartment.TuesdayEnd = s.OpeningHoursIncomingGoodsDepartment.TuesdayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.WednesdayStart = s.OpeningHoursIncomingGoodsDepartment.WednesdayStart;
                        d.OpeningHoursIncomingGoodsDepartment.WednesdayEnd = s.OpeningHoursIncomingGoodsDepartment.WednesdayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.ThursdayStart = s.OpeningHoursIncomingGoodsDepartment.ThursdayStart;
                        d.OpeningHoursIncomingGoodsDepartment.ThursdayEnd = s.OpeningHoursIncomingGoodsDepartment.ThursdayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.FridayStart = s.OpeningHoursIncomingGoodsDepartment.FridayStart;
                        d.OpeningHoursIncomingGoodsDepartment.FridayEnd = s.OpeningHoursIncomingGoodsDepartment.FridayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.SaturdayStart = s.OpeningHoursIncomingGoodsDepartment.SaturdayStart;
                        d.OpeningHoursIncomingGoodsDepartment.SaturdayEnd = s.OpeningHoursIncomingGoodsDepartment.SaturdayEnd;
                        d.OpeningHoursIncomingGoodsDepartment.SundayStart = s.OpeningHoursIncomingGoodsDepartment.SundayStart;
                        d.OpeningHoursIncomingGoodsDepartment.SundayEnd = s.OpeningHoursIncomingGoodsDepartment.SundayEnd;

                        d.OutdoorArea = s.OutdoorArea;
                        d.Parking = s.Parking;
                        d.Phone = s.Phone;
                        d.PostalCode = s.PostalCode;
                        d.Retailers = s.Retailers;
                        d.RoomRates = s.RoomRates;
                        d.SellingArea = s.SellingArea;
                        d.StateProvinceId = s.StateProvinceId;
                        d.Street = s.Street;
                        d.LocationTypeId = s.LocationTypeId;
                        d.VisitorFrequency = s.VisitorFrequency;
                        d.Website = s.Website;
                        d.WiFiAvailable = s.WiFiAvailable;
                        foreach(var contact in s.Contacts)
                        {
                            d.Contacts.Add(contact);
                        }
                    }
                }

                // Validierung deaktivieren, da wir möglicherweise unvollständige Daten importieren
                context.Configuration.ValidateOnSaveEnabled = false;
                context.SaveChanges();
            }
        }
    }

    public class ImportLine<T>
    {
        [Display(Name = "Zeile")]
        public int LineNumber { get; set; }
        public T Entity { get; set; }

        [Display(Name = "Status")]
        public ImportAction ImportAction { get; set; }

        [Display(Name = "Fehler")]
        public string Message { get; set; }
    }

    public enum ImportAction
    {
        [Display(Name = "Neu")]
        Create = 0,

        [Display(Name = "Update")]
        Update = 1
    }

}