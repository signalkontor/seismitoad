﻿using SeismitoadModel;

namespace MasterInstance.Services
{
    public class EmployeeSearchModel
    {
        #region A. Persönliche Daten
        public int CampaignId { get; set; }
        public EmployeeState? State { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int? AgeMin { get; set; }
        public int? AgeMax { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public string Website { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Street { get; set; }
        public string Pc1Color { get; set; }
        public string Pc2Color { get; set; }
        public bool Pc1Visible { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodeMin { get; set; }
        public string PostalCodeMax { get; set; }
        public string City { get; set; }
        public int Country { get; set; }
        public string PersonalDataRemark { get; set; }
        #endregion

        #region B. Body Profil und Talente
        public int? HeightMin { get; set; }
        public int? HeightMax { get; set; }
        public int? SizeMin { get; set; }
        public int? SizeMax { get; set; }
        public int ShirtSize { get; set; }
        public int JeansWidth { get; set; }
        public int JeansLength { get; set; }
        public int ShoeSize { get; set; }
        public int HairColor { get; set; }
        public bool? VisibleTattoos { get; set; }
        public bool? FacialPiercing { get; set; }
        public int ArtisticAbilities { get; set; }
        public int GastronomicAbilities { get; set; }
        public int Sports { get; set; }
        public int OtherAbilities { get; set; }
        public string OtherAbilitiesText { get; set; }
        #endregion

        #region C. Vertrag und Gewerbe
        public bool? SkeletonContract { get; set; }
        public bool? CopyTradeLicense { get; set; }
        public bool? ValidTradeLicense { get; set; }
        public string TaxOfficeLocation { get; set; }
        public string TaxNumber { get; set; }
        public bool? TurnoverTaxDeductible { get; set; }
        public bool? FreelancerQuestionaire { get; set; }
        public bool? HealthCertificate { get; set; }
        public bool? CopyHealthCertificate { get; set; }
        public int? CopyIdentityCard { get; set; }
        public string Comment { get; set; }
        #endregion

        #region D. Mobilität
        public bool? DriversLicense { get; set; }
        public int? DriversLicenseSince { get; set; }
        public string DriversLicenseClass1 { get; set; }
        public string DriversLicenseClass2 { get; set; }
        public string DriversLicenseRemark { get; set; }
        public bool? HasDigitalDriversCard { get; set; }
        public bool? IsCarAvailable { get; set;  }
        public bool? IsBahncardAvailable { get; set; }
        #endregion

        #region E. Ausbildung und Qualifikation
        public int Education { get; set; }
        public string Apprenticeships { get; set; }
        public string OtherCompletedApprenticeships { get; set; }
        public bool? Studies { get; set; }
        public string StudiesDetails { get; set; }
        public string WorkExperience { get; set; }
        public int LanguagesGerman { get; set; }
        public int LanguagesEnglish { get; set; }
        public int LanguagesSpanish { get; set; }
        public int LanguagesFrench { get; set; }
        public int LanguagesItalian { get; set; }
        public int LanguagesTurkish { get; set; }
        public string LanguagesOther { get; set; }
        public int SoftwareKnownledge { get; set; }
        public string SoftwareKnownledgeDetails { get; set; }
        public int HardwareKnownledge { get; set; }
        public string HardwareKnownledgeDetails { get; set; }
        public string InternalTraining { get; set; }
        public string ExternalTraining { get; set; }
        public bool? TeamleaderQualification { get; set; }
        #endregion

        #region Auftragswunsch
        public string PreferredTasks { get; set; }
        public string PreferredTypes { get; set; }
        public string PreferredWorkSchedule { get; set; }
        public bool? TravelWillingness { get; set; }
        public string AccommodationPossible1 { get; set; }
        public string AccommodationPossible2 { get; set; }
        #endregion

        #region Projekte / Standort
        public string Project { get; set; }
        public int? Training { get; set; }
        public string Location { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
        public int? Radius { get; set; }
        #endregion
    }
}