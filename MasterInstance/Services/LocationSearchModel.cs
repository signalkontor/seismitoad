﻿using MasterInstance.Areas.MD.Models;

namespace MasterInstance.Services
{
    public class LocationSearchModel : LocationViewModel
    {
        public string Location { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
        public int? Radius { get; set; }
    }
}