﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Elmah;
using Hangfire;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;

namespace MasterInstance.Services
{
    public class ProfileUploadsService
    {
        public static void Start()
        {
            if (MvcApplication.IsStaging)
                return;

            RecurringJob.AddOrUpdate("clean-deleted-profiles", () => CleanUploads(), "12 1 * * *");
        }

        public static void CleanUploads()
        {
            var basePath = HostingEnvironment.MapPath("~/PromoterUploads/Employee");
            var date1 = new DateTime(1800, 01, 02);
            var date2 = new DateTime(1800, 01, 03);
            using (var dbContext = new SeismitoadDbContext())
            {
                var employees = dbContext.Employees
                    .Where(e => e.State == EmployeeState.Deleted && date1 <= e.Profile.Birthday && e.Profile.Birthday < date2)
                    .Take(50);

                foreach (var employee in employees)
                {
                    employee.Profile.Birthday = new DateTime(1800, 01, 01);
                    var path = Path.Combine(basePath, "" + employee.Id);
                    if(Directory.Exists(path))
                    {
                        Directory.Delete(path, true);
                    }
                }
                dbContext.SaveChanges();
            }
        }
    }
}