﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MasterInstance.Services
{
    public static class MembershipService
    {
        /// <summary>
        /// Ändert den Benutzernamen
        /// </summary>
        /// <param name="providerUserKey">UserId des Users</param>
        /// <param name="newUserName">Der neue Benutzername</param>
        /// <returns>true, wenn der Name geändert werden konnte, sonst false.</returns>
        public static bool ChangeUsername(Guid providerUserKey, string newUserName)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString))
            {
                sqlConnection.Open();
                using (var command = new SqlCommand("SELECT count(*) FROM aspnet_Users WHERE LoweredUserName = @LoweredUserName AND UserId <> @UserId", sqlConnection))
                {
                    command.Parameters.Add(new SqlParameter("@LoweredUserName", newUserName.ToLower()));
                    command.Parameters.Add(new SqlParameter("@UserId", providerUserKey));
                    var rowsAffected = (int)command.ExecuteScalar();
                    if(rowsAffected > 0)
                        return false;
                }

                using (var command = new SqlCommand("UPDATE aspnet_Users SET UserName = @UserName, LoweredUserName = @LoweredUserName WHERE UserId = @UserId", sqlConnection))
                {
                    command.Parameters.Add(new SqlParameter("@UserName", newUserName));
                    command.Parameters.Add(new SqlParameter("@LoweredUserName", newUserName.ToLower()));
                    command.Parameters.Add(new SqlParameter("@UserId", providerUserKey));
                    var rowsAffected = command.ExecuteNonQuery();
                    return rowsAffected == 1;
                }
            }
        }
    }
}