﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using Elmah;
using Hangfire;
using MasterInstance.Models.ViewModels;
using SeismitoadModel;
using SeismitoadModel.DatabaseContext;
using SeismitoadShared;

namespace MasterInstance.Services
{
    public class DeferredMails
    {
        // Die selben Mails wie hier werden auch in der PromoterInstance in Controllers\MailController.cs gesendet.
        // Änderungen am Betreff oder Inhalt müssen daher immer an zwei Stellen gemacht werden.
        public static void Start()
        {
            RecurringJob.AddOrUpdate("deferred-mails-service", () => Send(), "*/5 * * * *");
        }

        public static void Send()
        {
            using (var dbContext = new SeismitoadDbContext())
            {
                // Auf atomare weise löschen wir alle DeferredMails die älter als 5 Minuten sind und holen uns das Ergebnis
                var allDeferredMails = dbContext.Database.SqlQuery<DeferredMail>(@"
UPDATE DeferredMails
SET InProgress = 1
OUTPUT INSERTED.Id, INSERTED.EmployeeId, INSERTED.Type, INSERTED.AssignmentIds
WHERE LastUpdate < DATEADD(MINUTE, -5, GETDATE())").ToList();

                foreach (var mail in allDeferredMails)
                {
                    var assignmentIds = mail.AssignmentIds
                        .Split(new[] {',' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(e => Convert.ToInt32(e))
                        .ToArray();

                    SendInternal(dbContext, mail.EmployeeId, assignmentIds, mail.Type);

                    dbContext.Database.ExecuteSqlCommand("DELETE FROM DeferredMails WHERE Id = @Id",
                        new SqlParameter("@Id", mail.Id));
                }
            }
        }

        private static void SendInternal(SeismitoadDbContext dbContext, int employeeId, int[] assignmentIds, string type)
        {
            var emailService = Helpers.GetEmailService();
            var recipient = "unknown recepient";
            var subject = type;
            try
            {
                var promoter= dbContext.Employees
                    .Where(e => e.Id == employeeId)
                    .Select(e => new {e.Firstname, e.Lastname, e.Email})
                    .Single();

                var assignments = dbContext.Assignments
                    .Where(e => assignmentIds.Contains(e.Id))
                    .Select(e => new
                    {
                        e.Id,
                        e.CampaignLocation.Campaign.Name,
                        e.DateStart,
                        e.DateEnd,
                        LocationName = e.CampaignLocation.Location.Name,
                        e.CampaignLocation.Location.Street,
                        e.CampaignLocation.Location.PostalCode,
                        e.CampaignLocation.Location.City
                    })
                    .OrderBy(e => e.DateStart)
                    .ToList();
                var assignmentStrings = assignments
                    .Select(e => string.Format("{0} {1:dd.MM.yyyy} Aktionszeiten: {1:HH:mm} - {2:HH:mm}", e.Name, e.DateStart, e.DateEnd));

                var mail = new DeferredAssignmentMail(type + "Days", "mjania")
                {
                    Firstname = promoter.Firstname,
                    Assignments = assignmentStrings
                };

                mail.AddRecipient(promoter.Email);
                recipient = mail.To;
                if (type == "Decline")
                {
                    // Nur auf Live die Advantage-User BCC setzen
                    if (MvcApplication.IsStaging == false)
                    {
#if ADVANTAGE
                        var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.Advantage);
#else
                        var users = Roles.GetUsersInRole(SeismitoadShared.Constants.Roles.User);
#endif
                        foreach (var user in users)
                        {
                            var userObj = Membership.GetUser(user, false);
                            mail.Bcc.Add(userObj.Email);
                        }
                    }
                }
                else
                {
                    // Confirm => dann speichern wir die Mail als Datei
                    var mailMessage = emailService.CreateMailMessage(mail);
                    var pathToSaveMail = SaveMailToDisk(employeeId, mailMessage);
                    var filename = Path.GetFileName(pathToSaveMail);
                    foreach (var assignment in assignments)
                    {
                        var confirmation = dbContext.ConfirmationEmails
                            .SingleOrDefault(e => e.AssignmentId == assignment.Id && e.EmployeeId == employeeId);
                        if (confirmation == null)
                        {
                            confirmation = new ConfirmationEmail();
                            dbContext.ConfirmationEmails.Add(confirmation);
                        }
                        confirmation.AssignmentId = assignment.Id;
                        confirmation.EmployeeId = employeeId;
                        confirmation.DateStart = assignment.DateStart;
                        confirmation.DateEnd = assignment.DateEnd;
                        confirmation.Filename = filename;
                        confirmation.LocationName = assignment.LocationName;
                        confirmation.Street = assignment.Street;
                        confirmation.PostcalCode = assignment.PostalCode;
                        confirmation.City = assignment.City;
                        confirmation.Firstname = promoter.Firstname;
                        confirmation.Lastname = promoter.Lastname;
                    }
                    dbContext.SaveChanges();
                }
                emailService.Send(mail);
            }
            catch (Exception exception)
            {
                var loggedException = new Exception(
                    $"Failed sending notification e-mail with subject '{subject}' to {recipient}", exception);
                ErrorLog.GetDefault(null).Log(new Error(loggedException));
                throw;
            }
        }

        private static string SaveMailToDisk(int employeeId, MailMessage mail)
        {
            var smptClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,
                EnableSsl = false,
                PickupDirectoryLocation = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString())
            };
            Directory.CreateDirectory(smptClient.PickupDirectoryLocation);
            smptClient.Send(mail);
            var mailFile = Directory.GetFiles(smptClient.PickupDirectoryLocation).Single();
            var confirmationMailDirectory = ConfigurationManager.AppSettings.Get("ConfirmationMailDirectory");
            confirmationMailDirectory = Path.Combine(confirmationMailDirectory, "" + employeeId);
            Directory.CreateDirectory(confirmationMailDirectory);
            var destFileName = Path.Combine(confirmationMailDirectory, $"{Guid.NewGuid()}.eml");
            File.Copy(mailFile, destFileName, true);
            File.Delete(mailFile);
            Directory.Delete(smptClient.PickupDirectoryLocation);
            return destFileName;
        }

        public class DeferredMail
        {
            public int Id { get; set; }
            public int EmployeeId { get; set; }
            public string Type { get; set; }
            public string AssignmentIds { get; set; }
        }

    }
}