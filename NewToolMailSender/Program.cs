﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using NewToolMailSender.Models;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace NewToolMailSender
{
    class Program
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["SeismitoadDbContext"].ConnectionString;

        static void Main(string[] args)
        {
            Console.WriteLine("Press [h] for help.");
            FixRazorEngineStuff();
            using (var connection = new SqlConnection(ConnectionString))
            {
                while (true)
                {
                    Console.WriteLine();
                    Console.Write("Enter command: ");
                    var keyChar = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                    switch (keyChar)
                    {
                        case 'h':
                            ShowHelp();
                            break;
                        case 'x':
                            AddBcc = !AddBcc;
                            Console.WriteLine("BCC is now " + (AddBcc ? "ON" : "OFF"));
                            break;
                        case 'q':
                            return;
                        case 'a':
                            ShowAmountSent(connection);
                            break;
                        case 'u':
                            ShowAmountUnsent(connection);
                            break;
                        case 's':
                            SendSingleMail(connection);
                            break;
                        case 'b':
                            var noMoreMails = false;
                            for (var i = 0; i < 20 && !noMoreMails; i++)
                            {
                                noMoreMails = !SendSingleMail(connection);
                            }
                            break;
                        case 't':
                            SendThottled(connection);
                            break;
                        default:
                            Console.WriteLine("Unknown command");
                            break;
                    }
                }
            }
        }

        private static void SendThottled(SqlConnection connection)
        {
            Console.WriteLine("How many mails should be sent? ");
            Console.Write("[a] 1.000 [b] 2.000 [c] 5.000 [d] 10.000: ");
            int amount;
            var keyChar = Console.ReadKey().KeyChar;
            switch (keyChar)
            {
                case 'a':
                    amount = 1000;
                    break;
                case 'b':
                    amount = 2000;
                    break;
                case 'c':
                    amount = 5000;
                    break;
                case 'd':
                    amount = 10000;
                    break;
                default:
                    Console.WriteLine("Unknown amount of mails.");
                    return;
            }

            Console.WriteLine();
            Console.WriteLine("About to send {0:#,###} mails.", amount);
            Console.WriteLine("Press any key to stop sending mails");

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            
            var task = Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < amount && token.IsCancellationRequested == false; i++)
                {
                    if (!SendSingleMail(connection))
                    {
                        // False => Es gibt keine weiteren Mails
                        return;
                    }
                    Thread.Sleep(2000);
                }
                if (token.IsCancellationRequested == false)
                {
                    Console.WriteLine("Done. Press any key to continue.");
                }
            }, token);
            
            Console.ReadKey();
            tokenSource.Cancel(); // Macht hoffentlich keine Probleme, wenn der Task schon fertig ist
        }

        private static void FixRazorEngineStuff()
        {
            // Brauchen wir eventuell...
            var config = new TemplateServiceConfiguration
            {
                BaseTemplateType = typeof (HtmlTemplateBase<>)
            };

            var service = new TemplateService(config);
            Razor.SetTemplateService(service);
        }

        public static bool AddBcc = false;

        private static bool SendSingleMail(IDbConnection connection)
        {
            const string query =
@"SELECT TOP 1 Id, Email, Firstname, users.UserName, StartCode
FROM Employees INNER JOIN
[seismitoad_aspnet].[dbo].[aspnet_Users] users ON users.UserId = ProviderUserKey
WHERE StartCode IS NOT NULL AND StartCode <> '___INVALID___' AND ISNULL(StartCodeSent, 0) = 0";

            int employeeId = -1;
            try
            {
                var model = connection.Query<StartMailModel>(query).Single();
                employeeId = model.Id;

                Console.WriteLine("Sending mail to {0} ({1}) with start code \"{2}\"", model.Firstname, model.Email,
                    model.StartCode);

                var mailer = new Mailer();
                var basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                var linkedResource1 = new LinkedResource(Path.Combine(Path.Combine(basePath, "Templates"), "image001.jpg"), MediaTypeNames.Image.Jpeg);
                linkedResource1.ContentId = "image001.jpg";
                var linkedResource2 = new LinkedResource(Path.Combine(Path.Combine(basePath, "Templates"), "image002.jpg"), MediaTypeNames.Image.Jpeg);
                linkedResource2.ContentId = "image002.jpg";

                var newToolMail = mailer.NewToolMail(model);
                var view = newToolMail.Mail.AlternateViews.First(e => e.ContentType.MediaType == MediaTypeNames.Text.Html);
                view.LinkedResources.Add(linkedResource1);
                view.LinkedResources.Add(linkedResource2);
                newToolMail.Deliver();

                connection.Execute("UPDATE Employees SET StartCodeSent = 1 WHERE Id = @EmployeeId",
                    new {EmployeeId = model.Id});
            }
            catch(InvalidOperationException)
            {
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                File.AppendAllText(@"C:\Logs\mail.txt", string.Format("{0}: {1}\n", employeeId, e.Message));
                connection.Execute("UPDATE Employees SET StartCode = '___INVALID___' WHERE Id = @EmployeeId", new { EmployeeId = employeeId });
            }
            return true;
        }

        private static void ShowAmountSent(IDbConnection connection)
        {
            var amountSent = connection.Query<int>("SELECT COUNT(*) FROM Employees WHERE StartCodeSent = 1").Single();
            Console.WriteLine("{0} mails have already been sent", amountSent);
        }

        private static void ShowAmountUnsent(IDbConnection connection)
        {
            const string query = @"SELECT COUNT(*) FROM Employees WHERE ISNULL(StartCodeSent, 0) = 0
                                   SELECT COUNT(*) FROM Employees WHERE ISNULL(StartCodeSent, 0) = 0 AND StartCode IS NOT NULL";
            var result = connection.QueryMultiple(query);
            Console.WriteLine("{0} are unsent. {1} of them have a startcode and thus can be sent",
                result.Read<int>().Single(), result.Read<int>().Single());
        }

        private static void ShowHelp()
        {
            Console.WriteLine("[h] show this help");
            Console.WriteLine("[x] toggle BCC");
            Console.WriteLine("[a] show amount sent");
            Console.WriteLine("[u] show amount unsent");
            Console.WriteLine("[s] send single mail");
            Console.WriteLine("[b] send batch of 20 mails");
            Console.WriteLine("[t] send throttled");
            Console.WriteLine("[q] quit");
            Console.WriteLine();
        }
    }
}
