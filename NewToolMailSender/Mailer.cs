﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using ActionMailerNext.Standalone;
using NewToolMailSender.Models;

namespace NewToolMailSender
{
    class Mailer : RazorMailerBase 
    {
        public override string ViewPath
        {
            get { return "Templates"; }
        }

        public RazorEmailResult NewToolMail(StartMailModel model)
        {
            // Setting up needed properties
            MailAttributes.From = new MailAddress("noreply@advantage-promotion.de");
            //MailAttributes.To.Add(new MailAddress("sbabilas@permedia.de"));
            //MailAttributes.To.Add(new MailAddress("jl@advantagemarketingservice.de"));
            //MailAttributes.To.Add(new MailAddress("cz@advantagemarketingservice.de"));
            MailAttributes.To.Add(new MailAddress(model.Email));
            if(Program.AddBcc)
            {
                MailAttributes.Bcc.Add(new MailAddress("mikko.jania@signalkontor.com"));
            }
            MailAttributes.Subject = "Es gibt Neuigkeiten bei advantage: Deine persönliche Seite ist online!";
            MailAttributes.Priority = MailPriority.High;

            //Calling the view which form the email body
            return Email("StartMail", model);
        }

    }
}
