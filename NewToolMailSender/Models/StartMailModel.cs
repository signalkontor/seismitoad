﻿namespace NewToolMailSender.Models
{
    public class StartMailModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string UserName { get; set; }
        public string StartCode { get; set; }
    }
}