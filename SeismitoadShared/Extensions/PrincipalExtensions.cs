using System;
using System.Security.Principal;
using System.Web.Security;

namespace SeismitoadShared.Extensions
{
    public static class PrincipalExtensions
    {
        public static Guid GetProviderUserKey(this IPrincipal user)
        {
            if(!user.Identity.IsAuthenticated)
                throw new InvalidOperationException("User must be authenticated");

// ReSharper disable PossibleNullReferenceException
            return (Guid)Membership.GetUser(user.Identity.Name).ProviderUserKey;
// ReSharper restore PossibleNullReferenceException
        }
    }
}