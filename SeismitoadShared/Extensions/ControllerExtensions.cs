﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace SeismitoadShared.Extensions
{
    public static class ControllerExtensions
    {
        public static void TelerikGridRequestParameters(this Controller controller, string gridName, int pageSize, out int page, out int size, out string orderBy, out string groupBy, out string filter)
        {
            page = int.Parse(controller.Request[string.Format("{0}-page", gridName)] ?? "0");
// ReSharper disable SpecifyACultureInStringConversionExplicitly
            size = int.Parse(controller.Request[string.Format("{0}-size", gridName)] ?? pageSize.ToString());
// ReSharper restore SpecifyACultureInStringConversionExplicitly
            orderBy = controller.Request[string.Format("{0}-orderBy", gridName)];
            groupBy = controller.Request[string.Format("{0}-groupBy", gridName)];
            filter = controller.Request[string.Format("{0}-filter", gridName)];
        }

        public static RouteValueDictionary CleanRouteValues(this Controller controller)
        {
            var routeValueDictionary = controller.Request.QueryString.ToRouteValueDictionary();
            var key = routeValueDictionary.Keys.FirstOrDefault(e => e.EndsWith("-mode"));
            if (key != null)
                routeValueDictionary.Remove(key);
            return routeValueDictionary;
        }
    }
}
