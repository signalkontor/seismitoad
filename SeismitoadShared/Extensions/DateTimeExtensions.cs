﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadShared.Extensions
{
    public static class DateTimeExtensions
    {
        public static int Quarter(this DateTime dateTime)
        {
            return (dateTime.Month - 1)/3 + 1;
        }

        public static int PreviousQuarter(this DateTime dateTime)
        {
            return dateTime.AddMonths(-3).Quarter();
        }

        public static int NextQuarter(this DateTime dateTime)
        {
            return dateTime.AddMonths(3).Quarter();
        }
    }
}
