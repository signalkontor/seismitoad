﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace SeismitoadShared.App_Start
{
    public class DefaultRouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}"); 
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("apple-touch-icon.png");
            routes.IgnoreRoute("{*appleIcons}", new { appleIcons = @"apple-touch-icon-.*\.png(/.*)?" }); // ignore all other apple-touch-icons
            routes.IgnoreRoute("{*siteMaps}", new { siteMaps = @"(google_)?sitemap(\.xml)?(\.gz)?" });
            routes.IgnoreRoute("crossdomain.xml");
            routes.IgnoreRoute("{*autodiscover}", new { autodiscover = "(.*/)autodiscover.xml" });
            routes.IgnoreRoute("{php}", new { php = @"\.php(\?.*|/.*)*$" });
            routes.IgnoreRoute("Report/dd.MM.yyyy");
        }
    }
}
