﻿using System.Collections.Generic;
using System.Web.Routing;

namespace SeismitoadShared.Models.ViewModels
{
    public class SkMenuItem
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public bool SaveState { get; set; }
        public IEnumerable<SkSubMenuItem> Items { get; set; }
    }

    public class SkSubMenuItem
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public RouteValueDictionary RouteValueDictionary { get; set; }
        public string Url { get; set; }
        public bool OpenInNewWindow { get; set; }
    }
}