﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeismitoadShared.Models.ViewModels
{
    public class SubmitViewModel
    {
        public bool ShowBackButton { get; set; }
        public bool BackButtonAsJavascript { get; set; }
        public string BackButtonUrl { get; set; }
        public string BackButtonText { get; set; }
        public string SubmitButtonText { get; set; }
        
        public SubmitViewModel(bool showBackButton = true,
            bool backButtonAsJavascript = true,
            string backButtonText = "Zurück",
            string submitButtonText = "Weiter",
            string backButtonUrl = null)
        {
            ShowBackButton = showBackButton;
            BackButtonAsJavascript = backButtonAsJavascript;
            BackButtonText = backButtonText;
            SubmitButtonText = submitButtonText;
            BackButtonUrl = backButtonUrl;
        }

        public string GetBackButtonHref()
        {
            if (ShowBackButton)
            {
                return BackButtonAsJavascript
                           ? "javascript:history.back();"
                           : BackButtonUrl;
            }
            return null;
        }
    }
}
