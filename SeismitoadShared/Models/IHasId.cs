namespace SeismitoadShared.Models
{
    public interface IHasId
    {
        int Id { get; set; }
    }
}
