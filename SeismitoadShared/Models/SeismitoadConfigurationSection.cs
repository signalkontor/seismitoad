using System;
using System.ComponentModel;
using System.Configuration;

namespace SeismitoadShared.Models
{
   [Obsolete("Use CustomerInstance.Settings")]
   public class SeismitoadConfigurationSection : ConfigurationSection
    {
        public static readonly SeismitoadConfigurationSection Current =
            (SeismitoadConfigurationSection)ConfigurationManager.GetSection("seismitoad/settings");

        [ConfigurationProperty("campaignId", IsRequired = true)]
        public int CampaignId
        {
            get
            {
                return (int)this["campaignId"];
            }
            set
            {
                this["campaignId"] = value;
            }
        }

        [ConfigurationProperty("salesRegionLabel", IsRequired = false, DefaultValue = "Vertriebsregion")]
        public string SalesRegionLabel
        {
            get
            {
                return (String)this["salesRegionLabel"];
            }
            set
            {
                this["salesRegionLabel"] = value;
            }
        }

        [ConfigurationProperty("messageAfterReCheck", IsRequired = false, DefaultValue = null)]
        public string MessageAfterReCheck
        {
            get
            {
                return (String)this["messageAfterReCheck"];
            }
            set
            {
                this["messageAfterReCheck"] = value;
            }
        }

        [ConfigurationProperty("showDownloadSection", DefaultValue = "false", IsRequired = false)]
        public Boolean ShowDownloadSection
        {
            get
            {
                return (Boolean)this["showDownloadSection"];
            }
            set
            {
                this["showDownloadSection"] = value;
            }
        }

        [ConfigurationProperty("pictureDownloadEnabled", DefaultValue = "false", IsRequired = false)]
        public Boolean PictureDownloadEnabled
        {
            get
            {
                return (Boolean)this["pictureDownloadEnabled"];
            }
            set
            {
                this["pictureDownloadEnabled"] = value;
            }
        }

        [ConfigurationProperty("pictureDownloadMaxFiles", DefaultValue = "100", IsRequired = false)]
        public Int32 PictureDownloadMaxFiles
        {
            get
            {
                return (Int32)this["pictureDownloadMaxFiles"];
            }
            set
            {
                this["pictureDownloadMaxFiles"] = value;
            }
        }
        [ConfigurationProperty("withoutSalesRegion", DefaultValue = false, IsRequired = false)]
        public Boolean WithoutSalesRegion
        {
            get
            {
                return (Boolean)this["withoutSalesRegion"];
            }
            set
            {
                this["withoutSalesRegion"] = value;
            }
        }


        [ConfigurationProperty("showAttendanceInAnalysis", DefaultValue = "false", IsRequired = false)]
        public Boolean ShowAttendanceInAnalysis
        {
            get
            {
                return (Boolean)this["showAttendanceInAnalysis"];
            }
            set
            {
                this["showAttendanceInAnalysis"] = value;
            }
        }

        [ConfigurationProperty("showSalesRegionFilterInAnalysis", DefaultValue = "false", IsRequired = false)]
        public Boolean ShowSalesRegionFilterInAnalysis
        {
            get
            {
                return (Boolean)this["showSalesRegionFilterInAnalysis"];
            }
            set
            {
                this["showSalesRegionFilterInAnalysis"] = value;
            }
        }

        [ConfigurationProperty("showTeamAssignments", DefaultValue = "false", IsRequired = false)]
        public Boolean ShowTeamAssignments
        {
            get
            {
                return (Boolean)this["showTeamAssignments"];
            }
            set
            {
                this["showTeamAssignments"] = value;
            }
        }

        [ConfigurationProperty("checkAttendanceAfterLogin", DefaultValue = "false", IsRequired = false)]
        public Boolean CheckAttendanceAfterLogin
        {
            get
            {
                return (Boolean)this["checkAttendanceAfterLogin"];
            }
            set
            {
                this["checkAttendanceAfterLogin"] = value;
            }
        }

        [ConfigurationProperty("instanceTitle", DefaultValue = "", IsRequired = false)]
        public String InstanceTitle
        {
            get
            {
                return (String)this["instanceTitle"];
            }
            set
            {
                this["instanceTitle"] = value;
            }
        }

        [ConfigurationProperty("headerLogoLink", IsRequired = false)]
        public String HeaderLogoLink
        {
            get
            {
                return (String)this["headerLogoLink"];
            }
            set
            {
                this["headerLogoLink"] = value;
            }
        }

        [ConfigurationProperty("theme", DefaultValue = "~/", IsRequired = true)]
        [RegexStringValidator(@"^~/.*$")]
        public String Theme
        {
            get
            {
                return (String)this["theme"];
            }
            set
            {
                this["theme"] = value;
            }
        }

        [ConfigurationProperty("reporterRedirectAfterLogin", DefaultValue = "/News", IsRequired = false)]
        [RegexStringValidator(@"^/.*$")]
        public String ReporterRedirectAfterLogin
        {
            get
            {
                return (String)this["reporterRedirectAfterLogin"];
            }
            set
            {
                this["reporterRedirectAfterLogin"] = value;
            }
        }

        [ConfigurationProperty("chartDefaultTheme", DefaultValue = "Metro", IsRequired = false)]
        public String ChartDefaultTheme
        {
            get
            {
                return (String)this["chartDefaultTheme"];
            }
            set
            {
                this["chartDefaultTheme"] = value;
            }
        }

        [ConfigurationProperty("masterInstanceUrl", IsRequired = true)]
        public String MasterInstanceUrl
        {
            get
            {
                return (String)this["masterInstanceUrl"];
            }
            set
            {
                this["masterInstanceUrl"] = value;
            }
        }

        [ConfigurationProperty("reportEditorExcludePropertyErrors", DefaultValue = true, IsRequired = false)]
        public bool ReportEditorExcludePropertyErrors
        {
            get
            {
                return (bool)this["reportEditorExcludePropertyErrors"];
            }
            set
            {
                this["reportEditorExcludePropertyErrors"] = value;
            }
        }

        [ConfigurationProperty("budgetManagementEnabled", DefaultValue = false, IsRequired = false)]
        public bool BudgetManagementEnabled
        {
            get
            {
                return (bool)this["budgetManagementEnabled"];
            }
            set
            {
                this["budgetManagementEnabled"] = value;
            }
        }

        // Create a "SalesManagement element."
        [ConfigurationProperty("salesManagement", IsRequired = false)]
        public SalesManagementElement SalesManagement
        {
            get
            {
                return (SalesManagementElement)this["salesManagement"];
            }
            set
            {
                this["salesManagement"] = value;
            }
        }

        // Create a "jQueryUI element."
        [ConfigurationProperty("jQueryUI", IsRequired = true)]
        public JQueryUIElement JQueryUI
        {
            get
            {
                return (JQueryUIElement)this["jQueryUI"];
            }
            set
            {
                this["jQueryUI"] = value;
            }
        }

        [ConfigurationProperty("azureStorage", IsRequired = false)]
        public AzureStorageElement AzureStorage
        {
            get
            {
                return (AzureStorageElement)this["azureStorage"];
            }
            set
            {
                this["azureStorage"] = value;
            }
        }
    }

    public class SalesManagementElement : ConfigurationElement
    {
        [ConfigurationProperty("enabled", IsRequired = true, DefaultValue = false)]
        public Boolean Enabled
        {
            get
            {
                return (Boolean)this["enabled"];
            }
            set
            {
                this["enabled"] = value;
            }
        }

        [ConfigurationProperty("version", DefaultValue = "1", IsRequired = false)]
        public Int32 Version
        {
            get
            {
                return (Int32)this["version"];
            }
            set
            {
                this["version"] = value;
            }
        }


        [ConfigurationProperty("allowUserCreatedItems", IsRequired = true)]
        public Boolean AllowUserCreatedItems
        {
            get
            {
                return (Boolean)this["allowUserCreatedItems"];
            }
            set
            {
                this["allowUserCreatedItems"] = value;
            }
        }

        [ConfigurationProperty("userCreatedItemPlaceholder", IsRequired = false, DefaultValue = "[Neu anlegen]")]
        public String UserCreatedItemPlaceholder
        {
            get
            {
                return (String)this["userCreatedItemPlaceholder"];
            }
            set
            {
                this["userCreatedItemPlaceholder"] = value;
            }
        }


        [ConfigurationProperty("askItemsInStock", IsRequired = true)]
        public Boolean AskItemsInStock
        {
            get
            {
                return (Boolean)this["askItemsInStock"];
            }
            set
            {
                this["askItemsInStock"] = value;
            }
        }

        [ConfigurationProperty("numberOfLinesPerTab", DefaultValue = "20", IsRequired = false)]
        public Int32 NumberOfLinesPerTab
        {
            get
            {
                return (Int32)this["numberOfLinesPerTab"];
            }
            set
            {
                this["numberOfLinesPerTab"] = value;
            }
        }

        [ConfigurationProperty("commissionModel", IsRequired = false, DefaultValue = null)]
        public String CommissionModel
        {
            get
            {
                return (String)this["commissionModel"];
            }
            set
            {
                this["commissionModel"] = value;
            }
        }

        [ConfigurationProperty("printViewForEmployeeEnabled", IsRequired = false, DefaultValue = false)]
        public Boolean PrintViewForEmployeeEnabled
        {
            get
            {
                return (Boolean)this["printViewForEmployeeEnabled"];
            }
            set
            {
                this["printViewForEmployeeEnabled"] = value;
            }
        }
    }

    public class JQueryUIElement : ConfigurationElement
    {
        [ConfigurationProperty("code", DefaultValue = "~/Scripts/jquery-ui-1.8.16.min.js", IsRequired = false)]
        [RegexStringValidator(@"^~/.+\.js$")]
        public String Code
        {
            get
            {
                return (String)this["code"];
            }
            set
            {
                this["code"] = value;
            }
        }

        [ConfigurationProperty("theme", DefaultValue = "~/Content/jqueryui/smoothness/jquery-ui-1.8.16.custom.css", IsRequired = true)]
        [RegexStringValidator(@"^~/.+\.css$")]
        public String Theme
        {
            get
            {
                return (String)this["theme"];
            }
            set
            {
                this["theme"] = value;
            }
        }
    }

    public class AzureStorageElement : ConfigurationElement
    {
        [ConfigurationProperty("enabled", IsRequired = true)]
        public Boolean Enabled
        {
            get
            {
                return (Boolean)this["enabled"];
            }
            set
            {
                this["enabled"] = value;
            }
        }

        [ConfigurationProperty("connectionString", IsRequired = true)]
        public String ConnectionString
        {
            get
            {
                return (String)this["connectionString"];
            }
            set
            {
                this["connectionString"] = value;
            }
        }

        [ConfigurationProperty("containerPrefix", IsRequired = true)]
        public String ContainerPrefix
        {
            get
            {
                return (String)this["containerPrefix"];
            }
            set
            {
                this["containerPrefix"] = value;
            }
        }
    }

}