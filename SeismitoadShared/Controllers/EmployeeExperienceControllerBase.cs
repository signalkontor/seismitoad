using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using ObjectTemplate;
using SeismitoadShared.Extensions;
using SeismitoadShared.Models;

namespace SeismitoadShared.Controllers
{
    public abstract class EmployeeExperienceControllerBase : Controller
    {
        [HttpPost]
        public virtual JsonResult GetSectorDetails()
        {
            var key = Request.Form.AllKeys.First(e => e.EndsWith("Sector"));
            var str = "1" + Request.Form[key];
            var selectListItems = Configurator.Instance.SelectListRepository.Get("SectorDetails", ViewData)
                .Where(e => e.Value.StartsWith(str));
            return Json(selectListItems);
        }

        [HttpPost]
        public virtual JsonResult GetTasks(string type)
        {
            if(type == null)
            {
                var key = Request.Form.AllKeys.First(e => e.EndsWith("Type"));
                type = Request.Form[key];
            }

            if (type == "3")
                return Json(new SelectListItem[0]);

            var str = "1" + type;
            var selectListItems = Configurator.Instance.SelectListRepository.Get("Task", ViewData)
                .Where(e => e.Value.Length < 4 || e.Value.StartsWith(str));
            return Json(selectListItems);
        }
    }
}
