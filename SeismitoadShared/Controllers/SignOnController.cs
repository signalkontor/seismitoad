﻿using System.Collections.Specialized;
using System.Web.Mvc;
using SeismitoadShared.Extensions;
using SeismitoadShared.Filters;
using System.Security.Principal;

namespace SeismitoadShared.Controllers
{
    public abstract class SignOnController : Controller
    {
        //
        // GET: /SignOn/To/{some url}
        public virtual ActionResult To(string url)
        {
            return To(User, url);
        }

        [NonAction]
        public static ActionResult To(IPrincipal user, string url)
        {
            var additionalParams = new NameValueCollection
                {
                    {"SSO", SingleSignOnFilter.GetSSOParameter(user.Identity.Name)}
                };
            return new RedirectResult(url.AddQueryString(additionalParams));
        }
    }
}
