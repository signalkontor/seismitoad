using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using SeismitoadShared.Extensions;
using SeismitoadShared.Models;

namespace SeismitoadShared.Controllers
{
    public abstract class CreateUpdateDeleteController<TEntity> : Controller where TEntity : class, IHasId, new()
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;
        protected Func<int?, IEnumerable<TEntity>> SelectFunc { private get; set; }
        protected Action<TEntity> OnBeforeSave { private get; set; }
        protected Action<TEntity> OnSave { private get; set; }

        protected CreateUpdateDeleteController()
        {
            // do nothing
        }

        protected CreateUpdateDeleteController(DbContext dbContext, DbSet<TEntity> dbSet)
        {
            _dbContext = dbContext;
            _dbSet = dbSet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Dummy Parameter der es erlaubt, dass Unterklassen Index()-Methoden mit einem int? Parameter haben.</param>
        /// <returns></returns>
        public virtual ActionResult Index(int? id)
        {
            return View(SelectFunc != null ? SelectFunc(id) : _dbSet);
        }

        [HttpPost]
        public virtual ActionResult Insert()
        {
            var entity = new TEntity();
            if (TryUpdateModel(entity))
            {
                _dbSet.Add(entity);
                _dbContext.SaveChanges();
// ReSharper disable Mvc.ActionNotResolved
                return RedirectToAction("Index", this.CleanRouteValues());
// ReSharper restore Mvc.ActionNotResolved
            }
            //The model is invalid - render the current view to show any validation errors
            return View("Index", SelectFunc != null ? SelectFunc(null) : _dbSet);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            var entity = _dbSet.Single(e => e.Id == id);
            _dbSet.Remove(entity);
            _dbContext.SaveChanges();
// ReSharper disable Mvc.ActionNotResolved
            return RedirectToAction("Index", this.CleanRouteValues());
// ReSharper restore Mvc.ActionNotResolved
        }

        [HttpPost]
        public virtual ActionResult Save(int id)
        {
            var entity = _dbSet.Single(e => e.Id == id);
            if (TryUpdateModel(entity))
            {
                if (OnBeforeSave != null)
                {
                    OnBeforeSave(entity);
                }
                _dbContext.SaveChanges();
                if (OnSave != null)
                {
                    OnSave(entity);
                }
// ReSharper disable Mvc.ActionNotResolved
                return RedirectToAction("Index", this.CleanRouteValues());
// ReSharper restore Mvc.ActionNotResolved
            }
            //The model is invalid - render the current view to show any validation errors
            return View("Index", SelectFunc != null ? SelectFunc(id) : _dbSet);
        }
    }
}
