﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SeismitoadShared.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CustomValidatorAttribute : ValidationAttribute
    {
        private readonly string _validatorName;
        public CustomValidatorAttribute(string validatorName)
        {
            _validatorName = validatorName;
        }

        public override bool RequiresValidationContext { get { return true; } }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Func<object, ValidationResult> validator;
            if (Validators.TryGetValue(_validatorName, out validator))
                return validator(validationContext.ObjectInstance);
            throw new InvalidOperationException("No such validator: " + _validatorName);
        }

        private static readonly Dictionary<string, Func<dynamic, ValidationResult>> Validators =
            new Dictionary<string, Func<dynamic, ValidationResult>>();

        public static void Add<T>(string validatorName, Func<T, ValidationResult> validator) where T : class
        {
            // Der etwas komische Ausdruck auf der rechten Seite konvertiert den
            // typisierten Lamda-Ausdruck in einen dynamischen.
            Validators[validatorName] = d => validator(d);
        }
    }
}
