﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadShared.Attributes
{
    [Obsolete("Use System.ComponentModel.DataAnnotations.EmailAddressAttribute")]
    public class EmailAttribute : RegularExpressionAttribute
    {
        private const string RegexEmailLenient = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

        private const string RegexEmailStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                                                + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                                                + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                                                + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                                                + @"[a-zA-Z]{2,}))$";

        public EmailAttribute() : this(true) { }
        public EmailAttribute(bool strict) : base(strict ? RegexEmailStrict : RegexEmailLenient)
        {
            ErrorMessage = "Das Feld \"{0}\" enthält keine gültige E-Mail-Adresse.";
        }
    }
}
