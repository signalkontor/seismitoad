﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadShared.Attributes
{
    public class AuthorizeExAttribute : AuthorizeAttribute
    {
        public bool AllowLocalRequests { get; set; }
        public List<string> AllowRequestsByIP { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // Check if we should always allow local requests and if this request is a local one
            if (AllowLocalRequests && httpContext.Request.IsLocal) return true;

            // Check if we should always allow requests by specific IPs and if this requests comes from one of those.
            if (AllowRequestsByIP != null && AllowRequestsByIP.Count > 0)
            {
                if (AllowRequestsByIP.Contains(httpContext.Request.UserHostAddress)) return true;
            }

            // Basic Authorization
            return base.AuthorizeCore(httpContext);
        }
    }
}
