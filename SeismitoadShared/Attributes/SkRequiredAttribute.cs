﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Resources;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadShared.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SkRequiredAttribute : Attribute, IMetadataAware
    {
        public string Roles { get; set; }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (metadata == null)
                throw new ArgumentNullException("metadata");

            // Es reicht IsRequired zu setzen, um eine Required-Validierung zu forcieren.
            // Der von zu ASP.NET MVC gehörende DataAnnotationsModelValidatorProvider fügt
            // nämlich automatisch ein RequiredAttribute hinzu, wenn IsRequired gesetzt ist.
            if (string.IsNullOrWhiteSpace(Roles))
                metadata.IsRequired = true;
            else
                metadata.IsRequired = Roles.Split(',').Select(s => s.Trim()).Any(HttpContext.Current.User.IsInRole);
        }
    }
}
