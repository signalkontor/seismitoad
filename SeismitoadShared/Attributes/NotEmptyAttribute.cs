﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadShared.Attributes
{
    /// <summary>
    /// Implements the validation of a generic list property having at least one element in it.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class NotEmptyAttribute : ValidationAttribute
    {
        private const string DefaultError = "'{0}' must have at least one element.";
        public NotEmptyAttribute()
            : base(DefaultError)
        {
        }

        public override bool IsValid(object value)
        {
            var list = value as IList;
            return (list != null && list.Count > 0);
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(ErrorMessageString, name);
        }
    }

}