﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ObjectTemplate.Attributes;

namespace SeismitoadShared.Constants
{
    public static class HtmlAttributes
    {
        public const int FullWidthColumnIndex = 1;
        public const int TwoThirdsWidthColumnIndex = 2;
        public const int SkToggle1 = 3; // Add more Toggles if needed

        public static void Initialize()
        {
            FormatAttribute.HtmlAttributes.Add(0, null); // do not remove!
            FormatAttribute.HtmlAttributes.Add(FullWidthColumnIndex, new { @class = "full-width" });
            FormatAttribute.HtmlAttributes.Add(TwoThirdsWidthColumnIndex, new { @class = "twothirds-width" });
            FormatAttribute.HtmlAttributes.Add(SkToggle1, new {@class = "sk-toggle-1"});
        }
    }
}
