﻿using System.Collections.Generic;

namespace SeismitoadShared.Helpers
{
    public static class StringHelpers
    {
        public static string Join(string seperator, string ellipsis, int totalLength, IEnumerable<object> objects)
        {
            var full = string.Join(seperator, objects);
            return full.Length <= totalLength ? full : full.Substring(0, totalLength - ellipsis.Length) + ellipsis;
        }
    }
}
