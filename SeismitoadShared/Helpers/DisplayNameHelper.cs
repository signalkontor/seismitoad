﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SeismitoadShared.Helpers
{
    public static class DisplayNameHelper
    {
        public static string GetDisplayName(Type type, string propertyName, bool preferShortName = false)
        {
            return GetDisplayName(type.GetProperty(propertyName), preferShortName);
        }

        public static string GetDisplayName(object obj, string propertyName, bool preferShortName = false)
        {
            return GetDisplayName(obj.GetType().GetProperty(propertyName), preferShortName);
        }

        public static string GetDisplayName(PropertyInfo propertyInfo, bool preferShortName = false)
        {
            // ReSharper disable PossibleNullReferenceException
            var displayAttributes = propertyInfo.GetCustomAttributes(typeof (DisplayAttribute), true);

            if(displayAttributes.Length > 0)
            {
                var displayAttribute = (displayAttributes[0] as DisplayAttribute);
                return preferShortName && !string.IsNullOrWhiteSpace(displayAttribute.ShortName) ? displayAttribute.ShortName : displayAttribute.Name;
            }

            var displayNameAttributes = propertyInfo.GetCustomAttributes(typeof (DisplayNameAttribute), true);
            if (displayNameAttributes.Length > 0)
            {
                return (displayAttributes[0] as DisplayNameAttribute).DisplayName;
            }

            // ReSharper restore PossibleNullReferenceException
            return propertyInfo.Name;
        }

        public static string GetDisplayDescription(PropertyInfo propertyInfo)
        {
            // ReSharper disable PossibleNullReferenceException
            var displayAttributes = propertyInfo.GetCustomAttributes(typeof(DisplayAttribute), true);

            if (displayAttributes.Length > 0)
            {
                var displayAttribute = (displayAttributes[0] as DisplayAttribute);
                return displayAttribute.Description;
            }

            // ReSharper restore PossibleNullReferenceException
            return null;
        }
    }
}