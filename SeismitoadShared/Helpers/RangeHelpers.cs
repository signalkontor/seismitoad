using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace SeismitoadShared.Helpers
{
    public static class RangeHelpers
    {
        public static T RangeMinimum<T>(Type type, string member, T defaultValue)
        {
            try
            {
                MemberInfo mi = type.GetMember(member)[0];
                var ra = (RangeAttribute)mi.GetCustomAttributes(typeof(RangeAttribute), false)[0];
                return (T)ra.Minimum;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        public static T RangeMaximum<T>(Type type, string member, T defaultValue)
        {
            try
            {
                MemberInfo mi = type.GetMember(member)[0];
                var ra = (RangeAttribute)mi.GetCustomAttributes(typeof(RangeAttribute), false)[0];
                return (T)ra.Maximum;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
    }
}
