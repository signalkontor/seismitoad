﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeismitoadShared.Helpers
{
    public static class PasswordHelper
    {
        private static readonly Random Random = new Random((int) LocalDateTime.Now.Ticks);

        public static string GeneratePassword(int passwordLength)
        {
            return new string(
                Enumerable
                    .Range(0, passwordLength)
                    .Select(i =>
                    {
                        switch (i % 3)
                        {
                            case 0:
                                var lower = (char)(0x61 + Random.Next(25));
                                if (lower == 'l') lower++;
                                return lower;
                            case 1:
                                // Zahlen ohne '0'
                                return (char)(0x31 + Random.Next(9));
                            default:
                                var upper = (char)(0x41 + Random.Next(25));
                                if (upper == 'I' || upper == 'O') upper++;
                                return upper;
                        }
                    }).ToArray());
        }
    }
}
