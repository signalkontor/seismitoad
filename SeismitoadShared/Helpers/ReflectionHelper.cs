﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using ObjectTemplate.Extensions;
using SeismitoadShared.Attributes;

namespace SeismitoadShared.Helpers
{
    public class ReflectionHelper
    {
        public static void WalkProperties(Type type, object obj, int depth, Action<PropertyInfo, object> action, IPrincipal user)
        {
            if (depth == 0)
                return;

            var propertyInfos = type.GetProperties();

            foreach (var propertyInfo in propertyInfos)
            {
                var excludeAttribute = propertyInfo.GetCustomAttributes(typeof(ExcludeFromExportAttribute), true).FirstOrDefault() as ExcludeFromExportAttribute;
                if (excludeAttribute != null)
                {
                    if (!excludeAttribute.AllowedRolesArray.Any() || !user.HasAnyRole(excludeAttribute.AllowedRolesArray))
                        continue;
                }
                var propertyType = propertyInfo.PropertyType;
                if (propertyType.IsValueType || propertyType == typeof(string))
                {
                    action(propertyInfo, obj);
                }
                else if (propertyType.IsClass && propertyType != typeof(IEnumerable))
                {
                    var newObj = obj == null ? null : propertyInfo.GetValue(obj, null);
                    WalkProperties(propertyInfo.PropertyType, newObj, depth - 1, action, user);
                }
            }
        }
    }
}
