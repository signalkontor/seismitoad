﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SeismitoadShared.ModelBinders
{
    public class TrimModelBinder : IModelBinder
    {
        private static bool TrimDisabled(ModelBindingContext bindingContext)
        {
            // Wenn ein Request-Parameter keinem Model zugeordnet ist verändern wir ihn lieber nicht.
            // Das kann immer dann der Fall sein, wenn auf den Parameter nur über Request.Params
            // zugegriffen wird, oder wenn es ein einfacher Action-Parameter ist.
            if (bindingContext.ModelMetadata == null || bindingContext.ModelMetadata.ContainerType == null)
                return true;

            // Nun wollen wir mehr über die Eigenschaft an die der Wert gebunden werden soll erfahren.
            // Für den Fall, dass der ContainerType keine entsprechend benannte Eigenschaft hat - was
            // eigentlich  nicht vorkommen dürfte - ändern wir den Wert lieber auch nicht.
            var property = bindingContext.ModelMetadata.ContainerType.GetProperty(bindingContext.ModelName);
            if (property == null)
                return true;

            // Wenn explizit [NoTrim] angegeben wurde, machen wir natürlich nichts.
            if (property.CustomAttributes.Any(a => a.AttributeType == typeof(NoTrimAttribute)))
                return true;

            // Eigentlich wollen wir bei Passwortfelder lieber kein Trim() durchführen. Allerdings
            // Scheint der Standard Membershipprovider sowieso ein Trim durchzuführen, also können
            // wir es auch ruhig selber machen, daher hier das return!
            return false;

            //var attributes = property.GetCustomAttributes(typeof (DataTypeAttribute), false)
            //                         .Cast<DataTypeAttribute>();

            //return attributes.Any(a => a.DataType == DataType.Password);
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var hasInvalidChars = false;
            ValueProviderResult valueResult = null;
            try
            {
                valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            }
            catch(HttpRequestValidationException)
            {
                hasInvalidChars = true;
                var unvalidatedValueProvider = bindingContext.ValueProvider as IUnvalidatedValueProvider;
                valueResult = unvalidatedValueProvider != null
                    ? unvalidatedValueProvider.GetValue(bindingContext.ModelName, true)
                    : new ValueProviderResult("", "", System.Threading.Thread.CurrentThread.CurrentCulture);
            }
                
            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                return null;
            
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueResult);
            if(hasInvalidChars)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("Das Feld '{0}' enthält ungültige Zeichen.", bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelName));
                bindingContext.ModelState.AddModelError("InvalidCharactersInInput", "Die Zeichen < und > sind als Eingabe nicht erlaubt.");
            }

            return TrimDisabled(bindingContext)
                       ? valueResult.AttemptedValue
                       : valueResult.AttemptedValue.Trim();
        }
    }

    /// <summary>
    /// Wenn ein Textfeld nicht automatisch getrimmt werden soll, kann man es mit diesem Attribut versehen.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class NoTrimAttribute : Attribute { }
}
