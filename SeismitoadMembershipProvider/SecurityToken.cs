using System.Globalization;

namespace SeismitoadMembershipProvider
{
    public class SecurityToken
    {
        private const string AuthenticationSecret = "a&s1##!+aWs29";

        public SecurityToken(string username, string password, int campaignId, string nonce)
        {
            var tmp = string.Format("{0}{1}{2}{3}{4}", username, password, campaignId, nonce, AuthenticationSecret);
            Value = tmp.ToSHA1();
        }

        public static SecurityToken ForServiceController(string nonce, int campaignId)
        {
            return new SecurityToken("ServiceController", "laraho53", campaignId, nonce);
        }

        public static SecurityToken ForDashboardController(int campaignId, long ticks)
        {
            return new SecurityToken("DashboardController", "laraho53", campaignId, ticks.ToString(CultureInfo.InvariantCulture));
        }

        public string Value { get; private set; }
    }
}
