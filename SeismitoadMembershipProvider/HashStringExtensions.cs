﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeismitoadMembershipProvider
{
    public static class HashStringExtensions
    {
        // ReSharper disable MemberCanBePrivate.Global
        public static string ToHexString(this IEnumerable<byte> bytes)
        {
            return string.Concat(bytes.Select(b => string.Format("{0:x2}", b)));
        }

        public static string ToHash(this string input, System.Security.Cryptography.HashAlgorithm algorithm)
        {
            return Encoding.UTF8.GetBytes(input).ToHash(algorithm);
        }

        public static string ToHash(this byte[] input, System.Security.Cryptography.HashAlgorithm algorithm)
        {
            var hash = algorithm.ComputeHash(input);
            return hash.ToHexString();
        }
        // ReSharper restore MemberCanBePrivate.Global

        // ReSharper disable InconsistentNaming
        public static string ToSHA1(this string input)
        {
            return input.ToHash(System.Security.Cryptography.SHA1.Create());
        }

        public static string ToSHA1(this byte[] input)
        {
            return input.ToHash(System.Security.Cryptography.SHA1.Create());
        }

        public static string ToMD5(this string input)
        {
            return input.ToHash(System.Security.Cryptography.MD5.Create());
        }
        // ReSharper restore InconsistentNaming
    }
}
