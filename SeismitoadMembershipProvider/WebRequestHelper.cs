﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SeismitoadMembershipProvider
{
    static class WebRequestHelper
    {
        private static readonly Random Rng = new Random((int)DateTime.Now.Ticks);

        static WebRequestHelper()
        {
            ServicePointManager.Expect100Continue = false;
        }

        public static void MakeRequest(string serviceUrl, string username, string password, int campaignId, out string[] roles, out int employeeId)
        {
            roles = null;
            employeeId = 0;
            var nonce1 = Rng.Next().ToString(CultureInfo.InvariantCulture);
            var nonce2 = Rng.Next().ToString(CultureInfo.InvariantCulture);
            var outgoingSecurityToken = new SecurityToken(username, password, campaignId, nonce1);
            var webRequest = WebRequest.Create(serviceUrl);

            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            using (var stream = new StreamWriter(webRequest.GetRequestStream()))
            {
                stream.Write("username={0}&password={1}&campaignId={2}&nonce1={3}&nonce2={4}&token={5}",
                    Uri.EscapeDataString(username), Uri.EscapeDataString(password), campaignId, nonce1, nonce2, outgoingSecurityToken.Value);
                stream.Flush();
            }
            
            var webResponse = webRequest.GetResponse();
            var responseStream = webResponse.GetResponseStream();
            if (responseStream == null)
                return;

            try
            {
                var streamReader = new StreamReader(responseStream, Encoding.ASCII);
                var result = streamReader.ReadToEnd();
                if (string.IsNullOrWhiteSpace(result) || result == "FAILED")
                {
                    return;
                }
                var values = result.Split(';');

                // Check token to validate that the response came from our server.
                var incomingSecurityToken = new SecurityToken(username, password, campaignId, nonce2);
                if (incomingSecurityToken.Value != values[0])
                {
                    return;
                }
                employeeId = int.Parse(values[1]);
                roles = new string[values.Length - 2];
                Array.Copy(values, 2, roles, 0, roles.Length);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
            {
            }
        }
    }
}
