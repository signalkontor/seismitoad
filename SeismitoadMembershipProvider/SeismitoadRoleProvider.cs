﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Security;

namespace SeismitoadMembershipProvider
{
    public class SeismitoadRoleProvider : RoleProvider
    {
        private static readonly object RolesForUserLocker = new object();
        private static readonly object EmployeeIdsForUserLocker = new object();
        private static readonly Dictionary<string, string[]> RolesForUser = new Dictionary<string, string[]>();
        private static readonly Dictionary<string, int> EmployeeIdsForUser = new Dictionary<string, int>();
        private string ServiceUrl { get; set; }
        public static int? CampaignId { get; set; }

        private static string GetConfigValue(string configValue, string defaultValue)
        {
            return String.IsNullOrEmpty(configValue) ? defaultValue : configValue;
        }

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
            ServiceUrl = GetConfigValue(config["serviceUrl"], "");
        }

        internal static void SetRolesForUser(string username, string[] roles)
        {
            lock (RolesForUserLocker)
            {
                RolesForUser[username] = roles;
            }
        }

        internal static void SetEmployeeIdForUser(string username, int id)
        {
            lock (EmployeeIdsForUserLocker)
            {
                EmployeeIdsForUser[username] = id;
            }
        }

        private void UpdateRolesIfNeccessary(string username)
        {
            if (CampaignId == null)
                throw new InvalidOperationException("CampaignId not set");

            lock (RolesForUserLocker)
            {
                if (RolesForUser.ContainsKey(username) && EmployeeIdsForUser.ContainsKey(username))
                    return;
            }

            string[] roles;
            int employeeId;
            WebRequestHelper.MakeRequest(ServiceUrl, username, "", CampaignId.Value, out roles, out employeeId);
            if (roles == null)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("WebRequestHelper.MakeRequest() did not return any roles."));
                roles = new string[0];
            }

            SetRolesForUser(username, roles);
            SetEmployeeIdForUser(username, employeeId);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            UpdateRolesIfNeccessary(username);
            lock (RolesForUserLocker)
            {
                return RolesForUser[username].Any(o => o == roleName);
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            UpdateRolesIfNeccessary(username);
            lock (RolesForUserLocker)
            {
                return RolesForUser[username];
            }
        }

        public int GetEmployeeIdForUser(string username)
        {
            UpdateRolesIfNeccessary(username);
            lock (EmployeeIdsForUserLocker)
            {
                return EmployeeIdsForUser[username];
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}
