    WICHTIG! Dieses Repository ist veraltet. Der aktuelle Stand findet sich auf https://signalkontor.visualstudio.com/

# README #

### Enthält ###

* Website: https://www.advantage-promotion.de
* Website: https://pm.permedia-report.de
* Gemeinsame Bibliotheken und Tools

### Deployment ###

Das Deployment der Anwendungen erfolgt aus TeamCity. Dabei wird das Konzept des BlueGreenDeployments verwendet (siehe [Martin Fowler - BlueGreenDeployment](http://martinfowler.com/bliki/BlueGreenDeployment.html)). Das heißt vor die eigentlichen Anwendungen ist ein Reverse-Proxy geschaltet. Dieser prüft anhand der URL /status.txt, auf welche Website (Blue oder Green) die Requests laufen sollen. Die Anwendungen enthalten im Root-Verzeichnis eine Datei status.txt mit dem Inhalt "down". Das heißt eine frisch eingespielte Version der Website ist erstmal inaktiv (down).

Daher ist es extrem wichtig, dass man nach Änderungen an der Anwendung diese auf die korrekte, zu dem Zeitpunkt inaktive Website, deployed. Ist beispielsweise blau aktiv und grün inaktiv und man deployed auf blau sind danach beide inaktiv und die Webasite nicht erreichbar. Daher muss immer auf die gerade inaktive Website (hier grün) deployed werden. Anschließend kann man auf dem Server selbst per localhost die eingespielte Anwendung testen. Wenn man zufrieden ist schaltet man anschließend von (in diesem Beispiel) blau auf grün um. Das macht man indem man bei grün in die status.txt "up" schreibt und bei blau "down". Auf dem Server liegen dafür auch Batch-Skripte bereit die das machen.

Im Falle der MasterInstance sollte man den Application Pool der inaktiven Anwendung stoppen, da sonst der Hangfire-Scheduler zweimal läuft, was nicht wünschenswert ist.

Durch diesen Wechsel zwischen blau und grün muss bei jedem Deployment in TeamCity der Name der Website manuell geändert werden (system.DeployIISAppPath).

    Hinweis: Aktuell funktioniert das Deployment über TeamCity nicht. Es muss aus Visual Studio deployed werden.

Blau und grün gilt individuell pro Website. Es ist also möglich das die PromoterInstance auf blau läuft und die MasterInstance auf grün.

**Sollte irgendein Teil dieser Hinweise zum Deployment unklar sein, wird von einem Deployment dringend abgeraten.**